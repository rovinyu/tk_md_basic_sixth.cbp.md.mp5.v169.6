#
#  Copyright Statement:
#  --------------------
#  This software is protected by Copyright and the information contained
#  herein is confidential. The software may not be copied and the information
#  contained herein may not be used or disclosed except with the written
#  permission of MediaTek Inc. (C) 2005
#
#  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
#  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
#  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
#  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
#  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
#  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
#  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
#  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
#  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
#  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
#
#  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
#  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
#  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
#  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
#  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
#  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
#  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
#  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
#  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
#  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
SHELL:=/bin/bash
# *************************************************************************
# Build path, directories
# *************************************************************************
FIXPATH        =  .
BUILDDIR       =  $(strip $(FIXPATH))/build
RELDIR         =  $(strip $(FIXPATH))/mtk_rel
COMMON_ROOT    =  common
PCORE_ROOT     =  c2k_core
UNIVERSAL_COMMON = ../common

PROJDIR        =  $(strip $(BUILDDIR))/$(strip $(PROJECT_NAME))/$(strip $(FLAVOR))
CUS_MTK_REL    =  $(strip $(RELDIR))/$(strip $(ORIGINAL_PROJECT_NAME))/$(strip $(ORIGINAL_FLAVOR))
COM_MTK_REL    =  $(strip $(RELDIR))/$(strip $(COMMON_ROOT))
PROJ_INTERNAL_DIR := $(subst build/,build_internal/,$(PROJDIR))

#include $(strip $(PROJDIR))/Custom.bld

TARGDIR        =  $(strip $(PROJDIR))/bin
TMPDIR         =  $(strip $(PROJDIR))/tmp
CUSTPACKDIR    =  $(strip $(PROJDIR))/custpack
BUILD_RELDIR   =  $(strip $(PROJDIR))/rel

OBJSDIR        =  $(strip $(TARGDIR))/obj
RULESDIR       =  $(strip $(TARGDIR))/dep
COMPLIBDIR     =  $(strip $(TARGDIR))/lib
COMPLOGDIR     =  $(strip $(TARGDIR))/log
MODULEINFODIR  =  $(strip $(TARGDIR))/module

CUS_MTK_LIB    = $(strip $(CUS_MTK_REL))/lib
COM_MTK_LIB    = $(strip $(COM_MTK_REL))/lib


TARGNAME       =  $(strip $(PROJECT_NAME))_MDBIN_$(strip $(SUB_BOARD_VER))_$(strip $(PLATFORM))_$(strip $(CHIP_VER))
TARGNAME_P     =  $(strip $(PROJECT_NAME))_$(strip $(SUB_BOARD_VER))_P_$(strip $(PLATFORM))_$(strip $(CHIP_VER))
#PLATLIBROOT    =  $(strip $(FIXPATH))\lib\$(strip $(PLATFORM))


TSTDIR         =  $(strip $(FIXPATH))/${CORE_ROOT}/service/tst
BUILD_TSTDIR := $(strip $(PROJDIR))/tst
TST_DB         =  $(strip $(TSTDIR))/database
TST_DB_SEC =  $(strip $(TSTDIR_SEC))/database
DBG_INFO_VERNO = $(subst .,_,$(VERNO))
CODE_GEN_LOG  := $(strip $(COMPLOGDIR))/codegen.log
BUILD_TST_DB         := $(BUILD_TSTDIR)/database
BUILD_TST_DB_SEC := $(BUILD_TSTDIR_SEC)/database
BPLGUINFOCUSTOMPREFIX = MDDB_P_$(strip $(PLATFORM))_$(strip $(CHIP_VER))
MODIS_TST_DB         := $(strip $(TST_DB))
MODIS_TST_DB_SEC := $(strip $(TST_DB_SEC))
MODISBUILD_TST_DB         := $(strip $(BUILD_TST_DB))
MODISBUILD_TST_DB_SEC := $(strip $(BUILD_TST_DB_SEC))
MODIS_RULESDIR := $(strip $(RULESDIR))
CGEN_L1_OUTDIR := $(strip $(PROJ_INTERNAL_DIR))/l1trace
ifeq ($(strip $(COMPILER)),RVCT)
  CGEN_DEF_FILE = Tgt_Cnf_RVCT
else ifeq ($(strip $(COMPILER)),GCC)
  CGEN_DEF_FILE = Tgt_Cnf_GCC
else ifeq ($(strip $(COMPILER)),ADS)
  CGEN_DEF_FILE = Tgt_Cnf
else
  CGEN_DEF_FILE = Tgt_Cnf
endif

BUILD_SYSDIR = $(strip $(PROJDIR))/custom/system
BUILD_CODEGENDIR = $(strip $(PROJDIR))/custom/driver/drv_gen

NOT_REMOVAL_DIR_FILE = make/${CORE_ROOT}/NotRemovalCodeDir.def



# Do not support new mmi feature file architecture
ifeq ($(strip $(CUSTOM_RELEASE)),TRUE)
  ifneq ($(strip $(CUSTOM_VERNO)),)
PURE_VERNO = $(subst .,_, $(subst $(call Upper,$(strip $(PROJECT_NAME))).,, $(subst MAUI_SW.,, $(subst MAUI_SW.CLASSB.,, $(call Upper,$(CUSTOM_VERNO))))))
  else
PURE_VERNO = $(subst .,_, $(subst $(call Upper,$(strip $(PROJECT_NAME))).,, $(subst MAUI_SW.,, $(subst MAUI_SW.CLASSB.,, $(call Upper,$(VERNO))))))
  endif
else
PURE_VERNO = $(subst .,_, $(subst $(call Upper,$(strip $(PROJECT_NAME))).,, $(subst MAUI_SW.,, $(subst MAUI_SW.CLASSB.,, $(call Upper,$(VERNO))))))
endif

BPMDMETADATABASE = BPMdMetaDatabase_$(strip $(PURE_VERNO))
BPLGUINFOCUSTOMSRCP := MDDB_P_InfoCustomAppSrcP_$(strip $(PLATFORM))_$(strip $(CHIP_VER))_$(strip $(PURE_VERNO)).EDB
NVDATABASENAME := MDDB.C2K.META_$(strip $(PLATFORM))_$(strip $(CHIP_VER))_$(strip $(PURE_VERNO)).EDB
NEWTARGNAME    =  $(strip $(TARGNAME)).$(strip $(PURE_VERNO))

PLATFORM_LIB   =

# version no dir
VERNODIR       =  $(strip $(PROJDIR))/verno

# summary log
LOG            =  $(strip $(TARGDIR))/link.log
BOOTLOADER_LOG =  $(strip $(TARGDIR))/boot.log
LNKERRORLOG    =  $(strip $(TARGDIR))/link_error.log

# Component's log file
COMPLOGDIR     =  $(strip $(TARGDIR))/log
COMPLINTLOGDIR     =  $(strip $(TARGDIR))/lint_log
MMI_FEATURES_LOG   =  $(strip $(COMPLOGDIR))/MMI_features.log
DB_CHECK_LOG   =  $(strip $(COMPLOGDIR))/db_check.log

INFOLOG := $(strip $(COMPLOGDIR))/info.log
INFOMAKELOG = $(strip $(COMPLOGDIR))/infomake.log
MCDDLL_INFOLOG = $(strip $(COMPLOGDIR))/mcddll_info.log
MCDDLL_UPDATE_LOG = $(strip $(COMPLOGDIR))/mcddll_update.log
INFOLOG_$(strip $(BLUETOOTH_SUPPORT)) = $(strip $(COMPLOGDIR_BT))/info_$(strip $(BLUETOOTH_SUPPORT)).log

ifeq ($(strip $(DEMO_PROJECT)),TRUE)
VERNOFILE     :=  make/Verno.bld
else
VERNOFILE     := 
endif

SLIM_BRANCH := $(call Upper,$(strip $(BRANCH)))
BRANCH   +=   $(call Upper,$(strip $(PROJECT_NAME)))
# *************************************************************************
# Set defaul value to eliminate "option not define" warning
# *************************************************************************
BM_NEW      = FALSE

INSIDE_MTK = 0
ifeq ($(strip $(call Upper,$(MTK_INTERNAL))),TRUE)
    INSIDE_MTK = 1
endif

ifndef BLUETOOTH_SUPPORT_SWITCH
  BLUETOOTH_SUPPORT_SWITCH=NONE
endif


SCATTERNAME = scat$(strip $(PLATFORM)).txt
SCATTERFILE := $(strip $(TARGDIR))/$(strip $(SCATTERNAME))
BL_SCATTERFILE := $(strip $(TARGDIR))/scatBL_$(strip $(PLATFORM)).txt


ifeq ($(strip $(COMPILER)),GCC)
  SCATTERNAME:= $(subst scat,lds,$(strip $(SCATTERNAME)))
  SCATTERFILE:= $(subst scat,lds,$(strip $(SCATTERFILE)))
  BL_SCATTERFILE:= $(subst scat,lds,$(strip $(BL_SCATTERFILE)))
endif

SCATTERFILE_FLASHTOOL := $(subst .txt,_flashtool.txt,$(strip $(SCATTERFILE)))
SCATTERFILE_FLASHTOOL_NAME := $(filter %.txt,$(subst /, ,$(SCATTERFILE_FLASHTOOL)))

# *************************************************************************
#  Implicit Rules and Compiler Options
# *************************************************************************
BIN_FILE       =  $(strip $(NEWTARGNAME)).bin
IMG_FILE       =  $(strip $(TARGNAME_P)).elf
SYM_FILE       =  $(strip $(TARGNAME_P)).sym
ROM_IMG_FILE   =  rompatch/$(strip $(PLATFORM))/$(strip $(CHIP_VER))/ROMSA_$(strip $(PLATFORM))_$(strip $(CHIP_VER)).sym
MAP_FILE       =  $(strip $(TARGNAME_P)).map
ifeq ($(strip $(COMPILER)),GCC)
LIS_FILE       =  $(strip $(TARGNAME_P)).map
else
LIS_FILE       =  $(strip $(TARGNAME)).lis
endif
# *************************************************************************
#  Settings for BOOTLOADER
# *************************************************************************
BTLDVERNODIR   =  $(strip $(FIXPATH))/boot
BUILDBTLDVERNODIR   =  $(strip $(PROJDIR))/verno_boot
BTLD_PREFIX    =  $(strip $(PROJECT_NAME))_BOOT_$(strip $(BTLD_VERNO))_$(strip $(PLATFORM))_$(strip $(PURE_VERNO))
BTLD_BIN_FILE  =  $(strip $(BTLD_PREFIX)).bin

ifndef MODIS_CONFIG
  MODIS_CONFIG := FALSE
endif
ifeq ($(strip $(MODIS_CONFIG)),TRUE)
  ifeq ($(strip $(call Upper,$(UE_SIMULATOR))),TRUE)
    MODIS_UESIM := UESim
  else
    MODIS_UESIM := MoDIS
  endif
endif

# *************************************************************************
# Settings for DSP
# *************************************************************************
DSPDIR   := $(strip $(FIXPATH))/obj/$(call Lower,$(strip $(DSP_SOLUTION)))
DSPM_IMG := $(strip $(DSPDIR))/$(call Lower,$(strip $(PLATFORM)))/$(strip $(DSP_IMG_FILE))

# *************************************************************************
# Settings for Guardian
# *************************************************************************
ifndef PARTIAL_SOURCE 
PARTIAL_SOURCE := FALSE
endif

# *************************************************************************
# File setting for linux/win
# *************************************************************************
ifeq ($(strip $(OS_VERSION)),MSWin32)
   CGEN := $(strip $(FIXPATH))\pcore\tools\Cgen.exe
   DRV_GEN := $(FIXPATH)\custom\driver\drv\Drv_Tool\DrvGen.exe
   CREATE_EMPTY_TRC_GEN_FILES := pcore\tools\CreateEmptyTrcGenFiles.exe
   MBL_COMPOSER := pcore\tools\MBL_Composer.exe
   DBG_INFOGEN  := pcore\tools\DbgInfoGen.exe
   UPDATE_IMG   := pcore\tools\update_img.exe
   HEADER_TEMP  :=  $(subst /,\,$(strip $(PROJDIR)))\header_temp
   HTOGETHER    := pcore\tools\hTogether\htogether_list.bat
   MinGWDir     := pcore\tools\MinGW\4.6.2\win
else
   CGEN := $(strip $(FIXPATH))/${CORE_ROOT}/tools/Cgen
   DRV_GEN := $(FIXPATH)/${CORE_ROOT}/custom/driver/drv/Drv_Tool/DrvGen
   CREATE_EMPTY_TRC_GEN_FILES := ${CORE_ROOT}/tools/CreateEmptyTrcGenFiles
   MBL_COMPOSER := ${CORE_ROOT}/tools/MBL_Composer
   DBG_INFOGEN  := ${CORE_ROOT}/tools/DbgInfoGen
   UPDATE_IMG   := ${CORE_ROOT}/tools/update_img
   HEADER_TEMP  :=  $(strip $(PROJDIR))/header_temp
   HTOGETHER    := ${CORE_ROOT}/tools/hTogether/htogether_list.sh
   MinGWDir     := ${CORE_ROOT}/tools/MinGW/4.6.2/linux
endif



