###############################################
# Define source file folder to SRC_LIST
###############################################

SRC_LIST =  $(OBJSDIR)/rfcust/rfcustbldtime.c
SRC_LIST  += hwd/rfdrivers/rfcust/nvram_rf_data_items.c
ifneq ($(filter MTK_DEV_RF_CUSTOMIZE, $(MODULE_DEFS)), )
  SRC_LIST  += hwd/rfdrivers/rfcust/hwdrfcustnvcfg.c
endif
###############################################
#  Define include path lists to INC_DIR
###############################################
INC_DIR = cust/hwd hwd/rfdrivers hwd dbm

ifeq ($(strip $(SIMULATION)),TRUE)
  INC_DIR += $(strip $(FIXPATH))/cust/hwd/$(strip $(USE_CUST_PLT))_SIM
else
  INC_DIR += $(strip $(FIXPATH))/cust/hwd/$(strip $(USE_CUST_PLT))
endif

###############################################
# Define the specified compile options to COMP_DEFS
###############################################
COMP_DEFS = 


###############################################
# Define the source file search paths to SRC_PATH
###############################################
SRC_PATH = 


