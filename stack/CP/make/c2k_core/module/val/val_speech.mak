###############################################
# Define source file folder to SRC_LIST
###############################################
SRC_LIST = $(OBJSDIR)/val_speech/val_speechbldtime.c

# ================
# MTK Audio/Speech
# ================

SRC_LIST  += val/val_speech/valspc_io.c
SRC_LIST  += val/val_speech/valspc_drv.c


# ================
###############################################
#  Define include path lists to INC_DIR
###############################################
INC_DIR +=  hwd/hwd_speech
INC_DIR +=  val/val_speech
INC_DIR +=  hwd/

###############################################
# Define the specified compile options to COMP_DEFS
###############################################
COMP_DEFS = 


###############################################
# Define the source file search paths to SRC_PATH
###############################################
SRC_PATH = 


