#
#  Copyright Statement:
#  --------------------
#  This software is protected by Copyright and the information contained
#  herein is confidential. The software may not be copied and the information
#  contained herein may not be used or disclosed except with the written
#  permission of MediaTek Inc. (C) 2005
#
#  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
#  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
#  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
#  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
#  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
#  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
#  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
#  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
#  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
#  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
#  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
#  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
#
#  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
#  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
#  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
#  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
#  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
#
#  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
#  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
#  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
#  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
#  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).

# *************************************************************************
# Include GNU Make Standard Library (GMSL)
# *************************************************************************
include ${CORE_ROOT}/tools/GMSL/gmsl
Upper = $(subst z,Z,$(subst y,Y,$(subst x,X,$(subst w,W,$(subst v,V,$(subst u,U,$(subst t,T,$(subst s,S,$(subst r,R,$(subst q,Q,$(subst p,P,$(subst o,O,$(subst n,N,$(subst m,M,$(subst l,L,$(subst k,K,$(subst j,J,$(subst i,I,$(subst h,H,$(subst g,G,$(subst f,F,$(subst e,E,$(subst d,D,$(subst c,C,$(subst b,B,$(subst a,A,$(1)))))))))))))))))))))))))))


CUSTOM_FLD_MAPPING = $(if $($(subst /,_,$(patsubst %/,%,$(subst //,/,$(subst \,/,$(call lc, $(1))))))), \
                     $($(subst /,_,$(patsubst %/,%,$(subst //,/,$(subst \,/,$(call lc, $(1))))))),\
                     $(if $(wildcard $(subst //,/,$(subst \,/,$(call lc, $(1))))/$(BOARD_VER)/$(strip $(CUSTOM_FLAVOR))),\
                     $(subst //,/,$(subst \,/,$(call lc, $(1))))/$(BOARD_VER)/$(strip $(CUSTOM_FLAVOR)),\
                     $(if $(wildcard $(subst //,/,$(subst \,/,$(call lc, $(1))))/$(BOARD_VER)/DEFAULT), \
                     $(subst //,/,$(subst \,/,$(call lc, $(1))))/$(BOARD_VER)/DEFAULT,)))


AUTO_MERGE_FILE_CHECK = $(if $(shell $(LIST_DIR) "$(call CUSTOM_FLD_MAPPING,$(1))/$(2)"  2>$(DEV_NUL)), $(call CUSTOM_FLD_MAPPING,$(1))/$(2), \
                        $(if $(shell $(LIST_DIR) "$(1)/_Default_BB/$(strip $(PLATFORM))/$(2)"  2>$(DEV_NUL)), $(1)/_Default_BB/$(strip $(PLATFORM))/$(2),))
# *************************************************************************
# Set defaul value to Ckmake flag
# *************************************************************************
DEPENDENCY_CONFLICT = FALSE
# *************************************************************************
# Dependency Check error message Interfaces
# *************************************************************************
# ------------------------------------------------------------------------
# Usage:        $(call DEP_ERR_SETA_OR_SETB,OptA,ValA,OptB,ValB)
#               $(call DEP_ERR_SETA_OR_SETB,OptA,ValA,OptB,ValB1/ValB2/.../ValBn)
#               $(call DEP_ERR_SETA_OR_SETB,OptA,ValA,OptB,non ValB)
# Output:       PLEASE set OptA as ValA or set OptB as ValB
#               PLEASE set OptA as ValA or set OptB as ValB1/ValB2/.../ValBn
#               PLEASE set OptA as ValA or set OptB as non ValB
#
# Example:      $(call DEP_ERR_SETA_OR_SETB,BROWSER_SUPPORT,OBIGO_Q05A,UNIFIED_MESSAGE_MARK_SEVERAL_SUPPORT,non OBIGO_Q05A)
#               PLEASE set BROWSER_SUPPORT as OBIGO_Q05A or set UNIFIED_MESSAGE_MARK_SEVERAL_SUPPORT as non OBIGO_Q05A
# ------------------------------------------------------------------------
DEP_ERR_SETA_OR_SETB = $(warning ERROR: PLEASE set $1 as $2 or set $3 as $4)
# ------------------------------------------------------------------------
# Usage:        $(call DEP_ERR_ONA_OR_OFFB,OptA,OptB)
#               $(call DEP_ERR_ONA_OR_OFFB,OptA,OptB$OptC&...&OptN)
# Output:       PLEASE turn on OptA or turn off OptB
#
# Example:      $(call DEP_ERR_ONA_OR_OFFB,NAND_SUPPORT,NAND_FLASH_BOOTING)
#               PLEASE turn on NAND_SUPPORT or turn off NAND_FLASH_BOOTING
# ------------------------------------------------------------------------
DEP_ERR_ONA_OR_OFFB = $(warning ERROR: PLEASE turn on $1 or turn off $2)
# ------------------------------------------------------------------------
# Usage:        $(call DEP_ERR_SETA_OR_OFFB,OptA,ValA,OptB)
#               $(call DEP_ERR_SETA_OR_OFFB,OptA,ValA1/ValA2/.../ValAn,OptB)
#               $(call DEP_ERR_SETA_OR_OFFB,OptA,non ValA,OptB)
# Output:       PLEASE set OptA as ValA or turn off OptB
#               PLEASE set OptA as ValA1/ValA2/.../ValAn or turn off OptB
#               PLEASE set OptA as non ValA or turn off OptB
#
# Example:      $(call DEP_ERR_SETA_OR_OFFB,MELODY_VER,DSP_WT_SYN,J2ME_SUPPORT)
#               PLEASE set MELODY_VER as DSP_WT_SYN or turn off J2ME_SUPPORT
# ------------------------------------------------------------------------
DEP_ERR_SETA_OR_OFFB = $(warning ERROR: PLEASE set $1 as $2 or turn off $3)
# ------------------------------------------------------------------------
# Usage:        $(call DEP_ERR_SETA_OR_ONB,OptA,ValA,OptB)
#               $(call DEP_ERR_SETA_OR_ONB,OptA,ValA1/ValA2/.../ValAn,OptB)
#               $(call DEP_ERR_SETA_OR_ONB,OptA,non ValA,OptB)
# Output:       PLEASE set OptA as ValA or turn on OptB
#               PLEASE set OptA as ValA1/ValA2/.../ValAn or turn on OptB
#               PLEASE set OptA as non ValA or turn on OptB
#
# Example:      $(call DEP_ERR_SETA_OR_ONB,MMS_SUPPORT,non OBIGO_Q05A,UNIFIED_MESSAGE_FOLDER)
#               PLEASE set MMS_SUPPORT as non OBIGO_Q05A or turn on UNIFIED_MESSAGE_FOLDER
# ------------------------------------------------------------------------
DEP_ERR_SETA_OR_ONB = $(warning ERROR: PLEASE set $1 as $2 or turn on $3)
# ------------------------------------------------------------------------
# Usage:        $(call DEP_ERR_OFFA_OR_OFFB,OptA,OptB)
# Output:       PLEASE turn off OptA or turn off OptB
#
# Example:      $(call DEP_ERR_OFFA_OR_OFFB,CMUX_SUPPORT,BLUETOOTH_SUPPORT)
#               PLEASE turn off CMUX_SUPPORT or turn off BLUETOOTH_SUPPORT
# ------------------------------------------------------------------------
DEP_ERR_OFFA_OR_OFFB = $(warning ERROR: PLEASE turn off $1 or turn off $2)
# ------------------------------------------------------------------------
# Usage:        $(call DEP_ERR_ONA_OR_ONB,OptA,OptB)
# Output:       PLEASE turn on OptA or turn on OptB
#
# Example:      $(call DEP_ERR_ONA_OR_ONB,UCM_SUPPORT,VOIP_SUPPORT)
#               PLEASE turn on UCM_SUPPORT or turn on VOIP_SUPPORT
# ------------------------------------------------------------------------
DEP_ERR_ONA_OR_ONB = $(warning ERROR: PLEASE turn on $1 or turn on $2)
# *************************************************************************
# Set defaul value to eliminate "option not define" warning
# *************************************************************************
BM_NEW      = FALSE

ifndef PROJECT_NAME
$(error PROJECT_NAME is not defined)
endif
ifndef PROJECT_MAKEFILE
$(error PROJECT_MAKEFILE is not defined)
endif
ifndef FLAVOR
$(error FLAVOR is not defined)
endif

# *************************************************************************
# Include temporary build script
# *************************************************************************

# Custom specific build script
include make/${CORE_ROOT}/Custom.bld         # Custom release build
# default cmd setting
include make/${CORE_ROOT}/cmd_cfg.mak

ifndef CUSTOM_RELEASE
  CUSTOM_RELEASE = FALSE         # Default custom release
endif
ifndef MTK_SUBSIDIARY
  MTK_SUBSIDIARY = FALSE
endif

-include make/Verno.bld
HW_VER := $(call Upper,$(strip $(PROJECT_NAME)))_HW
include $(PROJECT_MAKEFILE)

ifndef CUSTOM_FLAVOR
$(error CUSTOM_FLAVOR is not defined)
endif

-include $(strip $(TMPDIR))/~buildinfo.tmp
-include $(strip $(TMPDIR))/~net_path.tmp
-include $(strip $(TMPDIR))/cus_folder.tmp
-include make/${CORE_ROOT}/USER_SPECIFIC.mak


COMMINCDIRS    += $(CUSTOM_COMMINC)

#*************************************************************************
# auto-add ${CORE_ROOT}/interface/* folder to global include path if it exist *.h
#*************************************************************************
COMMINCDIRS += inc
COMMINCDIRS += shared
COMMINCDIRS += cust/fsm/Inc

ifeq ($(strip $(ETM_ENABLE)),TRUE)
COMMINCDIRS += hwd/rfdrivers/Etm
endif

COMMINCDIRS += shared/cross_core_umoly

ifndef RTOS
  RTOS = NUCLEUS
endif

ifndef RTOS_DEBUG
  RTOS_DEBUG = FALSE
endif

ifeq ($(strip $(RTOS)),NUCLEUS)
  COMMINCDIRS += nucleus/plus
  COMMINCDIRS += nucleus/plus/inc
endif

ifeq ($(strip $(RTOS)),NUCLEUS)
  ifeq ($(strip $(RTOS_DEBUG)),TRUE)
     COM_DEFS += NU_DEBUG
  else
     COM_DEFS += NU_NO_ERROR_CHECKING
  endif
endif

CPU_ARCH = ARM9

# ---------------------------------------------------------------
# The followings are global options from $(PROJECT_NAME).bld
# ---------------------------------------------------------------

ifdef PMIC
  ifneq ($(strip $(PMIC)),NONE)
    COM_DEFS    += PMIC_PRESENT
  endif

  PMIC_FOR_MT6735 = MT6328
  PMIC_FOR_MT6755 = MT6351
  PMIC_FOR_MT6750 = MT6353
  PMIC_FOR_MT6797 = MT6351
  PMIC_FOR_MT6757 = MT6351
  PMIC_FOR_MT6757P = MT6355

  ifneq ($(strip $(PMIC)),$(strip $(PMIC_FOR_$(strip $(PLATFORM)))))
    $(warning ERROR: $(strip $(PLATFORM)) does not support $(strip $(PMIC)))
    DEPENDENCY_CONFLICT = TRUE
  endif

  NEED_ADD_PMIC_COMPILE_LIST = MT6328 MT6351 MT6353 MT6355
  ifneq ($(filter $(NEED_ADD_PMIC_COMPILE_LIST),$(PMIC)),)
    ifeq ($(strip $(SIMULATION)),TRUE)
      COM_DEFS    += PMU_$(strip $(PMIC_FOR_$(strip $(SIMULATION_PLATFORM))))
    else
      COM_DEFS    += PMU_$(PMIC)
    endif
  endif
endif

ifdef UART3_SUPPORT
  ifeq ($(strip $(UART3_SUPPORT)),TRUE)

    COM_DEFS    += __UART3_SUPPORT__
    UART_PORT_FREE_COUNT = 1
  else
    UART_PORT_FREE_COUNT = 0
  endif
else
    UART_PORT_FREE_COUNT = 0
endif

UART_PORT_USED_COUNT = 0

ifeq ($(strip $(UART_PORT_FREE_COUNT)),1)
  ifeq ($(strip $(UART_PORT_USED_COUNT)),2)
    # uart count is not enough for engineer mode
    COM_DEFS += __ONLY_ONE_UART__
  endif
endif

ifeq ($(strip $(UART_PORT_FREE_COUNT)),0)
  ifneq ($(strip $(UART_PORT_USED_COUNT)),0)
    # uart count is not enough for engineer mode
    COM_DEFS += __ONLY_ONE_UART__
  endif
endif

 ifeq ($(strip $(SYS_DAT_SUPPORT)),TRUE)
    COM_DEFS += __DYNAMIC_ANTENNA_TUNING__
  endif

ifdef CHIP_VERSION_CHECK
  ifeq ($(strip $(CHIP_VERSION_CHECK)),TRUE)
    COM_DEFS += __CHIP_VERSION_CHECK__
  endif
else
  $(warning ERROR: The feature CHIP_VERSION_CHECK should be defined as TRUE or FALSE)
  DEPENDENCY_CONFLICT = TRUE
endif


SV5_PLATFORM = MT6735 MT6755 MT6797 MT6750 MT6757 MT6757P
SV5X_PLATFORM =

ARM9_PLATFORM = MT6735 MT6755 MT6797 MT6750 MT6757 MT6757P
ARM11_PLATFORM =
CR4_PLATFORM =
CR4_WITH_COPRO_PLATFORM =
ifdef PLATFORM
  ifneq ($(filter $(strip $(ARM9_PLATFORM)), $(strip $(PLATFORM))),)
    COM_DEFS    += __ARM9_MMU__
  endif
endif


ifndef VENDOR
  VENDOR = NONE
endif

ifdef VENDOR
  ifneq ($(strip $(VENDOR)),NONE)
    COM_DEFS +=  DEVAPP_RESOURCE
  endif
  COM_DEFS += VENDOR_$(strip $(VENDOR))
endif


# Determine if BOOTLOADER built required
NEED_BUILD_BOOTLOADER = FALSE
PSO_MODE_WORKAROUND = FALSE
ifeq ($(strip $(PLATFORM)),MT6735)
  NEED_BUILD_BOOTLOADER = TRUE
  BOOTLOADER_CHE = ON
endif

ifeq ($(strip $(PLATFORM)),MT6755)
  ifeq ($(strip $(SIMULATION)),TRUE)
    ifeq ($(strip $(SIMULATION_IPC)),SDIO)
      NEED_BUILD_BOOTLOADER = TRUE
      BOOTLOADER_CHE = ON
    endif
  endif
endif

ifneq ($(filter MT6757 MT6757P,$(PLATFORM)),)
  PSO_MODE_WORKAROUND = TRUE
endif

ifdef DSP_SOLUTION
  ifeq ($(strip $(DSP_SOLUTION)),DSPM)
    ifeq ($(filter $(strip $(ARM9_PLATFORM)), $(strip $(PLATFORM))),)
        $(warning ERROR: $(strip $(PLATFORM)) does not support $(strip $(DSP_SOLUTION)),only $(strip $(ARM9_PLATFORM)) series chips support $(strip $(DSP_SOLUTION)) DSP_SOLUTION)
        DEPENDENCY_CONFLICT = TRUE
    endif
  endif
endif


#end of option_HAL.mak
# *************************************************************************
# Environment and Tools
# *************************************************************************

include make/${COMMON_ROOT}/compiler.mak


# *************************************************************************
# Component Compile Options
# *************************************************************************

ifndef FLAVOR
   COM_DEFS     += __FLAVOR_NOT_PRESENT__
endif

ifdef FLAVOR
  ifneq ($(strip $(FLAVOR)),NONE)
    COM_DEFS    += __FLAVOR_$(strip $(FLAVOR))__
  else
    COM_DEFS    += __FLAVOR_NOT_PRESENT__
  endif
endif

ifeq ($(strip $(COMPILER)),RVCT)
  COM_DEFS += __RVCT__
endif

ifeq ($(strip $(COMPILER)),GCC)
  COM_DEFS += __OPTIMISE_TIME
endif

ifneq ($(filter $(strip $(PLATFORM)),$(SV5_PLATFORM)),)
    COM_DEFS += __SV5_ENABLED__
endif

# *************************************************************************
# Components
# *************************************************************************
#ifeq ($(strip $(CUSTOM_RELEASE)),TRUE)
#  ifneq ($(strip $(USE_CUST_PLT)),)
#    COMPLIST := rfcust
#  endif
#  COMPLIST += hwd_speech
#  COMPLIST += val_speech
#else
  ifeq ($(strip $(USE_MTK_SYS)),SSDVT)
    COMPLIST := cp exe hwd mon sys ssdvt
    COMPLIST += nucleus
  else
    #build pass module:
    COMPLIST := cp css cust verno_cust dbm exe fhc fsm hsc hwd hwd_speech iop ipc l1d
    COMPLIST += lec lmd logiq mon pst sec sys sddl
    COMPLIST += locsrv ota pe psw scc sms ps_custom rpc val_speech val
    COMPLIST += nucleus cvsd_codec msbc_codec ssd_algo_rvct sbp dbm_cust val_cust

    ifneq ($(strip $(USE_CUST_PLT)),)
      COMPLIST += rfcust
    endif

    ifneq ($(strip $(USE_CCIRQ_PATH)),NONE)
      COMPLIST += mdipc mdipc_user
    endif

    ifeq ($(strip $(CONFIG_UI)),NONE)
      COMPLIST += mmi
    endif

    ifneq ($(strip $(NO_DATASVC)),TRUE)
      COMPLIST += atc hlp rlpw rlpe
    endif

    ifneq ($(strip $(USE_1X_ONLY)),TRUE)
      COMPLIST += slc clc rmc fcp rcp
    endif

    ifeq ($(strip $(USE_RUIM)),TRUE)
      COMPLIST += uim
    endif

    ifeq ($(strip $(RF_MODULE)),MTK_ORIONC)
      COMPLIST += orioncdcr
    endif

    ifeq ($(strip $(RF_MODULE)),MT6176)
      COMPLIST += orionplusdcr
    endif

    ifeq ($(strip $(ETM_ENABLE)),TRUE)
   	  COMPLIST += etm
    endif
    ifeq ($(strip $(SIMULATION)),TRUE)
      ifeq ($(strip $(SIMULATION_RF_MODULE)),MTK_ORIONC)
        COMPLIST += orioncdcr
      endif
    endif

    ifeq ($(strip $(SLT)),TRUE)
      COMPLIST += slt
    endif

    ifeq ($(strip $(USE_SRLTE)),TRUE)
      COMPLIST += ll1a rtba
    endif

    ifeq ($(strip $(USE_ENCRYPT_VOICE)),TRUE)
      COMPLIST += crypt
    endif

    ifeq ($(strip $(TX_TAS_ENABLE)),TRUE)
      COMPLIST += tas
    endif
  endif
#endif

################################################################################
#
#      Configure Options
#
################################################################################
ifeq ($(strip $(CONFIG_UI)),NONE)
  SUPPORT_UTK=TRUE
endif

SUPPORT_DUMP_FAULT_LOG=TRUE

BOOT_LINK_FILE=$(call Lower,$(strip $(PLATFORM)))_boot.txt
ifeq ($(strip $(USE_MTK_SYS)),SSDVT)
  ifeq ($(strip $(ALL_IN_IRAM)),TRUE)
    CP_LINK_FILE=$(call Lower,$(strip $(PLATFORM)))_iram.txt
  else
    CP_LINK_FILE=$(call Lower,$(strip $(PLATFORM)))_ssdvt.txt
  endif
else
  ifeq ($(strip $(SIMULATION)),TRUE)
    CP_LINK_FILE=$(call Lower,$(strip $(SIMULATION_PLATFORM))).txt

    ifeq ($(strip $(PLATFORM)),MT6755)
      ifeq ($(strip $(SIMULATION_IPC)),CCIF)
        CP_LINK_FILE=mt6755.txt
      endif
    endif
  else
    CP_LINK_FILE=$(call Lower,$(strip $(PLATFORM))).txt
  endif
endif

################################################################################
#
#      System definitions
#
################################################################################

SYS_TARGET = $(ST_HW)

ifeq ($(strip $(SIMULATION)),TRUE)
  COM_DEFS += MTK_DEV_HW_SIM
  COM_DEFS += MTK_DEV_HW_SIM_$(strip $(PLATFORM)) MTK_DEV_HW_SIM_$(strip $(SUB_BOARD_VER))
  COM_DEFS += MTK_DEV_HW_SIM_RF
  COM_DEFS += MTK_DEV_HW_SIM_$(strip $(RF_MODULE)) MTK_DEV_HW_SIM_ORION_PLUS
endif

# Platform Options
COM_DEFS_FOR_MT6735  = MT6735 MT6735_$(strip $(CHIP_VER))
COM_DEFS_FOR_MT6755  = MT6755 MT6755_$(strip $(CHIP_VER))
COM_DEFS_FOR_MT6750  = MT6750 MT6750_$(strip $(CHIP_VER))
COM_DEFS_FOR_MT6797  = MT6797 MT6797_$(strip $(CHIP_VER))
COM_DEFS_FOR_MT6757  = MT6757 MT6757_$(strip $(CHIP_VER))
COM_DEFS_FOR_MT6757P  = MT6757P MT6757P_$(strip $(CHIP_VER))

COM_DEFS_FOR_PLATFORM =  COM_DEFS_FOR_$(strip $(PLATFORM))
ifdef $(COM_DEFS_FOR_PLATFORM)
  COM_DEFS += $(COM_DEFS_FOR_$(strip $(PLATFORM)))
  ifeq ($(strip $(SIMULATION)),TRUE)
    SYS_ASIC = $(SA_$(strip $(SIMULATION_PLATFORM)))
  else
    SYS_ASIC = $(SA_$(strip $(PLATFORM)))
  endif
else
  $(warning ERROR: [COM_DEFS_FOR_$(strip $(PLATFORM))] was not defined)
  DEPENDENCY_CONFLICT = TRUE
endif

ifeq ($(strip $(SO_VERSION)),)
  SYS_VERSION = $(SV_REV_A0)
else
  SYS_VERSION = $(SO_VERSION)
endif

# Board Options
MT6735_BOARD_NAME = DENALI
MT6755_BOARD_NAME = JADE
MT6797_BOARD_NAME = EVEREST
MT6757_BOARD_NAME = OLYMPUS

ifneq ($(strip $(BOARD_VER)),)
  COM_DEFS += $(BOARD_VER)
endif

COM_DEFS_FOR_DENALI = $(MT6735_BOARD_NAME)
COM_DEFS_FOR_JADE   = $(MT6755_BOARD_NAME)
COM_DEFS_FOR_EVEREST= $(MT6797_BOARD_NAME)
COM_DEFS_FOR_OLYMPUS= $(MT6757_BOARD_NAME)

COM_DEFS_FOR_SYS_BOARD = COM_DEFS_FOR_$(strip $(SUB_BOARD_VER))
ifdef $(COM_DEFS_FOR_SYS_BOARD)
  COM_DEFS += $(COM_DEFS_FOR_$(strip $(SUB_BOARD_VER)))
  COM_DEFS += BOARD_TYPE_NAME=\\\\\\\"$(strip $(SUB_BOARD_VER))\\\\\\\"
  COM_DEFS += BOARD_VERSION=\\\\\\\"$(strip $(HW_VERSION))\\\\\\\"
  ifeq ($(strip $(SIMULATION)),TRUE)
    COM_DEFS += $(COM_DEFS_FOR_$(strip $($(strip $(SIMULATION_PLATFORM))_BOARD_NAME)))
    BOARD_TYPE = BOARD_$(strip $($(strip $(SIMULATION_PLATFORM))_BOARD_NAME))
    SYS_BOARD = $(SB_$(strip $($(strip $(SIMULATION_PLATFORM))_BOARD_NAME)))
    COM_DEFS += BOARD_$(strip $($(strip $(SIMULATION_PLATFORM))_BOARD_NAME))
  else
    BOARD_TYPE = BOARD_$(strip $(SUB_BOARD_VER))
    SYS_BOARD = $(SB_$(strip $(SUB_BOARD_VER)))
    COM_DEFS += BOARD_$(strip $(SUB_BOARD_VER))
  endif
else
  $(warning ERROR: [COM_DEFS_FOR_$(strip $(SUB_BOARD_VER))] was not defined)
  DEPENDENCY_CONFLICT = TRUE
endif

SYS_DSPM_PATCH = $(SP_ALL)
SYS_DSPV_PATCH = $(SP_ALL)

# RF Module Options
COM_DEFS_FOR_MTK_ORIONC                  = MTK_PLT_RF_ORIONC     #for Denlai
COM_DEFS_FOR_MT6176                      = MTK_PLT_RF_MT6176 MTK_PLT_RF_ORION_PLUS     #for Jade

COM_DEFS_FOR_RF_MODULE =  COM_DEFS_FOR_$(strip $(RF_MODULE))
ifdef $(COM_DEFS_FOR_RF_MODULE)
  COM_DEFS +=  $(COM_DEFS_FOR_$(strip $(RF_MODULE)))
  ifeq ($(strip $(SIMULATION)),TRUE)
    COM_DEFS += $(COM_DEFS_FOR_$(strip $(SIMULATION_RF_MODULE)))
    SYS_OPTION_RF_HW = $(SYS_RF_$(strip $(SIMULATION_RF_MODULE)))
  else
    SYS_OPTION_RF_HW = $(SYS_RF_$(strip $(RF_MODULE)))
  endif
else
  $(warning ERROR: [COM_DEFS_FOR_$(strip $(RF_MODULE))] was not defined)
  DEPENDENCY_CONFLICT = TRUE
endif

ifeq ($(strip $(USE_DIVERSITY_RF)),TRUE)
  COM_DEFS += SYS_OPTION_DIVERSITY_RF
endif

SYS_CUST_PLT = $(SYS_PLT_$(strip $(USE_CUST_PLT)))

ifeq ($(strip $(ETM_ENABLE)),TRUE)
  COM_DEFS += IS_C2K_ET_FEATURE_SUPPORT
endif


ifeq ($(strip $(BPI_VM_CTRL_ENABLE)),TRUE)
  COM_DEFS += BPI_NUM_LARGE_32
endif
#
# Default IMD compiler flag
#
#SYS_OPTION_IMD_MODULE    = $(SYS_IMD_MODULE_NOTUSED)
SYS_OPTION_IMD_MODULE     = $(SYS_IMD_MODULE_ISUSED)
SYS_OPTION_IMD_GAIN_TABLE = $(SYS_IMD_GAIN_TABLE_NOSWITCH)

#
# Setup IMD compiler flag if IMD module is not used
#
ifeq ($(strip $(USE_IMD_MODULE)),FALSE)
  SYS_OPTION_IMD_MODULE     = $(SYS_IMD_MODULE_NOTUSED)
endif

#
# Setup IMD compiler flag if IMD module turns gain table
#
ifeq ($(strip $(TURN_IMD_TABLE)),TRUE)
  SYS_OPTION_IMD_GAIN_TABLE = $(SYS_IMD_GAIN_TABLE_SWITCH)
endif

#
# Default FMP MMSE MRC Switch flag
#
SYS_OPTION_FMP_MMSE_MRC   = $(SYS_FMP_MMSE_MRC_SWITCH)

#
# Setup FMP MMSE MRC compiler flag
#
ifeq ($(strip $(USE_MMSE_MRC_MODULE)),FALSE)
  SYS_OPTION_FMP_MMSE_MRC   = $(SYS_FMP_MMSE_MRC_NOSWITCH)
endif

#
# Default RPC High Threshold Flag
#
SYS_OPTION_RPC_THRESHOLD  = $(SYS_RPC_HIGH_THRESHOLD_NOTUSED)

#
# Setup RPC High Threshold Flag
#
ifeq ($(strip $(USE_RPC_FIX)),TRUE)
  SYS_OPTION_RPC_THRESHOLD  = $(SYS_RPC_HIGH_THRESHOLD_USED)
endif

#
# Default GPS RF compiler flags to SYS_GPS_RF_NONE
#
SYS_OPTION_GPS_RF_HW      = $(SYS_GPS_RF_NONE)

#
# Setup GPS RF compiler flags if GLONAV 1040 is used
#
ifeq ($(strip $(USE_GLONAV_1040_GPS_RF)),TRUE)
  SYS_OPTION_GPS_RF_HW      = $(SYS_GPS_RF_GLONAV_1040)
endif

#
# Default GPS path feature
#
SYS_OPTION_GPS_HW = $(SYS_GPS_DEFAULT)

#
# Default External GPS compiler flags to NONE
#
SYS_OPTION_EXTERNAL_GPS_HW = $(SYS_EXT_GPS_NONE)

ifeq ($(strip $(USE_GPS_TYPE)),GPS_TYPE_INTERNAL)
  SYS_OPTION_GPS_HW = $(SYS_GPS_LOCAL_INTERNAL)
endif

ifeq ($(strip $(USE_GPS_TYPE)),GPS_TYPE_EXTERNAL_ON_CP)
  COM_DEFS += SYS_OPTION_GPS_EXTERNAL
endif

ifeq ($(strip $(USE_GPS_TYPE)),GPS_TYPE_EXTERNAL_ON_AP)
  COM_DEFS += SYS_OPTION_GPS_EXTERNAL
  SYS_OPTION_EXTERNAL_GPS_HW = $(SYS_EXT_GPS_ON_AP)
endif

ifeq ($(strip $(USE_AP_CALL_GPS)),TRUE)
  COM_DEFS += SYS_OPTION_GPS_AP_CALL
endif

ifeq ($(strip $(USE_GPS_ON_AP_GPS_DEV0)),TRUE)
  SYS_OPTION_AP_GPS_HW = $(SYS_EXT_GPS_ON_AP_DEV0)
else
  SYS_OPTION_AP_GPS_HW =
endif

ifeq ($(strip $(CONFIG_AGPS)),TRUE)
  SYS_OPTION_AGPS_ENABLE = $(SYS_AGPS_ENABLE)
else
  SYS_OPTION_AGPS_ENABLE =
endif

SYS_OPTION_REFERENCE_FREQ = $(SYS_REFERENCE_FREQ_26_00)
SYS_OPTION_32K_CLK_SOURCE = $(SYS_OPTION_32K_CLK_INT_OSC)
SYS_OPTION_TCXO_CONFIG    = $(SYS_OPTION_TCXO_SINGLE)

#
# Default Audio path feature
#
SYS_OPTION_AUDIO_PATH_FEATURE = $(SYS_OPTION_AUDIO_PATH_FEATURE_DEFAULT)

#
# Audio patch feature
#
ifeq ($(strip $(AUDIO_PATH_FEATURE)),DEFAULT)
  SYS_OPTION_AUDIO_PATH_FEATURE = $(SYS_OPTION_AUDIO_PATH_FEATURE_DEFAULT)
else
  ifeq ($(strip $(AUDIO_PATH_FEATURE)),ONE)
    SYS_OPTION_AUDIO_PATH_FEATURE = $(SYS_OPTION_AUDIO_PATH_FEATURE_ONE)
  else
    ifeq ($(strip $(AUDIO_PATH_FEATURE)),TWO)
      SYS_OPTION_AUDIO_PATH_FEATURE = $(SYS_OPTION_AUDIO_PATH_FEATURE_TWO)
    endif
  endif
endif

ifeq ($(strip $(USE_USB)),FALSE)
  SYS_OPTION_USB = $(SYS_USB_NONE)
else
  SYS_OPTION_USB =
endif

# Default flashless IPC device flags to SYS_IPC_DEV_NONE
#
SYS_OPTION_IPC_DEV      = $(SYS_IPC_DEV_NONE)

#
# IPC feature for Flash-less support
#
ifeq ($(strip $(IPC_PATH_FEATURE)),USB)
  SYS_OPTION_IPC_DEV      = $(SYS_IPC_DEV_USB)
else
  ifeq ($(strip $(IPC_PATH_FEATURE)),DPRAM)
    SYS_OPTION_IPC_DEV      = $(SYS_IPC_DEV_DPRAM)
  else
    ifeq ($(strip $(IPC_PATH_FEATURE)),SDIO)
      SYS_OPTION_IPC_DEV      = $(SYS_IPC_DEV_SDIO)
    else
      ifeq ($(strip $(IPC_PATH_FEATURE)),SPI)
        SYS_OPTION_IPC_DEV      = $(SYS_IPC_DEV_SPI)
      else
        ifeq ($(strip $(IPC_PATH_FEATURE)),CCIF)
          SYS_OPTION_IPC_DEV      = $(SYS_IPC_DEV_CCIF)
        else
          SYS_OPTION_IPC_DEV      = $(SYS_IPC_DEV_NONE)
        endif
      endif
    endif
  endif
endif

SYS_OPTION_HDET_HW       = $(SYS_HDET_DISCRETE)
#
# LMV221 power detector
#
ifeq ($(strip $(HDET_HW)),LMV221)
  SYS_OPTION_HDET_HW       = $(SYS_HDET_LMV221)
endif

#
# ADL550 power detector
#
ifeq ($(strip $(HDET_HW)),ADL550)
  SYS_OPTION_HDET_HW       = $(SYS_HDET_ADL550)
endif

include make/c2k_core/makefile_predefined_macros.mak
COM_DEFS += $(foreach nm, $(SYS_DEFS),$(nm)=$($(nm)))

################################################################################
#
#      System Options definitions
#
################################################################################

COM_DEFS += SYS_TARGET_HW
COM_DEFS += SYS_INVERTER_UART1
COM_DEFS += SYS_OPTION_INTERNAL_PLL
COM_DEFS += SYS_OPTION_OSCEN_ACTIVE_LOW
COM_DEFS += SYS_OPTION_SMS_ENGINE
COM_DEFS += SYS_OPTION_OTASP
COM_DEFS += SYS_OPTION_RLP
COM_DEFS += SYS_OPTION_HL

ifndef FIX_AFC_OFF
  FIX_AFC_OFF = TRUE         # Default Fix AFC Off
endif

# SYS_OPTION_TX_TAS_ENABLE :Select the better antenna for TX , for JADE platform
# MTK_DEV_C2K_TAS:Select the better antenna for c2k voice call ,  for Denali paltform
ifeq ($(strip $(TX_TAS_ENABLE)),TRUE)
  COM_DEFS += SYS_OPTION_TX_TAS_ENABLE
else
  COM_DEFS += MTK_DEV_C2K_TAS
endif

ifeq ($(strip $(TAS_OFF_WHEN_NO_SIM)), TRUE)
  COM_DEFS += __TAS_OFF_WHEN_NO_SIM__
endif

ifeq ($(strip $(TAS_ANTENNA_IDX_ON_TEST_SIM)), TRUE)
  COM_DEFS += __TAS_ANTENNA_IDX_ON_TEST_SIM__
endif

ifeq ($(strip $(FIX_AFC_OFF)),TRUE)
  COM_DEFS += MTK_DEV_FIX_AFC_OFF
endif

ifeq ($(strip $(GET_META_MODE_OFF)),TRUE)
  COM_DEFS += MTK_META_MODE_OFF
endif

ifndef MMAFC_SHARED
  MMAFC_SHARED = FALSE         # Default MMAFC Off
endif

ifeq ($(strip $(MMAFC_SHARED)),TRUE)
  COM_DEFS += MTK_DEV_SHARED_AFC_TEST
endif

ifndef AGPS_SYNC
  AGPS_SYNC = TRUE         # Default AGPS On
endif

ifeq ($(strip $(AGPS_SYNC)),TRUE)
  COM_DEFS += MTK_GPS_SYNC_DEV
endif

ifneq "$(USE_DMU)" "FALSE"
  ifneq "$(CARRIER)" "SPRINT"
    COM_DEFS += SYS_OPTION_MIP_DMU
  endif
  COM_DEFS += SYS_OPTION_MIP
else
  ifneq ($(findstring VERIZON_EXTENSIONS,$(SO_EXTRA)),)
    COM_DEFS += SYS_OPTION_MIP
    COM_DEFS += SYS_OPTION_MIP_DMU
  else
    ifneq "$(USE_MOIP)" "FALSE"
      COM_DEFS +=  SYS_OPTION_MIP
    endif
  endif
endif

ifeq "$(USE_THROTTLING)" "TRUE"
  COM_DEFS += SYS_OPTION_HSPD_THROTTLING
else
  ifneq ($(findstring VERIZON_EXTENSIONS,$(SO_EXTRA)),)
    COM_DEFS += SYS_OPTION_HSPD_THROTTLING
  endif
endif

ifeq "$(USE_FALLBACK)" "TRUE"
  COM_DEFS += SYS_OPTION_HSPD_FALLBACK
else
  ifneq ($(findstring VERIZON_EXTENSIONS,$(SO_EXTRA)),)
    COM_DEFS += SYS_OPTION_HSPD_FALLBACK
  endif
endif

ifeq "$(USE_IRAT_MMC)" "TRUE"
  COM_DEFS += SYS_OPTION_IRAT_MMC
endif

COM_DEFS += SYS_OPTION_DATA_RAM_INUSE

ifneq "$(NO_SCH_RAM)" "TRUE"
  COM_DEFS += SYS_OPTION_SCH_RAM_INUSE
endif

COM_DEFS += SYS_OPTION_NTWKRMPKT_RAM_INUSE

ifneq "$(NO_AUTH)" "TRUE"
  COM_DEFS += SYS_OPTION_AUTHENTICATION
endif

COM_DEFS += SYS_OPTION_DUAL_NAM

SO_UIM = SYS_OPTION_RUIM
ifeq "$(USE_RUIM)" "TRUE"
  ifeq "$(USE_REMOTE_UICC)" "TRUE"
    SO_UIM += SYS_OPTION_REMOTE_UICC
  endif
  ifeq "$(USE_LOCAL_UICC)" "TRUE"
    SO_UIM += SYS_OPTION_LOCAL_UICC
  endif
  ifeq "$(USE_UTK)" "TRUE"
    SO_UIM += FEATURE_UTK FEATURE_CALL_CONTROL FEATURE_BIP FEATURE_EVENT_DOWNLOAD
  endif
  ifeq "$(USE_BIP)" "TRUE"
    SO_UIM += FEATURE_BIP
  endif

  COM_DEFS += $(SO_UIM)
endif

ifeq "$(USE_ENHANCEDAT)" "TRUE"
  COM_DEFS += SYS_OPTION_ENHANCEDAT
endif

ifeq "$(USE_RPC)" "TRUE"
  ifeq "$(USE_GPS_RPC)" "TRUE"
    COM_DEFS += SYS_OPTION_RPC
    COM_DEFS += SYS_OPTION_GPS_RPC
  endif
  ifeq "$(USE_IRAT_RPC)" "TRUE"
    COM_DEFS += SYS_OPTION_RPC
    COM_DEFS += SYS_OPTION_IRAT_RPC
  endif
endif

ifneq "$(NO_GPS)" "TRUE"
  COM_DEFS += SYS_OPTION_GPS_SIMULTANEOUS
endif

COM_DEFS += SYS_OPTION_DIGITAL_RX_AGC

ifeq "$(USE_PA2_CTRL)" "TRUE"
  COM_DEFS += SYS_OPTION_PA2_CTRL
endif
#
# Setup Default Rx Hysterisis compiler option is used
#
ifneq "$(USE_DEFAULT_RX_HYSTERESIS)" "FALSE"
  COM_DEFS += DEFAULT_RX_HYSTERESIS
endif
ifeq "$(USE_SIDB)" "TRUE"
  COM_DEFS +=  SYS_OPTION_FSM_SIDB
endif

ifeq "$(USE_PC_VOICE)" "TRUE"
  COM_DEFS += SYS_OPTION_PC_VOICE
endif

ifeq "$(USE_ATCMD_CH)" "TRUE"
    COM_DEFS += SYS_OPTION_ATCMD_CH SYS_OPTION_ATCMD_CH_2 SYS_OPTION_ATCMD_CH_3 SYS_OPTION_ATCMD_CH_4
endif

COM_DEFS += SYS_OPTION_ERI

#
#Carrier features option
#
ifeq "$(CARRIER)" "CT"
  COM_DEFS += CHINATELECOM_EXTENSIONS
endif

ifeq "$(CARRIER)" "VZW"
  COM_DEFS += VERIZON_EXTENSIONS
endif

ifeq "$(CARRIER)" "SPRINT"
  COM_DEFS += SPRINT_EXTENSIONS
endif

ifeq "$(CARRIER)" "RELIANCE"
  COM_DEFS += FEATURE_RELIANCE
endif

ifeq "$(CARRIER)" "KDDI"
  COM_DEFS += KDDI_EXTENSIONS
endif

ifeq "$(CARRIER)" "SMARTFREN"
  COM_DEFS += SMARTFREN_EXTENSIONS
endif

#
# IPV6 option
#
SO_CT = CHINATELECOM_EXTENSIONS
ifeq "$(SO_CT)" "CHINATELECOM_EXTENSIONS"
USE_IPV6 = TRUE
endif

ifeq "$(USE_EHRPD)" "TRUE"
USE_IPV6 = TRUE
endif

#
#EHRPD IMS option
#
ifeq "$(USE_EHRPD)" "TRUE"
  COM_DEFS += CBP7_EHRPD
endif

ifeq "$(USE_IPV6)" "TRUE"
  COM_DEFS += CBP7_IPV6
endif

ifeq "$(USE_NO_IPV6)" "TRUE"
  COM_DEFS += SYS_OPTION_NO_IPV6_STACK
endif

#
# Setup the compiler/assembler option SYS_OPTION_EVD0 (for 7.0x targets)
# based on the USE_1X_ONLY command line option
#
ifeq "$(USE_1X_ONLY)" "TRUE"
else
  COM_DEFS += SYS_OPTION_EVDO
endif

#
#Include AP Interface option
#
ifeq "$(USE_AP_INTERFACE)" "TRUE"
  COM_DEFS += SYS_OPTION_AP_INTERFACE
endif

COM_DEFS += SYS_OPTION_EXT_AUDIO_CODEC

ifeq "$(XCO_TYPE)" "DCXO"
  COM_DEFS += SYS_OPTION_USE_DCXO
endif

ifeq "$(NO_PPPHA_ENC_ENH)" "TRUE"
else
  COM_DEFS += SYS_OPTION_PPPHA_ENC_ENH
endif

# PACKAGE_DSP_IMG OPTION
#
ifeq "$(PACKAGE_DSP_IMG)" "TRUE"
  COM_DEFS += SYS_OPTION_PACKAGE_DSP_IMG
endif

ifneq "$(CARRIER)" "OM"
  COM_DEFS += FEATURE_CHINATELECOM
endif

COM_DEFS += SYS_OPTION_AUX_ADC_POLL

#
# Enable watchdog by default
#
ifeq "$(USE_WATCHDOG)" "TRUE"
  COM_DEFS += SYS_OPTION_HWD_WATCHDOG_ENABLE
endif

SO_SDIO_SLAVE = SYS_OPTION_SDIO_SLAVE SYS_OPTION_ENABLE_SDIO_SLAVE_DMA SYS_OPTION_SDIO_SLAVE_BOOT_ENABLE

ifeq "$(USE_MTK_SDIO_SLAVE)" "TRUE"
  COM_DEFS += $(SO_SDIO_SLAVE)
endif

SO_IOP_CCIF = SYS_OPTION_IOP_CCIF  SYS_OPTION_ENABLE_CCIF_DMA #SYS_OPTION_CCIF_LOOPTEST
ifeq "$(USE_IOP_CCIF)" "TRUE"
  COM_DEFS += $(SO_IOP_CCIF)
  COM_DEFS += MTK_DEV_CCCI_FS
endif
COM_DEFS += SYS_OPTION_WAIT_FOR_BOOT_TO_LOADER
COM_DEFS += SYS_OPTION_HWD_PROFILE
COM_DEFS += HWD_DBG_TXDAC_DIRECT

ifeq "$(USE_APT)" "TRUE"
  COM_DEFS += HWD_FEATURE_PA_SPDM_CTRL
endif

ifeq "$(USE_32K_CLK_BYPASS)" "TRUE"
  COM_DEFS += SYS_OPTION_32K_BYPASS
endif

ifeq "$(USE_PSNOTOPEN)" "TRUE"
  COM_DEFS += FEATURE_PS_NOT_TURNON_AUTO
endif

ifneq "$(CARRIER)" "OM"
  ifeq "$(USE_CARDLOCK)" "TRUE"
    COM_DEFS += CHINATELECOM_CARDLOCK
  endif
endif

ifeq "$(USE_GSM_PROTECTION)" "TRUE"
  COM_DEFS += SYS_OPTION_GSM_PROTECTION
endif

ifeq "$(USE_CCIRQ_PATH)" "PCCIF"
  COM_DEFS += SYS_OPTION_CCIRQ_PCCIF
endif

ifeq "$(USE_CCIRQ_PATH)" "CCIRQ"
  ifeq ($(strip $(SIMULATION)),TRUE)
    COM_DEFS += SYS_OPTION_CCIRQ_PCCIF
  else
    COM_DEFS += SYS_OPTION_CCIRQ_CCIRQ
#    COM_DEFS += SYS_OPTION_CCIRQ_UT_ON_D1
#    COM_DEFS += SYS_OPTION_CCIRQ_UT_ON_JADE
  endif
endif

COM_DEFS += $(SO_EXTRA)

COM_DEFS += SYS_DEBUG_FAULT_FILE_INFO
ifeq "$(FLASH_LESS_EN)" "TRUE"
  COM_DEFS += SYS_FLASH_LESS_SUPPORT
endif
ifeq "$(RAM_DUMP_2_AP)" "TRUE"
  COM_DEFS += SYS_RAM_DUMP_2_AP_SUPPORT
endif
ifeq "$(USE_UIM_POWER_SWITCH)" "TRUE"
  COM_DEFS += SYS_FEATURE_UIM_POWER_SWTICH
endif

ifeq "$(USE_CCISM)" "TRUE"
  COM_DEFS += SYS_OPTION_CCISM_2SCP
endif

ifeq ($(strip $(TX_SWTP_ENABLE)),TRUE)
  COM_DEFS += __TX_POWER_OFFSET_SUPPORT__
endif


################ UI Feature: ################
ifeq "$(CONFIG_UI)" "NONE"
  COM_DEFS += SYS_OPTION_NO_UI
  COM_DEFS += FEATURE_LCD_SIZE_320x240
endif

ifeq "$(SUPPORT_RTRE)" "TRUE"
  COM_DEFS += FEATURE_RTRE
endif

ifeq "$(SUPPORT_UTK)" "TRUE"
  COM_DEFS += FEATURE_UTK
endif

ifeq "$(SUPPORT_SMS_AUTO_REGISTER)" "TRUE"
  ifeq "$(USE_RUIM)" "TRUE"
    COM_DEFS += FEATURE_SMS_AUTO_REGISTER
  endif
endif

ifeq "$(SUPPORT_ESN_TRACK)" "TRUE"
  ifeq "$(USE_RUIM)" "TRUE"
    COM_DEFS += FEATURE_ESN_TRACK
  endif
endif

ifeq "$(SUPPORT_ESN_TRACK_TATA)" "TRUE"
  ifeq "$(USE_RUIM)" "TRUE"
    COM_DEFS += FEATURE_ESN_TRACK_TATA
  endif
endif

ifeq "$(SUPPORT_LSM)" "TRUE"
  COM_DEFS += FEATURE_LSM
endif

ifeq "$(SUPPORT_SMARTMSG)" "TRUE"
  ifeq "$(SUPPORT_VCARD)" "TRUE"
    COM_DEFS += FEATURE_SMARTMSG
  else
    ifeq "$(SUPPORT_VCALENDAR)" "TRUE"
      COM_DEFS += FEATURE_SMARTMSG
    endif
  endif
endif

ifeq "$(SUPPORT_SMS_CHAT)" "TRUE"
  COM_DEFS += FEATURE_SMS_CHAT
endif

ifeq "$(SUPPORT_VOICE_MAIL)" "TRUE"
  COM_DEFS += FEATURE_VOICE_MAIL
endif

ifeq "$(SUPPORT_UNITTEST)" "TRUE"
  COM_DEFS += FEATURE_UNITTEST
endif

ifeq "$(SUPPORT_MMS)" "TRUE"
  COM_DEFS += FEATURE_MMS
endif
ifeq "$(SUPPORT_WAP_TYPE)" "VAAWEB"
  COM_DEFS += FEATURE_VAAWEB
endif
ifeq "$(SUPPORT_QCLX)" "TRUE"
  COM_DEFS += FEATURE_APP_QCLX
endif
ifeq "$(SUPPORT_WAP_MMS_TYPE)" "JUPITOR"
  COM_DEFS += FEATURE_JUPITOR
endif
ifeq "$(SUPPORT_MMS_TYPE)" "VAAMMS"
  COM_DEFS += FEATURE_VAAMMS
endif
ifeq "$(SUPPORT_VOICEMEMO)" "TRUE"
  COM_DEFS += FEATURE_VOICEMEMO
endif

################ Driver Feature: ################
ifeq "$(RTC_TYPE)" "RTC_NONE"
  COM_DEFS += RTC_NONE
endif

ifeq "$(SUPPORT_DUMP_FAULT_LOG)" "TRUE"
  COM_DEFS += SYS_EXE_FAULT_DATA_DUMP
endif

ifeq "$(SUPPORT_GFX)" "TRUE"
  COM_DEFS += FEATURE_GFX_2D_BITBILT
endif

################ PSO Feature: ################
ifeq ($(strip $(DISABLE_SLEEP)),FALSE)
  ifeq ($(strip $(PSO_MODE_SUPPORT)),TRUE)
    COM_DEFS += SYS_OPTION_PSO_ENABLED
    ifeq ($(strip $(PSO_MODE_WORKAROUND)),TRUE)
      COM_DEFS += MTK_DEV_PSO_WORKAROUND
      #COM_DEFS += MTK_DEV_DSPM_PSO_WORKAROUND
    endif
  endif
endif

################################################################################
#
#      MTK Main Definitions
#
################################################################################

#USE_MTK_CBP
# Description:
#   Enable MTK changes
# Option Values:
#   DEVELOPMENT:  Enable MTK formal and informal changes
#   RELEASE:      Enable MTK formal changes only
#


#**************** FOR RELEASE VERSION ****************************
ifeq ($(strip $(USE_MTK_CBP)),RELEASE)
  COM_DEFS += MTK_CBP
  COM_DEFS += MTK_MODEL_STRING=\\\\\\\"$(strip $(PLATFORM))\\\\\\\"
  ifeq ($(strip $(CUSTOM_RELEASE)),TRUE)
    ifneq ($(strip $(CUSTOM_VERNO)),)
      COM_DEFS += MTK_VERNO_STRING=\\\\\\\"$(strip $(CUSTOM_VERNO))\\\\\\\"
    else
      COM_DEFS += MTK_VERNO_STRING=\\\\\\\"$(strip $(VERNO))\\\\\\\"
    endif
  else
    COM_DEFS += MTK_VERNO_STRING=\\\\\\\"$(strip $(VERNO))\\\\\\\"
  endif
endif

#**************** FOR DEVELOPMENT VERSION *************************

ifeq ($(strip $(USE_MTK_CBP)),DEVELOPMENT)
   COM_DEFS += MTK_CBP
   COM_DEFS += MTK_DEV_BUG_FIX_OTA
   COM_DEFS += MTK_DEV_BUG_FIX_XL2
   COM_DEFS += MTK_DEV_BUG_FIX_PE
   COM_DEFS += MTK_DEV_BUG_FIX_VAL
   COM_DEFS += MTK_DEV_BUG_FIX_HLP
   COM_DEFS += MTK_DEV_BUG_FIX_GPS
   COM_DEFS += MTK_DEV_BUG_FIX_SMS
   COM_DEFS += MTK_DEV_BUG_FIX_EVL1
   COM_DEFS += MTK_DEV_OPTIMIZE_EVL1
   COM_DEFS += MTK_DEV_HLP_OPTIMIZE_CP
   COM_DEFS += MTK_DEV_HLP_OPTIMIZE_UP
   COM_DEFS += MTK_DEV_TRACE_ENHANCEMENT
   COM_DEFS += MTK_DEV_BUG_FIX_EVL2
   COM_DEFS += MTK_DEV_OPTIMIZE_EVL2
   COM_DEFS += MTK_DEV_OPTIMIZE_XL2
   COM_DEFS += MTK_DEV_BUG_FIX_SLC
   COM_DEFS += MTK_DEV_BUG_FIX_CLC
   COM_DEFS += MTK_DEV_BUG_FIX_SS
   COM_DEFS += MTK_DEV_ETS_ENHANCEMENT
   COM_DEFS += MTK_DEV_OPTIMIZE_PE
   COM_DEFS += MTK_DEV_BUG_FIX_RUP
   COM_DEFS += MTK_DEV_OPTIMIZE_RUP
   COM_DEFS += MTK_DEV_BUG_FIX_CSS
   COM_DEFS += MTK_DEV_FIX_KW
   COM_DEFS += MTK_DEV_FIX_BUILDWARN
   COM_DEFS += MTK_DEV_BUG_FIX_EHRPD
   COM_DEFS += MTK_DEV_BUG_FIX_UIM
   COM_DEFS += MTK_DEV_ENGINEER_MODE
   COM_DEFS += MTK_DEV_FACTORY_MODE
   COM_DEFS += MTK_DEV_HSC_SHDR
   COM_DEFS += MTK_DEV_META_LOGGING
   COM_DEFS += MTK_DEV_GPSONE_ON_LTE
   #COM_DEFS += MTK_DEV_BRING_UP_DISABLE_SLOTTED_DO
   #COM_DEFS += MTK_DEV_DUMP_REG

   ##Used for workarounds in codes. disabled after all issue resolved.
   COM_DEFS += MTK_DEV_WORKAROUND

ifeq ($(strip $(PMIC)),MT6353)
   COM_DEFS += __INCREASE_VS1_VOLTAGE_SUPPORT__
endif

#SSDVT doesn't need FSM
ifneq ($(strip $(USE_MTK_SYS)),SSDVT)
   COM_DEFS += MTK_DEV_VERSION_CONTROL
endif
ifneq ($(filter MTK_DEV_VERSION_CONTROL, $(COM_DEFS)), )
   COM_DEFS += MTK_DEV_RF_CUSTOMIZE
endif

ifeq ($(strip $(USE_OPTIMIZE_LOG)),TRUE)
   COM_DEFS += MTK_DEV_OPTIMIZE_LOGGING
   ifeq ($(strip $(SUB_BOARD_VER)),DENALI)
      COM_DEFS += MTK_DEV_STARTUP_LOG
   else
      COM_DEFS += MTK_DEV_LOG_FILTER_NVRAM
   endif
endif

#For Nvram Checksum LID list
ifeq ($(strip $(USE_NVRAM_CHECKSUM_LID_LIST)),TRUE)
      COM_DEFS += MTK_NVRAM_CHECKSUM_LID_LIST
endif

#For Skipping some LID when auto-reset nv by SW changed
ifeq ($(strip $(USE_SKIP_AUTORST_LID)),TRUE)
      COM_DEFS += MTK_SKIP_AUTORST_LID
endif

ifeq ($(strip $(USE_SECURITY_LOG)),TRUE)
      COM_DEFS += MTK_DEV_SECURITY_LOG
endif

ifeq ($(strip $(USE_LOG_BUFFER_SIZE_96K)),TRUE)
   COM_DEFS += MTK_DEV_LOG_BUFFER_96K
endif

ifeq ($(strip $(USE_LOG_SPLM)),TRUE)
   COM_DEFS += MTK_DEV_LOG_SPLM
   COM_DEFS += MTK_DEV_LOG_PLS
endif

ifeq ($(strip $(USE_IRAT)),TRUE)
   COM_DEFS += MTK_DEV_C2K_IRAT
   ifeq ($(strip $(PLATFORM)),MT6735)
     COM_DEFS += MTK_DEV_TX_POWER_RPT_SUPPORT
   endif

   ifeq ($(strip $(USE_SRLTE)),TRUE)
     ifeq ($(strip $(USE_SRLTE_PS_IT)),TRUE)
       COM_DEFS += MTK_DEV_C2K_SRLTE
     endif
     ifeq ($(strip $(USE_RTB_CHECK)),TRUE)
       COM_DEFS += MTK_DEV_C2K_SRLTE_RTB_CHECK
     endif
     COM_DEFS += MTK_DEV_C2K_SRLTE_L1
     COM_DEFS += MTK_DEV_C2K_SRLTE_BASE
     COM_DEFS += MTK_CBP_SYNC_OPTIMIZE
     COM_DEFS += MTK_CBP_PCH_DIVERSITY_ENABLE

     ifeq ($(strip $(PLATFORM)),MT6735)
       COM_DEFS += MTK_DEV_DENALI_SRLTE_PRE_IT
     endif
   endif
   ##Enable the Function for Modem send TX Power to AP;Only enable MT6755 and follow version
   COM_DEFS += MTK_DEV_TX_RPT_AP
endif

ifeq ($(strip $(USE_ENCRYPT_VOICE)),TRUE)
   COM_DEFS += MTK_CBP_ENCRYPT_VOICE
endif

ifeq ($(strip $(USE_MTK_SDIO_SLAVE)),TRUE)
   COM_DEFS += MTK_DEV_USE_NEW_SDIO
endif


ifeq "$(USE_MEMORY_OPT)" "TRUE"
 COM_DEFS += MTK_DEV_MEMORY_OPT
endif

ifeq ($(strip $(SLT)),TRUE)
   COM_DEFS += MTK_DEV_SLT
else
   ifneq ($(strip $(ALL_IN_IRAM)),TRUE)
   COM_DEFS += MTK_DEV_SHARED_DBG
   endif
  ifeq ($(strip $(USE_IOP_CCIF)),TRUE)
    COM_DEFS += MTK_DEV_SHARED_AP_PROVIDE
  endif
endif

ifeq ($(strip $(SLT_TX)),TRUE)
   COM_DEFS += MTK_DEV_SLT_TX
endif

ifeq ($(strip $(SLT_LOGON)),TRUE)
   COM_DEFS += MTK_DEV_SLT_LOGON
endif

ifeq ($(strip $(USE_OPTIMIZE_EXCEPTION)),TRUE)
   COM_DEFS += MTK_DEV_OPTIMIZE_EXCEPTION
endif

ifeq ($(strip $(USE_EE_PRODUCTION_MODE)),TRUE)
   COM_DEFS += MTK_DEV_DISABLE_EE_DUMP
endif

#
# DISABLE_SLEEP option
#
ifeq ($(strip $(DISABLE_SLEEP)),TRUE)
  COM_DEFS += SYS_OPTION_DISABLE_SLEEP
endif

   COM_DEFS += MTK_MODEL_STRING=\\\\\\\"$(strip $(PLATFORM))\\\\\\\"
ifeq ($(strip $(CUSTOM_RELEASE)),TRUE)
  ifneq ($(strip $(CUSTOM_VERNO)),)
    COM_DEFS += MTK_VERNO_STRING=\\\\\\\"$(strip $(CUSTOM_VERNO))\\\\\\\"
  else
    COM_DEFS += MTK_VERNO_STRING=\\\\\\\"$(strip $(VERNO))\\\\\\\"
  endif
else
   COM_DEFS += MTK_VERNO_STRING=\\\\\\\"$(strip $(VERNO))\\\\\\\"
endif
ifneq ($(strip $(CUSTOM_HW_VERSION)),)
	COM_DEFS += CUSTOM_HW_VERSION=\"$(CUSTOM_HW_VERSION)\"
endif
ifneq ($(strip $(CUSTOM_SW_VERSION)),)
	COM_DEFS += CUSTOM_SW_VERSION=\"$(CUSTOM_SW_VERSION)\"
endif
endif

ifeq ($(strip $(EN_32KLESS)),TRUE)
   COM_DEFS += MTK_DEV_32K_LESS
endif

ifdef OTP_SUPPORT
  ifeq ($(strip $(OTP_SUPPORT)),TRUE)
    COM_DEFS    += __NVRAM_OTP__
  endif
endif

ifeq ($(strip $(USE_MULTICHANNEL)),TRUE)
   COM_DEFS += SYS_OPTION_MORE_AT_CHANNEL
endif

ifeq ($(strip $(DISABLE_VOICE_CALL)),TRUE)
   COM_DEFS += MTK_DEV_C2K_DISABLE_VOICE_CALL
endif

#Patch For Customer Crystal usage 
ifeq ($(strip $(CRSTAL_USAGE_SPECIAL)),TRUE)
      COM_DEFS += MT6355_NEW_CRSTALSELECT_METHOD
endif

#Google Carrier Restriction feature
COM_DEFS += __CARRIER_RESTRICTION__

#**********get version value from COM_DEFS******************
NV_VERNO_STRING=$(filter MTK_VERNO_STRING=%,$(COM_DEFS))
#remove '\' in string
NV_VERNO_STRING:=$(shell echo $(shell echo $(shell echo $(shell echo $(NV_VERNO_STRING)))))
NV_VERNO_STRING:=$(patsubst MTK_VERNO_STRING=%, %, $(NV_VERNO_STRING))
#**********************************************************

ifeq ($(strip $(MTK_AUDIO_SUPPORT)), TRUE)
  COM_DEFS += MTK_PLT_AUDIO
  ifdef MTK_BT_CHIP
    BLUETOOTH_SCO_SW_CODEC_BT_CHIP = MTK_CONSYS_MT6735 MTK_CONSYS_MT6755 MTK_CONSYS_MT6797 MTK_CONSYS_MT6750 MTK_CONSYS_MT6757
    ifneq ($(filter $(strip $(MTK_BT_CHIP)),$(BLUETOOTH_SCO_SW_CODEC_BT_CHIP)),)
      COM_DEFS += __BT_SCO_CODEC_SUPPORT__
      COM_DEFS += __CVSD_CODEC_SUPPORT__
    endif
  endif
  ifdef OPEN_DSP_SPEECH_SUPPORT
    ifeq ($(strip $(OPEN_DSP_SPEECH_SUPPORT)),TRUE)
      COM_DEFS += __OPEN_DSP_SPEECH_SUPPORT__
    endif
  endif
  ifdef MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT
    ifeq ($(strip $(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)),TRUE)
      COM_DEFS += MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT
    endif
  endif
endif

#******************* FOR FWE speech***************************
ifdef SPEECH_WW_OPERATOR_SCORE_ENHANCE
  ifeq ($(strip $(SPEECH_WW_OPERATOR_SCORE_ENHANCE)),TRUE)
    COM_DEFS += __SPEECH_WW_OPERATOR_SCORE_ENHANCE__
  endif
endif


#******************* FOR SSDVT SYSTEM ***************************
ifeq ($(strip $(USE_MTK_SYS)),SSDVT)
  COM_DEFS += MTK_DEV_SSDVT

  ifeq ($(strip $(ALL_IN_IRAM)),TRUE)
    COM_DEFS += MTK_DEV_ALL_IN_IRAM
    COM_DEFS += MTK_DEV_BRING_UP
  else
    COM_DEFS += MTK_DEV_SSDVT_USE_FIQ
  endif
endif

#******************* FOR MDONLY SYSTEM ***************************
ifeq ($(strip $(USE_MTK_SYS)),MDONLY_SYSTEM)
  #COM_DEFS    += SYS_DEBUG_UART_PRINTF
  #COM_DEFS    += MTK_DEV_DISABLE_ETS_LOG
  COM_DEFS += MTK_PLT_MODEM_ONLY
  ifneq ($(strip $(SUB_BOARD_VER)),DENALI)
    COM_DEFS += MTK_DEV_CCCI_FS
  endif
endif

#******************* FOR FULL SYSTEM ***************************
ifeq ($(strip $(USE_MTK_SYS)),FULL_SYSTEM)
  COM_DEFS += MTK_PLT_FULL_LOAD
  ifneq ($(strip $(SUB_BOARD_VER)),DENALI)
    COM_DEFS += MTK_DEV_EXCEPTION_CTI
  endif
endif

#******************* FOR BOARD TYPE ***************************
ifeq ($(strip $(SUB_BOARD_VER)),DENALI)
  COM_DEFS += MTK_PLT_DENALI
else
  COM_DEFS += MTK_DEV_NOT_SUPPORT_DDPC_IMMEDTRRIGER_WORKAROUND
  #COM_DEFS += MTK_DEV_SHARED_AFC_TEST
  #COM_DEFS += MTK_DEV_EVDO_SLOTTED_AFC_OPT
  COM_DEFS += MTK_DEV_ENABLE_DSP_DUMP
endif

ifeq ($(strip $(USE_EMI_PROFILING)),TRUE)
  COM_DEFS += MTK_EMI_PROFILING
endif

ifeq ($(strip $(SAR_TX_POWER_BACKOFF_SUPPORT)),TRUE)
  COM_DEFS += __SAR_TX_POWER_BACKOFF_SUPPORT__
endif

ifeq ($(strip $(C2K_HPUE_SUPPORT)),TRUE)
  COM_DEFS += SYS_OPTION_HPUE_ENABLED
endif

ifeq ($(strip $(USE_LOG_STATISTICS_PROFILE)),TRUE)
  COM_DEFS += LOG_STATISTICS_PROFILE
endif
ifeq ($(strip $(USE_LOG_TRACE_PROFILE)),TRUE)
  COM_DEFS += LOG_TRACE_PROFILE
endif
COM_DEFS += MTK_DEV_GCC_WARN_FIX

#COM_DEFS += SYS_DEBUG_UART_PRINTF

ifeq ($(strip $(USE_FORCE_EXCEPTION_UART)),TRUE)
  COM_DEFS += MTK_DEV_FORCE_EXCEPTION_UART
endif

VIA_DEFS := $(foreach nm, $(COM_DEFS),-D$(nm))

# Generate CUS_REL_SRC_COMP/REL_MTK_COMP
CUS_REL_SRC_COMP_TEMP := $(filter $(REL_SRC_COMP),$(COMPLIST))
CUS_REL_SRC_COMP += $(CUS_REL_SRC_COMP_TEMP)

ifeq ($(strip $(USE_MTK_SYS)),SSDVT)
  ifeq ($(filter ssdvt,$(REL_SRC_COMP)),)
    CUS_REL_SRC_COMP += ssdvt
  endif
endif

ifeq ($(strip $(NEED_BUILD_BOOTLOADER)),TRUE)
  ifneq ($(filter boot,$(REL_SRC_COMP)),)
    CUS_REL_SRC_COMP += boot
  endif
endif

CUS_REL_MTK_COMP_TEMP := $(filter $(REL_MTK_COMP),$(COMPLIST))
CUS_REL_MTK_COMP += $(CUS_REL_MTK_COMP_TEMP)


# Remove the REMOVE_CUSTOM_OPTION specified in project makefile from COM_DEFS
COM_DEFS  := $(filter-out $(REMOVE_CUSTOM_OPTION),$(COM_DEFS))

CLEANROOM_COMP =

###########################################
#temp add for module list
###########################################

# consistency check for COMPLIST & CUS_REL_XXX_COMP
# check each comp in $(COMPLIST) if it has been put in $(CUS_REL_XXX_COMP)
COMPLIST_NO_REL_TYPE_COMP = $(filter-out $(CUS_REL_SRC_COMP) $(CUS_REL_PAR_SRC_COMP) $(CUS_REL_HDR_COMP) $(CUS_REL_MTK_COMP),$(COMPLIST))
COMPLIST_NO_REL_TYPE_COMP := $(filter-out $(CLEANROOM_COMP),$(COMPLIST_NO_REL_TYPE_COMP))
ifneq ($(words $(COMPLIST_NO_REL_TYPE_COMP)),0)
  $(warning ERROR: $(foreach comp,$(COMPLIST_NO_REL_TYPE_COMP),"$(comp)") in COMPLIST but NOT in CUS_REL_XXX_COMP)
  DEPENDENCY_CONFLICT = TRUE
endif

# check each comp in $(CUS_REL_XXX_COMP) if it has been put in $(COMPLIST)
CREL_COMP_NOT_COMPLIST = $(filter-out $(COMPLIST),$(CUS_REL_SRC_COMP) $(CUS_REL_PAR_SRC_COMP) $(CUS_REL_HDR_COMP) $(CUS_REL_MTK_COMP))
CREL_COMP_NOT_COMPLIST := $(filter-out boot,$(CREL_COMP_NOT_COMPLIST))
ifneq ($(words $(CREL_COMP_NOT_COMPLIST)),0)
  $(warning ERROR: $(foreach comp,$(CREL_COMP_NOT_COMPLIST),"$(comp)") in CUS_REL_XXX_COMP but NOT in COMPLIST)
  DEPENDENCY_CONFLICT = TRUE
endif

# check no module define in $(CUS_REL_SRC_COMP) and $(CUS_REL_MTK_COMP) both
COMP_BOTH_IN_SRC_MTK = $(filter $(CUS_REL_SRC_COMP),$(CUS_REL_MTK_COMP))
ifneq ($(words $(COMP_BOTH_IN_SRC_MTK)),0)
  $(warning ERROR: $(foreach comp,$(COMP_BOTH_IN_SRC_MTK),"$(comp)") in CUS_REL_SRC_COMP but also in CUS_REL_MTK_COMP)
  DEPENDENCY_CONFLICT = TRUE
endif

# COMPLIST(during CUSTOM_RELEASE)      = CUS_REL_SRC_COMP + CUS_REL_PAR_SRC_COMP
COMPLIST =     $(strip $(CUS_REL_SRC_COMP))
COMPLIST := $(filter-out boot,$(COMPLIST))

# check the COM_DEFS value
COM_DEFS_INVALID_LIST := TRUE FALSE ENABLE DISABLE FULL SLIM NONE
ifdef COM_DEFS
  COM_DEFS_INVALID_VALUE := $(filter $(strip $(COM_DEFS)),$(COM_DEFS_INVALID_LIST))
  ifneq ($(COM_DEFS_INVALID_VALUE),)
    $(warning ERROR: COM_DEFS value ($(strip $(COM_DEFS_INVALID_VALUE))) is NOT allowable, please do Not assign COM_DEFS value as : ($(strip $(COM_DEFS_INVALID_LIST))))
    DEPENDENCY_CONFLICT = TRUE
  endif
endif

ifeq ($(strip $(CUSTOM_RELEASE)),FALSE)
  ifneq ($(strip $(FLAVOR)), $(strip $(ORIGINAL_FLAVOR)))
   $(warning ERROR: ORIGINAL_FLAVOR value should be $(FLAVOR))
   DEPENDENCY_CONFLICT = TRUE
  endif
endif

ifeq ($(strip $(CUSTOM_RELEASE)),FALSE)
  ifneq ($(strip $(PROJECT_NAME)), $(strip $(ORIGINAL_PROJECT_NAME)))
   $(warning ERROR: ORIGINAL_PROJECT_NAME value should be $(PROJECT_NAME))
   DEPENDENCY_CONFLICT = TRUE
  endif
endif

ifdef DEPENDENCY_CONFLICT
  ifeq ($(strip $(DEPENDENCY_CONFLICT)),TRUE)
    $(error PLEASE check above dependency errors!)
  endif
endif

ifeq ($(strip $(CUSTOM_RELEASE)),FALSE)
  RELEASE_LEVEL = NONE_RELEASE
endif

ifeq ($(strip $(CUSTOM_RELEASE)),TRUE)
  ifeq ($(strip $(MTK_SUBSIDIARY)),TRUE)
    RELEASE_LEVEL = SUB_RELEASE
  endif
endif

#rule for copy_br_bin
BR_FLAVOR := DEFAULT
BR_FILE   := cmdScript.bin
ifneq ($(strip $(CHIP_VER)),S00)
  BR_FLAVOR := $(CHIP_VER)
endif
ifneq ($(filter __FPGA__,$(strip $(COM_DEFS))),)
  BR_FLAVOR := FPGA
endif



# temp add
UNIVERSAL_DATABASE_SUPPORT = TRUE
