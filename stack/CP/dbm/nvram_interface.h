/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 * Filename:
 * ---------
 * nvram_interface.h
 *
 * Project:
 * --------
 *   C2K JADE
 *
 * Description:
 * ------------
 *   These are interface functions of NVRAM module.
 *
 ****************************************************************************/
#ifndef NVRAM_INTERFACE_H
#define NVRAM_INTERFACE_H

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */


#include "nvram_enums.h"
#include "nvram_defs.h"

/*****************************************************************************
 * FUNCTION
 *  NvramGetInfo
 * DESCRIPTION
 *  This is NvramGetInfo() function of NVRAM module.
 * PARAMETERS
 *  LID:         [IN]   NVRAM LID
 *  Total:       [OUT]  The record number of the LID
 *  Size:        [OUT]  The record Size of the LID
 * RETURNS
 *  NVRAM_ERRNO_SUCCESS if succeed, NVRAM_ERRNO_INVALID otherwise (ie, invalid LID).
 *****************************************************************************/
extern NvramErrnoEnum NvramGetInfo(NvramLIDEnumT LID, uint16 *Total, uint16 *Size);

/*****************************************************************************
 * FUNCTION
 *  NvramGetLIDNum
 * DESCRIPTION
 *  To get the Total number of LID
 * PARAMETERS
 *  void
 * RETURNS
 *  NvramPtr->ltable.TotalLID
  *****************************************************************************/
extern NvramLIDEnumT NvramGetLIDNum(void);

/*****************************************************************************
 * FUNCTION
 *  NvramCheckBackup
 * DESCRIPTION
 *  To tell given LID needs to backup or not
 * PARAMETERS
 *  LID:        [IN]    LID of the file
 *  prefix:     [IN]    file prefix
 * RETURNS
 *  NVRAM_IO_ERRNO_OK: need to backup
 *  NVRAM_IO_ERRNO_INVALID_LID: LID out of range or don't need to backup
  *****************************************************************************/
extern uint8 NvramCheckBackup(NvramLIDEnumT LID, char **prefix, char **verno);

/*****************************************************************************
 * FUNCTION
 *  NvramValidateFile
 * DESCRIPTION
 *  To validate integrity of  the given file
 * PARAMETERS
 *  LID:        [IN]    LID of the file
 *  path:       [IN]    path to validate
 * RETURNS
 *  NVRAM_IO_ERRNO_OK: valid
 *  NVRAM_IO_ERRNO_INVALID_LID: LID out of range
 *  others: invalid
  *****************************************************************************/
/*
extern NvramErrnoEnum NvramValidateFile(NvramLIDEnumT LID, WCHAR *path);
*/


/*****************************************************************************
 * FUNCTION
 *  NvramGetLockState
 * DESCRIPTION
 *  get Lock state from nvram context.
 * PARAMETERS
 *  void
 * RETURNS
 *  TRUE if locked, else FALSE
 *****************************************************************************/
extern bool NvramGetLockState(void);

/*****************************************************************************
 * FUNCTION
 *  NvramGetDefaultValue
 * DESCRIPTION
 *  This function is used to get default value for external modules.
 * PARAMETERS
 *  LID:             [IN]
 *  RecIndex:       [IN]
 *  p_buffer:        [IN/OUT]
 *
 * RETURNS
 *  NVRAM_DEFAULT_VALUE_FAIL : get default value fail
 *  NVRAM_DEFAULT_VALUE_FF   : p_buffer is invalid, default value is FF
 *  NVRAM_DEFAULT_VALUE_ZERO : p_buffer is invalid, default value is zero
 *  NVRAM_DEFAULT_VALUE_POINT: p_buffer is valid, use p_buffer to get default value
 *****************************************************************************/
extern NvramDefaultValueEnumT NvramGetDefaultValue(NvramLIDEnumT LID,
                                                        uint16 RecIndex,
                                                        uint8 **p_buffer);

/*****************************************************************************
 * FUNCTION
 *  NvramExternalReadData
 * DESCRIPTION
 *  This function is used to read data items for external modules.
 *  Please must check with NVRAM owner before you use this API.
 * PARAMETERS
 *  LID:            [IN]    NVRAM LID
 *  RecIndex:      [IN]    record index
 *  Buffer:         [OUT]    Buffer pointer
 *  BufSize:    [IN]    Buffer Size
 * RETURNS
 *  a boolean value
 * RETURN VALUES
 *  FALSE:  fail
 *  TRUE:   success
 *****************************************************************************/
extern bool NvramExternalReadData(NvramLIDEnumT LID, uint16 RecIndex, uint8 *Buffer, uint32 BufSize);

/*****************************************************************************
 * FUNCTION
 *  NvramExternalReadChksum
 * DESCRIPTION
 *  This function is used to read the checksum of data item for external module.
 *  Please must check with NVRAM owner before you use this API.
 * PARAMETERS
 *  LID:            [IN]    NVRAM LID
 *  RecIndex:      [IN]    record index
 *  RecAmount:     [IN]    read how many record
 *  Buffer:         [OUT]   Buffer pointer
 *  BufSize:    [IN]    Buffer Size
 * RETURNS
 *  a boolean value
 * RETURN VALUES
 *  FALSE:  fail
 *  TRUE:   success
 *****************************************************************************/
extern bool NvramExternalReadChksum(NvramLIDEnumT LID, uint16 RecIndex, uint16 RecAmount, uint8 *Buffer, uint32 BufSize);

/*****************************************************************************
 * FUNCTION
 *  NvramExternalWriteData
 * DESCRIPTION
 *  This function is used to write data items for external modules.
 *  Please must check with NVRAM owner before you use this API.
 * PARAMETERS
 *  LID:            [IN]    NVRAM LID
 *  RecIndex:      [IN]    record index
 *  Buffer:         [IN]    Buffer pointer
 *  BufSize:    [IN]    Buffer Size
 * RETURNS
 *  a boolean value
 * RETURN VALUES
 *  FALSE:  fail
 *  TRUE:   success
 *****************************************************************************/
extern bool NvramExternalWriteData(NvramLIDEnumT LID, uint16 RecIndex, uint8 *Buffer, uint32 BufSize);

/*****************************************************************************
 * FUNCTION
 *  NvramExternalResetData
 * DESCRIPTION
 *  This function is used to reset data items for external modules.
 *  Please must check with NVRAM owner before you use this API.
 * PARAMETERS
 *  LID:            [IN]    NVRAM LID
 *  RecIndex:      [IN]    record index
 *  Buffer:         [IN]    Buffer pointer
 * RETURNS
 *  a boolean value
 * RETURN VALUES
 *  FALSE:  fail
 *  TRUE:   success
 *****************************************************************************/
bool NvramExternalResetData(NvramLIDEnumT LID, uint16 RecIndex, uint16 RecAmount);

/*****************************************************************************
 * FUNCTION
 *  NvramCompareToDefaultValue
 * DESCRIPTION
 *  This function is used to compare the value in nvram file to default value
 * PARAMETERS
 *  LID:            [IN]    NVRAM LID
 *  RecIndex:      [IN]    record index, start from 1, but if the value is 0,
 *                          this function will compare all record to default value
 * RETURNS
 *  NvramErrnoEnum
 * RETURN VALUES
 *  NVRAM_ERRNO_SUCCESS:            no error
 *  NVRAM_ERRNO_FAIL:   at least one record is different to default value
 *****************************************************************************/
extern int32 NvramCompareToDefaultValue(NvramLIDEnumT LID, uint16 RecIndex);


/*****************************************************************************
 * FUNCTION
 *  NvramltableRegister
 * DESCRIPTION
 * PARAMETERS
 * RETURNS
 *****************************************************************************/
extern void NvramltableRegister(NvramltableEntryStruct *table);

/*****************************************************************************
 * for FT task to get all nvram data items
*****************************************************************************/
extern bool NvramUtilNextDataItem(NvramltableEntryStruct **entry);
extern bool NvramUtilGetDataItem(NvramltableEntryStruct **ldi, NvramLIDEnumT LID);


#ifdef MTK_NVRAM_CHECKSUM_LID_LIST
/*****************************************************************************
 * for Nvram LID list checksum
*****************************************************************************/
extern bool NvramExternalCalcLidListChecksum(char *buffer);
#endif

#ifdef __cplusplus
}
#endif

#endif /* NVRAM_INTERFACE_H */

