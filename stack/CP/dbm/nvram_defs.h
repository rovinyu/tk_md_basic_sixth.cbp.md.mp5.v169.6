/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 * Filename:
 * ---------
 * nvram_defs.h
 *
 * Project:
 * --------
 *   C2K JADE
 *
 * Description:
 * ------------
 *   This file contains definitions used by NVRAM task.
 *
 *******************************************************************************/

#ifndef NVRAM_DEFS_H
#define NVRAM_DEFS_H

#ifdef __cplusplus
extern "C"
{
#endif /* __cplusplus */ 

#include "nvram_enums.h"
#include "sysdefs.h"

/*****************************************************************************
 *  Define
 ****************************************************************************/

#define FILE_PREFIX_LEN 4
#define FILE_VERNO_LEN  3
#define NVRAM_FILE_LEN (FILE_PREFIX_LEN + 1 + FILE_VERNO_LEN)
#define NVRAM_MAX_DESC_LEN 20

#define NVRAM_EF_RESERVED_LID 0xFFFE

#define VER_LID(lid) lid##_VERNO lid
#ifndef NVRAM_AUTO_GEN
#define VER(lid)  lid##_VERNO
#else
#define VER(lid)  lid##_VERNO,#lid
#endif

#define NVRAM_EF_ZERO_DEFAULT_ZIP NVRAM_EF_ZERO_DEFAULT
#define NVRAM_EF_FF_DEFAULT_ZIP NVRAM_EF_FF_DEFAULT


#ifndef NVRAM_AUTO_GEN
    #define NVRAM_NORMAL(X) (uint8 const *)X
    #define NVRAM_NORMAL_NOT_GEN(X) (uint8 const *)X
    #define NVRAM_CUSTPACK(X) NVRAM_NORMAL(X)
    #define NVRAM_SECUPACK(X) NVRAM_EF_ZERO_DEFAULT
    #define NVRAM_DEFAULT_FUNC(X) (uint8 const*)X
#else
    #define NVRAM_NORMAL(X) #X, (uint8 const *)X
    #define NVRAM_NORMAL_NOT_GEN(X) #X,NVRAM_EF_ZERO_DEFAULT
    #define NVRAM_CUSTPACK(X) #X,NVRAM_EF_ZERO_DEFAULT
    #define NVRAM_SECUPACK(X) #X,(uint8 const *)X
    #define NVRAM_DEFAULT_FUNC(X) #X,NVRAM_EF_ZERO_DEFAULT
#endif

#define NVRAM_LOCK_PATTERN_LOCKED         "_NVRAM__YES_"
#define NVRAM_LOCK_PATTERN_UNLOCKED       " _NVRAM__NO_"

#define NVRAM_CHKSUM_SIZE sizeof(uint16)

/* AES 16 byte alignment, must be power of 2*/
#define NVRAM_MSP_ALIGNMENT 16
#define NVRAM_MSP_ALIGNMENT_REMAINDER(x) ((~(x) + 1) & (NVRAM_MSP_ALIGNMENT - 1))
#define NVRAM_MSP_ALIGNMENT_FLOOR(x) ((x) & (~(NVRAM_MSP_ALIGNMENT-1)))
#define NVRAM_MSP_ALIGNMENT_CEILING(x) (((x) + NVRAM_MSP_ALIGNMENT - 1) & ~(NVRAM_MSP_ALIGNMENT-1))

#define NVRAM_MSP_DECRYPT 0
#define NVRAM_MSP_ENCRYPT 1

#define NVRAM_APP_RESERVED 0

/* Attributes of a Logical Data Item, predefined. */
typedef uint16 NvramAttrT;
#define NVRAM_ATTR_AVERAGE          0x0000
#define NVRAM_ATTR_MULTI_DEFAULT    0x0001
#define NVRAM_ATTR_WRITEPROTECT     0x0002
#define NVRAM_ATTR_MULTIPLE         0x0004
#define NVRAM_ATTR_CONFIDENTIAL     0x0008
#define NVRAM_ATTR_MULTIREC_READ    0x0010
#define NVRAM_ATTR_MSP              0x0020 /* Protected by MTK Secure Platform */
#define NVRAM_ATTR_SW_VERNO_RESET   0x0040
#define NVRAM_ATTR_FACTORY_RESET    0x0080
#define NVRAM_ATTR_RING             0x0100
#define NVRAM_ATTR_PACKAGE          0x0200 /* NVRAM Reserved.LID will package in file */
#define NVRAM_ATTR_BACKUP_FAT       0x0400 /* Put a copy into backup partition, and the format is FAT */
#define NVRAM_ATTR_BACKUP_RAW       0x0800 /* Put a copy into backup partition, and the format is Raw data */
#define NVRAM_ATTR_ZIP_DEFAULT      0x1000 /* Nvram default value will be compressed, 
                                             the original default value must be wrapped by NVRAM_AUTO_GEN */
#define NVRAM_ATTR_NOT_ZIP_DEFAULT  0x2000
#define NVRAM_ATTR_COMMITTED        0x4000  /* Add for SP, add FS_COMMITTED while opening file */
#define NVRAM_ATTR_AUTO_RECOVER     0x8000  /* Add for SP, auto-recover while there is error in the file */


#define NVRAM_ATTR_ALL              0xFFFF


typedef uint32 NvramCategoryEnumT;
/* 
 * byte 0: NVRAM internal or Not 
 */
#define NVRAM_CATEGORY_USER             0x0000
#define NVRAM_CATEGORY_INTERNAL         0x0001

/* 
 * byte 1: storage information 
 */
#define NVRAM_CATEGORY_BACKUP_SDS       0x0010
#define NVRAM_CATEGORY_OTP              0x0020
#define NVRAM_CATEGORY_CUSTOM_DISK      0x0040 /* Used by custom, it means the data is put into another partition */

/* Used by custom, NVRAM will put custom's sensitive data into another folder
   if multiple folder feature turn on. Attribute of the data item in this Category
   must with NVRAM_ATTR_CONFIDENTIAL | NVRAM_ATTR_MULTIPLE */
#define NVRAM_CATEGORY_CUSTOM_SENSITIVE 0x0080


/* 
 * byte 2: default value information 
 */
#define NVRAM_CATEGORY_CUSTPACK         0x0100
#define NVRAM_CATEGORY_SECUPACK         0x0200
#define NVRAM_CATEGORY_FUNC_DEFAULT     0x0400

/* 
 * byte 3: factory tool/Smartphone Nvram related information 
 */
#define NVRAM_CATEGORY_CALIBRAT         0x1000
#define NVRAM_CATEGORY_IMPORTANT        0x2000
#define NVRAM_CATEGORY_IMPORTANT_L4     0x4000

#define NVRAM_CATEGORY_ALL              0xFFFF

typedef enum
{
    NVRAM_CLEAN_FOLDER_NONE = 0x00,
    NVRAM_CLEAN_FOLDER_SYSTEM = 0x01,       //custom_nvram_config.c
    NVRAM_CLEAN_FOLDER_FACTORY = 0x02,
    NVRAM_CLEAN_FOLDER_BRANCH = 0x04,
    NVRAM_CLEAN_FOLDER_ALL = 0xFF
} nvram_clean_folder_enum;
/*****************************************************************************
 *  Typedef
 ****************************************************************************/

typedef void (*NvramQueryDeaultFunc)(NvramLIDEnumT file_id, uint8 *Buffer, uint16 BufSize);

#ifndef NVRAM_AUTO_GEN
    typedef struct
    {
        NvramLIDEnumT          LID;                                /* Logical Data Item ID, U16 */                            
        uint16                 TotalRecords;                      /* Number of records */
        uint32                 Size;                               /* Size of one record. < sizeof(control Buffer) */                           
        uint8 const*           DefaultValue;                      /* Hardcoded default */
        NvramCategoryEnumT     Category;                           /* U32 */                       
        NvramAttrT             Attr;                               /* U16 */                           
        char                   FilePrefix[FILE_PREFIX_LEN + 1];
        char                   FileVerno[FILE_VERNO_LEN + 1];  
        char*                  description;                    
    } NvramltableEntryStruct;
#else
    typedef struct
    {
        NvramLIDEnumT          LID;                          
        uint16              TotalRecords;                  
        uint32              Size;                           
        char*               str_default_value;              
        uint8 const*        DefaultValue;                  
        NvramCategoryEnumT     Category;                       
        NvramAttrT         Attr;                           
        char                FilePrefix[FILE_PREFIX_LEN + 1];
        char                FileVerno[FILE_VERNO_LEN + 1];  
        char*               str_LID;                        
        char*               description;                    
    } NvramltableEntryStruct;
#endif

typedef struct
{
    NvramltableEntryStruct *ltable;
    uint32 Size;
}nvram_ltable_tbl_struct;

#if defined(NVRAM_AUTO_GEN)
#define ltable_entry_struct NvramltableEntryStruct __attribute__((aligned(4))) __attribute__((__section__("_nvram_ltable")))
#else
#ifdef NVRAM_SIM_ON_PC
#define ltable_entry_struct NvramltableEntryStruct
#else
#define ltable_entry_struct NvramltableEntryStruct __attribute__((section("_nvram_ltable")))
#endif
#endif


#ifndef NVRAM_CUSTPACK_TOTAL
#define NVRAM_CUSTPACK_TOTAL 1
#endif

typedef struct NVRAM_FS_PARAM_COMP
{
    uint32  opid_map;   //operate id
    uint32  ret[2];     //first bitmap second is error code
    uint32  Flag;       //fs_open [in] para
    uint32  *FileSize;  //fs_getfilesize [out] param
    int32   Offset;     //fs_seek [in] param
    int32   Whence;     //fs_seek [in] param
    void        *DataPtr;   //fs_read [out] param
    uint32  Length;     //fs_read [in]  param
    uint32  *Read;      //fs_read [out] param
} NVRAM_FS_PARAM_CMPT_T;

#define NVRAM_FS_CMPT_OPEN          ((1) << 0)
#define NVRAM_FS_CMPT_GETFILESIZE   ((1) << 1)
#define NVRAM_FS_CMPT_SEEK          ((1) << 2)
#define NVRAM_FS_CMPT_READ          ((1) << 3)
#define NVRAM_FS_CMPT_CLOSE         ((1) << 4)

/*****************************************************************************
 *  Global Function
 ****************************************************************************/

extern const uint8 NVRAM_EF_ZERO_DEFAULT[];
extern const uint8 NVRAM_EF_FF_DEFAULT[];

#ifdef __NVRAM_OTP__
typedef PACKED_PREFIX struct
{
   uint32 InitCode;
   uint8 Type;
   uint8 MEID[7];
   uint16 CheckSum;
} PACKED_POSTFIX NvOTPMEIDT;
#define NV_OTP_MEID_INIT_MAGIC  0x4d454944 /*MEID*/

#define GET_UINT8(p, pos) (*((uint8*)(p) + pos)) 
#define GET_UINT32(p) ((GET_UINT8(p, 0) << 24)|(GET_UINT8(p, 1) << 16)|(GET_UINT8(p, 2) << 8)|(GET_UINT8(p, 3)))  
#define OTP_MEID_V0(OtpPtr) (GET_UINT32((uint8*)OtpPtr + 4))
#define OTP_MEID_V1(OtpPtr) (GET_UINT32((uint8*)OtpPtr + 8))

#endif

#ifdef __cplusplus
}
#endif 

#endif /* NVRAM_DEFS_H */ 

