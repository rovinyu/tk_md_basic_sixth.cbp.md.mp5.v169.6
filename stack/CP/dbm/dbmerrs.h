/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef DBMERRS_H
#define DBMERRS_H
/*****************************************************************************
 
  FILE NAME:  dbmerrs.h

  DESCRIPTION:

    This file contains the fault codes for the DBM software unit.

*****************************************************************************/

/*------------------------------------------------------------------------
*  The following definitions are fault ids for MonFault routine.
*-----------------------------------------------------------------------*/

typedef enum 
{
    DBM_MSG_ID_ERR           = 0x00,
    DBM_SEG_NUM_ERR          = 0x01,
    DBM_SEG_SIZE_ERR         = 0x02,
    DBM_NOT_CACHED_ERR       = 0x03,
    DBM_NOT_RESPONDING_ERR   = 0x04,
    DBM_FLASH_TOO_SMALL_ERR  = 0x05,
    DBM_FLASH_AMBIGUOUS_ERR  = 0x06,
    DBM_BLK_DB_ID_ERR        = 0x07,
    DBM_BLK_DB_WRITE_ERR     = 0x08,
    DBM_BLK_DB_READ_ERR      = 0x09,
    DBM_NAM_LOCKED_ERR       = 0x0A,
    DBM_ERR_INVALID_SIG_ID   = 0x0B,
    DBM_WRITE_ERR            = 0x0C,
    DBM_ESN_NOT_CHANGED_INVALID_PASSWORD = 0x0D
#ifdef MTK_DEV_VERSION_CONTROL
    ,
    DBM_FILE_NOTEXIST        = 0x0E,
    DBM_FILE_LEN_ERR         = 0x0F
#endif
#ifdef MTK_DEV_RF_CUSTOMIZE
    ,
    DBM_INIT_RF_FAIL_ERR     = 0x10
#endif /* MTK_DEV_RF_CUSTOMIZE */
} DbmErrsT;

/*****************************************************************************
* $Log: dbmerrs.h $
* Revision 1.4  2006/01/03 09:09:46  wavis
* Merging in VAL.
* Revision 1.3.2.2  2005/11/07 14:23:51  wavis
* Merging in VAL/FSM.
* Revision 1.3.2.1  2005/09/29 10:18:05  wavis
* Duplicate revision
* Revision 1.3  2005/09/29 10:18:05  wavis
* Removal of FSI/FMM/NuFile.
* Revision 1.2  2004/03/24 15:53:51  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.1  2003/05/12 15:20:19  fpeng
* Initial revision
* Revision 1.4  2002/05/29 08:51:36  mshaver
* Added VIA Technologies copyright.
* Revision 1.3  2002/04/10 15:32:22  mshaver
* Added new DBM error for detecting an invalid signal at startup.
* Revision 1.2  2002/02/06 12:49:56  mshaver
* Added NAM locked mon fault.
* Revision 1.1  2000/10/06 13:40:19  mshaver
* Initial revision
* Revision 1.1  2000/10/06 18:26:48Z  mshaver
* Initial revision
* Revision 1.4  2000/06/07 15:05:48Z  mshaver
* Added new errors for dbm block reads/writes.
* Revision 1.3  2000/01/19 20:26:09Z  mshaver
* Fold in changes into the baseline
* Revision 1.2.1.3  2000/01/14 21:37:13Z  mshaver
* Forgot comma in DbmErrsT enum list
* Revision 1.2.1.2  2000/01/14 19:10:31Z  mshaver
* Added 2 error definitions for the dbm flash manager.
* Revision 1.2.1.1  1999/12/02 18:31:55Z  mshaver
* Duplicate revision
* Revision 1.2  1999/12/02 18:31:55Z  cdma
* Updated comments for CBP3.0
*****************************************************************************/
#endif
