/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2005-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/

/*************************************************************************************************
* 
* FILE NAME   : valatmisc.h
*
* DESCRIPTION : This file contains the declarations, definitions and data structures
*
*
* HISTORY     :
*     See Log at end of file
*
**************************************************************************************************/
#ifndef VALATMISC_H
#define VALATMISC_H

#ifdef SYS_FLASH_LESS_SUPPORT
#include "fsmrfsapi.h"
#endif

#ifdef MTK_CBP
#define VAL_ESN_SIZE    8
#endif

void MmiATGCAP (AtcSendAtMsgT* MsgDataP);
void MmiATCGMI (AtcSendAtMsgT* MsgDataP);
void MmiATCGMM(AtcSendAtMsgT* MsgDataP);
void MmiATCGMR (AtcSendAtMsgT* MsgDataP);
void MmiATGSN (AtcSendAtMsgT* MsgDataP);
void MmiATCSQ (AtcSendAtMsgT* MsgDataP);
void MmiATSAMECSQ (AtcSendAtMsgT* MsgDataP);
void MmiATCLVL (AtcSendAtMsgT* MsgDataP);
void MmiATHCMGL (AtcSendAtMsgT* MsgDataP);
void MmiATHCMGR (AtcSendAtMsgT* MsgDataP);	
void MmiATCNMA (AtcSendAtMsgT* MsgDataP);
void MmiATDTMF (AtcSendAtMsgT* MsgDataP);
void MmiATWLANUPD (AtcSendAtMsgT* MsgDataP);
void MmiATCVOICE (AtcSendAtMsgT* MsgDataP);
void MmiATCRMP (AtcSendAtMsgT* MsgDataP);
void MmiATCRSL (AtcSendAtMsgT* MsgDataP);
void MmiATCRPH (AtcSendAtMsgT* MsgDataP);
void MmiATVIP (AtcSendAtMsgT* MsgDataP);
void MmiATCTONE (AtcSendAtMsgT* MsgDataP);
void MmiATCDTMF (AtcSendAtMsgT* MsgDataP);
void MmiATCSOUND (AtcSendAtMsgT* MsgDataP);
void MmiATCTONESEQ(AtcSendAtMsgT* MsgDataP);
void MmiATCFUN (AtcSendAtMsgT* MsgDataP);
void MmiATVLPBK(AtcSendAtMsgT* MsgDataP);
void MmiATVUARTEST(AtcSendAtMsgT* MsgDataP);
void MmiATVUSBETS(AtcSendAtMsgT* MsgDataP);
//void MmiATVETSDEV(AtcSendAtMsgT* MsgDataP);
//void MmiATVDATACHAN(AtcSendAtMsgT* MsgDataP);
void MmiATURCHAN (AtcSendAtMsgT* MsgDataP);
void MmiATVTCHAN (AtcSendAtMsgT* MsgDataP);
void MmiATVTCHON (AtcSendAtMsgT* MsgDataP);
void MmiATVTCHSW (AtcSendAtMsgT* MsgDataP);
void MmiATMDMSIG (AtcSendAtMsgT* MsgDataP);
void MmiATVTAPP (AtcSendAtMsgT* MsgDataP);
void MmiATCSVOC (AtcSendAtMsgT* MsgDataP);
void MmiATCPVOC (AtcSendAtMsgT* MsgDataP);
void MmiATVGSMST(AtcSendAtMsgT* MsgDataP);


void ATCmdHandleKeyPressRpt (RegIdT RegId, uint32 MsgId, void *MsgBufferP);

void MmiATDDTM (AtcSendAtMsgT* MsgDataP);

void MmiATVP (AtcSendAtMsgT* MsgDataP);
void MmiATVGT (AtcSendAtMsgT* MsgDataP);
void MmiATVGR (AtcSendAtMsgT* MsgDataP);
void MmiATCMUT (AtcSendAtMsgT* MsgDataP);
void MmiATSPEAKER (AtcSendAtMsgT* MsgDataP);
void MmiATRESET (AtcSendAtMsgT* MsgDataP);
void MmiATREBOOT (AtcSendAtMsgT* MsgDataP);
void MmiATWLANCFG (AtcSendAtMsgT* MsgDataP);
void MmiATHWVER (AtcSendAtMsgT* MsgDataP);
void MmiATVOLT (AtcSendAtMsgT* MsgDataP);
void MmiATADC (AtcSendAtMsgT* MsgDataP);
void MmiATCSN (AtcSendAtMsgT* MsgDataP);
void MmiATVGPIOR (AtcSendAtMsgT* MsgDataP);
void MmiATVGPIOW (AtcSendAtMsgT* MsgDataP);
void MmiATVCUSVER (AtcSendAtMsgT* MsgDataP);
void MmiATVPHONEINFO (AtcSendAtMsgT* MsgDataP);
void MmiATAUDVERS (AtcSendAtMsgT* MsgDataP);
void MmiATRFSSYNC(AtcSendAtMsgT* MsgDataP);

#ifdef MTK_CBP
void MmiATESWLA (AtcSendAtMsgT *pMsgData);
void SendEnwinfoToAtc(EnwinfoEventE event);
#endif

#ifdef SYS_OPTION_DM
void MmiATOMADM (AtcSendAtMsgT* MsgDataP);
void MmiATOMALOG (AtcSendAtMsgT* MsgDataP);
void MmiATFUMO(AtcSendAtMsgT* MsgDataP);
void MmiATPRLV(AtcSendAtMsgT* MsgDataP);
void MmiATROAM (AtcSendAtMsgT* MsgDataP);
void MmiATOMASESS (AtcSendAtMsgT* MsgDataP);
void MmiATRTN (AtcSendAtMsgT* MsgDataP);
void MmiATHFA (AtcSendAtMsgT* MsgDataP);	
void MmiATPROXY (AtcSendAtMsgT* MsgDataP);	
extern uint32 PswGetSPC(void);
extern long atol(const char *nptr);
#else
void MmiATROAM (AtcSendAtMsgT* MsgDataP);
#endif

#ifdef MTK_CBP
void MmiATERXTESTMODE(AtcSendAtMsgT * MsgDataP);
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
void ValAtProcessHwdRxTxPowerDetectorMsg(ValHwdRxTxPowerDetectorMsgT *MsgDataP);
void SendETXPWRToAtc (int16 txPwr);
void SendERSSIToAtc(int16 mainRssi,int16 divRssi);
void MmiATERFTX(AtcSendAtMsgT* MsgDataP);
#endif

extern void ValResetByWatchdog(void);
extern bool ValPswGetVP(void);
extern UINT16 crcCalculate16Bit (UINT8 *message, UINT16  len);

#ifdef SYS_FLASH_LESS_SUPPORT
#ifdef MTK_DEV_CCCI_FS
#undef RfsImageForceSync
#define RfsImageForceSync(action)
#else
extern void RfsImageForceSync(RfsImageSyncActionT Action);
#endif
#endif

#endif
/**Log information: \main\Trophy\Trophy_zlin_href22058\1 2013-03-22 08:37:09 GMT zlin
** HREF#22058, Add RFS image upload action feild.**/
/**Log information: \main\Trophy\1 2013-03-22 08:51:43 GMT hzhang
** HREF#22058 to eanble RAM dump to APand add RFS image upload action feild.**/
