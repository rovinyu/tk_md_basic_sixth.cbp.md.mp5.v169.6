/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2009-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*********************************************************************************************
* FILE NAME   : ValEnhAtDefs.h
*
* DESCRIPTION :
*
* HISTORY:
*
*
*
********************************************************************************************/

/*==========================================================
      This file is used for customer to define customized AT command API

 ===========================================================*/
#ifndef  _VALENHATDEFS_H__
#define _VALENHATDEFS_H__

#include "sysdefs.h"
#include "valsmsapi.h"
#include "pswvalapi.h"
#ifdef MTK_CBP
#include "valatuim.h"
#endif

/*------------------------------------------------------------------------
Definition and Macros
-----------------------------------------------------------------------*/
#define   FREQ1       "800"
#define   FREQ2       " "

#define   KEY_PRESS            1
#define   KEY_RELEASE        0

#define BIT_SIZE(type) (sizeof(type) * 8)

#ifndef MIN
#define MIN(x,y)      (((x) < (y)) ? (x): (y))
#endif

#ifndef MAX
#define MAX(x,y)      (((x) > (y)) ? (x): (y))
#endif

#define MASK_TO_COPY(type) ((0xffffffff) >> (32 - BIT_SIZE(type)))

#define MASK(width, offset, data) \
   (((width) == BIT_SIZE(data)) ? (data) :   \
   ((((MASK_TO_COPY(data) << (BIT_SIZE(data) - ((width) % BIT_SIZE(data)))) & \
   MASK_TO_COPY(data)) >>  (offset)) & (data))) \

#define MASK_AND_SHIFT(width, offset, shift, data)  \
	((((signed) (shift)) < 0) ?       \
	MASK((width), (offset), (data)) << -(shift) :  \
	MASK((width), (offset), (data)) >>  (((unsigned) (shift)))) \

#define UNICODE_BOM 0xFEFF          /* byte order mark, normal */
#define UNICODE_BOM_SWAP 0xFFFE     /* byte order mark, reversed */
#define UPPER_BCD(bcd) ((bcd&0xF0) >> 4) /*The value of the upper 4 bits*/
#define LOWER_BCD(bcd) (bcd&0x0F) /*The value of the lower 4 bits*/

#define AT_MAX_NUM_DIAL_STR_LEN 32;

#if (defined SYS_OPTION_ATCMD_CH_2)
#ifdef MTK_CBP
#define DEFAULT_UNSOLICITED_RC_CHANNEL AT_CHAN_ATCMD_1
#else
#endif
#else
#define DEFAULT_UNSOLICITED_RC_CHANNEL AT_CHAN_ATCMD_1
#endif

#define VAL_PHB_UNICODE     (uint8)(0x80)
#define VAL_PHB_UNICODE_81  (uint8)(0x81)
#define VAL_PHB_UNICODE_82  (uint8)(0x82)

#ifdef MTK_CBP
#ifdef MTK_DEV_C2K_IRAT
#define VAL_MAX_UICC_SESSION_NUM  20  /* equal to MAX_LCH_NUM */
#else
#define VAL_MAX_UICC_SESSION_NUM  4   /* equal to MAX_LCH_NUM */
#endif
#endif

#define BIT_READ       0x01
#define BIT_UNREAD   0x02
#define BIT_SENT       0x04
#define BIT_UNSENT   0x08
#define BIT_ALL         0x10

#define BIT_DUPLICATE_REC_READ  0x20
#define BIT_DUPLICATE_REC_UNREAD  0x40

#ifdef MTK_CBP
#define MAX_DIAL_NUM_LEN 50
#else
#define MAX_DIAL_NUM_LEN 24
#endif

#if 0
 #define MAX_DIAL_NUM_LEN      VAL_PSW_MAX_CALLING_PARTY_NUMBER_SIZE
#endif


#define VAL_INVALID_ECIO   -512

#ifdef MTK_CBP
#define VAL_STABLE_ECIO_VALUE   -30
#define VAL_L1D_ECIO_VALUE  16

#define VAL_HWD_INVALID_TX_POWER    (-32768)

#define VAL_HDR_SINR_MULTI_TIMES     (512)

#define VAL_HDR_INVALID_SINR         0

#define VAL_HDR_MAX_SINR             200
#define VAL_HDR_MIN_SINR            -200

#define MAX_APN_NUM         20
#define MAX_APN_NI_LEN      100
#define MAX_APN_BEARER_LEN  20
#define MAX_APNED_STR_LEN   9

#define MAX_VAL_SET_ECC_NUM 10  /* Max total number of ECC number to set by AT+ECECCNUM */
#endif

#ifdef MTK_DEV_ENGINEER_MODE
#define VAL_DEFAULT_SERVICE_OPTION  3

#define VAL_CALL_EXIST      0
#define VAL_NO_CALL_EXIST   1
#endif

#if defined(__DYNAMIC_ANTENNA_TUNING__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
#define VAL_DAT_RCV_L1D_RSP   0x01
#define VAL_DAT_RCV_RMC_RSP   0x02
#define VAL_SAR_RCV_L1D_RSP   0x01
#define VAL_SAR_RCV_RMC_RSP   0x02

#define VAL_RF_FEATURE_INDEX_DAT 0x00
#define VAL_RF_FEATURE_INDEX_SAR 0x01

#endif


/*------------------------------------------------------------------------
    data structures
  ------------------------------------------------------------------------*/
typedef enum
{
   POWERDOWN_END,    /* Phone is offline */
		LOCKED_END,             /*Phone is CDMA locked*/
		NOSVC_END,              /* Phone has no service*/
		FADE_END,              /* Call Faded/Dropped */
		INTERCEPT_END,       /* Received Intercept from Base Station */
		REORDER_END,         /* Received Reorder from Base Station */
		RELEASE_END,	   /* Received a Release from Base Station (This is a normal call termination)*/
		SOREJECT_END,      /* Service Option rejected by Base Station */
		INCOMINGCALL_END,   /* Received Incoming Call */
		ALERTSTOP_END,       /* Received an alert stop from Base Station */
		ENDCALL_END,           /* Software ended the call (Normal release) */
		ENDACTIVATION_END,   /*  Received End Activation -OTASP calls only */
		ORIGFAIL_END,              /* Internal Software aborted the origination/call */
		NDSSFAIL_END,             /* NDSS failure (Network Directed System Selection, this is an IS-95B service)*/
		ACCPROBEEXH_END,   /* Maximum Access probes exhausted (The module failed to contact the Base Station)*/
		NOUIM_END,                 /* RUIM not present */
		INORIG_END,                /* Origination already in progress */
		ACCFAIL_END,             /* General Access Failure  */
		RETRY_END              /* Received retry order (IS-2000 only) */
} EndReasonT; /* used for +CEND */

typedef enum
{
   CM_CALL_END_OFFLINE=0,  /* Phone is offline */

   CM_CALL_END_LOCKED_END,             /*Phone is CDMA locked*/ /*Not defined in CT Spec*/

   CM_CALL_END_ORIGFAIL_END=12,              /* Internal Software aborted the origination/call */ /*Not defined in CT Spec*/

   CM_CALL_END_NO_SRV=21,  /* Phone has no service*/
   CM_CALL_END_FADE=22,  /* Call Faded/Dropped */
   CM_CALL_END_INTERCEPT=23,  /* Received Intercept from Base Station */
   CM_CALL_END_REORDER=24,  /* Received Reorder from Base Station */
   CM_CALL_END_REL_NORMAL=25,  /* Received a Release from Base Station (This is a normal call termination)*/
   CM_CALL_END_REL_SO_REJ=26,  /* Service Option rejected by Base Station */
   CM_CALL_END_INCOM_CALL=27,  /* Received Incoming Call */
   CM_CALL_END_ALERT_STOP=28,  /* Received an alert stop from Base Station */
   CM_CALL_END_CLIENT_END=29,  /* Software ended the call (Normal release) */
   CM_CALL_END_ACTIVATION=30,  /*  Received End Activation -OTASP calls only */
   CM_CALL_END_MC_ABORT=31,  /**/
   CM_CALL_END_RUIM_NOT_PRESENT=32,  /* RUIM not present */
   CM_CALL_END_NDSS_FAIL=99,  /* NDSS failure (Network Directed System Selection, this is an IS-95B service)*/
   CM_CALL_END_LL_CAUSE=100,  /*Released by lower layer, see cc_cause for more details*/
   CM_CALL_END_CONF_FAILED=101,  /* Network response Failure  */
   CM_CALL_END_INCOM_REJ=102,  /*Phone rejected the incoming call*/
   CM_CALL_END_SETUP_REJ=103,  /**/
   CM_CALL_END_NETWORK_END=104,  /*Release by network, see cc_cause for more details*/
   CM_CALL_END_NO_FUNDS=105,  /*No funds*/
   CM_CALL_END_NO_GW_SRV=106,  /*Not in the service area*/
   CM_CALL_END_REDIAL_TO_VOLTE=107 /*Redial to volte*/
#ifdef MTK_CBP
   ,CM_CALL_END_FDN_MISMATCH=241 /* dialed number does not match FDN list */
#endif
}EndStatusT;       /*used for ^CEND*/

typedef enum
{
   UNASSIGNED_CAUSE=1,
   NO_ROUTE_TO_DEST=3,
   CHANNEL_UNACCEPTABLE=6,
   OPERATOR_DETERMINED_BARRING=8,
   NORMAIL_CALL_CLEARING=16,
   USER_BUSY=17,
   NO_USER_RESPONDING=18,
   USER_ALERTING_NO_ANSWER=19,
   CALL_REJECTED=21,
   NUMBER_CHANGED=22,
   NON_SELECTED_USER_CLEARING=26,
   DESTINATION_OUT_OF_ORDER=27,
   INVALID_NUMBER_FORMAT=28,
   FACILITY_REJECTED=29,
   RESPONSE_TO_STATUS_ENQUIRY=30,
   NORMAL_UNSPECIFIED=31,
   NO_CIRCUIT_CHANNEL_AVAILABLE=34,
   NETWORK_OUT_OF_ORDER=38,
   TEMPORARY_FAILURE=41,
   SWITCHING_EQUIPMENT_CONGESTION=42,
   ACCESS_INFORMATION_DISCARDED=43,
   REQUESTED_CIRCUIT_CHANNEL_NOT_AVAILABLE=44,
   RESOURCES_UNAVAILABLE_UNSPECIFIED=47,
   QUALITY_OF_SERVICE_UNAVAILABLE=49,
   REQUESTED_FACILITY_NOT_SUBSCRIBED=50,
   INCOMING_CALL_BARRED_WITHING_CUG=55,
   BEARER_CAPABILITY_NOT_AUTHORISED=57,
   BEARER_CAPABILITY_NOT_PRESENTLY_AVAILABLE=58,
   SERVICE_OR_OPTION_NOT_AVAILABLE=63,
   BEARER_SERVICE_NOT_IMPLEMENTED=65,
   ACM_GEQ_ACMMAX=68,
   REQUESTED_FACILITY_NOT_IMPLEMENTED=69,
   ONLY_RESTRICTED_DIGITAL_INFO_BC_AVAILABLE=70,
   SERVICE_OR_OPTION_NOT_IMPLEMENTED=79,
   INVALID_TRANSACTION_ID_VALUE=81,
   USER_NOT_MEMBER_OF_CUG=87,
   INCOMPATIBLE_DESTINATION=88,
   INVALID_TRANSIT_NETWORK_SELECTION=91,
   SEMANTICALLY_INCORRECT_MESSAGE=95,
   INVALID_MANDATORY_INFORMATION=96,
   MESSAGE_TYPE_NON_EXISTENT=97,
   MESSAGE_TYPE_NOT_COMPATIBLE_WITH_PROT_STATE=98,
   IE_NON_EXISTENT_OR_NOT_IMPLEMENTED=99,
   CONDITIONAL_IE_ERROR=100,
   MESSAGE_NOT_COMPATIBLE_WITH_PROTOCOL_STATE=101,
   RECOVERY_ON_TIMER_EXPIRY=102,
   PROTOCOL_ERROR_UNSPECIFIED=111,
   INTERWORKING_UNSPECIFIED=127,
   REJ_UNSPECIFIED=160,
   AS_REJ_RR_REL_IND=161,
   AS_REJ_RR_RANDOM_ACCESS_FAILURE=162,
   AS_REJ_RRC_REL_IND=163,
   AS_REJ_RRC_CLOSE_SESSION_IND=164,
   AS_REJ_RRC_OPEN_SESSION_FAILURE=165,
   AS_REJ_LOW_LEVEL_FAIL=166,
   AS_REJ_LOW_LEVEL_FAIL_REDIAL_NOT_ALLOWED=167,
   MM_REJ_INVALID_SIM=168,
   MM_REJ_NO_SERVICE=169,
   MM_REJ_TIMER_T3230EXP=170,
   MM_REJ_NO_CELL_AVAILABLE=171,
   MM_REJ_WRONG_STATE=172,
   MM_REJ_ACCESS_CLASS_BLOCKED=173,
   ABORT_MSG_RECEIVED=174,
   OTHER_CAUSE=175,
   CNM_REJ_TIMER_T303_EXP=176,
   CNM_REJ_NO_RESOURES=177,
   CNM_MM_REL_PENDING=178,
   CNM_INVALID_USER_DATA=179
}CcCauseT;/*used for ^CEND*/

typedef enum
{
   CTSMS_NORMAL,
   CTSMS_CPT,
   CTSMS_VOICE_MAIL,
   CTSMS_SMS_REPORT,
}CTSmsTypeT;



typedef enum
{
   ATCMD_NOCALL_STATE,
   ATCMD_CALL_INTRAFFIC_STATE,
   ATCMD_CALL_ORIGINATE_WAIT_STATE,
   ATCMD_CALL_ALERT_WAIT_STATE,
   ATCMD_CALL_NO_SERVICE_STATE,
   ATCMD_CALL_STATE_NUM
} CallStateT;

typedef  enum
{
   MS_READY_STATE,
   MS_UNAVAIL_STATE,
   MS_UNKNOWN_STATE,
   MS_RINGING_STATE,
   MS_INCALL_STATE,
   MS_STATE_NUM
}PhoneStateT;  /*module state enum*/

typedef  enum
{
   VOICE_SERVICE,
   DATA_SERVICE,
   FAX_SERVICE,
   SMS_SERVICE,
   OTASP_SERVICE,
   MARKOV_LOOPBACK_SERVICE,
   UNKNOWN_SERVICE=9
}ServTypeT;

typedef enum
{
   UNKNOWN_TERM,
   MOBILE_TERM,
   MOBILE_ORIG
}TermTypeT; /*call type enum*/

typedef enum
{
   NOT_REG_SEARCHING,   /*not registered, ME is not currently searching a new operator to register to*/
   HOME_REG,                     /* registered, home network*/
   NOT_REG,                        /* not registered, but ME is currently searching for a BS*/
   REG_DENIED,                   /* registration denied*/
   UNKNOWN_REGSTAT,      /* unknown*/
   REG_ROAM                     /* registered, roaming*/
}RegStatT;    /* registration state enum*/

typedef enum
{
   NO_ERROR,
   NO_CDMA_SERVICE,
   IN_CALL,                      /*module is in a call, operation  not allowed*/
   NOT_IN_CALL,             /*module is not in a call, operation  not allowed*/
   UNKNOWN_CALL_STAT,
   CALL_BARRING,
   INVALID_SO,
   INVALID_PARAMETER_RPT,
   ALLOW_DURING_INCOMINGCALL, /*operation only allowed during an incoming call*/
   INVALID_MODE_SELECTION,
   INVALID_ROAM_SELECTION,
   INVALID_BAND_SELECTION
}ErrRptCodeT; /*value used for +CEER command*/

typedef enum
{
   UIM = 0,
   ME,
   MT,     /*UIM first, ME&UIM*/
   MT2,   /*ME first, ME&UIM*/
   RAM,
   UNSPEC
}SmsPreferredMemT;

typedef PACKED_PREFIX struct
{
   bool         Enable;
   bool         Mode;
   uint8        Class;
} PACKED_POSTFIX CcwaInfoT; /* +CCWA parameters*/

typedef PACKED_PREFIX struct
{
   bool              VromEnable;
   bool              VromInit;
} PACKED_POSTFIX VromInfoT; /* +VROM parameters*/

typedef PACKED_PREFIX struct
{
   bool         Enable;
   uint8        Number[32];
} PACKED_POSTFIX CcfcInfoT; /* +CCFC parameters*/

typedef PACKED_PREFIX struct
{
   uint8 Mode; /* 0 stop play, 1 play */
   uint8 Dest;  /*0 common speaker, 1 handfree speaker*/
   uint16 Freq;
   uint8 Vol;
   uint8 Dur;
} PACKED_POSTFIX CtoneT; /* +CTONE parameters */

typedef PACKED_PREFIX struct
{
   uint8 Mode; /* 0 stop play, 1 play */
   char  Dtmf;
   uint8 Vol;
   uint8 Dur;
} PACKED_POSTFIX CdtmfT; /* +CDTMF parameters*/

typedef PACKED_PREFIX struct
{
   bool    enable;
   uint8   Val;          /* 0: No side tone, 1: Handset Sidetone levels
                                 2: Headset Sidetone levels, 3: Max Sidetone level*/
} PACKED_POSTFIX  SidetT;

typedef PACKED_PREFIX struct
{
   uint16 DtmfMode;         /* VAL_SOUND_CONTINUES  or  VAL_SOUND_BURST*/
   uint16 tb;                      /* timeout for burst dtmf tone */
   uint16 tc;                       /* timeout for continuous dtmf tone */
} PACKED_POSTFIX  DtmfInfoT;

typedef enum
{
   CFUN_LPM = 0,
   CFUN_ONLINE = 1,
   CFUN_OFFLINE = 4,
   CFUN_FTM = 5,
   CFUN_RESET = 6
}ATCfunT;

typedef enum {
   ATSMS_DIGIT_MODE_INVALID = -1,
   ATSMS_DIGIT_MODE_4_BIT,
   ATSMS_DIGIT_MODE_8_BIT
} ATSmsDigitType;

typedef enum
{
   REC_UNREAD,
   REC_READ,
   STO_UNSENT,
   STO_SENT,
   DUPLICATE_REC_READ,
   DUPLICATE_REC_UNREAD,
   ALL
} ATSMSState;

typedef enum
{
#ifdef MTK_CBP
   SMS_ENCODING_UNSPECIFIED_OCTET = 0, /* 8-bit ISO Latin - 1 */
#else
   SMS_ENCODING_INVALID = -1,       /* Bad character encoding value*/
   SMS_ENCODING_UNSPECIFIED_OCTET,  /* 8-bit ISO Latin - 1 */
#endif
   SMS_ENCODING_IS91_EP,            /* 8-bit */
   SMS_ENCODING_ASCII,              /* 7-bit ASCII */
   SMS_ENCODING_IA5,                /* 7-bit similar to ASCII */
   SMS_ENCODING_UNICODE,            /* 16-bit. (Code must support double byte characters)*/
   SMS_ENCODING_JIS,                /* 8 or 16 bit japanese encoding*/
   SMS_ENCODING_KS,                 /* 8 or 16 bit Korean encoding*/
   SMS_ENCODING_LATIN8,             /* 8-bit. Hebrew in [0xe0..0xfa].*/
   SMS_ENCODING_LATIN1,             /* 8-bit.*/
   SMS_ENCODING_GSM7BIT,
#ifdef MTK_CBP
   /* SMS_ENCODING_INVALID is assigned to unsigned variable in MmiATHSMSSS which causes
    * build warning.
    */
   SMS_ENCODING_NA,                 /* No encoding field present. This is a bad situation.*/
   SMS_ENCODING_INVALID             /* Bad character encoding value*/
#else
   SMS_ENCODING_NA                  /* No encoding field present. This is a bad situation.*/
#endif
} ATSmsEncodingType;

typedef enum
{
   CTAT_SMS_GSM_7BIT,
   CTAT_SMS_ASCII,
   CTAT_SMS_IA5,
   CTAT_SMS_OCTET,
   CTAT_SMS_LATIN,
   CTAT_SMS_LATIN_HEBREW,
   CTAT_SMS_UNICODE,
   CTAT_SMS_OTHER
}CTATSmsFormatT;

typedef enum
{
   CTAT_SMS_TYPE_NORMAL = 0,
   CTAT_SMS_TYPE_CPT,
   CTAT_SMS_TYPE_VOICE_MAIL,
   CTAT_SMS_TYPE_REPORT,
   CTAT_SMS_TYPE_MAX
}CTATSmsTypeT;

typedef enum
{
   CTAT_SMS_STATE_UNREAD = 0,
   CTAT_SMS_STATE_READ,
   CTAT_SMS_STATE_UNSEND,
   CTAT_SMS_STATE_SENT,
   CTAT_SMS_STATE_ALL,
   CTAT_SMS_STATE_MAX
}CTATSmsStateT;

typedef enum
{
   SC,
   BARRING_INCOMING_ORIG,
   BARRING_INCOMING,
   BARRING_ORIG,
   BARRING_IR,
   BARRING_OI,
   BARRING_OX,
#ifdef MTK_CBP
   FD,
#endif
   MaxFacNum
}FaciTypeT; /* +CLCK */
#if 0
typedef enum
{
   PIN1,
   PIN2,
   BARRING_ORIG,
   BARRING_INCOMING,
   MaxFacNum
}FaciTypeT; /* +CLCK */
#endif

typedef PACKED_PREFIX struct
{
   uint8             CMGFMode; /* 1 Text Mode; 0 PDU Mode  */

   SmsPreferredMemT   mem1;      /*Memory to be used when listing, reading and deleting messages*/
   SmsPreferredMemT   mem2;      /*Memory to be used when writing and sending messages*/
   SmsPreferredMemT   mem3;      /*Received messages will be placed to this storage if routing to TE is not set*/
   uint8             CSDHParameter;
   /* +CSMP parameter*/
   uint16           TeleSrvId;
   uint8             vp;
   ValSmsMsgEncodeT   Encoding;

   /*+CNMI parameter*/
   uint8              CNMImode;
   uint8              bm;
   uint8              bfr;

   /* ^HSMSSS parameter */
   bool            needDak; /* if need Delivery ack */
   ValSmsPriorityT prt;          /* priority */
   ValSmsPrivacyT  prv;        /* privacy */

   uint8             mt;
   uint8             ds;

   bool              ToBeSave;
} PACKED_POSTFIX SmsCfgT;

typedef enum
{
    CS_ASCII = 0,
    CS_UCS2 = 1,
}CstypeT;

typedef enum
{
    Ciev_BatteryInd = 0,
    Ciev_SignalLevl = 1,
    Ciev_NetWorkServ = 2,
    Ciev_UnreadSms = 3,
    Ciev_VoiceCallInd = 4,
    Ciev_RoamInd = 5,
    Ciev_UimPinSt = 6,
    Ciev_SmsMemFulInd = 7,
    Ciev_UimInsertInd = 12,
    Ciev_FlashInfoInd = 13,
    Ciev_MinLockStInd = 100,
    Ciev_SmsPhbInitFinished = 101,
    Ciev_E911ModeInd = 102,
    Ciev_C109_Ind = 103,
    Ciev_DORMANT_Ind = 106,
#ifdef MTK_DEV_C2K_IRAT
    Ciev_GetUimInfo_Ind = 107,
    Ciev_Md1ModeSwitch_Ind = 110,
#else
    Ciev_GetUimNam_Ind = 107,
#endif
    Ciev_PppReconnect_Ind = 108,
    Ciev_IccidReady_Ind = 109,
    Ciev_prefmode_ind = 130,
    Ciev_Prlid_ind = 131,
    Ciev_Ind_MaxNum = 255
}CievIndT;


typedef PACKED_PREFIX struct
{
   uint8             CLIP;
   uint8             DtmfDurOn;
   uint8             DtmfDurOff;
/*   uint8             VGRParameter;
   uint8             VGTParameter;
*/
/*   bool               CMUTParameter;
   uint8              SpeakerMode;   +SPEAKER  parameter 0 common speaker, 1 handfree speaker*/
   CcwaInfoT      CcwaParameter;
   CcfcInfoT       CcfcParameter[3];
   CtoneT          CtoneParameter;
   CdtmfT          CdtmfParameter;
   SidetT           SidetParameter;

   DtmfInfoT      DtmfParameter;  /* ^DTMF command */
   ATCfunT      Fun;

   uint8 CvoiceMode;
   uint8 DdsetexPort;
   bool ModeURC;  /* ^MODE */

   bool MccMnc;
   uint8             REG;
   uint8             Vpon;
   uint8             Vser;
   bool             hdrVser;
   uint8             PrefMode;
   bool              NetmodeURC;

   SmsCfgT      SmsParms;
   ValDeviceT   PhbdeviceType;
   uint8             ATMaxUimNum;
   bool              CAIPParameter;
   bool              CMERParameter;
   VromInfoT      VromParamter;
   CstypeT       Cstype;
   bool              Vudsp;
   uint8           AllCievInd;
   uint8  		CievInd[Ciev_Ind_MaxNum];
   bool         DormantStateInd;
   bool             ConnectURC;
   bool            DosessionUrc;
   bool            Vmefl; /* +VMEFL */
   bool         enwkExistenceMode;    /* TRUE:enable +ENWEXISTENCE URC,FALSE:disable +ENWEXISTENCE URC */
   bool         ecgregMode;           /* TRUE:enable +ECGREG URC,FALSE:disable +ECGREG URC */
   bool         edefroamMode;         /* TRUE:enable +EDEFROAM URC,FALSE:disable +EDEFROAM URC */
   bool         edoromMode;           /* TRUE:enable +EDOROM URC,FALSE:disable +EDOROM URC */
} PACKED_POSTFIX ATCfgParmT;

typedef enum
{
   PS_DISABLED,
   PS_ENABLED
}PSStateT;


/*****************************************************************************
  ValAtPsStatusT:
    In Hybrid mode, both 1x and DO protocol stack opened/closed means Protocol stack opened/closed;
    In 1x only mode,  1x protocol stack opened/closed means Protocol stack opened/closed;
    In DO only mode, DO protocol stack opened/closed means Protocol stack opened/closed;

        VAL_AT_PS_CLOSED:       Protocl stack closed;
        VAL_AT_PS_OPENED:       Protocol stack opend;
        VAL_AT_PS_NOT_FINISH: Protocol stack is opening/closing.
*****************************************************************************/
typedef enum
{
    VAL_AT_PS_CLOSED,
    VAL_AT_PS_OPENED,
    VAL_AT_PS_NOT_FINISH
} ValAtPsStatusT;

typedef enum
{
   AT_PKT_NOT_DORMANT = 0,
   AT_PKT_DORMANT = 1,
   AT_PKT_RESERVED
}ATPktStateT; /*0: not dormant state.  1: dormant state.  2-255: reserved*/
#if 0
typedef enum
{
   AT_PKT_RELEASED = 0,
   AT_PKT_DORMANT = 1,
   AT_PKT_ACTIVE = 2
}ATPktStateT; /*0: released state.  1: dormant state. 2: active state*/
#endif

typedef enum
{
   CIEV_AT_PKT_RELEASED = 0,
   CIEV_AT_PKT_DORMANT = 1,
   CIEV_AT_PKT_ACTIVE = 2
}CievATPktStateT; /*0: released state.  1: dormant state. 2: active state*/

typedef enum
{
    AT_NOT_REGISTERED = 0,
    AT_REGISTERED = 1,
    AT_REGISTER_DENIED = 2
}ATRegisteredStateT;

#ifdef MTK_CBP
typedef enum
{
    VAL_NWK_NOT_EXIST = 0,  /* Network not exist */
    VAL_NWK_EXIST,          /* Network exist */
    VAL_NWK_EXIST_INVALID_STATUS = 0xFF /*Invalid status*/
}ValNwkExistenceStatusT;

typedef struct
{
    uint8                   wApn;
    uint8                   ApnCl;
    char                    ApnNI[MAX_APN_NI_LEN];
    IPAddrTypeT             ApnType;
    char                    ApnBearer[MAX_APN_BEARER_LEN];
    bool                    ApnEd;
    uint8                   ApnTime;
    bool                    present;
    bool                    NwRestricted; /*set to check if NW restrict this APN*/
} ValVzWAPNInfoT;

typedef struct
{
    uint8   apnIndex;
    char    apnNwId[MAX_APN_NI_LEN];
    uint16  apnInactTimer;
    bool    present;
} ValApnInactTimerT;

typedef struct
{
    uint8           valSetEccNumStr[VAL_PSW_MAX_CALLING_PARTY_NUMBER_SIZE + 1];     /*ECC Number*/
}ValAtSetEccNumT;

typedef struct
{
    uint8           setEccNumCount;            /*the max number of ValSetEccNumList*/    
    ValAtSetEccNumT valSetEccNumList[MAX_VAL_SET_ECC_NUM];/*Ecc number list*/
} ValAtSetEccNumListT;

typedef enum
{
    VAL_AT_RAT_UNSPECIFIED = 0,
    VAL_AT_RAT_1xRTT,
    VAL_AT_RAT_HRPD,
    VAL_AT_RAT_EHRPD,
    
    VAL_AT_RAT_INVALID
} ValAtRatT;
#endif

#ifdef MTK_DEV_ENGINEER_MODE
typedef enum
{
    VAL_AT_RTT_NOSVC = 0,
    VAL_AT_RTT_INIT,
    VAL_AT_RTT_IDLE,
    VAL_AT_RTT_TRAFFICINI,
    VAL_AT_RTT_TRAFFIC,
    VAL_AT_RTT_STATE_NUM
}ValAtRttStateT;

typedef enum
{
    VAL_AT_EVDO_INACTIVE = 0,
    VAL_AT_EVDO_ACQUISITION,
    VAL_AT_EVDO_SYNC,
    VAL_AT_EVDO_IDLE,
    VAL_AT_EVDO_ACCESS,
    VAL_AT_EVDO_CONNECTED,
    VAL_AT_EVDO_STATE_NUM
}ValAtEvdoStateT;
#endif

typedef PACKED_PREFIX struct
{
    CallStateT       CallState;
    PSStateT          PSSate;
    ValPowerupStatusT   HrpdPSState;
    PhoneStateT     PhoneState;
    TermTypeT       TermType;
    ServTypeT        ServiceType;
    /*  bool              AlertInd;*/
    uint8                   Band;/* 0-follow PRL,1-1900Mhz,2-800Mhz */
    uint8                   RegStat; /* 0-not registered&not search, 1-registered home
                                        2-not registered&searching, 4 unknown, 5 registered&roaming*/

    ValPswServiceStatusT    ServiceStat;
#ifdef MTK_DEV_C2K_IRAT
    ValPswServiceStatusT    SrvStatOfUnReg;
#endif

#ifdef MTK_CBP
    ValPswServiceStatusT    StableServiceStat; 
    ValNwkExistenceStatusT  NwkExistence;
#endif
    ValPswServiceStatusT    HdrServiceStat;
    ATRegisteredStateT      IsRegistered;
#ifdef MTK_CBP
    ValRoamTypeT            RegRoamStatus;
    ValRoamTypeT            HrpdRegRoamStatus;    
    ValSysInPrlStatusT      SysInPrl;
    ValSysInPrlStatusT      DoSysInPrl;
    bool                    isFemtoCell;    /* TRUE: Current 1xRTT system is in femtocell */
    bool                    DoisFemtoCell;  /* TRUE: Current EVDO system is in femtocell */
    uint16                  locArea;    /* Current 1xRTT NID */
    uint8                   DoSectorID[16]; /* Current EVDO system sector ID */
    uint8                   DoSubnetmask;   /* Current EVDO system subnet mask */
    bool                    conStatus;      /* TRUE: Current 1xRTT is in connected state */
    bool                    doConStatus;    /* TRUE: Current EVDO is in connected state */
    bool                    VoicePrivacyOn; /* TRUE: The Voice Privacy mode negotiated with network is On */
    bool                    psRegistered;   /* Current PS register status */
    ValAtRatT               psType;         /* Current PS registered type */
#endif
    uint8             Vrom;
    uint8             HrpdVrom;
    bool              RomStValid; /* TRUE: roaming status has been reported */
    bool              HrpdRomStValid; /* TRUE: roaming status has been reported */
    uint8                 NamNum;    /* NAM_1=1 */
    uint8                 SlotCycleIndex;         /* slot cycle index */

    bool                  InVoiceCall;
    ErrRptCodeT     ErrCode;  /*used for reporting call process error of +CEER cmd */
    uint8                 FacMode[7];   /*used for +CLCK command */
    bool                  InE911Mode; /*mode: TRUE indicates that PSW emergency mode is enabled
                                                                            FALE indicates that PSW emergency mode is disabled*/

   uint8              NetPreMode;   /* used in ^PREFMODE command. 2: CDMA  4: HDR 8: CDMA/HDR HYBRID */
   uint8              NetPreModeSaved;    /* used in special case */
   ATPktStateT     DormantState; /* ifdef FEATURE_CHINATELECOM: 0: not dormant state.  1: dormant state.  2-255: reserved */
   CievATPktStateT CievDormantState;
   ATUimStateT     uimState; /*1: uim card. 2: sim card. 3: uim sim card. 4. unkown card. 240: ROMSIM version. 255: card not ready. */
   uint8           ProRevInUse;             /* Protocol Revision in use */
   ValWmcConnStatusT WmcConnSt;
   PswCheckForCustomLockStateT MinLockSt;

   uint8                SysMode;  /* 0: No service   2: CDMA    4: HDR   8: CDMA/HDR HYBRID  */
   uint8            ConnectSt; /* 0: not connected; 1: 1x data;   2: HDR data; */
   uint8            C109Status;  /* 0: low;  1: high; 255: initial value */
} PACKED_POSTFIX  MsStatusT;

typedef enum
{
   CMD_IDLE,
   CMD_WFR_UIM_STATUS,
   CMD_WFR_CHV_RSP
/*    CMD_WFR_CHV_VERIFY,
      CMD_WFR_CHV_CHANGE*/
}  CmdStateT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
    UTRFSH_NOT_USED = 0,
    UTRFSH_FILE_CHG = 1,
    UTRFSH_UICC_RESET =2,
    UTRFSH_UIM_APP_REINIT =3,
    UTRFSH_UIM_APP_REINIT_AND_FILE_CHG = 4,
    UTRFSH_ACTION_NUM
} UtkRefreshActionT;
#endif

#ifdef MTK_CBP
typedef enum
{
   ESWLA_PHASE_NONE = 0,
   ESWLA_CMD_RCVD = 1,
   ESWLA_REPLY_OK = 2
}AtEswlaPhaseEnumT;
#endif

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
    UIM_BT_DISCONNECT_STATUS = 0,
    UIM_BT_CONNECT_STATUS,
    UIM_BT_STATUS_NUM=2
}UimBTStatusT;
#endif

#if defined(__DYNAMIC_ANTENNA_TUNING__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
#define VAL_DAT_SAR_RCV_CMD_MAX_NUM     8
#define VAL_DAT_SAR_SEQ_INVALID_NUM     0xFF
typedef PACKED_PREFIX struct {
    uint8   uSeqNum;    /*index of received Set DAT or SAR cmd*/
    
#ifdef __DYNAMIC_ANTENNA_TUNING__  
    uint8   uRcvDatCfgRspFlg;
#endif/* DAT */

#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
    uint8   uRcvSarCfgRspFlg;
#endif/* SAR */
}PACKED_POSTFIX ValDatSarCfgIonfT;
#endif

#define DEFAULT_RSSI_DELTA 1

typedef PACKED_PREFIX struct
{
    char    DialedDigits[VAL_PSW_MAX_CALLING_PARTY_NUMBER_SIZE+1];
    char    DataDialedDig[VAL_PSW_MAX_CALLING_PARTY_NUMBER_SIZE+1];
    uint8   CallingNum[VAL_PSW_MAX_CALLING_PARTY_NUMBER_SIZE+1];
    bool    DialedIsE911;
#ifdef MTK_CBP
    bool    isRedialFromVoLTE;/* redial from VoLTE */
    bool    AtChvRecvd; /* TRUE:AT+CHV is received(User hang up the call),FALSE:AT+CHV is not received */
    uint8   callId_bitmap; /* bitmap for callId to indicate which callId is in use */
    bool    isWPSCall; /* TRUE: is in WPS call procedure,FALSE:is not in WPS call procedure */
#endif
    ValNumberTypeT CallingNumType1;
    uint8   CallingNum1[VAL_PSW_MAX_CALLING_PARTY_NUMBER_SIZE+1];
    uint8   FirstCallId;
    uint8   VponStSaved;
    ValNumberTypeT CallingNumType;
    ValPresentationIndicatorT CallingNumPi;

    ATCfgParmT     CfgInfo;
    MsStatusT      Status; /* Phone's state info */
    ValBSCoOrdMsgT BsInfo;

    IMSIType        Imsi;
    ValNetworkInfo  NetworkInfo; /*MCC/MNC from network*/
    bool            NetworkInfoValid; /* TRUE: Already saved MCC/MNC in network.  FALSE: Not yet. */
    uint32          UIM_ID;
    /*  uint32      Esn;*/
    uint8           Mdn[16];  /* ASCII code type */
    int16           Rssi;
#ifdef MTK_CBP
    int16           StableRssi;
    bool            ImsimProgrammed; /* CSIM IMSI_M programmed status */
#endif
#ifdef MTK_DEV_C2K_IRAT
    int16           EcIo;
    int16           StableEcIo;
#endif
#ifdef MTK_DEV_FACTORY_MODE
    uint8           Rssi2;
    uint8           DoRssi1;
    uint8           DoRssi2;
#endif
#ifdef MTK_CBP
    int16           TxAnt;
    int16           DoTxAnt;
#endif
    int32           RssiHrpd;
    uint8           CsqHrpd;
    uint8           HdrCsq;

#if defined (MTK_CBP) && defined (MTK_DEV_C2K_IRAT)
    uint16          HdrSinr;            /*this is Sinr raw value multiplied by 512*/
    bool            EnableUrcSinr;      /*Control URC "^HDRSINR",TRUE:enable,FALSE:disable*/
#endif
    
#ifdef MTK_DEV_C2K_IRAT
    int16           HdrEcIo;
#endif
    bool            HrpdNtwkAcqd;
    bool            Arsi;
    uint8           RssiDelta;
    bool            HRssiLvl;
    uint8           RingCount;
    uint8           UnslctRcChan;
#ifdef MTK_CBP
#ifdef SYS_OPTION_MORE_AT_CHANNEL
    uint8           SmsUrcChan;
    uint8           CcUrcChan;
    uint8           NwUrcChan;
#else
    uint8           SmsUnslctRcChan;
    uint8           SendSmsUnslctRcChan;
#endif
#endif

    uint32          ConnStartTime;  /*start time when call is connected*/
    uint32          ConnEndTime;    /*end time when call is disconnected*/
    NetworkPrefT    ConnSystem;
#ifdef TCPIP_ATC
    char 		    sPppusername[HLP_MAX_USRID_LEN];
    char	        sPpppassword[HLP_MAX_PSWD_LEN];
#endif
    CstypeT         Cstype;

#if ((SYS_OPTION_GPS_HW ==SYS_GPS_LOCAL_INTERNAL) || defined SYS_OPTION_GPS_PGPS || defined SYS_OPTION_GPS_EXTERNAL) /*HCBP#6003:Add Gps output call back function for AT in for phoenix project*/
    bool		    bGpsLocation;
#endif
   /* The following two chan added for UIM cmd. Mult-ATP */
    uint8           ATUimStatusCallbkChan;
    uint8           ATUimChvCallbkChan;

#ifndef MTK_CBP
    /* there is only one unsolicited channel */
    AtcSendAtRespMsgT  UnSlctResultList;
    bool               UnslctPending;
    uint8              UnslctLines;
#endif

    uint8              bHrpdSessionOpen;
    uint8              Ps1XState;
#ifdef SYS_OPTION_DM
    uint8	    Oamlog;
    uint8 	    Oamfumo;
    uint8	    Oamprl;
    uint8	    Oamroam;
    uint8	    Oamrmguard;
#endif

#ifdef MTK_CBP
    uint8       UiccSessionOpenedNum;
    uint8       UiccSessionId[VAL_MAX_UICC_SESSION_NUM]; /* for +CGLA and +CRLA */
    uint8       UiccSessionIdIsClosing;
#else
    uint8       UiccSessionId;
#endif

#ifdef MTK_DEV_C2K_IRAT
    UimBTStatusT        UimBTStatus;
#endif

#ifdef MTK_DEV_C2K_IRAT
    UtkRefreshActionT    UtkRefreshAction;
    bool                 LteArfcnMode;
    bool                 EarfcnInfoValid;
    uint8                NumOfEarfcns;
    uint16               Earfcn[MAX_EUTRA_FREQS_NUM];
#endif

#ifdef MULTI_ENHATP_DISABLE
    CmdStateT            EnhATCmdStatus;
    uint8                EnhATRspLinesIndex;    /*used for multiline response*/
    uint16               EnhATcmdId;            /*used to save multiline's cmd */
    bool                 EhnATWaitForRspAck;
    bool                 EhnATWaitForResultAck;
    uint8                CurrProcChan;
#else
    CmdStateT            EnhATCmdStatus[AT_CHAN_NUM];
    uint8                EnhATRspLinesIndex[AT_CHAN_NUM];    /*used for multiline response*/
    uint16               EnhATcmdId[AT_CHAN_NUM];            /*used to save multiline's cmd */
    bool                 EhnATWaitForRspAck[AT_CHAN_NUM];
    bool                 EhnATWaitForResultAck[AT_CHAN_NUM];
#endif

#ifdef MTK_DEV_ENGINEER_MODE
    RfTstControlModulationT  RfTstModulation;
    uint8                    engInfo_mode;
    uint16                   engInfo_info_levels;
    uint8                    engInfo_period;
    ValForceTxAntModeT       forceTxAntMode;
    uint8                    forceTxAntIndex;
    ValAtRttStateT           State;
    ValAtEvdoStateT          DoState;
    uint16                   serviceOption;
    uint8                    rateReduc;
#endif
#ifdef MTK_DEV_FACTORY_MODE
    uint8                   esimMode;
    uint8                   ecbandMode;
    uint8                   ecsqMode;
    uint8                   ecbandSys;
    uint8                   ecbandBand;
    uint16                  ecbandChannel;
#endif

#ifdef MTK_CBP
    AtEswlaPhaseEnumT       eswlaPhase;
    uint8                   eswlaChan;
    bool                    evocdMode;
    bool                    ecesntMode;
    bool                    regResumeFlag;
    uint8                   otaRegMode;
    bool                    eTCAssignMode;
    ValHwdTstReqModeT       valHwdTstMode;
    bool                    ermsMode;
    uint8                   setSystemSelectChan;
    bool                    inSystemSelectProcess;
    bool                    eiPrlMode;      /* TRUE:enable +EIPRL URC,FALSE:disable +EIPRL URC */
    bool                    eFCellMode;     /* TRUE:enable +EFCELL URC,FALSE:disable +EFCELL URC */
    ValApnInactTimerT       apnInactTimerTable[MAX_APN_NUM];	/* APN Inactivity timer table */
    ValNwkSrvStageT         curr1xNwkSrvStage;    /* To indicate if it is power off situation or normal service to no service */
    bool                    ecConStMode;      /* TRUE:enable +ECCONST URC,FALSE:disable +ECCONST URC */
#endif
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
    uint8                   psSuspendFlag;   
    int16                   suspendRssi; /* CSQ RSSI for SUSPEND */
    int16                   suspendEcIo; /* CSQ EcIo for SUSPEND */
    ValPswServiceStatusT    suspendServStat; /* VSER for SUSPEND */
    ValPswServiceStatusT    suspendHdrServiceStat;
    bool                    suspendHrpdNtwkAcqd; /* MODE for SUSPEND */
    uint8                   suspendSysMode; /* MODE for SUSPEND */
    uint16                  suspendMcc; /* MNCMCC for SUSPEND */
	uint8                   suspendImsi_11_12; /* MNCMCC for SUSPEND */
    bool                    suspendNetworkInfoValid; /* MNCMCC for SUSPEND */
    ValRoamTypeT            suspendRegRoamStatus; /* SYSINFO for SUSPEND */
    bool                    suspendRomStValid; /* SYSINFO for SUSPEND */
    ValRoamTypeT            suspendHrpdRegRoamStatus; /* SYSINFO for SUSPEND */
    bool                    suspendHrpdRomStValid; /* SYSINFO for SUSPEND */    
    int32                   suspendRssiHrpd;
    uint8                   suspendCsqHrpd;
    uint8                   suspendHdrCsq;
    int16                   suspendHdrEcIo;
    uint16                  suspendHdrSinr;
#endif

#ifdef MTK_CBP
    uint8                   enableCsqRptThrld;       /*1: enable to report CSQ when Rssi or ec/io below the the threshold, 
                                                                                                2: enable to report CSQ when RSSI or ec/io upper the threshold,
                                                                                                0: do nothing*/
    uint8                   thresholdEcIo;              /*threshold for ec/io,range (0 - 64)*/
    uint8                   thresholdRssi;              /*threshold for Rssi, range ( 52 - 113)*/

    bool                    enableEccNumUrc;        /*1,enable to report EF ECC Number URC, 0, do nothing*/
    ValAtUimEccNumListT     stValEccNumOfUim;       /*Saved the Ecc num from EF ECC of the uim*/
    ValAtSetEccNumListT     valSetEccNumber;           /* save the ecc number set from ECECCNUM */

    uint8                   uDestIdOfTxRxTimeRsp;
    uint8                   uRcvTxRxTimeRspFlg;     /*0x01, Val has received Rsp for quering rx/tx time info from DO
                                                                                        * 0x02, Val has received Rsp for quering rx/tx time info from L1D*/
    uint32                  uRxTimeInfo;
    uint32                  aTxtimeInfo[VAL_NUM_TX_POWER_LEVELS];

    uint8                   bCallExistMd1;
    uint8                   bCallExistMd3;
    uint8                   bEccCallExistMd3;

    
#if defined(__DYNAMIC_ANTENNA_TUNING__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
    ValDatSarCfgIonfT       RcvDatSarCmdList[VAL_DAT_SAR_RCV_CMD_MAX_NUM];
#endif

#endif
#ifdef MTK_CBP_ENCRYPT_VOICE
	uint8                   EncryptVoiceMode;/*0:manual mode, 1: automatical mode*/
	uint8                   EncryptVoiceInitiated;
	uint8                   BatteryLevel;
#endif
} PACKED_POSTFIX  ATParmInfo;

typedef struct
{
   char*		Name;
   void		(*CmdProcessFunc)(AtcSendAtMsgT* MsgDataP);
}AtCmdInfo;

typedef enum
{
   PHONE_FAILURE = 0,
   OPERATION_NOT_ALLOWED=3,
   OPERATION_NOT_SUPPORTED=4,
   UIM_NOT_INSERTED=10,
   UIM_PIN1_REQUIRED=11,
   UIM_PUK1_REQUIRED=12,
   UIM_FAILURE=13,
   UIM_BUSY=14,
   UIM_WRONG=15,
   UIM_WRONG_PASSWORD=16,
   UIM_PIN2_REQUIRED=17,
   UIM_PUK2_REQUIRED=18,

   PBK_FULL=20,
   PBK_INVALID_INDEX=21,
   PBK_ENTRY_NOT_FOUND=22,
   PBK_MEMORY_FAILURE=23,

   TEXT_STRING_TOO_LONG=24,
   DIAL_STRING_TOO_LONG=26,
   INVALID_CHARACTERS_IN_DIAL_STRING=27,
   NO_NEWORK_SERVICE=30,
   NETWORK_TIMEOUT=31,
   NETWORK_NOT_ALLOWED_EMERGENCY_CALLS_ONLY=32,
   NEWORK_PER_PIN_REQUIRED=40,
   SW_RESOURCE_NOT_AVAILABLE,
   INVALID_PARAMETER,
   NV_MEMORY_FAILURE,
   STORAGE_FULL,
   INVALID_CPIN,
   PHB_BUSY_INITING = 48,

#ifdef FEATURE_UTK
   UTK_IN_SESSION = 50,
   UTK_RSP_TIMEOUT,
#endif
#ifdef MTK_DEV_C2K_IRAT
   /*Defined for Bluetooth Access UIM Profile error*/
   UIM_ERROR_NO_REASON = 60,
   UIM_NOT_ACCESSIBLE,
   UIM_ALREADY_POWER_OFF,
   UIM_ALREADY_POWER_ON,
#endif
#ifdef MTK_CBP
   UIM_NO_SUCH_APP = 90,
#endif
   UNKNOWN=100,
#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
   MISSING_APN = 127,
#endif
   SERVICE_OPTION_TEMPORARILY_OUT_OF_ORDER = 134,
#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
   FEATURE_UNSUPPORTED = 140,
   UNKNOWN_PDP = 143,
   ALREADY_ACTIVE = 146,
   NETWORK_FAILURE = 158,
   MAX_COUNT_REACHED = 178,
   CALL_NUMBER_FDN_MISMATCH = 241,
#endif
   NETWORK_REJECTED_SUPSERV_REQUEST=257,
   RETRY_OPERATION=258,
   INVALID_DEFLECTED_TO_NUMBER=259,
   DEFLECTED_TO_OWN_NUMBER=260,
   UNKNOWN_SUBSCRIBER=261,
   SERVICE_NOT_AVAILABLE=262,
   UNKNOWN_CLASS=263,
   UNKNOWN_NETWORK_MESSAGE=264,
   NO_CNMA_ACK_EXPECTED = 340,
#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
    CPOF_NOT_ALLOWED_WHEN_SET_RAT_MODE = 350,
    PSDM_SPECIFIC_ERROR_START = 0x8000,
    GENERAL_ERROR = PSDM_SPECIFIC_ERROR_START,
    UNAUTHORIZED_APN,
    PDN_LIMIT_EXCEEDED,
    NO_PGW_AVAILABLE,
    PGW_UNREACHABLE,
    PGW_REJECT,
    INSUFFICIENT_PARAMETERS,
    RESOURCE_UNAVAILABLE,
    ADMIN_PROHIBITED,
    PDN_ID_ALREADY_IN_USE,
    SUBSCRIPTION_LIMITATION,
    PDN_CONN_ALREADY_EXIST_FOR_PDN,
    EMERGENCY_NOT_SUPPORTED,
    RECONNECT_NOT_ALLOWED,

    EAP_AKA_FAILURE = 0x8080,
    RETRY_TMR_THROTTLING,
    NETWORK_NO_RSP,
    PDN_ATTACH_ABORT,
    PDN_LIMIT_EXCEEDED_IN_UE_SIDE,
    PDN_ID_ALREADY_IN_USE_IN_UE_SIDE,
    INVALID_PDN_ATTACH_REQ,
    PDN_REC_FAILURE,
    MAIN_CONN_SETUP_FAILURE,
    BEARER_RESOURCE_UNAVAILABLE,
    OP_ABORT_BY_USER,
    RTT_DATA_CONNECTED,
    EAP_AKA_REJECT,
    LCP_NEGO_3_FAILURE,
    TCH_SETUP_FAILURE,
    NW_NO_RSP_IN_LCP,
    NW_NO_RSP_IN_AUTH,

    UNSPECIFIED = 0x80b0,
    PPP_AUTH_FAIL = 0x80b6,
    MIP_PPP_LCP_TMO,
    MIP_PPP_NCP_TMO,
    SIP_PPP_LCP_TMO,
    SIP_PPP_NCP_TMO,
    MIP_PPP_OPT_MISMATCH,
    SIP_PPP_OPT_MISMATCH,
    MIP_SOL_TMO,
    MIP_RRQ_TMO,
    MIP_RRP_ERR,
    MIP_LCP_FAIL,
    SIP_LCP_FAIL,
    MIP_NCP_FAIL,
    SIP_NCP_FAIL,
    PPP_A12_AUTH_FAIL,
    PPP_EAP_AKA_AUTH_FAIL,
    MIP_ADMIN_PROHIB_FAIL,
    MULTI_TCH_FAIL,
    PPP_A12_LCP_TMO,

    PDP_BUSY = 0x80e0,
    PDP_INVALID_PARAMETER,
    PDP_IRAT_BUSY,
    ONLY_PRI_CID_ALLOWED,

    PSDM_SPECIFIC_ERROR_END = PDP_BUSY + 0xff,
#endif
    CALL_INDEX_ERROR = 65280,
    CALL_STATE_ERROR = 65281,
    SYS_STATE_ERROR = 65282,
    PARAMETERS_ERROR = 65283,
#ifdef MTK_CBP
    CME_ERROR_NULL,
#endif
    CmeError_MAX/*avoid compile warning*/
} CmeErrorEnumT;

typedef enum
{
  CMS_ERR_SMS_TRANS_REJECTED = 21,
  CMS_ERR_Unspec_TP_DCS_ERROR = 159,
  CMS_ERR_TP_VPF_NOT_SUPPORTED = 198,
  CMS_ERR_TP_VP_NOT_SUPPORTED = 199,
  CMS_ERR_R_UIM_SMS_STORAGE_FULL = 208,
  CMS_ERR_MEM_CAPACITY_EXCEEDED = 211,
  CMS_ERR_OPERATION_NOT_ALLOWED = 302,
  CMS_ERR_OPERATION_NOT_SUPPORTED = 303,
  CMS_ERR_INVALID_PDU_MODE_PARA = 304,
  CMS_ERR_INVALID_TXT_MODE_PARAMETER = 305,
  CMS_ERR_MEM_FAIL = 320,
  CMS_ERR_INVALID_MEM_INDEX = 321,
  CMS_ERR_MEM_FULL =322,
  CMS_ERR_NO_CNMA_ACK_EXPECTED = 340,
  CMS_ERR_INVALID_DESTINATION_ADDR = 513,
  CMS_ERR_INVALID_MSG_BODY_LEN = 514,
  CMS_ERR_PHONE_NOT_IN_SERVICE = 515,
  CMS_ERR_INVALID_PREF_MEM_STORAGE = 516,
  CMS_ERR_USER_TEMINATED = 517,
  CMS_ERR_FDN_APPROVE_FAILED = 518
}CmsErrorEnumT;


#if 0
typedef enum
{
   OPERATION_NOT_ALLOWED=3,
   OPERATION_NOT_SUPPORTED,
   UIM_NOT_INSERTED=10,
   UIM_PIN1_REQUIRED,
   UIM_PUK1_REQUIRED,
   UIM_FAILURE,
   UIM_BUSY=14,
   UIM_WRONG_PASSWORD=16,
   UIM_PIN2_REQUIRED,
   UIM_PUK2_REQUIRED,

   PBK_FULL=20,
   PBK_INVALID_INDEX,
   PBK_ENTRY_NOT_FOUND,

   TXT_TOO_LONG=24,
   DIAL_TOO_LONG=26,
   INVALID_CHARACTERS_IN_DIAL_STRING=27,
   NO_NEWORK_SERVICE=30,
   NEWORK_PER_PIN_REQUIRED=40,
   SW_RESOURCE_NOT_AVAILABLE,
   INVALID_PARAMETER,
   NV_MEMORY_FAILURE,
   STORAGE_FULL,
   INVALID_CPIN,
   PHB_BUSY_INITING = 48,

#ifdef FEATURE_UTK
   UTK_IN_SESSION = 50,
   UTK_RSP_TIMEOUT,

#endif
   NO_CNMA_ACK_EXPECTED = 340,

   CmeError_MAX/*avoid compile warning*/
} CmeErrorEnumT;
#endif

typedef  struct
{
   bool Enable;
} ATEnableT;

typedef enum
{
   UNCONDITIONAL_FORWARDING,
   MOBILE_BUSY,
   NO_REPLY
} ATReasonT;

typedef enum
{
   ACH_REGISTRATION
} ATUimAuthMsgT;



typedef enum
{
  Ciev_BatteryLev0 = 0,
  Ciev_BatteryLev1 = 1,
  Ciev_BatteryLev2 = 2,
  Ciev_BatteryLev3 = 3,
  Ciev_BatteryLev4 = 4,
  Ciev_BatteryLev5 = 5
}Ciev_BatteryIndT;

typedef enum
{
  Ciev_SignalLev0 = 0,
  Ciev_SignalLev1 = 1,
  Ciev_SignalLev2 = 2,
  Ciev_SignalLev3 = 3,
  Ciev_SignalLev4 = 4,
  Ciev_SignalLev5 = 5,
}Ciev_SignalLevlT;

typedef enum
{
  Ciev_InServ = 0,
  Ciev_OutOfServ = 1
}Ciev_NetWorkServT;

typedef enum
{
  Ciev_No_UnreadSms = 0,
  Ciev_UnreadSms_Ind = 1
}Ciev_UnreadSmsT;

typedef enum
{
  Ciev_NoCall = 0,
  Ciev_InCall = 1
}Ciev_VoiceCallIndT;

typedef enum
{
  Ciev_RoamOff = 0,
  Ciev_RoamOn = 1
}Ciev_RoamIndT;

typedef enum
{
  Ciev_PinVerified = 0,
  Ciev_PinNotVerified = 1
}Ciev_UimPinStT;


typedef enum
{
  Ciev_SmsMemNotFull = 0,
  Ciev_SmsMemFull = 1
}Ciev_SmsMemFulIndT;


typedef enum
{
  Ciev_UimNotInsert = 0,
  Ciev_UimInsert = 1
}Ciev_UimInsertIndT;

/* Phone keys */
typedef enum
{
  VAL_KP_NO_KEY           = 0x00,     /* No Key                                  */
  VAL_KP_LEFT_SOFT_KEY    = 0x01,     /* value temporarily assigned, should be changed */
  VAL_KP_RIGHT_SOFT_KEY   = 0x02,     /* value temporarily assigned, should be changed */
  VAL_KP_VOLUME_UP_KEY    = 0x03,     /* value temporarily assigned, should be changed */
  VAL_KP_VOLUME_DOWN_KEY  = 0x04,     /* value temporarily assigned, should be changed */
  VAL_KP_LEFT_KEY         = 0x09,     /* Left arrow                              */
  VAL_KP_RIGHT_KEY        = 0x0A,     /* Right arrow                             */
  VAL_KP_MESSAGE_KEY      = 0x0B,     /* Shortcut to SMS/MMS/e-mail              */
  VAL_KP_POUND_KEY        = 0x23,     /* '#' key, ASCII '#'                      */
  VAL_KP_STAR_KEY         = 0x2A,     /* '*' key, ASCII '*'                      */
  VAL_KP_0_KEY            = 0x30,     /* '0' key, ASCII '0'                      */
  VAL_KP_1_KEY            = 0x31,     /* '1' key, ASCII '1'                      */
  VAL_KP_2_KEY            = 0x32,     /* '2' key, ASCII '2'                      */
  VAL_KP_3_KEY            = 0x33,     /* '3' key, ASCII '3'                      */
  VAL_KP_4_KEY            = 0x34,     /* '4' key, ASCII '4'                      */
  VAL_KP_5_KEY            = 0x35,     /* '5' key, ASCII '5'                      */
  VAL_KP_6_KEY            = 0x36,     /* '6' key, ASCII '6'                      */
  VAL_KP_7_KEY            = 0x37,     /* '7' key, ASCII '7'                      */
  VAL_KP_8_KEY            = 0x38,     /* '8' key, ASCII '8'                      */
  VAL_KP_9_KEY            = 0x39,     /* '9' key, ASCII '9'                      */
  VAL_KP_SEND_KEY         = 0x50,     /* Send key                                */
  VAL_KP_END_KEY          = 0x51,     /* End key or Power key (Based on Target)  */
  VAL_KP_CLR_KEY          = 0x52,     /* Clear key                               */
  VAL_KP_SELECT_KEY       = 0x53,     /* Select key                              */
  VAL_KP_UP_KEY           = 0x54,     /* Up-arrow key was pressed                */
  VAL_KP_DOWN_KEY         = 0x55,     /* Down-arrow key was pressed              */

  VAL_KP_MENU_KEY         = 0x56,     /* Menu key                                 */
  VAL_KP_SMS_KEY          = 0x57,     /* Reserve key                              */
  VAL_KP_PHONEBOOK_KEY    = 0x58,     /* Phonebook key                            */
  VAL_KP_RCL_KEY          = 0x59,     /* Rcl key                                  */
  VAL_KP_DND_MUTE_KEY     = 0x5A,     /* DND/MUTE key                             */
  VAL_KP_SPK_KEY          = 0x5B,     /* SPK key                                  */
  VAL_KP_HOOK_DOWN_KEY    = 0x5C,     /* Hook Down Key                            */
  VAL_KP_HOOK_UP_KEY      = 0x5D,     /* Hook Up Key                              */
  VAL_KP_HOOK_HOLD_KEY    = 0x5E,     /* Hook hold down key                       */

  VAL_KP_RECORD_KEY       = 0x5F,     /* Record Key */
  VAL_KP_FLIP_KEY         = 0x60,     /* Flip detect key */

  VAL_KP_CAMERA_KEY       = 0x61,     /* Camera snap shot key */
  VAL_KP_FUNCTION_KEY     = 0x62,     /* Special function key */

  VAL_KP_A_KEY            = 0x63,     /* 'A' dummy key   */
  VAL_KP_B_KEY            = 0x64,     /* 'B' dummy key   */
  VAL_KP_C_KEY            = 0x65,     /* 'C' dummy key   */
  VAL_KP_D_KEY            = 0x66,     /* 'D' dummy key   */

  VAL_KP_PWR_KEY          = 0x6F      /* Power key                                */
} ValKeypadKeyIdT;

typedef void (*UiAtCallback)(AtcSendAtMsgT *p);
typedef void (*UiAtNewSmsCallback)(void *p);

#ifdef MTK_CBP
extern ValVzWAPNInfoT ValVzWAPNInfoTable[MAX_APN_NUM];
#endif

/*------------------------------------------------------------------------
   Function declarations
  ------------------------------------------------------------------------*/
#ifdef __cplusplus
extern "C" {
#endif

SmsPreferredMemT MemPrefer( char* mem_str );
bool ascii2pdu(uint8* pdumsg, uint8* asciistr, uint32 pdulen);
bool pdu2ascii(uint8* asciistr, uint8* pdumsg, uint32 pdulen);
bool pbcd_to_ascii (uint8* p_dest, const uint8* p_src, uint8 numDigs);
bool ascii_to_pbcd(uint8* p_dest, const uint8* p_src, uint8 numDigs);
bool dialStrConvert(uint8* p_dest, const uint8* p_src);
bool isDialString(uint8* str);

uint32 CountPduLen (uint8* PduBuf, uint32 MaxLen);
char * GetEnhCmdRspBuf(uint8 chan, uint8 line);
void SendCmdRspMsgToAtc( void *MsgP);
void SendErrorToAtc(uint8 chan);
void SendOKToAtc(const char*   cmdName, uint8 chan);
void SendCmdCMEErrorToAtc (CmeErrorEnumT  errCode, uint8 chan);
void SendCmdCMSErrorToAtc (uint16  errCode, uint8 chan);
void  SendClccToAtc(uint8 id, uint8 dir,uint8 state, uint8 CallType,uint8 mpty,uint8* CallingNumber,uint8 NumType);
void SendDSDORMANTToAtc(uint8 dormantState);
void SendPppDORMANTToAtc(uint8 result);
uint8 getSmsBitbyState(ValSmsStatT state);
bool smsUserDataDecoding(uint8* p_dest, const uint8* p_src,
                        uint16 numFields, uint8 encoding);
bool smsUserDataEncoding(uint8* p_dest, const uint8* p_src,
                         uint16 numFields, uint8 encoding);
bool AT_SMS_Payload_Decode(uint8* p_dest, uint8* payload_len, const uint8* p_src);

ATParmInfo  *ValGetAtParmsPointer(void);
void SetWaitForRspACK(uint8 chan, bool value);
void MemsetEnhCmdRspBuf(uint8 chan, uint8 line, uint16 size);
uint8 GetRspLinesIndex(uint8 chan);
void SetRspLinesIndex(uint8 chan, uint8 value);
void sendUnslctResult(AtcSendAtRespMsgT*  pResult);

bool ValAtGetWaitForCNMA(void);
void ValAtSetWaitForCNMA(bool cnma);

#ifndef MTK_CBP
void ValATCmdRegister(UiAtCallback p, UiAtNewSmsCallback psms, AtCmdInfo *tabP, uint32 tabSize);
void ValATCmdUnregister(void);
#endif

#ifdef MTK_CBP
/* multiple unsolicited AT cmd rsp channel support */
typedef struct  AT_UNS_NODE_STRUCT
{
    struct AT_UNS_NODE_STRUCT  *cs_previous;
    struct AT_UNS_NODE_STRUCT  *cs_next;
    AtcSendAtRespMsgT          at_uns;
}  AT_UNS_NODE;

BOOL PutUnsRecord(AtcSendAtRespMsgT*  patUns, BOOL toHead);
BOOL GetUnsRecord(AT_UNS_NODE ** rec, uint8 chan);
void RemoveUnsRecord(uint8 chan);
void FlushUnsList(uint8 chan);
void ValEnhAtSendUnslctResult(AtcSendAtRespMsgT* pResult, uint8 uns_chan);
uint8 GetUnsRspNumInList(uint8 chan);
#endif

#ifdef __cplusplus
}
#endif /* __cplusplus */
ValAtPsStatusT ValAtGetPsStatus(void);
#endif

/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_zzeng_href16339\1 2011-01-13 08:05:43 GMT zzeng
** HREF#16339**/
/**Log information: \main\CBP7FeaturePhone\5 2011-03-17 12:26:24 GMT jinqi
** CBP7FeaturePhone_zzeng_href16339**/
/**Log information: \main\Trophy\Trophy_hjin_href21995\1 2013-03-07 06:37:49 GMT hjin
** HREF#21995:report Ciev_IccidReady_Ind when Iddid ready**/
/**Log information: \main\Trophy\1 2013-03-07 06:46:41 GMT cshen
** href#21995**/
/**Log information: \main\Trophy\Trophy_zjiang_href22086\1 2013-04-02 06:40:14 GMT zjiang
** HREF#22086.扩展AT+CIEV命令使之可以单独控制每个CIEV主动上报命令.**/
/**Log information: \main\Trophy\2 2013-04-02 06:46:25 GMT cshen
** href#22086**/
/**Log information: \main\Trophy\Trophy_0.3.X\1 2013-03-29 09:00:08 GMT fwu
** HREF#008278, Support uart to be the AT communication channel between AP and CP.**/
/**Log information: \main\Trophy\Trophy_fwu_href22082\1 2013-04-03 02:26:17 GMT fwu
** HREF#22082, Modified to support UART1 to be the AT channel between AP and CP.**/
/**Log information: \main\Trophy\3 2013-04-03 02:56:51 GMT hzhang
** HREF#22082 to merge code.**/
/**Log information: \main\Trophy\Trophy_ylxiao_href22158\1 2013-04-19 09:40:45 GMT ylxiao
** HREF#22158, remove compile warnings, fix bug**/
/**Log information: \main\Trophy\4 2013-04-22 01:48:38 GMT zlin
** HREF#22158. merge code.**/
/**Log information: \main\Trophy\Trophy_zjiang_href22230_fix1\1 2013-07-05 08:03:17 GMT zjiang
** HREF#22230**/
/**Log information: \main\Trophy\Trophy_zjiang_href22233\1 2013-07-08 02:39:56 GMT zjiang
** HREF#22233.修改CSQ上报默认门限为限为1.**/
/**Log information: \main\Trophy\6 2013-07-08 02:44:14 GMT cshen
** href#22233**/
/**Log information: \main\Trophy\Trophy_zjiang_href22286\1 2013-10-16 08:51:12 GMT zjiang
** HREF#22286.fix crts 21713.根据客户需求添加+VMEFL命令.**/
/**Log information: \main\Trophy\7 2013-10-16 09:01:48 GMT cshen
** href#22286**/
/**Log information: \main\Trophy\Trophy_zjiang_href22353\1 2014-01-09 07:07:06 GMT zjiang
** HREF#22353.增加主动上报+CIEV:130,<mode>用于上报模块工作模式改变.**/
/**Log information: \main\Trophy\9 2014-01-09 07:09:12 GMT cshen
** href#22353**/
/**Log information: \main\Trophy\Trophy_zjiang_href22375\1 2014-01-15 02:40:18 GMT zjiang
** HREF#22375.**/
/**Log information: \main\Trophy\10 2014-01-15 02:49:27 GMT cshen
** href#22375**/
