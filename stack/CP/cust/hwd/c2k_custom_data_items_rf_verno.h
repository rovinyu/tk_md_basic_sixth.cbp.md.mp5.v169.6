/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 * Filename:
 * ---------
 * c2k_custom_data_items_rf_verno.h
 *
 * Project:
 * --------
 *   C2K
 *
 * Description:
 * ------------
 *    This file defines logical data items verno related with RF stored in NVRAM
 *    Customer should add the verno number after modify the default value
 *
 *
 *****************************************************************************/
#ifndef __C2K_CUSTOM_DATA_ITEMS_RF_VERNO_H__
#define __C2K_CUSTOM_DATA_ITEMS_RF_VERNO_H__

#define NVRAM_EF_HWD_RF_CUST_DATA_LID_VERNO                                   "000"
#define NVRAM_EF_HWD_RF_CUST_TAS_LID_VERNO                                    "002"
#define NVRAM_EF_HWD_RF_CUST_LAST_LID_VERNO                                   "000"
#define NVRAM_EF_HWD_RF_CUST_BPI_MASK_LID_VERNO                               "000"
#define NVRAM_EF_HWD_RF_CUST_BPI_DATA_LID_VERNO                               "000"

#define NVRAM_EF_HWD_MIPI_PARAM_LID_VERNO                                     "000"
#define NVRAM_EF_HWD_MIPI_INITIAL_CW_LID_VERNO                                "000"
#define NVRAM_EF_HWD_MIPI_SLEEP_CW_LID_VERNO                                  "000"
#define NVRAM_EF_HWD_MIPI_USID_CHANGE_TABLE_LID_VERNO                         "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_RX_EVENT_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_RX_EVENT_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_RX_EVENT_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_RX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_B_MIPI_RX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_C_MIPI_RX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_A_MIPI_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_B_MIPI_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_C_MIPI_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_A_MIPI_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_B_MIPI_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_C_MIPI_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_A_MIPI_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
/*MIPI band D version define*/
#define NVRAM_EF_HWD_BAND_D_MIPI_RX_EVENT_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_D_MIPI_RX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_D_MIPI_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_D_MIPI_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_D_MIPI_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_D_MIPI_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
/*MIPI band E version define*/
#define NVRAM_EF_HWD_BAND_E_MIPI_RX_EVENT_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_E_MIPI_RX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_E_MIPI_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_E_MIPI_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_E_MIPI_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_E_MIPI_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_EVDO_LID_VERNO               "000"

#if defined(HWD_ETM_NV_ENABLE)
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
/*MIPI band D version define*/

#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
/*MIPI band E version define*/
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_EVENT_LID_VERNO                           "001"
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_DATA_LID_VERNO                            "001"
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_EVENT_LID_VERNO                          "000"
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_DATA_LID_VERNO                           "000"
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_1XRTT_LID_VERNO              "000"
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_EVDO_LID_VERNO               "000"
#endif


/*DRDI define */
#define NVRAM_EF_HWD_DRDI_VERNO                                               "000"
#define NVRAM_EF_HWD_DRDI_PARAM_LID_VERNO                                     NVRAM_EF_HWD_DRDI_VERNO
#define NVRAM_EF_HWD_DRDI_PARAM2_LID_VERNO                                    NVRAM_EF_HWD_DRDI_VERNO
#define NVRAM_EF_HWD_DRDI_REMAP_TABLE_LID_VERNO                               NVRAM_EF_HWD_DRDI_VERNO

#define NVRAM_EF_HWD_1X_ACC_TX_POWER_OFFSET_LID_VERNO                          "002"

#endif


