/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * c2k_custom_drdi.c
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * C file containing implementations pertaining
 * to the RF custom files.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/


HwdDrdiRemapTblT DRDI_REMAP_TABLE =
{
    {  /* 00  ~  (C2K_CUSTOM_TOTAL_SET_NUMS-1) */
       /* 00 */
           0
#if IS_C2K_DRDI_ENABLE
       /*      01,  02,  03,  04,  05,  06,  07,  08,  09, */
            , 127,   1,   1,   1,   1, 127, 127,   2, 127,
       /* 10,  11,  12,  13,  14,  15,  16,  17,  18,  19, */
           2,   2,  12,  13,  14,  15,  16,  17,  18,  19,
       /* 20,  21,  22,  23,  24,  25,  26,  27,  28,  29, */
          20,  21,  22,  23,  24,  25,  26,  27,  28,  29,
       /* 30,  31,  32,  33,  34,  35,  36,  37,  38,  39, */
          30,  31,  32,  33,  34,  35,  36,  37,  38,  39,
       /* 40,  41,  42,  43,  44,  45,  46,  47,  48,  49, */
          40,  41,  42,  43,  44,  45,  46,  47,  48,  49,
       /* 50,  51,  52,  53,  54,  55,  56,  57,  58,  59, */
          50,  51,  52,  53,  54,  55,  56,  57,  58,  59,
       /* 60,  61,  62,  63,  64,  65,  66,  67,  68,  69, */
          60,  61,  62,  63,  64,  65,  66,  67,  68,  69,
       /* 70,  71,  72,  73,  74,  75,  76,  77,  78,  79, */
          70,  71,  72,  73,  74,  75,  76,  77,  78,  79,
       /* 80,  81,  82,  83,  84,  85,  86,  87,  88,  89, */
          80,  81,  82,  83,  84,  85,  86,  87,  88,  89,
       /* 90,  91,  92,  93,  94,  95,  96,  97,  98,  99, */
          90,  91,  92,  93,  94,  95,  96,  97,  98,  99,
       /*100, 101, 102, 103, 104, 105, 106, 107, 108, 109, */
         100, 101, 102, 103, 104, 105, 106, 107, 108, 109,
       /*110, 111, 112, 113, 114, 115, 116, 117, 118, 119, */
         110, 111, 112, 113, 114, 115, 116, 117, 118, 119,
       /*120, 121, 122, 123, 124, 125, 126, 127, 128, 129, */
         120, 121, 122, 123, 124, 125, 126, 127
#endif
    }
};

