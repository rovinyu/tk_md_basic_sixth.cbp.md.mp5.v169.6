/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * c2k_custom_rf.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF custom files.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _C2K_CUSTOM_RF_Set0_H
#define _C2K_CUSTOM_RF_Set0_H
/************************************************************/
/* Supported Band Class .                                   */
/************************************************************/
#define  CUST_BAND_A_Set0            SYS_BAND_CLASS_0
#define  CUST_BAND_B_Set0            SYS_BAND_CLASS_1
#define  CUST_BAND_C_Set0            SYS_BAND_CLASS_10
#define  CUST_BAND_D_Set0            SYS_BAND_CLASS_NOT_USED
#define  CUST_BAND_E_Set0            SYS_BAND_CLASS_NOT_USED
#define  BAND_A_SUPPORTED_Set0       TRUE
#define  BAND_B_SUPPORTED_Set0       TRUE
#define  BAND_C_SUPPORTED_Set0       TRUE
#define  BAND_D_SUPPORTED_Set0       FALSE
#define  BAND_E_SUPPORTED_Set0       FALSE

/************************************************************/
/* Define your own band selection on one of 3 LNA ports.    */
/* There are two high band and one low band to choose.      */
/* All options are listed below:                            */
/* LNA_HB_1/LNA_HB_2/LNA_LB_1/NON_USED_BAND                 */
/************************************************************/
#define  BAND_A_RX_IO_SEL_Set0      PRX3
#define  BAND_B_RX_IO_SEL_Set0      PRX10
#define  BAND_C_RX_IO_SEL_Set0      PRX3
#define  BAND_D_RX_IO_SEL_Set0      NONE_USED_PRX
#define  BAND_E_RX_IO_SEL_Set0      NONE_USED_PRX

#define  BAND_A_RXD_IO_SEL_Set0     DRX3
#define  BAND_B_RXD_IO_SEL_Set0     DRX13
#define  BAND_C_RXD_IO_SEL_Set0     DRX3
#define  BAND_D_RXD_IO_SEL_Set0     NONE_USED_DRX
#define  BAND_E_RXD_IO_SEL_Set0     NONE_USED_DRX

/************************************************************/
/* Define your own tx output selection                      */
/* There are two high band and one low band to choose.      */
/* All options are listed below:                            */
/* TX_HB_1/TX_HB_2/TX_LB_1/TX_NULL_BAND    */
/************************************************************/
#define  BAND_A_TX_IO_SEL_Set0      TX_LB4
#define  BAND_B_TX_IO_SEL_Set0      TX_MB2
#define  BAND_C_TX_IO_SEL_Set0      TX_LB4
#define  BAND_D_TX_IO_SEL_Set0      TX_NULL_BAND
#define  BAND_E_TX_IO_SEL_Set0      TX_NULL_BAND

/************************************************************/
/* Define your own tx detect port selection                 */
/* There are two detect paths to choose                     */
/* All options are listed below:                            */
/* TX_DET_IO_DET1/TX_DET_IO_DET2/TX_DET_IO_NULL             */
/************************************************************/
#define BAND_A_TX_DET_IO_SEL_Set0   TX_DET_IO_DET1
#define BAND_B_TX_DET_IO_SEL_Set0   TX_DET_IO_DET1
#define BAND_C_TX_DET_IO_SEL_Set0   TX_DET_IO_DET1
#define BAND_D_TX_DET_IO_SEL_Set0   TX_DET_IO_NULL
#define BAND_E_TX_DET_IO_SEL_Set0   TX_DET_IO_NULL

/************************************************************/
/* Event timing parameters in us resolution                 */
/************************************************************/
#define  TC_PR1_Set0                  (0)
#define  TC_PR2_Set0                  (0)
#define  TC_PR2B_Set0                 (0)
#define  TC_PR3_Set0                  (0)

#define  TC_PT1_Set0                  (0)
#define  TC_PT2_Set0                  (0)
#define  TC_PT2B_Set0                 (0)
#define  TC_PT3_Set0                  (0)

#define  TC_PTG1_Set0                 (5)
#define  TC_PTG2_Set0                 (0)
#define  TC_PTG2B_Set0                (0)
#define  TC_PTG3_Set0                 (5)

#define  TC_PTPC1_Set0                (5)
#define  TC_PTPC2_Set0                (0)
#define  TC_PTPC2B_Set0               (0)

#define  DC2DC_OFFSET_Set0           (25)

/*------------------------------------*/
/* BPI function for Orion+ in Jade    */
/* (shall be modified to confirm to   */
/*  the actual design)                */
/*------------------------------------*/
/* PRCB : bit  BPI   pin function     */
/*         0    0    (NC)             */
/*         1    1    (NC)             */
/*         2    2    (NC)             */
/*         3    3    (NC)             */
/*         4    4    (NC)             */
/*         5    5    (NC)             */
/*         6    6    (NC)             */
/* ---------------------------------- */
/*         7    7    DRX ASM V1       */
/*         8    8    DRX ASM V2       */
/*         9    9    DRX ASM V3       */
/*         10   10   (NC)             */
/*         11   11   (NC)             */
/*         12   12   DRX Tuner 1      */
/*         13   13   DRX Tuner 2      */
/*         14   14   DRX Tuner 3      */
/*         15   15   DRX Tuner 4      */
/*         16   16   (NC)             */
/*         17   17   (NC)             */
/*         18   18   M/LB DPDT        */
/*         19   19   (NC)             */
/*         20   20   (NC)             */
/*         21   21   (NC)             */
/*         22   22   PDET Switch      */
/* ---------------------------------- */
/*         23   23   Reserved         */
/*         24   24   Reserved         */
/*         25   25   Reserved         */
/*         26   26   Reserved         */
/*         27   27   Reserved         */
/*         28   28   Reserved         */
/*         29   29   Reserved         */
/*         30   30   Reserved         */
/*         31   31   Reserved         */
/*------------------------------------*/
/*------------------------------------------------------------------ */
/* BPI mask and data.                                                */
/*-------------------------------------------------------------------*/
/* -------- PDATA_BAND_A Start ---------*/
#define  PMASK_BAND_A_PR1_Set0       0x0004C000
#define  PDATA_BAND_A_PR1_Set0       0x00048000
#define  PMASK_BAND_A_PR2_Set0       0x0004C000
#define  PDATA_BAND_A_PR2_Set0       0x00048000
#define  PMASK_BAND_A_PR2B_Set0      0x0004C000
#define  PDATA_BAND_A_PR2B_Set0      0x00048000
#define  PMASK_BAND_A_PR3_Set0       0x0004C000
#define  PDATA_BAND_A_PR3_Set0       0x00040000
#define  PMASK_BAND_A_PT1_Set0       0x0044C000
#define  PDATA_BAND_A_PT1_Set0       0x00048000
#define  PMASK_BAND_A_PT2_Set0       0x0044C000
#define  PDATA_BAND_A_PT2_Set0       0x00048000
#define  PMASK_BAND_A_PT2B_Set0      0x0044C000
#define  PDATA_BAND_A_PT2B_Set0      0x00048000
#define  PMASK_BAND_A_PT3_Set0       0x00440000
#define  PDATA_BAND_A_PT3_Set0       0x00040000
/* -------- PDATA_BAND_A End ---------*/
/* -------- PDATA_BAND_A RXD Start ---*/
#define  PMASK2_BAND_A_PR1_Set0      0x0004F380
#define  PDATA2_BAND_A_PR1_Set0      0x00048080
#define  PMASK2_BAND_A_PR2_Set0      0x0004F380
#define  PDATA2_BAND_A_PR2_Set0      0x00048080
#define  PMASK2_BAND_A_PR2B_Set0     0x0004F380
#define  PDATA2_BAND_A_PR2B_Set0     0x00048080
#define  PMASK2_BAND_A_PR3_Set0      0x0004F380
#define  PDATA2_BAND_A_PR3_Set0      0x00040000
/* -------- PDATA_BAND_A RXD End -----*/
/* -------- PDATA_BAND_A_TXGATE Start -----*/
#define  PMASK_BAND_A_PTG1_Set0     0x00000000
#define  PDATA_BAND_A_PTG1_Set0     0x00000000
#define  PMASK_BAND_A_PTG2_Set0     0x00000000
#define  PDATA_BAND_A_PTG2_Set0     0x00000000
#define  PMASK_BAND_A_PTG2B_Set0    0x00000000
#define  PDATA_BAND_A_PTG2B_Set0    0x00000000
#define  PMASK_BAND_A_PTG3_Set0     0x00000000
#define  PDATA_BAND_A_PTG3_Set0     0x00000000
/* -------- PDATA_BAND_A_TXGATE End  -----*/

/* -------- PDATA_BAND_B Start ---------*/
#define  PMASK_BAND_B_PR1_Set0       0x0004C000
#define  PDATA_BAND_B_PR1_Set0       0x00040000
#define  PMASK_BAND_B_PR2_Set0       0x0004C000
#define  PDATA_BAND_B_PR2_Set0       0x00040000
#define  PMASK_BAND_B_PR2B_Set0      0x0004C000
#define  PDATA_BAND_B_PR2B_Set0      0x00040000
#define  PMASK_BAND_B_PR3_Set0       0x0004C000
#define  PDATA_BAND_B_PR3_Set0       0x00040000
#define  PMASK_BAND_B_PT1_Set0       0x0044C000
#define  PDATA_BAND_B_PT1_Set0       0x00040000
#define  PMASK_BAND_B_PT2_Set0       0x0044C000
#define  PDATA_BAND_B_PT2_Set0       0x00040000
#define  PMASK_BAND_B_PT2B_Set0      0x0044C000
#define  PDATA_BAND_B_PT2B_Set0      0x00040000
#define  PMASK_BAND_B_PT3_Set0       0x00440000
#define  PDATA_BAND_B_PT3_Set0       0x00040000
/* -------- PDATA_BAND_B End ---------*/
/* -------- PDATA_BAND_B RXD Start ---*/
#define  PMASK2_BAND_B_PR1_Set0      0x0004F380
#define  PDATA2_BAND_B_PR1_Set0      0x00040280
#define  PMASK2_BAND_B_PR2_Set0      0x0004F380
#define  PDATA2_BAND_B_PR2_Set0      0x00040280
#define  PMASK2_BAND_B_PR2B_Set0     0x0004F380
#define  PDATA2_BAND_B_PR2B_Set0     0x00040280
#define  PMASK2_BAND_B_PR3_Set0      0x0004F380
#define  PDATA2_BAND_B_PR3_Set0      0x00040000
/* -------- PDATA_BAND_B RXD End -----*/
/* -------- PDATA_BAND_B_TXGATE Start -----*/
#define  PMASK_BAND_B_PTG1_Set0     0x00000000
#define  PDATA_BAND_B_PTG1_Set0     0x00000000
#define  PMASK_BAND_B_PTG2_Set0     0x00000000
#define  PDATA_BAND_B_PTG2_Set0     0x00000000
#define  PMASK_BAND_B_PTG2B_Set0    0x00000000
#define  PDATA_BAND_B_PTG2B_Set0    0x00000000
#define  PMASK_BAND_B_PTG3_Set0     0x00000000
#define  PDATA_BAND_B_PTG3_Set0     0x00000000
/* -------- PDATA_BAND_B_TXGATE End  -----*/

/* -------- PDATA_BAND_C Start ---------*/
#define  PMASK_BAND_C_PR1_Set0       0x0004C000
#define  PDATA_BAND_C_PR1_Set0       0x00048000
#define  PMASK_BAND_C_PR2_Set0       0x0004C000
#define  PDATA_BAND_C_PR2_Set0       0x00048000
#define  PMASK_BAND_C_PR2B_Set0      0x0004C000
#define  PDATA_BAND_C_PR2B_Set0      0x00048000
#define  PMASK_BAND_C_PR3_Set0       0x0004C000
#define  PDATA_BAND_C_PR3_Set0       0x00040000
#define  PMASK_BAND_C_PT1_Set0       0x0044C000
#define  PDATA_BAND_C_PT1_Set0       0x00048000
#define  PMASK_BAND_C_PT2_Set0       0x0044C000
#define  PDATA_BAND_C_PT2_Set0       0x00048000
#define  PMASK_BAND_C_PT2B_Set0      0x0044C000
#define  PDATA_BAND_C_PT2B_Set0      0x00048000
#define  PMASK_BAND_C_PT3_Set0       0x00440000
#define  PDATA_BAND_C_PT3_Set0       0x00040000
/* -------- PDATA_BAND_C End ---------*/
/* -------- PDATA_BAND_C RXD Start ---*/
#define  PMASK2_BAND_C_PR1_Set0      0x0004F380
#define  PDATA2_BAND_C_PR1_Set0      0x00048080
#define  PMASK2_BAND_C_PR2_Set0      0x0004F380
#define  PDATA2_BAND_C_PR2_Set0      0x00048080
#define  PMASK2_BAND_C_PR2B_Set0     0x0004F380
#define  PDATA2_BAND_C_PR2B_Set0     0x00048080
#define  PMASK2_BAND_C_PR3_Set0      0x0004F380
#define  PDATA2_BAND_C_PR3_Set0      0x00040000
/* -------- PDATA_BAND_C RXD End -----*/
/* -------- PDATA_BAND_C_TXGATE Start -----*/
#define  PMASK_BAND_C_PTG1_Set0     0x00000000
#define  PDATA_BAND_C_PTG1_Set0     0x00000000
#define  PMASK_BAND_C_PTG2_Set0     0x00000000
#define  PDATA_BAND_C_PTG2_Set0     0x00000000
#define  PMASK_BAND_C_PTG2B_Set0    0x00000000
#define  PDATA_BAND_C_PTG2B_Set0    0x00000000
#define  PMASK_BAND_C_PTG3_Set0     0x00000000
#define  PDATA_BAND_C_PTG3_Set0     0x00000000
/* -------- PDATA_BAND_C_TXGATE End  -----*/

/* -------- PDATA_BAND_D Start ---------*/
#define  PMASK_BAND_D_PR1_Set0       0x00000000
#define  PDATA_BAND_D_PR1_Set0       0x00000000
#define  PMASK_BAND_D_PR2_Set0       0x00000000
#define  PDATA_BAND_D_PR2_Set0       0x00000000
#define  PMASK_BAND_D_PR2B_Set0      0x00000000
#define  PDATA_BAND_D_PR2B_Set0      0x00000000
#define  PMASK_BAND_D_PR3_Set0       0x00000000
#define  PDATA_BAND_D_PR3_Set0       0x00000000
#define  PMASK_BAND_D_PT1_Set0       0x00000000
#define  PDATA_BAND_D_PT1_Set0       0x00000000
#define  PMASK_BAND_D_PT2_Set0       0x00000000
#define  PDATA_BAND_D_PT2_Set0       0x00000000
#define  PMASK_BAND_D_PT2B_Set0      0x00000000
#define  PDATA_BAND_D_PT2B_Set0      0x00000000
#define  PMASK_BAND_D_PT3_Set0       0x00000000
#define  PDATA_BAND_D_PT3_Set0       0x00000000
/* -------- PDATA_BAND_D End ---------*/
/* -------- PDATA_BAND_D RXD Start ---*/
#define  PMASK2_BAND_D_PR1_Set0      0x00000000
#define  PDATA2_BAND_D_PR1_Set0      0x00000000
#define  PMASK2_BAND_D_PR2_Set0      0x00000000
#define  PDATA2_BAND_D_PR2_Set0      0x00000000
#define  PMASK2_BAND_D_PR2B_Set0     0x00000000
#define  PDATA2_BAND_D_PR2B_Set0     0x00000000
#define  PMASK2_BAND_D_PR3_Set0      0x00000000
#define  PDATA2_BAND_D_PR3_Set0      0x00000000
/* -------- PDATA_BAND_D RXD End -----*/
/* -------- PDATA_BAND_D_TXGATE Start -----*/
#define  PMASK_BAND_D_PTG1_Set0     0x00000000
#define  PDATA_BAND_D_PTG1_Set0     0x00000000
#define  PMASK_BAND_D_PTG2_Set0     0x00000000
#define  PDATA_BAND_D_PTG2_Set0     0x00000000
#define  PMASK_BAND_D_PTG2B_Set0    0x00000000
#define  PDATA_BAND_D_PTG2B_Set0    0x00000000
#define  PMASK_BAND_D_PTG3_Set0     0x00000000
#define  PDATA_BAND_D_PTG3_Set0     0x00000000
/* -------- PDATA_BAND_D_TXGATE End  -----*/

/* -------- PDATA_BAND_E Start ---------*/
#define  PMASK_BAND_E_PR1_Set0       0x00000000
#define  PDATA_BAND_E_PR1_Set0       0x00000000
#define  PMASK_BAND_E_PR2_Set0       0x00000000
#define  PDATA_BAND_E_PR2_Set0       0x00000000
#define  PMASK_BAND_E_PR2B_Set0      0x00000000
#define  PDATA_BAND_E_PR2B_Set0      0x00000000
#define  PMASK_BAND_E_PR3_Set0       0x00000000
#define  PDATA_BAND_E_PR3_Set0       0x00000000
#define  PMASK_BAND_E_PT1_Set0       0x00000000
#define  PDATA_BAND_E_PT1_Set0       0x00000000
#define  PMASK_BAND_E_PT2_Set0       0x00000000
#define  PDATA_BAND_E_PT2_Set0       0x00000000
#define  PMASK_BAND_E_PT2B_Set0      0x00000000
#define  PDATA_BAND_E_PT2B_Set0      0x00000000
#define  PMASK_BAND_E_PT3_Set0       0x00000000
#define  PDATA_BAND_E_PT3_Set0       0x00000000
/* -------- PDATA_BAND_E End ---------*/
/* -------- PDATA_BAND_E RXD Start ---*/
#define  PMASK2_BAND_E_PR1_Set0      0x00000000
#define  PDATA2_BAND_E_PR1_Set0      0x00000000
#define  PMASK2_BAND_E_PR2_Set0      0x00000000
#define  PDATA2_BAND_E_PR2_Set0      0x00000000
#define  PMASK2_BAND_E_PR2B_Set0     0x00000000
#define  PDATA2_BAND_E_PR2B_Set0     0x00000000
#define  PMASK2_BAND_E_PR3_Set0      0x00000000
#define  PDATA2_BAND_E_PR3_Set0      0x00000000
/* -------- PDATA_BAND_E RXD End -----*/
/* -------- PDATA_BAND_E_TXGATE Start -----*/
#define  PMASK_BAND_E_PTG1_Set0     0x00000000
#define  PDATA_BAND_E_PTG1_Set0     0x00000000
#define  PMASK_BAND_E_PTG2_Set0     0x00000000
#define  PDATA_BAND_E_PTG2_Set0     0x00000000
#define  PMASK_BAND_E_PTG2B_Set0    0x00000000
#define  PDATA_BAND_E_PTG2B_Set0    0x00000000
#define  PMASK_BAND_E_PTG3_Set0     0x00000000
#define  PDATA_BAND_E_PTG3_Set0     0x00000000
/* -------- PDATA_BAND_E_TXGATE End  -----*/


/* -------- PDATA TPC Start -----*/
#define  PMASK_PTPC1_Set0           0x00000000
#define  PDATA_PTPC1_Set0           0x00000000
#define  PMASK_PTPC2_Set0           0x00000000
#define  PDATA_PTPC2_Set0           0x00000000
#define  PMASK_PTPC2B_Set0          0x00000000
#define  PDATA_PTPC2B_Set0          0x00000000
/* -------- PDATA TPC End  -----*/


/*------------------------------------------------------------------ */
/* C2K Transmit Antenna Switch feature (TAS)                         */
/*-------------------------------------------------------------------*/
#define  C2K_TAS_ENABLE_Set0                 0                    /* 1/0: enable/disable the TAS feature */

#define  C2K_TAS_TEST_SIM_ENABLE_Set0        0                    /* 1/0: enable/disable TAS when using test SIM */

#define  C2K_TAS_BPI_PIN1_Set0               C2K_TAS_BPI_PIN_NULL  /* The 1st BPI pin number for TAS */
#define  C2K_TAS_BPI_PIN2_Set0               C2K_TAS_BPI_PIN_NULL /* The 2nd BPI pin number for TAS */
#define  C2K_TAS_BPI_PIN3_Set0               C2K_TAS_BPI_PIN_NULL /* The 3rd BPI pin number for TAS */
#define  C2K_TAS_BPI_MASK_Set0               C2K_TAS_BPI_PIN_GEN(1/*PIN1 En*/, 1/*PIN_2 En*/, 1/*PIN_3 En*/, Set0)

#define  C2K_TAS_INIT_ANT_Set0               0  /* The Initial TAS antenna index,
                                              0  : The settings of PDATA_BAND_x_Pxx (original ones)
                                              1~7: The settings of PDATA_BAND_x_TAS1~7 */
#define  C2K_FORCE_TX_ANTENNA_ENABLE_Set0    0  /* 1/0: Force the antenna index or not */
#define  C2K_FORCE_TX_ANTENNA_IDX_Set0       0  /* Force to which antenna index */

/* -------- PDATA_BAND_A_TAS Start -------*/
#define  PDATA_BAND_A_TAS1_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_A_TAS2_Set0              C2K_TAS_BPI_PIN_GEN(1, 0, 0, Set0)
#define  PDATA_BAND_A_TAS3_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_A_TAS4_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_A_TAS5_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_A_TAS6_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_A_TAS7_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)

/* -------- PDATA_BAND_B_TAS Start -------*/
#define  PDATA_BAND_B_TAS1_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_B_TAS2_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)    
#define  PDATA_BAND_B_TAS3_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_B_TAS4_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_B_TAS5_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_B_TAS6_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_B_TAS7_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)

/* -------- PDATA_BAND_C_TAS Start -------*/
#define  PDATA_BAND_C_TAS1_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_C_TAS2_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_C_TAS3_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_C_TAS4_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_C_TAS5_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_C_TAS6_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_C_TAS7_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)

/* -------- PDATA_BAND_D_TAS Start -------*/
#define  PDATA_BAND_D_TAS1_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_D_TAS2_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_D_TAS3_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_D_TAS4_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_D_TAS5_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_D_TAS6_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_D_TAS7_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)

/* -------- PDATA_BAND_E_TAS Start -------*/
#define  PDATA_BAND_E_TAS1_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_E_TAS2_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_E_TAS3_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_E_TAS4_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_E_TAS5_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_E_TAS6_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)
#define  PDATA_BAND_E_TAS7_Set0              C2K_TAS_BPI_PIN_GEN(0, 0, 0, Set0)

/* TAS init index on test SIM */
#define  ANT_IDX_ON_TEST_SIM_BAND_A_Set0           1
#define  ANT_IDX_ON_TEST_SIM_BAND_B_Set0           1
#define  ANT_IDX_ON_TEST_SIM_BAND_C_Set0           1
#define  ANT_IDX_ON_TEST_SIM_BAND_D_Set0           1
#define  ANT_IDX_ON_TEST_SIM_BAND_E_Set0           1
/*******************************************************/
/* Rx diversity path enable.                           */
/* TRUE - RX diversity path is enabled.                */
/* FALSE - RX diversity path is disabled.              */
/*******************************************************/
#define  RX_DIVERSITY_ENABLE_Set0        TRUE

/*******************************************************/
/* For RXD sigle test, customer may use the RXD only.  */
/* TRUE -- Only use RX diversity path for testing.     */
/*******************************************************/
#define  RX_DIVERSITY_ONLY_TEST_Set0     FALSE

/*******************************************************/
/* PA Vdd is controlled by DC2DC or not.               */
/* TRUE - PA Vdd is controlled by DC2DC.               */
/* FALSE - PA Vdd is not controlled by DC2DC.          */
/*******************************************************/
#define  PA_VDD_DC2DC_ENABLE_Set0        FALSE

/*******************************************************/
/* PA Vdd is controlled by PMU or not.                 */
/* TRUE - PA Vdd is controlled by PMU.                 */
/* FALSE - PA Vdd is not controlled by PMU.            */
/*******************************************************/
#define  PA_VDD_PMU_ENABLE_Set0          TRUE

/*******************************************************/
/* PA Vdd is supplied by battery or not.               */
/* TRUE - PA Vdd is supplied by battery.               */
/* FALSE - PA Vdd is not supplied by battery.          */
/*******************************************************/
#define  PA_VDD_BATT_ENABLE_Set0         FALSE

/*******************************************************/
/* For the RF chip internal temperature measurement.   */
/* TRUE - use RF internal ADC to measure.              */
/* FALSE - do not use RF internal ADC to measure.      */
/*******************************************************/
#define  TEMPERATURE_MEAS_EN_Set0        TRUE

#endif /* _C2K_CUSTOM_RF_Set0_H */
/*----------------------------------------------------------------------------
 End of File
----------------------------------------------------------------------------*/

