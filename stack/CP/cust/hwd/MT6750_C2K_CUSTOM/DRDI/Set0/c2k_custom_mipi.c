/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained`
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * c2k_custom_mipi.c
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * C file containing implementations pertaining
 * to the MIPI custom files.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/


/***************************************************************************
* MIPI Initial CW Table Data
***************************************************************************/
MIPI_IMM_DATA_TABLE_T MIPI_INITIAL_CW_Set0[] =
{
/* elm type    , port_sel       , data_seq  ,    USID          , addr , data  , wait_time(us) */
//		{MIPI_ASM, MIPI_PORT0, REG_W, MIPI_USID_INIT0, {0x1C, 0x38}, 0 }, //Broadcast ID, Standard MIPI, PM_TRIG = normal mode
    {MIPI_NULL, 0,0,0,{0,0},0},
};


/***************************************************************************
 * MIPI Change USDI Table Data
 ***************************************************************************/
MIPI_USID_CHANGE_T MIPI_USID_CHANGE_TABLE_Set0[] =
{
    // USID change type   , port_sel        , current USID , PRODUCT_ID , MANUFACTORY_ID   new USID
    //{USID_REG_W,          , MIPI_PORT0      , 0xF          , 0x85       , 0x1A5         ,  0xE },
    {USID_REG_W           , MIPI_PORT1 , 0xF          , 0x0        , 0x1A5          , 0xC     },
    {USID_NULL,0,0,0,0,0},
};


/***************************************************************************
* MIPI Sleep CW Table Data
***************************************************************************/
MIPI_IMM_DATA_TABLE_T MIPI_SLEEP_CW_Set0[] =
{
    // elm type   , port_sel      , data_seq ,    USID          , addr , data  , wait_time(us)
//		{MIPI_ASM, MIPI_PORT0, REG_W, MIPI_USID_INIT0, {0x1C, 0xb8}, 0 }, //Broadcast ID, Standard MIPI, PM_TRIG = low power mode
    {MIPI_NULL, 0,0,0,{0,0},0},
};


/***************************************************************************
* MIPI RX Event and CW Table
***************************************************************************/
/* Rx Events */
MIPI_EVENT_TABLE_T BAND_A_MIPI_RX_EVENT_Set0[] =
{
    /* No.     elm type  , data idx       , evt_type      , evt_offset             */
    /*                    { start, stop },                  ( us )                 */
    { /* 0  */ MIPI_ASM , { 0    , 4    }, MIPI_TRX_ON    , MIPI_ASM_RX_ON0_Set0 },
    { /* 1  */ MIPI_ASM , { 5    , 6    }, MIPI_TRX_OFF   , MIPI_ASM_RX_OFF0_Set0},
    { /* 2  */ MIPI_ASM , { 7    , 9    }, MIPI_RX_DIV_ON , MIPI_ASM_DRX_ON0_Set0},
    { /* 3  */ MIPI_ASM , { 10   , 11   }, MIPI_RX_DIV_OFF, MIPI_ASM_DRX_OFF0_Set0},
    { /* 4  */ MIPI_NULL, { 0    , 0    }, MIPI_EVENT_NULL, 0},
};

MIPI_EVENT_TABLE_T BAND_B_MIPI_RX_EVENT_Set0[] =
{
    /* No.     elm type  , data idx       , evt_type      , evt_offset             */
    /*                    { start, stop },                  ( us )                 */
    { /* 0  */ MIPI_ASM , { 0    , 4    }, MIPI_TRX_ON    , MIPI_ASM_RX_ON0_Set0 },
    { /* 1  */ MIPI_ASM , { 5    , 6    }, MIPI_TRX_OFF   , MIPI_ASM_RX_OFF0_Set0},
    { /* 2  */ MIPI_ASM , { 7    , 9    }, MIPI_RX_DIV_ON , MIPI_ASM_DRX_ON0_Set0},
    { /* 3  */ MIPI_ASM , { 10   , 11   }, MIPI_RX_DIV_OFF, MIPI_ASM_DRX_OFF0_Set0},
    { /* 4  */ MIPI_NULL, { 0    , 0    }, MIPI_EVENT_NULL, 0},
};

MIPI_EVENT_TABLE_T BAND_C_MIPI_RX_EVENT_Set0[] =
{
    /* No.     elm type  , data idx       , evt_type      , evt_offset             */
    /*                    { start, stop },                  ( us )                 */
    { /* 0  */ MIPI_ASM , { 0    , 4    }, MIPI_TRX_ON    , MIPI_ASM_RX_ON0_Set0 },
    { /* 1  */ MIPI_ASM , { 5    , 6    }, MIPI_TRX_OFF   , MIPI_ASM_RX_OFF0_Set0},
    { /* 2  */ MIPI_ASM , { 7    , 9    }, MIPI_RX_DIV_ON , MIPI_ASM_DRX_ON0_Set0},
    { /* 3  */ MIPI_ASM , { 10   , 11   }, MIPI_RX_DIV_OFF, MIPI_ASM_DRX_OFF0_Set0},
    { /* 4  */ MIPI_NULL, { 0    , 0    }, MIPI_EVENT_NULL, 0},
  
};

MIPI_EVENT_TABLE_T BAND_D_MIPI_RX_EVENT_Set0[] =
{
    /* No.     elm type  , data idx       , evt_type      , evt_offset             */
    /*                    { start, stop },                  ( us )                 */
    { /* 0  */ MIPI_ASM , { 0    , 1    }, MIPI_TRX_ON    , MIPI_ASM_RX_ON0_Set0 },
    { /* 1  */ MIPI_ASM , { 2    , 3    }, MIPI_TRX_OFF   , MIPI_ASM_RX_OFF0_Set0},
    { /* 3  */ MIPI_NULL, { 0    , 0    }, MIPI_EVENT_NULL, 0},
};

MIPI_EVENT_TABLE_T BAND_E_MIPI_RX_EVENT_Set0[] =
{
    /* No.     elm type  , data idx       , evt_type      , evt_offset             */
    /*                    { start, stop },                  ( us )                 */
    { /* 0  */ MIPI_ASM , { 0    , 1    }, MIPI_TRX_ON    , MIPI_ASM_RX_ON0_Set0 },
    { /* 1  */ MIPI_ASM , { 2    , 3    }, MIPI_TRX_OFF   , MIPI_ASM_RX_OFF0_Set0},
    { /* 3  */ MIPI_NULL, { 0    , 0    }, MIPI_EVENT_NULL, 0},
};

/* Rx Data */
MIPI_DATA_SUBBAND_TABLE_T BAND_A_MIPI_RX_DATA_Set0[] =
{
   //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8600  /*100 kHz*/ , {0x1C ,0x38} }, { 8668  /*100 kHz*/ , {0x1C ,0x38} }, { 8736  /*100 kHz*/ , {0x1C ,0x38} }, { 8804  /*100 kHz*/ , {0x1C ,0x38} }, { 8872  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 1  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8600  /*100 kHz*/ , {0x00 ,0x80} }, { 8668  /*100 kHz*/ , {0x00 ,0x80} }, { 8736  /*100 kHz*/ , {0x00 ,0x80} }, { 8804  /*100 kHz*/ , {0x00 ,0x80} }, { 8872  /*100 kHz*/ , {0x00 ,0x80} },} },
   { /* 2  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8600  /*100 kHz*/ , {0x05 ,0x00} }, { 8668  /*100 kHz*/ , {0x05 ,0x00} }, { 8736  /*100 kHz*/ , {0x05 ,0x00} }, { 8804  /*100 kHz*/ , {0x05 ,0x00} }, { 8872  /*100 kHz*/ , {0x05 ,0x00} },} },
   { /* 3  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8600  /*100 kHz*/ , {0x03 ,0x00} }, { 8668  /*100 kHz*/ , {0x03 ,0x00} }, { 8736  /*100 kHz*/ , {0x03 ,0x00} }, { 8804  /*100 kHz*/ , {0x03 ,0x00} }, { 8872  /*100 kHz*/ , {0x03 ,0x00} },} },
   { /* 4  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8600  /*100 kHz*/ , {0x02 ,0x04} }, { 8668  /*100 kHz*/ , {0x02 ,0x04} }, { 8736  /*100 kHz*/ , {0x02 ,0x04} }, { 8804  /*100 kHz*/ , {0x02 ,0x04} }, { 8872  /*100 kHz*/ , {0x02 ,0x04} },} },
   { /* 5  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8600  /*100 kHz*/ , {0x02 ,0x00} }, { 8668  /*100 kHz*/ , {0x02 ,0x00} }, { 8736  /*100 kHz*/ , {0x02 ,0x00} }, { 8804  /*100 kHz*/ , {0x02 ,0x00} }, { 8872  /*100 kHz*/ , {0x02 ,0x00} },} },
   { /* 6  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8600  /*100 kHz*/ , {0x1C ,0x38} }, { 8668  /*100 kHz*/ , {0x1C ,0x38} }, { 8736  /*100 kHz*/ , {0x1C ,0x38} }, { 8804  /*100 kHz*/ , {0x1C ,0x38} }, { 8872  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 7  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8600  /*100 kHz*/ , {0x1C ,0x38} }, { 8668  /*100 kHz*/ , {0x1C ,0x38} }, { 8736  /*100 kHz*/ , {0x1C ,0x38} }, { 8804  /*100 kHz*/ , {0x1C ,0x38} }, { 8872  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 8  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8600  /*100 kHz*/ , {0x00 ,0x04} }, { 8668  /*100 kHz*/ , {0x00 ,0x04} }, { 8736  /*100 kHz*/ , {0x00 ,0x04} }, { 8804  /*100 kHz*/ , {0x00 ,0x04} }, { 8872  /*100 kHz*/ , {0x00 ,0x04} },} },
   { /* 9  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8600  /*100 kHz*/ , {0x01 ,0x1F} }, { 8668  /*100 kHz*/ , {0x01 ,0x1F} }, { 8736  /*100 kHz*/ , {0x01 ,0x1F} }, { 8804  /*100 kHz*/ , {0x01 ,0x1F} }, { 8872  /*100 kHz*/ , {0x01 ,0x1F} },} },
   { /* 10 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8600  /*100 kHz*/ , {0x00 ,0x00} }, { 8668  /*100 kHz*/ , {0x00 ,0x00} }, { 8736  /*100 kHz*/ , {0x00 ,0x00} }, { 8804  /*100 kHz*/ , {0x00 ,0x00} }, { 8872  /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 11 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8600  /*100 kHz*/ , {0x01 ,0x00} }, { 8668  /*100 kHz*/ , {0x01 ,0x00} }, { 8736  /*100 kHz*/ , {0x01 ,0x00} }, { 8804  /*100 kHz*/ , {0x01 ,0x00} }, { 8872  /*100 kHz*/ , {0x01 ,0x00} },} },
   { /* 12 */ MIPI_NULL, 0             , 0         , 0                     ,{ { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_B_MIPI_RX_DATA_Set0[] =
{
   //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 19300 /*100 kHz*/ , {0x1C ,0x38} }, { 19420 /*100 kHz*/ , {0x1C ,0x38} }, { 19540 /*100 kHz*/ , {0x1C ,0x38} }, { 19660 /*100 kHz*/ , {0x1C ,0x38} }, { 19780 /*100 kHz*/ , {0x1C ,0x38} },} }, 
   { /* 1  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 19300 /*100 kHz*/ , {0x00 ,0x80} }, { 19420 /*100 kHz*/ , {0x00 ,0x80} }, { 19540 /*100 kHz*/ , {0x00 ,0x80} }, { 19660 /*100 kHz*/ , {0x00 ,0x80} }, { 19780 /*100 kHz*/ , {0x00 ,0x80} },} }, 
   { /* 2  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 19300 /*100 kHz*/ , {0x03 ,0x00} }, { 19420 /*100 kHz*/ , {0x03 ,0x00} }, { 19540 /*100 kHz*/ , {0x03 ,0x00} }, { 19660 /*100 kHz*/ , {0x03 ,0x00} }, { 19780 /*100 kHz*/ , {0x03 ,0x00} },} },
   { /* 3  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 19300 /*100 kHz*/ , {0x02 ,0x00} }, { 19420 /*100 kHz*/ , {0x02 ,0x00} }, { 19540 /*100 kHz*/ , {0x02 ,0x00} }, { 19660 /*100 kHz*/ , {0x02 ,0x00} }, { 19780 /*100 kHz*/ , {0x02 ,0x00} },} },
   { /* 4  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 19300 /*100 kHz*/ , {0x05 ,0x05} }, { 19420 /*100 kHz*/ , {0x05 ,0x05} }, { 19540 /*100 kHz*/ , {0x05 ,0x05} }, { 19660 /*100 kHz*/ , {0x05 ,0x05} }, { 19780 /*100 kHz*/ , {0x05 ,0x05} },} }, 
   { /* 5  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 19300 /*100 kHz*/ , {0x05 ,0x00} }, { 19420 /*100 kHz*/ , {0x05 ,0x00} }, { 19540 /*100 kHz*/ , {0x05 ,0x00} }, { 19660 /*100 kHz*/ , {0x05 ,0x00} }, { 19780 /*100 kHz*/ , {0x05 ,0x00} },} }, 
   { /* 6  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 19300 /*100 kHz*/ , {0x1C ,0x38} }, { 19420 /*100 kHz*/ , {0x1C ,0x38} }, { 19540 /*100 kHz*/ , {0x1C ,0x38} }, { 19660 /*100 kHz*/ , {0x1C ,0x38} }, { 19780 /*100 kHz*/ , {0x1C ,0x38} },} }, 
   { /* 7  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 19300 /*100 kHz*/ , {0x1C ,0x38} }, { 19420 /*100 kHz*/ , {0x1C ,0x38} }, { 19540 /*100 kHz*/ , {0x1C ,0x38} }, { 19660 /*100 kHz*/ , {0x1C ,0x38} }, { 19780 /*100 kHz*/ , {0x1C ,0x38} },} }, 
   { /* 8  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 19300 /*100 kHz*/ , {0x00 ,0x1F} }, { 19420 /*100 kHz*/ , {0x00 ,0x1F} }, { 19540 /*100 kHz*/ , {0x00 ,0x1F} }, { 19660 /*100 kHz*/ , {0x00 ,0x1F} }, { 19780 /*100 kHz*/ , {0x00 ,0x1F} },} }, 
   { /* 9  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 19300 /*100 kHz*/ , {0x01 ,0x04} }, { 19420 /*100 kHz*/ , {0x01 ,0x04} }, { 19540 /*100 kHz*/ , {0x01 ,0x04} }, { 19660 /*100 kHz*/ , {0x01 ,0x04} }, { 19780 /*100 kHz*/ , {0x01 ,0x04} },} }, 
   { /* 10 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 19300 /*100 kHz*/ , {0x00 ,0x00} }, { 19420 /*100 kHz*/ , {0x00 ,0x00} }, { 19540 /*100 kHz*/ , {0x00 ,0x00} }, { 19660 /*100 kHz*/ , {0x00 ,0x00} }, { 19780 /*100 kHz*/ , {0x00 ,0x00} },} }, 
   { /* 11 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 19300 /*100 kHz*/ , {0x01 ,0x00} }, { 19420 /*100 kHz*/ , {0x01 ,0x00} }, { 19540 /*100 kHz*/ , {0x01 ,0x00} }, { 19660 /*100 kHz*/ , {0x01 ,0x00} }, { 19780 /*100 kHz*/ , {0x01 ,0x00} },} }, 
   { /* 12 */ MIPI_NULL, 0             , 0         , 0                     ,{ { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_C_MIPI_RX_DATA_Set0[] =
{
   //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8510  /*100 kHz*/ , {0x1C ,0x38} }, { 8560  /*100 kHz*/ , {0x1C ,0x38} }, { 8610  /*100 kHz*/ , {0x1C ,0x38} }, { 8660  /*100 kHz*/ , {0x1C ,0x38} }, { 9350  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 1  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8510  /*100 kHz*/ , {0x00 ,0x80} }, { 8560  /*100 kHz*/ , {0x00 ,0x80} }, { 8610  /*100 kHz*/ , {0x00 ,0x80} }, { 8660  /*100 kHz*/ , {0x00 ,0x80} }, { 9350  /*100 kHz*/ , {0x00 ,0x80} },} },
   { /* 2  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8510  /*100 kHz*/ , {0x05 ,0x00} }, { 8560  /*100 kHz*/ , {0x05 ,0x00} }, { 8610  /*100 kHz*/ , {0x05 ,0x00} }, { 8660  /*100 kHz*/ , {0x05 ,0x00} }, { 9350  /*100 kHz*/ , {0x05 ,0x00} },} },
   { /* 3  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8510  /*100 kHz*/ , {0x03 ,0x00} }, { 8560  /*100 kHz*/ , {0x03 ,0x00} }, { 8610  /*100 kHz*/ , {0x03 ,0x00} }, { 8660  /*100 kHz*/ , {0x03 ,0x00} }, { 9350  /*100 kHz*/ , {0x03 ,0x00} },} },
   { /* 4  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8510  /*100 kHz*/ , {0x02 ,0x04} }, { 8560  /*100 kHz*/ , {0x02 ,0x04} }, { 8610  /*100 kHz*/ , {0x02 ,0x04} }, { 8660  /*100 kHz*/ , {0x02 ,0x04} }, { 9350  /*100 kHz*/ , {0x02 ,0x04} },} },
   { /* 5  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8510  /*100 kHz*/ , {0x02 ,0x00} }, { 8560  /*100 kHz*/ , {0x02 ,0x00} }, { 8610  /*100 kHz*/ , {0x02 ,0x00} }, { 8660  /*100 kHz*/ , {0x02 ,0x00} }, { 9350  /*100 kHz*/ , {0x02 ,0x00} },} },
   { /* 6  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8510  /*100 kHz*/ , {0x1C ,0x38} }, { 8560  /*100 kHz*/ , {0x1C ,0x38} }, { 8610  /*100 kHz*/ , {0x1C ,0x38} }, { 8660  /*100 kHz*/ , {0x1C ,0x38} }, { 9350  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 7  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8510  /*100 kHz*/ , {0x1C ,0x38} }, { 8560  /*100 kHz*/ , {0x1C ,0x38} }, { 8610  /*100 kHz*/ , {0x1C ,0x38} }, { 8660  /*100 kHz*/ , {0x1C ,0x38} }, { 9350  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 8  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8510  /*100 kHz*/ , {0x00 ,0x04} }, { 8560  /*100 kHz*/ , {0x00 ,0x04} }, { 8610  /*100 kHz*/ , {0x00 ,0x04} }, { 8660  /*100 kHz*/ , {0x00 ,0x04} }, { 9350  /*100 kHz*/ , {0x00 ,0x04} },} },
   { /* 9  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8510  /*100 kHz*/ , {0x01 ,0x1F} }, { 8560  /*100 kHz*/ , {0x01 ,0x1F} }, { 8610  /*100 kHz*/ , {0x01 ,0x1F} }, { 8660  /*100 kHz*/ , {0x01 ,0x1F} }, { 9350  /*100 kHz*/ , {0x01 ,0x1F} },} },
   { /* 10 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8510  /*100 kHz*/ , {0x00 ,0x00} }, { 8560  /*100 kHz*/ , {0x00 ,0x00} }, { 8610  /*100 kHz*/ , {0x00 ,0x00} }, { 8660  /*100 kHz*/ , {0x00 ,0x00} }, { 9350  /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 11 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM1_Set0   ,{ { 8510  /*100 kHz*/ , {0x01 ,0x00} }, { 8560  /*100 kHz*/ , {0x01 ,0x00} }, { 8610  /*100 kHz*/ , {0x01 ,0x00} }, { 8660  /*100 kHz*/ , {0x01 ,0x00} }, { 9350  /*100 kHz*/ , {0x01 ,0x00} },} },
   { /* 12 */ MIPI_NULL, 0             , 0         , 0                     ,{ { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_D_MIPI_RX_DATA_Set0[] =
{
   //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 1  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 2  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 3  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 8  */ MIPI_NULL, 0             , 0         , 0                ,{ { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_E_MIPI_RX_DATA_Set0[] =
{
   //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 1  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 2  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 3  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 8  */ MIPI_NULL, 0             , 0         , 0                ,{ { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } }, { 0     /*100 kHz*/ , {0    ,0   } },} },
};


/* TX Event */
MIPI_EVENT_TABLE_T BAND_A_MIPI_TX_EVENT_Set0[] =
{
   /* No.     elm type     , data idx      , evt_type       , evt_offset             */
   /*                       { start, stop },                  ( us )                 */
   { /* 0  */ MIPI_PA  ,    { 0    , 4    }, MIPI_TRX_ON      , MIPI_PA_TX_ON0_Set0  },
   { /* 1  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TRX_OFF     , MIPI_PA_TX_OFF0_Set0 },
   { /* 2  */ MIPI_PA  ,    { 6    , 6    }, MIPI_TX_GATE_ON  , MIPI_PA_TX_GATE_ON0_Set0  },
   { /* 3  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TX_GATE_OFF , MIPI_PA_TX_GATE_OFF0_Set0 },
   { /* 4  */ MIPI_ASM ,    { 7    , 10    }, MIPI_TRX_ON      , MIPI_ASM_TX_ON0_Set0 },
   { /* 5  */ MIPI_ASM ,    { 7    , 10    }, MIPI_TRX_OFF     , MIPI_ASM_TX_ON0_Set0 },
   { /* 6  */ MIPI_NULL,    { 0    , 0    }, MIPI_EVENT_NULL, 0               },
};

MIPI_EVENT_TABLE_T BAND_B_MIPI_TX_EVENT_Set0[] =
{
   /* No.     elm type     , data idx      , evt_type       , evt_offset             */
   /*                       { start, stop },                  ( us )                 */
   { /* 0  */ MIPI_PA  ,    { 0    , 4    }, MIPI_TRX_ON      , MIPI_PA_TX_ON0_Set0  },
   { /* 1  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TRX_OFF     , MIPI_PA_TX_OFF0_Set0 },
   { /* 2  */ MIPI_PA  ,    { 6    , 6    }, MIPI_TX_GATE_ON  , MIPI_PA_TX_GATE_ON0_Set0  },
   { /* 3  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TX_GATE_OFF , MIPI_PA_TX_GATE_OFF0_Set0 },
   { /* 4  */ MIPI_ASM ,    { 7    , 10    }, MIPI_TRX_ON      , MIPI_ASM_TX_ON0_Set0 },
   { /* 5  */ MIPI_ASM ,    { 7    , 10    }, MIPI_TRX_OFF     , MIPI_ASM_TX_ON0_Set0 },
   { /* 6  */ MIPI_NULL,    { 0    , 0    }, MIPI_EVENT_NULL, 0               },
};

MIPI_EVENT_TABLE_T BAND_C_MIPI_TX_EVENT_Set0[] =
{
   /* No.     elm type     , data idx      , evt_type       , evt_offset             */
   /*                       { start, stop },                  ( us )                 */
   { /* 0  */ MIPI_PA  ,    { 0    , 4    }, MIPI_TRX_ON      , MIPI_PA_TX_ON0_Set0  },
   { /* 1  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TRX_OFF     , MIPI_PA_TX_OFF0_Set0 },
   { /* 2  */ MIPI_PA  ,    { 6    , 6    }, MIPI_TX_GATE_ON  , MIPI_PA_TX_ON0_Set0  },
   { /* 3  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TX_GATE_OFF , MIPI_PA_TX_OFF0_Set0 },
   { /* 4  */ MIPI_ASM ,    { 7    , 10    }, MIPI_TRX_ON      , MIPI_ASM_TX_ON0_Set0 },
   { /* 5  */ MIPI_ASM ,    { 7    , 10    }, MIPI_TRX_OFF     , MIPI_ASM_TX_ON0_Set0 },
   { /* 6  */ MIPI_NULL,    { 0    , 0    }, MIPI_EVENT_NULL, 0               },
};

MIPI_EVENT_TABLE_T BAND_D_MIPI_TX_EVENT_Set0[] =
{
   /* No.     elm type     , data idx      , evt_type       , evt_offset             */
   /*                       { start, stop },                  ( us )                 */
   { /* 0  */ MIPI_PA  ,    { 0    , 4    }, MIPI_TRX_ON      , MIPI_PA_TX_ON0_Set0  },
   { /* 1  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TRX_OFF     , MIPI_PA_TX_OFF0_Set0 },
   { /* 2  */ MIPI_PA  ,    { 6    , 6    }, MIPI_TX_GATE_ON  , MIPI_PA_TX_ON0_Set0  },
   { /* 3  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TX_GATE_OFF , MIPI_PA_TX_OFF0_Set0 },
   { /* 4  */ MIPI_ASM ,    { 7    , 8    }, MIPI_TRX_ON      , MIPI_ASM_TX_ON0_Set0 },
   { /* 5  */ MIPI_ASM ,    { 7    , 8    }, MIPI_TRX_OFF     , MIPI_ASM_TX_ON0_Set0 },
   { /* 6  */ MIPI_NULL,    { 0    , 0    }, MIPI_EVENT_NULL, 0               },
};

MIPI_EVENT_TABLE_T BAND_E_MIPI_TX_EVENT_Set0[] =
{
   /* No.     elm type     , data idx      , evt_type       , evt_offset             */
   /*                       { start, stop },                  ( us )                 */
   { /* 0  */ MIPI_PA  ,    { 0    , 4    }, MIPI_TRX_ON      , MIPI_PA_TX_ON0_Set0  },
   { /* 1  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TRX_OFF     , MIPI_PA_TX_OFF0_Set0 },
   { /* 2  */ MIPI_PA  ,    { 6    , 6    }, MIPI_TX_GATE_ON  , MIPI_PA_TX_ON0_Set0  },
   { /* 3  */ MIPI_PA  ,    { 5    , 5    }, MIPI_TX_GATE_OFF , MIPI_PA_TX_OFF0_Set0 },
   { /* 4  */ MIPI_ASM ,    { 7    , 8    }, MIPI_TRX_ON      , MIPI_ASM_TX_ON0_Set0 },
   { /* 5  */ MIPI_ASM ,    { 7    , 8    }, MIPI_TRX_OFF     , MIPI_ASM_TX_ON0_Set0 },
   { /* 6  */ MIPI_NULL,    { 0    , 0    }, MIPI_EVENT_NULL, 0               },
};

/* TX Data */
MIPI_DATA_SUBBAND_TABLE_T BAND_A_MIPI_TX_DATA_Set0[] =
{
  //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8150  /*100 kHz*/ , {0x1C ,0x38} }, { 8218  /*100 kHz*/ , {0x1C ,0x38} }, { 8286  /*100 kHz*/ , {0x1C ,0x38} }, { 8354  /*100 kHz*/ , {0x1C ,0x38} }, { 8422  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 1  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8150  /*100 kHz*/ , {0x00 ,0x89} }, { 8218  /*100 kHz*/ , {0x00 ,0x89} }, { 8286  /*100 kHz*/ , {0x00 ,0x89} }, { 8354  /*100 kHz*/ , {0x00 ,0x89} }, { 8422  /*100 kHz*/ , {0x00 ,0x89} },} },
   { /* 2  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8150  /*100 kHz*/ , {0x01 ,0xB7} }, { 8218  /*100 kHz*/ , {0x01 ,0xB7} }, { 8286  /*100 kHz*/ , {0x01 ,0xB7} }, { 8354  /*100 kHz*/ , {0x01 ,0xB7} }, { 8422  /*100 kHz*/ , {0x01 ,0xB7} },} },
   { /* 3  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8150  /*100 kHz*/ , {0x02 ,0x00} }, { 8218  /*100 kHz*/ , {0x02 ,0x00} }, { 8286  /*100 kHz*/ , {0x02 ,0x00} }, { 8354  /*100 kHz*/ , {0x02 ,0x00} }, { 8422  /*100 kHz*/ , {0x02 ,0x00} },} },
   { /* 4  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8150  /*100 kHz*/ , {0x03 ,0x86} }, { 8218  /*100 kHz*/ , {0x03 ,0x86} }, { 8286  /*100 kHz*/ , {0x03 ,0x86} }, { 8354  /*100 kHz*/ , {0x03 ,0x86} }, { 8422  /*100 kHz*/ , {0x03 ,0x86} },} },
   { /* 5  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8150  /*100 kHz*/ , {0x00 ,0x89} }, { 8218  /*100 kHz*/ , {0x00 ,0x89} }, { 8286  /*100 kHz*/ , {0x00 ,0x89} }, { 8354  /*100 kHz*/ , {0x00 ,0x89} }, { 8422  /*100 kHz*/ , {0x00 ,0x89} },} },
   { /* 6  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8150  /*100 kHz*/ , {0x00 ,0x8D} }, { 8218  /*100 kHz*/ , {0x00 ,0x8D} }, { 8286  /*100 kHz*/ , {0x00 ,0x8D} }, { 8354  /*100 kHz*/ , {0x00 ,0x8D} }, { 8422  /*100 kHz*/ , {0x00 ,0x8D} },} },
   { /* 7  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8150  /*100 kHz*/ , {0x1C ,0x38} }, { 8218  /*100 kHz*/ , {0x1C ,0x38} }, { 8286  /*100 kHz*/ , {0x1C ,0x38} }, { 8354  /*100 kHz*/ , {0x1C ,0x38} }, { 8422  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 8  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8150  /*100 kHz*/ , {0x00 ,0x80} }, { 8218  /*100 kHz*/ , {0x00 ,0x80} }, { 8286  /*100 kHz*/ , {0x00 ,0x80} }, { 8354  /*100 kHz*/ , {0x00 ,0x80} }, { 8422  /*100 kHz*/ , {0x00 ,0x80} },} },
   { /* 9  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8150  /*100 kHz*/ , {0x02 ,0x04} }, { 8218  /*100 kHz*/ , {0x02 ,0x04} }, { 8286  /*100 kHz*/ , {0x02 ,0x04} }, { 8354  /*100 kHz*/ , {0x02 ,0x04} }, { 8422  /*100 kHz*/ , {0x02 ,0x04} },} },
   { /* 10 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8150  /*100 kHz*/ , {0x04 ,0x0B} }, { 8218  /*100 kHz*/ , {0x04 ,0x0B} }, { 8286  /*100 kHz*/ , {0x04 ,0x0B} }, { 8354  /*100 kHz*/ , {0x04 ,0x0B} }, { 8422  /*100 kHz*/ , {0x04 ,0x0B} },} },
   { /* 11 */ MIPI_NULL, 0                 , 0     , 0                     ,{ {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_B_MIPI_TX_DATA_Set0[] =
{
  //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 18500 /*100 kHz*/ , {0x1C ,0x38} }, { 18620 /*100 kHz*/ , {0x1C ,0x38} }, { 18740 /*100 kHz*/ , {0x1C ,0x38} }, { 18860 /*100 kHz*/ , {0x1C ,0x38} }, { 18980 /*100 kHz*/ , {0x1C ,0x38} },} }, 
   { /* 1  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 18500 /*100 kHz*/ , {0x00 ,0x18} }, { 18620 /*100 kHz*/ , {0x00 ,0x18} }, { 18740 /*100 kHz*/ , {0x00 ,0x18} }, { 18860 /*100 kHz*/ , {0x00 ,0x18} }, { 18980 /*100 kHz*/ , {0x00 ,0x18} },} }, 
   { /* 2  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 18500 /*100 kHz*/ , {0x01 ,0xC7} }, { 18620 /*100 kHz*/ , {0x01 ,0xC7} }, { 18740 /*100 kHz*/ , {0x01 ,0xC7} }, { 18860 /*100 kHz*/ , {0x01 ,0xC7} }, { 18980 /*100 kHz*/ , {0x01 ,0xC7} },} }, 
   { /* 3  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 18500 /*100 kHz*/ , {0x02 ,0x30} }, { 18620 /*100 kHz*/ , {0x02 ,0x30} }, { 18740 /*100 kHz*/ , {0x02 ,0x30} }, { 18860 /*100 kHz*/ , {0x02 ,0x30} }, { 18980 /*100 kHz*/ , {0x02 ,0x30} },} }, 
   { /* 4  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 18500 /*100 kHz*/ , {0x03 ,0x86} }, { 18620 /*100 kHz*/ , {0x03 ,0x86} }, { 18740 /*100 kHz*/ , {0x03 ,0x86} }, { 18860 /*100 kHz*/ , {0x03 ,0x86} }, { 18980 /*100 kHz*/ , {0x03 ,0x86} },} }, 
   { /* 5  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 18500 /*100 kHz*/ , {0x00 ,0x18} }, { 18620 /*100 kHz*/ , {0x00 ,0x18} }, { 18740 /*100 kHz*/ , {0x00 ,0x18} }, { 18860 /*100 kHz*/ , {0x00 ,0x18} }, { 18980 /*100 kHz*/ , {0x00 ,0x18} },} }, 
   { /* 6  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 18500 /*100 kHz*/ , {0x00 ,0x1C} }, { 18620 /*100 kHz*/ , {0x00 ,0x1C} }, { 18740 /*100 kHz*/ , {0x00 ,0x1C} }, { 18860 /*100 kHz*/ , {0x00 ,0x1C} }, { 18980 /*100 kHz*/ , {0x00 ,0x1C} },} }, 
   { /* 7  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 18500 /*100 kHz*/ , {0x1C ,0x38} }, { 18620 /*100 kHz*/ , {0x1C ,0x38} }, { 18740 /*100 kHz*/ , {0x1C ,0x38} }, { 18860 /*100 kHz*/ , {0x1C ,0x38} }, { 18980 /*100 kHz*/ , {0x1C ,0x38} },} }, 
   { /* 8  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 18500 /*100 kHz*/ , {0x00 ,0x80} }, { 18620 /*100 kHz*/ , {0x00 ,0x80} }, { 18740 /*100 kHz*/ , {0x00 ,0x80} }, { 18860 /*100 kHz*/ , {0x00 ,0x80} }, { 18980 /*100 kHz*/ , {0x00 ,0x80} },} }, 
   { /* 9  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 18500 /*100 kHz*/ , {0x05 ,0x05} }, { 18620 /*100 kHz*/ , {0x05 ,0x05} }, { 18740 /*100 kHz*/ , {0x05 ,0x05} }, { 18860 /*100 kHz*/ , {0x05 ,0x05} }, { 18980 /*100 kHz*/ , {0x05 ,0x05} },} }, 
   { /* 10 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 18500 /*100 kHz*/ , {0x04 ,0x13} }, { 18620 /*100 kHz*/ , {0x04 ,0x13} }, { 18740 /*100 kHz*/ , {0x04 ,0x13} }, { 18860 /*100 kHz*/ , {0x04 ,0x13} }, { 18980 /*100 kHz*/ , {0x04 ,0x13} },} }, 
   { /* 11 */ MIPI_NULL, 0                 , 0     , 0                     ,{ {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_C_MIPI_TX_DATA_Set0[] =
{
  //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8060  /*100 kHz*/ , {0x1C ,0x38} }, { 8110  /*100 kHz*/ , {0x1C ,0x38} }, { 8160  /*100 kHz*/ , {0x1C ,0x38} }, { 8210  /*100 kHz*/ , {0x1C ,0x38} }, { 8960  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 1  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8060  /*100 kHz*/ , {0x00 ,0x89} }, { 8110  /*100 kHz*/ , {0x00 ,0x89} }, { 8160  /*100 kHz*/ , {0x00 ,0x89} }, { 8210  /*100 kHz*/ , {0x00 ,0x89} }, { 8960  /*100 kHz*/ , {0x00 ,0x89} },} },
   { /* 2  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8060  /*100 kHz*/ , {0x01 ,0xB7} }, { 8110  /*100 kHz*/ , {0x01 ,0xB7} }, { 8160  /*100 kHz*/ , {0x01 ,0xB7} }, { 8210  /*100 kHz*/ , {0x01 ,0xB7} }, { 8960  /*100 kHz*/ , {0x01 ,0xB7} },} },
   { /* 3  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8060  /*100 kHz*/ , {0x02 ,0x00} }, { 8110  /*100 kHz*/ , {0x02 ,0x00} }, { 8160  /*100 kHz*/ , {0x02 ,0x00} }, { 8210  /*100 kHz*/ , {0x02 ,0x00} }, { 8960  /*100 kHz*/ , {0x02 ,0x00} },} },
   { /* 4  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8060  /*100 kHz*/ , {0x03 ,0x86} }, { 8110  /*100 kHz*/ , {0x03 ,0x86} }, { 8160  /*100 kHz*/ , {0x03 ,0x86} }, { 8210  /*100 kHz*/ , {0x03 ,0x86} }, { 8960  /*100 kHz*/ , {0x03 ,0x86} },} },
   { /* 5  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8060  /*100 kHz*/ , {0x00 ,0x89} }, { 8110  /*100 kHz*/ , {0x00 ,0x89} }, { 8160  /*100 kHz*/ , {0x00 ,0x89} }, { 8210  /*100 kHz*/ , {0x00 ,0x89} }, { 8960  /*100 kHz*/ , {0x00 ,0x89} },} },
   { /* 6  */ MIPI_PA  , MIPI_PORT1    , REG_W     , MIPI_USID_PA1_Set0    ,{ { 8060  /*100 kHz*/ , {0x00 ,0x8D} }, { 8110  /*100 kHz*/ , {0x00 ,0x8D} }, { 8160  /*100 kHz*/ , {0x00 ,0x8D} }, { 8210  /*100 kHz*/ , {0x00 ,0x8D} }, { 8960  /*100 kHz*/ , {0x00 ,0x8D} },} },
   { /* 7  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8060  /*100 kHz*/ , {0x1C ,0x38} }, { 8110  /*100 kHz*/ , {0x1C ,0x38} }, { 8160  /*100 kHz*/ , {0x1C ,0x38} }, { 8210  /*100 kHz*/ , {0x1C ,0x38} }, { 8960  /*100 kHz*/ , {0x1C ,0x38} },} },
   { /* 8  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8060  /*100 kHz*/ , {0x00 ,0x80} }, { 8110  /*100 kHz*/ , {0x00 ,0x80} }, { 8160  /*100 kHz*/ , {0x00 ,0x80} }, { 8210  /*100 kHz*/ , {0x00 ,0x80} }, { 8960  /*100 kHz*/ , {0x00 ,0x80} },} },
   { /* 9  */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8060  /*100 kHz*/ , {0x02 ,0x04} }, { 8110  /*100 kHz*/ , {0x02 ,0x04} }, { 8160  /*100 kHz*/ , {0x02 ,0x04} }, { 8210  /*100 kHz*/ , {0x02 ,0x04} }, { 8960  /*100 kHz*/ , {0x02 ,0x04} },} },
   { /* 10 */ MIPI_ASM , MIPI_PORT0    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 8060  /*100 kHz*/ , {0x04 ,0x0B} }, { 8110  /*100 kHz*/ , {0x04 ,0x0B} }, { 8160  /*100 kHz*/ , {0x04 ,0x0B} }, { 8210  /*100 kHz*/ , {0x04 ,0x0B} }, { 8960  /*100 kHz*/ , {0x04 ,0x0B} },} },
   { /* 11 */ MIPI_NULL, 0                 , 0     , 0                     ,{ {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_D_MIPI_TX_DATA_Set0[] =
{
   //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 1  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 2  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 3  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 4  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 5  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 6  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 7  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 8  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 9  */ MIPI_NULL, 0                 , 0     , 0                ,{ {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } },} },
};

MIPI_DATA_SUBBAND_TABLE_T BAND_E_MIPI_TX_DATA_Set0[] =
{
   //No.      elm type     , port_sel          , data_seq      , USID             ,{ { subband-0 freq    ,addr ,data }, { subband-1 freq    ,addr ,data }, { subband-2 freq    ,addr ,data }, { subband-3 freq    ,addr ,data }, { subband-4 freq    ,addr ,data },
   { /* 0  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 1  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 2  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 3  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 4  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 5  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 6  */ MIPI_PA  , MIPI_PORT0    , REG_W     , MIPI_USID_PA0_Set0    ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 7  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 8  */ MIPI_ASM , MIPI_PORT1    , REG_W     , MIPI_USID_ASM0_Set0   ,{ { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} }, { 00000 /*100 kHz*/ , {0x00 ,0x00} },} },
   { /* 9  */ MIPI_NULL, 0                 , 0     , 0                ,{ {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } }, {     0 /*100 kHz*/ , {0    ,0   } },} },
};

/* TPC Event */
MIPI_EVENT_TABLE_T BAND_A_MIPI_TPC_EVENT_Set0[] =
{
   /* No.     elm type     , data idx       , evt_type       , evt_offset    */
   /*                        { start, stop },                  ( us )        */
   { /* 0  */ MIPI_PA  ,     { 0    , 2    }, MIPI_TPC_SET   , 5        },
   { /* 1  */ MIPI_NULL,     { 0    , 0    }, MIPI_EVENT_NULL, 0        },
};

MIPI_EVENT_TABLE_T BAND_B_MIPI_TPC_EVENT_Set0[] =
{
   /* No.     elm type     , data idx       , evt_type       , evt_offset    */
   /*                        { start, stop },                  ( us )        */
   { /* 0  */ MIPI_PA  ,     { 0    , 2    }, MIPI_TPC_SET   , 5        },
   { /* 1  */ MIPI_NULL,     { 0    , 0    }, MIPI_EVENT_NULL, 0        },
};

MIPI_EVENT_TABLE_T BAND_C_MIPI_TPC_EVENT_Set0[] =
{
   /* No.     elm type     , data idx       , evt_type       , evt_offset    */
   /*                        { start, stop },                  ( us )        */
   { /* 0  */ MIPI_PA  ,     { 0    , 2    }, MIPI_TPC_SET   , 5        },
   { /* 1  */ MIPI_NULL,     { 0    , 0    }, MIPI_EVENT_NULL, 0        },
};

MIPI_EVENT_TABLE_T BAND_D_MIPI_TPC_EVENT_Set0[] =
{
   /* No.     elm type     , data idx       , evt_type       , evt_offset    */
   /*                        { start, stop },                  ( us )        */
   { /* 0  */ MIPI_PA  ,     { 0    , 2    }, MIPI_TPC_SET   , 5        },
   { /* 1  */ MIPI_NULL,     { 0    , 0    }, MIPI_EVENT_NULL, 0        },
};

MIPI_EVENT_TABLE_T BAND_E_MIPI_TPC_EVENT_Set0[] =
{
   /* No.     elm type     , data idx       , evt_type       , evt_offset    */
   /*                        { start, stop },                  ( us )        */
   { /* 0  */ MIPI_PA  ,     { 0    , 2    }, MIPI_TPC_SET   , 5        },
   { /* 1  */ MIPI_NULL,     { 0    , 0    }, MIPI_EVENT_NULL, 0        },
};

/* TPC Data */
MIPI_DATA_TABLE_T BAND_A_MIPI_TPC_DATA_Set0[] =
{
   //No.     elm type   , port_sel     , data_seq, USID                     , address                  , data
   {/* 0  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA0},
   {/* 1  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA1},
   {/* 2  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA2},
   {/* 3  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA3},
   {/* 4  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA4},
   {/* 5  */ MIPI_NULL  , 0            , 0       , 0                        , 0                        , 0                    },
};

MIPI_DATA_TABLE_T BAND_B_MIPI_TPC_DATA_Set0[] =
{
   //No.     elm type   , port_sel     , data_seq, USID                     , address                  , data
   {/* 0  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA0},
   {/* 1  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA1},
   {/* 2  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA2},
   {/* 3  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA3},
   {/* 4  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA4},
   {/* 5  */ MIPI_NULL  , 0            , 0       , 0                        , 0                        , 0                    },
};

MIPI_DATA_TABLE_T BAND_C_MIPI_TPC_DATA_Set0[] =
{
   //No.     elm type   , port_sel     , data_seq, USID                     , address                  , data
   {/* 0  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA0},
   {/* 1  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA1},
   {/* 2  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA2},
   {/* 3  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA3},
   {/* 4  */ MIPI_PA_SEC, MIPI_PORT1   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA4},
   {/* 5  */ MIPI_NULL  , 0            , 0       , 0                        , 0                        , 0                    },
};

MIPI_DATA_TABLE_T BAND_D_MIPI_TPC_DATA_Set0[] =
{
   //No.     elm type   , port_sel     , data_seq, USID                     , address                  , data
   {/* 0  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA0},
   {/* 1  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA1},
   {/* 2  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA2},
   {/* 3  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA3},
   {/* 4  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA4},
   {/* 5  */ MIPI_NULL  , 0            , 0       , 0                        , 0                        , 0                    },
};

MIPI_DATA_TABLE_T BAND_E_MIPI_TPC_DATA_Set0[] =
{
   //No.     elm type   , port_sel     , data_seq, USID                     , address                  , data
   {/* 0  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA0},
   {/* 1  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA1},
   {/* 2  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA2},
   {/* 3  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA3},
   {/* 4  */ MIPI_PA_SEC, MIPI_PORT0   , REG_W   , MIPI_PA_SECTION_USID     , MIPI_PA_SECTION_ADDRESS  , MIPI_PA_SECTION_DATA4},
   {/* 5  */ MIPI_NULL  , 0            , 0       , 0                        , 0                        , 0                    },
};

MIPI_TPC_SECTION_TABLE_T BAND_A_MIPI_PA_SECTION_DATA_1XRTT_Set0[MIPI_SUBBAND_NUM] =
{
   {
      8150, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8218, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8286, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      8354, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8422, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_B_MIPI_PA_SECTION_DATA_1XRTT_Set0[MIPI_SUBBAND_NUM] =
{
   {
      18500, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x76}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x97}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      18620, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x76}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x97}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      18740, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x76}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x97}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      18860, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x76}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x97}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      18980, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x76}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x97}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_C_MIPI_PA_SECTION_DATA_1XRTT_Set0[MIPI_SUBBAND_NUM] =
{
   {
      8060, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8110, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8160, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      8210, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8960, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x45}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x86}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0x98}, { 0x2, 0x00}, { 0x3, 0x88}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_D_MIPI_PA_SECTION_DATA_1XRTT_Set0[MIPI_SUBBAND_NUM] =
{
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_E_MIPI_PA_SECTION_DATA_1XRTT_Set0[MIPI_SUBBAND_NUM] =
{
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

/*** EVDO TPC Sections ***/
MIPI_TPC_SECTION_TABLE_T BAND_A_MIPI_PA_SECTION_DATA_EVDO_Set0[MIPI_SUBBAND_NUM] =
{
   {
      8150, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8218, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8286, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      8354, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8422, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_B_MIPI_PA_SECTION_DATA_EVDO_Set0[MIPI_SUBBAND_NUM] =
{
   {
      18500, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x44}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x65}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x56}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xC7}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      18620, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x44}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x65}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x56}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xC7}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      18740, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x44}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x65}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x56}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xC7}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      18860, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x44}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x65}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x56}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xC7}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      18980, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x13}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x14}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x30}, { 0x3, 0x06}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x44}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x65}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x56}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xC7}, { 0x2, 0x30}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_C_MIPI_PA_SECTION_DATA_EVDO_Set0[MIPI_SUBBAND_NUM] =
{
   {
      8060, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8110, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8160, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      8210, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      8960, /*100kHz*/
      MIPI_USID_PA1_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         /* 0 */ {{{ 0x1, 0x12}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 1 */ {{{ 0x1, 0x23}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 2 */ {{{ 0x1, 0x25}, { 0x2, 0x00}, { 0x3, 0x89}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 3 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 4 */ {{{ 0x1, 0x35}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 5 */ {{{ 0x1, 0x85}, { 0x2, 0x00}, { 0x3, 0x80}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 6 */ {{{ 0x1, 0x66}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         /* 7 */ {{{ 0x1, 0xB7}, { 0x2, 0x00}, { 0x3, 0x86}, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_D_MIPI_PA_SECTION_DATA_EVDO_Set0[MIPI_SUBBAND_NUM] =
{
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

MIPI_TPC_SECTION_TABLE_T BAND_E_MIPI_PA_SECTION_DATA_EVDO_Set0[MIPI_SUBBAND_NUM] =
{
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
      }
   },
   {
      00000, /*100kHz*/
      MIPI_USID_PA0_Set0, /*USID*/
      {
         // PAEn=1
         // PA_SEC_DATA0, PA_SEC_DATA1, PA_SEC_DATA2, PA_SEC_DATA3, PA_SEC_DATA4
         // {addr, data}, {addr, data}, {addr, data}, {addr, data}, {addr, data}
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
         {{{ 0x0, 0x0}, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }, { 0x0, 0x0 }}},
       }
   },
};

