/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/**************************************************************************************************
* %version: 2 %  %instance: HZPT_2 %   %date_created: Fri Mar 23 15:07:25 2007 %  %created_by: jingzhang %  %derived_by: jingzhang %
**************************************************************************************************/

/*****************************************************************************
 
  FILE NAME: FsmDev.h

  DESCRIPTION:

    common definitions for all device.

*****************************************************************************/


#ifndef __FSM_DEV_H__

#define __FSM_DEV_H__


#include "fsmdefs.h"


/**************************************************
 *
 * For all devices and all device drivers
 *
 **************************************************/

enum
{
	DEV_TYPE_FLASH = 0,
	DEV_TYPE_SD,
	DEV_TYPE_NAND_FLASH
};


/* Hardware Error Values */
enum
{
	HW_ERR_NONE = 0,
	HW_ERR_PARAM,
	HW_ERR_READ,
	HW_ERR_WRITE,
	HW_ERR_ERASE,
	HW_ERR_SYSTEM,
	HW_ERR_SUSPEND
};


/* Device control command. */
enum
{
	DEV_CTRL_DEVICE_DETACH	= 1,
	DEV_CTRL_END			= 2 /* The last common code, device-specific code will follow this code. */
};


struct _FSM_DEV_DRV;

typedef struct
{
	/* char				DevName[16];  	 DevName field is not used by now. */

	struct _FSM_DEV_DRV *	DevDrvP;
	uint8					DevType;
	void		  *			MediaObjP;		/* Reserved for File system use. */
} FsmDevObjHdrT;


typedef uint32 (*DEV_READ_FUNCPTR)(FsmDevObjHdrT * , uint8 * , uint32 , uint32 );

typedef uint32 (*DEV_WRITE_FUNCPTR)(FsmDevObjHdrT * , uint8 * , uint32 , uint32 );

typedef uint32 (*DEV_COPY_FUNCPTR)(FsmDevObjHdrT * , uint32 , uint32 , uint32 );

typedef uint32 (*DEV_ERASE_FUNCPTR)(FsmDevObjHdrT * , uint32 );

typedef uint32 (*DEV_CTRL_FUNCPTR)(FsmDevObjHdrT * , uint32 , void *);

typedef uint32 (*DEV_INIT_FUNCPTR)(FsmDevObjHdrT * );


typedef struct _FSM_DEV_DRV
{
	DEV_READ_FUNCPTR		FsmDevRead;
	DEV_WRITE_FUNCPTR		FsmDevWrite;
	DEV_COPY_FUNCPTR		FsmDevCopy;
	DEV_ERASE_FUNCPTR		FsmDevErase;
	DEV_CTRL_FUNCPTR		FsmDevCtrl;
	DEV_INIT_FUNCPTR		FsmDevInit;

	/*
	uint32 (*FsmDevRead)(void * DevObjP, uint8 * buf, uint32 Addr, uint32 length);
	uint32 (*FsmDevWrite)(void * DevObjP, uint8 * buf, uint32 Addr, uint32 length);
	uint32 (*FsmDevCopy)(void * DevObjP, uint32 DstAddr, uint32 SrcAddr, uint32 length);
	uint32 (*FsmDevErase)(void * DevObjP, uint32 Addr);
	uint32 (*FsmDevCtrl)(void * DevObjP, uint32 CtrlCode, void *arg);
	uint32 (*FsmDevInit)(void * DevObjP);
	*/

} FsmDevDrvT;


#define VALIDATE_ADDRESS


#endif /* __FSM_DEV_H__ */

/*****************************************************************************
* $Log: FsmDev.h $
* Revision 1.1  2007/10/29 11:51:09  binye
* Initial revision
* Revision 1.1  2007/10/12 09:51:00  dsu
* Initial revision
* Revision 1.1  2006/12/19 14:55:25  yjin
* Initial revision
* Revision 1.1  2006/11/26 21:35:28  yliu
* Initial revision
* Revision 1.2  2006/10/25 15:31:44  yjin
* fsm update
* Revision 1.1  2006/10/24 15:00:58  binye
* Initial revision
* Revision 1.2  2006/01/07 11:46:59  wavis
* Merging in VAL.
* Revision 1.1.1.2  2005/12/14 11:15:15  wavis
* Added support for DOS File System.
* Revision 1.1.1.1  2005/11/08 13:18:56  wavis
* Duplicate revision
* Revision 1.1  2005/11/08 13:18:56  vnarayana
* Initial revision
* Revision 1.1  2005/11/07 14:33:34  wavis
* Initial revision
* Revision 1.1  2005/10/12 15:47:48  dorloff
* Initial revision
* Revision 1.2  2005/04/05 19:05:51  lwang
* Synchronized from FSM baseline:
*    defined the last common control code.
* Revision 1.1  2005/01/30 11:02:00  lwang
* Initial revision
* Revision 1.1  2004/08/03 12:40:33  javese
* Initial revision
* Revision 1.2  2004/06/07 15:37:18  jjs
* delete PACKED qualifier in the structure definitions.
* Revision 1.1  2004/05/21 16:57:47  jjs
* Initial revision
* Revision 1.2  2004/03/17 12:58:33  zgy
* Revision 1.9  2004/03/16 15:59:37  jjs
* Revision 1.8  2004/03/11 15:26:15  jjs
* added copyright info.
* Revision 1.7  2003/11/05 10:25:55  wsm
* Revision 1.6  2003/10/15 18:27:16  jjs
* Revision 1.5  2003/09/14 16:56:18  jjs
* Revision 1.4  2003/09/13 19:42:46  jjs
* Revision 1.3  2003/09/12 12:20:59  jjs
* Revision 1.2  2003/09/09 20:14:26  jjs
* Added pseudo device for test.
* Revision 1.1  2003/09/09 15:09:22  jjs
* Initial revision
*****************************************************************************/


/**Log information: \main\vtui2_5x\2 2008-06-28 13:42:22 GMT khong
** HREF#747 :
|Merge selle software to baseline.**/
