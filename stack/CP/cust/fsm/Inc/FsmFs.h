/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
 
  FILE NAME: FsmFs.h

  DESCRIPTION:

    common definitions for all fs.

*****************************************************************************/


#ifndef __FSM_FS_H__

#define __FSM_FS_H__

#include "fsmdefs.h"
#include "FsmDev.h"


typedef struct
{
   FsmDevObjHdrT *DevObjP;
} FsmFileDescriptorHdrT;



typedef struct
{
	FsmDevObjHdrT  *DevObjP;
} FsmMediaObjHdrT;


typedef  uint32   (*FSM_CREATE_FUNCPTR)(FsmDevObjHdrT * , char * , uint32 , uint32 * );

typedef  uint32   (*FSM_OPEN_FUNCPTR)(FsmDevObjHdrT * , char * , uint32 , uint32 * );

typedef  uint32   (*FSM_DELETE_FUNCPTR)(FsmDevObjHdrT * , char * );

typedef  uint32   (*FSM_CLOSE_FUNCPTR)(uint32 );

typedef  uint32   (*FSM_READ_FUNCPTR)(uint32 , uint8 * , uint32 , uint32 * );

typedef  uint32   (*FSM_WRITE_FUNCPTR)(uint32 , uint8 * , uint32 , uint32 * );

typedef  uint32   (*FSM_IOCTRL_FUNCPTR)(uint32 , uint32 , void * );

typedef  uint32   (*FSM_INIT_FUNCPTR)(FsmDevObjHdrT * , uint32 );

typedef  uint32   (*FSM_TERMINATE_FUNCPTR)(FsmDevObjHdrT * );

typedef struct
{
   FSM_CREATE_FUNCPTR   FsmCreate;
   FSM_OPEN_FUNCPTR     FsmOpen;
   FSM_DELETE_FUNCPTR   FsmDelete;
   FSM_CLOSE_FUNCPTR    FsmClose;
   FSM_READ_FUNCPTR     FsmRead;
   FSM_WRITE_FUNCPTR    FsmWrite;
   FSM_IOCTRL_FUNCPTR   FsmIoCtrl;
   FSM_INIT_FUNCPTR     FsmInit;
   FSM_TERMINATE_FUNCPTR   FsmTerminate;

	/*
	uint32	(*FsmCreate)(FsmDevObjHdrT * DevObjP, char * FileName, uint32 CreateFlag, uint32 * ErrorCodeP);
	uint32	(*FsmOpen)(FsmDevObjHdrT * DevObjP, char * FileName, uint32 OpenMode, uint32 * ErrorCodeP);
	uint32	(*FsmDelete)(FsmDevObjHdrT * DevObjP, char * FileName);
	uint32	(*FsmClose)(uint32 fd);
	uint32	(*FsmRead)(uint32 fd, uint8 * Buf, uint32 size, uint32 * ErrorCodeP);
	uint32	(*FsmWrite)(uint32 fd, uint8 * Buf, uint32 size, uint32 * ErrorCodeP);
	uint32	(*FsmIoCtrl)(uint32 fd, uint32 cmd, void * arg);
	uint32	(*FsmInit)(FsmDevObjHdrT * DevObjP, uint32 SectorSize);
	uint32	(*FsmTerminate)(FsmDevObjHdrT * DevObjP);
	*/
} FsmFsDrvT;


#endif /* __FSM_FS_H__ */


/*****************************************************************************
* $Log: FsmFs.h $
* Revision 1.1  2005/10/12 15:47:51  dorloff
* Initial revision
* Revision 1.1  2005/01/30 12:02:03  lwang
* Initial revision
* Revision 1.1  2004/08/03 12:40:34  javese
* Initial revision
* Revision 1.2  2004/06/07 15:37:24  jjs
* delete PACKED qualifier in the structure definitions.
* Revision 1.1  2004/05/21 16:57:49  jjs
* Initial revision
* Revision 1.3  2004/03/17 12:58:36  zgy
* Revision 1.8  2004/03/16 15:59:41  jjs
* Revision 1.7  2004/03/11 15:24:34  jjs
* Revision 1.6  2003/11/05 10:26:26  wsm
* Revision 1.5  2003/09/14 16:56:26  jjs
* Revision 1.4  2003/09/12 12:42:08  jjs
* Revision 1.3  2003/09/12 12:23:55  jjs
* Revision 1.2  2003/09/12 12:18:55  jjs
* Revision 1.1  2003/09/09 15:09:23  jjs
* Initial revision
*****************************************************************************/

