/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
 
  FILE NAME: Fsmdefs.h

  DESCRIPTION:

    Defines compile options and MACROs for the whole FSM module.

*****************************************************************************/



#ifndef __FSM_DEFS_H__

#define __FSM_DEFS_H__

#define OS_NUCLEUS              1

#define OS_WINDOWS              2

#if defined MTK_PLT_ON_PC
#define OS_OSCAR          3
#define OS_TYPE                 OS_OSCAR
#else
#define OS_TYPE                 OS_NUCLEUS
#endif

/*#define USE_NFFS_OPTION*/
#ifndef MTK_DEV_CCCI_FS
#if (defined SYS_FLASH_LESS_SUPPORT || defined MTK_PLT_ON_PC)
#define USE_DOSFS_OPTION
#endif
#endif

#define FSM_TIME_SUPPORT

#if (OS_TYPE == OS_NUCLEUS)

#include "nucleus.h"
#include "sysapi.h"
#include "monapi.h"

#define FsmPrintf MonPrintf

#elif (OS_TYPE == OS_OSCAR)

#include "Osc_type.h"
#include "Osc_func.h"
#include "sysapi.h"
#include "monapi.h"

#define FsmPrintf MonPrintf

#else

#include "windows.h"

#include "stdio.h"
#include "assert.h"

typedef DWORD       uint32;
typedef WORD        uint16;
typedef BYTE        uint8;

typedef long        int32;
typedef short       int16;
typedef char        int8;

#undef PACKED
#define PACKED      

#undef FIELD_OFFSET

#if 0
extern int FsmPrintf(const char * format, ...);
#endif
/*
#define MonPrintf       FsmPrintf
*/
#define ASSERT(x)   
#define MonPrintf       printf

#define MonFault        assert
#define MonTrace

#define MON_FSM_FAULT_UNIT 0
#define MON_CP_FSM_FILE_READ_TRACE_ID 0
#define MON_CP_FSM_FILE_WRITE_TRACE_ID 0
#define MON_HALT 0

#endif  /* OS_WINDOWS */

/*--------------------------------------------*/

/*#define DEBUG_FSM*/

/*--------------------------------------------*/


#define FIELD_SIZE(type, field)     (sizeof((((type *)0)->field)))

#define FIELD_OFFSET(type, field)   ((uint32)(&(((type *)0)->field)))

#ifndef TRUE
#define TRUE    (bool)1
#endif
#ifndef FALSE
#define FALSE   (bool)0
#endif
/*
#ifndef TRUE
#define TRUE    1
#endif
#ifndef FALSE
#define FALSE   0
#endif
*/

#include "errors.h"

#endif /* __FSM_DEFS_H__ */



