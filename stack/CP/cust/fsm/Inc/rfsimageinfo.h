/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/


/*****************************************************************************
* 
* FILE NAME   : rfsimageinfo.h
*
* DESCRIPTION : Define the base type used for RFS image and version control
*****************************************************************************/

#ifndef __RFS_IMAGE_INFO_H__
#define __RFS_IMAGE_INFO_H__

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"

/*----------------------------------------------------------------------------
  Macros
----------------------------------------------------------------------------*/
#define INVALID_FSM_IMAGE_ADDR  0xFFFFFFFF
#define RF_IS_CALIBRATED_FLAG   0xC
#define DEFAULT_VERSION_FLAG    0xD
#define RFS_FILE_NAME_LEN       32

/*----------------------------------------------------------------------------
 Typedefs
----------------------------------------------------------------------------*/
typedef char FileNameT[RFS_FILE_NAME_LEN];

typedef PACKED_PREFIX enum
{
   FSM_IMG_INIT = 0,
   RF_IMG = FSM_IMG_INIT,
   CUST_IMG,
   RW_IMG,
   MAX_FSM_IMG_TYPE
} PACKED_POSTFIX FsmImageEnumT;


typedef PACKED_PREFIX struct
{
   FileNameT     FileName;
   FsmImageEnumT ImageBelongs;
} PACKED_POSTFIX FileAttrT;

typedef PACKED_PREFIX struct
{
   uint8 MajorVer;
   uint8 MinorVer;
   uint8 ValueVer;
   uint8 CustVer;
} PACKED_POSTFIX FileVerT;

typedef PACKED_PREFIX struct
{
   uint16 SegIndex;
   uint32 SegSize;
   void*  DfValuePtr;
   uint32 DfValueSize;
} PACKED_POSTFIX SegInfoT;

typedef PACKED_PREFIX struct
{
   FileNameT        FileName;
   FileVerT         FileVer;
   const SegInfoT*  FileSegInfoPtr;
   uint32     SegNum;
} PACKED_POSTFIX ImageFileInfoT;

typedef PACKED_PREFIX struct
{
   uint32 FsmImageAddr[MAX_FSM_IMG_TYPE];
   uint32 CheckSum;
} PACKED_POSTFIX FsmImageRegionHeader;

#endif

/*****************************************************************************
* End of File
*****************************************************************************/
