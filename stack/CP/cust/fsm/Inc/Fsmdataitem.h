/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/**************************************************************************************************
* %version: 2 %  %instance: HZPT_2 %   %date_created: Fri Mar 23 15:07:39 2007 %  %created_by: jingzhang %  %derived_by: jingzhang %
**************************************************************************************************/

/*****************************************************************************
 
  FILE NAME: FsmDataItem.h

  DESCRIPTION:

    Data item manager definitions.

*****************************************************************************/


#ifndef __FSM_DATAITEM_H__

#define __FSM_DATAITEM_H__

#include "fsmdefs.h"


typedef struct
{
	uint16                  ItemType;
	uint16                  ItemId;
	uint32			        ItemLength;
	uint8			        Attrib;
    uint8                   Method;
} FsmItemInfoT;

/* wildcard for data item search. */
#define WILDCARD_TYPE             0xFFFF
#define WILDCARD_ID               0xFFFF

/* Interface definition */

uint32		FsmDataItemOpen(uint16 type, uint16 id, uint32 mode);
uint32		FsmDataItemClose(uint32 ItemFd);
uint32		FsmDataItemWrite(uint16 type, uint16 id, uint32 offset, uint8 * buffer, uint32 size);
uint32		FsmDataItemRead(uint16 type, uint16 id, uint32 offset, uint8 * buffer, uint32 size);

uint32		FsmDataItemDelete(uint16 type, uint16 id);

uint32		FsmDataItemFindFirst(uint16 type, uint16 id, FsmItemInfoT * Info);

uint32		FsmDataItemFindNext(uint32 ItemFd, FsmItemInfoT * Info);

#define FsmDataItemFindClose(a) FsmDataItemClose(a)

uint32		FsmDataItemFlush(uint16 type, uint16 id);
uint32		FsmDataItemFlushAll(void);
uint32		FsmDataItemError(uint32 ItemFd);
uint32		FsmDataItemGetError(void);

/* interface define end */

#endif

/*****************************************************************************
* $Log: Fsmdataitem.h $
* Revision 1.1  2007/10/29 11:51:08  binye
* Initial revision
* Revision 1.1  2007/10/12 09:50:57  dsu
* Initial revision
* Revision 1.1  2006/12/19 14:55:23  yjin
* Initial revision
* Revision 1.1  2006/11/26 21:35:26  yliu
* Initial revision
* Revision 1.1  2006/10/24 15:00:58  binye
* Initial revision
* Revision 1.1  2005/11/08 13:18:54  vnarayana
* Initial revision
* Revision 1.1  2005/11/07 14:33:31  wavis
* Initial revision
* Revision 1.1  2005/10/12 15:47:46  dorloff
* Initial revision
* Revision 1.1  2005/01/30 12:01:57  lwang
* Initial revision
* Revision 1.2  2004/09/29 15:49:57  lwang
* Revision 1.1  2004/08/03 12:40:31  javese
* Initial revision
* Revision 1.1  2004/07/26 17:14:54  jjs
* Initial revision
* Revision 1.2  2004/06/07 15:49:46  jjs
* delete PACKED qualifier in the structure definitions.
* Revision 1.1  2004/05/21 16:57:19  jjs
* Initial revision
* Revision 1.3  2004/03/17 12:57:45  zgy
* Revision 1.7  2004/03/16 15:54:19  jjs
* Revision 1.6  2004/03/11 15:30:43  jjs
* Revision 1.5  2003/10/26 10:46:25  jjs
* Revision 1.4  2003/10/24 13:16:32  jjs
* Revision 1.3  2003/10/23 14:01:52  wsm
* Revision 1.2  2003/10/21 16:30:28  wsm
* Revision 1.1  2003/10/21 16:14:21  jjs
* Initial revision
* Revision 1.1  2003/09/30 15:06:58  wsm
* Initial revision
*****************************************************************************/
