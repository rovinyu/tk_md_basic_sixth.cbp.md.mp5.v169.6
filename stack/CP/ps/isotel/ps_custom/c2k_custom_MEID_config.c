/*************************************************************
*
* Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*************************************************************/

/*****************************************************************************

    FILE NAME:
        c2k_custom_MEID_config.c
    DESCRIPTION:
        Contains C2K MEID default config function

*****************************************************************************/
#include "pswapi.h"
#include "nvram_data_items_id.h"

#define KEY_TO_ESN (0x1F67D49B)
extern uint16 calcChecksum(uint8 *ptr, uint16 size);


/*Customer can config ARRAY MEID default*/
static uint8 MEID_DEFAULT[8] = {0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0, 0x0};



void NvramAssignMEIDDefault(uint16 LID, uint8 *Buffer, uint16 BufSize)
{
   SecureDataStructT *DataP;
   uint32 Temp, Upper32, Lower32;
   DataP = (SecureDataStructT *)Buffer;
   SysMemset(DataP, 0x0, sizeof(SecureDataStructT));
    
   Upper32 = *((uint32 *)MEID_DEFAULT);
   Lower32 = *((uint32 *)(MEID_DEFAULT + 4));
  
   Temp = KEY_TO_ESN ^ Upper32;/*Encrypt the upper32 of ESN/MEID*/
   DataP->MSID.MOBILE_ID_1 = (Temp & 0xFFFF0000) >> 16;
   DataP->MSID.MOBILE_ID_2 = (Temp & 0x0000FFFF);

   Temp = KEY_TO_ESN ^ Lower32; /*Encrypt the lower32 of ESN/MEID*/
   DataP->MSID.MOBILE_ID_3 = (Temp & 0xFFFF0000) >> 16;
   DataP->MSID.MOBILE_ID_4 = (Temp & 0x0000FFFF);

   DataP->MSID.mobileIdType = MEID;
  
   DataP->AKEY_NAM1[1] = 1;
   DataP->AKEY_NAM2[1] = 1;
   DataP->checkSum = calcChecksum((uint8 *)DataP, sizeof(SecureDataStructT)-sizeof(DataP->checkSum));
}
