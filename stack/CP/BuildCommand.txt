Compiler: GCC 4.9.3

Build Command:
  ./make.sh                          # help info
  ./make.sh "<PROJECT_FILE>" new     # rebuild
  ./make.sh "<PROJECT_FILE>" update  # build (no clean)
  ./make.sh "<PROJECT_FILE>" clean   # clean

PROJECT_FILE = PROJECT(FLAVOR).mak  (in make/projects folder)

Build Output Directory:
  CP/build/<PROJECT>/<FLAVOR>/...  
  
Project Image File:
  CP/build/<PROJECT>/<FLAVOR>/bin/<PROJECT>_MDBIN_*.bin   
  
  
[MUST]Environment Variable Set:
Please set the environment variable GCC_Path in CP/make.sh before start build!

 
 
