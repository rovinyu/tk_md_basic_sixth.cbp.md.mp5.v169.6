/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/

/*****************************************************************************
* 
* FILE NAME   : hwdrfcustnv.c
*
* DESCRIPTION : Implementations containing global variable and functions
*               to the RF custom file.
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "dbmapi.h"
#include "dbmrfcust.h"
#include "rfsimageinfo.h"
#include "hwdrfpublic.h"
#include "hwdrfcommon.h"
#include "hwdcal.h"
#include "hwdnv.h"
#if (SYS_BOARD >= SB_JADE)
#include "hwdmipipublic.h"
#include "dbmmipi.h"
#include "c2k_custom_mipi.h"
#include "hwdpocpublic.h"
#include "hwddrdi.h"
#include "c2k_custom_drdi.h"
#include "hwdnvcustdefs.h"
#include "hwdrftaspublic.c"
#ifdef __DYNAMIC_ANTENNA_TUNING__
#include "nvram_data_items_id.h"
#endif
#endif

/*----------------------------------------------------------------------------
 Defines and Macros
----------------------------------------------------------------------------*/
#if (SYS_BOARD >= SB_JADE)
/*******************************************************/
/* For the RF Acc Tx Power Offset in 1xrtt.            */
/* Unit is the dBm ,default is 0dBm                      */
/*******************************************************/
#define  C2K_ACC_TX_POWER_DEFAULT_OFFSET           (0)

/*******************************************************/
/* For the Max Tx Pwr Backoff when FCH+SCH in 1xrtt.   */
/* Unit is the 1/2 dBm ,default is 0.5 dBm                    */
/*******************************************************/
#define  C2K_SCH_TX_PWR_BACKOFF_DEFAULT_OFFSET        (1)

#endif

/*----------------------------------------------------------------------------
 Typedefs
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Local Variables
----------------------------------------------------------------------------*/
/*Put these data into FsmDefaultInfo Section, for perl tools to scratch*/
#if defined(__ARMCC_VERSION)
#pragma arm section rodata = "FsmDefaultInfo", rwdata = "FsmDefaultInfo"
#endif
#if (SYS_BOARD < SB_JADE)
/* The default table of RF custom parameters */
const HwdRfCustomDataT hwdRfCustomData =
{
    /* structVersion */
    0,
    /* isDataUpdate */
    1,
    /* Band class supported */
    {
        {CUST_BAND_A, BAND_A_SUPPORTED, HWD_RF_SUB_CLASS_ALL_SUPPORTED},
        {CUST_BAND_B, BAND_B_SUPPORTED, HWD_RF_SUB_CLASS_ALL_SUPPORTED},
        {CUST_BAND_C, BAND_C_SUPPORTED, HWD_RF_SUB_CLASS_ALL_SUPPORTED},
    },
#if defined(MTK_PLT_RF_ORIONC) && !defined(MTK_DEV_HW_SIM_RF)
    {
        {
            /* Band A mask */
            {
                PMASK_BAND_A_PR1,
                PMASK_BAND_A_PR2,
                PMASK_BAND_A_PR2B,
                PMASK_BAND_A_PR3,
                PMASK_BAND_A_PR3A,
                PMASK_BAND_A_PT1,
                PMASK_BAND_A_PT2,
                PMASK_BAND_A_PT2B,
                PMASK_BAND_A_PT3,
                PMASK_BAND_A_PT3A,
                PMASK2_BAND_A_PR1,
                PMASK2_BAND_A_PR2,
                PMASK2_BAND_A_PR2B,
                PMASK2_BAND_A_PR3,
                PMASK2_BAND_A_PR3A,
    
                /* TX gate mask */
                PMASK_BAND_A_PRG1,
                PMASK_BAND_A_PRG2,
                PMASK_BAND_A_PRG2B,
                PMASK_BAND_A_PRG3,
                PMASK_BAND_A_PRG3A,
                PMASK_BAND_A_PTG1,
                PMASK_BAND_A_PTG2,
                PMASK_BAND_A_PTG2B,
                PMASK_BAND_A_PTG3,
                PMASK_BAND_A_PTG3A,
                PMASK2_BAND_A_PRG1,
                PMASK2_BAND_A_PRG2,
                PMASK2_BAND_A_PRG2B,
                PMASK2_BAND_A_PRG3,
                PMASK2_BAND_A_PRG3A,
            },
            /* Band B mask */
            {
                PMASK_BAND_B_PR1,
                PMASK_BAND_B_PR2,
                PMASK_BAND_B_PR2B,
                PMASK_BAND_B_PR3,
                PMASK_BAND_B_PR3A,
                PMASK_BAND_B_PT1,
                PMASK_BAND_B_PT2,
                PMASK_BAND_B_PT2B,
                PMASK_BAND_B_PT3,
                PMASK_BAND_B_PT3A,
                PMASK2_BAND_B_PR1,
                PMASK2_BAND_B_PR2,
                PMASK2_BAND_B_PR2B,
                PMASK2_BAND_B_PR3,
                PMASK2_BAND_B_PR3A,
    
                /* TX gate mask */
                PMASK_BAND_B_PRG1,
                PMASK_BAND_B_PRG2,
                PMASK_BAND_B_PRG2B,
                PMASK_BAND_B_PRG3,
                PMASK_BAND_B_PRG3A,
                PMASK_BAND_B_PTG1,
                PMASK_BAND_B_PTG2,
                PMASK_BAND_B_PTG2B,
                PMASK_BAND_B_PTG3,
                PMASK_BAND_B_PTG3A,
                PMASK2_BAND_B_PRG1,
                PMASK2_BAND_B_PRG2,
                PMASK2_BAND_B_PRG2B,
                PMASK2_BAND_B_PRG3,
                PMASK2_BAND_B_PRG3A,
            },
            /* Band C mask */
            {
                PMASK_BAND_C_PR1,
                PMASK_BAND_C_PR2,
                PMASK_BAND_C_PR2B,
                PMASK_BAND_C_PR3,
                PMASK_BAND_C_PR3A,
                PMASK_BAND_C_PT1,
                PMASK_BAND_C_PT2,
                PMASK_BAND_C_PT2B,
                PMASK_BAND_C_PT3,
                PMASK_BAND_C_PT3A,
                PMASK2_BAND_C_PR1,
                PMASK2_BAND_C_PR2,
                PMASK2_BAND_C_PR2B,
                PMASK2_BAND_C_PR3,
                PMASK2_BAND_C_PR3A,
    
                /* TX gate mask */
                PMASK_BAND_C_PRG1,
                PMASK_BAND_C_PRG2,
                PMASK_BAND_C_PRG2B,
                PMASK_BAND_C_PRG3,
                PMASK_BAND_C_PRG3A,
                PMASK_BAND_C_PTG1,
                PMASK_BAND_C_PTG2,
                PMASK_BAND_C_PTG2B,
                PMASK_BAND_C_PTG3,
                PMASK_BAND_C_PTG3A,
                PMASK2_BAND_C_PRG1,
                PMASK2_BAND_C_PRG2,
                PMASK2_BAND_C_PRG2B,
                PMASK2_BAND_C_PRG3,
                PMASK2_BAND_C_PRG3A,
            }
        },
        /* Power Control BPI mask */
        {
            PMASK_PRPC1,
            PMASK_PRPC2,
            PMASK_PRPC2B,
            PMASK_PRPC3,
            PMASK_PRPC3A,
            PMASK_PTPC1,
            PMASK_PTPC2,
            PMASK_PTPC2B,
            PMASK_PTPC3,
            PMASK_PTPC3A,
            PMASK2_PRPC1,
            PMASK2_PRPC2,
            PMASK2_PRPC2B,
            PMASK2_PRPC3,
            PMASK2_PRPC3A,
        }
    },
    {
        {
            /* Band A data */
            {
                PDATA_BAND_A_PR1,
                PDATA_BAND_A_PR2,
                PDATA_BAND_A_PR2B,
                PDATA_BAND_A_PR3,
                PDATA_BAND_A_PR3A,
                PDATA_BAND_A_PT1,
                PDATA_BAND_A_PT2,
                PDATA_BAND_A_PT2B,
                PDATA_BAND_A_PT3,
                PDATA_BAND_A_PT3A,
                PDATA2_BAND_A_PR1,
                PDATA2_BAND_A_PR2,
                PDATA2_BAND_A_PR2B,
                PDATA2_BAND_A_PR3,
                PDATA2_BAND_A_PR3A,
    
                /* TX gate data */
                PDATA_BAND_A_PRG1,
                PDATA_BAND_A_PRG2,
                PDATA_BAND_A_PRG2B,
                PDATA_BAND_A_PRG3,
                PDATA_BAND_A_PRG3A,
                PDATA_BAND_A_PTG1,
                PDATA_BAND_A_PTG2,
                PDATA_BAND_A_PTG2B,
                PDATA_BAND_A_PTG3,
                PDATA_BAND_A_PTG3A,
                PDATA2_BAND_A_PRG1,
                PDATA2_BAND_A_PRG2,
                PDATA2_BAND_A_PRG2B,
                PDATA2_BAND_A_PRG3,
                PDATA2_BAND_A_PRG3A,
            },
            /* Band B data */
            {
                PDATA_BAND_B_PR1,
                PDATA_BAND_B_PR2,
                PDATA_BAND_B_PR2B,
                PDATA_BAND_B_PR3,
                PDATA_BAND_B_PR3A,
                PDATA_BAND_B_PT1,
                PDATA_BAND_B_PT2,
                PDATA_BAND_B_PT2B,
                PDATA_BAND_B_PT3,
                PDATA_BAND_B_PT3A,
                PDATA2_BAND_B_PR1,
                PDATA2_BAND_B_PR2,
                PDATA2_BAND_B_PR2B,
                PDATA2_BAND_B_PR3,
                PDATA2_BAND_B_PR3A,
    
                /* TX gate data */
                PDATA_BAND_B_PRG1,
                PDATA_BAND_B_PRG2,
                PDATA_BAND_B_PRG2B,
                PDATA_BAND_B_PRG3,
                PDATA_BAND_B_PRG3A,
                PDATA_BAND_B_PTG1,
                PDATA_BAND_B_PTG2,
                PDATA_BAND_B_PTG2B,
                PDATA_BAND_B_PTG3,
                PDATA_BAND_B_PTG3A,
                PDATA2_BAND_B_PRG1,
                PDATA2_BAND_B_PRG2,
                PDATA2_BAND_B_PRG2B,
                PDATA2_BAND_B_PRG3,
                PDATA2_BAND_B_PRG3A,
            },
            /* Band C data */
            {
                PDATA_BAND_C_PR1,
                PDATA_BAND_C_PR2,
                PDATA_BAND_C_PR2B,
                PDATA_BAND_C_PR3,
                PDATA_BAND_C_PR3A,
                PDATA_BAND_C_PT1,
                PDATA_BAND_C_PT2,
                PDATA_BAND_C_PT2B,
                PDATA_BAND_C_PT3,
                PDATA_BAND_C_PT3A,
                PDATA2_BAND_C_PR1,
                PDATA2_BAND_C_PR2,
                PDATA2_BAND_C_PR2B,
                PDATA2_BAND_C_PR3,
                PDATA2_BAND_C_PR3A,
    
                /* TX gate data */
                PDATA_BAND_C_PRG1,
                PDATA_BAND_C_PRG2,
                PDATA_BAND_C_PRG2B,
                PDATA_BAND_C_PRG3,
                PDATA_BAND_C_PRG3A,
                PDATA_BAND_C_PTG1,
                PDATA_BAND_C_PTG2,
                PDATA_BAND_C_PTG2B,
                PDATA_BAND_C_PTG3,
                PDATA_BAND_C_PTG3A,
                PDATA2_BAND_C_PRG1,
                PDATA2_BAND_C_PRG2,
                PDATA2_BAND_C_PRG2B,
                PDATA2_BAND_C_PRG3,
                PDATA2_BAND_C_PRG3A,
            }
        },
        {
            /* Power Control BPI data */
            PDATA_PRPC1,
            PDATA_PRPC2,
            PDATA_PRPC2B,
            PDATA_PRPC3,
            PDATA_PRPC3A,
            PDATA_PTPC1,
            PDATA_PTPC2,
            PDATA_PTPC2B,
            PDATA_PTPC3,
            PDATA_PTPC3A,
            PDATA2_PRPC1,
            PDATA2_PRPC2,
            PDATA2_PRPC2B,
            PDATA2_PRPC3,
            PDATA2_PRPC3A,
        }
    },
#endif
    /*** BPI control timing */
    {
        /* RF window timing */
        M_UsToChips(TC_PR1),
        M_UsToChips(TC_PR2),
        M_UsToChips(TC_PR2B),

        M_UsToChips(TC_PR3),
        M_UsToChips(TC_PR3A),

        M_UsToChips(TC_RXD_PR1),
        M_UsToChips(TC_RXD_PR2),
        M_UsToChips(TC_RXD_PR2B),

        M_UsToChips(TC_RXD_PR3),
        M_UsToChips(TC_RXD_PR3A),

        M_UsToChips(TC_PT1),
        M_UsToChips(TC_PT2),
        M_UsToChips(TC_PT2B),

        M_UsToChips(TC_PT3),
        M_UsToChips(TC_PT3A),

        /* RF Gate timing */
        M_UsToChips(TC_PRG1),
        M_UsToChips(TC_PRG2),
        M_UsToChips(TC_PRG2B),

        M_UsToChips(TC_PRG3),
        M_UsToChips(TC_PRG3A),

        M_UsToChips(TC_RXD_PRG1),
        M_UsToChips(TC_RXD_PRG2),
        M_UsToChips(TC_RXD_PRG2B),

        M_UsToChips(TC_RXD_PRG3),
        M_UsToChips(TC_RXD_PRG3A),

        M_UsToChips(TC_PTG1),
        M_UsToChips(TC_PTG2),
        M_UsToChips(TC_PTG2B),

        M_UsToChips(TC_PTG3),
        M_UsToChips(TC_PTG3A),

        /* RF Power Control timing */
        M_UsToChips(TC_PRPC1),
        M_UsToChips(TC_PRPC2),
        M_UsToChips(TC_PRPC2B),

        M_UsToChips(TC_PRPC3),
        M_UsToChips(TC_PRPC3A),

        M_UsToChips(TC_RXD_PRPC1),
        M_UsToChips(TC_RXD_PRPC2),
        M_UsToChips(TC_RXD_PRPC2B),

        M_UsToChips(TC_RXD_PRPC3),
        M_UsToChips(TC_RXD_PRPC3A),

        M_UsToChips(TC_PTPC1),
        M_UsToChips(TC_PTPC2),
        M_UsToChips(TC_PTPC2B),

        M_UsToChips(TC_PTPC3),
        M_UsToChips(TC_PTPC3A),
    },
    {
        M_UsToChips(DC2DC_OFFSET),
    },
    /* RX LNA port selection */
    {
        BAND_A_RX_IO_SEL,
        BAND_B_RX_IO_SEL,
        BAND_C_RX_IO_SEL,
    },
    /* RX Diverisity LNA port selection */
    {
        BAND_A_RXD_IO_SEL,
        BAND_B_RXD_IO_SEL,
        BAND_C_RXD_IO_SEL,
    },
    /* TX path selection */
    {
        BAND_A_TX_IO_SEL,
        BAND_B_TX_IO_SEL,
        BAND_C_TX_IO_SEL,
    },
    /* Rx diversity enable */
    RX_DIVERSITY_ENABLE,
    /* Rx diversity test enable. The diversity path will be always on if this
       is TRUE. Used by HSC */
    RX_DIVERSITY_ONLY_TEST,
    /* PMU VPA control disable/enable */
    PA_VDD_PMU_ENABLE,
    /* BATT VPA control disable/enable */
    PA_VDD_BATT_ENABLE,
    /* DC2DC VPA control disable/enable */
    PA_VDD_DC2DC_ENABLE,
    /* DC2DC VPA control disable/enable */
    PA_VDD_HPUE_ENABLE,
    /* Internal temperature measurement enable*/
    TEMPERATURE_MEAS_EN,
    /*SAR feature disable/enable*/
    TPO_ENABLE,
    /*SAR feature metamode disable/enable*/
    TPO_META_ENABLE
};


/* TAS default configurations */
const HwdRfCustTasT hwdRfCustomTas =
{
    /* TAS enable */
    C2K_TAS_ENABLE,
    /* TAS initial antenna index */
    C2K_TAS_INIT_ANT,
    /* Force Tx antenna enalbe */
    C2K_FORCE_TX_ANTENNA_ENABLE,
    /* Forced Tx antenna index */
    C2K_FORCE_TX_ANTENNA_IDX,
    /* TAS data mask */
    C2K_TAS_BPI_MASK,
    /* TAS data by Band */
    {
        /* Band A */
        {
            PDATA_BAND_A_TAS1,
            PDATA_BAND_A_TAS2,
            PDATA_BAND_A_TAS3,
            PDATA_BAND_A_TAS4,
            PDATA_BAND_A_TAS5,
            PDATA_BAND_A_TAS6,
            PDATA_BAND_A_TAS7
        },
        /* Band B */
        {
            PDATA_BAND_B_TAS1,
            PDATA_BAND_B_TAS2,
            PDATA_BAND_B_TAS3,
            PDATA_BAND_B_TAS4,
            PDATA_BAND_B_TAS5,
            PDATA_BAND_B_TAS6,
            PDATA_BAND_B_TAS7
        },
        /* Band C */
        {
            PDATA_BAND_C_TAS1,
            PDATA_BAND_C_TAS2,
            PDATA_BAND_C_TAS3,
            PDATA_BAND_C_TAS4,
            PDATA_BAND_C_TAS5,
            PDATA_BAND_C_TAS6,
            PDATA_BAND_C_TAS7
        },
    }
};

const uint8 dummyCustData = 0;
const uint8 dummyCalData = 0;
#else
/***** RF PARAMs ******/
RF_CUST_CUSTOM_DATA(SetDefault);
RF_CUST_BPI_MASK(SetDefault);
RF_CUST_BPI_DATA(SetDefault);
RF_CUST_TAS_DATA(SetDefault);
#endif

#ifdef __DYNAMIC_ANTENNA_TUNING__
#include "c2k_custom_rf_dat.h"
#include "c2k_custom_rf_dat.c"
C2K_DAT_FE_ROUTE_DATABASE(SetDefault);
C2K_DAT_BPI_DATABASE(SetDefault);
C2K_DAT_CAT_A_MIPI_EVENT_TABLE(SetDefault);
C2K_DAT_CAT_A_MIPI_DATA_TABLE(SetDefault);
C2K_DAT_CAT_B_MIPI_EVENT_TABLE(SetDefault);
C2K_DAT_CAT_B_MIPI_DATA_TABLE0(SetDefault);
C2K_DAT_CAT_B_MIPI_DATA_TABLE1(SetDefault);
C2K_DAT_CAT_B_MIPI_DATA_TABLE2(SetDefault);
C2K_DAT_CAT_B_MIPI_DATA_TABLE3(SetDefault);
#endif
#if (SYS_BOARD >= SB_JADE)
/* TAS default configurations */
const Hwd1xL1dRfCustT Hwd1xL1dRfCust =
{
    /* 1X Acc Tx Power Offset(1/8 dBm) */
    C2K_ACC_TX_POWER_DEFAULT_OFFSET,
    /* 1X Sch Tx Power Backoff(1/2 dBm) */
    C2K_SCH_TX_PWR_BACKOFF_DEFAULT_OFFSET
};
#endif

/***** RF Calibration Data ******/
#include "c2k_custom_rf.c"

#if (SYS_BOARD >= SB_JADE)

/***** MIPI PARAMs ******/
MIPI_CUSTOM_PARAM(SetDefault);
#include "c2k_custom_mipi.c"

/***** DRDI PARAMs ******/
#include "hwddrdi.c"
#include "c2k_custom_drdi.c"

/***** POC PARAMs ******/
const HwdPocStateT HwdPocState =
{
    POC_NOT_INIT,
};
const MMRFC_C2K_RESULT_T HwdRfPocResult =
{
    0x00B16BAD,
    0xB0B0FEED
};

/***** DRDI PARAMs ******/
/* const */HwdDrdiParamT HwdDrdiParam =
{
    IS_C2K_DRDI_ENABLE,
    C2K_DEBUG_ENABLE,
    C2K_TOTAL_SET_NUMS,
    C2K_TOTAL_REAL_SET_NUMS
};
#if defined (HWD_ETM_NV_ENABLE)   
/***** ETM PARAMs ******/
#include "c2k_custom_etm.c"
#endif
/* const */HwdDrdiParam2T HwdDrdiParam2 =
{
    C2K_DEBUG_DEFAULT_SET_INDEX
};

#endif

/***** DBM Seg Info List ******/
#undef DBM_RF_CUST_ITEM
#if (SYS_BOARD >= SB_JADE)
#define DBM_RF_CUST_ITEM(nAME, sIZE, dATA, iNPUTdATA, fUNC,bAND) {DBM_##nAME##_SEG, (sIZE), NULL, 0},
#else
#define DBM_RF_CUST_ITEM(nAME, sIZE, dATA, iNPUTdATA, fUNC,bAND) {DBM_##nAME##_SEG, (sIZE), (void *)&dATA, sizeof(dATA)},
#endif
const SegInfoT rfCustFileSegInfoList[] =
{
#include "dbmrfcustid.h"
};

#undef DBM_RF_CAL_ITEM
#if (SYS_BOARD >= SB_JADE)
#define DBM_RF_CAL_ITEM(nAME, sIZE, dATA, fUNC, bAND)  {DBM_##nAME##_SEG, (sIZE), NULL, 0},
#else
#define DBM_RF_CAL_ITEM(nAME, sIZE, dATA, fUNC, bAND)  {DBM_##nAME##_SEG, (sIZE), (void *)&dATA, sizeof(dATA)},
#endif
const SegInfoT rfCalSegInfoList[] =
{
#include "dbmrfcalid.h"
};

#if (SYS_BOARD >= SB_JADE)
#undef DBM_MIPI_ITEM
#define DBM_MIPI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) {DBM_##nAME##_SEG, (sIZE), NULL, 0},
const SegInfoT mipiSegInfoList[] =
{
#include "dbmmipiid.h"
};

#undef DBM_POC_ITEM
#define DBM_POC_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) {DBM_##nAME##_SEG, (sIZE), (void *)&dEFAULTdATA, sizeof(dEFAULTdATA)},
const SegInfoT pocSegInfoList[] =
{
#include "dbmpocid.h"
};

#undef DBM_DRDI_ITEM
#define DBM_DRDI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC) {DBM_##nAME##_SEG, (sIZE), (void *)&dEFAULTdATA, sizeof(dEFAULTdATA)},
const SegInfoT drdiSegInfoList[] =
{
#include "dbmdrdiid.h"
};


/************************* Default Pointer and Size *********************/

/******************** RF PARAM **********************/
void* HwdRfCustCustomDataSetDefaultPtr[DBM_MAX_SEG_RF_CUST_DB] =
{
    #undef DBM_RF_CUST_ITEM
    #define DBM_RF_CUST_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC,bAND) (void *)&dEFAULTdATA##_SetDefault,
    #include "dbmrfcustid.h"
};

uint32 HwdRfCustCustomDataSetDefaultSize[DBM_MAX_SEG_RF_CUST_DB] =
{
    #undef DBM_RF_CUST_ITEM
    #define DBM_RF_CUST_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC,bAND) sizeof(dEFAULTdATA##_SetDefault),
    #include "dbmrfcustid.h"
};

/******************** RF CAL **********************/
void* HwdRfCalCustomDataSetDefaultPtr[DBM_MAX_SEG_RF_CAL_DB] =
{
    #undef DBM_RF_CAL_ITEM
    #define DBM_RF_CAL_ITEM(nAME, sIZE, dEFAULTdATA, fUNC, bAND) (void *)&dEFAULTdATA##_SetDefault,
    #include "dbmrfcalid.h"
};

uint32 HwdRfCalCustomDataSetDefaultSize[DBM_MAX_SEG_RF_CAL_DB] =
{
    #undef DBM_RF_CAL_ITEM
    #define DBM_RF_CAL_ITEM(nAME, sIZE, dEFAULTdATA, fUNC, bAND) sizeof(dEFAULTdATA##_SetDefault),
    #include "dbmrfcalid.h"
};

/******************** MIPI **********************/
void* HwdMipiCustomDataSetDefaultPtr[DBM_MAX_SEG_MIPI_DB] =
{
    #undef DBM_MIPI_ITEM
    #define DBM_MIPI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) (void *)dEFAULTdATA##_SetDefault,
    #include "dbmmipiid.h"
};

uint32 HwdMipiCustomDataSetDefaultSize[DBM_MAX_SEG_MIPI_DB] =
{
    #undef DBM_MIPI_ITEM
    #define DBM_MIPI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) sizeof(dEFAULTdATA##_SetDefault),
    #include "dbmmipiid.h"
};
#endif

#if defined(__ARMCC_VERSION)
#pragma arm section rodata, rwdata
#endif


#ifdef __DYNAMIC_ANTENNA_TUNING__

void NvramAssignDatCustDefault(NvramLIDEnumT LID,uint8 *Buffer, uint16 BufSize)
{
    uint32 drdiEn, setIdx;
	uint16 segIdx = 0;
	bool setIdxValid = FALSE;
	uint16 routeindex = 0;
    
	drdiEn = HwdDrdiEnable();
	setIdxValid = HwdDrdiGetSetIndex(TRUE, &setIdx);
	if(NVRAM_EF_HWD_DAT_FE_BPI_DATABASE_LID == LID)
	{
	   segIdx = HwdNvLidToIdx(&HwdRfCustSegProcessInfo, LID);
	}
	else
	{
       segIdx = HwdNvLidToIdx(&HwdMipiSegProcessInfo, LID);
	}
	
	C2K_CUSTOM_DAT_FE_DATABASE_T  *C2K_rf_BPI_DAT_data_buffer = NULL;
	C2K_MIPI_DAT_Event_CatA_T  *C2K_rf_mipi_CatA_DAT_EVENT_buffer = NULL;	
	C2K_MIPI_DAT_Event_CatB_T  *C2K_rf_mipi_CatB_DAT_EVENT_buffer = NULL;
	C2K_MIPI_DAT_Data_CatA_T   *C2K_rf_mipi_CatA_DAT_DATA_buffer = NULL;	
	C2K_MIPI_DAT_Data_CatB_T   *C2K_rf_mipi_CatB_DAT_DATA_buffer= NULL;
	
	MonAssert(setIdx < C2K_TOTAL_REAL_SET_NUMS, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, setIdx, MON_HALT);
    MonAssert(segIdx < DBM_MAX_SEG_MIPI_DB, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, segIdx, MON_HALT);

    SysMemset(Buffer, 0x0, BufSize);
	if (drdiEn && setIdxValid)
	{
	   switch(LID)
       	{
      case NVRAM_EF_HWD_DAT_FE_BPI_DATABASE_LID:
		  	 C2K_rf_BPI_DAT_data_buffer =(C2K_CUSTOM_DAT_FE_DATABASE_T*)malloc(sizeof(C2K_CUSTOM_DAT_FE_DATABASE_T));
			 memcpy(&(C2K_rf_BPI_DAT_data_buffer->dat_cat_a_fe_db),((HwdCustomDynamicDatBpiData*)HwdRfCustCustomDataSetNPtr[setIdx][segIdx])->dat_cat_a_fe_db,sizeof(C2K_CUSTOM_DAT_FE_CAT_A_T));
			 memcpy(&(C2K_rf_BPI_DAT_data_buffer->dat_cat_b_fe_db),((HwdCustomDynamicDatBpiData*)HwdRfCustCustomDataSetNPtr[setIdx][segIdx])->dat_cat_b_fe_db,sizeof(C2K_CUSTOM_DAT_FE_CAT_B_T));			 

			 memcpy(Buffer,C2K_rf_BPI_DAT_data_buffer,sizeof(C2K_CUSTOM_DAT_FE_DATABASE_T));
			 free(C2K_rf_BPI_DAT_data_buffer);
			break;

       case NVRAM_EF_HWD_DAT_CAT_A_MIPI_EVENT_TABLE_LID:	  	
			C2K_rf_mipi_CatA_DAT_EVENT_buffer =(C2K_MIPI_DAT_Event_CatA_T*)malloc(sizeof(C2K_MIPI_DAT_Event_CatA_T));
    		for(routeindex  =0; routeindex< C2K_DAT_MAX_CAT_A_ROUTE_NUM; routeindex++)
			 {
			     memcpy(C2K_rf_mipi_CatA_DAT_EVENT_buffer->mipiDatEventRoute[routeindex].mipiDatEvent,(((MIPI_EVENT_TABLE_T **)HwdMipiCustomDataSetNPtr[setIdx][segIdx])[routeindex]),sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM);
    		 }				
			     memcpy(Buffer, C2K_rf_mipi_CatA_DAT_EVENT_buffer,sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM*C2K_DAT_MAX_CAT_A_ROUTE_NUM); 
			 free(C2K_rf_mipi_CatA_DAT_EVENT_buffer);
			break;
			
       	case NVRAM_EF_HWD_DAT_CAT_B_MIPI_EVENT_TABLE_LID:
			C2K_rf_mipi_CatB_DAT_EVENT_buffer =(C2K_MIPI_DAT_Event_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Event_CatB_T));
		  	for(routeindex  =0; routeindex< C2K_DAT_MAX_CAT_B_ROUTE_NUM; routeindex++)
			{
			    memcpy(C2K_rf_mipi_CatB_DAT_EVENT_buffer->mipiDatEventRoute[routeindex].mipiDatEvent,(((MIPI_EVENT_TABLE_T **)HwdMipiCustomDataSetNPtr[setIdx][segIdx])[routeindex]),sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM);
			}
			
			    memcpy(Buffer, C2K_rf_mipi_CatB_DAT_EVENT_buffer,sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM*C2K_DAT_MAX_CAT_B_ROUTE_NUM);
			free(C2K_rf_mipi_CatB_DAT_EVENT_buffer);
			break;
			
       	case NVRAM_EF_HWD_DAT_CAT_A_MIPI_DATA_TABLE_LID:
			C2K_rf_mipi_CatA_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatA_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatA_T));
		    for(routeindex  =0; routeindex< C2K_DAT_MAX_CAT_A_ROUTE_NUM; routeindex++)
			{
				memcpy(C2K_rf_mipi_CatA_DAT_DATA_buffer->mipiDatDataRoute[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T **)HwdMipiCustomDataSetNPtr[setIdx][segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
			}
		    memcpy(Buffer, C2K_rf_mipi_CatA_DAT_DATA_buffer,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_MAX_CAT_A_ROUTE_NUM);
			free(C2K_rf_mipi_CatA_DAT_DATA_buffer);
			break;
			
       	case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE0_LID:
			C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
		  	for(routeindex  =0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
			{
			   memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute0[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetNPtr[setIdx][segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
			}
		    memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute0,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM);
		    free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
			break;

       	 case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE1_LID:		  	
			C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
		  	for(routeindex  =0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
			{
			   memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute1[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetNPtr[setIdx][segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
			}	
			memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute1,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM);
			free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
			break;

		 case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE2_LID:	
			C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
		  	for(routeindex  =0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
			{
			    memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute2[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetNPtr[setIdx][segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
			}	
			memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute2,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM);
			free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
			break;
			
		case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE3_LID:	
			C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
		   for(routeindex  =0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
		   {
			  memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute3[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetNPtr[setIdx][segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
		   }  
		   memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute3,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM); 
		   free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
		  break;
       	  default:
       		 break;
       	}
	}
	else
	{
      switch(LID)
      {
     case NVRAM_EF_HWD_DAT_FE_BPI_DATABASE_LID:
 	  	 C2K_rf_BPI_DAT_data_buffer =(C2K_CUSTOM_DAT_FE_DATABASE_T*)malloc(sizeof(C2K_CUSTOM_DAT_FE_DATABASE_T));
 		 memcpy(&(C2K_rf_BPI_DAT_data_buffer->dat_cat_a_fe_db),((HwdCustomDynamicDatBpiData*)HwdRfCustCustomDataSetDefaultPtr[segIdx])->dat_cat_a_fe_db,sizeof(C2K_CUSTOM_DAT_FE_CAT_A_T));
 		 memcpy(&(C2K_rf_BPI_DAT_data_buffer->dat_cat_b_fe_db),((HwdCustomDynamicDatBpiData*)HwdRfCustCustomDataSetDefaultPtr[segIdx])->dat_cat_b_fe_db,sizeof(C2K_CUSTOM_DAT_FE_CAT_B_T));			 
 
 		 memcpy(Buffer,C2K_rf_BPI_DAT_data_buffer,sizeof(C2K_CUSTOM_DAT_FE_DATABASE_T));
 		 free(C2K_rf_BPI_DAT_data_buffer);
 		break;
 		
     case NVRAM_EF_HWD_DAT_CAT_A_MIPI_EVENT_TABLE_LID:	  	
 		C2K_rf_mipi_CatA_DAT_EVENT_buffer =(C2K_MIPI_DAT_Event_CatA_T*)malloc(sizeof(C2K_MIPI_DAT_Event_CatA_T));
 		for(routeindex  =0; routeindex< C2K_DAT_MAX_CAT_A_ROUTE_NUM; routeindex++)
 		 {
 		     memcpy(C2K_rf_mipi_CatA_DAT_EVENT_buffer->mipiDatEventRoute[routeindex].mipiDatEvent,(((MIPI_EVENT_TABLE_T **)HwdMipiCustomDataSetDefaultPtr[segIdx])[routeindex]),sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM);
 		 }				
 		 memcpy(Buffer, C2K_rf_mipi_CatA_DAT_EVENT_buffer,sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM*C2K_DAT_MAX_CAT_A_ROUTE_NUM); 
 		 free(C2K_rf_mipi_CatA_DAT_EVENT_buffer);
 		break;
 		
     case NVRAM_EF_HWD_DAT_CAT_B_MIPI_EVENT_TABLE_LID:
 		C2K_rf_mipi_CatB_DAT_EVENT_buffer =(C2K_MIPI_DAT_Event_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Event_CatB_T));
 	  	for(routeindex  =0; routeindex< C2K_DAT_MAX_CAT_B_ROUTE_NUM; routeindex++)
 		{
 		    memcpy(C2K_rf_mipi_CatB_DAT_EVENT_buffer->mipiDatEventRoute[routeindex].mipiDatEvent,(((MIPI_EVENT_TABLE_T **)HwdMipiCustomDataSetDefaultPtr[segIdx])[routeindex]),sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM);
 		}
 			
 		memcpy(Buffer, C2K_rf_mipi_CatB_DAT_EVENT_buffer,sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_EVENT_NUM*C2K_DAT_MAX_CAT_B_ROUTE_NUM);
 		free(C2K_rf_mipi_CatB_DAT_EVENT_buffer);
 		break;
 		
     case NVRAM_EF_HWD_DAT_CAT_A_MIPI_DATA_TABLE_LID:
 		C2K_rf_mipi_CatA_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatA_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatA_T));
 	    for(routeindex  =0; routeindex< C2K_DAT_MAX_CAT_A_ROUTE_NUM; routeindex++)
 		{
 			memcpy(C2K_rf_mipi_CatA_DAT_DATA_buffer->mipiDatDataRoute[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T **)HwdMipiCustomDataSetDefaultPtr[segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
 		}
 		
	    memcpy(Buffer, C2K_rf_mipi_CatA_DAT_DATA_buffer,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_MAX_CAT_A_ROUTE_NUM);
		free(C2K_rf_mipi_CatA_DAT_DATA_buffer);
 		break;
 		
     case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE0_LID:
 		C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
 	  	for(routeindex  = 0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
 		{
 		   memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute0[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetDefaultPtr[segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
		}
 		memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute0,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM);
 		free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
 		break;
 
     case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE1_LID:		  	
 		C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
 	  	for(routeindex  =0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
 		{
 		   memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute1[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetDefaultPtr[segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);

		}	
 		memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute1,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM);
 		free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
 		break;
 
     case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE2_LID:	
 		C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
 	  	for(routeindex  =0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
 		{
 		    memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute2[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetDefaultPtr[segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);

		}	
 		memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute2,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM);
 		free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
 		break;
 		
     case NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE3_LID:	
 		C2K_rf_mipi_CatB_DAT_DATA_buffer =(C2K_MIPI_DAT_Data_CatB_T*)malloc(sizeof(C2K_MIPI_DAT_Data_CatB_T));
 	   for(routeindex  =0; routeindex< C2K_DAT_CAT_B_ROUTE_NUM; routeindex++)
 	   {
 		  memcpy(C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute3[routeindex].mipiDatData,(((MIPI_DATA_SUBBAND_TABLE_T**)HwdMipiCustomDataSetDefaultPtr[segIdx])[routeindex]),sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM);
	   }  
 	   memcpy(Buffer, C2K_rf_mipi_CatB_DAT_DATA_buffer->mipiDatDataRoute3,sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_DATA_NUM*C2K_DAT_CAT_B_ROUTE_NUM);   
 	   free(C2K_rf_mipi_CatB_DAT_DATA_buffer);
 	  break;
 
     default:
       break;
    	}
  }	
}
#endif

#if (SYS_BOARD >= SB_JADE)
void NvramAssignRfCustDefault(NvramLIDEnumT LID, uint8 *Buffer, uint16 BufSize)
{
    uint32 drdiEn, setIdx;
    uint16 segIdx = 0;
    bool setIdxValid = FALSE;

    drdiEn = HwdDrdiEnable();
    setIdxValid = HwdDrdiGetSetIndex(TRUE, &setIdx);
    segIdx = HwdNvLidToIdx(&HwdRfCustSegProcessInfo, LID);

    MonAssert(setIdx < C2K_TOTAL_REAL_SET_NUMS, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, setIdx, MON_HALT);
    MonAssert(segIdx < DBM_MAX_SEG_RF_CUST_DB, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, segIdx, MON_HALT);

    SysMemset(Buffer, 0x0, BufSize);
    if (drdiEn && setIdxValid)
    {
        uint32 segSize = HwdRfCustCustomDataSetNSize[setIdx][segIdx];

        if (segSize > BufSize)
        {
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, setIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, BufSize, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segSize, MON_HALT);
        }
        else
        {
            memcpy(Buffer, HwdRfCustCustomDataSetNPtr[setIdx][segIdx], segSize);
        }
    }
    else
    {
        uint32 segSize = HwdRfCustCustomDataSetDefaultSize[segIdx];

        if (segSize > BufSize)
        {
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, setIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, BufSize, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segSize, MON_HALT);
        }
        else
        {
            memcpy(Buffer, HwdRfCustCustomDataSetDefaultPtr[segIdx], segSize);
        }
    }
}


void NvramAssignRfCalDefault(NvramLIDEnumT LID, uint8 *Buffer, uint16 BufSize)
{
    uint32 drdiEn, setIdx;
    uint16 segIdx = 0;
    bool setIdxValid = FALSE;

    drdiEn = HwdDrdiEnable();
    setIdxValid = HwdDrdiGetSetIndex(TRUE, &setIdx);
    segIdx = HwdNvLidToIdx(&HwdRfCalSegProcessInfo, LID);

    MonAssert(setIdx < C2K_TOTAL_REAL_SET_NUMS, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, setIdx, MON_HALT);
    MonAssert(segIdx < DBM_MAX_SEG_RF_CAL_DB, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, segIdx, MON_HALT);

    SysMemset(Buffer, 0x0, BufSize);
    if (drdiEn && setIdxValid)
    {
        uint32 segSize = HwdRfCalCustomDataSetNSize[setIdx][segIdx];

        if (segSize > BufSize)
        {
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, setIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, BufSize, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segSize, MON_HALT);
        }
        else
        {
            memcpy(Buffer, HwdRfCalCustomDataSetNPtr[setIdx][segIdx], segSize);
        }
    }
    else
    {
        uint32 segSize = HwdRfCalCustomDataSetDefaultSize[segIdx];

        if (segSize > BufSize)
        {
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, setIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, BufSize, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segSize, MON_HALT);
        }
        else
        {
            memcpy(Buffer, HwdRfCalCustomDataSetDefaultPtr[segIdx], segSize);
        }
    }
}


void NvramAssignMipiDefault(NvramLIDEnumT LID, uint8 *Buffer, uint16 BufSize)
{
    uint32 drdiEn, setIdx;
    uint16 segIdx = 0;
    bool setIdxValid = FALSE;

    drdiEn = HwdDrdiEnable();
    setIdxValid = HwdDrdiGetSetIndex(TRUE, &setIdx);
    segIdx = HwdNvLidToIdx(&HwdMipiSegProcessInfo, LID);

    MonAssert(setIdx < C2K_TOTAL_REAL_SET_NUMS, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, setIdx, MON_HALT);
    MonAssert(segIdx < DBM_MAX_SEG_MIPI_DB, MON_HWD_FAULT_UNIT, HWD_ERR_DBM_DATA_ERR, segIdx, MON_HALT);

    SysMemset(Buffer, 0x0, BufSize);
    if (drdiEn && setIdxValid)
    {
        uint32 segSize = HwdMipiCustomDataSetNSize[setIdx][segIdx];

        if (segSize > BufSize)
        {
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, setIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, BufSize, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segSize, MON_HALT);
        }
        else
        {
            memcpy(Buffer, HwdMipiCustomDataSetNPtr[setIdx][segIdx], segSize);
        }
    }
    else
    {
        uint32 segSize = HwdMipiCustomDataSetDefaultSize[segIdx];

        if (segSize > BufSize)
        {
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, setIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segIdx, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, BufSize, MON_CONTINUE);
            MonFault(MON_HWD_FAULT_UNIT, HWD_ERR_DRDI, segSize, MON_HALT);
        }
        else
        {
            memcpy(Buffer, HwdMipiCustomDataSetDefaultPtr[segIdx], segSize);
        }
    }
}
#endif


/*****************************************************************************
* End of File
*****************************************************************************/


