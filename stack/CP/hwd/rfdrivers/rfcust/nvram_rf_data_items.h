/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 * Filename:
 * ---------
 * nvram_rf_data_items_rf.h
 *
 * Project:
 * --------
 *   C2K
 *
 * Description:
 * ------------
 *    This file defines logical data items information related with RF stored in NVRAM
 *
 *
 *
 *****************************************************************************/
 #ifndef __NVRAM_RF_DATA_ITEMS_H__
 #define __NVRAM_RF_DATA_ITEMS_H__

#include "nvram_defs.h"
#include "hwdmipipublic.h"
#include "hwdrfpublic.h"
#include "hwddrdi.h"
#include "hwdpocpublic.h"
#include "c2k_custom_data_items_rf_verno.h"

#define HWD_RF_FILE_CAL_STATE_SIZE    20
#define GPS_CAL_SIZE                  16
#define HWD_RF_CUST_LASH_SIZE         1

#define NVRAM_EF_HWD_RF_CUST_BPI_MASK_SIZE                          sizeof(HwdRfCustBpiMaskT)
#define NVRAM_EF_HWD_RF_CUST_BPI_MASK_TOTAL                         1

#define NVRAM_EF_HWD_RF_CUST_BPI_DATA_SIZE                          sizeof(HwdRfCustBpiDataT)
#define NVRAM_EF_HWD_RF_CUST_BPI_DATA_TOTAL                         1

#define NVRAM_EF_HWD_RF_CUST_DATA_SIZE                              sizeof(HwdRfCustomDataT)
#define NVRAM_EF_HWD_RF_CUST_DATA_TOTAL                             1

#define NVRAM_EF_HWD_RF_CUST_TAS_SIZE                               sizeof(HwdRfCustTasT)
#define NVRAM_EF_HWD_RF_CUST_TAS_TOTAL                              1

#define NVRAM_EF_HWD_RF_CUST_LAST_SIZE                              HWD_RF_CUST_LASH_SIZE
#define NVRAM_EF_HWD_RF_CUST_LAST_TOTAL                             1

#define NVRAM_EF_HWD_RF_FILE_CAL_STATE_SIZE                         HWD_RF_FILE_CAL_STATE_SIZE
#define NVRAM_EF_HWD_RF_FILE_CAL_STATE_TOTAL                        1
/* RF Cal Data size define*/

#define NVRAM_EF_HWD_BAND_A_GPS_CAL_SIZE                            GPS_CAL_SIZE
#define NVRAM_EF_HWD_BAND_A_GPS_CAL_TOTAL                           1

#define NVRAM_EF_HWD_BAND_B_GPS_CAL_SIZE                            GPS_CAL_SIZE
#define NVRAM_EF_HWD_BAND_B_GPS_CAL_TOTAL                           1

#define NVRAM_EF_HWD_BAND_C_GPS_CAL_SIZE                            GPS_CAL_SIZE
#define NVRAM_EF_HWD_BAND_C_GPS_CAL_TOTAL                           1

#define NVRAM_EF_HWD_BAND_D_GPS_CAL_SIZE                            GPS_CAL_SIZE
#define NVRAM_EF_HWD_BAND_D_GPS_CAL_TOTAL                           1

#define NVRAM_EF_HWD_BAND_E_GPS_CAL_SIZE                            GPS_CAL_SIZE
#define NVRAM_EF_HWD_BAND_E_GPS_CAL_TOTAL                           1

#define NVRAM_EF_HWD_AFC_PARMS_SIZE                                 sizeof(HwdAfcCalibParmT)
#define NVRAM_EF_HWD_AFC_PARMS_TOTAL                                1

#define NVRAM_EF_HWD_AFC_COMP_PARMS_SIZE                            sizeof(HwdAfcCompCalibParmT)
#define NVRAM_EF_HWD_AFC_COMP_PARMS_TOTAL                           1

#define NVRAM_EF_HWD_TEMP_ADC_SIZE                                  sizeof(HwdTempCalibTableT)
#define NVRAM_EF_HWD_TEMP_ADC_TOTAL                                 1

#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_A_SIZE                       sizeof(HwdTxPwrCalibDataT)
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_A_TOTAL                      1

#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_B_SIZE                       sizeof(HwdTxPwrCalibDataT)
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_B_TOTAL                      1

#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_C_SIZE                       sizeof(HwdTxPwrCalibDataT)
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_C_TOTAL                      1

#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_D_SIZE                       sizeof(HwdTxPwrCalibDataT)
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_D_TOTAL                      1

#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_E_SIZE                       sizeof(HwdTxPwrCalibDataT)
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_E_TOTAL                      1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_A_SIZE               sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_A_TOTAL              1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_B_SIZE               sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_B_TOTAL              1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_C_SIZE               sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_C_TOTAL              1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_D_SIZE               sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_D_TOTAL              1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_E_SIZE               sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_E_TOTAL              1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_A_SIZE                sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_A_TOTAL               1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_B_SIZE                sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_B_TOTAL               1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_C_SIZE                sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_C_TOTAL               1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_D_SIZE                sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_D_TOTAL               1

#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_E_SIZE                sizeof(HwdTxPaGainCompTblT)
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_E_TOTAL               1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_A_SIZE                sizeof(HwdTxDetCouplerLossTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_A_TOTAL               1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_B_SIZE                sizeof(HwdTxDetCouplerLossTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_B_TOTAL               1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_C_SIZE                sizeof(HwdTxDetCouplerLossTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_C_TOTAL               1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_D_SIZE                sizeof(HwdTxDetCouplerLossTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_D_TOTAL               1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_E_SIZE                sizeof(HwdTxDetCouplerLossTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_E_TOTAL               1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_A_SIZE     sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_A_TOTAL    1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_B_SIZE     sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_B_TOTAL    1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_C_SIZE     sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_C_TOTAL    1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_D_SIZE     sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_D_TOTAL    1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_E_SIZE     sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_E_TOTAL    1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_A_SIZE      sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_A_TOTAL     1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_B_SIZE      sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_B_TOTAL     1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_C_SIZE      sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_C_TOTAL     1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_D_SIZE      sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_D_TOTAL     1

#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_E_SIZE      sizeof(HwdTxDetCouplerLossCompTblT)
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_E_TOTAL     1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_A_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_A_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_B_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_B_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_C_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_C_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_D_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_D_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_E_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_E_TOTAL             1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_A_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_A_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_B_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_B_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_C_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_C_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_D_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_D_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_E_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_E_TOTAL              1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_A_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_A_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_B_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_B_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_C_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_C_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_D_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_D_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_E_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_E_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_A_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_A_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_B_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_B_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_C_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_C_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_D_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_D_TOTAL             1

#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_E_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_E_TOTAL             1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_A_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_A_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_B_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_B_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_C_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_C_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_D_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_D_TOTAL              1

#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_E_SIZE               sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_E_TOTAL              1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_A_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_A_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_B_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_B_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_C_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_C_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_D_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_D_TOTAL             1

#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_E_SIZE              sizeof(HwdRxPathLossCompTblT)
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_E_TOTAL             1

/*MIPI LID size and record total*/
#define NVRAM_EF_HWD_MIPI_PARAM_SIZE                                sizeof(HwdMipiCustParamT) * MPII_CUST_PARAM_NUM
#define NVRAM_EF_HWD_MIPI_PARAM_TOTAL                               1

#define NVRAM_EF_HWD_MIPI_INITIAL_CW_SIZE                           sizeof(MIPI_IMM_DATA_TABLE_T) * MIPI_INITIAL_CW_MAX_NUM
#define NVRAM_EF_HWD_MIPI_INITIAL_CW_TOTAL                          1

#define NVRAM_EF_HWD_MIPI_SLEEP_CW_SIZE                             sizeof(MIPI_IMM_DATA_TABLE_T) * MIPI_SLEEP_CW_MAX_NUM
#define NVRAM_EF_HWD_MIPI_SLEEP_CW_TOTAL                            1

#define NVRAM_EF_HWD_MIPI_USID_CHANGE_TABLE_SIZE                    sizeof(MIPI_USID_CHANGE_T) * MIPI_MAX_USID_CHANGE_NUM
#define NVRAM_EF_HWD_MIPI_USID_CHANGE_TABLE_TOTAL                   1

#define NVRAM_EF_HWD_BAND_A_MIPI_RX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_RX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_RX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_B_MIPI_RX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_RX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_RX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_C_MIPI_RX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_RX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_RX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_A_MIPI_RX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_RX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_RX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_B_MIPI_RX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_RX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_RX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_C_MIPI_RX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_RX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_RX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_A_MIPI_TX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_TX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_TX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_B_MIPI_TX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_TX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_TX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_C_MIPI_TX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_TX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_TX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_A_MIPI_TX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_TX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_TX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_B_MIPI_TX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_TX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_TX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_C_MIPI_TX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_TX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_TX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_A_MIPI_TPC_EVENT_SIZE                     sizeof(MIPI_EVENT_TABLE_T) * MIPI_TPC_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_TPC_EVENT_TOTAL                    1

#define NVRAM_EF_HWD_BAND_B_MIPI_TPC_EVENT_SIZE                     sizeof(MIPI_EVENT_TABLE_T) * MIPI_TPC_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_TPC_EVENT_TOTAL                    1

#define NVRAM_EF_HWD_BAND_C_MIPI_TPC_EVENT_SIZE                     sizeof(MIPI_EVENT_TABLE_T) * MIPI_TPC_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_TPC_EVENT_TOTAL                    1

#define NVRAM_EF_HWD_BAND_A_MIPI_TPC_DATA_SIZE                      sizeof(MIPI_DATA_TABLE_T) * MIPI_TPC_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_TPC_DATA_TOTAL                     1

#define NVRAM_EF_HWD_BAND_B_MIPI_TPC_DATA_SIZE                      sizeof(MIPI_DATA_TABLE_T) * MIPI_TPC_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_TPC_DATA_TOTAL                     1

#define NVRAM_EF_HWD_BAND_C_MIPI_TPC_DATA_SIZE                      sizeof(MIPI_DATA_TABLE_T) * MIPI_TPC_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_TPC_DATA_TOTAL                     1

#define NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_1XRTT_SIZE         sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_1XRTT_TOTAL        1

#define NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_1XRTT_SIZE         sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_1XRTT_TOTAL        1

#define NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_1XRTT_SIZE         sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_1XRTT_TOTAL        1

#define NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_EVDO_SIZE          sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_A_MIPI_PA_SECTION_DATA_EVDO_TOTAL         1

#define NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_EVDO_SIZE          sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_B_MIPI_PA_SECTION_DATA_EVDO_TOTAL         1

#define NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_EVDO_SIZE          sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_C_MIPI_PA_SECTION_DATA_EVDO_TOTAL         1

#define NVRAM_EF_HWD_BAND_D_MIPI_RX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_RX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_RX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_D_MIPI_RX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_RX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_RX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_D_MIPI_TX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_TX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_TX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_D_MIPI_TX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_TX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_TX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_D_MIPI_TPC_EVENT_SIZE                     sizeof(MIPI_EVENT_TABLE_T) * MIPI_TPC_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_TPC_EVENT_TOTAL                    1

#define NVRAM_EF_HWD_BAND_D_MIPI_TPC_DATA_SIZE                      sizeof(MIPI_DATA_TABLE_T) * MIPI_TPC_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_TPC_DATA_TOTAL                     1

#define NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_1XRTT_SIZE         sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_1XRTT_TOTAL        1

#define NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_EVDO_SIZE          sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_D_MIPI_PA_SECTION_DATA_EVDO_TOTAL         1

#define NVRAM_EF_HWD_BAND_E_MIPI_RX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_RX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_RX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_E_MIPI_RX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_RX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_RX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_E_MIPI_TX_EVENT_SIZE                      sizeof(MIPI_EVENT_TABLE_T) * MIPI_TX_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_TX_EVENT_TOTAL                     1

#define NVRAM_EF_HWD_BAND_E_MIPI_TX_DATA_SIZE                       sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_TX_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_TX_DATA_TOTAL                      1

#define NVRAM_EF_HWD_BAND_E_MIPI_TPC_EVENT_SIZE                     sizeof(MIPI_EVENT_TABLE_T) * MIPI_TPC_EVENT_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_TPC_EVENT_TOTAL                    1

#define NVRAM_EF_HWD_BAND_E_MIPI_TPC_DATA_SIZE                      sizeof(MIPI_DATA_TABLE_T) * MIPI_TPC_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_TPC_DATA_TOTAL                     1

#define NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_1XRTT_SIZE         sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_1XRTT_TOTAL        1

#define NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_EVDO_SIZE          sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_PA_SECTION_DATA_MAX_NUM
#define NVRAM_EF_HWD_BAND_E_MIPI_PA_SECTION_DATA_EVDO_TOTAL         1


/*POC data lid size and lid total define*/
#define NVRAM_EF_HWD_POC_STATE_SIZE                                 sizeof(HwdPocStateT)
#define NVRAM_EF_HWD_POC_STATE_TOTAL                                1

#define NVRAM_EF_HWD_POC_BAND_A_SIZE                                HWD_POC_DATA_SIZE
#define NVRAM_EF_HWD_POC_BAND_A_TOTAL                               1

#define NVRAM_EF_HWD_POC_BAND_B_SIZE                                HWD_POC_DATA_SIZE
#define NVRAM_EF_HWD_POC_BAND_B_TOTAL                               1

#define NVRAM_EF_HWD_POC_BAND_C_SIZE                                HWD_POC_DATA_SIZE
#define NVRAM_EF_HWD_POC_BAND_C_TOTAL                               1

#define NVRAM_EF_HWD_POC_BAND_D_SIZE                                HWD_POC_DATA_SIZE
#define NVRAM_EF_HWD_POC_BAND_D_TOTAL                               1

#define NVRAM_EF_HWD_POC_BAND_E_SIZE                                HWD_POC_DATA_SIZE
#define NVRAM_EF_HWD_POC_BAND_E_TOTAL                               1

//#define NVRAM_EF_HWD_DRDI_PARAM_SIZE                                sizeof(HwdDrdiParamT)
#define NVRAM_EF_HWD_DRDI_PARAM_TOTAL                               1

//#define NVRAM_EF_HWD_DRDI_PARAM2_SIZE                               sizeof(HwdDrdiParam2T)
#define NVRAM_EF_HWD_DRDI_PARAM2_TOTAL                              1

//#define NVRAM_EF_HWD_DRDI_REMAP_TABLE_SIZE                          sizeof(HwdDrdiRemapTblT)
#define NVRAM_EF_HWD_DRDI_REMAP_TABLE_TOTAL                         1

#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_A_SIZE                       sizeof(int16)
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_A_TOTAL                      1

#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_B_SIZE                       sizeof(int16)
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_B_TOTAL                      1

#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_C_SIZE                       sizeof(int16)
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_C_TOTAL                      1

#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_D_SIZE                       sizeof(int16)
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_D_TOTAL                      1

#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_E_SIZE                       sizeof(int16)
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_E_TOTAL                      1

#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_A_SIZE                     sizeof(HwdTxPwrBackOffTblT)
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_A_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_B_SIZE                     sizeof(HwdTxPwrBackOffTblT)
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_B_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_C_SIZE                     sizeof(HwdTxPwrBackOffTblT)
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_C_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_D_SIZE                     sizeof(HwdTxPwrBackOffTblT)
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_D_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_E_SIZE                     sizeof(HwdTxPwrBackOffTblT)
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_E_TOTAL                    1
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_A_SIZE                     sizeof(HwdSarTxPowerOffsetT)
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_A_TOTAL                    1
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_B_SIZE                     sizeof(HwdSarTxPowerOffsetT)
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_B_TOTAL                    1
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_C_SIZE                     sizeof(HwdSarTxPowerOffsetT)
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_C_TOTAL                    1
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_D_SIZE                     sizeof(HwdSarTxPowerOffsetT)
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_D_TOTAL                    1
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_E_SIZE                     sizeof(HwdSarTxPowerOffsetT)
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_E_TOTAL                    1
#endif
#ifdef __TX_POWER_OFFSET_SUPPORT__
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_A_SIZE                     sizeof(HwdTxPowerOffsetT)
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_A_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_B_SIZE                     sizeof(HwdTxPowerOffsetT)
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_B_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_C_SIZE                     sizeof(HwdTxPowerOffsetT)
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_C_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_D_SIZE                     sizeof(HwdTxPowerOffsetT)
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_D_TOTAL                    1
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_E_SIZE                     sizeof(HwdTxPowerOffsetT)
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_E_TOTAL                    1
#endif

#define NVRAM_EF_HWD_1X_ACC_TX_POWER_OFFSET_SIZE                    sizeof(Hwd1xL1dRfCustT)
#define NVRAM_EF_HWD_1X_ACC_TX_POWER_OFFSET_TOTAL                   1

/*ETM */
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TX_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_DATA_SIZE                  sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_ETM_TX_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TX_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TX_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_DATA_SIZE                  sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_ETM_TX_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TX_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TX_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_DATA_SIZE                  sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_ETM_TX_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TX_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TX_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_DATA_SIZE                  sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_ETM_TX_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TX_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TX_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_DATA_SIZE                  sizeof(MIPI_DATA_SUBBAND_TABLE_T) * MIPI_ETM_TX_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TX_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TPC_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_DATA_SIZE                  sizeof(MIPI_DATA_TABLE_T) * MIPI_ETM_TPC_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_TPC_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TPC_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_DATA_SIZE                  sizeof(MIPI_DATA_TABLE_T) * MIPI_ETM_TPC_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_TPC_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TPC_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_DATA_SIZE                  sizeof(MIPI_DATA_TABLE_T) * MIPI_ETM_TPC_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_TPC_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TPC_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_DATA_SIZE                  sizeof(MIPI_DATA_TABLE_T) * MIPI_ETM_TPC_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_TPC_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_EVENT_SIZE                  sizeof(MIPI_EVENT_TABLE_T) * MIPI_ETM_TPC_EVENT_MAX_NUM 
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_EVENT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_DATA_SIZE                  sizeof(MIPI_DATA_TABLE_T) * MIPI_ETM_TPC_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_TPC_DATA_TOTAL                 1

#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_1XRTT_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_1XRTT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_1XRTT_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_1XRTT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_1XRTT_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_1XRTT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_1XRTT_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_1XRTT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_1XRTT_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_1XRTT_TOTAL                 1

#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_EVDO_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_A_MIPI_ETM_PA_SECTION_DATA_EVDO_TOTAL                 1

#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_EVDO_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_B_MIPI_ETM_PA_SECTION_DATA_EVDO_TOTAL                 1

#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_EVDO_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_C_MIPI_ETM_PA_SECTION_DATA_EVDO_TOTAL                 1

#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_EVDO_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_D_MIPI_ETM_PA_SECTION_DATA_EVDO_TOTAL                 1

#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_EVDO_SIZE                  sizeof(MIPI_TPC_SECTION_TABLE_T) * MIPI_ETM_PA_SECTION_DATA_MAX_NUM 
#define NVRAM_EF_HWD_BAND_E_MIPI_ETM_PA_SECTION_DATA_EVDO_TOTAL                 1

#ifdef __DYNAMIC_ANTENNA_TUNING__
#define NVRAM_EF_HWD_DAT_FE_ROUTE_DATABASE_SIZE                                   sizeof(C2K_CUSTOM_DAT_FE_ROUTE_DATABASE_T)
#define NVRAM_EF_HWD_DAT_FE_ROUTE_DATABASE_TOTAL                                     1

#define NVRAM_EF_HWD_DAT_FE_BPI_DATABASE_SIZE                                     sizeof(C2K_CUSTOM_DAT_FE_DATABASE_T)
#define NVRAM_EF_HWD_DAT_FE_BPI_DATABASE_TOTAL                                       1

#define NVRAM_EF_HWD_DAT_CAT_A_MIPI_EVENT_TABLE_SIZE                              sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_ROUTE_A_NUM*C2K_MIPI_DAT_EVENT_NUM
#define NVRAM_EF_HWD_DAT_CAT_A_MIPI_EVENT_TABLE_TOTAL                                1

#define NVRAM_EF_HWD_DAT_CAT_A_MIPI_DATA_TABLE_SIZE                               sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_ROUTE_A_NUM*C2K_MIPI_DAT_DATA_NUM
#define NVRAM_EF_HWD_DAT_CAT_A_MIPI_DATA_TABLE_TOTAL                                 1

#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_EVENT_TABLE_SIZE                              sizeof(MIPI_EVENT_TABLE_T)*C2K_MIPI_DAT_ROUTE_B_NUM*C2K_MIPI_DAT_EVENT_NUM
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_EVENT_TABLE_TOTAL                                1

#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE0_SIZE                              sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_PER_ROUTE_B_NUM*C2K_MIPI_DAT_DATA_NUM
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE0_TOTAL                                1

#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE1_SIZE                              sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_PER_ROUTE_B_NUM*C2K_MIPI_DAT_DATA_NUM
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE1_TOTAL                                1

#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE2_SIZE                              sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_PER_ROUTE_B_NUM*C2K_MIPI_DAT_DATA_NUM
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE2_TOTAL                                1

#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE3_SIZE                              sizeof(MIPI_DATA_SUBBAND_TABLE_T)*C2K_MIPI_DAT_PER_ROUTE_B_NUM*C2K_MIPI_DAT_DATA_NUM
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE3_TOTAL                                1

#define NVRAM_EF_HWD_DAT_FE_ROUTE_DATABASE_LID_VERNO                                   "000"
#define NVRAM_EF_HWD_DAT_FE_BPI_DATABASE_LID_VERNO                                     "000"
#define NVRAM_EF_HWD_DAT_CAT_A_MIPI_EVENT_TABLE_LID_VERNO                              "000"
#define NVRAM_EF_HWD_DAT_CAT_A_MIPI_DATA_TABLE_LID_VERNO                               "000"
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_EVENT_TABLE_LID_VERNO                              "000"
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE0_LID_VERNO                              "000"
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE1_LID_VERNO                              "000"
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE2_LID_VERNO                              "000"
#define NVRAM_EF_HWD_DAT_CAT_B_MIPI_DATA_TABLE3_LID_VERNO                              "000"

#endif

#define NVRAM_EF_HWD_BAND_A_GPS_CAL_LID_VERNO                                 "000"
#define NVRAM_EF_HWD_BAND_B_GPS_CAL_LID_VERNO                                 "000"
#define NVRAM_EF_HWD_BAND_C_GPS_CAL_LID_VERNO                                 "000"
#define NVRAM_EF_HWD_BAND_D_GPS_CAL_LID_VERNO                                 "000"
#define NVRAM_EF_HWD_BAND_E_GPS_CAL_LID_VERNO                                 "000"

#define NVRAM_EF_HWD_RF_FILE_CAL_STATE_LID_VERNO                              "000"
#define NVRAM_EF_HWD_AFC_PARMS_LID_VERNO                                      "000"
#define NVRAM_EF_HWD_AFC_COMP_PARMS_LID_VERNO                                 "000"
#define NVRAM_EF_HWD_TEMP_ADC_LID_VERNO                                       "000"
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_A_LID_VERNO                            "000"
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_B_LID_VERNO                            "000"
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_C_LID_VERNO                            "000"
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_D_LID_VERNO                            "000"
#define NVRAM_EF_HWD_TX_TPC_LEVEL_BAND_E_LID_VERNO                            "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_A_LID_VERNO                    "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_B_LID_VERNO                    "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_C_LID_VERNO                    "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_D_LID_VERNO                    "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_1XRTT_BAND_E_LID_VERNO                    "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_A_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_B_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_C_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_D_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_PAGAIN_COMP_EVDO_BAND_E_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_A_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_B_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_C_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_D_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_BAND_E_LID_VERNO                     "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_A_LID_VERNO          "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_B_LID_VERNO          "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_C_LID_VERNO          "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_D_LID_VERNO          "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_E_LID_VERNO          "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_A_LID_VERNO           "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_B_LID_VERNO           "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_C_LID_VERNO           "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_D_LID_VERNO           "000"
#define NVRAM_EF_HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_E_LID_VERNO           "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_A_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_B_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_C_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_D_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_HPM_BAND_E_LID_VERNO                   "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_A_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_B_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_C_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_D_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_HPM_BAND_E_LID_VERNO                    "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_A_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_B_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_C_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_D_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_HPM_BAND_E_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_A_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_B_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_C_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_D_LID_VERNO                   "000"
#define NVRAM_EF_HWD_MAIN_RX_PATH_LOSS_LPM_BAND_E_LID_VERNO                   "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_A_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_B_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_C_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_D_LID_VERNO                    "000"
#define NVRAM_EF_HWD_DIV_RX_PATH_LOSS_LPM_BAND_E_LID_VERNO                    "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_A_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_B_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_C_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_D_LID_VERNO                   "000"
#define NVRAM_EF_HWD_SHDR_RX_PATH_LOSS_LPM_BAND_E_LID_VERNO                   "000"

/*POC version define*/
#define NVRAM_EF_HWD_POC_STATE_LID_VERNO                                      "000"
#define NVRAM_EF_HWD_POC_BAND_A_LID_VERNO                                     "000"
#define NVRAM_EF_HWD_POC_BAND_B_LID_VERNO                                     "000"
#define NVRAM_EF_HWD_POC_BAND_C_LID_VERNO                                     "000"
#define NVRAM_EF_HWD_POC_BAND_D_LID_VERNO                                     "000"
#define NVRAM_EF_HWD_POC_BAND_E_LID_VERNO                                     "000"

/*AGPS define */
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_A_LID_VERNO                            "000"
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_B_LID_VERNO                            "000"
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_C_LID_VERNO                            "000"
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_D_LID_VERNO                            "000"
#define NVRAM_EF_HWD_AGPS_GRP_DLY_BAND_E_LID_VERNO                            "000"

/*TX Power Backoff define */
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_A_LID_VERNO                          "000"
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_B_LID_VERNO                          "000"
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_C_LID_VERNO                          "000"
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_D_LID_VERNO                          "000"
#define NVRAM_EF_HWD_TX_PWR_BACKOFF_BAND_E_LID_VERNO                          "000"
/*TX Power offset define */
#ifdef __TX_POWER_OFFSET_SUPPORT__
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_A_LID_VERNO                           "000"
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_B_LID_VERNO                           "000"
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_C_LID_VERNO                           "000"
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_D_LID_VERNO                           "000"
#define NVRAM_EF_HWD_TX_PWR_OFFSET_BAND_E_LID_VERNO                           "000"
#endif
/*SAR TX Power offset define */
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_A_LID_VERNO                       "000"
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_B_LID_VERNO                       "000"
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_C_LID_VERNO                       "000"
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_D_LID_VERNO                       "000"
#define NVRAM_EF_HWD_SAR_TX_PWR_OFFSET_BAND_E_LID_VERNO                       "000"
#endif
#endif
