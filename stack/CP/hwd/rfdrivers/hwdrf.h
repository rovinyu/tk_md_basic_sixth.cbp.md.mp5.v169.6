/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDRF_H_
#define _HWDRF_H_
/*****************************************************************************
* 
* FILE NAME   : hwdrf.h
*
* DESCRIPTION : Header file containing typedefs and definitions pertaining
*               to the RF Hardware Driver which are local to the HWD unit.
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "ipcapi.h"
#include "hwdcal.h"

#include "do_rcpapi.h"
#include "hwdrfbsi.h"
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#include "hwddelayapi.h"
#endif
#include "hwdmsapi.h"

/*----------------------------------------------------------------------------
 Defines and Macros
----------------------------------------------------------------------------*/
/* TxOn Register Configuration */
#define HWD_RF_TXON_CFG_CMOS           0x0001
#define HWD_RF_TXON_CFG_ACTIVE_HIGH    0x0002
#define HWD_RF_TXON_CFG_ENABLE         0x0004
#define HWD_RF_TXON_CFG_OVERRIDE       0x0008

/* Define the static configuration */
#define HWD_RF_TXON_CFG_STATIC         (HWD_RF_TXON_CFG_CMOS | HWD_RF_TXON_CFG_ACTIVE_HIGH | HWD_RF_TXON_CFG_ENABLE)
#define HWD_RF_RFON_CFG_STATIC         (HWD_ON_CFG_CMOS | HWD_ON_CFG_DRIVE | HWD_ON_CFG_ACTIVE_HI)

/* Define number of RF_ONs and EXTERNAL TX_ONs in use with current VIA CBP ASIC */
#define HWD_MAX_NUMBER_RFONS   8

#define HWD_MAX_NUMBER_TXONS   8

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_INVALID_TXON      (0xff)
#endif

/*----------------------------------------------------------------------------
 Typedefs
----------------------------------------------------------------------------*/
/* TxOn types */
typedef enum
{
   HWD_TX_DAC=0,     /* internal TX_ON */
   HWD_TX_ON0,
   HWD_TX_ON1,
   HWD_TX_ON2,
   HWD_TX_ON3,
   HWD_TX_ON4,
   HWD_TX_ON5,
   HWD_TX_ON6,
   HWD_TX_ON7,
   HWD_TX_DAC2       /* internal auxiliary TX_ON */
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   ,
   HWD_TX_INT1 = HWD_TX_DAC2
#endif
} HwdTxOnTypeT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* TX_ON operation type */
typedef enum
{
   HWD_TX_ON_CFG,
   HWD_TX_ON_AUTO_SET,
   HWD_TX_ON_DELAY_WRITE,
   HWD_TX_ON_FORCE_WRITE,
   HWD_TX_ON_READ
} HwdTxOnOperTypeT;

/* RF_ON operation type */
typedef enum
{
   HWD_RF_ON_CFG,
   HWD_RF_ON_WRITE,
   HWD_RF_ON_READ
} HwdRfOnOperTypeT;

/** Enumeration of pa hysteresis type */
typedef enum paHystType_tag{
  HWD_PA_HYST_HIGH_AND_MID = 0,
  HWD_PA_HYST_MID_AND_LOW  = 1,
  HWD_PA_HYST_NUM          = 2
}paHystTypeEnum;


/**The enum of RF Driver scenarios*/
typedef enum {
  HWD_RF_RX_WARM_UP,
  HWD_RF_RX_BURST,
  HWD_RF_RX_SLEEP,
  HWD_RF_RX_AGC,
  HWD_RF_RX2_PARTIAL_ON,
  HWD_RF_RX2_PARTIAL_OFF,
  HWD_RF_TX_WARM_UP,
  HWD_RF_TX_BURST,
  HWD_RF_TX_SLEEP,
  HWD_RF_TX_AGC,
  HWD_RF_DCXO_AFC,
  HWD_RF_SRX_AFC,
  HWD_RF_TX_DC,
  HWD_RF_RX_RC,
  HWD_RF_RX_DC_G0_G7,
  HWD_RF_RX_DC_G8_G15,
  HWD_RF_RX_DC_G16_G23,
  HWD_RF_RX_DC_G24_G31,
  HWD_RF_TX_G0_G7,
  HWD_RF_TX_G8_G15,
  HWD_RF_TX_G16_G23,
  HWD_RF_RX_DC_TOTAL,
  HWD_RF_TX_TOTAL
} HwdRfScenarioT;

/* RF scenarios feedback code words context*/
typedef struct
{
    /* the feedback parameters*/
    uint32        param0;
    uint32        param1;
    uint32        param2;
    uint32        param3;
    uint32        param4;
    uint32        param5;
    uint32        param6;
    uint32        param7;
}HwdRfScenariosParamRspT;


/** TXAGC Table context */
typedef struct HwdRfTxAgcTableT_tag
{
  int16   pgaGain;    /** TX_PGA relative gain with G0, 1/32dB unit */
}HwdRfTxAgcTableT;

/** TX PDET AGC Table context */
typedef struct HwdRfTxDetTableT_tag
{
  int16   txDetPower;  /** The coupling tx power from PA */
  int16   txDetGain;   /** TXDET path gain, 1/32dB unit */
}HwdRfTxDetTableT;



/** RXAGC Table context */
typedef struct HwdRfRxAgcTableT_tag
{
  HwdRxDagcSwitchThreshT     rxPower;      /** RX power, 1/32dBm unit */
  int16                      rxGainQ5;     /** rx gain in Q5 Format */
  HwdRfLnaModeEnumT          lnaMode;      /** LNA mode*/
}HwdRfRxAgcTableT;

typedef struct  HwdRfDevCfgT_tag
{
  /********************** Device Tx part parameter**************/

  /** RF tx gain table size */
  uint16 txAgcTableSize;
  uint16 txPgaATblSize;
  uint16 txPgaBTblSize;

  /** RF tx gain table pointer */
  HwdRfTxAgcTableT *txAgcTablePtr;
 
  /** RF PGA mode*/
  HwdPgaModeT txPgaMode;
 
  /** RF tx PDET table size */
  uint16 txDetTableSize;

  /** RF tx Pdet table pointer */
  HwdRfTxDetTableT *txDetTablePtr;

  /** RF pa control table max index number */
  uint16 paTableMaxIdx;

  /** RF pa control table pointer */
  HwdRfPaContextT *paTablePtr;

  /** RF pa threshold table pointer, index is PA mode */
  HwdTxSwitchThreshT *paThreshTblPtr;

  /** RF PA phase jump compensation table pointer, index is PA mode*/
  int16 *paPhaseJumpComp;

  /** RF PDET path coupler loss pointer, index is PA mode*/
  int16 *couplerLoss;
  int16 *couplerLossComp;

  /** RF PA frequency and temperature compensation, index is PA mode*/
  int16 *paGainCalibComp;

  /** RF PA temperature compensation, index is PA mode*/
  int16 *PaGainCalibTempComp;

  /** The PA mode info.*/
  HwdRfPaModeEnumT paMode;

  /** The PA gain table index*/
  uint16 paTableIndex;

  /** The TXAGC gain table index*/
  uint16 txGainIndex;

  /** The TX Pdet table index*/
  uint16 txDetTableIndex;

  /** The TXAGC delay loader signal start offset timing info.*/
  int16  paVddDlyOffset;
  int16  txPgaDlyOffset;
  int16  paModeDlyOffset;
  int16  gbb0ClDlyOffset;
  int16  gbb0OlDlyOffset;
  int16  gbb1DlyOffset;
  int16  paPhaseJumpDlyOffset;
  int16  pdetRdDlyOffset;
  int16  ddpcRefMeasDlyOffset;
  int16  detAdcOffDlyOffset;
  int16  detMeasTime;

  /** Tx high/low power mode*/
  HwdRfPwrModeT txPwrMode;

  /********************** Device Rx part parameter**************/

  /** the reference RSSI in amplitude log2 format*/
  int16 refRssiAlog2[HWD_RF_MAX_NUM_OF_RX];

  /** the the reference gain that include digital and analog gain in amplitude log2 format*/
  int16 refGainAlog2[HWD_RF_MAX_NUM_OF_RX];

  /** RF rx gain table max index number */
  uint16 rxAgcTableSize;

  /** RF rx gain table pointer */
  HwdRfRxAgcTableT *rxAgcTablePtr;

  /** RF rx dcoc table pointer, using Rx AGC table index to LUT*/
  HwdRfRxDcocT *rxDcocTable[HWD_RF_MAX_NUM_OF_RX];

  /** RF lna phase jump compensation table pointer*/
  int32 *lnaPhaseJumpComp;

  /** RF RXAGC frequency and temperature compensation pointer, index is LNA mode*/
#if defined(MTK_PLT_RF_ORIONC) && (!defined(MTK_DEV_HW_SIM_RF))
  int16 *rxAgcCalibComp[HWD_RF_MAX_NUM_OF_RX];
  int16 *rxAgcCalibFreqComp[HWD_RF_MAX_NUM_OF_RX];
  int16 rxAgcCalibTempComp[HWD_RF_MAX_NUM_OF_RX];
#else
  int16 *rxAgcCalibCompHpm[HWD_RF_MAX_NUM_OF_RX];
  int16 *rxAgcCalibCompLpm[HWD_RF_MAX_NUM_OF_RX];
  /** RF RXAGC frequency compensation pointer, index is LNA mode */
  int16 *rxAgcCalibFreqComp[HWD_RF_MAX_NUM_OF_RX];
  /** RF RXAGC temperature compensation in 1/32dB */
  int16 rxAgcCalibTempComp[HWD_RF_MAX_NUM_OF_RX];
  /** RF RXAGC current frequency and temperature compensation in 1/32dB */
  int16 rxAgcCalibTotalComp[HWD_RF_MAX_NUM_OF_RX];
#endif

  /** The LNA mode info.*/
  HwdRfLnaModeEnumT lnaMode[HWD_RF_MAX_NUM_OF_RX];

  /** Number of LNA modes .*/
  uint16 lnaModeNum;

  /** The Rx AGC table index.*/
  uint16 rxGainIndex[HWD_RF_MAX_NUM_OF_RX];

  /** The initial step gain for Rx Gain table interpolation*/
  uint16 interStepGain;

  /** The Rx band info.*/
  HwdRfBandT rxBand[HWD_RF_MAX_NUM_OF_RX];

  /** Rx high/low power mode*/
  HwdRfPwrModeT rxPwrMode[HWD_RF_MAX_NUM_OF_RX];

  /** RX FSM status*/
  HwdRxFsmT rxFsm;

}HwdRfDevCfgT;


/** Ads structure to provide parameters to the HwdRfGetRxGainConfiguration() function. */
typedef struct HwdRfGetRxGainParamT_tag
{
   /** [in] - The rx path info */
  HwdRfRxEnumT      rxPath;
   /** [in] - The rx power info */
  int16             rxPower;
   /** [in] - The device configuration structre pointer */
  HwdRfDevCfgT     *devCfgPtr;
   /** [out] - The new gain index of RF RXAGC table */
  uint16            newGainIndex;
   /** [out] �CThe RF Rx path analog gain in Q5 format */
   int16            stepGainQ5;
}HwdRfGetRxGainParamT;

/* The code words of a specific RF chip */
typedef PACKED_PREFIX struct
{
   uint32 txGainRdCmd;
   uint32 txGateOnCmd;
   uint32 txGateOffCmd;
} PACKED_POSTFIX  HwdRfSpiCwT;
#endif


/* Define RF Function Pointer structure */
typedef struct 
{
   void (*CtrlInit)(void);
   void (*MXSInit)(void);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   void (*RfCnstInit)(void);
#else
   void (*RfCnstInit)(void *pParm);
#endif
   void (*DefaultCalSet)(void);
   void (*RxOn[HWD_RF_MAX_NUM_OF_RX])(uint8 InterfaceType);
   void (*RxOff[HWD_RF_MAX_NUM_OF_RX])(void);
   void (*TxOn[HWD_RF_MAX_NUM_OF_TX])(uint8 InterfaceType);
   void (*TxOff[HWD_RF_MAX_NUM_OF_TX])(void);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   void (*TxPaCtrl[HWD_RF_MAX_NUM_OF_TX])(HwdAgcGainStateT);
   void (*TxPaDelayCtrl[HWD_RF_MAX_NUM_OF_TX])(HwdAgcGainStateT,RcpTxLoadBoundaryT);
   void (*RxLnaCtrl[HWD_RF_MAX_NUM_OF_RX])(HwdRxAgcGainStateT);
#endif
   uint16 (*GetEcIoRelThresh)(void);
   HwdSupportedCDMABandT *(*RfSupportedBands)(void);
   void (*GpsOn)(void);
   void (*GpsOff)(void);
   void (*GsmCdmaInterferenceEnable)(bool);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   void   (*RfDevInit)(HwdRfDevCfgT *rfDevCfgPtr);
   uint16 (*GetTemperatureAdc)(bool externalMeas);
   void   (*StartTemperatureMeas)(bool externalMeas);
   void   (*PowerOnCal)(void);
   void   (*PreburstTxCal)(uint8 InterfaceType);
   void   (*TxAgcCtrl)(uint16 txAgcTblIdx,uint16 txDetTblIdx,bool enableDdpc,uint16 hslotBoundary);
   int16  (*GetDdpcResult)(void);
   void   (*UpdateRxDcocTable)(int16 reqDcocI, int16 reqDcocQ, int16 *resDcocI, int16 *resDcocQ, uint16 gainIndex, HwdRfMpaEnumT rxPath);
   void   (*RxAgcCtrl)(HwdRfPwrModeT pwrMode, uint16 rxAgcTblIdx, HwdRfMpaEnumT rxPath);
   void   (*RxBurst)(HwdRfMpaEnumT rxPath);
   void   (*TxBurst)(void);
   void   (*GetCw)(void *);
   void   (*SetSpiData)(HwdRfBsiIdT SpiBlock,uint32 Address, uint32 Data);
   uint32 (*GetSpiData)(HwdRfBsiIdT SpiBlock,uint32 Address);
   void   (*WakeupInit)(HwdRfDevCfgT *rfDevCfgPtr);
   void   (*TxGatedCtrl)(bool);
   uint32 (*GetChipId)(void);
   void   *(*GetSerConf)(void);
   void   *(*GetScenarioCw)(void *adsPtr);
   void   *(*GetAntCfg)(void);
   HwdRfRxEnumT (*RfMpaRxPathToRfRxPath)(HwdRfMpaEnumT rfMpaPath);
   void   (*SetRxPowerMode)(HwdRfMpaEnumT rxPath,HwdRfPwrModeT pwrMode);
   void   (*SetStxPowerMode)(HwdRfPwrModeT txRelockLpm,HwdRfPwrModeT dynPmOob);
   void   (*SetDynTxPowerMode)(HwdRfPwrModeT dynPmEvm,HwdRfPwrModeT dynPmOob);
   void   (*GetTxGainCfg)(void *adsPtr);
   void   (*GetTxDetCfg)(void *adsPtr);
   HwdRfDevCfgT *(*GetDevCfgPtr)(void);
   void   (*SetOrigPocResult)(void *adsPtr);
   uint32 (*GetOrigPocResultSize)(void);
   void   (*SetFinalPocResult)(void *adsPtr,HwdRfBandT rfBand);
   void   *(*GetFinalPocResult)(void);
   uint32 (*GetFinalPocResultSize)(void);
   void   (*GetPocCfg)(void *adsPtr);
   uint32 (*GetPocCfgSize)(void);
   void   (*GetTxPocTbl)(void *adsPtr);
   void   (*GetDetPocTbl)(void *adsPtr);
   void   (*GetRxPocTbl)(void *adsPtr);
   void   (*SavePorParam)(void *adsPtr, uint32 porSize);
   int16  (*GetRxCalPower)(HwdRfPwrModeT pwrMode, HwdRfLnaModeEnumT lnaMode);
   uint32*(*GetVs1TablePtr)(void);
#endif
#if defined(MTK_CBP) && defined(MTK_PLT_ON_PC)
   void   (*PaOn)(void);
   void   (*PaOff)(void);
   void   (*SetPaStatus)(bool);
#endif
} RfFuncPtrTblT;

/* Define RF_ON and TX_ON Mapping structures */
typedef enum
{
   GPO = 0,     /* RF_ON/TX_ON used as general purpose output (GPO) by CP */
   RXAGC_CTRL,  /* RF_ON is used as LNA control by DSPM */
   GATED,       /* TX_ON is gated by Tx Buffer hardware */
   PA1_CTRL,    /* TX_ON is used as PA1 control by DSPM (usually low->medium transitions) */
   PA2_CTRL,    /* TX_ON is used as PA2 control by DSPM (usually medium->high transitions) */
   DSPM_GPO,    /* RF_ON/TX_ON used as general purpose output (GPO) by DSPM */
   NOT_USED     /* RF_ON/TX_ON not in use */

} LineModeT;

typedef enum
{
   OFF_ST8 = 0,  /* RF_ON/TX_ON inactive for current gain state */
   ON_ST8,       /* RF_ON/TX_ON actively driven for current gain state */
   NA            /* Line State not applicable for current line mode */

} LineStateT;

typedef struct
{
   LineModeT  LineMode;   /* RF_ON/TX_ON mode (i.e. GPO or LNA/PA control) */
   LineStateT LowGainSt;  /* Low LNA gain state (if applicable)   */
   LineStateT MedGainSt;  /* Med LNA gain state (if applicable)   */
   LineStateT HighGainSt; /* High LNA gain state (if applicable)  */

} HwdLineMappingT;
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef HwdLineMappingT HwdLineMappingArrayT[HWD_MAX_NUMBER_TXONS];
#endif

/* RfOn types */
typedef enum
{
   HWD_RF_ON0 = 0,
   HWD_RF_ON1,
   HWD_RF_ON2,
   HWD_RF_ON3,
   HWD_RF_ON4,
   HWD_RF_ON5,
   HWD_RF_ON6,
   HWD_RF_ON7
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   ,
   HWD_RF_ON8,
   HWD_RF_ON9
#endif
} HwdRfOnTypeT;

typedef enum {
   LOW_GAIN = 0,
   MED_GAIN,
   HIGH_GAIN,
   AUTO_MODE
}GainStateT;

typedef enum {

   HWD_PIN_TYPE_GPIO = 0,
   HWD_PIN_TYPE_TX_ON, 
   HWD_PIN_TYPE_RF_ON,
   HWD_PIN_TYPE_LAST

}HwdRfPinTypeT;


/* Define RF PLL channel read msg command */
typedef PACKED_PREFIX struct 
{
   HwdRfPinTypeT     type;
   uint8            id;  
} PACKED_POSTFIX  HwdRfPinT;

extern void HwdRfPinGenericControl(const HwdRfPinT* pin, uint32 ctrl);
#define HWD_RF_PIN_CONFIG_NONE    0
extern void HwdRfPinGenericControlConfig(const HwdRfPinT* pin, uint32 config);


/* Define RF PLL channel read msg command */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT     RspInfo;
   uint8    RfPath;      /* Main, Div or both */   
} PACKED_POSTFIX  HwdRfPllChanGetMsgT;

/* Define RF PLL channel read response */
typedef PACKED_PREFIX struct 
{
   uint8  Band;
   uint16 Channel;
} PACKED_POSTFIX  HwdRfPllChanGetRspT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef enum
{
   PLL_SET_MAIN_RX,
   PLL_SET_DIV_RX,
   PLL_SET_TX,
   BURST_SET_MAIN_RX,
   BURST_SET_DIV_RX,
   BURST_SET_TX,
   TURN_OFF_MAIN_RX,
   TURN_OFF_DIV_RX,
   TURN_OFF_TX,
   TX_GATE,
} HwdRfStateT;

typedef struct
{
  RfFuncPtrTblT  *RfFuncPtr;
  PllFuncPtrTblT *PllFuncPtr;
  PllT           **PllDataPtr;
  PllRfConfigT   **PllRfCfgPtr;
  bool           **MainRxIQSwap;
  bool           **MainTxIQSwap;
  bool           **DiversityRxIQSwap;
  bool           **SecRxIQSwap;
}HwdRfPlatformCfgT;


typedef enum
{
    HWD_TPC_VM0_SIG,
    HWD_TPC_VM1_SIG,
    HWD_TPC_MI0_SIG,
    HWD_TPC_MI1_SIG,
    HWD_TPC_MI2_SIG,
    HWD_TPC_MI3_SIG,
    HWD_TPC_SIG_NUM
} HwdTpcSigT;

#ifdef SYS_OPTION_HPUE_ENABLED
typedef enum
{
    HWD_HPUE_MI0_SIG,
    HWD_HPUE_MI1_SIG,
    HWD_HPUE_MI2_SIG,
    HWD_HPUE_MI3_SIG,
    HWD_HPUE_SIG_NUM
} HwdHpueSigT;
#endif

typedef enum
{
    TX_BURST_NORMAL,
    TX_BURST_IMM,

    TX_BURST_MAX
} HwdTxBurstTypeT;

#if (SYS_BOARD >= SB_JADE)
#ifdef __INCREASE_VS1_VOLTAGE_SUPPORT__
typedef enum
{
    WIN_RX1 = 0,
    WIN_RX2,
    WIN_TX,

    WIN_TYPE_MAX
} HwdRfWinTypeT;

typedef enum
{
    WIN_OFF = 0,
    WIN_ON,

    WIN_OP_MAX
} HwdRfWinOpT;
#endif
#endif

#endif

/*----------------------------------------------------------------------------
 Global Variables
----------------------------------------------------------------------------*/
extern HwdSupportedCDMABandT HwdRfSupportedBandsTable[];
extern uint16 HwdDefaultRfControl;
extern uint16 HwdDefaultRfControl2;

extern HwdRfConstantsT HwdRfConstants;

extern uint8 GRFRefMod;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern const uint8 HWD_TCXO_SETTLE_MS;

/* Software default Rx/Tx AGC coordinate pairs (initialized based on RF option) */
extern HwdRxPwrCoordinateT HwdDefaultRxPwrCoords[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_DEFAULT_GAIN_POINTS];
extern HwdTxPwrCoordinateT HwdDefaultTxPwrCoords[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_NUM_DEFAULT_GAIN_POINTS];

/* Software default AFC variables (initialized based on RF option) */ 
extern int16 HwdDefaultAfcHwvInterc;
extern int16 HwdDefaultAfcSlopeStepPerPpm;

/* Variable to keep track of Tx SD DAC I/Q Level setting (set based on RF option) */
extern uint8 HwdTxGainLevelSetting;

extern HwdRxDagcCalibDataT HwdDefaultMainRxDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxDagcCalibDataT HwdDefaultDivRxDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxDagcCalibDataT HwdDefaultSecRxDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
#else
#ifndef MTK_DEV_RF_CUSTOMIZE 
extern HwdRxDagcCalibDataT HwdDefaultMainRxDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdRxDagcCalibDataT HwdDefaultDivRxDagcCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdTxPwrCalibDataT HwdDefaultTxPwrCalibData[HWD_NUM_SUPPORTED_BAND_CLASS];
extern HwdAfcCalibParmT HwdDefaultAfcValue;
#endif
#endif
extern const GainStateDirT MainGainPrefDir[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_MAX_NUM_DIGITAL_GAIN_STATES-1];
extern const GainStateDirT MainGainPrefDirNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_MAX_NUM_DIGITAL_GAIN_STATES-1];
extern const GainStateDirT DivGainPrefDir[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_MAX_NUM_DIGITAL_GAIN_STATES-1];
extern const GainStateDirT DivGainPrefDirNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_MAX_NUM_DIGITAL_GAIN_STATES-1];
extern const GainStateDirT AuxGainPrefDir[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_MAX_NUM_DIGITAL_GAIN_STATES-1];
extern const GainStateDirT AuxGainPrefDirNoIMD[HWD_NUM_SUPPORTED_BAND_CLASS][HWD_MAX_NUM_DIGITAL_GAIN_STATES-1];

extern void (*TxFltrTestFuncPtr)(void);

extern HwdRfVersionT HwdRfVersion;

extern bool HwdRfCurrentState[HWD_RF_MPA_MAX_PATH_NUM];

extern int16 *HwdRfIcControlTableP;

/* common RF pins */
extern const HwdRfPinT HWD_TX_GATED;
extern const HwdRfPinT HWD_TX_CELL_PA_ON;
extern const HwdRfPinT HWD_TX_PCS_PA_ON;
extern const HwdRfPinT HWD_TX_AWS_PA_ON;
extern const HwdRfPinT HWD_TX_PA_GAIN;
extern const HwdRfPinT HWD_TX_PA_GAIN2;

extern const HwdDelayTxOnTypeT  HWD_DELAY_TX_PA_PWR;
extern const HwdDelayTxOnTypeT  HWD_DELAY_TX_GATE;
extern const HwdDelayPdmT  HWD_DELAY_PDM_TX_AGC_BN;

extern const HwdDelayModeTxOnTypeT HWD_DELAY_MASK_TX_PA_PWR;
extern const HwdDelayModeTxOnTypeT HWD_DELAY_MASK_TX_GATE;

extern const HwdDelayTxOnTypeT  HWD_DELAY_TX_PA_PWR_2;
extern const HwdDelayModeTxOnTypeT  HWD_DELAY_MASK_TX_PA_PWR_2;
#ifndef MTK_CBP
extern const RfFuncPtrTblT RfFuncPtrTbl;
#endif


#if defined (MTK_DEV_OPTIMIZE_EVL1)
#if !defined(MTK_CBP) || defined(MTK_PLT_ON_PC)
/* the earliest timing to configure EVDO PA before system time's slot boundary, in chip unit */
extern uint8 HwdRfDOPaEarliestTiming;
#endif
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* Tx AGC */
extern HwdDlyLdrOftT HwdTxAgcDelayOffset[];
extern uint16 HwdTxAgcDelayTrig[];
extern uint16 HwdTpcDelayOffset[][HWD_TPC_SIG_NUM];
extern uint16 HwdTpcTrig[][HWD_TPC_SIG_NUM];

#ifdef SYS_OPTION_HPUE_ENABLED
extern uint16 HwdHpueTrig[HWD_RF_BAND_MAX][HWD_HPUE_SIG_NUM];
extern uint16 HwdHpueDelayOffset[HWD_RF_BAND_MAX][HWD_HPUE_SIG_NUM];
extern uint32 HwdCustGetHpueEventOffset(HwdRfBandT band);
#endif

#ifdef MTK_DEV_HW_SIM_RF
extern HwdDlyLdrOftT HwdTxAgcDelayOffsetHwSim[];
extern uint16 HwdTxAgcDelayTrigHwSim[];
extern uint16 HwdTpcDelayOffsetHwSim[][HWD_TPC_SIG_NUM];
extern uint16 HwdTpcTrigHwSim[][HWD_TPC_SIG_NUM];
#endif /* MTK_DEV_HW_SIM_RF */

extern void HwdTxAgcInitDelayOffset(HwdDlyLdrOftT *offsetTblPtr, HwdRfDevCfgT *rfDevCfgPtr);
#endif /* MTK_CBP */


/*----------------------------------------------------------------------------
 Function Prototypes
----------------------------------------------------------------------------*/
/* TxOn control */
extern void  HwdTxOnAutoSet(HwdTxOnTypeT SelectedTxOn);
extern void  HwdTxOnSet(HwdTxOnTypeT SelectedTxOn);
extern void  HwdTxOnClear(HwdTxOnTypeT SelectedTxOn);
extern void  HwdTxOnForce(HwdTxOnTypeT SelectedTxOn, uint8 TxOnValue);
extern uint8 HwdReadTxOn(HwdTxOnTypeT SelectedTxOn);
extern void  HwdMainRfPllChannelGetResponse(HwdRfPllChanGetMsgT *MsgDataPtr);
extern void  HwdMainRfPllBandSet(SysCdmaBandT PllBand, uint8 InterfaceType, bool ForceSet);
extern void  HwdDivRfPllChannelGetResponse(HwdRfPllChanGetMsgT *MsgDataPtr);
extern void  HwdDivRfPllBandSet(SysCdmaBandT PllBand, uint8 InterfaceType, bool ForceSet);
extern void  HwdSecRfPllChannelGetResponse(HwdRfPllChanGetMsgT *MsgDataPtr);
extern void  HwdSecRfPllBandSet(SysCdmaBandT PllBand, uint8 InterfaceType, bool ForceSet);

extern void  HwdRfMainCtrlReconfigure(SysCdmaBandT Band, uint8 InterfaceType);
extern void  HwdRfDivCtrlReconfigure(SysCdmaBandT Band, uint8 InterfaceType);
extern void  HwdRfSecCtrlReconfigure(SysCdmaBandT Band, uint8 InterfaceType);

extern void  HwdRfMainRxOn(uint8 InterfaceType);
extern void  HwdRfMainRxOff(uint8 InterfaceType);
extern void  HwdRfDivRxOn(uint8 InterfaceType);
extern void  HwdRfDivRxOff(uint8 InterfaceType);
extern void  HwdRfSecRxOn(uint8 InterfaceType);
extern void  HwdRfSecRxOff(uint8 InterfaceType);

extern void  HwdRfMainTxOn(uint8 InterfaceType);
extern void  HwdRfMainTxOff(uint8 InterfaceType);

extern void  HwdRfAuxTxOn(uint8 InterfaceType);
extern void  HwdRfAuxTxOff(uint8 InterfaceType);

extern bool  HwdRfMainRxIQSwapGet(HwdRfBandT RfBand);
extern bool  HwdRfDivRxIQSwapGet(HwdRfBandT RfBand);

extern void  HwdRfCtrlInit(void);
extern void  HwdRfMXSInit(void);
extern void  HwdRfDefaultCalSet(void);

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern void  HwdRfOnConfig(HwdRfOnTypeT SelectedRfOn, uint16 ConfigMask);
extern void  HwdTxOnConfig(HwdTxOnTypeT SelectedTxOn, uint16 ConfigMask);
#endif
extern void  HwdTxOnSetupHoldSet(const HwdRfPinT* SelectedTxOn, uint8 SetupCounts, uint8 HoldCounts);
extern uint16 HwdRfOnRead (HwdRfOnTypeT SelectedRfOn);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern void  HwdRfOnWrite(HwdRfOnTypeT SelectedRfOn, uint16 Data);
#endif
extern void  HwdRfOnForce (HwdRfOnTypeT SelectedRfOn, uint16 Data);
extern uint16 HwdRfGetEcIoRelThresh(void);
extern HwdSupportedCDMABandT *HwdRfSupportedBands(void);
extern SysCdmaBandT HwdRfBandConvert(HwdRfBandT Band);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern HwdRfBandT HwdRfBandClassConvert(SysCdmaBandT bandClass);
#endif

extern void  HwdFiqGainStateChange(void);
extern void  HwdLisrGainStateFailsafe(void);
#if (SYS_BOARD >= SB_JADE)
extern void  HwdIrqGainStateChange(void);
#endif
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void  HwdRfMainTxPaCtrl(uint16 GainState);
#endif
extern void  HwdRfMainTxPaDelayCtrl(uint16 GainState, RcpTxLoadBoundaryT hslot_boundary);

extern void  HwdRfPaCtrl(HwdAgcGainStateT GainState);
extern void  HwdRfPaCtrlDelaySpi(HwdAgcGainStateT GainState, RcpTxLoadBoundaryT hslot_boundary);

#if!defined(MTK_CBP) || defined(MTK_PLT_ON_PC)
extern void  HwdRfUpdateMainRxBankCalData(HwdRfBandT RfBand, HwdRfCalDataT *RfCalDataP);
extern void  HwdRfUpdateDivRxBankCalData(HwdRfBandT RfBand, HwdRfCalDataT *RfCalDataP);
extern void  HwdRfUpdateSecRxBankCalData(HwdRfBandT RfBand, HwdRfCalDataT *RfCalDataP);
#endif 

extern bool  HwdRfDriverCmd(uint16 code);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern uint8 HwdRfDlyModeRegBitPos(HwdDelayModeCtrlUnitT CtrlIndex);
extern uint8 HwdRfDlyMaskRegBitPos(HwdDelayMaskCtrlUnitT CtrlIndex);
#endif

#ifdef DEFAULT_RX_HYSTERESIS
extern uint8 HwdCheckRfVersion(HwdRfVersionT *RfVersionP);
#endif
extern bool  HwdRefModCtrl( uint8 RefMod );
extern void  HwdRfChangePreemptionDelay(uint16 Delay);

extern void  HwdRfRestoreReg(void);
extern void  HwdRfGpsOn(void);
extern void  HwdRfGpsOff(void);

extern uint16 HwdRfGetDspmAfcPdmAddr(void);
extern uint16 HwdRfGetDspmTxAgcPdmAddr(void);
extern uint16 HwdRfGetDspmTxAgcSpdmAddr(void);

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))   // for remove build warning
extern void HwdRfGsmCompensateMsg(uint8 Status);
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfMainRxBurst()

  DESCRIPTION:      This function is used to turn RF main Rx into burst mode,
                    and enable Rx DFE main path.

  PARAMETERS:       void

  RETURNED VALUES:  void 

*****************************************************************************/
extern void HwdRfMainRxBurst(void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfDivRxBurst()

  DESCRIPTION:      This function is used to turn RF div Rx into burst mode.
                    and enable Rx DFE main path.

  PARAMETERS:       void

  RETURNED VALUES:  void 

*****************************************************************************/
extern void HwdRfDivRxBurst(void);

/*****************************************************************************
 
  FUNCTION NAME:    HwdRfTxBurst()

  DESCRIPTION:      This function is used to turn RF Tx into burst mode.
                    and enable Tx DFE main path.

  PARAMETERS:       void

  RETURNED VALUES:  void 

*****************************************************************************/
extern void HwdRfTxBurst(HwdTxBurstTypeT);

/***********************************************
 FUNCTION NAME: HwdRfGetDevCfgPtr()
 
 DESCRIPTION:  This fucntion is used to get the RF configuration structure pointer

 PARAMETERS:  HwdRfMpaEnumT RfPath

 RETURNS VALUES: RF configuration structure pointer
 ***********************************************/
extern HwdRfDevCfgT *HwdRfGetDevCfgPtr(void);
#ifdef MTK_DEV_HW_SIM_RF
extern HwdRfDevCfgT *HwdRfGetDevCfgPtrHwSim(void);
#endif


/*****************************************************************************
 
  FUNCTION NAME:    HwdRfFreqBandConvert

  DESCRIPTION:      This function converts a HwdRfBandT type to the corresponding
                    supported RF frequency high/low band type.

  PARAMETERS:       Band - The Physical layer band (A - E)

  RETURNED VALUES:  The corresponding RF frequency high/low band

*****************************************************************************/
HwdRfFreqBandT HwdRfFreqBandConvert(HwdRfBandT rfBand);
/*****************************************************************************
 
  FUNCTION NAME:    HwdRfGetPaCfgInfo()

  DESCRIPTION:      This function is used to check whether the PA mode should
                    be changed refer to the hysteresis region, and return the
                    PA configuration.

  PARAMETERS:       paChange    - Indicate whether the PA mode needs change 
                                  when the timing is critical.
                    targetTxPwr - Target output power at antenna, in 1/32 dB

  RETURNED VALUES:  paCfgInfo  - The HwdRfPaContextT structure pointer;

*****************************************************************************/
#if defined (MTK_DEV_OPTIMIZE_EVL1) && !defined(MTK_PLT_ON_PC)
DEF_CodeInIram(extern void HwdRfGetPaCfgInfo(bool paChange,int16 targetTxPwr,HwdRfPaContextT *paCfgInfoPtr,HwdRfDevCfgT *devCfgPtr));
#else
extern void HwdRfGetPaCfgInfo(bool paChange,int16 targetTxPwr,HwdRfPaContextT *paCfgInfoPtr,
                       HwdRfDevCfgT *devCfgPtr);
#endif

#if (defined(MTK_CBP)&& (!defined(MTK_PLT_ON_PC))) && (defined MTK_DEV_DUMP_REG)
extern void HwdRfCtrlRegLogAll(void);
extern void HwdRfCtrlTxOnRegLogAll(void);
extern void HwdRfCtrlRfOnRegLogAll(void);
#endif
extern void *HwdRfGetScenarioCodeWord(void *adsPtr);
#ifdef MTK_DEV_HW_SIM_RF
extern void *HwdRfGetScenarioCodeWordHwSim(void *adsPtr);
#endif

/*****************************************************************************

    FUNCTION NAME:   HwdRfGetRfBsiCw

    DESCRIPTION:     This routine is used to get the RF BSI CWs of Gating on/off
                     and DDPC read
    PARAMETERS:      void

    RETURNED VALUES: void

*****************************************************************************/
extern void HwdRfGetRfBsiCw(HwdRfSpiCwT *pRfBsiCw);
/*****************************************************************************

    FUNCTION NAME:   HwdRfMpaRxPathToRfRxPath

    DESCRIPTION:     This routine is used to convert Mpa rx path type to Rf rx path type

    PARAMETERS:      HwdRfMpaEnumT

    RETURNED VALUES: HwdRfRxEnumT

*****************************************************************************/
extern HwdRfRxEnumT HwdRfMpaRxPathToRfRxPath(HwdRfMpaEnumT rfMpaPath);
#ifdef MTK_DEV_HW_SIM_RF
extern HwdRfRxEnumT HwdRfMpaRxPathToRfRxPathHwSim(HwdRfMpaEnumT rfMpaPath);
#endif

#if (SYS_BOARD >= SB_JADE)
#ifdef __INCREASE_VS1_VOLTAGE_SUPPORT__
extern void HwdRfVrf18PsrrFix(HwdRfWinTypeT type, HwdRfWinOpT op);
#endif
#endif

#if defined(__TX_POWER_OFFSET_SUPPORT__) || defined(__INCREASE_VS1_VOLTAGE_SUPPORT__)
/*****************************************************************************

  FUNCTION NAME:   HwdRfGetShareMemL12Md3MmrfFeatureAddr
  
  DESCRIPTION:     Get the MMRF feature share memory address

  PARAMETERS:      void
  
  RETURNED VALUES: MMRF feature share memory address.

*****************************************************************************/
extern uint32 *HwdRfGetShareMemL12Md3MmrfFeatureAddr(void);
#endif

#if (SYS_BOARD >= SB_JADE)
#ifdef __INCREASE_VS1_VOLTAGE_SUPPORT__
extern void HwdRfVrf18PsrrFix(HwdRfWinTypeT type, HwdRfWinOpT op);
extern uint32* HwdRfGetVs1TablePtr(void);
#endif
#endif

#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
extern int16 HwdGetTxSwtpOffset(void);
#endif
#endif
/*****************************************************************************
* End of File
*****************************************************************************/
#endif
