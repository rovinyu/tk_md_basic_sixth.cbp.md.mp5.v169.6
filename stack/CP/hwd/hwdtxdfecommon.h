/*******************************************************************************
*  Modification Notice:
*  --------------------------
*  This software is modified by MediaTek Inc. and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdtxdfecommon.h
 *
 * Project:
 * --------
 *   MTK OrionC/OrionPlus Project
 *
 * Description:
 * ------------
 *   This file contains the DDPC Loop Related Functions written in C.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:$
 * $Modtime:$
 * $Log:$
 *
 *
 *
 *
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 ******************************************************************************/
#ifndef HWDTXDFECOMMON_H
#define HWDTXDFECOMMON_H

#include "hwddefs.h"
#include "hwdapi.h"
#include "hwdrfapi.h"



/*****************************************************************************
  STRUCT/ENUM DEFINES:
*****************************************************************************/
/** Define Tx operation mode: open/close loop */
typedef enum
{
    TX_MODE_OPEN_LOOP = 0,  /** Open loop mode */
    TX_MODE_CLOSE_LOOP,     /** Close loop mode */
    TX_MODE_NUM
}TxMode;

/** Define Tx clip scale selection */
typedef enum
{
    TX_CLIP_SCALE_OLD_MODE = 0,  /** Use old scale with gain */
    TX_CLIP_SCALE_NEW_MODE,      /** Remove scale for tx clipper */
    TX_CLIP_SCALE_MODE_NUM
}TxClipScaleMode;

/** Define fine digital gain adjustment module operation status */
typedef enum
{
    TX_FINE_GAIN_ADJ_DISEN = 0, /** Disable fine digital gain adjustment module */
    TX_FINE_GAIN_ADJ_EN,        /** Enable fine digital gain adjustment module */
    TX_FINE_GAIN_ADJ_NUM,
}TxFineGainAdjMode;


/*****************************************************************************
  MACRO DEFINES:
*****************************************************************************/
#define TX_ORIONPLUS_EN     1
#define TX_DET_DET_ANTI_DROOP_TRUNC_SEL     7
    
/** Define bit position of NCO module reset bit offset */
#define TX_DO_NCO_RSTN_BIT_OFT            0x5
    
/** Define bit position of TxUPC module reset bit offset */
#define TX_DO_UPC_RSTN_BIT_OFT            0x6
    
/** Define bit position of fine digital gain module reset bit offset */
#define TX_DO_FINE_GAIN_RSTN_BIT_OFT      0x7
    
/** Define DDPC Loop mode bit offset */
#define TX_DO_LOOP_MODE_BIT_OFT          0x1
    
/** Define Tx Clip Scale mode bit offset */
#define TX_DO_CLIP_SCALE_MODE_BIT_OFT     0x2
    
/** Define bit offset of GBB1 field for 2nd half slot */
#define TX_DO_HALF_SLOT_GBB1_OFT         0x4
    
/** Define NCO enable bit offset */
#define TX_DO_NCO_EN_BIT_OFT              0xB


/*****************************************************************************
  GLOBAL FUNCTION DEFINES:
*****************************************************************************/


/*****************************************************************************
  Global Varible
*****************************************************************************/


/*************************************************************************
      End of the file
*************************************************************************/
#endif

