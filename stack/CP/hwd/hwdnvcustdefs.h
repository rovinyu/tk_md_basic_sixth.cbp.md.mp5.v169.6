/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdbpi.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _HWDNVCUSTDEFS_H_
#define _HWDNVCUSTDEFS_H_

#define RF_CUST_CUSTOM_DATA_BC_SUPPORT(sETiDX) \
{\
    {CUST_BAND_A_##sETiDX, BAND_A_SUPPORTED_##sETiDX, HWD_RF_SUB_CLASS_ALL_SUPPORTED},\
    {CUST_BAND_B_##sETiDX, BAND_B_SUPPORTED_##sETiDX, HWD_RF_SUB_CLASS_ALL_SUPPORTED},\
    {CUST_BAND_C_##sETiDX, BAND_C_SUPPORTED_##sETiDX, HWD_RF_SUB_CLASS_ALL_SUPPORTED},\
    {CUST_BAND_D_##sETiDX, BAND_D_SUPPORTED_##sETiDX, HWD_RF_SUB_CLASS_ALL_SUPPORTED},\
    {CUST_BAND_E_##sETiDX, BAND_E_SUPPORTED_##sETiDX, HWD_RF_SUB_CLASS_ALL_SUPPORTED},\
}

#define RF_CUST_CUSTOM_DATA_BPI_TIMING(sETiDX) \
{\
    /* RF window timing */\
    M_UsToChips(TC_PR1_##sETiDX),\
    M_UsToChips(TC_PR2_##sETiDX),\
    M_UsToChips(TC_PR2B_##sETiDX),\
    M_UsToChips(TC_PR3_##sETiDX),\
    M_UsToChips(TC_PR3A),\
\
    M_UsToChips(TC_RXD_PR1),\
    M_UsToChips(TC_RXD_PR2),\
    M_UsToChips(TC_RXD_PR2B),\
    M_UsToChips(TC_RXD_PR3),\
    M_UsToChips(TC_RXD_PR3A),\
\
    M_UsToChips(TC_PT1_##sETiDX),\
    M_UsToChips(TC_PT2_##sETiDX),\
    M_UsToChips(TC_PT2B_##sETiDX),\
    M_UsToChips(TC_PT3_##sETiDX),\
    M_UsToChips(TC_PT3A),\
\
    /* RF Gate timing */\
    M_UsToChips(TC_PRG1),\
    M_UsToChips(TC_PRG2),\
    M_UsToChips(TC_PRG2B),\
    M_UsToChips(TC_PRG3),\
    M_UsToChips(TC_PRG3A),\
\
    M_UsToChips(TC_RXD_PRG1),\
    M_UsToChips(TC_RXD_PRG2),\
    M_UsToChips(TC_RXD_PRG2B),\
    M_UsToChips(TC_RXD_PRG3),\
    M_UsToChips(TC_RXD_PRG3A),\
\
    M_UsToChips(TC_PTG1_##sETiDX),\
    M_UsToChips(TC_PTG2_##sETiDX),\
    M_UsToChips(TC_PTG2B_##sETiDX),\
    M_UsToChips(TC_PTG3_##sETiDX),\
    M_UsToChips(TC_PTG3A),\
\
    /* RF Power Control timing */\
    M_UsToChips(TC_PRPC1),\
    M_UsToChips(TC_PRPC2),\
    M_UsToChips(TC_PRPC2B),\
    M_UsToChips(TC_PRPC3),\
    M_UsToChips(TC_PRPC3A),\
\
    M_UsToChips(TC_RXD_PRPC1),\
    M_UsToChips(TC_RXD_PRPC2),\
    M_UsToChips(TC_RXD_PRPC2B),\
    M_UsToChips(TC_RXD_PRPC3),\
    M_UsToChips(TC_RXD_PRPC3A),\
\
    M_UsToChips(TC_PTPC1_##sETiDX),\
    M_UsToChips(TC_PTPC2_##sETiDX),\
    M_UsToChips(TC_PTPC2B_##sETiDX),\
    M_UsToChips(TC_PTPC3),\
    M_UsToChips(TC_PTPC3A),\
},\
{\
    M_UsToChips(DC2DC_OFFSET_##sETiDX),\
}

#define RF_CUST_CUSTOM_DATA_RF_PORTS(sETiDX)\
/* RX LNA port selection */\
{\
    BAND_A_RX_IO_SEL_##sETiDX,\
    BAND_B_RX_IO_SEL_##sETiDX,\
    BAND_C_RX_IO_SEL_##sETiDX,\
    BAND_D_RX_IO_SEL_##sETiDX,\
    BAND_E_RX_IO_SEL_##sETiDX,\
},\
/* RX Diverisity LNA port selection */\
{\
    BAND_A_RXD_IO_SEL_##sETiDX,\
    BAND_B_RXD_IO_SEL_##sETiDX,\
    BAND_C_RXD_IO_SEL_##sETiDX,\
    BAND_D_RXD_IO_SEL_##sETiDX,\
    BAND_E_RXD_IO_SEL_##sETiDX,\
},\
/* TX path selection */\
{\
    BAND_A_TX_IO_SEL_##sETiDX,\
    BAND_B_TX_IO_SEL_##sETiDX,\
    BAND_C_TX_IO_SEL_##sETiDX,\
    BAND_D_TX_IO_SEL_##sETiDX,\
    BAND_E_TX_IO_SEL_##sETiDX,\
},\
/* TX detect path selection */\
{\
    BAND_A_TX_DET_IO_SEL_##sETiDX,\
    BAND_B_TX_DET_IO_SEL_##sETiDX,\
    BAND_C_TX_DET_IO_SEL_##sETiDX,\
    BAND_D_TX_DET_IO_SEL_##sETiDX,\
    BAND_E_TX_DET_IO_SEL_##sETiDX,\
}

#ifdef SYS_OPTION_HPUE_ENABLED
#define RF_CUST_CUSTOM_DATA_ENS(sETiDX)\
/* Rx diversity enable */\
RX_DIVERSITY_ENABLE_##sETiDX,\
/* Rx diversity test enable. The diversity path will be always on if this is TRUE. Used by HSC */\
RX_DIVERSITY_ONLY_TEST_##sETiDX,\
/* PMU VPA control disable/enable */\
PA_VDD_PMU_ENABLE_##sETiDX,\
/* BATT VPA control disable/enable */\
PA_VDD_BATT_ENABLE_##sETiDX,\
/* DC2DC VPA control disable/enable */\
PA_VDD_DC2DC_ENABLE_##sETiDX,\
/* HPUE VPA control disable/enable */\
PA_VDD_HPUE_ENABLE_##sETiDX,\
/* Internal temperature measurement enable*/\
TEMPERATURE_MEAS_EN_##sETiDX
#else
#define RF_CUST_CUSTOM_DATA_ENS(sETiDX)\
/* Rx diversity enable */\
RX_DIVERSITY_ENABLE_##sETiDX,\
/* Rx diversity test enable. The diversity path will be always on if this is TRUE. Used by HSC */\
RX_DIVERSITY_ONLY_TEST_##sETiDX,\
/* PMU VPA control disable/enable */\
PA_VDD_PMU_ENABLE_##sETiDX,\
/* BATT VPA control disable/enable */\
PA_VDD_BATT_ENABLE_##sETiDX,\
/* DC2DC VPA control disable/enable */\
PA_VDD_DC2DC_ENABLE_##sETiDX,\
/* Internal temperature measurement enable*/\
TEMPERATURE_MEAS_EN_##sETiDX
#endif

#if defined(HWD_ETM_NV_ENABLE)
#define RF_CUST_CUSTOM_DATA_EN_ETM(sETiDX)\
/* ETM VPA control disable/enable*/\
PA_VDD_ETM_ENABLE_##sETiDX
#else
#define RF_CUST_CUSTOM_DATA_EN_ETM(sETiDX) FALSE
#endif

#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
#define RF_CUST_CUSTOM_DATA_EN_SAR(sETiDX)\
/*SAR feature disable/enable*/\
TPO_ENABLE_##sETiDX,\
/*SAR feature metamode disable/enable*/\
TPO_META_ENABLE_##sETiDX
#endif
/* The default table of RF custom parameters */
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
#define RF_CUST_CUSTOM_DATA(sETiDX) \
const HwdRfCustomDataT hwdRfCustomData_##sETiDX =\
{\
    0, 1, RF_CUST_CUSTOM_DATA_BC_SUPPORT(sETiDX),\
    RF_CUST_CUSTOM_DATA_BPI_TIMING(sETiDX),\
    RF_CUST_CUSTOM_DATA_RF_PORTS(sETiDX),\
    RF_CUST_CUSTOM_DATA_ENS(sETiDX),\
    RF_CUST_CUSTOM_DATA_EN_SAR(sETiDX),\
    RF_CUST_CUSTOM_DATA_EN_ETM(sETiDX)\
}
#else
#define RF_CUST_CUSTOM_DATA(sETiDX) \
const HwdRfCustomDataT hwdRfCustomData_##sETiDX =\
{\
    0, 1, RF_CUST_CUSTOM_DATA_BC_SUPPORT(sETiDX),\
    RF_CUST_CUSTOM_DATA_BPI_TIMING(sETiDX),\
    RF_CUST_CUSTOM_DATA_RF_PORTS(sETiDX),\
    RF_CUST_CUSTOM_DATA_ENS(sETiDX),\
    RF_CUST_CUSTOM_DATA_EN_ETM(sETiDX)\
}
#endif
/* BPI mask segment default values */
#define RF_CUST_BPI_MASK(sETiDX) \
const HwdRfCustBpiMaskT hwdRfCustomBpiMask_##sETiDX =\
{\
    {\
        /* Band A mask */\
        {\
            PMASK_BAND_A_PR1_##sETiDX,\
            PMASK_BAND_A_PR2_##sETiDX,\
            PMASK_BAND_A_PR2B_##sETiDX,\
            PMASK_BAND_A_PR3_##sETiDX,\
            PMASK_BAND_A_PR3A,\
            PMASK_BAND_A_PT1_##sETiDX,\
            PMASK_BAND_A_PT2_##sETiDX,\
            PMASK_BAND_A_PT2B_##sETiDX,\
            PMASK_BAND_A_PT3_##sETiDX,\
            PMASK_BAND_A_PT3A,\
            PMASK2_BAND_A_PR1_##sETiDX,\
            PMASK2_BAND_A_PR2_##sETiDX,\
            PMASK2_BAND_A_PR2B_##sETiDX,\
            PMASK2_BAND_A_PR3_##sETiDX,\
            PMASK2_BAND_A_PR3A,\
            /* TX gate mask */\
            PMASK_BAND_A_PRG1,\
            PMASK_BAND_A_PRG2,\
            PMASK_BAND_A_PRG2B,\
            PMASK_BAND_A_PRG3,\
            PMASK_BAND_A_PRG3A,\
            PMASK_BAND_A_PTG1_##sETiDX,\
            PMASK_BAND_A_PTG2_##sETiDX,\
            PMASK_BAND_A_PTG2B_##sETiDX,\
            PMASK_BAND_A_PTG3_##sETiDX,\
            PMASK_BAND_A_PTG3A,\
            PMASK2_BAND_A_PRG1,\
            PMASK2_BAND_A_PRG2,\
            PMASK2_BAND_A_PRG2B,\
            PMASK2_BAND_A_PRG3,\
            PMASK2_BAND_A_PRG3A,\
        },\
        /* Band B mask */\
        {\
            PMASK_BAND_B_PR1_##sETiDX,\
            PMASK_BAND_B_PR2_##sETiDX,\
            PMASK_BAND_B_PR2B_##sETiDX,\
            PMASK_BAND_B_PR3_##sETiDX,\
            PMASK_BAND_B_PR3A,\
            PMASK_BAND_B_PT1_##sETiDX,\
            PMASK_BAND_B_PT2_##sETiDX,\
            PMASK_BAND_B_PT2B_##sETiDX,\
            PMASK_BAND_B_PT3_##sETiDX,\
            PMASK_BAND_B_PT3A,\
            PMASK2_BAND_B_PR1_##sETiDX,\
            PMASK2_BAND_B_PR2_##sETiDX,\
            PMASK2_BAND_B_PR2B_##sETiDX,\
            PMASK2_BAND_B_PR3_##sETiDX,\
            PMASK2_BAND_B_PR3A,\
            /* TX gate mask */\
            PMASK_BAND_B_PRG1,\
            PMASK_BAND_B_PRG2,\
            PMASK_BAND_B_PRG2B,\
            PMASK_BAND_B_PRG3,\
            PMASK_BAND_B_PRG3A,\
            PMASK_BAND_B_PTG1_##sETiDX,\
            PMASK_BAND_B_PTG2_##sETiDX,\
            PMASK_BAND_B_PTG2B_##sETiDX,\
            PMASK_BAND_B_PTG3_##sETiDX,\
            PMASK_BAND_B_PTG3A,\
            PMASK2_BAND_B_PRG1,\
            PMASK2_BAND_B_PRG2,\
            PMASK2_BAND_B_PRG2B,\
            PMASK2_BAND_B_PRG3,\
            PMASK2_BAND_B_PRG3A,\
        },\
        /* Band C mask */\
        {\
            PMASK_BAND_C_PR1_##sETiDX,\
            PMASK_BAND_C_PR2_##sETiDX,\
            PMASK_BAND_C_PR2B_##sETiDX,\
            PMASK_BAND_C_PR3_##sETiDX,\
            PMASK_BAND_C_PR3A,\
            PMASK_BAND_C_PT1_##sETiDX,\
            PMASK_BAND_C_PT2_##sETiDX,\
            PMASK_BAND_C_PT2B_##sETiDX,\
            PMASK_BAND_C_PT3_##sETiDX,\
            PMASK_BAND_C_PT3A,\
            PMASK2_BAND_C_PR1_##sETiDX,\
            PMASK2_BAND_C_PR2_##sETiDX,\
            PMASK2_BAND_C_PR2B_##sETiDX,\
            PMASK2_BAND_C_PR3_##sETiDX,\
            PMASK2_BAND_C_PR3A,\
            /* TX gate mask */\
            PMASK_BAND_C_PRG1,\
            PMASK_BAND_C_PRG2,\
            PMASK_BAND_C_PRG2B,\
            PMASK_BAND_C_PRG3,\
            PMASK_BAND_C_PRG3A,\
            PMASK_BAND_C_PTG1_##sETiDX,\
            PMASK_BAND_C_PTG2_##sETiDX,\
            PMASK_BAND_C_PTG2B_##sETiDX,\
            PMASK_BAND_C_PTG3_##sETiDX,\
            PMASK_BAND_C_PTG3A,\
            PMASK2_BAND_C_PRG1,\
            PMASK2_BAND_C_PRG2,\
            PMASK2_BAND_C_PRG2B,\
            PMASK2_BAND_C_PRG3,\
            PMASK2_BAND_C_PRG3A,\
        },\
        /* Band D mask */\
        {\
            PMASK_BAND_D_PR1_##sETiDX,\
            PMASK_BAND_D_PR2_##sETiDX,\
            PMASK_BAND_D_PR2B_##sETiDX,\
            PMASK_BAND_D_PR3_##sETiDX,\
            PMASK_BAND_D_PR3A,\
            PMASK_BAND_D_PT1_##sETiDX,\
            PMASK_BAND_D_PT2_##sETiDX,\
            PMASK_BAND_D_PT2B_##sETiDX,\
            PMASK_BAND_D_PT3_##sETiDX,\
            PMASK_BAND_D_PT3A,\
            PMASK2_BAND_D_PR1_##sETiDX,\
            PMASK2_BAND_D_PR2_##sETiDX,\
            PMASK2_BAND_D_PR2B_##sETiDX,\
            PMASK2_BAND_D_PR3_##sETiDX,\
            PMASK2_BAND_D_PR3A,\
            /* TX gate mask */\
            PMASK_BAND_D_PRG1,\
            PMASK_BAND_D_PRG2,\
            PMASK_BAND_D_PRG2B,\
            PMASK_BAND_D_PRG3,\
            PMASK_BAND_D_PRG3A,\
            PMASK_BAND_D_PTG1_##sETiDX,\
            PMASK_BAND_D_PTG2_##sETiDX,\
            PMASK_BAND_D_PTG2B_##sETiDX,\
            PMASK_BAND_D_PTG3_##sETiDX,\
            PMASK_BAND_D_PTG3A,\
            PMASK2_BAND_D_PRG1,\
            PMASK2_BAND_D_PRG2,\
            PMASK2_BAND_D_PRG2B,\
            PMASK2_BAND_D_PRG3,\
            PMASK2_BAND_D_PRG3A,\
        },\
        /* Band E mask */\
        {\
            PMASK_BAND_E_PR1_##sETiDX,\
            PMASK_BAND_E_PR2_##sETiDX,\
            PMASK_BAND_E_PR2B_##sETiDX,\
            PMASK_BAND_E_PR3_##sETiDX,\
            PMASK_BAND_E_PR3A,\
            PMASK_BAND_E_PT1_##sETiDX,\
            PMASK_BAND_E_PT2_##sETiDX,\
            PMASK_BAND_E_PT2B_##sETiDX,\
            PMASK_BAND_E_PT3_##sETiDX,\
            PMASK_BAND_E_PT3A,\
            PMASK2_BAND_E_PR1_##sETiDX,\
            PMASK2_BAND_E_PR2_##sETiDX,\
            PMASK2_BAND_E_PR2B_##sETiDX,\
            PMASK2_BAND_E_PR3_##sETiDX,\
            PMASK2_BAND_E_PR3A,\
            /* TX gate mask */\
            PMASK_BAND_E_PRG1,\
            PMASK_BAND_E_PRG2,\
            PMASK_BAND_E_PRG2B,\
            PMASK_BAND_E_PRG3,\
            PMASK_BAND_E_PRG3A,\
            PMASK_BAND_E_PTG1_##sETiDX,\
            PMASK_BAND_E_PTG2_##sETiDX,\
            PMASK_BAND_E_PTG2B_##sETiDX,\
            PMASK_BAND_E_PTG3_##sETiDX,\
            PMASK_BAND_E_PTG3A,\
            PMASK2_BAND_E_PRG1,\
            PMASK2_BAND_E_PRG2,\
            PMASK2_BAND_E_PRG2B,\
            PMASK2_BAND_E_PRG3,\
            PMASK2_BAND_E_PRG3A,\
        }\
    },\
    /* Power Control BPI mask */\
    {\
        PMASK_PRPC1,\
        PMASK_PRPC2,\
        PMASK_PRPC2B,\
        PMASK_PRPC3,\
        PMASK_PRPC3A,\
        PMASK_PTPC1_##sETiDX,\
        PMASK_PTPC2_##sETiDX,\
        PMASK_PTPC2B_##sETiDX,\
        PMASK_PTPC3,\
        PMASK_PTPC3A,\
        PMASK2_PRPC1,\
        PMASK2_PRPC2,\
        PMASK2_PRPC2B,\
        PMASK2_PRPC3,\
        PMASK2_PRPC3A,\
    }\
}

/* BPI data segment default values */
#define RF_CUST_BPI_DATA(sETiDX) \
const HwdRfCustBpiDataT hwdRfCustomBpiData_##sETiDX =\
{\
    {\
        /* Band A data */\
        {\
            PDATA_BAND_A_PR1_##sETiDX,\
            PDATA_BAND_A_PR2_##sETiDX,\
            PDATA_BAND_A_PR2B_##sETiDX,\
            PDATA_BAND_A_PR3_##sETiDX,\
            PDATA_BAND_A_PR3A,\
            PDATA_BAND_A_PT1_##sETiDX,\
            PDATA_BAND_A_PT2_##sETiDX,\
            PDATA_BAND_A_PT2B_##sETiDX,\
            PDATA_BAND_A_PT3_##sETiDX,\
            PDATA_BAND_A_PT3A,\
            PDATA2_BAND_A_PR1_##sETiDX,\
            PDATA2_BAND_A_PR2_##sETiDX,\
            PDATA2_BAND_A_PR2B_##sETiDX,\
            PDATA2_BAND_A_PR3_##sETiDX,\
            PDATA2_BAND_A_PR3A,\
\
            /* TX gate data */\
            PDATA_BAND_A_PRG1,\
            PDATA_BAND_A_PRG2,\
            PDATA_BAND_A_PRG2B,\
            PDATA_BAND_A_PRG3,\
            PDATA_BAND_A_PRG3A,\
            PDATA_BAND_A_PTG1_##sETiDX,\
            PDATA_BAND_A_PTG2_##sETiDX,\
            PDATA_BAND_A_PTG2B_##sETiDX,\
            PDATA_BAND_A_PTG3_##sETiDX,\
            PDATA_BAND_A_PTG3A,\
            PDATA2_BAND_A_PRG1,\
            PDATA2_BAND_A_PRG2,\
            PDATA2_BAND_A_PRG2B,\
            PDATA2_BAND_A_PRG3,\
            PDATA2_BAND_A_PRG3A,\
        },\
        /* Band B data */\
        {\
            PDATA_BAND_B_PR1_##sETiDX,\
            PDATA_BAND_B_PR2_##sETiDX,\
            PDATA_BAND_B_PR2B_##sETiDX,\
            PDATA_BAND_B_PR3_##sETiDX,\
            PDATA_BAND_B_PR3A,\
            PDATA_BAND_B_PT1_##sETiDX,\
            PDATA_BAND_B_PT2_##sETiDX,\
            PDATA_BAND_B_PT2B_##sETiDX,\
            PDATA_BAND_B_PT3_##sETiDX,\
            PDATA_BAND_B_PT3A,\
            PDATA2_BAND_B_PR1_##sETiDX,\
            PDATA2_BAND_B_PR2_##sETiDX,\
            PDATA2_BAND_B_PR2B_##sETiDX,\
            PDATA2_BAND_B_PR3_##sETiDX,\
            PDATA2_BAND_B_PR3A,\
\
            /* TX gate data */\
            PDATA_BAND_B_PRG1,\
            PDATA_BAND_B_PRG2,\
            PDATA_BAND_B_PRG2B,\
            PDATA_BAND_B_PRG3,\
            PDATA_BAND_B_PRG3A,\
            PDATA_BAND_B_PTG1_##sETiDX,\
            PDATA_BAND_B_PTG2_##sETiDX,\
            PDATA_BAND_B_PTG2B_##sETiDX,\
            PDATA_BAND_B_PTG3_##sETiDX,\
            PDATA_BAND_B_PTG3A,\
            PDATA2_BAND_B_PRG1,\
            PDATA2_BAND_B_PRG2,\
            PDATA2_BAND_B_PRG2B,\
            PDATA2_BAND_B_PRG3,\
            PDATA2_BAND_B_PRG3A,\
        },\
        /* Band C data */\
        {\
            PDATA_BAND_C_PR1_##sETiDX,\
            PDATA_BAND_C_PR2_##sETiDX,\
            PDATA_BAND_C_PR2B_##sETiDX,\
            PDATA_BAND_C_PR3_##sETiDX,\
            PDATA_BAND_C_PR3A,\
            PDATA_BAND_C_PT1_##sETiDX,\
            PDATA_BAND_C_PT2_##sETiDX,\
            PDATA_BAND_C_PT2B_##sETiDX,\
            PDATA_BAND_C_PT3_##sETiDX,\
            PDATA_BAND_C_PT3A,\
            PDATA2_BAND_C_PR1_##sETiDX,\
            PDATA2_BAND_C_PR2_##sETiDX,\
            PDATA2_BAND_C_PR2B_##sETiDX,\
            PDATA2_BAND_C_PR3_##sETiDX,\
            PDATA2_BAND_C_PR3A,\
\
            /* TX gate data */\
            PDATA_BAND_C_PRG1,\
            PDATA_BAND_C_PRG2,\
            PDATA_BAND_C_PRG2B,\
            PDATA_BAND_C_PRG3,\
            PDATA_BAND_C_PRG3A,\
            PDATA_BAND_C_PTG1_##sETiDX,\
            PDATA_BAND_C_PTG2_##sETiDX,\
            PDATA_BAND_C_PTG2B_##sETiDX,\
            PDATA_BAND_C_PTG3_##sETiDX,\
            PDATA_BAND_C_PTG3A,\
            PDATA2_BAND_C_PRG1,\
            PDATA2_BAND_C_PRG2,\
            PDATA2_BAND_C_PRG2B,\
            PDATA2_BAND_C_PRG3,\
            PDATA2_BAND_C_PRG3A,\
        },\
        /* Band D data */\
        {\
            PDATA_BAND_D_PR1_##sETiDX,\
            PDATA_BAND_D_PR2_##sETiDX,\
            PDATA_BAND_D_PR2B_##sETiDX,\
            PDATA_BAND_D_PR3_##sETiDX,\
            PDATA_BAND_D_PR3A,\
            PDATA_BAND_D_PT1_##sETiDX,\
            PDATA_BAND_D_PT2_##sETiDX,\
            PDATA_BAND_D_PT2B_##sETiDX,\
            PDATA_BAND_D_PT3_##sETiDX,\
            PDATA_BAND_D_PT3A,\
            PDATA2_BAND_D_PR1_##sETiDX,\
            PDATA2_BAND_D_PR2_##sETiDX,\
            PDATA2_BAND_D_PR2B_##sETiDX,\
            PDATA2_BAND_D_PR3_##sETiDX,\
            PDATA2_BAND_D_PR3A,\
\
            /* TX gate data */\
            PDATA_BAND_D_PRG1,\
            PDATA_BAND_D_PRG2,\
            PDATA_BAND_D_PRG2B,\
            PDATA_BAND_D_PRG3,\
            PDATA_BAND_D_PRG3A,\
            PDATA_BAND_D_PTG1_##sETiDX,\
            PDATA_BAND_D_PTG2_##sETiDX,\
            PDATA_BAND_D_PTG2B_##sETiDX,\
            PDATA_BAND_D_PTG3_##sETiDX,\
            PDATA_BAND_D_PTG3A,\
            PDATA2_BAND_D_PRG1,\
            PDATA2_BAND_D_PRG2,\
            PDATA2_BAND_D_PRG2B,\
            PDATA2_BAND_D_PRG3,\
            PDATA2_BAND_D_PRG3A,\
        },\
        /* Band E data */\
        {\
            PDATA_BAND_E_PR1_##sETiDX,\
            PDATA_BAND_E_PR2_##sETiDX,\
            PDATA_BAND_E_PR2B_##sETiDX,\
            PDATA_BAND_E_PR3_##sETiDX,\
            PDATA_BAND_E_PR3A,\
            PDATA_BAND_E_PT1_##sETiDX,\
            PDATA_BAND_E_PT2_##sETiDX,\
            PDATA_BAND_E_PT2B_##sETiDX,\
            PDATA_BAND_E_PT3_##sETiDX,\
            PDATA_BAND_E_PT3A,\
            PDATA2_BAND_E_PR1_##sETiDX,\
            PDATA2_BAND_E_PR2_##sETiDX,\
            PDATA2_BAND_E_PR2B_##sETiDX,\
            PDATA2_BAND_E_PR3_##sETiDX,\
            PDATA2_BAND_E_PR3A,\
        \
            /* TX gate data */\
            PDATA_BAND_E_PRG1,\
            PDATA_BAND_E_PRG2,\
            PDATA_BAND_E_PRG2B,\
            PDATA_BAND_E_PRG3,\
            PDATA_BAND_E_PRG3A,\
            PDATA_BAND_E_PTG1_##sETiDX,\
            PDATA_BAND_E_PTG2_##sETiDX,\
            PDATA_BAND_E_PTG2B_##sETiDX,\
            PDATA_BAND_E_PTG3_##sETiDX,\
            PDATA_BAND_E_PTG3A,\
            PDATA2_BAND_E_PRG1,\
            PDATA2_BAND_E_PRG2,\
            PDATA2_BAND_E_PRG2B,\
            PDATA2_BAND_E_PRG3,\
            PDATA2_BAND_E_PRG3A,\
        }\
    },\
    {\
        /* Power Control BPI data */\
        PDATA_PRPC1,\
        PDATA_PRPC2,\
        PDATA_PRPC2B,\
        PDATA_PRPC3,\
        PDATA_PRPC3A,\
        PDATA_PTPC1_##sETiDX,\
        PDATA_PTPC2_##sETiDX,\
        PDATA_PTPC2B_##sETiDX,\
        PDATA_PTPC3,\
        PDATA_PTPC3A,\
        PDATA2_PRPC1,\
        PDATA2_PRPC2,\
        PDATA2_PRPC2B,\
        PDATA2_PRPC3,\
        PDATA2_PRPC3A,\
    }\
}

/* TAS default configurations */
#define RF_CUST_TAS_DATA(sETiDX) \
const HwdRfCustTasT hwdRfCustomTas_##sETiDX =\
{\
    /* TAS enable */\
    C2K_TAS_ENABLE_##sETiDX,\
    /* TAS test SIM enable */\
    C2K_TAS_TEST_SIM_ENABLE_##sETiDX,\
    /* TAS initial antenna index */\
    C2K_TAS_INIT_ANT_##sETiDX,\
    /* Force Tx antenna enalbe */\
    C2K_FORCE_TX_ANTENNA_ENABLE_##sETiDX,\
    /* Forced Tx antenna index */\
    C2K_FORCE_TX_ANTENNA_IDX_##sETiDX,\
    /* TAS data mask */\
    C2K_TAS_BPI_MASK_##sETiDX,\
    /* TAS data by Band */\
    {\
        /* Band A */\
        {\
            PDATA_BAND_A_TAS1_##sETiDX,\
            PDATA_BAND_A_TAS2_##sETiDX,\
            PDATA_BAND_A_TAS3_##sETiDX,\
            PDATA_BAND_A_TAS4_##sETiDX,\
            PDATA_BAND_A_TAS5_##sETiDX,\
            PDATA_BAND_A_TAS6_##sETiDX,\
            PDATA_BAND_A_TAS7_##sETiDX\
        },\
        /* Band B */\
        {\
            PDATA_BAND_B_TAS1_##sETiDX,\
            PDATA_BAND_B_TAS2_##sETiDX,\
            PDATA_BAND_B_TAS3_##sETiDX,\
            PDATA_BAND_B_TAS4_##sETiDX,\
            PDATA_BAND_B_TAS5_##sETiDX,\
            PDATA_BAND_B_TAS6_##sETiDX,\
            PDATA_BAND_B_TAS7_##sETiDX\
        },\
        /* Band C */\
        {\
            PDATA_BAND_C_TAS1_##sETiDX,\
            PDATA_BAND_C_TAS2_##sETiDX,\
            PDATA_BAND_C_TAS3_##sETiDX,\
            PDATA_BAND_C_TAS4_##sETiDX,\
            PDATA_BAND_C_TAS5_##sETiDX,\
            PDATA_BAND_C_TAS6_##sETiDX,\
            PDATA_BAND_C_TAS7_##sETiDX\
        },\
        /* Band D */\
        {\
            PDATA_BAND_D_TAS1_##sETiDX,\
            PDATA_BAND_D_TAS2_##sETiDX,\
            PDATA_BAND_D_TAS3_##sETiDX,\
            PDATA_BAND_D_TAS4_##sETiDX,\
            PDATA_BAND_D_TAS5_##sETiDX,\
            PDATA_BAND_D_TAS6_##sETiDX,\
            PDATA_BAND_D_TAS7_##sETiDX\
        },\
        /* Band E */\
        {\
            PDATA_BAND_E_TAS1_##sETiDX,\
            PDATA_BAND_E_TAS2_##sETiDX,\
            PDATA_BAND_E_TAS3_##sETiDX,\
            PDATA_BAND_E_TAS4_##sETiDX,\
            PDATA_BAND_E_TAS5_##sETiDX,\
            PDATA_BAND_E_TAS6_##sETiDX,\
            PDATA_BAND_E_TAS7_##sETiDX\
        }\
    },\
    {\
        ANT_IDX_ON_TEST_SIM_BAND_A_##sETiDX,\
        ANT_IDX_ON_TEST_SIM_BAND_B_##sETiDX,\
        ANT_IDX_ON_TEST_SIM_BAND_C_##sETiDX,\
        ANT_IDX_ON_TEST_SIM_BAND_D_##sETiDX,\
        ANT_IDX_ON_TEST_SIM_BAND_E_##sETiDX,\
    }\
};\
const uint8 dummyCalData_##sETiDX = 0;\
const uint8 dummyCustData_##sETiDX = 0

/***** MIPI PARAMs ******/
#define MIPI_CUSTOM_PARAM(sETiDX) \
HwdMipiCustParamT HwdMipiCustomParam_##sETiDX[] =\
{\
    {IS_MIPI_FRONT_END_ENABLE_##sETiDX}\
}

#ifdef __DYNAMIC_ANTENNA_TUNING__
#define C2K_DAT_FEATURE_ENABLE(s)                  C2K_DAT_FEATURE_ENABLE_##s

#define C2K_MIPI_DAT_EVENT(rt,s)                   C2K_##rt##_MIPI_EVENT_##s
#define C2K_MIPI_DAT_DATA(rt,s)                    C2K_##rt##_MIPI_DATA_##s

#define C2K_SB_DAT_CONFIGURE(b,s)\
{\
  {\
        {b##_DAT_STATE0_CAT_A_CONFIG_##s, b##_DAT_STATE0_CAT_B_CONFIG_##s},  \
        {b##_DAT_STATE1_CAT_A_CONFIG_##s, b##_DAT_STATE1_CAT_B_CONFIG_##s} , \
        {b##_DAT_STATE2_CAT_A_CONFIG_##s, b##_DAT_STATE2_CAT_B_CONFIG_##s} , \
        {b##_DAT_STATE3_CAT_A_CONFIG_##s, b##_DAT_STATE3_CAT_B_CONFIG_##s} , \
        {b##_DAT_STATE4_CAT_A_CONFIG_##s, b##_DAT_STATE4_CAT_B_CONFIG_##s} , \
        {b##_DAT_STATE5_CAT_A_CONFIG_##s, b##_DAT_STATE5_CAT_B_CONFIG_##s} , \
        {b##_DAT_STATE6_CAT_A_CONFIG_##s, b##_DAT_STATE6_CAT_B_CONFIG_##s} , \
        {b##_DAT_STATE7_CAT_A_CONFIG_##s, b##_DAT_STATE7_CAT_B_CONFIG_##s} , \
  }\
}

#define   C2K_DAT_FE_ROUTE_DATABASE(sETiDX)\
const C2K_CUSTOM_DAT_FE_ROUTE_DATABASE_T hwdRfCustomDatRouteDataBase_##sETiDX = \
{\
  C2K_DAT_FEATURE_ENABLE_##sETiDX,\
  {\
    C2K_SB_DAT_CONFIGURE( C2K_BAND_A  , sETiDX ) ,\
    C2K_SB_DAT_CONFIGURE( C2K_BAND_B  , sETiDX ) ,\
    C2K_SB_DAT_CONFIGURE( C2K_BAND_C  , sETiDX ) ,\
    C2K_SB_DAT_CONFIGURE( C2K_BAND_D  , sETiDX ) ,\
    C2K_SB_DAT_CONFIGURE( C2K_BAND_E  , sETiDX ) ,\
  }\
};
#define   C2K_DAT_BPI_DATABASE(sETiDX)\
const HwdCustomDynamicDatBpiData hwdRfCustomDatBpiDataBase_##sETiDX =\
{\
   &(C2K_DAT_CAT_A_DATABASE_##sETiDX),\
   &(C2K_DAT_CAT_B_DATABASE_##sETiDX),\
};

#define   C2K_DAT_CAT_A_MIPI_EVENT_TABLE(sETiDX)\
const MIPI_EVENT_TABLE_T *hwdRfCustomDatCatAMipiEventTable_##sETiDX[C2K_MIPI_DAT_ROUTE_A_NUM] =\
{\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route0,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route1,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route2,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route3,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route4,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route5,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route6,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route7,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route8,     sETiDX),\
    C2K_MIPI_DAT_EVENT(      DAT_CAT_A_Route9,     sETiDX),\
};

#define   C2K_DAT_CAT_A_MIPI_DATA_TABLE(sETiDX)\
const MIPI_DATA_SUBBAND_TABLE_T *hwdRfCustomDatCatAMipiDataTable_##sETiDX[C2K_MIPI_DAT_ROUTE_A_NUM] =\
{\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route0,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route1,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route2,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route3,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route4,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route5,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route6,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route7,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route8,     sETiDX),\
   C2K_MIPI_DAT_DATA(       DAT_CAT_A_Route9,     sETiDX),\
};

#define   C2K_DAT_CAT_B_MIPI_EVENT_TABLE(sETiDX)\
const MIPI_EVENT_TABLE_T *hwdRfCustomDatCatBMipiEventTable_##sETiDX[C2K_MIPI_DAT_ROUTE_B_NUM] =\
{\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route0,      sETiDX),    /*Index  0*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route1,      sETiDX),    /*Index  1*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route2,      sETiDX),    /*Index  2*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route3,      sETiDX),    /*Index  3*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route4,      sETiDX),    /*Index  4*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route5,      sETiDX),    /*Index  5*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route6,      sETiDX),    /*Index  6*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route7,      sETiDX),    /*Index  7*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route8,      sETiDX),    /*Index  8*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route9,      sETiDX),    /*Index  9*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route10,     sETiDX),    /*Index  10*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route11,     sETiDX),    /*Index  11*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route12,     sETiDX),    /*Index  12*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route13,     sETiDX),    /*Index  13*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route14,     sETiDX),    /*Index  14*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route15,     sETiDX),    /*Index  15*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route16,     sETiDX),    /*Index  16*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route17,     sETiDX),    /*Index  17*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route18,     sETiDX),    /*Index  18*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route19,     sETiDX),    /*Index  19*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route20,     sETiDX),    /*Index  20*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route21,     sETiDX),    /*Index  21*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route22,     sETiDX),    /*Index  22*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route23,     sETiDX),    /*Index  23*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route24,     sETiDX),    /*Index  24*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route25,     sETiDX),    /*Index  25*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route26,     sETiDX),    /*Index  26*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route27,     sETiDX),    /*Index  27*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route28,     sETiDX),    /*Index  28*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route29,     sETiDX),    /*Index  29*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route30,     sETiDX),    /*Index  30*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route31,     sETiDX),    /*Index  31*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route32,     sETiDX),    /*Index  32*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route33,     sETiDX),    /*Index  33*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route34,     sETiDX),    /*Index  34*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route35,     sETiDX),    /*Index  35*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route36,     sETiDX),    /*Index  36*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route37,     sETiDX),    /*Index  37*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route38,     sETiDX),    /*Index  38*/\
       C2K_MIPI_DAT_EVENT(      DAT_CAT_B_Route39,     sETiDX),    /*Index  39*/\
};

#define   C2K_DAT_CAT_B_MIPI_DATA_TABLE0(sETiDX)\
const MIPI_DATA_SUBBAND_TABLE_T *hwdRfCustomDatCatBMipiDataTable0_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM] =\
{\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route0,      sETiDX),    /*Index  0*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route1,      sETiDX),    /*Index  1*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route2,      sETiDX),    /*Index  2*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route3,      sETiDX),    /*Index  3*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route4,      sETiDX),    /*Index  4*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route5,      sETiDX),    /*Index  5*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route6,      sETiDX),    /*Index  6*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route7,      sETiDX),    /*Index  7*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route8,      sETiDX),    /*Index  8*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route9,      sETiDX),    /*Index  9*/\
};

#define   C2K_DAT_CAT_B_MIPI_DATA_TABLE1(sETiDX)\
const MIPI_DATA_SUBBAND_TABLE_T *hwdRfCustomDatCatBMipiDataTable1_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM] =\
{\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route10,     sETiDX),    /*Index  10*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route11,     sETiDX),    /*Index  11*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route12,     sETiDX),    /*Index  12*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route13,     sETiDX),    /*Index  13*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route14,     sETiDX),    /*Index  14*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route15,     sETiDX),    /*Index  15*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route16,     sETiDX),    /*Index  16*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route17,     sETiDX),    /*Index  17*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route18,     sETiDX),    /*Index  18*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route19,     sETiDX),    /*Index  19*/\
};

#define   C2K_DAT_CAT_B_MIPI_DATA_TABLE2(sETiDX)\
const MIPI_DATA_SUBBAND_TABLE_T *hwdRfCustomDatCatBMipiDataTable2_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM] =\
{\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route20,     sETiDX),    /*Index  20*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route21,     sETiDX),    /*Index  21*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route22,     sETiDX),    /*Index  22*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route23,     sETiDX),    /*Index  23*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route24,     sETiDX),    /*Index  24*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route25,     sETiDX),    /*Index  25*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route26,     sETiDX),    /*Index  26*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route27,     sETiDX),    /*Index  27*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route28,     sETiDX),    /*Index  28*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route29,     sETiDX),    /*Index  29*/\
};

#define   C2K_DAT_CAT_B_MIPI_DATA_TABLE3(sETiDX)\
const MIPI_DATA_SUBBAND_TABLE_T *hwdRfCustomDatCatBMipiDataTable3_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM] =\
{\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route30,     sETiDX),    /*Index  30*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route31,     sETiDX),    /*Index  31*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route32,     sETiDX),    /*Index  32*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route33,     sETiDX),    /*Index  33*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route34,     sETiDX),    /*Index  34*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route35,     sETiDX),    /*Index  35*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route36,     sETiDX),    /*Index  36*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route37,     sETiDX),    /*Index  37*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route38,     sETiDX),    /*Index  38*/\
      C2K_MIPI_DAT_DATA(       DAT_CAT_B_Route39,     sETiDX),    /*Index  39*/\
};
#endif

#define HWD_RF_CUST_CUSTOM_DATA_DECLARE(sETiDX) \
extern const HwdRfCustomDataT hwdRfCustomData_##sETiDX;\
extern const HwdRfCustBpiMaskT hwdRfCustomBpiMask_##sETiDX;\
extern const HwdRfCustBpiDataT hwdRfCustomBpiData_##sETiDX;\
extern const HwdRfCustTasT hwdRfCustomTas_##sETiDX;\
extern const uint8 dummyCustData_##sETiDX

#define HWD_RF_CAL_CUSTOM_DATA_DECLARE(sETiDX) \
/** Temperature ADC cal data*/\
extern HwdTempCalibTableT          hwdDefaultTempAdcCalData_##sETiDX;\
extern HwdAfcCompCalibParmT        hwdDefaultAfcCompValue_##sETiDX;\
/** AFC cal data*/\
extern HwdAfcCalibParmT            hwdDefaultAfcValue_##sETiDX;\
/** AFC cal data*/\
extern HwdAfcTempCompTblT          hwdDefaultAfcTempComp_##sETiDX;\
/** Tx TPC level cal data*/\
extern HwdTxPwrCalibDataT          hwdDefaultTxTpcLevel_BandA_##sETiDX;\
extern HwdTxPwrCalibDataT          hwdDefaultTxTpcLevel_BandB_##sETiDX;\
extern HwdTxPwrCalibDataT          hwdDefaultTxTpcLevel_BandC_##sETiDX;\
extern HwdTxPwrCalibDataT          hwdDefaultTxTpcLevel_BandD_##sETiDX;\
extern HwdTxPwrCalibDataT          hwdDefaultTxTpcLevel_BandE_##sETiDX;\
/** Tx PA gain frequency and temperature compensation cal data*/\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainComp1xRtt_BandA_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainComp1xRtt_BandB_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainComp1xRtt_BandC_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainComp1xRtt_BandD_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainComp1xRtt_BandE_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainCompEvdo_BandA_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainCompEvdo_BandB_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainCompEvdo_BandC_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainCompEvdo_BandD_##sETiDX;\
extern HwdTxPaGainCompTblT         hwdDefaultTxPaGainCompEvdo_BandE_##sETiDX;\
/** Tx DET couplerloss cal data*/\
extern HwdTxDetCouplerLossTblT     hwdDefaultTxDetCpl_BandA_##sETiDX;\
extern HwdTxDetCouplerLossTblT     hwdDefaultTxDetCpl_BandB_##sETiDX;\
extern HwdTxDetCouplerLossTblT     hwdDefaultTxDetCpl_BandC_##sETiDX;\
extern HwdTxDetCouplerLossTblT     hwdDefaultTxDetCpl_BandD_##sETiDX;\
extern HwdTxDetCouplerLossTblT     hwdDefaultTxDetCpl_BandE_##sETiDX;\
/** Tx DET couplerloss frequency and temperature compensation cal data*/\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplComp1xRtt_BandA_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplComp1xRtt_BandB_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplComp1xRtt_BandC_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplComp1xRtt_BandD_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplComp1xRtt_BandE_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplCompEvdo_BandA_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplCompEvdo_BandB_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplCompEvdo_BandC_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplCompEvdo_BandD_##sETiDX;\
extern HwdTxDetCouplerLossCompTblT hwdDefaultTxDetCplCompEvdo_BandE_##sETiDX;\
/** Rx pathloss compensation cal data in high power mode*/\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossHpm_BandA_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossHpm_BandB_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossHpm_BandC_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossHpm_BandD_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossHpm_BandE_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossHpm_BandA_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossHpm_BandB_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossHpm_BandC_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossHpm_BandD_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossHpm_BandE_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossHpm_BandA_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossHpm_BandB_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossHpm_BandC_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossHpm_BandD_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossHpm_BandE_##sETiDX;\
/** Rx pathloss compensation cal data in low power mode*/\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossLpm_BandA_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossLpm_BandB_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossLpm_BandC_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossLpm_BandD_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultMainRxPathLossLpm_BandE_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossLpm_BandA_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossLpm_BandB_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossLpm_BandC_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossLpm_BandD_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultDivRxPathLossLpm_BandE_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossLpm_BandA_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossLpm_BandB_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossLpm_BandC_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossLpm_BandD_##sETiDX;\
extern HwdRxPathLossCompTblT       hwdDefaultShdrRxPathLossLpm_BandE_##sETiDX;\
extern const uint8 dummyCalData_##sETiDX;\
/** AGPS group delay cal data*/\
extern int16 hwdDefaultAGpsGroupDelay_BandA_##sETiDX;\
extern int16 hwdDefaultAGpsGroupDelay_BandB_##sETiDX;\
extern int16 hwdDefaultAGpsGroupDelay_BandC_##sETiDX;\
extern int16 hwdDefaultAGpsGroupDelay_BandD_##sETiDX;\
extern int16 hwdDefaultAGpsGroupDelay_BandE_##sETiDX;\
/** Tx temperature power backoff region cal data*/\
extern HwdTxPwrBackOffTblT hwdTxPwrBackOffRegion_BandA_##sETiDX;\
extern HwdTxPwrBackOffTblT hwdTxPwrBackOffRegion_BandB_##sETiDX;\
extern HwdTxPwrBackOffTblT hwdTxPwrBackOffRegion_BandC_##sETiDX;\
extern HwdTxPwrBackOffTblT hwdTxPwrBackOffRegion_BandD_##sETiDX;\
extern HwdTxPwrBackOffTblT hwdTxPwrBackOffRegion_BandE_##sETiDX;\
/** Tx power offset cal data for SWTP*/\
extern HwdTxPowerOffsetT hwdTxPowerOffset_BandA_##sETiDX;\
extern HwdTxPowerOffsetT hwdTxPowerOffset_BandB_##sETiDX;\
extern HwdTxPowerOffsetT hwdTxPowerOffset_BandC_##sETiDX;\
extern HwdTxPowerOffsetT hwdTxPowerOffset_BandD_##sETiDX;\
extern HwdTxPowerOffsetT hwdTxPowerOffset_BandE_##sETiDX;\
extern HwdSarTxPowerOffsetT hwdTxPowerOffsetSar_BandA_##sETiDX;\
extern HwdSarTxPowerOffsetT hwdTxPowerOffsetSar_BandB_##sETiDX;\
extern HwdSarTxPowerOffsetT hwdTxPowerOffsetSar_BandC_##sETiDX;\
extern HwdSarTxPowerOffsetT hwdTxPowerOffsetSar_BandD_##sETiDX;\
extern HwdSarTxPowerOffsetT hwdTxPowerOffsetSar_BandE_##sETiDX

#define HWD_MIPI_CUSTOM_DATA_DECLARE(sETiDX) \
extern HwdMipiCustParamT HwdMipiCustomParam_##sETiDX[];\
extern MIPI_IMM_DATA_TABLE_T MIPI_INITIAL_CW_##sETiDX[];\
extern MIPI_IMM_DATA_TABLE_T MIPI_SLEEP_CW_##sETiDX[];\
extern MIPI_USID_CHANGE_T MIPI_USID_CHANGE_TABLE_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_A_MIPI_RX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_B_MIPI_RX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_C_MIPI_RX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_D_MIPI_RX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_E_MIPI_RX_EVENT_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_A_MIPI_RX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_B_MIPI_RX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_C_MIPI_RX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_D_MIPI_RX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_E_MIPI_RX_DATA_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_A_MIPI_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_B_MIPI_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_C_MIPI_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_D_MIPI_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_E_MIPI_TX_EVENT_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_A_MIPI_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_B_MIPI_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_C_MIPI_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_D_MIPI_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_E_MIPI_TX_DATA_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_A_MIPI_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_B_MIPI_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_C_MIPI_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_D_MIPI_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_E_MIPI_TPC_EVENT_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_A_MIPI_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_B_MIPI_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_C_MIPI_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_D_MIPI_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_E_MIPI_TPC_DATA_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_A_MIPI_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_B_MIPI_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_C_MIPI_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_D_MIPI_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_E_MIPI_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_A_MIPI_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_B_MIPI_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_C_MIPI_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_D_MIPI_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_E_MIPI_PA_SECTION_DATA_EVDO_##sETiDX[]

/*ETM*/
#define HWD_ETM_CUSTOM_DATA_DECLARE(sETiDX) \
extern MIPI_EVENT_TABLE_T BAND_A_MIPI_ETM_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_B_MIPI_ETM_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_C_MIPI_ETM_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_D_MIPI_ETM_TX_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_E_MIPI_ETM_TX_EVENT_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_A_MIPI_ETM_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_B_MIPI_ETM_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_C_MIPI_ETM_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_D_MIPI_ETM_TX_DATA_##sETiDX[];\
extern MIPI_DATA_SUBBAND_TABLE_T BAND_E_MIPI_ETM_TX_DATA_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_A_MIPI_ETM_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_B_MIPI_ETM_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_C_MIPI_ETM_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_D_MIPI_ETM_TPC_EVENT_##sETiDX[];\
extern MIPI_EVENT_TABLE_T BAND_E_MIPI_ETM_TPC_EVENT_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_A_MIPI_ETM_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_B_MIPI_ETM_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_C_MIPI_ETM_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_D_MIPI_ETM_TPC_DATA_##sETiDX[];\
extern MIPI_DATA_TABLE_T BAND_E_MIPI_ETM_TPC_DATA_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_A_MIPI_ETM_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_B_MIPI_ETM_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_C_MIPI_ETM_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_D_MIPI_ETM_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_E_MIPI_ETM_PA_SECTION_DATA_1XRTT_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_A_MIPI_ETM_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_B_MIPI_ETM_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_C_MIPI_ETM_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_D_MIPI_ETM_PA_SECTION_DATA_EVDO_##sETiDX[];\
extern MIPI_TPC_SECTION_TABLE_T BAND_E_MIPI_ETM_PA_SECTION_DATA_EVDO_##sETiDX[]

#ifdef __DYNAMIC_ANTENNA_TUNING__
#define HWD_DAT_CUSTOM_DATA_DECLARE(sETiDX) \
extern const C2K_CUSTOM_DAT_FE_ROUTE_DATABASE_T hwdRfCustomDatRouteDataBase_##sETiDX;\
extern const HwdCustomDynamicDatBpiData hwdRfCustomDatBpiDataBase_##sETiDX;\
extern const MIPI_EVENT_TABLE_T *hwdRfCustomDatCatAMipiEventTable_##sETiDX[C2K_MIPI_DAT_ROUTE_A_NUM];\
extern const MIPI_DATA_SUBBAND_TABLE_T  *hwdRfCustomDatCatAMipiDataTable_##sETiDX[C2K_MIPI_DAT_ROUTE_A_NUM];\
extern const MIPI_EVENT_TABLE_T *hwdRfCustomDatCatBMipiEventTable_##sETiDX[C2K_MIPI_DAT_ROUTE_B_NUM];\
extern const MIPI_DATA_SUBBAND_TABLE_T  *hwdRfCustomDatCatBMipiDataTable0_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM];\
extern const MIPI_DATA_SUBBAND_TABLE_T	*hwdRfCustomDatCatBMipiDataTable1_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM];\
extern const MIPI_DATA_SUBBAND_TABLE_T	*hwdRfCustomDatCatBMipiDataTable2_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM];\
extern const MIPI_DATA_SUBBAND_TABLE_T	*hwdRfCustomDatCatBMipiDataTable3_##sETiDX[C2K_MIPI_DAT_PER_ROUTE_B_NUM]
#endif

#endif /* _HWDNVCUSTDEFS_H_ */

