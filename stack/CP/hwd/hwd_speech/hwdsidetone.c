/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "exeapi.h"
#include "monapi.h"
#include "monids.h"
#include "hwdsidetone.h"
#include "hwdspherr.h"

#include "reg_base.h"
#include "hwdsal_exp.h"


#define true         (bool)(1==1)
#define false        (bool)(1==0)

// TODO: 
// done [SAL]


typedef struct SIDETONE_INFO_STRUCT
{
	uint16 extStVolume; // value set from AP, should be Adaptive side tone volume


	ExeTimerT timer; 


	SIDETONE_SOLUTION_VER solutionVer;
	bool  isActive; // periodically update the Hw AFE Sidetone Gain 
} SIDETONE_INFO;

static SIDETONE_INFO gSidetoneInfo;

//=============================================================================


#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define WriteApReg(_addr, _value) (*(volatile uint32 *)(_addr) = (_value))
#else 
#define WriteApReg(_addr, _value)                MonTrace(MON_CP_HWD_SPH_REG_WRITE32_TRACE_ID, 2, _addr, _value)
#endif

#if defined(MTK_PLT_AUDIO)
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define AP_AFE_SIDDETONE_GAIN              (AP_AUDSYS_base + 0x01EC)
#else
#define AP_AFE_SIDDETONE_GAIN              (AP_AUDSYS_base + 0x01EC)
#endif 

#endif // defined(MT6589) || defined(MT6572) || defined(MT6582) || defined(MT6592) || defined(MT6571) || defined(MT6595) || defined(MT6752)

//=============================================================================


/*
	Like callback function periodically
*/
void sidetone_hw_UpdateHwStVolume(uint32 param)
{
	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 1010, param);
#ifdef AP_AFE_SIDDETONE_GAIN
	if(gSidetoneInfo.isActive){
		
		uint16 dspValue, targetValue; 
        uint32 tempTargetValue;
		//read from DSP and update the volume 
		dspValue=0;
		dspValue=SAL_Sidetone_GetAdaptiveGain(); // Q1.15 Format
		
		// multiple, 
		// gSidetoneInfo.extStVolume is Q4.12 Format
		// HW is Q1.15 Format		
        tempTargetValue = (((uint32)(dspValue* gSidetoneInfo.extStVolume))>>12);
		if(tempTargetValue > 0x7FFF) {
			tempTargetValue = 0x7FFF;
		}
        targetValue = (uint16)(tempTargetValue);

		// write to ap hw 
		WriteApReg(AP_AFE_SIDDETONE_GAIN, targetValue);		

		// WriteApReg(AP_AFE_SIDDETONE_GAIN, 0x7FFF);		
		MonTrace(MON_CP_HWD_SPH_SIDETONE_VOLUME_TRACE_ID, 2, gSidetoneInfo.solutionVer, targetValue);
		/*
		if(!kal_if_hisr()){
			kal_trace(TRACE_GROUP_SP_DEBUG, SIDETONE_WRITE_TO_HW, targetValue);
		} else {
			kal_dev_trace(TRACE_GROUP_SP_DEBUG, SIDETONE_WRITE_TO_HW, targetValue);
		}
		*/

		// trigger the timer again.
		// kal_set_timer(gSidetoneInfo.timer, (kal_timer_func_ptr)(sidetone_hw_UpdateHwStVolume), NULL, 4, 0);
		ExeTimerReset(&(gSidetoneInfo.timer), sidetone_hw_UpdateHwStVolume, 2, 0);
		ExeTimerStart(&(gSidetoneInfo.timer));
	}

#else
	ASSERT(0); // register not define, it should to use hw sidetone
#endif
}


void sidetone_hw_UpdateStart(void)
{
	// kal_trace(TRACE_INFO, SIDETONE_START, SIDETONE_SOLUTION_VER_HW);
	MonTrace(MON_CP_HWD_SPH_SIDETONE_CONTROL_TRACE_ID, 2, SIDETONE_SOLUTION_VER_HW, true);

	gSidetoneInfo.isActive = true;

	// trigger the timer again.
	// kal_set_timer(gSidetoneInfo.timer, (kal_timer_func_ptr)(sidetone_hw_UpdateHwStVolume), NULL, 4, 0);
	ExeTimerReset(&(gSidetoneInfo.timer), sidetone_hw_UpdateHwStVolume, 2, 0);
	ExeTimerStart(&(gSidetoneInfo.timer));

}

void sidetone_hw_UpdateStop(void)
{
	// kal_trace(TRACE_INFO, SIDETONE_STOP);
	
	MonTrace(MON_CP_HWD_SPH_SIDETONE_CONTROL_TRACE_ID, 2, SIDETONE_SOLUTION_VER_HW, false);
	
	gSidetoneInfo.isActive = false;

	// kal_cancel_timer(gSidetoneInfo.timer);
	ExeTimerStop(&(gSidetoneInfo.timer));	
}

/**
	Be called from SPC message handler, so we do not leave any log here. 
	Please try to check A2M message and you can get the value

	@vol: volume calculated by AP side
*/
void sidetone_hw_SetExtStVolume(uint16 vol)
{	
	gSidetoneInfo.extStVolume = vol; 	
}


void sidetone_hw_TurnSwSidetoneOn(bool on)
{
	// do nothing when using hardware sidetone
	(void) on;
}


void sidetone_hw_Init(void)
{
	//gSidetoneInfo.extStVolume = 0x7fff;
	// init value sync with AP side int value
	gSidetoneInfo.extStVolume = 0;

	
	// gSidetoneInfo.gptTimer = DclSGPT_Open(DCL_GPT_CB, FLAGS_NONE);
	// gSidetoneInfo.timer = kal_create_timer("sidetone"); 
	ExeTimerCreate(&(gSidetoneInfo.timer), sidetone_hw_UpdateHwStVolume, 0, 0, 0);
	
}

// ============================================================================

void sidetone_dspSw_SetExtStVolume(uint16 vol);

void sidetone_dspSw_UpdateStart(void)
{ 
	// kal_trace(TRACE_INFO, SIDETONE_START, SIDETONE_SOLUTION_VER_DSP_SW);
	MonTrace(MON_CP_HWD_SPH_SIDETONE_CONTROL_TRACE_ID, 2, SIDETONE_SOLUTION_VER_DSP_SW, true);

	gSidetoneInfo.isActive = true;

	// update dsp's sidetone volume
	sidetone_dspSw_SetExtStVolume(gSidetoneInfo.extStVolume);

}

void sidetone_dspSw_UpdateStop(void)
{ 
	// kal_trace(TRACE_INFO, SIDETONE_STOP);
	MonTrace(MON_CP_HWD_SPH_SIDETONE_CONTROL_TRACE_ID, 2, SIDETONE_SOLUTION_VER_DSP_SW, false);
	
	gSidetoneInfo.isActive = false;
}

/**
	Be called from SPC message handler, so we do not leave any log here. 
	Please try to check A2M message and you can get the value

	@vol: volume calculated by AP side
*/
void sidetone_dspSw_SetExtStVolume(uint16 vol)
{ 
	gSidetoneInfo.extStVolume = vol;

	if(gSidetoneInfo.isActive){
		uint16 targetValue; // This is For DSP Q4.12 Format
		
		// gSidetoneInfo.extStVolume is Q4.12 Format for HW sidetone gain.
		// targetValue is SW sidetone gain, which is 12 db loader then hw sidetone gain
		// As the result, we need to left shift 2 bit to calucate correct sw sidetone gain

		targetValue = (gSidetoneInfo.extStVolume<<2);
		if(targetValue > 0x7FFF) {
			targetValue = 0x7FFF;
		}
		
		// write to DSP
		SAL_Sidetone_SetGain(targetValue);
		MonTrace(MON_CP_HWD_SPH_SIDETONE_VOLUME_TRACE_ID, 2, gSidetoneInfo.solutionVer, targetValue);
		/*
		if(!kal_if_hisr()){
			kal_trace(TRACE_GROUP_SP_DEBUG, SIDETONE_WRITE_TO_HW, targetValue);
		} else {
			kal_dev_trace(TRACE_GROUP_SP_DEBUG, SIDETONE_WRITE_TO_HW, targetValue);
		}
		*/
	}
}


/**
	Please use this function during DSP power on. (The function may access sherif)
	@on: ture for turning on, false for turing off.
*/
void sidetone_dspSw_TurnSwSidetoneOn(bool on)
{
	(void) on;
	SAL_Sidetone_Enable(on);
}

void sidetone_dspSw_Init(void)
{ 
	//gSidetoneInfo.extStVolume = 0x7fff;
	// init value sync with AP side int value
	gSidetoneInfo.extStVolume = 0;

}




//=============================================================================
// APIs 
//=============================================================================

/**
	@solutionVer: 0 for Hw 
*/
void SIDETONE_UpdateStart(void)
{
	// kal_trace(TRACE_INFO, SIDETONE_START, gSidetoneInfo.solutionVer);
	MonTrace(MON_CP_HWD_SPH_SIDETONE_CONTROL_TRACE_ID, 2, gSidetoneInfo.solutionVer, true);

	gSidetoneInfo.isActive = true;
	
	if(SIDETONE_SOLUTION_VER_HW == gSidetoneInfo.solutionVer) {
		// trigger the timer again.
		// kal_set_timer(gSidetoneInfo.timer, (kal_timer_func_ptr)(sidetone_hw_UpdateHwStVolume), NULL, 4, 0);		
		ExeTimerReset(&(gSidetoneInfo.timer), sidetone_hw_UpdateHwStVolume, 2, 0);
		ExeTimerStart(&(gSidetoneInfo.timer));
	} else {
		SIDETONE_SetExtStVolume(gSidetoneInfo.extStVolume);
	}

}

void SIDETONE_UpdateStop(void)
{
	// kal_trace(TRACE_INFO, SIDETONE_STOP, gSidetoneInfo.solutionVer);
	MonTrace(MON_CP_HWD_SPH_SIDETONE_CONTROL_TRACE_ID, 2, gSidetoneInfo.solutionVer, false);
		
	gSidetoneInfo.isActive = false;

	if(SIDETONE_SOLUTION_VER_HW == gSidetoneInfo.solutionVer) {
		// kal_cancel_timer(gSidetoneInfo.timer);
		ExeTimerStop(&(gSidetoneInfo.timer));
	}
}	

void SIDETONE_SetExtStVolume(uint16 vol) 
{
    uint16 dspValue = 32767, targetValue; 
    uint32 tempTargetValue;	

	if(SIDETONE_SOLUTION_VER_HW == gSidetoneInfo.solutionVer) {
		sidetone_hw_SetExtStVolume(vol);
        tempTargetValue = (((uint32)(dspValue* vol))>>12);
        if(tempTargetValue > 0x7FFF) {
            tempTargetValue = 0x7FFF;
        }
        targetValue = (uint16)(tempTargetValue);
        WriteApReg(AP_AFE_SIDDETONE_GAIN, targetValue);	
        
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 0x01EC, targetValue);
	} else if (SIDETONE_SOLUTION_VER_DSP_SW == gSidetoneInfo.solutionVer) {
		sidetone_dspSw_SetExtStVolume(vol);
	} else { // do nothing
	}	
	
}

/**
	Please use this function during DSP power on. (The function may access sherif)
	@on: ture for turning on, false for turing off.
*/
void SIDETONE_TurnSwSidetoneOn(bool on)
{
	if(SIDETONE_SOLUTION_VER_HW == gSidetoneInfo.solutionVer) {
		sidetone_hw_TurnSwSidetoneOn(on);
	} else if(SIDETONE_SOLUTION_VER_DSP_SW == gSidetoneInfo.solutionVer){
		sidetone_dspSw_TurnSwSidetoneOn(on);
	} else {
		// ASSERT(0); // do nothing, in case sidetone is not use actually. 
	}
}

void SIDETONE_SetSolutionVer(SIDETONE_SOLUTION_VER solutionVer)
{	
	ASSERT((SIDETONE_SOLUTION_VER_HW == solutionVer) || (SIDETONE_SOLUTION_VER_DSP_SW == solutionVer), 
		HWDSPH_ERR_SIDETONE_UNKNOW_SOLUTION, solutionVer);

	//record the solution
	gSidetoneInfo.solutionVer = solutionVer;
}

void SIDETONE_ResetSolutionVer(void)
{
	ASSERT((SIDETONE_SOLUTION_VER_HW == gSidetoneInfo.solutionVer) || (SIDETONE_SOLUTION_VER_DSP_SW == gSidetoneInfo.solutionVer), 
		HWDSPH_ERR_SIDETONE_UNKNOW_SOLUTION, gSidetoneInfo.solutionVer	);

	//record the solution
	gSidetoneInfo.solutionVer = SIDETONE_SOLUTION_VER_UNDEF;
}


void SIDETONE_Init(void)
{
	//gSidetoneInfo.extStVolume = 0x7fff;
	// init value sync with AP side int value
	gSidetoneInfo.extStVolume = 0;

	
	// gSidetoneInfo.gptTimer = DclSGPT_Open(DCL_GPT_CB, FLAGS_NONE);
	ExeTimerCreate(&(gSidetoneInfo.timer), sidetone_hw_UpdateHwStVolume, 0, 0, 0);
	// gSidetoneInfo.timer = kal_create_timer("sidetone"); 
	
}

