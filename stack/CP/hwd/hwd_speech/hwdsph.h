/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _HWD_SPH_H_
#define _HWD_SPH_H_


// #include "exeapi.h"
#include "sysdefs.h"

/*---------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Defines  Macros
----------------------------------------------------------------------------*/

#define true         (bool)(1==1)
#define false        (bool)(1==0)

#if 0 //defined(MT6752)
#define SPC_CHIP_BACK_PHONECALL_USED
#endif

#if 0 //defined(MT6752) //defined(MT6595)
#define SPH_CHIP_BACK_MODE_FIX_TO_NORMAL
#endif


#define NUM_COMMON_PARAS  12
#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
#define NUM_MODE_PARAS 48
#else
#define NUM_MODE_PARAS   16
#endif
#define TOTAL_NETWORK_NUMBER 5 //GSM WCDMA CDMA VOLTE VOIP c2k
#define NUM_VOL_PARAS     4

#define NUM_SPH_MODE      8
#define NUM_SPH_INPUT_FIR  6
#define NUM_SPH_OUTPUT_FIR 6

#define NUM_DMNR_PARAM (44)
#define NUM_WB_DMNR_PARAM (76)
#define NUM_FIR_COEFFS    45
#define NUM_WB_FIR_COEFFS    90
#define NUM_MAGI_CLARITY_PARAM (32)

	
#define SPH_MODE_NORMAL  0
#define SPH_MODE_EARPHONE  1
#define SPH_MODE_LOUDSPK  2
#define SPH_MODE_BT_EARPHONE 3
#define SPH_MODE_BT_CORDLESS 4
#define SPH_MODE_BT_CARKIT   5
#define SPH_MODE_AUX1  6
#define SPH_MODE_AUX2  7
#define SPH_MODE_HAC   8
#define SPH_MODE_USB   9
#define SPH_MODE_LINEIN_VIA_BT_CORDLESS 10
#define SPH_MODE_UNDEFINED  11

#define  L1SP_KEYTONE         0
#define  L1SP_TONE            1
#define  L1SP_SPEECH          2
#define  L1SP_SND_EFFECT      3
#define  L1SP_AUDIO           4
#define  L1SP_VOICE           5
#define  L1SP_DAI             6
#define  L1SP_LINEIN          7
#define  L1SP_MAX_AUDIO       8

//[REMIND] The following definition is related to L1SP_STATE_2G_SPEECH_ON, please use L1SP_GetState() to search the relationship
#define RAT_2G_MODE  0
#define RAT_3G_MODE  1 
#define RAT_3G324M_MODE 2
#define RAT_4G_MODE  3
#define RAT_C2K_MODE 4

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
typedef enum {
	  L1SP_L4C_C2K_NONE 	  	= 0x0000, 
  	L1SP_L4C_C2K_QCELP13K   	= 0x0001,
	  L1SP_L4C_C2K_EVRCA 	  	= 0x0002, 
	  L1SP_L4C_C2K_EVRCB	  	= 0x0003, 
	  L1SP_L4C_C2K_EVRCNW_WB	= 0x0004, 
	  L1SP_L4C_C2K_EVRCNW_NB	= 0x0005, 	  
} L1SP_L4C_Codec;

typedef enum {
	L1SP_L4C_ESPEECH_0,
	L1SP_L4C_ESPEECH_1,
	L1SP_L4C_START_GENERATE_SID,
	L1SP_L4C_STOP_GENERATE_SID,
} L1SP_L4C_Event;

typedef enum {
		L1SP_L4C_EVENT_NONE,
		L1SP_L4C_EVENT_CS,
		L1SP_L4C_EVENT_PS,	
} L1SP_L4C_Event_Mode;

typedef enum{
   L1SP_STATE_IDLE = 0, 
   L1SP_STATE_2G_SPEECH_ON,
   L1SP_STATE_3G_SPEECH_ON,
   L1SP_STATE_3G324M_SPEECH_ON,
   L1SP_STATE_3G_SPEECH_CLOSING,    
   L1SP_STATE_4G_SPEECH_ON,
   L1SP_STATE_4G_SPEECH_CLOSING,    
   L1SP_STATE_C2K_SPEECH_ON,
   L1SP_STATE_C2K_SPEECH_CLOSING,       
   //L1SP_STATE_3G_SPEECH_CLOSED 
}L1SP_STATE_T;


typedef enum { // saving inside l1sp.isUlMute, bit wise record
	SP_MIC_MUTE_POS_FROM_SPC = 0x1,	
	SP_MIC_MUTE_POS_FROM_MED = 0x2,

	SP_MIC_MUTE_POS_FROM_ALL = 0xFFFF,
	
}SP_MIC_MUTE_POS;

typedef enum{ //bit mask   
   L1SP_FORCEDUNMUTE_ALL = 0xFFFF,
   L1SP_FORCEDUNMUTE_PCMROUTER = 0x01,
   L1SP_FORCEDUNMUTE_PCMRECORD = 0x02,
   L1SP_FORCEDUNMUTE_ACOUSTICLOOPBACK = 0x04,
   L1SP_FORCEDUNMUTE_RAWPCMRECORD = 0x08,
}L1SP_FORCEDUNMUTE_BITMASK; 

typedef struct{
   int msg;
   int param;
}LISP_LINK2VAL_MsgT;

/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define SPH_CHIP_BACK_PHONECALL_USE
#endif



/*----------------------------------------------------------------------------
 Global API Function Prototypes
----------------------------------------------------------------------------*/
void L1SP_Init( void );

void L1SP_LoadCommonSpeechPara( uint16 c_para[NUM_COMMON_PARAS] );
void L1SP_LoadSpeechPara( void );
void L1SP_SetState(uint8 state);


uint8 L1SP_GetState( void );

void SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_BITMASK mask, bool b);

uint16 *Sp_GetCommonSpeechPara(void);
uint16 *Sp_GetSpeechPara(void);
uint16 *Sp_GetWbSpeechPara(void);
void SP_SetSpeechPara( uint16 sph_m_para[NUM_MODE_PARAS] );
void SP_SetWbSpeechPara( uint16 m_para[NUM_MODE_PARAS] );

bool sp_getIsBtOn(void);
void sp_setBtOn(bool on);


void SP_L4C_SetEvent(L1SP_L4C_Event event, L1SP_L4C_Event_Mode mode);
void L1SP_Speech_On( uint8 RAT_Mode );
void L1SP_Speech_Off( void );

void l1sp_digiOnly_SetOutputVolume(int16 digitalGain);
void l1sp_digiOnly_SetEnhRefOutputVolume(int16 digitalRefGain);
void l1sp_digiOnly_SetMicrophoneVolume(int16 digitalGain);

void SP_MuteUlFromDiffPos(bool mute, SP_MIC_MUTE_POS pos);
void L1SP_MuteSpeaker( bool mute );
void SP_MuteUlEnhResult(bool mute);
void SP_MuteUlSource(bool mute);

bool L1SP_TCH_State( void );
// void l1sp_NetworkStatusNotification(void);
void l1sp_CodecStatusNotification(uint16 data);
int  L1SP_GetC2KSO_Codec(void);

void L1SP_C2K_Est(int codec);
void L1SP_C2K_DeEst( void );
void L1SP_Reload_SPE_Para( void );
void L1SP_Reload_MagiClarity_Para( void );
void L1SP_C2K_IntraRAT(int codec);

bool SP_IsMicMute(void);
uint8 L1SP_GetSpeechMode( void );
void L1SP_SpeechLoopBackEnable(bool fgEnable);

void l1sp_send_codec_status_notify(uint16 data);//to L4C
uint16 L1SP_GetAudID(void);
void L1SP_FreeAudID(uint16 aud_id);
void L1SP_MagiClarityData(const short MAGI_CLARITY_PAR[NUM_MAGI_CLARITY_PARAM]);

void SP_SetForcedUnMuteULController(L1SP_FORCEDUNMUTE_BITMASK mask, bool b);
void SP_SetForcedUnMuteDLController(L1SP_FORCEDUNMUTE_BITMASK mask, bool b);
#endif //  _HWD_SPH_H_

/*****************************************************************************
 END OF FILE
*****************************************************************************/

