/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2014
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *  hwdbtsco_drv.h
 *
 * Project:
 * --------
 *  The sixth
 *
 * Description:
 * ------------
 *  BT SCO driver header file
 *
 * Author:
 * -------
 *  Lanus Chao (mtk03453)
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision: $
 * $Modtime:  $
 * $Log:      $
 *
 * 01 13 2015 lanus.chao
 * [SIXTH00001470] [C2K] BT CVSD/mSBC check in
 * BT enable
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

#ifndef __BT_SCO_DRV_H
#define __BT_SCO_DRV_H

#include "hwdcommon_def.h"
#include "hwdmedia.h"
#include "hwdaudioservice.h"
#include "reg_base.h"

extern void CVSD_DownSample_Process(void *pHandle, short *pInSample, short *pOutSample, short *pTempBuffer, int iSourceSamples);
extern int CVSD_DownSample_GetMemory(void);
extern void *CVSD_DownSample_Init(char *pBuffer);
extern void CVSD_UpSample_Process(void *pHandle, short *pInSample, short *pOutSample, short *pTempBuffer, int iSourceSamples);
extern int CVSD_UpSample_GetMemory(void);
extern void *CVSD_UpSample_Init(char *pBuffer);

void BT_SCO_Loopback_OFF(void);
void BT_SCO_Loopback_ON(kal_bool fDisableCodec, kal_bool fWideBand);
kal_bool BT_SCO_IS_SPEECH_ON(void);
void BT_SCO_SPEECH_OFF(void);
void BT_SCO_SPEECH_ON(kal_bool fWideBand);
void BT_SCO_Init(void);

#if defined(__CVSD_CODEC_SUPPORT__) 
#define __MSBC_CODEC_SUPPORT__ 1

// avoid build error
#ifndef HWD_INT_BLUETOOTH
#define HWD_INT_BLUETOOTH  (0x1<<8)
#endif 

// BT HW Register address
//#define BTSRAM_base              (0xA8080000) //defined in mcu/driver/sys_drv/regbase/inc/reg_base_mtxxxx.h
//#define BTPKT_base               (0xA8000000) //defined in mcu/driver/sys_drv/regbase/inc/reg_base_mtxxxx.h
#define BT_SCO_HW_REG_PACKET_R     ((volatile kal_uint32*)(BTPKT_base+0x0FD0))
#define BT_SCO_HW_REG_PACKET_W     ((volatile kal_uint32*)(BTPKT_base+0x0FD4))
#define BT_SCO_HW_REG_CONTROL      ((volatile kal_uint32*)(BTPKT_base+0x0FD8))

#endif

#if defined(MT6755) 
#define BT_ECHO_REF_ENABLED
#endif 

#ifndef NULL
#define NULL 0
#endif

#define UPPER_BOUND(in,up)      ((in) > (up) ? (up) : (in))
#define LOWER_BOUND(in,lo)      ((in) < (lo) ? (lo) : (in))
#define BOUNDED(in,up,lo)       ((in) > (up) ? (up) : (in) < (lo) ? (lo) : (in))
#define MAXIMUM(a,b)            ((a) > (b) ? (a) : (b))
#define MINIMUM(a,b)            ((a) < (b) ? (a) : (b))
#define FOUR_BYTE_ALIGNED(size) (((size) + 3) & ~0x3)


#define MSBC_BTSTREAM_FRAME_BYTE 57
#define MSBC_PCM_FRAME_BYTE 240 //120 sample

#define BT_SCO_PACKET_120 120
#define BT_SCO_PACKET_180 180

#define BT_CVSD_TX_NREADY    (0x1U<<21)
#define BT_CVSD_RX_READY     (0x1U<<22)
#define BT_CVSD_TX_UNDERFLOW (0x1U<<23)
#define BT_CVSD_RX_OVERFLOW  (0x1U<<24)
#define BT_CVSD_INTERRUPT    (0x1U<<31)

#define BT_CVSD_CLEAR        (BT_CVSD_TX_NREADY | BT_CVSD_RX_READY | BT_CVSD_TX_UNDERFLOW | BT_CVSD_RX_OVERFLOW | BT_CVSD_INTERRUPT)

//TX 
#define SCO_TX_ENCODE_SIZE           (60                             ) // 60 byte (60*8 samples)
#define SCO_TX_PACKER_BUF_NUM        (8                              ) 
#define SCO_TX_PACKET_MASK           (0x7                            )
#define SCO_TX_PCM64K_BUF_SIZE       (SCO_TX_ENCODE_SIZE*2*8         ) // 60 * 2 * 8 byte
#define SCO_TX_RINGBUFFER_SIZE       (320*9                          ) // = 2880, should by multple of 320 and 360 and 240

//RX
#define SCO_RX_PLC_SIZE              (30                    ) 
#define SCO_RX_PACKER_BUF_NUM        (16                    ) 
#define SCO_RX_PACKET_MASK           (0xF                   )
#define SCO_RX_PCM64K_BUF_SIZE       (SCO_RX_PLC_SIZE*2*8   ) 
#define SCO_RX_PCM8K_BUF_SIZE        (SCO_RX_PLC_SIZE*2     ) 
#define SCO_RX_RINGBUFFER_SIZE       (320*9                 ) // = 2880, should by multple of 320 and 360 and 240

#define SCORX_BT_CVSD_RX_PACKET_BUFFER_LEN (BT_CVSD_BLOCKSIZE * 6) // 360 byte

#define NB_SPEECH_FRAME_SIZE 320 //BYTE
#define WB_SPEECH_FRAME_SIZE 640 //BYTE
#if defined(BT_ECHO_REF_ENABLED)
#define BT_ECHO_REF_DACA_DL_SMPL_CNT        320                                     // 20 ms x 16kHz = 320 samples
#define BT_ECHO_REF_DACA_DL_BUF_SIZE        (BT_ECHO_REF_DACA_DL_SMPL_CNT * 2)      // 320 samples x 2 = 640 bytes
#define BT_ECHO_REF_DACA_UL_SMPL_CNT        320                                     // 20 ms x 16kHz = 320 samples
#define BT_ECHO_REF_DACA_UL_BUF_SIZE        (BT_ECHO_REF_DACA_UL_SMPL_CNT * 2)      // 320 samples x 2 = 640 bytes
#define BT_ECHO_REF_BUF_DEPTH_IN_MS         256                                     // 256 ms
#define BT_ECHO_REF_BUF_SIZE_IN_HALF_WORD   (BT_ECHO_REF_BUF_DEPTH_IN_MS * 16)      // 256 ms x 16kHz = 4096 samples
#define BT_ECHO_REF_BUF_SIZE_IN_BYTE        (BT_ECHO_REF_BUF_SIZE_IN_HALF_WORD * 2) // 4096 samples x 2 = 8192 bytes
#define BT_ECHO_REF_LATENCY_IN_MS           220                                     // 200 ms
#define BT_ECHO_REF_LATENCY_IN_SMPL_CNT     (BT_ECHO_REF_LATENCY_IN_MS * 16)        // 200 ms x 16kHz = 3200 samples
#define BT_ECHO_REF_LATENCY_IN_BYTE         (BT_ECHO_REF_LATENCY_IN_SMPL_CNT * 2)   // 3200 samples x 2 = 6400 bytes
#define BT_ECHO_REF_TOTAL_BUF_SIZE          (BT_ECHO_REF_DACA_DL_BUF_SIZE + BT_ECHO_REF_DACA_UL_BUF_SIZE + BT_ECHO_REF_BUF_SIZE_IN_BYTE)
#else   // defined(BT_ECHO_REF_ENABLED)
#define BT_ECHO_REF_TOTAL_BUF_SIZE          0
#endif  // defined(BT_ECHO_REF_ENABLED)

typedef enum {
  BT_SCO_STATE_IDLE=0,
  BT_SCO_STATE_RUNNING,
  BT_SCO_STATE_ENDING
} BT_SCO_STATE;

typedef enum {
  BT_SCO_MODE_SPEECH,
  BT_SCO_MODE_LOOPBACK_WITH_CODEC,
  BT_SCO_MODE_LOOPBACK_WITHOUT_CODEC
} BT_SCO_MODE;

typedef enum {
  BT_SCO_MOD_CVSD_ENCODE,
  BT_SCO_MOD_CVSD_DECODE,
  BT_SCO_MOD_PLC_NB,
  BT_SCO_MOD_CVSD_TX_SRC,
  BT_SCO_MOD_CVSD_RX_SRC,
  BT_SCO_MOD_PCM_RINGBUF_TX,
  BT_SCO_MOD_PCM_RINGBUF_RX,
  BT_SCO_MOD_MSBC_DECODE,
  BT_SCO_MOD_MSBC_ENCODE,
  BT_SCO_MOD_PLC_WB
} BT_SCO_MODULE;

typedef enum {
  BT_SCO_DIRECT_BT2ARM,
  BT_SCO_DIRECT_ARM2BT
} BT_SCO_DIRECT;

typedef enum {
  BT_SCO_CVSD_30 =0,
  BT_SCO_CVSD_60 =1,
  BT_SCO_CVSD_90 =2,
  BT_SCO_CVSD_120=3,
  BT_SCO_CVSD_10 =4,
  BT_SCO_CVSD_20 =5,
  BT_SCO_CVSD_MAX=6
} BT_SCO_PACKET_LEN;

#if defined(BT_ECHO_REF_ENABLED)
typedef struct {
    kal_uint32 write_pointer;
    kal_uint32 read_pointer;
    kal_uint32 buffer_byte_count;
    kal_uint8 *buffer_base_pointer;
} ring_buffer_information_t;
#endif  // defined(BT_ECHO_REF_ENABLED)


typedef struct {
   //handle
   void          *pSRCHandle;
   void          *pEncHandle;
   AUD_RB_INFO   RingBuffer;

   //bitstream buffer
   kal_uint8     PacketBuf[SCO_TX_PACKER_BUF_NUM][SCO_TX_ENCODE_SIZE];
   kal_uint32    iPacket_w;        
   kal_uint32    iPacket_r; 
   kal_uint8     PcmBuf_64k[SCO_TX_PCM64K_BUF_SIZE];
   kal_uint8     PcmBuf_Temp[SCO_TX_PCM64K_BUF_SIZE]; 
   kal_uint32    uPcmBuf_w;
   kal_bool      fUnderflow;
} BT_SCO_TX;

typedef struct {
   //handle
   void          *pDecHandle;
   void          *pPLCHandle;
   void          *pSRCHandle;

   //bitstream buffer
   kal_uint8     PacketBuf[SCO_RX_PACKER_BUF_NUM][SCO_RX_PLC_SIZE];
   kal_bool      PacketValid[SCO_RX_PACKER_BUF_NUM];
   kal_uint32    iPacket_w;        
   kal_uint32    iPacket_r;       

   //temp buffer
   kal_uint8     PcmBuf_64k[SCO_RX_PCM64K_BUF_SIZE];
   kal_uint8     PcmBuf_Temp[SCO_RX_PCM64K_BUF_SIZE];
   kal_uint8     PcmBuf_8k[SCO_RX_PCM8K_BUF_SIZE];
   kal_uint32    uPcmBuf_r; //for PcmBuf_8k
   AUD_RB_INFO   RingBuffer;
   kal_bool      fOverflow;
#if defined(BT_ECHO_REF_ENABLED)
    // Echo reference
    kal_uint16 echo_ref_buf             [BT_ECHO_REF_BUF_SIZE_IN_HALF_WORD];
    kal_uint16 echo_ref_daca_dl_tmp_buf [BT_ECHO_REF_DACA_DL_SMPL_CNT];
    kal_uint16 echo_ref_daca_ul_tmp_buf [BT_ECHO_REF_DACA_UL_SMPL_CNT];
    kal_uint32 echo_ref_byte_cnt_down;
    ring_buffer_information_t echo_ref_ring;
#endif  // defined(BT_ECHO_REF_ENABLED)

} BT_SCO_RX;

#if defined(BT_ECHO_REF_ENABLED)
kal_uint32 ring_buffer_get_data_byte_count (ring_buffer_information_t *p_info);
kal_uint32 ring_buffer_get_space_byte_count (ring_buffer_information_t *p_info);
void ring_buffer_get_write_information (ring_buffer_information_t *p_info, kal_uint8 **pp_buffer, kal_uint32 *p_byte_count);
void ring_buffer_get_read_information (ring_buffer_information_t *p_info, kal_uint8 **pp_buffer, kal_uint32 *p_byte_count);
void ring_buffer_write_done (ring_buffer_information_t *p_info, kal_uint32 write_byte_count);
void ring_buffer_read_done (ring_buffer_information_t *p_info, kal_uint32 read_byte_count);
#endif  // defined(BT_ECHO_REF_ENABLED)

#endif //__BT_SCO_DRV_H

