/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "valapi.h"
#include "monids.h"
#include "hwdam.h"
#include "hwdlinkapi.h"
#include "hwdspherr.h"
#include "hwdsal_exp.h"

#if defined(__CVSD_CODEC_SUPPORT__) 
#include "hwdbtsco_drv.h"
#endif

#if 0
#include "kal_public_api.h"
#include "kal_general_types.h"
#include "kal_trace.h"
#include "string.h"
#include "reg_base.h"
#include "custom_equipment.h" /* custom_cfg_audio_ec_range() */
#include "device.h" /* MAX_VOL_LEVEL,  in ps\l4\include */
#include "audcoeff_default.h"

#include "audio_def.h"
#include "l1audio.h"
#include "l1audio_trace.h"
#include "l1sp_trc.h"
#include "l1audio_sph_trc.h"

#include "media.h"
#include "sp_enhance.h"
#include "afe.h"
#include "am.h"
#include "vm.h"
#include "sal_def.h"
#include "sal_exp.h"

#include "mml1_rf_global.h"
#include "sp_drv.h"

#include "l4_ps_api.h" 

#if defined(__DATA_CARD_SPEECH__) || defined(__BT_SCO_CODEC_SUPPORT__)
#include "sp_daca.h"
#endif

#ifdef __TWOMICNR_SUPPORT__
#include "two_mic_NR_custom_if.h"
#endif

#if !defined(__SMART_PHONE_MODEM__)
#include "apAudSysConfig.h"
#else
#include "sidetone.h"
#endif

#if defined(__VOLTE_SUPPORT__) 
#include "l1audio.h"
#endif

#define __BT_SUPPORT__

// for modem project, DONGLE product use
#if defined(MT6280)
//	#define DACA_SPE_ENABLE_INTERNAL
#endif

#include "audio_msgid.h"
#endif

// TODO: c2k 
// kal_trace ==> add trace 
// [sp_mutex] 
// [setFlag]
// [VoC]
// [add SAL]
// ~done [enhancement Init]

#include "sysdefs.h"
#include "assert.h"
#include "hwdsph.h"
#include "hwdsp_enhance.h" 
#include "hwdcommon_def.h"
#include "hwdafe_def.h"
#include "hwdafe.h"
#include "hwdam.h"
#include "hwdaudioservice.h"

#if defined(MTK_PLT_MODEM_ONLY)
#include "hwdApAudSysConfig.h"
#endif

#include "hwdsal_def.h"
#include "valpswapi.h"
#include "valspc_drv.h"

#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
#include "iopccism.h"
#endif

//#define ASSERT(expr) assert(expr)

// TODO: c2k 
// done [ApAudSys_config]
// [trace]
// [SAL]
// [am]
extern _SP_ENC SP_ENC; 

/* ------------------------------------------------------------------------------ */
/*  Speech Interface                                                              */
/* ------------------------------------------------------------------------------ */

static struct {
   uint16   aud_id;
// #if defined(__SMART_PHONE_MODEM__)   
//    uint16   aud_id_network_status;
// #endif   

	 uint16   aud_id_codec_status;
   uint16   sph_c_para[NUM_COMMON_PARAS];
   uint16   sph_m_para[NUM_MODE_PARAS];
   uint16   sph_v_para[NUM_VOL_PARAS]; // useless, should fine time to remove it  
	uint16   sph_m_paraWb[NUM_MODE_PARAS];

   uint8    mic_volume; //  [analog+digital gain]value corrently set to DSP&HW; when ANALOG_AFE_PATH_EXIST is NOT defined, this is 0
 
   uint16   isULMute; // flag to keep the DSP UL MUTE states
   bool     isDLMute; // flag to keep the DSP DL MUTE states
   
   uint32   forcedUnmuteDLController;
   uint32   forcedUnmuteULController;

   bool     isUlEnhResultMute;
	bool     isUlSourceMute;
   bool     codec_mute; 
//	TODO: c2k
   L1SP_L4C_Event_Mode   codec_mute_mode;
   bool     tch_state; // true: TCH on; false: TCH off
   uint8    state; // recording the network is 2G or 3G      
   bool     bt_on; // include DSP BT path setting and AFE path switch

// TODO: c2k
// #if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )
   bool     interRAT;
// #endif

#if defined(__DATA_CARD_SPEECH__)
   void (*strmOnHandler)(void *); 
   void (*strmOffHandler)(void *);
   void (*strmHdl)(uint32 event, void *param);
#endif      
   
#if defined(__ECALL_SUPPORT__)
   void (*pcm4wayOnHandler)(void *); 
   void (*pcm4wayOffHandler)(void *);
#endif
	int              c2k_so_codec;
	bool             isSO_Connected;
	bool             isConSSO_Done;
} l1sp;

void L1SP_C2K_IntraRAT(int codec){
   LISP_LINK2VAL_MsgT *msgptr;
   
   MonTrace(MON_CP_HWD_SPH_C2K_INTRARAT_TRACE_ID, 1, 0);
   if(SP_ENC.Is_Voice_Encryption)
   {
    	ResetEncInfo();
   }
   msgptr = (LISP_LINK2VAL_MsgT*)ExeMsgBufferGet(sizeof(LISP_LINK2VAL_MsgT));
   msgptr->msg   = 66;
   msgptr->param = codec;  
   ExeMsgSend( EXE_VAL_ID, VAL_CUST_MAILBOX, VAL_LINK2SDRV_MSG, 
                 (void *)msgptr, sizeof(LISP_LINK2VAL_MsgT) );
   MonTrace(MON_CP_HWD_SPH_C2K_INTRARAT_TRACE_ID, 1, 1);	
}

uint16 L1SP_GetAudID(void)/*Be careful.Before Locking SleepMode, to access DSP sherrif tasks much time. So access DSP must be after SetFlag*/
{
   uint16 aud_id = 0;
	// TODO: [setFlag]
   aud_id = L1Audio_GetAudioID();
   L1Audio_SetFlag( aud_id ); 
   // end TODO: [setFlag]
   return aud_id;
}
void L1SP_FreeAudID(uint16 aud_id)
{
	// TODO: [setFlag]
   L1Audio_ClearFlag( aud_id );
   L1Audio_FreeAudioID( aud_id );
   //end TODO: [setFlag]
}
void SO_IntraRAT( int codec )
{
	 int16 aud_id;
   MonTrace(MON_CP_HWD_SPH_SO_INTRARAT_TRACE_ID, 1, 0);    
   aud_id = L1SP_GetAudID();   
   AM_C2K_IntraRAT(codec);
   L1SP_FreeAudID(aud_id);
   MonTrace(MON_CP_HWD_SPH_SO_INTRARAT_TRACE_ID, 1, 1);                      
}

void L1SP_C2K_Est(int codec){   

   LISP_LINK2VAL_MsgT *msgptr;
   
   MonTrace(MON_CP_HWD_SPH_C2K_EST_TRACE_ID, 1, 0);
   if(SP_ENC.Is_Voice_Encryption)
   {
    	ResetEncInfo();
   }
   msgptr = (LISP_LINK2VAL_MsgT*)ExeMsgBufferGet(sizeof(LISP_LINK2VAL_MsgT));
   msgptr->msg   = 22;
   msgptr->param = codec;  
   ExeMsgSend( EXE_VAL_ID, VAL_CUST_MAILBOX, VAL_LINK2SDRV_MSG, 
                 (void *)msgptr, sizeof(LISP_LINK2VAL_MsgT) );
   MonTrace(MON_CP_HWD_SPH_C2K_EST_TRACE_ID, 1, 1);
}

void L1SP_C2K_DeEst( void ){   
   LISP_LINK2VAL_MsgT *msgptr;
  
   MonTrace(MON_CP_HWD_SPH_C2K_DEEST_TRACE_ID, 1, 0);   
   msgptr = (LISP_LINK2VAL_MsgT*)ExeMsgBufferGet(sizeof(LISP_LINK2VAL_MsgT));
   msgptr->msg   = 44;
   msgptr->param = 0;   
   ExeMsgSend( EXE_VAL_ID, VAL_CUST_MAILBOX, VAL_LINK2SDRV_MSG, 
                 (void *)msgptr, sizeof(LISP_LINK2VAL_MsgT) );
   MonTrace(MON_CP_HWD_SPH_C2K_DEEST_TRACE_ID, 1, 1);   
}

void sub_SPC2K_DisconSSO_Done(void)
{
	  uint16 aud_id;
	 MonTrace(MON_CP_HWD_SPH_SO_DISCON_DONE_TRACE_ID, 1, 0); 
	 aud_id = L1SP_GetAudID();
   l1sp.isConSSO_Done = false;//clear
   SPC2K_DisconSSO_Done();
   L1SP_FreeAudID(aud_id);
   MonTrace(MON_CP_HWD_SPH_SO_DISCON_DONE_TRACE_ID, 1, 1);  
}

void SO_Disconnect( void )
{
   uint16 aud_id;
   MonTrace(MON_CP_HWD_SPH_SO_DISCONNECT_TRACE_ID, 1, 0);  
   aud_id = L1SP_GetAudID();                 
   ASSERT(l1sp.isSO_Connected, HWDSPH_ERR_FORCE_ASSERT, 0);
   SP_L4C_SetEvent(L1SP_L4C_ESPEECH_0, L1SP_L4C_EVENT_CS);
   //SAL_C2K_SetValue(SAL_C2K_DL_RATE, IPC_SPCH_ERASURE);
   l1sp.c2k_so_codec   = SAL_C2K_COD_MODE_UNDEF;
   l1sp.isSO_Connected = false;
//#if !defined(__WAIT_SP1__)     
   sub_SPC2K_DisconSSO_Done();
//#endif   
   L1SP_FreeAudID(aud_id);
   MonTrace(MON_CP_HWD_SPH_SO_DISCONNECT_TRACE_ID, 1, 1);                        
}

int L1SP_GetC2KSO_Codec( void )
{
   return l1sp.c2k_so_codec;
}

void sub_SPC2K_ConSSO_Done(void)
{
   if(!l1sp.isConSSO_Done){
   	  MonTrace(MON_CP_HWD_SPH_SKIP_SO_CON_DONE_TRACE_ID, 1, 0); 
      l1sp.isConSSO_Done = true;
      SPC2K_ConSSO_Done();
   }else{//skip
   	  MonTrace(MON_CP_HWD_SPH_SKIP_SO_CON_DONE_TRACE_ID, 1, 1); 
  	  //log
   }
}

void SO_Connect(int codec)//ESpeech:1 is presumed before this is invoked.
{
   MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 0); 
   MonTrace(MON_CP_HWD_SPH_SO_CODEC_TRACE_ID, 1, codec);
   
   ASSERT(!l1sp.isSO_Connected, HWDSPH_ERR_FORCE_ASSERT, 0);

   l1sp.c2k_so_codec = codec;

   if(AM_IsSpeechOn()){ //speech On(2G) -> SO connect or  during a call, SO change
       MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 1); 

      if( L1SP_GetState() == L1SP_STATE_2G_SPEECH_ON ){//prepare InterRAT HO                           
         MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 2);
         AM_InterRAT_2G_to_C2K(codec);
         L1SP_SetState( L1SP_STATE_C2K_SPEECH_ON );
      }else if( L1SP_GetState() == L1SP_STATE_C2K_SPEECH_ON ){
         MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 3);
         AM_C2K_IntraRAT(codec);
         L1SP_SetState( L1SP_STATE_C2K_SPEECH_ON );
      }else{//others
         MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 4);
         ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
      }     
      
      sub_SPC2K_ConSSO_Done();

      MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 5); 
 
   }else{
      MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 6);                  
      //don't send ack until SpeechOn 
   }
   l1sp.isSO_Connected = true;
   SP_L4C_SetEvent(L1SP_L4C_ESPEECH_1, L1SP_L4C_EVENT_CS); 
      
   MonTrace(MON_CP_HWD_SPH_SO_CONNECT_TRACE_ID, 1, 7);   
}

bool L1SP_isC2KSO_Connected()
{
   return l1sp.isSO_Connected;
}

// TODO: c2k [sp_mutex] kal_enhmutexid sp_mutex;

//	TODO: c2k
// #if defined(__BT_SUPPORT__)
bool L1SP_IsBluetoothOn( void )
{
	uint8 mode = SPE_GetSpeechMode();
   return (mode==SPH_MODE_BT_CORDLESS || mode==SPH_MODE_BT_EARPHONE || 
      mode==SPH_MODE_BT_CARKIT || mode==SPH_MODE_LINEIN_VIA_BT_CORDLESS);
}
// #endif

void SP_SetSpeechPara( uint16 sph_m_para[NUM_MODE_PARAS] )

{
   SysMemcpy(l1sp.sph_m_para, sph_m_para, NUM_MODE_PARAS*sizeof(uint16));
}

//	TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))

// #endif

/*******************************************************************/
/*   The new interface for DSP speech enhancement function / Bluetooth */
/*******************************************************************/
void sp_setBtOn(bool on)
{
	l1sp.bt_on = on;
}

bool sp_getIsBtOn(void)
{
	return l1sp.bt_on;
}

void L1SP_Reload_SPE_Para( void )
{
   SPE_LoadSpeechPara(l1sp.sph_c_para, l1sp.sph_m_para, NULL, l1sp.sph_m_paraWb); // l1sp.sph_v_para no one use it.
}


#if 0 // defined(__VOLTE_SUPPORT__)
void ltecsr_set_jbm_para(uint16 c_para[NUM_COMMON_PARAS]);
#endif

void L1SP_LoadCommonSpeechPara( uint16 c_para[NUM_COMMON_PARAS] )
{
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   uint16 aud_id = L1SP_GetAudID();
// #endif    
#if 0 // defined(__VOLTE_SUPPORT__)
   ltecsr_set_jbm_para(c_para);  //temporary solution for Field Trial
#endif
   SysMemcpy(l1sp.sph_c_para, c_para, NUM_COMMON_PARAS*sizeof(uint16));
#if !defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
   SetSpeechEnhancement( true );
#endif 
   
   // Some common parameters are used in non-speech function
   // Force to load common parameter
   SPE_LoadSpeechPara(l1sp.sph_c_para, NULL, NULL, NULL);
	
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
// #endif
}

uint16 *Sp_GetCommonSpeechPara(void)
{
	return l1sp.sph_c_para;
}



uint16 *Sp_GetSpeechPara(void)
{
	return l1sp.sph_m_para;
}


void SP_SetWbSpeechPara( uint16 m_para[NUM_MODE_PARAS] )
{
   SysMemcpy( l1sp.sph_m_paraWb, m_para, NUM_MODE_PARAS*sizeof(uint16));
}

uint16 *Sp_GetWbSpeechPara(void)
{
	return l1sp.sph_m_paraWb;
}


uint8 L1SP_GetSpeechMode( void )
{
    return SPE_GetSpeechMode();
}


#if 0
/**
	@index: [Input] Identify which LID is going to provide
	@buffer: [Output] Buffer with the contain the result
	@len: [Input] Length of the output buffer
*/
void L1SP_GetNvramInfoByIndex(L1SP_NVRAM_INFO_INDEX index, void *buffer, uint16 len)
{
	switch(index)
	{

	case L1SP_NVRAM_INFO_INDEX_WB_SPEECH_INPUT_FIR:
	{
		ASSERT(len == (NUM_SPH_INPUT_FIR * NUM_WB_FIR_COEFFS), HWDSPH_ERR_FORCE_ASSERT, 0);
		memcpy(buffer, SPE_getAllWbSpeechFirCoeff_InputOnly(), sizeof(kal_uint16) * NUM_SPH_INPUT_FIR * NUM_WB_FIR_COEFFS);
	}
		break;
	case L1SP_NVRAM_INFO_INDEX_WB_SPEECH_OUTPUT_FIR:
	{
		ASSERT(len == (NUM_SPH_OUTPUT_FIR * NUM_WB_FIR_COEFFS), HWDSPH_ERR_FORCE_ASSERT, 0);
		memcpy(buffer, SPE_getAllWbSpeechFirCoeff_OutputOnly(), sizeof(kal_uint16) * NUM_SPH_OUTPUT_FIR * NUM_WB_FIR_COEFFS);
	}
		break;
	case L1SP_NVRAM_INFO_INDEX_WB_SPEECH_MODE_PARAM:
	{
		ASSERT(len == (NUM_SPH_MODE * NUM_MODE_PARAS), HWDSPH_ERR_FORCE_ASSERT, 0);
		memcpy(buffer, SPE_getAllWbSpeechModePara(),  sizeof(kal_uint16) * NUM_SPH_MODE * NUM_MODE_PARAS);
	}
		break;
		
	default:
		ASSERT(0); 
	}
}

/**
	MED can put all nvram structure to AUD via this function. Audio/Speech driver will parsing the LID by itself. 
	
	@index: [Input] Identify which LID is used to parsing the buffer
	@buffer: [Input] Buffer with the contain from nvram
	@len: [Input] Length of the input buffer
*/
void L1SP_SetNvramInfoByIndex(L1SP_NVRAM_INFO_INDEX index, uint8* buffer, uint16 len)
{
	switch(index)
	{
	case L1SP_NVRAM_INFO_INDEX_UNDEF:		

		break;
	case L1SP_NVRAM_INFO_INDEX_PARAM:
	{
		int16 bufCur=0;

		//NB input FIR
		l1sp_setAllSpeechFirCoeff_InputOnly((int16 *)(buffer + bufCur), 6 * NUM_FIR_COEFFS);
        bufCur += (sizeof(kal_uint16)* 6 * NUM_FIR_COEFFS);

		//NB output FIR
		l1sp_setAllSpeechFirCoeff_OutputOnly((int16 *)(buffer + bufCur), 6 * NUM_FIR_COEFFS);
        bufCur += (sizeof(kal_uint16)* 6 * NUM_FIR_COEFFS);

		//selected FIR
		// kal_mem_cpy(&(l1sp.sph_selectedOutFirIndex), (buffer + bufCur), sizeof(kal_uint16));
		bufCur += sizeof(kal_uint16);
		
		//common
		{
		uint16 *data = (uint16 *)(buffer + bufCur);
		L1SP_LoadCommonSpeechPara(data); //common paramter will copy into l1sp.sph_c_para in this function
		bufCur += sizeof(kal_uint16) * 12; //hardcoding size
		}

		//mode parameter
		l1sp_setAllSpeechModePara((uint16 *)(buffer + bufCur), ( NUM_SPH_MODE * 16));
		bufCur += (sizeof(kal_uint16)* NUM_SPH_MODE * 16);

		// kal_mem_cpy(l1sp.sph_allVolumePara, (buffer + bufCur), (sizeof(kal_uint16)*3*7*4));
		// l1sp.setNvramValuesFlag |= L1SP_NVRAM_VALUES_FLAG_VOLUME_PARAM;
		
		/*		
	    	kal_uint16 Media_Playback_Maximum_Swing;
    		kal_int16 Melody_FIR_Coeff_Tbl[25];
	    	kal_int16 audio_compensation_coeff[3][45];
		*/
	}
		break;
		
// #if WB_SPE_SUPPORT
	case L1SP_NVRAM_INFO_INDEX_WB_SPEECH_INPUT_FIR:
	{
		// memcpy(l1sp.sph_allWbInFirCoeff, buffer, sizeof(kal_uint16) * NUM_SPH_INPUT_FIR * NUM_WB_FIR_COEFFS);		
		//l1sp.setNvramValuesFlag |= L1SP_NVRAM_VALUES_FLAG_WB_IN_FIR; // (1<<4) bit [4]
		l1sp_setAllWbSpeechFirCoeff_InputOnly((kal_int16 *)buffer, NUM_SPH_INPUT_FIR * NUM_WB_FIR_COEFFS);
	}
		break;
	case L1SP_NVRAM_INFO_INDEX_WB_SPEECH_OUTPUT_FIR:
	{
		// memcpy(l1sp.sph_allWbOutFirCoeff, buffer, sizeof(kal_uint16) * NUM_SPH_OUTPUT_FIR * NUM_WB_FIR_COEFFS);
		// l1sp.setNvramValuesFlag |= L1SP_NVRAM_VALUES_FLAG_WB_OUT_FIR; // (1<<5) bit [5]
		l1sp_setAllWbSpeechFirCoeff_OutputOnly((kal_int16 *)buffer, NUM_SPH_OUTPUT_FIR * NUM_WB_FIR_COEFFS);
	}
		break;
	case L1SP_NVRAM_INFO_INDEX_WB_SPEECH_MODE_PARAM:
	{
		// memcpy(l1sp.sph_allWbModePara, buffer, sizeof(kal_uint16) * NUM_SPH_MODE * NUM_MODE_PARAS);
		// 	l1sp.setNvramValuesFlag |= L1SP_NVRAM_VALUES_FLAG_WB_MODE; // (1<<3) bit [3]
		l1sp_setAllWbSpeechModePara((kal_uint16 *)buffer, NUM_SPH_MODE * NUM_MODE_PARAS);
	}
		break;
// #endif

	default:
		ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0); 
	}
		
}

#endif // if 0 

#if 0
/**
	@device: device using in speech. pleae refer to 
		#define  L1SP_BUFFER_0        0x01    // NOTE: Don't use buffer definition directly
		#define  L1SP_BUFFER_1        0x02    //       Use speaker definition below
		#define  L1SP_BUFFER_ST       0x04
		#define  L1SP_BUFFER_EXT      0x08
		#define  L1SP_BUFFER_EXT_G    0x10
		#define  L1SP_STEREO2MONO     0x20     // Do not use this term for speaker definition
		#define  L1SP_BUFFER_ST_M     (L1SP_BUFFER_ST|L1SP_STEREO2MONO)
		#define  L1SP_EXT_DAC_BUF0    0x40 
		#define  L1SP_EXT_DAC_BUF1    0x80
	Phase-out function without using
*/
void L1SP_SetOutputDevice( uint8 device )
{
#if 0 //def ANALOG_AFE_PATH_EXIST
   l1sp.output_dev = device;
   AFE_SetOutputDevice( L1SP_SPEECH, device );   
#endif // #ifdef ANALOG_AFE_PATH_EXIST
}


/**
	@return: device using in speech.
	Phase-out function without using
*/
uint8 L1SP_GetOutputDevice(void)
{
   return 0; //l1sp.output_dev;
}
#endif // if 0

#if 0
/**
	Function is used when MODEM side has PGA gain & DSP digital gain control 
	i.e. ANALOG_AFE_PATH_EXIST is defined
	
	@volume1: MMI(EM) value from 0 to 256	
	@digital_gain_index:unit is 0.5 db. Value from 0 to -64db (seems option)
*/
void L1SP_SetOutputVolume( uint8 volume1, int8 digital_gain_index )
{
#if 0 // ANALOG_AFE_PATH_EXIST  
   l1sp.sph_dl_vol = volume1; 	
   AFE_SetOutputVolume( L1SP_SPEECH, volume1, digital_gain_index );   
#endif
}
#endif

// #ifndef ANALOG_AFE_PATH_EXIST  // ONLY exist digital path and hw
void l1sp_digiOnly_SetOutputVolume(int16 digitalGain)
{
	AFE_DigitalOnly_SetDigitalGain( L1SP_SPEECH, digitalGain );
}
// #endif

void l1sp_digiOnly_SetEnhRefOutputVolume(int16 digitalRefGain)
{
	AFE_DigitalOnly_SetEnhRefDigitalGain(digitalRefGain);
}

#if 0
/**
	this function is to replace L1SP_SetSpeechVolumeLevel()
	
	@level: [input] speech volume level for speech
	@v_paraIndex: [input] volume paramter (saving in nvram) index
*/
void L1SP_SetSpeechVolumeLevelByIndex(kal_uint8 level, kal_uint8 v_paraIndex)
{
//  phase out. 
// 	L1SP_SetSpeechVolumeLevel(level, l1sp.sph_allVolumePara[v_paraIndex][level]);
}
#endif //if 0

#if 0
/**
	Phase out function without using
	@src: microhpone source for speech, plese refer to
		#define  L1SP_LNA_0           0
		#define  L1SP_LNA_1           1
		#define  L1SP_LNA_DIGITAL     2
		#define  L1SP_LNA_FMRR        3
	
*/
void L1SP_SetInputSource( uint8 src )
{
#if 0 //def ANALOG_AFE_PATH_EXIST    
   AFE_SetInputSource( src );
#endif
}

/**
	Phase out function without using
	@return: microphone source for speech
*/
uint8 L1SP_GetInputSource( void )
{
	return 0; // AFE_GetInputSource();
}
#endif 

#if 0
/**
	@mic_volume: MMI(EM) value. Including Analog & digital gain
*/
void L1SP_SetMicrophoneVolume( uint8 mic_volume )
{


}
#endif // if 0 

// #ifndef ANALOG_AFE_PATH_EXIST  // ONLY exist digital path and hw
void l1sp_digiOnly_SetMicrophoneVolume(int16 digitalGain)
{
	AFE_DigitalOnly_SetMicrophoneVolume(digitalGain);
}
// #endif

#if 0
uint8 L1SP_GetMicrophoneVolume( void )
{
   return 0;
}
/**
	Phase out function without using
	@sidetone: value from EM, which is 0~255. 
*/
void L1SP_SetSidetoneVolume( uint8 sidetone )
{
	// keep API header to avoid link error
}

/**
	Phase out function without using
	@return: value from EM, which is 0~255. 
*/
uint8 L1SP_GetSidetoneVolume( void )
{
	// keep API header to avoid link error, 
	return 0;
}
#endif // #if 0


void SP_MuteUlEnhResult(bool mute)
{
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
			uint16 aud_id = L1SP_GetAudID();
// #endif  
	l1sp.isUlEnhResultMute = mute;
	AM_Mute_UL_EnhResult_Speech(mute);

// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
// #endif 

}

void SP_MuteUlSource(bool mute)
{
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
			uint16 aud_id = L1SP_GetAudID();
// #endif  
	l1sp.isUlSourceMute = mute;
	AM_Mute_UL_Source_Speech(mute);

// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
// #endif 

}


void SP_MuteController() // central controll, and placed between L1Audio_SetFlag and L1Audio_ClearFlag
{
   //return; returning at this point will be equal to disable this feature
   // TODO: c2k kal_trace(TRACE_INFO, L1SP_MUTE_CONTROL_STATUS, 2, l1sp.isULMute, l1sp.isDLMute, l1sp.codec_mute, l1sp.forcedUnmuteController);
   //DL 
   if(l1sp.forcedUnmuteDLController){
      AM_Mute_DL_8K_Speech(l1sp.isDLMute); 
      AM_Mute_DL_Speech_Traffic(false);
   } else {
      AM_Mute_DL_8K_Speech(l1sp.isDLMute);
      AM_Mute_DL_Speech_Traffic(l1sp.codec_mute);
   }
   if(l1sp.forcedUnmuteULController)
   {
      //UL
      AM_Mute_UL_Codec_Speech(0 != l1sp.isULMute);            
   } else {
      if(l1sp.codec_mute){
         AM_Mute_UL_Codec_Speech(true);         
      }else{      
         AM_Mute_UL_Codec_Speech(0 != l1sp.isULMute);     
      }
   }

}
void SP_MuteUlFromDiffPos(bool mute, SP_MIC_MUTE_POS pos)
{
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
		uint16 aud_id = L1SP_GetAudID();
// #endif   

	if(mute) 
		l1sp.isULMute |= pos; 
	else 
		l1sp.isULMute &= (~pos);

	// AM_Mute_UL_Codec_Speech( 0 != l1sp.isULMute );
	SP_MuteController();
	
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
// #endif      	
}

/**
	For MED use ONLY!!! 
	other function please use void SP_MuteUlFromDiffPos(bool mute, SP_MIC_MUTE_POS pos)
*/
void L1SP_MuteMicrophone( bool mute )
{	
   
   //AFE_MuteMicrophone( mute ); // do not use AFE_MuteMicrophone for speech enhancement
   // AM_MuteULSpeech( mute );
   // l1sp.isULMute = mute;
   SP_MuteUlFromDiffPos(mute, SP_MIC_MUTE_POS_FROM_MED);
}

void L1SP_MuteSpeaker( bool mute )
{
#if 0 // #ifdef ANALOG_AFE_PATH_EXIST
   AFE_MuteSpeaker( L1SP_SPEECH, mute );
#else

// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   uint16 aud_id = L1SP_GetAudID();
// #endif      
	//AM_MuteDLSpeech( mute );
   l1sp.isDLMute = mute;
   SP_MuteController();
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
//#endif      
	
#endif
}

bool SP_IsSpeakerMute(void)
{
        return (l1sp.isDLMute!=0);
}

bool SP_IsMicMute(void)
{
	return (l1sp.isULMute!=0);
}

/**
	For MED use ONLY!!! 
	other function please use SP_IsMicMute() instead
*/
bool L1SP_IsMicrophoneMuted( void )
{
	return (l1sp.isULMute&SP_MIC_MUTE_POS_FROM_MED)!=0; 
   // return AM_IsULSpeechMuted();
}

//extern void CSR_SP3G_Callback( SP3G_Event event, void *data );
// extern void vt_SP3G_Callback( kal_uint8 event, void *data );
// extern void CSR_Codec_Ready(int rab_id);
// extern void CSR_Codec_Close(int rab_id);
/*
#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )

void L1SP_3G_Request(void)
{  
   if(l1sp.state ==  L1SP_STATE_3G_SPEECH_ON)
   {
      CSR_Codec_Ready(SP3G_Rab_Id());
      return;
   }
}
#endif
*/

void L1SP_SetState(uint8 state)
{
#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )
   if(( l1sp.state == L1SP_STATE_2G_SPEECH_ON && state == L1SP_STATE_3G_SPEECH_ON ) ||
      ( l1sp.state == L1SP_STATE_2G_SPEECH_ON && state == L1SP_STATE_4G_SPEECH_ON ) ||
      ( l1sp.state == L1SP_STATE_3G_SPEECH_ON && state == L1SP_STATE_2G_SPEECH_ON ) ||
      ( l1sp.state == L1SP_STATE_3G_SPEECH_ON && state == L1SP_STATE_4G_SPEECH_ON ) ||      
      ( l1sp.state == L1SP_STATE_4G_SPEECH_ON && state == L1SP_STATE_2G_SPEECH_ON ) ||
      ( l1sp.state == L1SP_STATE_4G_SPEECH_ON && state == L1SP_STATE_3G_SPEECH_ON ) )
      l1sp.interRAT = true;
#endif
   l1sp.state = state; 
	// TODO: c2k trace
   // L1Audio_Msg_Speech_State(L1Audio_Speech_State(state));
   MonTrace(MON_CP_HWD_SPH_SPEECH_STATE_TRACE_ID, 1, state);   
}

uint8 L1SP_GetState( void )
{
   return l1sp.state; 
}


bool L1SP_IsSpeechOn( void )
{
    return (AM_IsSpeechOn());
}

bool voc_flag = false;


void L1SP_Speech_On( uint8 RAT_Mode )
{
	// TODO: c2k [VoC]
	/*
   uint32 module_id = MOD_L1AUDIO_SPH_SRV;
   uint32 voc_ptn = TVCI_CREATE_FILE;
   uint32 *voc_ptn_ptr = &voc_ptn;
   */
	// End TODO: c2k [VoC]
   
   MonTrace(MON_CP_HWD_SPH_SPEECH_ON_TRACE_ID, 4, 0, l1sp.tch_state, l1sp.isSO_Connected, RAT_Mode);
	
   if( AM_IsSpeechOn() )
   {
#if defined(__CVSD_CODEC_SUPPORT__) 
      if((L1SP_GetSpeechMode() == SPH_MODE_BT_EARPHONE || L1SP_GetSpeechMode() == SPH_MODE_BT_CARKIT) && !BT_SCO_IS_SPEECH_ON())
      {
         bool is_WB = AFE_GetVoice8KMode() == 1 ? KAL_TRUE : KAL_FALSE;
         BT_SCO_SPEECH_ON(is_WB);
      }
#endif
      return;
   }

#if defined(MTK_PLT_MODEM_ONLY) // (!defined(__SMART_PHONE_MODEM__)) && (!defined(__DATA_CARD_SPEECH__))
	// done TODO: c2k [ApAudSys_config]
   // ApAudSys_config();
#endif // defined(__SMART_PHONE_MODEM__)

   // for phone call used. 
   l1sp.aud_id = L1Audio_GetAudioID();
   L1Audio_SetFlag( l1sp.aud_id );     /* To lock sleep mode */
	 
	 
   SP_MuteController();
	//Check the mute status, this should always unmute. 
	// TODO: [add SAL] ASSERT(SAL_Mute_Check(SAL_MUTE_DL_8K)==0, HWDSPH_ERR_FORCE_ASSERT, 0);
	
#if !defined(__SMART_PHONE_MODEM__)
	// this two function is useless under smart phone, remove it to reduce process time
   // TONE_StopAndWait();
   // KT_StopAndWait();
#endif
   
   spe_updateSpeAppMask(0xff, true);

#if defined(__VOLTE_SUPPORT__)
	sp4g_speech_init( RAT_4G_MODE );
#endif

#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )
#ifdef __VIDEO_CALL_SUPPORT__
   if( RAT_Mode == RAT_3G324M_MODE )
      sp3g_speech_init( RAT_3G324M_MODE );
   else
#endif
      //In Dual mode, allow 2G/3G capability at initialization stage  for InterRAT HO
      //if 3G324M, there is no InterRAT
      sp3g_speech_init( RAT_3G_MODE );
	// TODO: c2k tracke
	// L1Audio_Msg_SP(0);
     
	// choose the mode is 2g /3g
#if defined(__VOLTE_SUPPORT__)
   if( SP4G_Rab_State()){
      RAT_Mode = RAT_4G_MODE;
   }else 
#endif
   if( SP3G_Rab_State() && RAT_Mode != RAT_3G324M_MODE ){
      RAT_Mode = RAT_3G_MODE;
   }else
#endif      
   if (l1sp.isSO_Connected){
      RAT_Mode = RAT_C2K_MODE;
   }else if ( l1sp.tch_state ){
      RAT_Mode = RAT_2G_MODE;
   }else if(RAT_Mode != RAT_3G324M_MODE ){
   	  RAT_Mode = RAT_2G_MODE;
   }
   l1sp.interRAT = false;
   
   MonTrace(MON_CP_HWD_SPH_SPEECH_ON_TRACE_ID, 4, 1, l1sp.tch_state, l1sp.isSO_Connected, RAT_Mode);

	// TODO: c2k [am]
   AM_SpeechOn(RAT_Mode, 0);

   switch(RAT_Mode)
   {
      case RAT_2G_MODE:
#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )      	
         // TODO: c2k [sp_mutex] kal_take_enh_mutex( sp_mutex );           
         if( SP3G_Rab_State()){
		         void SP3G_Rab_Est_sub();
		         SP3G_Rab_Est_sub();              	
         }else
#endif         	
         {
      	   AM_SetDSP2GReset(true);
           L1SP_SetState(L1SP_STATE_2G_SPEECH_ON);           	
         }
#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )      	         
         // TODO: c2k [sp_mutex] kal_give_enh_mutex(sp_mutex);
   #if !defined(__L1_STANDALONE__)
         #if !defined( __UMTS_TDD128_MODE__ )
         {
            if(query_ps_conf_test_mode()==PS_CONF_TEST_FTA)
               SP3G_SetDTX(false);
            else
               SP3G_SetDTX(true);
         }
         #endif  
   #endif             
#endif         	         
 
         break;
#if defined( __UMTS_RAT__ ) || defined( __UMTS_TDD128_MODE__ )
      case RAT_3G_MODE: 
         L1SP_SetState(L1SP_STATE_3G_SPEECH_ON);
         CSR_Codec_Ready(SP3G_Rab_Id());
#if !defined(__L1_STANDALONE__)
    #if !defined( __UMTS_TDD128_MODE__ )
         {
            if(query_ps_conf_test_mode()==PS_CONF_TEST_FTA)
               SP3G_SetDTX(false);
            else
               SP3G_SetDTX(true);
         }
    #endif
#endif         
         break;
#ifdef __VIDEO_CALL_SUPPORT__
      case RAT_3G324M_MODE: 
         L1SP_SetState(L1SP_STATE_3G324M_SPEECH_ON);
         vt_SP3G_Callback( (kal_uint8)SP3G_CODEC_READY, (void*)0 );
         break;
#endif
#endif
#if defined(__VOLTE_SUPPORT__)
      case RAT_4G_MODE: 
         L1SP_SetState(L1SP_STATE_4G_SPEECH_ON);
         PSR_SP4G_Callback(SP4G_CODEC_READY, (void*)SP4G_Rab_Id());              
         break;
#endif
      case RAT_C2K_MODE:
         L1SP_SetState(L1SP_STATE_C2K_SPEECH_ON);
         sub_SPC2K_ConSSO_Done();
         break;
      default: 
         ASSERT(false, HWDSPH_ERR_FORCE_ASSERT, 0);
         break;
   }
   
   MonTrace(MON_CP_HWD_SPH_SPEECH_ON_TRACE_ID, 4, 2, l1sp.tch_state, l1sp.isSO_Connected, RAT_Mode);

	// TODO: c2k [VoC]
	/*
#ifndef __L1_STANDALONE__ // avoid link error
   if(!tst_trace_check_ps_filter_off(TRACE_GROUP_VM, &module_id, 0x2))
   {
      tst_vc_response(TVCI_VM_LOGGING, (const kal_uint8*)voc_ptn_ptr, 4);
      VMREC_Start(NULL, 0, true);
      voc_flag = true;
   }
#endif
	*/
	// End TODO: c2k [VoC]

	//always unmute DSP uplink when speech on (This is to keep the mute definition)
	SP_MuteUlEnhResult(false); 

   SetSpeechEnhancement( true );

	//always unmute DSP uplink when speech on (This is to keep the mute definition)
	SP_MuteUlFromDiffPos(false, SP_MIC_MUTE_POS_FROM_ALL); 
	L1SP_MuteSpeaker(false);

	
// for phone call usage   
#if !DATA_CARD_DISABLE_INTERNAL
#if defined(__DATA_CARD_SPEECH__)
   if (l1sp.strmOnHandler != NULL)
      l1sp.strmOnHandler( NULL );
#endif
#endif


#if defined(__ECALL_SUPPORT__)
   if (l1sp.pcm4wayOnHandler != NULL)
      l1sp.pcm4wayOnHandler( (void *)0);
#endif

#if defined(__CVSD_CODEC_SUPPORT__) 
   if(L1SP_GetSpeechMode() == SPH_MODE_BT_EARPHONE || L1SP_GetSpeechMode() == SPH_MODE_BT_CARKIT)
   {
      kal_bool is_WB = AFE_GetVoice8KMode() == 1 ? KAL_TRUE : KAL_FALSE;
      BT_SCO_SPEECH_ON(is_WB);
   }
#endif
   MonTrace(MON_CP_HWD_SPH_SPEECH_ON_TRACE_ID, 4, 3, l1sp.tch_state, l1sp.isSO_Connected, RAT_Mode);
}


#if defined(__DATA_CARD_SPEECH__)
void L1SP_Register_Strm_Handler(
   void (*onHandler)(void *hdl),
   void (*offHandler)(void *hdl),
   void (*hdl)(uint32 event, void *param))
{
   l1sp.strmOnHandler  = onHandler;
   l1sp.strmOffHandler = offHandler;
   l1sp.strmHdl        = hdl;
   //For MOLY00004781, sometimes auto script hit that register handler later than speech on. In this case, we let daca run.
   if( AM_IsSpeechOn() ){
      l1sp.strmOnHandler(NULL);
   }
}

void L1SP_UnRegister_Strm_Handler( void )
{
   if(AM_IsDataCardOn() && (l1sp.strmOffHandler != NULL)){
      /* Normal     Case: PLUGIN(RegisterHandler)-->SpeechOn(OnHandler)-->SpeechOff(OffHandler) -->PLUGOUT(UnRegisterHandler)
         Unexpected Case: PLUGIN(RegisterHandler)-->SpeechOn(OnHandler)-->PLUGOUT(UnRegisterHandler), registerHandler again -->SpeechOff(OffHandler)     */      
      DACA_Stop(DACA_APP_TYPE_ACTIVE_UL_DL_WB);   
   }
   l1sp.strmOnHandler  = NULL;
   l1sp.strmOffHandler = NULL;
}
#endif

#if defined(__ECALL_SUPPORT__)
void L1SP_Register_Pcm4WayService(void (*onHandler)(void *), void (*offHandler)(void *))
{
   l1sp.pcm4wayOnHandler = onHandler; 
   l1sp.pcm4wayOffHandler = offHandler;
}

void L1SP_UnRegister_Pcm4Way_Service( void )
{
   l1sp.pcm4wayOnHandler = NULL; 
   l1sp.pcm4wayOffHandler = NULL;
}
#endif

void L1SP_Speech_Off( void )
{
	// TODO: c2k [VoC]
	/*
   uint32 voc_ptn = TVCI_CLOSE_FILE;
   uint32 *voc_ptn_ptr = &voc_ptn;
    */
   //End TODO: c2k [VoC]
   MonTrace(MON_CP_HWD_SPH_SPEECH_OFF_TRACE_ID, 1, 0);
#if defined(__CVSD_CODEC_SUPPORT__) 
   if(BT_SCO_IS_SPEECH_ON())
   {
      BT_SCO_SPEECH_OFF();
   }
#endif
  
   if( !AM_IsSpeechOn() )
      return;

	// mute DL to prevent sound pushes to hardware buffer then causes noise. Sync with the end of speech unumte
	L1SP_MuteSpeaker(true);
		
#if !DATA_CARD_DISABLE_INTERNAL      
//FIXME: for phone call usage
#if defined(__DATA_CARD_SPEECH__)
   if (l1sp.strmOffHandler != NULL)
      l1sp.strmOffHandler( NULL );
#endif
#endif

#if defined(__ECALL_SUPPORT__)
   if (l1sp.pcm4wayOffHandler != NULL)
      l1sp.pcm4wayOffHandler( (void *)0 );
#endif

#if 0
	// recording
   if(l1sp.offHandler != NULL)
      l1sp.offHandler( (void *)(uint32)l1sp.state );
#endif

   SetSpeechEnhancement( false );

	// TODO: c2k [VoC]
	/*
   #ifndef __L1_STANDALONE__ // avoid link error
   // kal_uint32 module_id = MOD_L1AUDIO_SPH_SRV;	   
   // if(!tst_trace_check_ps_filter_off(TRACE_GROUP_VM, &module_id, 0x2) && voc_flag)
   if(voc_flag)
   {
      tst_vc_response(TVCI_VM_LOGGING, (const uint8*)voc_ptn_ptr, 4);
	  VMREC_Stop(true);
	  voc_flag = false;
   }
	#endif
	*/
	//End TODO: c2k [VoC]

	// TODO: c2k [am]
   AM_SpeechOff(0);

#if defined(__VOLTE_SUPPORT__)
   {
      uint8 state;
      state = l1sp.state;
      
      if(l1sp.state == L1SP_STATE_4G_SPEECH_ON || l1sp.interRAT ){
         L1SP_SetState(L1SP_STATE_4G_SPEECH_CLOSING);
      }
      sp4g_speech_close();
         
      /* only these cases should wait state, if only 2G happen, then return to idle */
      if( state == L1SP_STATE_4G_SPEECH_ON || l1sp.interRAT
      ){
            PSR_SP4G_Callback(SP4G_CODEC_CLOSED, (void*)SP4G_Rab_Id());
      }
   }
#endif

#if defined( __UMTS_RAT__ )
   {
      uint8 state;
      state = l1sp.state;
      
      if(l1sp.state == L1SP_STATE_3G_SPEECH_ON || l1sp.interRAT
#ifdef __VIDEO_CALL_SUPPORT__
          || l1sp.state == L1SP_STATE_3G324M_SPEECH_ON
#endif
      ){
         L1SP_SetState(L1SP_STATE_3G_SPEECH_CLOSING);
      }
      sp3g_speech_close();
         
    /* only these cases should wait state, if only 2G happen, then return to idle */
      if( state == L1SP_STATE_3G_SPEECH_ON || l1sp.interRAT
#ifdef __VIDEO_CALL_SUPPORT__
          || state == L1SP_STATE_3G324M_SPEECH_ON
#endif
      ){
         //uint32 waitTime;
#ifdef __VIDEO_CALL_SUPPORT__
         if( state == L1SP_STATE_3G324M_SPEECH_ON )
            vt_SP3G_Callback( (uint8)SP3G_CODEC_CLOSED, (void*)0);
         else
#endif
            CSR_Codec_Close(SP3G_Rab_Id());
         /*
         for(waitTime = 0 ; ; waitTime ++)
         {     
            if(l1sp.state == L1SP_STATE_3G_SPEECH_CLOSED)
               break;
            ASSERT(waitTime < 20, HWDSPH_ERR_FORCE_ASSERT, 0);     
            kal_sleep_task(1);
         }*/
      }
   }
#endif

   {
      uint8 state;
      state = l1sp.state;
      
      if(l1sp.state == L1SP_STATE_C2K_SPEECH_ON || l1sp.isSO_Connected){
         L1SP_SetState(L1SP_STATE_C2K_SPEECH_CLOSING);
      }
      ;//TODO : Daniel :sp3g_speech_close();
         
      
      if( state == L1SP_STATE_C2K_SPEECH_ON || l1sp.isSO_Connected){
         //uint32 waitTime;
            ;//TODO : Daniel : CSR_Codec_Close(SP3G_Rab_Id());
      }
   }

   L1SP_SetState( L1SP_STATE_IDLE );

#if defined(__VIBRATION_SPEAKER_SUPPORT__)
   VIBR_Vibration_Activate();
#endif

	//always unmute DSP uplink after speech off to keep the mute definition
	SP_MuteUlEnhResult(false);
	SP_MuteUlFromDiffPos(false, SP_MIC_MUTE_POS_FROM_ALL); 
	L1SP_MuteSpeaker(false);
	
   L1Audio_ClearFlag( l1sp.aud_id );
   L1Audio_FreeAudioID( l1sp.aud_id );
   MonTrace(MON_CP_HWD_SPH_SPEECH_OFF_TRACE_ID, 1, 1);  
}

void L1SP_Set_DAI_Mode( uint8 mode )
{
	//this feature is phased out
}

void L1SP_SetAfeLoopback( bool enable )
{
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   uint16 aud_id = L1SP_GetAudID();
// #endif         
   if( enable )
      AFE_TurnOnLoopback();
   else
      AFE_TurnOffLoopback();
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
// #endif      
}

#if 0 
void L1SP_SetFIR( bool enable )
{
   if( enable ){
		
#if 0 // #ifdef ANALOG_AFE_PATH_EXIST		
      AFE_TurnOnFIR( L1SP_SPEECH );
      AFE_TurnOnFIR( L1SP_VOICE );
#endif // ANALOG_AFE_PATH_EXIST		

   }else{
   
#if 0 // #ifdef ANALOG_AFE_PATH_EXIST   
      AFE_TurnOffFIR( L1SP_SPEECH );
      AFE_TurnOffFIR( L1SP_VOICE );
#endif //ANALOG_AFE_PATH_EXIST

   }
}
#endif // #if 0


void L1SP_LoadSpeechPara( void ) 
{
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   uint16 aud_id = L1SP_GetAudID();
// #endif   
   SPE_LoadSpeechPara(l1sp.sph_c_para, l1sp.sph_m_para, NULL, l1sp.sph_m_paraWb);  //l1sp.sph_v_para seems no one use it
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
// #endif      
}

extern void spc_send_network_status_notify(void);

/**
	the Function run on VAL Task. 
*/
void l1sp_CodecStatusNotification(uint16 codec)
{   	
	// send via L4C. too keep original process
	l1sp_send_codec_status_notify(codec);	
}


/** Remove
void l1sp_NetworkStatusNotification(void)
{
#if defined(__SMART_PHONE_MODEM__)     
   if(kal_if_lisr())
	   L1Audio_LSetEvent(l1sp.aud_id_network_status, NULL);
	else
	   L1Audio_SetEvent(l1sp.aud_id_network_status, NULL);
#endif	   
}
*/

//VAL task
void l1sp_send_codec_status_notify(uint16 data)//to L4C
{
	 uint16 codec = data;
	 L1SP_L4C_Codec l4c_codec = L1SP_L4C_C2K_NONE;

	  
	 switch(codec){
	    case SAL_C2K_COD_MODE_UNDEF:
	      l4c_codec = L1SP_L4C_C2K_NONE;
	      break;
	    case SAL_C2K_COD_QCELP13K:
	      l4c_codec = L1SP_L4C_C2K_QCELP13K;
	      break;
	    case SAL_C2K_COD_EVRCA:
	      l4c_codec = L1SP_L4C_C2K_EVRCA;
	      break;
		case SAL_C2K_COD_EVRCB:
	      l4c_codec = L1SP_L4C_C2K_EVRCB;
	      break;
	    case SAL_C2K_COD_EVRCNW_WB:
	      l4c_codec = L1SP_L4C_C2K_EVRCNW_WB;
	      break;
	    case SAL_C2K_COD_EVRCNW_NB:
	      l4c_codec = L1SP_L4C_C2K_EVRCNW_NB;
	      break;  
	    default:
		 ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);
	      break;
	 }
	 //kal_trace(TRACE_INFO, L1SP_L4C_SEND_CODEC, codec, l4c_codec);
	 MonTrace(MON_CP_HWD_SPH_L4C_CODEC_TRACE_ID, 2,codec,l4c_codec);
	 
	 SendEvocdToAtc(l4c_codec);
}

#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
extern const unsigned short Speech_Normal_Mode_Para[16];
extern const unsigned short WB_Speech_Normal_Mode_Para[16];
#endif 

void L1SP_Init( void )
{
#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
  uint16 commonP[] = { 0, 55997, 31000, 10752, 	32769,	   0,	  0,	 0, 	0,	   0, 0, 0};
   l1sp.tch_state = false;   
#endif

#if defined(MTK_PLT_MODEM_ONLY) // (!defined(__SMART_PHONE_MODEM__)) && (!defined(__DATA_CARD_SPEECH__))
	// done TODO: c2k [ApAudSys_config]
   ApAudSys_config();
#endif

   SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_ALL, false);

   SysMemset(&l1sp.sph_c_para, 0, NUM_COMMON_PARAS*sizeof(uint16));
   SysMemset(&l1sp.sph_m_para, 0, NUM_MODE_PARAS*sizeof(uint16));
   SysMemset(&l1sp.sph_v_para, 0, NUM_VOL_PARAS*sizeof(uint16));
	SysMemset(&l1sp.sph_m_paraWb, 0, NUM_MODE_PARAS*sizeof(uint16));
#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
   memcpy(l1sp.sph_c_para, commonP, NUM_COMMON_PARAS*sizeof(uint16));
   memcpy(l1sp.sph_m_para, Speech_Normal_Mode_Para, 16*sizeof(uint16));
   memcpy( l1sp.sph_m_paraWb, WB_Speech_Normal_Mode_Para, 16*sizeof(uint16));
   L1SP_LoadCommonSpeechPara(commonP);		 		
#endif

	l1sp.mic_volume = 0;

   l1sp.bt_on = false;
#if defined( __UMTS_RAT__ )
   sp3g_init();
   l1sp.interRAT = false;
#endif
#if defined(__VOLTE_SUPPORT__)
   sp4g_init();
#endif
   SPC2K_init();
	// ~ done TODO: c2k [enhancement Init]
   SPE_Init();
   l1sp.isDLMute = l1sp.isULMute = false;
   l1sp.codec_mute = true;
   l1sp.codec_mute_mode = L1SP_L4C_EVENT_NONE;

#if defined(__DATA_CARD_SPEECH__)
   l1sp.strmOnHandler  = NULL;
   l1sp.strmOffHandler = NULL;
   l1sp.strmHdl = NULL;
#endif      
#if defined(__ECALL_SUPPORT__)
   l1sp.pcm4wayOnHandler = NULL; 
   l1sp.pcm4wayOffHandler = NULL;
#endif

   L1SP_SetState(L1SP_STATE_IDLE);

// #if defined(__SMART_PHONE_MODEM__) // REMOVE 
// 	l1sp.aud_id_network_status = L1Audio_GetAudioID();
// 	L1Audio_SetEventHandler(l1sp.aud_id_network_status, (L1Audio_EventHandler)spc_send_network_status_notify);  
// #endif

   
	// for open dsp
#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
	 IopCcismDevOpen(IOP_CCISM_AUDIO_CHANNEL, NULL, NULL);
#endif


   l1sp.c2k_so_codec = SAL_C2K_COD_MODE_UNDEF;
   l1sp.isConSSO_Done = l1sp.isSO_Connected = false;
}

bool L1SP_TCH_State( void )
{
   return l1sp.tch_state;
}

extern uint32 SP3G_GetCodecMode(void);
extern uint32 SP4G_GetCodecMode(void);

// This is invoked by 2G L1D(LISR)
void L1SP_TCH_Notify( bool bOn )
{
#if defined(_SWITCH_AFE_CLK_)      
   uint16 L1D_GetRF(uint16 mode);
   uint16 RF_2G = L1D_GetRF(MML1_RF_2G);
   L1Audio_Msg_L1D_GetRF(RF_2G);
#endif   
   if( !bOn ){ // for 2G->3G handover, mute speech in case Speech_Off command too late to avoid noise
   	// TODO: c2k [trace]
      // L1Audio_Msg_TCH_NOTIFY(L1AUDIO_Str_onoff(0), L1Audio_Speech_State(l1sp.state));
      l1sp.tch_state = FALSE;
#if defined(_SWITCH_AFE_CLK_)               
      if( 2 == RF_2G){ //return :   1 -> RF1,  2-> RF2
         *MD2GSYS_AFE_CK_SEL = 0x0;         
      }
#endif      
      //mute speech
#if defined(__VOLTE_SUPPORT__)
      if( SP4G_Rab_State() && l1sp.state != L1SP_STATE_4G_SPEECH_ON && l1sp.state != L1SP_STATE_IDLE){//2G->4G fail case, TCH will be off
         SP4G_Reset();
         AM_InterRAT_2G_to_4G(SP4G_GetCodecMode());         
         L1SP_SetState( L1SP_STATE_4G_SPEECH_ON );
      }else     
#endif                     

#if defined( __UMTS_RAT__ )      
      if( SP3G_Rab_State() && l1sp.state != L1SP_STATE_3G_SPEECH_ON && l1sp.state != L1SP_STATE_IDLE){//3G->2G fail case, TCH will be off
         SP3G_Reset();
         AM_InterRAT_2G_to_3G(SP3G_GetCodecMode());        
         L1SP_SetState( L1SP_STATE_3G_SPEECH_ON );
      }
#endif      
      //else if( l1sp.state == L1SP_STATE_2G_SPEECH_ON ) 
         // in case speech off first then TCH off notify.(2G only. If in 3G, then don't care)
         //*DP_SC_MUTE |= 0x0002;
   }
   else{
      // TODO: c2k [trace]
      // L1Audio_Msg_TCH_NOTIFY(L1AUDIO_Str_onoff(1), L1Audio_Speech_State(l1sp.state));
      l1sp.tch_state = true;
#if defined(_SWITCH_AFE_CLK_)         
      if( 2 == RF_2G){ //return :   1 -> RF1,  2-> RF2
         *MD2GSYS_AFE_CK_SEL = 0x1;
      }
#endif      
      //*DP_SC_MUTE &= ~0x0002;
#if defined(__VOLTE_SUPPORT__)
      if( l1sp.state == L1SP_STATE_4G_SPEECH_ON ){//4G->2G HO
         AM_InterRAT_4G_to_2G();
         L1SP_SetState( L1SP_STATE_2G_SPEECH_ON );
      }else 
#endif               

#if defined( __UMTS_RAT__ )      
      if( l1sp.state == L1SP_STATE_3G_SPEECH_ON ){//3G->2G HO
         AM_InterRAT_3G_to_2G();         
         L1SP_SetState( L1SP_STATE_2G_SPEECH_ON );
      }
#endif      
   }

}

void L1SP_SpeechLoopBackEnable(bool fgEnable)
{
	 SAL_LBK_Codec(fgEnable);
}

//-----------------------------------------------------------------------------

bool SP_is_codec_mute()
{
	 return l1sp.codec_mute;
}

void SP_L4C_SetEvent(L1SP_L4C_Event event, L1SP_L4C_Event_Mode mode)
{
   // TODO: c2k kal_trace(TRACE_INFO, L1SP_L4C_EVENT, event, mode, l1sp.codec_mute_mode);
   // TODO: c2k kal_trace(TRACE_INFO, L1SP_MUTE_CONTROL_STATUS, 0, l1sp.isULMute, l1sp.isDLMute, l1sp.codec_mute, l1sp.forcedUnmuteController);
   
   ASSERT( L1SP_L4C_EVENT_NONE == l1sp.codec_mute_mode || mode == l1sp.codec_mute_mode , HWDSPH_ERR_FORCE_ASSERT, 0);
   
   switch(event){
   case L1SP_L4C_ESPEECH_0:
      ASSERT(!l1sp.codec_mute, HWDSPH_ERR_FORCE_ASSERT, 0);
      l1sp.codec_mute = true;
      l1sp.codec_mute_mode = L1SP_L4C_EVENT_NONE;
      break;   
   case L1SP_L4C_ESPEECH_1:
      ASSERT(l1sp.codec_mute, HWDSPH_ERR_FORCE_ASSERT, 0);
      l1sp.codec_mute = false;
      l1sp.codec_mute_mode = mode;
      break;      
   default:
      ASSERT(0, HWDSPH_ERR_FORCE_ASSERT, 0);   
   }

   {   
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   uint16 aud_id = L1SP_GetAudID();
// #endif      
   SP_MuteController();
// TODO: c2k
// #if ( defined( __CENTRALIZED_SLEEP_MANAGER__ )&& defined( MTK_SLEEP_ENABLE ))
   L1SP_FreeAudID(aud_id);
// #endif  
   // TODO: c2k kal_trace(TRACE_INFO, L1SP_MUTE_CONTROL_STATUS, 1, l1sp.isULMute, l1sp.isDLMute, l1sp.codec_mute, l1sp.forcedUnmuteController);
   }
}

void SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_BITMASK mask, bool b)
{
   SP_SetForcedUnMuteDLController(mask, b);
   SP_SetForcedUnMuteULController(mask, b);
}

void SP_SetForcedUnMuteDLController(L1SP_FORCEDUNMUTE_BITMASK mask, bool b)
{
   
   if(b){  
      l1sp.forcedUnmuteDLController |= mask;
   }else{
      l1sp.forcedUnmuteDLController &= (~mask);
   }
   
   {
       uint16 aud_id = L1SP_GetAudID();
       SP_MuteController();
       L1SP_FreeAudID(aud_id);
   }
}

void SP_SetForcedUnMuteULController(L1SP_FORCEDUNMUTE_BITMASK mask, bool b)
{
   MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 0xABCDAAAA, l1sp.forcedUnmuteULController, mask, b);
   MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 0xABCDAAAA, l1sp.forcedUnmuteDLController);
   if(b){  
      l1sp.forcedUnmuteULController |= mask;
   }else{
      l1sp.forcedUnmuteULController &= (~mask);
   }
   
   {
       uint16 aud_id = L1SP_GetAudID();
       SP_MuteController();
       L1SP_FreeAudID(aud_id);
   }
   
   MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 0xEFFFFFFF, l1sp.forcedUnmuteULController);
   MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 0xEFFFFFFF, l1sp.forcedUnmuteDLController);
}


#if defined(__VOLTE_SUPPORT__)
static const L1SP_Tones tone_call_waiting[]           =  {  { 440,   0, 200, 100,   1 },
                                                            { 440,   0, 200,3500,   0 }   };        

extern void spc_BgSndConfig(uint8 ulGainLevel, uint8 dlGainLevel);
int spc_ul_gain = 7;
int spc_dl_gain = 7;
#endif
void SpeechDVT(int item){
#if defined(__VOLTE_SUPPORT__)
   switch(item){
   case 0:  
      spc_ul_gain --;    
      if(spc_ul_gain<0)spc_ul_gain = 0;
      spc_BgSndConfig(spc_ul_gain, spc_dl_gain);
      break;
   case 1:      
      spc_ul_gain ++;
      if(spc_ul_gain>7)spc_ul_gain = 7;
      spc_BgSndConfig(spc_ul_gain, spc_dl_gain);         
      break;      
   case 2:      
      spc_dl_gain --;    
      if(spc_dl_gain<0)spc_dl_gain = 0;
      spc_BgSndConfig(spc_ul_gain, spc_dl_gain);         
      break;
   case 3:      
      spc_dl_gain ++;   
      if(spc_dl_gain>7)spc_dl_gain = 7;       
      spc_BgSndConfig(spc_ul_gain, spc_dl_gain);         
      break;    
              
   case 6:
      L1SP_Speech_On(RAT_2G_MODE);
      break;
   case 7:
      L1SP_Speech_Off();
      break;

#if 0
   //for test 4G G.711
   case 9:
      SetFrameType(SP4G_CODEC_G711);
      break;

   //for test 4G G.722
   case 10:
      SetFrameType(SP4G_CODEC_G722);
      break;

   //for test 4G G.723.1
   case 11:
      SetFrameType(SP4G_CODEC_G723_1);
      break;

   //for test 4G G.726
   case 12:
      SetFrameType(SP4G_CODEC_G726);
      break;

   //for test 4G G.729
   case 13:
      SetFrameType(SP4G_CODEC_G729);
      break;

   //for test 4G G.711 -> 4G AMR
   case 14:
      SetFrameType(SP4G_CODEC_AMR_12_2);
      break;
#endif /*#if 0*/

    case 57:
      SP4G_PSR_DL_KT_Play(300, 1300, 20000);   
      break;
    case 58:
      SP4G_PSR_DL_KT_Stop();
      break;                
    case 67:
      SP4G_PSR_UL_KT_Play(1000, 0, 20000);   
      break;
    case 68:
      SP4G_PSR_UL_KT_Stop();
      break;   
    case 69:
      KT_SetOutputVolume(100,4096*2);
      break;                   
    case 71:
      {
         SP4G_PSR_UL_Tone_Play( tone_call_waiting );  
      }
      break;
    case 72:
      {
         SP4G_PSR_UL_Tone_Stop( );  
      }
      break;  
    case 73:
      {
         SP4G_PSR_DL_Tone_Play( tone_call_waiting );  
      }
      break;
    case 74:
      {
         SP4G_PSR_DL_Tone_Stop( );  
      }
      break;        
   }
#endif   
}

#if defined(__VOLTE_SUPPORT__)  
#include "l1sp_el2_struct.h"
#if defined(__MCU_DTMF_SUPPORT__)       
void volte_Tone_bgSnd_closeHandler(void);//BGSND HISR to MED  ToDO
void volte_KT_bgSnd_closeHandler(void);//BGSND HISR to MED  ToDO
void volte_BGSND_ULHdr(void);
uint32 VolteToneGetID();
void VolteToneSetID(uint32 u32val);
bool VolteToneGetUsed();
void VolteToneSetUsed(bool bval);
#endif //#if defined(__MCU_DTMF_SUPPORT__)       
void SP_M2M_Handler(ilm_struct *ilm_ptr)
{
   switch (ilm_ptr->msg_id) {
      case MSG_ID_AUDIO_M2M_VOLTE_MAC_TIMING_INFO:
      {
         sub_SP4G_emac_timing_update(ilm_ptr->local_para_ptr);
      }
      break;                 
#if defined(__MCU_DTMF_SUPPORT__)       
      case MSG_ID_AUDIO_M2M_VOLTE_KT_BGSND_CLOSE:
      {
         void volte_KT_BgSndClose(void);
         volte_KT_BgSndClose();
      }
      break;  
      case MSG_ID_AUDIO_M2M_VOLTE_TONE_BGSND_CLOSE:
      {
         void volte_Tone_BgSndClose(void);
         volte_Tone_BgSndClose();
      }
      break;             
      case MSG_ID_AUDIO_M2M_VOLTE_DL_KT_PLAY:
      {
         //SP4G_PSR_DL_KT_Play(5000, 8000, 100000);
         ilm_PSR_DL_KT_t *local_para;
         local_para = (ilm_PSR_DL_KT_t *)(ilm_ptr->local_para_ptr);
         KT_Play(local_para->freq1, local_para->freq2, local_para->duration, DSP_TONE);
      }
      break;
      case MSG_ID_AUDIO_M2M_VOLTE_DL_KT_STOP:      
      {
         //SP4G_PSR_DL_KT_Stop();
		 KT_Stop(DSP_TONE);
      }     
      break;  
      case MSG_ID_AUDIO_M2M_VOLTE_UL_KT_PLAY:
      {
         //SP4G_PSR_UL_KT_Play(5000, 8000, 100000);
         ilm_PSR_DL_KT_t *local_para;
         local_para = (ilm_PSR_DL_KT_t *)(ilm_ptr->local_para_ptr);
         if( KAL_FALSE == VolteToneGetUsed() ){	
            KT_Play(local_para->freq1, local_para->freq2, local_para->duration, MCU_TONE);
            VolteToneSetID( EXT_BGSND_Start(volte_KT_bgSnd_closeHandler, NULL, volte_BGSND_ULHdr, 7, 7) );
            VolteToneSetUsed(KAL_TRUE);
         }else{
            kal_prompt_trace(MOD_L1SP, "[SP4G_PSR_UL_KT_Play] Skip");	   
         }
      }         
      break;
      case MSG_ID_AUDIO_M2M_VOLTE_UL_KT_STOP:      
      {
         //SP4G_PSR_UL_KT_Stop();
         if( KAL_TRUE == VolteToneGetUsed() ){ 
            EXT_BGSND_Flush( VolteToneGetID() );  
            KT_Stop(MCU_TONE);	   
         }else{
            kal_prompt_trace(MOD_L1SP, "[SP4G_PSR_UL_KT_Stop] Skip");		 
         }
      }         
      break;   
      case MSG_ID_AUDIO_M2M_VOLTE_DL_TONE_PLAY:      
      {
         //SP4G_PSR_DL_Tone_Play( tone_call_waiting );
         ilm_PSR_DL_TONE_t *local_para;
         local_para = (ilm_PSR_DL_TONE_t *)(ilm_ptr->local_para_ptr);
		 TONE_Play(local_para->pToneList, DSP_TONE);
      }         
      break;   
      case MSG_ID_AUDIO_M2M_VOLTE_DL_TONE_STOP:      
      {
         //SP4G_PSR_DL_Tone_Stop();
		 TONE_Stop(DSP_TONE);
      }         
      break;   
      case MSG_ID_AUDIO_M2M_VOLTE_UL_TONE_PLAY:      
      {
         //SP4G_PSR_UL_Tone_Play( tone_call_waiting );
         ilm_PSR_DL_TONE_t *local_para;
         local_para = (ilm_PSR_DL_TONE_t *)(ilm_ptr->local_para_ptr);
         if( KAL_FALSE == VolteToneGetUsed() ){    
            TONE_Play(local_para->pToneList, MCU_TONE);
            VolteToneSetID( EXT_BGSND_Start(volte_Tone_bgSnd_closeHandler, NULL, volte_BGSND_ULHdr, 7, 7) );
            VolteToneSetUsed(KAL_TRUE);
         }else{
            kal_prompt_trace(MOD_L1SP, "[SP4G_PSR_UL_Tone_Play] Skip"); 	  
         }
      }         
      break;   
      case MSG_ID_AUDIO_M2M_VOLTE_UL_TONE_STOP:      
      {
         //SP4G_PSR_UL_Tone_Stop();
         if( KAL_TRUE == VolteToneGetUsed() ){	
            EXT_BGSND_Flush( VolteToneGetID() ); 
            TONE_Stop(MCU_TONE);	  
         }else{
            kal_prompt_trace(MOD_L1SP, "[SP4G_PSR_UL_Tone_Stop] Skip"); 	   
         }
      }         
      break;   
#endif             
   }
}
#endif     


/*****************************************************************************
 End of File
*****************************************************************************/

