/*****************************************************************************
*  Copyright Statement: 
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * afe2.c
 *
 * Project:
 * --------
 *   Maui_sw
 *
 * Description:
 * ------------
 *   Audio Front End
 *
 * Author:
 * -------
 * Phil Hsieh
 *
 *------------------------------------------------------------------------------
 * $Revision:   1.20  $
 * $Modtime:   Aug 15 2005 14:00:06  $
 * $Log:   //mtkvs01/vmdata/Maui_sw/archives/mcu/l1audio/afe2.c-arc  $
 *
 * 07 03 2014 fu-shing.ju
 * [MOLY00071405] Merge K2 Speech DVT code
 * Merge K2 Speech DVT code.
 *
 *
 *    Rev 1.0   Dec 31 2004 11:29:16   BM
 * Initial revision.
 *
 *******************************************************************************/
// TODO: c2k 
// [busyWait Delay]
// ~done [AM] 
// [check hisr]
// [trace]
// ~done [chip init]
// ~done [chip event handler]

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        INCLUDE FILES
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/


#include "monids.h"
#include "monapi.h"
#include "sysdefs.h"
#include "sysapi.h"
#include "hwdafe.h"
#include "hwdcommon_def.h"
#include "hwdam.h"
#include "hwdaudioservice.h"
#include "hwdspherr.h"
//#define ASSERT(expr) assert(expr)

typedef enum{
	HWD_SPH_AFE_VOLUME_TYPE_DIGI_DL = 0 ,
	HWD_SPH_AFE_VOLUME_TYPE_DIGI_DL_ENH,
	HWD_SPH_AFE_VOLUME_TYPE_DIGI_UL
} HWD_SPH_AFE_VOLUME_TYPE_T;

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        Global Variables
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/
AFE_STRUCT_T afe;
const unsigned short default_bt_pcm_out_vol = 0x1000; // 0dB
const unsigned short default_bt_pcm_out_enh_ref_vol = 0x1000; // 0db
const unsigned short   DG_DL_Speech    = 0x1000;
const unsigned short   DG_Microphone   = 0x1400;
const unsigned short   DG_DAF          = 32767;


//=============================================================================================
//                  AFE Unit Test
//=============================================================================================
#if 0 // UNIT_TEST_AFE2
kal_uint16 digital_gain_ut[129];
float digital_gain_ut_diff[128];
kal_uint16 get_digital_gain(kal_int8 index)
{
   AFE_SetOutputVolume(0, 0, index);
   return afe.aud[0].digital_gain;
}
void afe2_unit_test( void )
{
   kal_int8 digital_gain_index;

   for(digital_gain_index=-128; digital_gain_index<=0; digital_gain_index++) {
      digital_gain_ut[digital_gain_index+128] = get_digital_gain(digital_gain_index);
   }

   /// python cmd: 20 * math.log10(0x8000/21.0) = 63.86dB
   ASSERT(digital_gain_ut[0] == 21 , HWDSPH_ERR_FORCE_ASSERT, 0);
   ASSERT(digital_gain_ut[128] == 0x8000 , HWDSPH_ERR_FORCE_ASSERT, 0);

   for(digital_gain_index=-128; digital_gain_index<=-1; digital_gain_index++) {
      digital_gain_ut_diff[digital_gain_index+128] =
         digital_gain_ut[digital_gain_index+128+1] * 1.0F /
         digital_gain_ut[digital_gain_index+128];
      /// perfect value is math.pow(10.0, 0.5/20.0) = 1.05925
      ASSERT(digital_gain_ut_diff[digital_gain_index+128] >= 1.035F , HWDSPH_ERR_FORCE_ASSERT, 0);
      ASSERT(digital_gain_ut_diff[digital_gain_index+128] <= 1.084F , HWDSPH_ERR_FORCE_ASSERT, 0);
   }

   /// shall assert here
   get_digital_gain(1);
}
#endif

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                        AFE Internal Utility Functions
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/
uint16 regGet16WithLog(volatile uint16 *regAddr)
{
	MonTrace(MON_CP_HWD_SPH_REG_READ16_TRACE_ID, 1, regAddr);
	return 0;
}

uint32 regGet32WithLog(volatile uint32 *regAddr)
{
	MonTrace(MON_CP_HWD_SPH_REG_READ32_TRACE_ID, 1, regAddr);
	return 0;
}

// static void _update_digital_gain(kal_int16 v_lowest, kal_int16 a_lowest);

/* ========================================================================= */
/*   AFE Event Handler                                                  */
/*   This function runs under the context of L1Audio Task                    */
/* ========================================================================= */
static void AFE_Event_Handler( void *data )
{
	// TODO: c2k [check hisr]
   //ASSERT(!kal_if_hisr());

	// ~done TODO: c2k [chip event handler]
   AFE_Chip_Event_Handler(data);
}
void AFE_Init_status(bool flag)
{
   afe.afe_init = flag;
}

//=============================================================================================
//                  AFE Init
//=============================================================================================
/*****************************************************************************
* FUNCTION
*  AFE_Init
* DESCRIPTION
*   This function is to initialize the AFE module.
*
* PARAMETERS
*  None
* RETURNS
*  None
* CALLER
*  L1Audio_Task, L1Audio_ResetDevice
* GLOBALS AFFECTED
*  None
*****************************************************************************/
void AFE_Init( void )
{
   static bool afe_initialized = FALSE;
   int I;

   // RB_INIT( afe.regq );

   afe.refresh          = FALSE;
#if 0 // def ANALOG_AFE_PATH_EXIST		
   afe.sp_flag          = 0;
   afe.fir_flag         = 0;		
   afe.mic_flag         = 0;
#else
	afe.pathWork_flag        =0;
	afe.digitOnly_mic_volume = DG_DL_Speech; // 0db
#endif // ANALOG_AFE_PATH_EXIST 
	afe.digitOnly_sphEnhRef_digital_gain = DG_DL_Speech; // 0db   
#if 0
	afe.mic_src          = 0;	
	afe.mic_volume       = 7;        //analog gain value
   afe.mic_mute         = FALSE;
	afe.sidetone_volume  = 127;
   afe.sidetone_disable = FALSE;
	afe.accessory_flag   = FALSE;
#endif
   
   afe.ext_op_on        = FALSE;
   afe.ext_op_delay     = 0;
   // afe.mute             = FALSE;
   afe.loopback         = FALSE;
   afe.bt_flag          = FALSE;
   afe.audio_fs         = 0;  //32K

#if 0 // defined(EXT_DAC_SUPPORT)
   afe.ext_dac_i2s_on   = FALSE;
   afe.ext_dac_add_st   = FALSE;
   afe.ext_dac_aud_func = -1;
   afe.ext_dac_level_gain  = 0x8000;
   EXT_DAC_Init();   
#endif       

   for( I = 0; I < L1SP_MAX_AUDIO; I++ ) {
      afe.aud[I].out_dev      = 0;
      afe.aud[I].volume       = 0x77;
#if 0 // def ANALOG_AFE_PATH_EXIST		
      afe.aud[I].digital_gain = 0x8000;  // (in 1.15 fixed-point) 0 dB
      afe.aud[I].digital_gain_index = 0;
      afe.aud[I].digital_gain_level = 100;
#else 
		afe.aud[I].digitOnly_digital_gain = 4096; // 0db. 
#endif // ANALOG_AFE_PATH_EXIST
      afe.aud[I].mute         = FALSE;

   }
	
   afe.afe_state                 = AFE_STATE_OFF;
   afe.toneLoopbackRec           = FALSE;

   if (FALSE == afe_initialized) {
      afe.aud_id = L1Audio_GetAudioID();
      L1Audio_SetEventHandler( afe.aud_id, AFE_Event_Handler );
      afe_initialized = TRUE;      
   }
#if 0 // UNIT_TEST_AFE2
   {
      afe2_unit_test();
   }
#endif

#if (defined(MT6595) || defined(MT6752)) && (MDAFE_DVT_CASE1 || APAUDSYS_DVT_32K)
	afe.voice8kMode = 2;
#elif (defined(MT6595) || defined(MT6752)) && (MDAFE_DVT_CASE3 || APAUDSYS_DVT_8K) // 8k
	afe.voice8kMode = 0;
#else
	afe.voice8kMode = 1; // default value
#endif
	// ~done TODO: c2k [chip init]
   AFE_Chip_Init();
}

uint8 AFE_GetAfeSate(void)
{
   return afe.afe_state;
}

#if 0 //def ANALOG_AFE_PATH_EXIST		
void SearchSpkFlag( int16 *v_lowest, int16 *a_lowest )
{
   int16 I, val;

	/*
   if(afe.mute)
   {
      *v_lowest = -1; 
      *a_lowest = -1;  
      return;
   }   
   */
   for( I = L1SP_MAX_AUDIO; I >= 0; I-- ) {
      if( afe.sp_flag & (1<<I) ) {
         val = (uint16)afe.aud[I].out_dev;
         if( val & (L1SP_BUFFER_0|L1SP_BUFFER_1) )
            *v_lowest = I;
         if( val & L1SP_BUFFER_ST )
            *a_lowest = I;
      }
   }
   
   // move _update_digital_gain here for APM 
   _update_digital_gain(*v_lowest, *a_lowest); 
   
   if( *a_lowest >= 0 )
      if( afe.aud[*a_lowest].volume == 0 || afe.aud[*a_lowest].mute == TRUE )
      {
         *a_lowest = -1;
         AM_DSP_SetAudioDigitalGain(0); // *DP_DigiGain_Setting = 0;
         AM_DSP_SetSpeechDigitalGain(0); //*DP_VOL_OUT_PCM = 0; 
         AM_DSP_SetSpeechEnhRefDigitalGain(DG_DL_Speech);
      }
   if( *v_lowest >= 0 )
      if( afe.aud[*v_lowest].volume == 0 || afe.aud[*v_lowest].mute == TRUE )
         *v_lowest = -1;
   if (!if_lisr())
		kal_trace(TRACE_STATE, AFE_DIGI_GAIN, AM_DSP_GetSpeechDigitalGain(), AM_DSP_GetAudioDigitalGain(), AM_GetSpeechEnhRefDigitalGain());
}
#else 

void SearchPathWorkingFlag(int16 * v_lowest,int16 * a_lowest)
{
	int16 I;

	*v_lowest = -1; 
	*a_lowest = -1;

	/*
	if(afe.mute)
   {
      return;
   }   
   */

	for( I = L1SP_MAX_AUDIO; I >= 0; I-- ) {
		if(afe.pathWork_flag & (1<<I)){
			switch (I){
				case L1SP_KEYTONE:
				case L1SP_TONE:
				case L1SP_SPEECH:
		      case L1SP_SND_EFFECT:
		      case L1SP_VOICE:
				case L1SP_DAI:
		      case L1SP_LINEIN:
				{
			
					*v_lowest = I;
					break;
		      }
				case L1SP_AUDIO:
				{
		      	*a_lowest = I;
					break;
				}
			}
		}
   }
}
#endif 

void AFE_SetRefresh( void )
{
   afe.refresh = TRUE;
}

// TODO: c2k [busyWait Delay]
void AFE_DELAY(uint16 delay)
{
	/*
   uint32 curr_frc, latest_us;

   curr_frc = ust_get_current_time();//unit: micro second (us)
   do{
      latest_us = ust_get_current_time();;
   }while(delay > ust_get_duration(curr_frc, latest_us));
   */
}

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                       Chapter: Digital/Analog Gain Related
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

//=============================================================================================
//                  Section: [Gain] Digital Gain Convert
//=============================================================================================


const uint16 digitOnly_quater_dB_tableForSpeech[296] = 
{
	10289, 9997, 9713, 9438, /* 8  ~ 7.25 dB*/
	9170, 8910, 8657, 8411, /* 7   ~ 6.25 dB*/
	8173, 7941, 7715, 7497, /* 6   ~ 5.25 dB*/
	7284, 7077, 6876, 6681, /* 5   ~ 4.25 dB*/
	6492, 6308, 6129, 5955, /* 4   ~ 3.25 dB*/
	5786, 5622, 5462, 5307, /* 3   ~ 2.25 dB*/
	5120, 5010, 4868, 4730, /* 2   ~ 1.25 dB*/    // uplink begin (2 db == 5120 == 0x1400)
	4596, 4465, 4339, 4216, /* 1   ~ 0.25 dB*/
	4096, 3980, 3867, 3757, /* 0   ~ -0.75   dB*/ //  downlink begin (0 db == 4096 == 0x1000) ==> chip newer then MT6572
	3645, 3547, 3446, 3349, /* -1  ~ -1.75   dB*/ // downlink begin (-1db == 3645 == 0xE3D) ==> Old chip MT6280 & MT6589 
	3254, 3161, 3072, 2984, /* -2  ~ -2.75   dB*/
	2900, 2817, 2738, 2660, /* -3  ~ -3.75   dB*/
	2584, 2511, 2440, 2371, /* -4  ~ -4.75   dB*/
	2303, 2238, 2175, 2113, /* -5  ~ -5.75   dB*/
	2053, 1995, 1938, 1883, /* -6  ~ -6.75   dB*/
	1830, 1778, 1727, 1678, /* -7  ~ -7.75   dB*/
	1631, 1584, 1539, 1496, /* -8  ~ -8.75   dB*/
	1453, 1412, 1372, 1333, /* -9  ~ -9.75   dB*/
	1295, 1259, 1223, 1188, /* -10 ~ -10.75  dB*/
	1154, 1122, 1090, 1059, /* -11 ~ -11.75  dB*/
	1029, 1000, 971 , 944 , /* -12 ~ -12.75  dB*/
	917 , 891 , 866 , 841 , /* -13 ~ -13.75  dB*/
	817 , 794 , 772 , 750 , /* -14 ~ -14.75  dB*/
	728 , 708 , 688 , 668 , /* -15 ~ -15.75  dB*/
	649 , 631 , 613 , 595 , /* -16 ~ -16.75  dB*/
	579 , 562 , 546 , 531 , /* -17 ~ -17.75  dB*/
	516 , 501 , 487 , 473 , /* -18 ~ -18.75  dB*/
	460 , 447 , 434 , 422 , /* -19 ~ -19.75  dB*/
	410 , 398 , 387 , 376 , /* -20 ~ -20.75  dB*/
	365 , 355 , 345 , 335 , /* -21 ~ -21.75  dB*/
	325 , 316 , 307 , 298 , /* -22 ~ -22.75  dB*/
	290 , 282 , 274 , 266 , /* -23 ~ -23.75  dB*/
	258 , 251 , 244 , 237 , /* -24 ~ -24.75  dB*/
	230 , 224 , 217 , 211 , /* -25 ~ -25.75  dB*/
	205 , 199 , 194 , 188 , /* -26 ~ -26.75  dB*/
	183 , 178 , 173 , 168 , /* -27 ~ -27.75  dB*/
	163 , 158 , 154 , 150 , /* -28 ~ -28.75  dB*/
	145 , 141 , 137 , 133 , /* -29 ~ -29.75  dB*/
	130 , 126 , 122 , 119 , /* -30 ~ -30.75  dB*/
	115 , 112 , 109 , 106 , /* -31 ~ -31.75  dB*/
	103 , 100 , 97  , 94  , /* -32 ~ -32.75  dB*/
	92  , 89  , 87  , 84  , /* -33 ~ -33.75  dB*/
	82  , 79  , 77  , 75  , /* -34 ~ -34.75  dB*/
	73  , 71  , 69  , 67  , /* -35 ~ -35.75  dB*/
	65  , 63  , 61  , 60  , /* -36 ~ -36.75  dB*/
	58  , 56  , 55  , 53  , /* -37 ~ -37.75  dB*/
	52  , 50  , 49  , 47  , /* -38 ~ -38.75  dB*/
	46  , 45  , 43  , 42  , /* -39 ~ -39.75  dB*/
	41  , 40  , 39  , 38  , /* -40 ~ -40.75  dB*/
	37  , 35  , 34  , 33  , /* -41 ~ -41.75  dB*/
	33  , 32  , 31  , 30  , /* -42 ~ -42.75  dB*/
	29  , 28  , 27  , 27  , /* -43 ~ -43.75  dB*/
	26  , 25  , 24  , 24  , /* -44 ~ -44.75  dB*/
	23  , 22  , 22  , 21  , /* -45 ~ -45.75  dB*/
	21  , 20  , 19  , 19  , /* -46 ~ -46.75  dB*/
	18  , 18  , 17  , 17  , /* -47 ~ -47.75  dB*/
	16  , 16  , 15  , 15  , /* -48 ~ -48.75  dB*/
	15  , 14  , 14  , 13  , /* -49 ~ -49.75  dB*/
	13  , 13  , 12  , 12  , /* -50 ~ -50.75  dB*/
	12  , 11  , 11  , 11  , /* -51 ~ -51.75  dB*/
	10  , 10  , 10  , 9   , /* -52 ~ -52.75  dB*/
	9   , 9   , 9   , 8   , /* -53 ~ -53.75  dB*/
	8   , 8   , 8   , 7   , /* -54 ~ -54.75  dB*/
	7   , 7   , 7   , 7   , /* -55 ~ -55.75  dB*/
	6   , 6   , 6   , 6   , /* -56 ~ -56.75  dB*/
	6   , 6   , 5   , 5   , /* -57 ~ -57.75  dB*/
	5   , 5   , 5   , 5   , /* -58 ~ -58.75  dB*/
	5   , 4   , 4   , 4   , /* -59 ~ -59.75  dB*/
	4   , 4   , 4   , 4   , /* -60 ~ -60.75  dB*/
	4   , 4   , 3   , 3   , /* -61 ~ -61.75  dB*/
	3   , 3   , 3   , 3   , /* -62 ~ -62.75  dB*/
	3   , 3   , 3   , 3   , /* -63 ~ -63.75  dB*/
	3   , 3   , 2   , 2   , /* -64 ~ -64.75  dB*/
	2   , 2   , 2   , 2   , /* -65 ~ -65.75  dB*/
};

/**
	Only use when MODEM do not have analog device, ie. ANALOG_AFE_PATH_EXIST is NOT defined

	@digitalGainQDb: input digital gain value in dB. 4 step/1db. 
	  for example: digitalGainDb=1  --> 0.25 dB; digitalGainDb=FFF4(-12) --> -3dB
	@isDL: true for downlink, false for uplink
	@return: value set to dsp
	  
*/
uint16 _digitOnly_convert_digital_gain_forSpeech(int16 digitalGainQDb, bool isDl)
{
	int16 idx = (-digitalGainQDb);

	if(isDl){
#if defined(MT6589)		
		idx += 36; // begin from -1 db 
#else // data card chip should begin from 0
		idx += 32; // begin from 0 db 
#endif
	} else {
		// for UL. useless now
	}
	
	return digitOnly_quater_dB_tableForSpeech[idx];
	
}

void _digitOnly_update_digital_gain(int16 v_lowest, int16 a_lowest)
{
#if defined(__BT_SUPPORT__) // DSP_BT_SUPPORT
   if(AM_IsBluetoothOn()) // for the case of bluetooth use
   {
   	// don't care audio function 

		// speech function
      if( afe.aud[L1SP_VOICE].mute == TRUE || afe.aud[L1SP_SPEECH].mute == TRUE ) {
			// ~ done TODO: c2k [AM] 
         AM_DSP_SetSpeechDigitalGain(0); //*DP_VOL_OUT_PCM = 0;
         AM_DSP_SetSpeechEnhRefDigitalGain(default_bt_pcm_out_enh_ref_vol); // truly 0db in DSP
      }
      else
      {
      	// ~ done TODO: c2k [AM] 
			AM_DSP_SetSpeechDigitalGain(default_bt_pcm_out_vol);
         AM_DSP_SetSpeechEnhRefDigitalGain(default_bt_pcm_out_enh_ref_vol); // truly 0db in DSP
      }

		// don't care line-in function, which is phase out
      return;
   }
#endif //defined(__BT_SUPPORT__) // DSP_BT_SUPPORT

   if (v_lowest >= 0) {
      if (a_lowest >= 0) // Voice Amp ON && Audio Amp ON
      {
         ASSERT(0 , HWDSPH_ERR_FORCE_ASSERT, 0); // should not have audio function
         // AM_DSP_SetAudioDigitalGain(afe.aud[a_lowest].digitOnly_digital_gain );
      } 
      else  //Voice Amp ON && Audio Amp OFF
      {
      	if(TRUE == afe.aud[v_lowest].mute){
				// ~ done TODO: c2k [AM] 
				AM_DSP_SetSpeechDigitalGain(0);
				AM_DSP_SetSpeechEnhRefDigitalGain(DG_DL_Speech); // default is 0db from ap side
      	} else {
      		// ~ done TODO: c2k [AM] 
	         AM_DSP_SetSpeechDigitalGain(afe.aud[v_lowest].digitOnly_digital_gain );
				AM_DSP_SetSpeechEnhRefDigitalGain(afe.digitOnly_sphEnhRef_digital_gain);
      	}
      }
   } 
   else if (a_lowest >= 0)   // Voice Amp OFF && Audio Amp ON
   {
      ASSERT(0 , HWDSPH_ERR_FORCE_ASSERT, 0); // should not have audio function

		// AM_DSP_SetAudioDigitalGain(afe.aud[a_lowest].digitOnly_digital_gain );
   } 
   else  // Voice Amp OFF && Audio Amp OFF; Restore the values to default value (for Bluetooth)
   {
       // ~done TODO: c2k [AM] 
		 AM_DSP_SetSpeechDigitalGain(DG_DL_Speech); //*DP_VOL_OUT_PCM = DG_DL_Speech;
       AM_DSP_SetSpeechEnhRefDigitalGain(DG_DL_Speech); // default is 0db from ap side
       AM_DSP_SetAudioDigitalGain(DG_DAF); // *DP_DigiGain_Setting = DG_DAF;
   }

	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 1133,0,3311);
	// TODO: c2k [check hisr] [trace]
   //if (!kal_if_lisr())
		// kal_trace(TRACE_STATE, AFE_DIGI_GAIN, AM_DSP_GetSpeechDigitalGain(), AM_DSP_GetAudioDigitalGain(), AM_GetSpeechEnhRefDigitalGain());
}


//=============================================================================================
//                  Section: [Gain] Digital Gain Setting  (Set/Get/Mute)
//=============================================================================================

/**
	@aud_func: 
	@digitalGainQdB: input digital gain value in quarter dB, i.e. 4 step/1db. 
	  for example: digitalGainDb=1  --> 0.25 dB; digitalGainDb=FFF4(-12) --> -3dB
*/
void AFE_DigitalOnly_SetDigitalGain(uint8 aud_func, int16 digitalGainQdB)
{	
	// done TODO: c2k [trace]
   // kal_trace(TRACE_STATE, AFE_SET_GIDI_GAIN, aud_func, digitalGainQdB);
   MonTrace(MON_CP_HWD_SPH_AFE_VOLUME_SET_TRACE_ID, 3, HWD_SPH_AFE_VOLUME_TYPE_DIGI_DL, aud_func, digitalGainQdB);
	ASSERT((digitalGainQdB)<=0 && (digitalGainQdB>=-256) , HWDSPH_ERR_FORCE_ASSERT, 0); // value frome -64 db to 0 db
	
	if ((L1SP_AUDIO != aud_func)
		&& (L1SP_DAI != aud_func)
		&&(L1SP_LINEIN != aud_func)) {
		afe.aud[aud_func].digitOnly_digital_gain = _digitOnly_convert_digital_gain_forSpeech(digitalGainQdB, true); 
	} else {
		// do nothing		
	}
	
	L1Audio_SetEvent(afe.aud_id, NULL);
}


/**

	@digitalGainQdB: input digital gain value in quarter dB, i.e. 4 step/1db. 
	  for example: digitalGainDb=1  --> 0.25 dB; digitalGainDb=FFF4(-12) --> -3dB
*/
void AFE_DigitalOnly_SetEnhRefDigitalGain(int16 refDigitalGainQdB)
{
	// done TODO:  c2k [trace]
	// kal_trace(TRACE_STATE, AFE_SET_SPH_REF_GIDI_GAIN, refDigitalGainQdB);
   MonTrace(MON_CP_HWD_SPH_AFE_VOLUME_SET_TRACE_ID, 3, HWD_SPH_AFE_VOLUME_TYPE_DIGI_DL_ENH, 0, refDigitalGainQdB);
	ASSERT((refDigitalGainQdB)<=0 && (refDigitalGainQdB>=-28) , HWDSPH_ERR_FORCE_ASSERT, 0); // value from -7 db to 0 db
	
	afe.digitOnly_sphEnhRef_digital_gain = _digitOnly_convert_digital_gain_forSpeech(refDigitalGainQdB, true);
	
	L1Audio_SetEvent(afe.aud_id, NULL);
}


//=============================================================================================
//                  Section: [Gain] Device Gain (Mute/Output Volume)
//=============================================================================================

void AFE_DigitalOnly_SetMicrophoneVolume( uint8 mic_volume )
{
	// done TODO: c2k [trace]
	// kal_brief_trace(TRACE_STATE, AFE_SET_MIC_VOLUME, mic_volume);
	MonTrace(MON_CP_HWD_SPH_AFE_VOLUME_SET_TRACE_ID, 3, HWD_SPH_AFE_VOLUME_TYPE_DIGI_UL, 0, mic_volume);
	ASSERT((mic_volume<=120) , HWDSPH_ERR_FORCE_ASSERT, 0); // range is 0~ 30 dB


	afe.digitOnly_mic_volume = (mic_volume>>2); // 4 is 1 db 
	L1Audio_SetEvent(afe.aud_id, NULL);
}

void AFE_MuteSpeaker( uint8 aud_func, bool mute )
{
   afe.aud[aud_func].mute = mute;
	// TODO: c2k [trace]
	// kal_trace(TRACE_STATE, AFE_MUTE_SPEAKER, aud_func, mute);
   AFE_SetRefresh();
}

bool AFE_IsSpeakerMuted( uint8 aud_func)
{
   return afe.aud[aud_func].mute;
}


void AFE_DigitalOnly_GetOutputVolume(uint8 aud_func, uint8 *vol)
{
	*vol = afe.aud[aud_func].digitOnly_digital_gain;
}


/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                       Chapter: Path/Device Control
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

/*****************************************************************************
* FUNCTION
*   AFE_TurnOnPathWork
* DESCRIPTION
*   This function is to turn on the working path of an audio function.
*
* PARAMETERS
*  aud_func - the audio function
* RETURNS
*  None
* CALLER
*  Task
* GLOBALS AFFECTED
*  None
*****************************************************************************/
#ifndef ANALOG_AFE_PATH_EXIST // suing when analog path is NOT exiting

void AFE_TurnOnPathWork( kal_uint8 aud_func )
{
	// kal_uint32 savedMask;
	SysIntDisable(SYS_IRQ_INT);  // savedMask = SaveAndSetIRQMask(); 	// Disable interrupt to prevent race condition
	afe.pathWork_flag|= (1<<aud_func);
	SysIntEnable(SYS_IRQ_INT);   //RestoreIRQMask(savedMask);

	// AFE_SetRefresh();
	L1Audio_SetEvent(afe.aud_id, NULL);

	///Currently, when video recording, video LISR will call Media_Record.
	// TODO: c2k [trace]
	// L1Audio_Msg_AFE_TurnPath( L1AUDIO_Str_onoff(1), L1AUDIO_Func_Name(aud_func) );
}
#endif

/*****************************************************************************
* FUNCTION
*  AFE_TurnOffPathWork
* DESCRIPTION
*   This function is to turn off the working path of an audio function.
*
* PARAMETERS
*  aud_func - the audio function
* RETURNS
*  None
* CALLER
*  Task
* GLOBALS AFFECTED
*  None
*****************************************************************************/
#ifndef ANALOG_AFE_PATH_EXIST // suing when analog path is NOT exiting
void AFE_TurnOffPathWork( uint8 aud_func )
{
   // uint32 savedMask;
   /// Disable interrupt to prevent race condition
   SysIntDisable(SYS_IRQ_INT); // savedMask = SaveAndSetIRQMask();
   afe.pathWork_flag &= ~(1<<aud_func);
	SysIntEnable(SYS_IRQ_INT);  //  RestoreIRQMask(savedMask);
   
   // AFE_SetRefresh();
	L1Audio_SetEvent(afe.aud_id, NULL);
	
   ///Currently, when video recording, video LISR will call Media_Record.
   // TODO: c2k [trace]
	//L1Audio_Msg_AFE_TurnPath( L1AUDIO_Str_onoff(0), L1AUDIO_Func_Name(aud_func) );
}
#endif


//=============================================================================================
//                  Section: [Path] BT Functions
//=============================================================================================
void AFE_SetBtFlag(bool on)
{
	afe.bt_flag = on;
}

//=============================================================================================
//                  Section: [Path] DAI Related Functions
//=============================================================================================
void AFE_TurnOnDAI( void )
{
	 ASSERT(0 , HWDSPH_ERR_FORCE_ASSERT, 0);//it shouldn't be used
   //*AFE_VDB_CON |= 0x0020;
   //L1Audio_Msg_AFE_Switch( L1AUDIO_Str_onoff(1), AFE_Switch_Name(1) );
}

void AFE_TurnOffDAI( void )
{
	 ASSERT(0 , HWDSPH_ERR_FORCE_ASSERT, 0);//it shouldn't be used
   //*AFE_VDB_CON &= ~0x0020;
   //L1Audio_Msg_AFE_Switch( L1AUDIO_Str_onoff(0), AFE_Switch_Name(1) );
}



//=============================================================================================
//                  Section: [Path] Input/Output Source/Device Related
//=============================================================================================
/*****************************************************************************
* FUNCTION
*  AFE_SetOutputDevice
* DESCRIPTION
*   This function is to set the output device of an audio function.
*
* PARAMETERS
*  aud_func - the audio function
*  device - be L1SP_SPEAKER1, L1SP_SPEAKER2, or L1SP_LOUDSPEAKER
* RETURNS
*  None
* GLOBALS AFFECTED
*  None
*****************************************************************************/

uint8 AFE_GetOutputDevice( uint8 aud_func )
{
   return afe.aud[aud_func].out_dev;
}

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                       Chapter: Audio/Speech Rleated Features
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                       Chapter: AFE Related Features
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/

//=============================================================================================
//                  Section: [AFE Feature] ABB Registers Backup/Store (for 65nm projects)
//=============================================================================================
void AFE_RegisterBackup(void)
{
#if __RELOAD_HW_COEFF__
	// TODO: c2k [trace]
   // L1Audio_Msg_AFE_RegBackup( L1AUDIO_Str_Bool(1) );
   
   _AfeRegisterBackupByChip();
#endif
}

void AFE_RegisterStore(void)
{
#if __RELOAD_HW_COEFF__
	// TODO: c2k [trace]
   // L1Audio_Msg_AFE_RegBackup( L1AUDIO_Str_Bool(0) );
   
   _AfeRegisterStoreByChip();
#endif
}

//=============================================================================================
//                  Section: [AFE Feature] AFE Loop back
//=============================================================================================
void AFE_EnableToneLoopBackFlag( bool param )
{
   if(param)
      afe.toneLoopbackRec = TRUE;
   else
      afe.toneLoopbackRec = FALSE;
}

bool AFE_GetLoopbackStatus( void )
{
   return afe.loopback;
}

