/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _HWD_SIDETONE_H
#define _HWD_SIDETONE_H

// ----------------------------------------------------------------------------

typedef enum {
	SIDETONE_SOLUTION_VER_UNDEF = 0,
	SIDETONE_SOLUTION_VER_HW,
	SIDETONE_SOLUTION_VER_DSP_SW	
	
}SIDETONE_SOLUTION_VER;


// ----------------------------------------------------------------------------

#if defined(MTK_PLT_AUDIO) // ((defined(MT6592) || defined(MT6571) || defined(MT6595) || defined(MT6752)) && defined(__SMART_PHONE_MODEM__))

#define SMART_PHONE_SIDETONE_SOLUTION_VER SIDETONE_SOLUTION_VER_HW
#else //((defined(MT6589) || defined(MT6572) || defined(MT6582) ) using sw sidetone

#define SMART_PHONE_SIDETONE_SOLUTION_VER SIDETONE_SOLUTION_VER_DSP_SW
#endif

// ----------------------------------------------------------------------------
//=========================== call from spc_drv.c ============================
void SIDETONE_UpdateStart(void);
void SIDETONE_UpdateStop(void);

void SIDETONE_SetSolutionVer(SIDETONE_SOLUTION_VER solutionVer);
void SIDETONE_ResetSolutionVer(void);


/**
	@vol: value from ap
*/
void SIDETONE_SetExtStVolume(uint16 vol);


//=============================================================================

/**
	Please use this function during DSP power on. (The function may access sherif)
	@on: ture for turning on, false for turing off.
*/
void SIDETONE_TurnSwSidetoneOn(bool on);


void SIDETONE_Init(void);

#endif //_HWD_SIDETONE_H

