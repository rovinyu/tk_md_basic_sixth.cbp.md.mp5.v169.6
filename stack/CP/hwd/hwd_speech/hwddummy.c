/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "Exeapi.h"
#include "Dspvapi.h"
#include "Valsndapi.h"
#include "Hwdaudio.h"
#include "Hwdappsapi.h"

#define true         (bool)(1==1)
#define false        (bool)(1==0)

// Dummy Function form Hwdmmapps.c
ExeHisrT HwdMMAppsHisrCb;

IpcSpchSrvcOptT HwdMMAppsGetVocoder (void)
{
	return IPC_SPCH_SRVC_OPTION_NULL;
}

void HwdMMAppsVocoderDnld (IpcSpchSrvcOptT SrvcOpt, bool ForceDnld)
{
}

HwdMMAppsDspvStateT HwdMMAppsGetDspvState (void)
{
	return HWD_MMAPPS_DSPV_NOT_INITIALIZED;
}

void HwdMMAppsResetVocoderApp (void)
{
}

bool HwdMMAppsGetMusicStatusSemReq (void)
{
	return false;
}

void HwdMMAppsMusicGetBufRegCB (HwdMMAppsMusicGetBufFuncT FuncP)
{
}

void HwdMMAppsMusicStatusReg (ExeTaskIdT TaskId, ExeMailboxIdT Mailbox, uint32 MsgId)
{
}

bool HwdMMAppsIsResetInProg (void)
{
	return false;
}

bool HwdMMAppsSetVocoder (IpcSpchSrvcOptT NewSrvcOpt)
{
	return false;
}



// Dummy Function from Hwdaudio.c
void HwdAudioExtAmpEnable( bool Enable )
{
}

void HwdAudioHandsetAmpEnable( bool Enable )
{
}

void HwdAudioPcEnable( bool Enable )
{
}

void HwdAudioTtyEnable( bool Enable )
{
}

void HwdAudioL1DTstGetPhoneStatus( uint16 *TxMuted, uint16 *RxMuted )
{
}
void HwdAudioDisableSidetone (void)
{
}

void HwdAudioDualMicCtrl (bool OnOff)
{
}

HwdAudioLoopbackTypeT HwdAudioGetLoopbackState (void)
{
	return HWD_AUDIO_LOOPBACK_DISABLED;
}

void HwdAudioInitTuningParams (ExeRspMsgT *RspInfoP, uint8 DataSource, char *PathName)
{
}

bool HwdAudioIsModeSupported(HwdAudioModeT Mode)
{
	return false;
}

HwdAudioModeT HwdAudioModeGet( void )
{
	return HWD_AUDIO_NUM_MODES;
}

void HwdAudioReadTuningParams (ExeRspMsgT *RspInfoP, uint8 ParamType, bool ReadDefaults)
{ 
}

void HwdAudioRingerVolumeConstant (bool VolConstant)
{
}

void HwdAudioSetModeCfgReqd (void)
{
}

void HwdAudioSetSpkrChanMute (HwdAudioChanMuteT ChanMute)
{
}

void HwdAudioStoreTuningFile (ExeRspMsgT *RspInfoP, char *PathName)
{
}

void HwdAudioToneVolSet( HwdAudioVolT Volume )
{
}

void HwdExtAmpPause (uint16 HwState)
{
}

void HwdAudioPathSet( bool Enabled, uint16 LogicalPath )
{
}

HwdAudioVolT HwdAudioVolumeGet( uint16 LogicalPath )
{
	return HWD_AUDIO_VOL_DEFAULT;
}

void HwdAudioHeadsetAmpEnable( bool Enable )
{
}

void HwdAudioLoopbackConfig(HwdAudioLoopbackConfigMsgT *MsgDataPtr)
{
}

void HwdAudioToneVolumeLow (bool VolLow)
{
}

void HwdAudioUpdateTuningParams (ExeRspMsgT *RspInfoP, uint8 ParamType, uint16 Size, uint8 Mode, uint8 *DataP)
{
}

void HwdAudioVibrateStart (uint32 VibrateTime, uint32 RestTime, int32 TotDuration)
{
}

void HwdAudioVibrateStop (void)
{
}

HwdVolumeModeT HwdAudioVolModeGet (void)
{
	return HWD_AUDIO_VOICE_VOL_MODE;
}

void HwdAudioVolumeDataSet( uint16 LogicalPath, HwdAudioVolT Volume )
{
}

void HwdAudioVolumeSet( uint16 LogicalPath, HwdAudioVolT Volume )
{
}

// Dummy Function from Hwdtone.c
bool HwdToneIsActive (void)
{
	return false;
}

void HwdToneSrvcOptDisconnecting (void)
{
	
}

bool HwdToneDtmf (ValSndDtmfIdT DtmfId, uint8 Mode, uint16 Length)
{
	return false;
}

void HwdTonePlay (ValSndToneIdT ToneId, uint8 NumIterations)
{
}

void HwdToneStop (IpcToneModeT StopMode)
{
}

#define ToneDataT ValSndToneDataT

void HwdUserTonePlay (ToneDataT *ToneDataP, uint16 NumTonesInSeq, uint16 Iterations)
{
}

// Dummy Function from Hwdappsaudio.c
void HwdAppsAudioModeSet( HwdAppsAudioDevModesT DevMode, HwdAudioModeT Mode, ExeRspMsgT *RspMsgP ) 
{
}

void HwdAppsAudioRegisterCB (HwdAppsSoundOverFuncT FuncP)
{
}






