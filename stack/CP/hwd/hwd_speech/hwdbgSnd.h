/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef __BGSNG_H__
#define __BGSNG_H__

#define BGSND_RB_DEFAULT_THRESHOLD    1024

typedef enum {
	BGSND_STATE_IDLE = 0,
	BGSND_STATE_RUN, 
	BGSND_STATE_STOP,
	BGSND_STATE_CLOSE,
}BGSND_STATE_TYPE;


int32 BGSND_GetFreeSpace(void);
void BGSND_WriteData(uint8 *srcBuf, int32 bufLen);

void BGSND_ConfigULMixer(bool bULSPHFlag, int8 ULSNDGain );
void BGSND_ConfigDLMixer(bool bDLSPHFlag, int8 DLSNDGain );
void BGSND_Start(void (*bgSnd_hisrHandler)(void), void (*bgSnd_offHandler)(void), uint8 ULGainLevel , uint8 DLGainLevel);
void BGSND_Stop(void);
void BGSND_Close(void);
void BGSND_INIT(void);
BGSND_STATE_TYPE BGSND_GetStatus(void);

#if 0 
typedef enum { 
	BGSND_DL_PROCESS = 0, //you cannot change the value since I use "1-type" to switch either.
	BGSND_UL_PROCESS = 1, 
}BGSND_PROCESS_TYPE;


typedef enum {
	EXT_SRC_STATE_IDLE = 0,
	EXT_SRC_STATE_RUN, 
	EXT_SRC_STATE_FLUSHING,
   EXT_SRC_STATE_FLUSHING_OVER,
	EXT_SRC_STATE_STOPPING,
}EXT_SRC_STATE_TYPE;


int32 EXT_BGSND_GetFreeSpace(uint32 id, BGSND_PROCESS_TYPE type);
int32 EXT_BGSND_GetDataCount(uint32 id, BGSND_PROCESS_TYPE type);
void EXT_BGSND_WriteSrcBuffer(uint32 id, kal_uint8 *srcBuf, kal_int32 bufLen, BGSND_PROCESS_TYPE type);
void EXT_BGSND_WriteExtBuffer(uint32 id, int gain, BGSND_PROCESS_TYPE type);

void EXT_BGSND_ConfigMixer(int id, kal_bool bSPHFlag, kal_int8 SNDGain, BGSND_PROCESS_TYPE type);
uint32 EXT_BGSND_Start(void (*offHdr)(void),
                       void (*DLHisr)(void),       //move data from src -> ext
                       void (*ULHisr)(void),       //move data from src -> ext    
                       kal_int8 DLSNDGain,
                       kal_int8 ULSNDGain                       
                       /*,                       
                       void (*EventHandler)(void)*/);
void EXT_BGSND_Flush(uint32 id);
void EXT_BGSND_Close(uint32 id);                       

uint32 EXT_BGSND_init();

EXT_SRC_STATE_TYPE EXT_BGSND_GetStatus(int id,  BGSND_PROCESS_TYPE enULDL);
#endif 
#endif //__BGSNG_H__
