/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "reg_base.h"
#include "hwdcommon_def.h"
//#include "kal_general_types.h"
//#include "kal_trace.h"
#include "fd216_idma.h"
#include "fd216_l1d_reg.h"
#include "fd216_l1d_reg_dsp.h"

#define DSP_PWDN                (4+8)
#define DSP_PWDN_RESET          (1+4+8)
#define DSP_PWDN_BOOT           (2+4+8)
#define DSP_PWDN_BOOT_RESET     (1+2+4+8)



const kal_char*   fd216_str_dsp_ptch_ver;
const kal_uint16* fd216_p_ena;
const kal_uint16* fd216_p_page;
const kal_uint16* fd216_p_add;
const kal_uint32* fd216_p_val;
const kal_uint32* fd216_patch_content;

extern void  get_addr_dsp_ptch( const kal_char   **ptr_str_dsp_ptch_ver, 
                              const kal_uint16 **ptr_p_ena, 
                              const kal_uint16 **ptr_p_page,
                              const kal_uint16 **ptr_p_add,
                              const kal_uint32 **ptr_p_val,
                              const kal_uint32 **ptr_patch_content );
                              
kal_uint16 FD216_idma_load_cm(kal_uint16 page, kal_uint16 address, kal_uint32 len, const kal_uint32 *image)
{
   kal_uint32 i;
   volatile kal_uint32 *cmptr, *imptr;
   
   cmptr = (volatile kal_uint32 *)(IDMA_CM0 + (page << 16) + (address << 2));//跟北京要base_address, C2K有fd216
   imptr = (volatile kal_uint32 *) image;

   //kal_prompt_trace(MOD_L1SP, "FD216_idma_load_cm(1) %x %x", cmptr, imptr);
   
   i = *imptr++; // 跟daniel,是不是patch content從來不用第一行
   for(i=1 ; i<len; i++){
      *cmptr++ = *imptr++;
   }   
   return 1;
}


                              
void FD216_idma_load_Genral(kal_bool fInit)
{
   kal_uint32 i, j;    
   kal_uint64 val;        // Modify for patch 48 entry in 6223.                    
   kal_uint16 patch_num ;   // add for assign patch number                      

/*   
   *SHARE_DSPCTL = DSP_PWDN;
   *SHARE_DSPCTL = DSP_PWDN_RESET;
   
   *SHARE_DSPCTL = DSP_PWDN_BOOT;
   *SHARE_DSPCTL = DSP_PWDN_BOOT_RESET;  因為現在 會有hardware直接trigger FD216進bootmode
*/   
   //mcu\driver\dsp_ram\dsp_ptch_6595.c

   get_addr_dsp_ptch( &fd216_str_dsp_ptch_ver, &fd216_p_ena, &fd216_p_page, &fd216_p_add, &fd216_p_val, &fd216_patch_content ); 

// *SHARE_DSPCTL = DSP_PWDN;            //在假設給電, 一些register就都被清乾淨了
// *SHARE_DSPCTL = DSP_PWDN_RESET;     

   //kal_prompt_trace(MOD_L1SP, "FD216_idma_load_Genral(1) %x %x %x %x %x %x", fd216_str_dsp_ptch_ver, fd216_p_ena, fd216_p_page, fd216_p_add, fd216_p_val, fd216_patch_content);
   
   *PATCH_EN = 0;  // 要跟SS 確認新的C2K base_address, 這個是DSP peripheral register,  //解除bootmode前,enable就可以
   j = 1;
   val = 0;
   
   patch_num = 2;//--> patch entry個數 2, MCU patch @@,多寫沒有用,後面的2-24,靠sheriff通知DSP去寫   
   for( i=0; i<patch_num; i++){
      val += fd216_p_ena[i] * (j << i);
      //kal_prompt_trace(MOD_L1SP, "FD216_idma_load_Genral(2) %x %x", val, fd216_p_ena[i]);
	  
      *((volatile kal_uint32 *)(PATCH_PAGE(i))) = fd216_p_page[i];   //要hack的位置
	  //kal_prompt_trace(MOD_L1SP, "FD216_idma_load_Genral(3) %x %x %x", *((volatile kal_uint32 *)(PATCH_PAGE(i))), ((volatile kal_uint32 *)(PATCH_PAGE(i))), fd216_p_page[i]);
	  	
      *((volatile kal_uint32 *)(PATCH_ADDR(i))) = fd216_p_add[i];     //要hack的位置
	  //kal_prompt_trace(MOD_L1SP, "FD216_idma_load_Genral(4) %x %x %x", *((volatile kal_uint32 *)(PATCH_ADDR(i))), ((volatile kal_uint32 *)(PATCH_ADDR(i))), fd216_p_add[i]);
	  
      *((volatile kal_uint32 *)(PATCH_INST_LOW(i)))  = (fd216_p_val[i] & 0x00ffff);  //跳到的位置        
  	  //kal_prompt_trace(MOD_L1SP, "FD216_idma_load_Genral(5) %x %x %x", *((volatile kal_uint32 *)(PATCH_INST_LOW(i))), ((volatile kal_uint32 *)(PATCH_INST_LOW(i))), fd216_p_val[i]);
	  
      *((volatile kal_uint32 *)(PATCH_INST_HIGH(i))) = (fd216_p_val[i] >> 16);         //跳到的位置        
	  //kal_prompt_trace(MOD_L1SP, "FD216_idma_load_Genral(6) %x %x ", *((volatile kal_uint32 *)(PATCH_INST_HIGH(i))), ((volatile kal_uint32 *)(PATCH_INST_HIGH(i))));	  
   }    
   
   if( val != 0){
      //jame request 量時間 for power on memory
      //ActionItem 改macro 2, 0x3c00, 0x400
      FD216_idma_load_cm(2, 0x3c00, 0x400, fd216_patch_content);   //為了反應夠快,所以patch放,FD216裡面
   }
}

void  FD216_idma_get_patch_pointer(  const kal_uint16 **ptr_p_ena,
                               const kal_uint16 **ptr_p_page,
                               const kal_uint16 **ptr_p_add,
                               const kal_uint32 **ptr_p_val )
{   *ptr_p_ena = fd216_p_ena;
    *ptr_p_page = fd216_p_page;
    *ptr_p_add = fd216_p_add;
    *ptr_p_val = fd216_p_val;
}

void FD216_idma_load(void)
{

   FD216_idma_load_Genral(KAL_TRUE);   
}   
