/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _L1D_REG_DSP_H_
#define _L1D_REG_DSP_H_


#define DPRAM2_CPU_base       DPRAM_CPU_base
//#define DPRAM_base(n)         (DPRAMADDR  )(DPRAM2_CPU_base+(n)*2)
#define DPRAM_base_S(n)       (DPRAMADDR_S)(DPRAM2_CPU_base+(n)*2)

#include "dpram_6735.h"				

#if 0
#if defined(MT6280)
   #include "dpram_6280.h"
#elif defined(MT6589)
	#include "dpram_6589.h"
#elif defined(MT6572)
	#include "dpram_6572.h"
#elif defined(MT6582)
	#include "dpram_6582.h"	
#elif defined(MT6290)
	#include "dpram_6290.h"		
#elif defined(MT6592)
	#include "dpram_6592.h"	
#elif defined(MT6571)
	#include "dpram_6571.h"	
#elif defined(MT6595)
	#include "dpram_6595.h"	
#elif defined(MT6752)
	#include "dpram_6752.h"				
#else 
	#error Need DSP guys give the dpram_xxxx.h file
#endif
#endif

#define DUMMY_RW_DPRAM1       (DPRAM_base(0x28000))  /* Dummy write/read dpram addr, and this addr will not be used by anyone */
#define DUMMY_RW_DPRAM2       (DPRAM_base(0x28002))  /* Dummy write/read dpram addr, and this addr will not be used by anyone */

/* --------------------------------------------------------------------------------------------------------------------- */
 //power on
#define SHARE_DSPCON          ((APBADDR)(SHAREG2_base+0x0000))   /* DSP Control Register                                 */
#define SHARE_M2DI1           ((APBADDR)(SHAREG2_base+0x0004))   /* MCU-to-DSP Interrupt1 Register                       */
#define SHARE_M2DI2           ((APBADDR)(SHAREG2_base+0x0008))   /* MCU-to-DSP Interrupt2 Register                       */

//power off    //c2k SHAREG2_base ���令�F SHARE_base
#define SHARE_D2MCON          ((APBADDR)(SHARE_base+0x000C))     /* DSP-to-MCU Interrupt Control Register                */
#define SHARE_D2MSTA          ((APBADDR)(SHARE_base+0x0010))     /* DSP-to-MCU Interrupt Status Register                 */
#define SHARE_D2MTID          ((APBADDR)(SHARE_base+0x0014))     /* DSP Task Identification Register                     */

//#define SHARE2_M2DI1          SHARE_M2DI1
//#define SHARE2_M2DI2          SHARE_M2DI2
/* --------------------------------------------------------------------------------------------------------------------- */

/*----------------------*\
|* Misc Share Registers *|
\* --------------------------------------------------------------------------------------------------------------------- */
#define SHARE_PWDNCON         ((APBADDR)(SHARE_base +0x0048))    /* CNTR POWER DOWN CONTROL                              */
#define SHARE_DSP1CKR         ((APBADDR)(SHARE_base +0x004C))    /* Slave DSP Clock Switch Register(for deep sleep mode) */
#define SHARE_DSPENCL         ((APBADDR)(SHARE_base +0x0040))    /* DSP Code Memory Encryption Key LW                    */
#define SHARE_DSPENCH         ((APBADDR)(SHARE_base +0x0044))    /* DSP Code Memory Encryption Key HW                    */
#define SHARE_DSP2OPT         ((APBADDR)(SHARE_base +0x0074))    /* Avoid DSP HW dummy read and DSP memory clock gating  */
#define SHARE_D22MCTL2        ((APBADDR)(SHARE_base +0x0078))    /* DSP2-to-MCU Interrupt Control2                       */
#define SHARE_D22MSTA2        ((APBADDR)(SHARE_base +0x007C))    /* DSP2-to-MCU Interrupt Status2                        */

#define SHARE2_PWDNCON        SHARE_PWDNCON
/* --------------------------------------------------------------------------------------------------------------------- */

/*-------------------*\
|* Task Interrupt    *|
\* --------------------------------------------------------------------------------------------------------------------- */
#define DP_D2C_TASK0          ((APBADDR)(SHARE_base+0x0050))     /* 0th FW tsk interrupt                                 */
#define DP_D2C_TASK1          ((APBADDR)(SHARE_base+0x0054))     /* 1st FW tsk interrupt                                 */
#define DP_D2C_TASK2          ((APBADDR)(SHARE_base+0x0058))     /* 2nd FW tsk interrupt                                 */
#define DP_D2C_TASK3          ((APBADDR)(SHARE_base+0x005C))     /* 3th FW tsk interrupt                                 */
#define DP_D2C_TASK4          ((APBADDR)(SHARE_base+0x0060))     /* 4th FW tsk interrupt                                 */
#define DP_D2C_TASK5          ((APBADDR)(SHARE_base+0x0064))     /* 5th FW tsk interrupt                                 */
#define DP_D2C_TASK6          ((APBADDR)(SHARE_base+0x0068))     /* 6th FW tsk interrupt                                 */
#define DP_D2C_TASK7          ((APBADDR)(SHARE_base+0x006C))     /* 7th FW tsk interrupt                                 */
/* --------------------------------------------------------------------------------------------------------------------- */



/*----------------------*\
|* SHAREG Backup Region *|
\* --------------------------------------------------------------------------------------------------------------------- */
#define SHARE1_BACKUP1_START     SHARE_D2MCON
#define SHARE1_BACKUP1_SIZE      (1)
#define SHARE1_BACKUP1_END       (SHARE1_BACKUP1_START+SHARE1_BACKUP1_SIZE)

#define SHARE1_BACKUP2_START     SHARE_DSP1CKR  /*SHARE_PWDNCON. To avoid MCU from access the addr. between SHARE_PWDNCON and SHARE_DSP1CKR*/
#define SHARE1_BACKUP2_SIZE      (1)
#define SHARE1_BACKUP2_END       (SHARE1_BACKUP2_START+SHARE1_BACKUP2_SIZE)

#define SHARE1_SIZE              (SHARE1_BACKUP1_SIZE+SHARE1_BACKUP2_SIZE)

//#define DP_DSP_STATUS                 ((DPRAMADDR)(DPRAM_CPU_base+(DP_DSP_TASK_STATUS_BASE+0x000)*2))
//#define DP_MCU_STATUS_MTCMOS          ((DPRAMADDR)(DPRAM_CPU_base+(DP_DSP_TASK_STATUS_BASE+0x003)*2))

/*
#define DP_DSP_STATUS                 ((DPRAMADDR)(DPRAM_CPU_base+(DP_DSP_TASK_STATUS_BASE+0x000)*2))
#define DP_MCU_STATUS                 ((DPRAMADDR)(DPRAM_CPU_base+(DP_DSP_TASK_STATUS_BASE+0x001)*2))
#define DP_DSP_STATUS_MTCMOS          ((DPRAMADDR)(DPRAM_CPU_base+(DP_DSP_TASK_STATUS_BASE+0x002)*2))
#define DP_MCU_STATUS_MTCMOS          ((DPRAMADDR)(DPRAM_CPU_base+(DP_DSP_TASK_STATUS_BASE+0x003)*2))
*/


#define DP_DSP_STATUS_                 (DPRAM_base(DP_DSP_STATUS)) 
#define DP_MCU_STATUS_                 (DPRAM_base(DP_MCU_STATUS)) 
#define DP_DSP_STATUS_MTCMOS_          (DPRAM_base(DP_DSP_STATUS_MTCMOS)) 
#define DP_MCU_STATUS_MTCMOS_          (DPRAM_base(DP_MCU_STATUS_MTCMOS)) 

#define DEBUG_ASSERT_CTRL_             (DPRAM_base(DEBUG_ASSERT_CTRL)) 
#define DP_SLOW_IDLE_DIVIDER_          (DPRAM_base(DP_SLOW_IDLE_DIVIDER)) 
#define DP_DSP_ROM_VERSION_            (DPRAM_base(DP_DSP_ROM_VERSION)) 

//also defined in "dpram_6595.h"	
#define PATCH_START_ADDR          DPRAM_base(DP_PATCH_DOWNLOAD_BEGIN_ADDR)
//#define PATCH_START_ADDR  DP_NB_RES_S0_0
//#define DP_NB_RES_S0_0                ((DPRAMADDR)(DPRAM_CPU_base+(DP_NB_RESULT_RX0_BASE+0x000)*2))

#define DP_Slow_Idle_Divider          (DPRAM_base(DP_SLOW_IDLE_DIVIDER))      

#endif
