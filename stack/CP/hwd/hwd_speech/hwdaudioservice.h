/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
/*****************************************************************************
* 
* FILE NAME   : hwdaudioservice.h
* DESCRIPTION : *
*
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

#ifndef _HWD_SPH_AUDIO_SERVICE_H_
#define _HWD_SPH_AUDIO_SERVICE_H_


// #include "exeapi.h"
#include "sysdefs.h"
#include "hwdcommon_def.h"

/*----------------------------------------------------------------------------
 Global Defines  Macros
----------------------------------------------------------------------------*/
typedef void	(*L1Audio_EventHandler)(void*);


#define	MAX_AUDIO_FUNCTIONS		21

#define  NUM_DEBUG_INFO      9
#define  VM_DEBUG_INFO       0
#define  VOIPEVL_DEBUG_INFO  1
#define  AMR_REC_DEBUG_INFO  2
#define  AWB_REC_DEBUG_INFO  3
#define  APM_DEBUG_INFO      4
#define  STEREO_RECORD_INFO  5
#define  AUDIO_DEBUG_MISC    6  // factory mode audio debug parameter 6
#define  CTM_DEBUG_INFO      7
#define  AVB_DEBUG_INFO      8


void L1Audio_Init(void);
bool L1Audio_IsTaskInitilized(void);
void L1Audio_Task_process(void);

uint16 L1Audio_GetAudioID( void );
void L1Audio_FreeAudioID( uint16 aud_id );
void L1Audio_SetFlag( uint16 audio_id );
void L1Audio_ClearFlag( uint16 audio_id );

uint16 L1Audio_GetDebugInfo( uint8 index );
void L1Audio_SetDebugInfoN( uint16 index, uint16 debug_info );
void L1Audio_SetDebugInfo( uint16 debug_info[NUM_DEBUG_INFO]);
void L1Audio_DisallowSleep( uint16 audio_id );
void L1Audio_AllowSleep( uint16 audio_id );

void L1Audio_LSetEvent( uint16 audio_id, void *data );
void L1Audio_SetEventHandler( uint16 audio_id, L1Audio_EventHandler handler );
void L1Audio_SetEvent( uint16 audio_id, void *data );

void L1Audio_HookHisrHandler( uint16 magic_no, L1Audio_EventHandler handler, void *userData );
void L1Audio_UnhookHisrHandler( uint16 magic_no );

#endif // _HWD_SPH_AUDIO_SERVICE_H_

