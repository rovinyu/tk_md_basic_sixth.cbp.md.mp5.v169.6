/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2014
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *  hwddaca.c
 *
 * Project:
 * --------
 *  The sixth
 *
 * Description:
 * ------------
 *  DACA driver
 *
 * Author:
 * -------
 *  Lanus Chao(mtk03453)
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision: $
 * $Modtime:  $
 * $Log:      $
 *
 * 01 13 2015 lanus.chao
 * [SIXTH00001470] [C2K] BT CVSD/mSBC check in
 * BT enable
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

#include "monapi.h"    // trace usage
#include "monids.h"    // trace usage 
#include "exeapi.h" 
#include "reg_base.h"
#include "hwdam.h"
#include "hwdafe_def.h"
#include "hwdmedia.h"
#include "hwddaca.h"
#include "hwdsph.h"
#include "hwdsal_def.h"
#include "hwdsal_exp.h"
#include "hwdspherr.h"
#include "hwdaudioservice.h"

#define DACA_STATE    0x4

static _DACA_EX_T dacaEx;

void Daca_hisrDlHdlr(void *param) 
{
   dacaEx.daca_dl_hdlr();   
}

void Daca_hisrUlHdlr(void *param) 
{
   dacaEx.daca_ul_hdlr();   
}

void DACA_Start(void (*daca_dl_hdlr)(void), void (*daca_ul_hdlr)(void), DACA_APP_TYPE app_type)
{
   uint32 I;
   
   dacaEx.aud_daca_id = L1Audio_GetAudioID();
   L1Audio_SetFlag( dacaEx.aud_daca_id ); 

   L1Audio_HookHisrHandler(DP_D2C_DACA_REQ_DL,(L1Audio_EventHandler)Daca_hisrDlHdlr, 0);
   L1Audio_HookHisrHandler(DP_D2C_DACA_REQ_UL,(L1Audio_EventHandler)Daca_hisrUlHdlr, 0);
   dacaEx.daca_dl_hdlr = daca_dl_hdlr; 
   dacaEx.daca_ul_hdlr = daca_ul_hdlr;     
        
   dacaEx.app_type = app_type;     
   
   if(app_type & DACA_IN_PHONE_CALL){
      dacaEx.am_type = AM_PCMEX_TYPE_DACA_DEDICATION;  
   }else{
      dacaEx.am_type = AM_PCMEX_TYPE_DACA_IDLE_WO_ENH;
   }
   
   if(app_type & DACA_USE_NB){
      dacaEx.frame_size = 160;//PCMNWAY_BAND_INFO_NB;
   }else{
      dacaEx.frame_size = 320;//PCMNWAY_BAND_INFO_WB;
   }         

	
    /* the begining of configure the SAL */  
   if(app_type & DACA_USE_UL){
      SAL_PcmEx_SetStateUL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_ON);
   }
   
   if(app_type & DACA_USE_DL){
      SAL_PcmEx_SetStateDL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_ON);
   }  
 
   AM_PCM_EX_On(dacaEx.am_type, (uint32)&dacaEx);   
   
   for(dacaEx.state = DACA_STATE, I = 0; ;I ++){
   	kal_bool is_ready = true;
   	if((dacaEx.app_type & DACA_USE_UL) && (!SAL_PcmEx_CheckStateUL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_RDY)))
			is_ready = false;
		if((dacaEx.app_type & DACA_USE_DL) && (!SAL_PcmEx_CheckStateDL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_RDY)))
			is_ready = false;
		
      if(is_ready)
         break;
      ASSERT(I < 22, HWDSPH_ERR_FORCE_ASSERT, 0);  // wait 200ms    
      ExeTaskWait(1);
   }    
   /* the end of configure the SAL */ 
}

void DACA_Stop(DACA_APP_TYPE app_type)
{
   ASSERT((dacaEx.app_type == app_type) && (dacaEx.state == DACA_STATE), HWDSPH_ERR_FORCE_ASSERT, 0);  
  
    /* the begining of configure the SAL */  
   if(dacaEx.app_type & DACA_USE_UL){//either ready or off
   	ASSERT(SAL_PcmEx_CheckStateUL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_RDY), HWDSPH_ERR_FORCE_ASSERT, 0);
   }else{
   	ASSERT(SAL_PcmEx_CheckStateUL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_OFF), HWDSPH_ERR_FORCE_ASSERT, 0);
   }
   
   if(dacaEx.app_type & DACA_USE_DL){//either ready or off
		ASSERT(SAL_PcmEx_CheckStateDL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_RDY), HWDSPH_ERR_FORCE_ASSERT, 0);
   }else{
   	ASSERT(SAL_PcmEx_CheckStateDL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_OFF), HWDSPH_ERR_FORCE_ASSERT, 0);
   }

	 SAL_PcmEx_SetStateUL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_OFF);
	 SAL_PcmEx_SetStateDL(SAL_PCMEX_TYPE_DACA, SAL_PCMEX_OFF);
	  
   AM_PCM_EX_Off(dacaEx.am_type, (uint32)&dacaEx);    
    /* the end of configure the SAL */  
    
   L1Audio_UnhookHisrHandler(DP_D2C_DACA_REQ_DL); 
   L1Audio_UnhookHisrHandler(DP_D2C_DACA_REQ_UL); 

   L1Audio_ClearFlag( dacaEx.aud_daca_id );
   L1Audio_FreeAudioID( dacaEx.aud_daca_id );
   
   dacaEx.daca_dl_hdlr   = dacaEx.daca_ul_hdlr = NULL;
   dacaEx.state          = 0; 
   dacaEx.app_type       = DACA_APP_TYPE_UNDEFINE; 
   dacaEx.am_type        = 0xFFFF;      
}

extern bool SP_IsSpeakerMute(void);

uint32 DACA_GetFromSD(uint16 *dl_buf)
{
   uint32 I; 
   uint32 fsize = dacaEx.frame_size;
	
	if(SP_IsSpeakerMute()){
		  memset(dl_buf, 0, fsize * sizeof(uint16));
	} else { 
      volatile int16* ptr = (int16 *)SAL_PcmEx_GetBuf(SAL_PCMEX_DACA_BUF_DL);
      int16 *out_buf = (int16 *)dl_buf;
      int32 tmp;
   
      int32 gain = AM_DSP_GetSpeechDigitalGain();
      ASSERT(gain > 0 && gain <= 4096, HWDSPH_ERR_FORCE_ASSERT, 0);

      for( I = 0 ; I < fsize; I++ ){
      	 tmp = *ptr++;
      	 tmp *= gain;
         *out_buf++ = (int16)(tmp >> 12);
      }
	}
   return fsize;
}

uint32 DACA_GetFromBeforeSRC (uint16 *dl_buf)
{
    kal_uint32 fsize = 320;
    if (SP_IsSpeakerMute()) {
        memset(dl_buf, 0, fsize * sizeof(kal_uint16));
	} else {
        volatile kal_int16 *p_in = (volatile kal_int16 *)SAL_PcmEx_GetBuf(SAL_PCMEX_BUF_SD);
        kal_int16 *p_ou = (kal_int16 *)dl_buf;
        kal_uint32 smpl_idx;
        kal_uint32 smpl_cnt = fsize;
        for (smpl_idx = 0 ; smpl_idx < smpl_cnt; smpl_idx++) {
            *p_ou++ = *p_in++;
        }
    }
    return fsize;
}

void DACA_PutToSE(const uint16 *ul_data)  
{
   uint32 I;   
	uint32 fsize = dacaEx.frame_size;
   volatile uint16* ptr = SAL_PcmEx_GetBuf(SAL_PCMEX_DACA_BUF_UL);
   for( I = fsize; I > 0; I-- )
      *ptr++ = *ul_data++;

}

void DACA_FillSE(uint16 value)
{
   uint32 I;
   uint32 fsize = dacaEx.frame_size;
   volatile uint16* ptr = SAL_PcmEx_GetBuf(SAL_PCMEX_DACA_BUF_UL);
   for( I = fsize; I > 0; I-- )
      *ptr++ = value;
}

