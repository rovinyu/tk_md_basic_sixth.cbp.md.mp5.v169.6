/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "exeapi.h" 
#include "exedefs.h"
#include "exeutil.h"
#include "monapi.h"
#include "monids.h"
#include "monvtst.h"
#include "valsndapi.h"
#include "hwdapi.h"
#include "hwdappsapi.h"
#include "hwdpsave.h"
#include "hwdlinkapi.h"
#include "hwdsal_def.h"
#include "hwdsal_exp.h"
#include "ipccaal.h"
#include "lmdapi.h"
#include "dspvapi.h"
#include "sysdefs.h"
#include "hwdam.h"
#include "hwdsph.h"
#include "hwdaudio_dsp_d2c_def.h"
#include "hwdaudioservice.h"
#include "valapi.h" 
#include "valspc_drv.h"


//void SPC2K_DDL_SendToHwd( void );
static void CodReady( void );
static void CodClose( void );
static void CodHBLenGet( uint16 u2RS, IpcSpchSrvcOptRateT u2Rate, uint16 *p2HBLen );
static void CodLenRateGet( uint16 u2HBLen, IpcSpchSrvcOptRateT *pu2Rate );
static void CodSOMinRateSet( IpcSpchSrvcOptT SO );
static void DLFrameProc(IpcDsvSendSpkrVoiceFwdChPcktDataMsgT *MsgFLDataP);
SPC2K_Struct spc2k;

uint16 au2ToneTab1K[C2K_COD_RS_1_FULL_LEN_WORD16] = {0x6eac, 0xe370, 0x4862, 0xf8f0, 0x0444, 0x7c16, 0x5e3a, 0x688f, 0x8ca0, 0xc171, 0x4a00};
uint16 RateReduc_last = 0;

static void CodSOMinRateSet( IpcSpchSrvcOptT SO)
{
    SAL_C2K_SetValue(SAL_C2K_ENC_MIN_RATE, IPC_SPCH_EIGHTH);
    switch (SO)
    {
        case IPC_SPCH_SRVC_OPTION3:
            spc2k.u2RS = C2K_COD_RS_1;
            spc2k.u2Cod = C2K_COD_EVRCA;
            break;
            
        case IPC_SPCH_SRVC_OPTION68:
            spc2k.u2RS = C2K_COD_RS_1;
            spc2k.u2Cod = C2K_COD_EVRCB;
            break;
        
        case IPC_SPCH_SRVC_OPTION17:
            spc2k.u2RS = C2K_COD_RS_2;
            spc2k.u2Cod = C2K_COD_QCELP13K;
            break;

#ifdef MTK_CBP_ENCRYPT_VOICE
        case IPC_SPCH_SRVC_OPTION32944:
            spc2k.u2RS = C2K_COD_RS_1;
            spc2k.u2Cod = C2K_COD_EVRCA;
            SAL_C2K_SetValue(SAL_C2K_ENC_MIN_RATE, IPC_SPCH_FULL);
            break;
#endif

        default:
            MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_SO_ERR, SO, MON_HALT);
            break;
    }
    MonTrace (MON_CP_SPH_LINK_PARSE, 2, spc2k.u2RS, spc2k.u2Cod);
}

static void CodHBLenGet( uint16 u2RS, IpcSpchSrvcOptRateT u2Rate, uint16 *p2HBLen )
{
    if (C2K_COD_RS_1 == u2RS)
    {
        switch (u2Rate)
        {
            case IPC_SPCH_BLANK:
                *p2HBLen = C2K_COD_RS_1_BLANK_LEN_WORD16;
                break;
            case IPC_SPCH_EIGHTH:
                *p2HBLen = C2K_COD_RS_1_EIGHTH_LEN_WORD16;
                break;
            case IPC_SPCH_QUARTER:
                *p2HBLen = C2K_COD_RS_1_QUARTER_LEN_WORD16;
                break;
            case IPC_SPCH_HALF:
                *p2HBLen = C2K_COD_RS_1_HALF_LEN_WORD16;
                break;
            case IPC_SPCH_FULL:
                *p2HBLen = C2K_COD_RS_1_FULL_LEN_WORD16;
                break;
            case IPC_SPCH_ERASURE:
                *p2HBLen = C2K_COD_RS_1_ERASURE_LEN_WORD16;
                break;
            case IPC_SPCH_FULL_LIKELY:
                *p2HBLen = C2K_COD_RS_1_FULL_LIKELY_LEN_WORD16;
                break;
            default:
                break;
        }
    }
    else if(C2K_COD_RS_2 == u2RS)
    {
        switch (u2Rate)
        {
            case IPC_SPCH_BLANK:
                *p2HBLen = C2K_COD_RS_2_BLANK_LEN_WORD16;
                break;
            case IPC_SPCH_EIGHTH:
                *p2HBLen = C2K_COD_RS_2_EIGHTH_LEN_WORD16;
                break;
            case IPC_SPCH_QUARTER:
                *p2HBLen = C2K_COD_RS_2_QUARTER_LEN_WORD16;
                break;
            case IPC_SPCH_HALF:
                *p2HBLen = C2K_COD_RS_2_HALF_LEN_WORD16;
                break;
            case IPC_SPCH_FULL:
                *p2HBLen = C2K_COD_RS_2_FULL_LEN_WORD16;
                break;
            case IPC_SPCH_ERASURE:
                *p2HBLen = C2K_COD_RS_2_ERASURE_LEN_WORD16;
                break;
            default:
                break;
        }
    }	 
}

static void CodLenRateGet( uint16 u2HBLen, IpcSpchSrvcOptRateT *pu2Rate )
{
    switch (u2HBLen)
    {
        case C2K_COD_RS_1_BLANK_LEN_WORD16:
            *pu2Rate = IPC_SPCH_BLANK;
            break;
        case C2K_COD_RS_1_EIGHTH_LEN_WORD16:
            *pu2Rate = IPC_SPCH_EIGHTH;
            break;
        case C2K_COD_RS_1_QUARTER_LEN_WORD16:
            *pu2Rate = IPC_SPCH_QUARTER;
            break;
        case C2K_COD_RS_1_HALF_LEN_WORD16:
            *pu2Rate = IPC_SPCH_HALF;
            break;
        case C2K_COD_RS_1_FULL_LEN_WORD16:
            *pu2Rate = IPC_SPCH_FULL;
            break;
        default:
            MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_DL_RATE_ERR, u2HBLen, MON_HALT);
            break;
    }
}

void SPC2K_init( void )
{
   spc2k.u4HBFrmCountUL1 = 0;
   spc2k.u4HBFrmCountUL2 = 0;
   spc2k.u4HBFrmCountDL1 = 0;
   spc2k.u4HBFrmCountDL2 = 0;
   spc2k.bCodRdy = FALSE;   
   spc2k.bSta     = FALSE;
   spc2k.bFstDLFrm = FALSE;
   spc2k.bAppSta = FALSE;
   spc2k.bLMDLbk = FALSE;
   spc2k.bCAALLbk = FALSE;
   spc2k.u2Cod = C2K_COD_UNDEF;
   spc2k.u2RS = C2K_COD_RS_UNDEF;
#ifndef C2K_SDRV_DISABLE
#ifdef C2K_AP_ENCRYPTION
   L1Audio_HookHisrHandler(DP_D2C_C2K_DONE_UL,(L1Audio_EventHandler)SPC2K_UL_GetSpeechFrame_HISR, 0);
#else
   L1Audio_HookHisrHandler(DP_D2C_C2K_DONE_UL,(L1Audio_EventHandler)SPC2K_UL_GetSpeechFrame, 0);
#endif
   L1Audio_HookHisrHandler(DP_D2C_C2K_DONE_DL,(L1Audio_EventHandler)SPC2K_DL_PutSpeechFrame, 0);
#endif
}

void SPC2K_ConSSO_Req( uint16 u2SO, uint16 u2MaxEncRate)
{
    MonTrace (MON_CP_SPH_LINK_CON_SSO_REQ, 2, u2SO, u2MaxEncRate); 
    if(TRUE == spc2k.bSta)
    {
        MonTrace (MON_CP_SPH_LINK_WARN_SSO_REQ_REPEAT, 0); 
        //return;
    }
    spc2k.bSta = TRUE;
    CodSOMinRateSet( u2SO );
    
#ifndef C2K_SDRV_DISABLE
    L1SP_C2K_Est(spc2k.u2Cod);
#else
    SPC2K_ConSSO_Done();
#endif
}

void SPC2K_ConSSO_Done( void )
{
    MonTrace (MON_CP_SPH_LINK_CON_SSO_DONE, 0);
    ExeMsgSend (EXE_IPC_ID, IPC_MAILBOX_DSPV_ASYN, SDRV_SSO_CONN_DONE, NULL, 0);    
}

void SPC2K_ConSSO_Done_Rsp( void )
{
    enum CAALMsgId MsgId;
    
    CodReady();
    spc2k.bFstDLFrm = TRUE;
    spc2k.u4HBFrmCountUL1 = 0;
    spc2k.u4HBFrmCountUL2 = 0;
    spc2k.u4HBFrmCountDL1 = 0;
    spc2k.u4HBFrmCountDL2 = 0;
    if(FALSE == spc2k.bCAALLbk)
    {    
        MsgId = CAAL_SSO_CONN_RSP_MSG;
        CAAL_ReadMsgsFromDsp(MsgId, NULL);
    }
}

void SPC2K_DisconSSO_Req( void )
{
    MonTrace (MON_CP_SPH_LINK_DISCON_SSO_REQ, 0); 
    if(FALSE == spc2k.bSta)
    {
        MonTrace (MON_CP_SPH_LINK_WARN_DISCON_SSO_REQ_REPEAT, 0); 
        //return;
    } 
#ifdef MTK_CBP_ENCRYPT_VOICE
    SAL_C2K_SetValue(SAL_C2K_ENC_MIN_RATE, IPC_SPCH_EIGHTH);
#endif

#ifndef C2K_SDRV_DISABLE
    L1SP_C2K_DeEst();
#else
    SPC2K_DisconSSO_Done();
#endif
}

void SPC2K_DisconSSO_Done( void )
{
    MonTrace (MON_CP_SPH_LINK_DISCON_SSO_DONE, 0);  
    ExeMsgSend (EXE_IPC_ID, IPC_MAILBOX_DSPV_ASYN, SDRV_SSO_DISCONN_DONE, NULL, 0); 
}

void SPC2K_DisconSSO_Done_Rsp( void )
{
    enum CAALMsgId MsgId;
    
    spc2k.bSta = FALSE;
    CodClose();
    if(FALSE == spc2k.bCAALLbk)
    { 
        MsgId = CAAL_SSO_DISCONN_RSP_MSG;
        CAAL_ReadMsgsFromDsp(MsgId, NULL);
    }
}


bool SPC2K_State( void )
{
   return spc2k.bSta;
}

static void CodReady( void )
{
    MonTrace (MON_CP_SPH_LINK_COD_RDY, 1, spc2k.bCodRdy);      
    if(!spc2k.bCodRdy)
   	{
        spc2k.bCodRdy = TRUE;
    }
}

static void CodClose( void )
{
     MonTrace (MON_CP_SPH_LINK_COD_CLOSE, 1, spc2k.bCodRdy); 
    if(spc2k.bCodRdy)
   	{ 
        spc2k.bCodRdy = FALSE;      
    }
}

static void DLFrameProc(IpcDsvSendSpkrVoiceFwdChPcktDataMsgT *MsgFLDataP)
{
    volatile uint16 *pu2Addr;
    uint16 u2HBLen;
#ifdef C2K_AP_ENCRYPTION
    IpcSpchSrvcOptRateT u2Rate;
    uint8 *pucHBLen;
#endif
    uint16 i;
    
    //check rate
    if((MsgFLDataP->SpkrVoiceFwdChPcktRate != IPC_SPCH_BLANK) && (MsgFLDataP->SpkrVoiceFwdChPcktRate != IPC_SPCH_EIGHTH) && 
    	 (MsgFLDataP->SpkrVoiceFwdChPcktRate != IPC_SPCH_QUARTER) && (MsgFLDataP->SpkrVoiceFwdChPcktRate != IPC_SPCH_HALF) && 
    	 (MsgFLDataP->SpkrVoiceFwdChPcktRate != IPC_SPCH_FULL) && (MsgFLDataP->SpkrVoiceFwdChPcktRate != IPC_SPCH_ERASURE) && 
    	 (MsgFLDataP->SpkrVoiceFwdChPcktRate != IPC_SPCH_FULL_LIKELY))
    {
        MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_DL_RATE_ERR, MsgFLDataP->SpkrVoiceFwdChPcktRate, MON_HALT);
    }       
    CodHBLenGet(spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktRate, &u2HBLen);
    //check size
    if(MsgFLDataP->SpkrVoiceFwdChPcktSize != u2HBLen)
    {
        MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_DL_HB_SIZE_ERR, MsgFLDataP->SpkrVoiceFwdChPcktSize, MON_HALT);
    }
#ifndef C2K_SAL_DISABLE

#ifdef C2K_AP_ENCRYPTION
    if(GetVoiceEncSwitch())
    {
        //Put HB DL to encrypt
        for(i = 0; i < u2HBLen; i++)
        {
            spc2k.u2HBTemp[i] = MsgFLDataP->SpkrVoiceFwdChPcktData[i];
        }

        PutC2KDLOriData(&(spc2k.u2HBTemp[0]), (uint8)(u2HBLen*2));
        
        //Get encrypted HB DL
        pucHBLen = (uint8 *)&u2HBLen;
        if(TRUE == (GetC2KDLDecData(&(spc2k.u2HBTemp[0]), pucHBLen)))
        {
            u2HBLen = *pucHBLen/2;
            MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 0x1969,1,*pucHBLen);
            CodLenRateGet( u2HBLen, &u2Rate );
            MsgFLDataP->SpkrVoiceFwdChPcktRate = u2Rate;
            for(i = 0; i < u2HBLen; i++)
            {
                MsgFLDataP->SpkrVoiceFwdChPcktData[i] = spc2k.u2HBTemp[i];
            }
        }
        else
        {
            MsgFLDataP->SpkrVoiceFwdChPcktRate = IPC_SPCH_BLANK;
            u2HBLen = C2K_COD_RS_2_BLANK_LEN_WORD16;
        }
        
    }
#endif

    SAL_C2K_SetValue(SAL_C2K_DL_RATE, MsgFLDataP->SpkrVoiceFwdChPcktRate);
    pu2Addr = SAL_C2K_GetAddr(SAL_C2K_ADDR_RXHB);
    for(i = 0; i < u2HBLen; i++)
    {
        *pu2Addr = MsgFLDataP->SpkrVoiceFwdChPcktData[i];
        pu2Addr++;
    }       
    SAL_C2K_SetDLReady();
#endif
}


void SPC2K_DL_PutSpeechFrame_Do(IpcDsvSendSpkrVoiceFwdChPcktDataMsgT *MsgFLDataP)
{
    spc2k.u4HBFrmCountDL1++;
    spc2k.u4HBFrmCountDL2++;
    if(!spc2k.bCodRdy)
    {
        MonTrace (MON_CP_SPH_LINK_WARN_COD_DL_UNRDY, 0);
        return;
    }
    
//    MonTrace (MON_CP_SPH_LINK_HB_DL1, 10, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, spc2k.u4HBFrmCountDL1, MsgFLDataP->SpkrVoiceFwdChPcktData[0], MsgFLDataP->SpkrVoiceFwdChPcktData[1], 
//              MsgFLDataP->SpkrVoiceFwdChPcktData[2], MsgFLDataP->SpkrVoiceFwdChPcktData[3], MsgFLDataP->SpkrVoiceFwdChPcktData[4], MsgFLDataP->SpkrVoiceFwdChPcktData[5], 
//              MsgFLDataP->SpkrVoiceFwdChPcktData[6]);
    
#ifdef C2K_DTS_DISABLE
    if(spc2k.bFstDLFrm)
    {
        MonTrace (MON_CP_SPH_LINK_COD_1ST_FRAME, 0);
        spc2k.bFstDLFrm = FALSE;
    #ifndef C2K_SDRV_DISABLE 
        L1SP_C2K_IntraRAT(spc2k.u2Cod);
    #endif
    }
    else
    {
        DLFrameProc(MsgFLDataP);
    }
#else
    DLFrameProc(MsgFLDataP);
#endif
    MonTrace (MON_CP_SPH_LINK_PARSE, 2, spc2k.u2RS, spc2k.u2Cod);
    MonTrace (MON_CP_SPH_LINK_HB_DL1, 10, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, spc2k.u4HBFrmCountDL1, MsgFLDataP->SpkrVoiceFwdChPcktData[0], MsgFLDataP->SpkrVoiceFwdChPcktData[1], 
              MsgFLDataP->SpkrVoiceFwdChPcktData[2], MsgFLDataP->SpkrVoiceFwdChPcktData[3], MsgFLDataP->SpkrVoiceFwdChPcktData[4], MsgFLDataP->SpkrVoiceFwdChPcktData[5], 
              MsgFLDataP->SpkrVoiceFwdChPcktData[6]);
    MonTrace (MON_CP_SPH_LINK_HB_DL2, 5, spc2k.u4HBFrmCountDL2, MsgFLDataP->SpkrVoiceFwdChPcktData[7], MsgFLDataP->SpkrVoiceFwdChPcktData[8], MsgFLDataP->SpkrVoiceFwdChPcktData[9], MsgFLDataP->SpkrVoiceFwdChPcktData[10]);    
    // log DL HB
    //switch (MsgFLDataP->SpkrVoiceFwdChPcktRate)
    //{
    //    case IPC_SPCH_BLANK:
    //    case IPC_SPCH_ERASURE:
    //        MonTrace (MON_CP_SPH_LINK_HB_DL, 1, 0x0);
    //    break;
    //    
    //    case IPC_SPCH_EIGHTH:
    //        if(C2K_COD_RS_1 == spc2k.u2RS)
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 3, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0]);
    //        }
    //        else
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 4, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[1]);
    //        }
    //    break;
    //    
    //    case IPC_SPCH_QUARTER:
    //        if(C2K_COD_RS_1 == spc2k.u2RS)
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 5, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[1], MsgFLDataP->SpkrVoiceFwdChPcktData[2]);
    //        }
    //        else
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 6, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[1], MsgFLDataP->SpkrVoiceFwdChPcktData[2], MsgFLDataP->SpkrVoiceFwdChPcktData[3]);
    //        }
    //    break;
    //    
    //    case IPC_SPCH_HALF:
    //        if(C2K_COD_RS_1 == spc2k.u2RS)
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 7, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[1], MsgFLDataP->SpkrVoiceFwdChPcktData[2], MsgFLDataP->SpkrVoiceFwdChPcktData[3], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[4]);
    //        }
    //        else
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 10, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[1], MsgFLDataP->SpkrVoiceFwdChPcktData[2], MsgFLDataP->SpkrVoiceFwdChPcktData[3], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[4], MsgFLDataP->SpkrVoiceFwdChPcktData[5], MsgFLDataP->SpkrVoiceFwdChPcktData[6], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[7]);
    //        }
    //    break;
    //    
    //    case IPC_SPCH_FULL:
    //    case IPC_SPCH_FULL_LIKELY:
    //        if(C2K_COD_RS_1 == spc2k.u2RS)
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 13, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[1], MsgFLDataP->SpkrVoiceFwdChPcktData[2], MsgFLDataP->SpkrVoiceFwdChPcktData[3], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[4], MsgFLDataP->SpkrVoiceFwdChPcktData[5], MsgFLDataP->SpkrVoiceFwdChPcktData[6], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[7], MsgFLDataP->SpkrVoiceFwdChPcktData[8], MsgFLDataP->SpkrVoiceFwdChPcktData[9], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[10]);
    //        }
    //        else
    //        {
    //            MonTrace (MON_CP_SPH_LINK_HB_DL, 1, MsgFLDataP->SpkrVoiceFwdChPcktRate, spc2k.u2RS, MsgFLDataP->SpkrVoiceFwdChPcktData[0], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[1], MsgFLDataP->SpkrVoiceFwdChPcktData[2], MsgFLDataP->SpkrVoiceFwdChPcktData[3], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[4], MsgFLDataP->SpkrVoiceFwdChPcktData[5], MsgFLDataP->SpkrVoiceFwdChPcktData[6], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[7], MsgFLDataP->SpkrVoiceFwdChPcktData[8], MsgFLDataP->SpkrVoiceFwdChPcktData[9], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[10], MsgFLDataP->SpkrVoiceFwdChPcktData[11], MsgFLDataP->SpkrVoiceFwdChPcktData[12], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[13], MsgFLDataP->SpkrVoiceFwdChPcktData[14], MsgFLDataP->SpkrVoiceFwdChPcktData[15], 
    //                      MsgFLDataP->SpkrVoiceFwdChPcktData[16]);
    //        }
    //    break;
    //    
    //    default:
    //    break;
    //}
}

void SPC2K_SOCM_Set(IpcDsvSendSsoCtrlParamsMsgT *pMsg)
{
    uint16 u2SOCM;
    uint16 u2ValId;
    uint16 *pu2RateReduc;
    uint16 *pu2RateReduc_EM;
    
    MonTrace (MON_CP_SPH_LINK_SOCM, 3, pMsg->AudioSsoCtrlRrmLevel, pMsg->AudioSsoCtrlM2mMode, pMsg->AudioSsoCtrlInitCodecMode); 
    u2SOCM = (pMsg->AudioSsoCtrlRrmLevel << C2K_SOCM_RATE_REDUC_SHIFT) || (pMsg->AudioSsoCtrlM2mMode << C2K_SOCM_MOBILE_TO_MOBILE_SHIFT) || pMsg->AudioSsoCtrlInitCodecMode;

#ifndef C2K_SAL_DISABLE
    switch(spc2k.u2Cod)
    {
        case SAL_C2K_COD_QCELP13K:
            u2ValId = SAL_C2K_SOCM_QCELP13K;
            SAL_C2K_SetValue(u2ValId, u2SOCM);
        break;
        
        case SAL_C2K_COD_EVRCA:
            u2ValId = SAL_C2K_SOCM_EVRCA;
            SAL_C2K_SetValue(u2ValId, u2SOCM);
        break;
        
        case SAL_C2K_COD_EVRCB:
            u2ValId = SAL_C2K_SOCM_EVRCB;
            SAL_C2K_SetValue(u2ValId, u2SOCM);
        break;
        
        default:
            
        break;
    }
    
    pu2RateReduc = (uint16 *)ExeMsgBufferGet(sizeof(uint16));
    *pu2RateReduc = pMsg->AudioSsoCtrlRrmLevel;
    
    ExeMsgSend (EXE_VAL_ID, VAL_MAILBOX , VAL_SPH_RATE_REDUCE_NOTIFY_MSG, (void *)pu2RateReduc, sizeof(uint16));

    if (RateReduc_last - pMsg->AudioSsoCtrlRrmLevel != 0)
    {
    	RateReduc_last = pMsg->AudioSsoCtrlRrmLevel;
    	pu2RateReduc_EM = (uint16 *)ExeMsgBufferGet(sizeof(uint16));
      *pu2RateReduc_EM = pMsg->AudioSsoCtrlRrmLevel;
      ExeMsgSend (EXE_VAL_ID, VAL_ATC_MAILBOX, VAL_IPC_RATE_REDUCTION_MSG, (void *)pu2RateReduc_EM, sizeof(uint16));
    }   
    
    
#endif
}

void SPC2K_EncMaxRate_Set(IpcDsvSetMicVoiceEncMaxRateMsgT *pMsg)
{
    MonTrace (MON_CP_SPH_LINK_ENC_MAX_RATE, 1, pMsg->MicVoiceEncMaxRate); 
    //Check ENC_MAX_RATE val
#ifdef MTK_CBP_ENCRYPT_VOICE    
    if((pMsg->MicVoiceEncMaxRate != IPC_SPCH_BLANK) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_EIGHTH) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_QUARTER) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_HALF) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_FULL) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_ENCRYPT_BLK) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_ENCRYPT_FULL))
#else
    if((pMsg->MicVoiceEncMaxRate != IPC_SPCH_BLANK) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_EIGHTH) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_QUARTER) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_HALF) && (pMsg->MicVoiceEncMaxRate != IPC_SPCH_FULL))
#endif
    {
        MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_ENC_MAX_RATE_VAL_ERR, pMsg->MicVoiceEncMaxRate, MON_HALT);
    }
#ifndef C2K_SAL_DISABLE
    SAL_C2K_SetValue(SAL_C2K_ENC_MAX_RATE, pMsg->MicVoiceEncMaxRate);
#ifdef MTK_CBP_ENCRYPT_VOICE  
    if(IPC_SPCH_ENCRYPT_BLK == pMsg->MicVoiceEncMaxRate)
    {
        SAL_C2K_SetValue(SAL_C2K_ENC_MIN_RATE, IPC_SPCH_BLANK);
        SAL_C2K_SetValue(SAL_C2K_ENC_MAX_RATE, IPC_SPCH_BLANK);
    }
    else if(IPC_SPCH_ENCRYPT_FULL == pMsg->MicVoiceEncMaxRate)
    {
        SAL_C2K_SetValue(SAL_C2K_ENC_MIN_RATE, IPC_SPCH_FULL);
        SAL_C2K_SetValue(SAL_C2K_ENC_MAX_RATE, IPC_SPCH_FULL);
    }
#endif
#endif
}

void SPC2K_UL_GetSpeechFrame_HISR( void )
{
    //temp log
    MonTrace (MON_CP_SPH_LINK_COD_1ST_FRAME, 0);
    ExeMsgSend (EXE_IPC_ID, IPC_MAILBOX_DSPV_ASYN, SDRV_UL_GET_FRAME, NULL, 0);
}

void SPC2K_UL_GetSpeechFrame( void )
{   
    uint16 u2SO;
    uint16 u2HBLen;
#ifdef C2K_AP_ENCRYPTION
    IpcSpchSrvcOptRateT u2Rate;
    uint8 *pucHBLen;
#endif
    uint16 i;
    volatile uint16 *pu2Addr;
    volatile uint16 *pu2LbkDLAddr;
    IpcCpSpchDataRevChPacketMsgT* MsgRevPktP;
    enum CAALMsgId MsgId;
    
    spc2k.u4HBFrmCountUL1++;
    spc2k.u4HBFrmCountUL2++;
    if(!spc2k.bCodRdy)
    {
        MonTrace (MON_CP_SPH_LINK_WARN_COD_UL_UNRDY, 0);
        return;
    }

#ifndef C2K_SAL_DISABLE 
    if (FALSE == SAL_C2K_IsULReady())
    {
        MonTrace (MON_CP_SPH_LINK_WARN_UL_UNSYNC, 0);
        SAL_C2K_SetULUnsync();
        return;
    }
#endif

#ifndef C2K_SAL_DISABLE     
    u2SO = SAL_C2K_GetValue(SAL_C2K_VALUE_SO);
#endif
    //check SO
    if((u2SO != SAL_C2K_COD_QCELP13K) && (u2SO != SAL_C2K_COD_EVRCA) && (u2SO != SAL_C2K_COD_EVRCB))
    {
        MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_SO_ERR, u2SO, MON_HALT);
    }
    //check DSP & CAAL SO conflict
    if(u2SO != spc2k.u2Cod)
    {
        MonTrace (MON_CP_SPH_LINK_WARN_SO_CONFLICT, 0);
        return;
    }
    MsgRevPktP = (IpcCpSpchDataRevChPacketMsgT *)ExeMsgBufferGet(sizeof(IpcCpSpchDataRevChPacketMsgT) + 2*C2K_COD_RATE_MAX_LEN_WORD16); 
    if(NULL == MsgRevPktP)
    {
        MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_UL_PKT_MEM_ALLOC_ERR, 0, MON_HALT);
    } 
#ifndef C2K_SAL_DISABLE 
    MsgRevPktP->MppSpchRate = SAL_C2K_GetValue(SAL_C2K_VALUE_UL_RATE);
#endif
    //check rate
    if((MsgRevPktP->MppSpchRate != IPC_SPCH_BLANK) && (MsgRevPktP->MppSpchRate != IPC_SPCH_EIGHTH) && 
    	 (MsgRevPktP->MppSpchRate != IPC_SPCH_QUARTER) && (MsgRevPktP->MppSpchRate != IPC_SPCH_HALF) && 
    	 (MsgRevPktP->MppSpchRate != IPC_SPCH_FULL) && (MsgRevPktP->MppSpchRate != IPC_SPCH_ERASURE) && 
    	 (MsgRevPktP->MppSpchRate != IPC_SPCH_FULL_LIKELY))
    {
        MonFault (MON_IPC_FAULT_UNIT, C2K_LINK_UL_RATE_ERR, MsgRevPktP->MppSpchRate, MON_HALT);
    }
    CodHBLenGet(spc2k.u2RS, MsgRevPktP->MppSpchRate, &u2HBLen);
    MsgRevPktP->NumMppSpchData = u2HBLen;

#ifndef C2K_SAL_DISABLE 

    pu2Addr = SAL_C2K_GetAddr(SAL_C2K_ADDR_TXHB);

#ifdef C2K_AP_ENCRYPTION
    if(GetVoiceEncSwitch())
    {
        //Put HB UL to encrypt
        for(i = 0; i < u2HBLen; i++)
        {
            spc2k.u2HBTemp[i] = pu2Addr[i];
        }
        
//        MonTrace (MON_CP_SPH_LINK_HB_UL1, 10, MsgRevPktP->MppSpchRate, spc2k.u2RS, spc2k.u4HBFrmCountUL1, 
//                  pu2Addr[0], pu2Addr[1], pu2Addr[2], pu2Addr[3], pu2Addr[4], pu2Addr[5], pu2Addr[6]);
//        MonTrace (MON_CP_SPH_LINK_HB_UL1, 10, MsgRevPktP->MppSpchRate, spc2k.u2RS, spc2k.u4HBFrmCountUL1, 
//                  spc2k.u2HBTemp[0], spc2k.u2HBTemp[1], spc2k.u2HBTemp[2], spc2k.u2HBTemp[3], spc2k.u2HBTemp[4], spc2k.u2HBTemp[5], spc2k.u2HBTemp[6]);

        PutC2KULOriData (&(spc2k.u2HBTemp[0]), (uint8)u2HBLen*2);
        
        //Get encrypted HB UL
        pucHBLen = (uint8 *)&u2HBLen;
        if(TRUE == (GetC2KULEncData(&(spc2k.u2HBTemp[0]), pucHBLen)))
        {
            u2HBLen = *pucHBLen/2;
            MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, 0x1969,2,*pucHBLen);
            CodLenRateGet( u2HBLen, &u2Rate );
            MsgRevPktP->MppSpchRate = u2Rate;
            for(i = 0; i < u2HBLen; i++)
            {
                pu2Addr[i] = spc2k.u2HBTemp[i];
            }
        }
        else
        {
            MsgRevPktP->MppSpchRate = IPC_SPCH_BLANK;
            u2HBLen = C2K_COD_RS_2_BLANK_LEN_WORD16;
        }
    }
#endif

    for(i = 0; i < u2HBLen; i++)
    {
        MsgRevPktP->MppSpchData[i] = *pu2Addr;
        pu2Addr++;
    }
    SAL_C2K_SetULEmpty();        
#endif
    MonTrace (MON_CP_SPH_LINK_PARSE, 2, spc2k.u2RS, spc2k.u2Cod);
    MonTrace (MON_CP_SPH_LINK_HB_UL1, 10, MsgRevPktP->MppSpchRate, spc2k.u2RS, spc2k.u4HBFrmCountUL1, MsgRevPktP->MppSpchData[0], MsgRevPktP->MppSpchData[1], 
              MsgRevPktP->MppSpchData[2], MsgRevPktP->MppSpchData[3], MsgRevPktP->MppSpchData[4], MsgRevPktP->MppSpchData[5], 
              MsgRevPktP->MppSpchData[6]);
    MonTrace (MON_CP_SPH_LINK_HB_UL2, 5, spc2k.u4HBFrmCountUL2, MsgRevPktP->MppSpchData[7], MsgRevPktP->MppSpchData[8], MsgRevPktP->MppSpchData[9], MsgRevPktP->MppSpchData[10]);
    if(TRUE == spc2k.bCAALLbk)
    {
        SAL_C2K_SetValue(SAL_C2K_DL_RATE, MsgRevPktP->MppSpchRate);
        pu2LbkDLAddr = SAL_C2K_GetAddr(SAL_C2K_ADDR_RXHB);        
        for(i = 0; i < u2HBLen; i++)
        {
            *pu2LbkDLAddr = MsgRevPktP->MppSpchData[i];
            pu2LbkDLAddr++;
        }
        ExeMsgBufferFree(MsgRevPktP);
        MsgRevPktP = NULL;
    }
    else
    {
        MsgId = CAAL_SPH_DATA_UL_MSG;
        CAAL_ReadMsgsFromDsp(MsgId, (void *)MsgRevPktP);
    }
}

void SPC2K_DL_PutSpeechFrame(IpcDsvSendSpkrVoiceFwdChPcktDataMsgT *MsgFLDataP)
{
    if(TRUE == spc2k.bCAALLbk)
    {
        SPC2K_DL_PutSpeechFrame_Do(MsgFLDataP);
    }
}

void SPC2K_GetSyncDelayRW( uint16 *u2DelR, uint16 *u2DelW, uint16 *u2DelM)
{
    *u2DelR = C2K_DELR * 8;
    *u2DelW = C2K_DELW * 8;
    *u2DelM = C2K_DELM * 8;
    MonTrace (MON_CP_SPH_LINK_DELRWM, 3, *u2DelR, *u2DelW, *u2DelM);
}


#ifdef MTK_PLT_AUDIO
void SPC2K_Link_Lbk(HwdSphLbkMsgT *pMsg)
{   	  
    MonTrace (MON_CP_SPH_LINK_LBK, 3, pMsg->u2Mode, pMsg->u2SO, pMsg->bStart);
    if(C2K_LMD_LBK == pMsg->u2Mode)
    {
        CAAL_Loopback_LMD(pMsg->u2SO, pMsg->bStart);
    }
    else if(C2K_CAAL_LBK == pMsg->u2Mode)
    {
        CAAL_Loopback_CAAL(pMsg->u2SO, pMsg->bStart);
    }
}

void SPC2K_Debug_Info(IpcDsvSendAudioChanQltyMsgT * pMsg)
{
#ifndef C2K_SAL_DISABLE 
    SAL_C2K_SetValue(SAL_C2K_EBNT, pMsg->EbNt);
#endif
}
#endif
