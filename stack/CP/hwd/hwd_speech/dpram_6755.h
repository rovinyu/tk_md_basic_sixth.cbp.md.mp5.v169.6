/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
/****************************************************************************    
 * File: DPRAM.H
 *
 * Version: 2.00
 *
 * Purpose:  Define the addresses and fields of the Dual-Port RAM, DPRAM, which is shared by
 *           the MCU and the DSP. The DPRAM occupies 1K words in its size and is located in
 *           the IO bus of MT6208's DSP subsystem. DSP firmwares, such as equalizer, FCCH detection, Channel
 *           codec, will include this file for the DPRAM adddresses to READ the required parameters AND
 *           WRITE the processing results.
 *
 * Date Created:  03/26/2001 
 * Update Records:
 *      05/20/2001 - Refine the #define fields so that only the starting addresses of the DPRAM
 *                   tables are included and defined in this file. Since reading/writing the DPRAM
 *                   is continuous, this update uses less #define.
 *      05/21/2001 - Add DP_HW_ADDR and DP_HW_DATA for DSP H/W definitions
 *      06/06/2001 - Modify DPRAM address definition for MT6208 IO Memory
 *      07/04/2001 - Add definitions for GPRS multislot TX/RX
 *      08/08/2001 - modify the address of each base, mask DP_PATCH_ADDR, DP_PORT_ADDR,
 *                   DP_DMA_ADDR, DP_FW_INT_MASK_1,DP_FW_INT_MASK_2. by Rolly
 * Note:
 *    1. The address after DP_NB_RES3_ADDR should be modified accordingly
 *
 *      12/05/2001 - modify the addresess DP_NB_RES4_ADDR, DP_NB_RES5_ADDR,
 *                   DP_SB_RES_ADDR and DP_PM_RESx_ADDR (x=0-7)
 *                 - Add #define DP_TX_GPRS_CONSTELLATION_ADDR    0X009A
 *      03/29/2002 - Modify for MT6205, GSM only
 * Note:
 *    1. The defined addresses in this file follows Silver-1 Functional Spec.
 *       However,  these addresses are not finalized yet and might change slightly later.
 *
 * Author: YJ Tsai, RD5/MediaTek Inc.
 *
 * Return Value: None.
 ****************************************************************************/

#ifndef _DPRAM_H_
#define _DPRAM_H_

// Shared Memory Field
#define SHERIF_START_ADDR                    0x3980

#define DP_DSP_TASK_STATUS_BASE              0x3980 /* 0x3980 */

#define DP_DSP_STATUS                        DP_DSP_TASK_STATUS_BASE + 0x000 /* 0x3980 */
#define DP_MCU_STATUS                        DP_DSP_TASK_STATUS_BASE + 0x001 /* 0x3981 */
#define DP_DSP_STATUS_MTCMOS                 DP_DSP_TASK_STATUS_BASE + 0x002 /* 0x3982 */
#define DP_MCU_STATUS_MTCMOS                 DP_DSP_TASK_STATUS_BASE + 0x003 /* 0x3983 */
#define DEBUG_ASSERT_CTRL                    DP_DSP_TASK_STATUS_BASE + 0x004 /* 0x3984 */
#define DP_SLOW_IDLE_DIVIDER                 DP_DSP_TASK_STATUS_BASE + 0x005 /* 0x3985 */
#define DP_DSP_ROM_VERSION                   DP_DSP_TASK_STATUS_BASE + 0x006 /* 0x3986 */

#define DP_DSP_PATCH_BASE                    DP_DSP_TASK_STATUS_BASE + 0x007 /* 0x3987 */

#define DP_PATCH_DOWNLOAD_BEGIN_ADDR         DP_DSP_PATCH_BASE + 0x000 /* 0x3987 */

#define DP2_SP_CODEC_CTRL_BASE               DP_DSP_PATCH_BASE + 0x058 /* 0x39df */

#define DP_SC_ADDR                           DP2_SP_CODEC_CTRL_BASE + 0x000 /* 0x39df */
#define SPH_COD_CTRL                         DP2_SP_CODEC_CTRL_BASE + 0x000 /* 0x39df */
#define SPH_DEL_R                            DP2_SP_CODEC_CTRL_BASE + 0x001 /* 0x39e0 */
#define SPH_DEL_W                            DP2_SP_CODEC_CTRL_BASE + 0x002 /* 0x39e1 */
#define DP_MARGIN_PCM_ADDR                   DP2_SP_CODEC_CTRL_BASE + 0x003 /* 0x39e2 */
#define SPH_DEL_M_DL                         DP2_SP_CODEC_CTRL_BASE + 0x003 /* 0x39e2 */
#define SPH_COD_MODE                         DP2_SP_CODEC_CTRL_BASE + 0x004 /* 0x39e3 */
#define DP2_SC_VAD                           DP2_SP_CODEC_CTRL_BASE + 0x005 /* 0x39e4 */
#define SPH_MUTE_CTRL                        DP2_SP_CODEC_CTRL_BASE + 0x006 /* 0x39e5 */
#define SPH_DEL_O                            DP2_SP_CODEC_CTRL_BASE + 0x007 /* 0x39e6 */
#define DP2_Nsync_value                      DP2_SP_CODEC_CTRL_BASE + 0x008 /* 0x39e7 */

#define DP2_SP_AUDIO_INTERFACE_CTRL_BASE     DP2_SP_CODEC_CTRL_BASE + 0x009 /* 0x39e8 */

#define SPH_DL_VOL                           DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x000 /* 0x39e8 */
#define SPH_8K_CTRL                          DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x001 /* 0x39e9 */
#define SPH_DEV_MODE                         DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x002 /* 0x39ea */
#define SPH_3G_SMR_CTRL                      DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x003 /* 0x39eb */
#define SPH_DEL_M_UL                         DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x004 /* 0x39ec */
#define SPH_UL_VOL                           DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x005 /* 0x39ed */
#define SPH_ST_VOL                           DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x006 /* 0x39ee */
#define AUDIO_ASP_WAV_OUT_GAIN               DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x007 /* 0x39ef */
#define SPH_PM_ADDR_BKF_FLT_COEF_UL_NB       DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x008 /* 0x39f0 */
#define SPH_PM_ADDR_BKF_FLT_COEF_DL_NB       DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x009 /* 0x39f1 */
#define SPH_PM_ADDR_BKF_FLT_COEF_UL_WB       DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x00a /* 0x39f2 */
#define SPH_PM_ADDR_BKF_FLT_COEF_DL_WB       DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x00b /* 0x39f3 */
#define SPH_PM_ADDR_ST_FLT_COEF              DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x00c /* 0x39f4 */
#define SPH_PM_ADDR_SRC_FLT_COEF             DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x00d /* 0x39f5 */
#define SPH_DM_ADDR_EMP                      DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x00e /* 0x39f6 */
#define SPH_BT_MODE                          DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x00f /* 0x39f7 */
#define SPH_ENH_DL_CTRL                      DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x010 /* 0x39f8 */
#define SPH_ENH_DL_GAIN                      DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x011 /* 0x39f9 */

#define DP2_SP_KT_CTRL_BASE                  DP2_SP_AUDIO_INTERFACE_CTRL_BASE + 0x013 /* 0x39fb */

#define DP_KEYTONE_CTRL                      DP2_SP_KT_CTRL_BASE + 0x000 /* 0x39fb */
#define KEYTONE1_CTRL                        DP2_SP_KT_CTRL_BASE + 0x000 /* 0x39fb */
#define KEYTONE2_CTRL                        DP2_SP_KT_CTRL_BASE + 0x001 /* 0x39fc */
#define DP_ASP_FORCE_KT_8K                   DP2_SP_KT_CTRL_BASE + 0x002 /* 0x39fd */
#define KEYTONE1_FREQ                        DP2_SP_KT_CTRL_BASE + 0x003 /* 0x39fe */
#define KEYTONE2_FREQ                        DP2_SP_KT_CTRL_BASE + 0x006 /* 0x3a01 */
#define DP_KEYTONE_ATT_GAIN                  DP2_SP_KT_CTRL_BASE + 0x009 /* 0x3a04 */
#define KEYTONE_ATT_GAIN_ADDR                DP2_SP_KT_CTRL_BASE + 0x009 /* 0x3a04 */

#define DP2_SP_8kPCM_CTRL_BASE               DP2_SP_KT_CTRL_BASE + 0x00a /* 0x3a05 */

#define SPH_DM_ADDR_SE2_PTR                  DP2_SP_8kPCM_CTRL_BASE + 0x000 /* 0x3a05 */
#define SPH_DM_ADDR_SE3_BUF                  DP2_SP_8kPCM_CTRL_BASE + 0x001 /* 0x3a06 */
#define SPH_DM_ADDR_SE4_BUF                  DP2_SP_8kPCM_CTRL_BASE + 0x002 /* 0x3a07 */
#define SPH_DM_ADDR_ANC_PTR                  DP2_SP_8kPCM_CTRL_BASE + 0x003 /* 0x3a08 */
#define SPH_PCM_REC_CTRL                     DP2_SP_8kPCM_CTRL_BASE + 0x004 /* 0x3a09 */
#define SPH_DM_ADDR_EPL_UL_PRE_BUF           DP2_SP_8kPCM_CTRL_BASE + 0x005 /* 0x3a0a */
#define SPH_DM_ADDR_EPL_UL_POS_BUF           DP2_SP_8kPCM_CTRL_BASE + 0x006 /* 0x3a0b */
#define SPH_DM_ADDR_EPL_DL_PRE_BUF           DP2_SP_8kPCM_CTRL_BASE + 0x007 /* 0x3a0c */
#define SPH_DM_ADDR_EPL_DL_POS_BUF           DP2_SP_8kPCM_CTRL_BASE + 0x008 /* 0x3a0d */
#define SPH_DM_ADDR_EPL_UL2_BUF              DP2_SP_8kPCM_CTRL_BASE + 0x009 /* 0x3a0e */
#define SPH_DM_ADDR_EPL_UL3_BUF              DP2_SP_8kPCM_CTRL_BASE + 0x00a /* 0x3a0f */
#define SPH_DM_ADDR_EPL_UL4_BUF              DP2_SP_8kPCM_CTRL_BASE + 0x00b /* 0x3a10 */
#define DP_D2C_SPEECH_UL_INT                 DP2_SP_8kPCM_CTRL_BASE + 0x00c /* 0x3a11 */
#define DP_D2C_SPEECH_DL_INT                 DP2_SP_8kPCM_CTRL_BASE + 0x00d /* 0x3a12 */
#define SPH_3G_UNSYNC_STATUS                 DP2_SP_8kPCM_CTRL_BASE + 0x00e /* 0x3a13 */
#define SPH_DM_ADDR_BGS_UL_BUF               DP2_SP_8kPCM_CTRL_BASE + 0x00f /* 0x3a14 */
#define SPH_DM_ADDR_BGS_DL_BUF               DP2_SP_8kPCM_CTRL_BASE + 0x010 /* 0x3a15 */
#define SPH_BGS_LEN_UL                       DP2_SP_8kPCM_CTRL_BASE + 0x011 /* 0x3a16 */
#define SPH_BGS_LEN_DL                       DP2_SP_8kPCM_CTRL_BASE + 0x012 /* 0x3a17 */
#define SPH_BGS_CTRL                         DP2_SP_8kPCM_CTRL_BASE + 0x013 /* 0x3a18 */
#define SPH_BGS_MIX                          DP2_SP_8kPCM_CTRL_BASE + 0x014 /* 0x3a19 */

#define DP2_SP_VM_CTRL_BASE                  DP2_SP_8kPCM_CTRL_BASE + 0x015 /* 0x3a1a */

#define DP_AWB_RX_TCH_S0_0                   DP2_SP_VM_CTRL_BASE + 0x000 /* 0x3a1a */
#define SPH_2G_SD_DATA_HDR                   DP2_SP_VM_CTRL_BASE + 0x000 /* 0x3a1a */
#define DP_SD1_VM_1                          DP2_SP_VM_CTRL_BASE + 0x001 /* 0x3a1b */
#define SPH_2G_SD_DATA_HB                    DP2_SP_VM_CTRL_BASE + 0x001 /* 0x3a1b */
#define DP2_AWB_SD_TCH_MOD                   DP2_SP_VM_CTRL_BASE + 0x01f /* 0x3a39 */
#define DP_AWB_TX_TCH_S0_0                   DP2_SP_VM_CTRL_BASE + 0x020 /* 0x3a3a */
#define SPH_2G_SE_DATA_HDR                   DP2_SP_VM_CTRL_BASE + 0x020 /* 0x3a3a */
#define DP_SD2_VM_1                          DP2_SP_VM_CTRL_BASE + 0x021 /* 0x3a3b */
#define SPH_2G_SE_DATA_HB                    DP2_SP_VM_CTRL_BASE + 0x021 /* 0x3a3b */
#define DP_Encoder_Used_Mode                 DP2_SP_VM_CTRL_BASE + 0x042 /* 0x3a5c */
#define DP_Decoder_Used_Mode                 DP2_SP_VM_CTRL_BASE + 0x043 /* 0x3a5d */
#define DP_VM_DBG_INFO                       DP2_SP_VM_CTRL_BASE + 0x044 /* 0x3a5e */
#define DP_3G_DEBUG_INFO                     DP2_SP_VM_CTRL_BASE + 0x044 /* 0x3a5e */
#define DP_C2K_DEBUG_INFO                    DP2_SP_VM_CTRL_BASE + 0x044 /* 0x3a5e */
#define SPH_CTM_AMR_REAL_RX_TYPE             DP2_SP_VM_CTRL_BASE + 0x07c /* 0x3a96 */

#define DP2_SP_CTM_BASE                      DP2_SP_VM_CTRL_BASE + 0x081 /* 0x3a9b */

#define SPH_DM_ADDR_PNW_UL1_BUF              DP2_SP_CTM_BASE + 0x000 /* 0x3a9b */
#define SPH_DM_ADDR_PNW_UL2_BUF              DP2_SP_CTM_BASE + 0x001 /* 0x3a9c */
#define SPH_DM_ADDR_PNW_UL3_BUF              DP2_SP_CTM_BASE + 0x002 /* 0x3a9d */
#define SPH_DM_ADDR_PNW_UL4_BUF              DP2_SP_CTM_BASE + 0x003 /* 0x3a9e */
#define SPH_DM_ADDR_PNW_DL_BUF               DP2_SP_CTM_BASE + 0x004 /* 0x3a9f */
#define SPH_PNW_CTRL_UL1                     DP2_SP_CTM_BASE + 0x005 /* 0x3aa0 */
#define SPH_PNW_CTRL_UL2                     DP2_SP_CTM_BASE + 0x006 /* 0x3aa1 */
#define SPH_PNW_CTRL_DL                      DP2_SP_CTM_BASE + 0x007 /* 0x3aa2 */
#define SPH_CTM_CTRL                         DP2_SP_CTM_BASE + 0x008 /* 0x3aa3 */
#define SPH_CTM_BFI_FACCH_REPORT             DP2_SP_CTM_BASE + 0x009 /* 0x3aa4 */
#define SPH_PNW_LEN_UL1                      DP2_SP_CTM_BASE + 0x00a /* 0x3aa5 */
#define SPH_PNW_LEN_UL2                      DP2_SP_CTM_BASE + 0x00b /* 0x3aa6 */
#define SPH_PNW_LEN_UL3                      DP2_SP_CTM_BASE + 0x00c /* 0x3aa7 */
#define SPH_PNW_LEN_UL4                      DP2_SP_CTM_BASE + 0x00d /* 0x3aa8 */
#define SPH_PNW_LEN_DL                       DP2_SP_CTM_BASE + 0x00e /* 0x3aa9 */
#define SPH_CTM_AMR_CODEBOOK_GAIN_LIMIT      DP2_SP_CTM_BASE + 0x00f /* 0x3aaa */
#define SPH_CTM_AMR_CODEBOOK_GAIN_UPDATE     DP2_SP_CTM_BASE + 0x010 /* 0x3aab */
#define SPH_CTM_COSIM_CTRL                   DP2_SP_CTM_BASE + 0x011 /* 0x3aac */

#define DP2_SP_AEC_CTRL_BASE                 DP2_SP_CTM_BASE + 0x012 /* 0x3aad */

#define SPH_ENH_UL_CTRL                      DP2_SP_AEC_CTRL_BASE + 0x000 /* 0x3aad */
#define DP_AEC_CTRL                          DP2_SP_AEC_CTRL_BASE + 0x001 /* 0x3aae */
#define DP_TDNC_CTRL                         DP2_SP_AEC_CTRL_BASE + 0x001 /* 0x3aae */
#define DP_EES_CTRL                          DP2_SP_AEC_CTRL_BASE + 0x002 /* 0x3aaf */
#define DP_DMNR_CTRL                         DP2_SP_AEC_CTRL_BASE + 0x003 /* 0x3ab0 */
#define DP_AEC_CONTROL_WORD                  DP2_SP_AEC_CTRL_BASE + 0x004 /* 0x3ab1 */
#define DP_EC_PAR_ADDR                       DP2_SP_AEC_CTRL_BASE + 0x007 /* 0x3ab4 */
#define DP2_ES_Time_Const                    DP2_SP_AEC_CTRL_BASE + 0x007 /* 0x3ab4 */
#define DP2_ES_Vol_Const                     DP2_SP_AEC_CTRL_BASE + 0x008 /* 0x3ab5 */

#define DP2_SP_VR_CTRL_BASE                  DP2_SP_AEC_CTRL_BASE + 0x00f /* 0x3abc */

#define DP_VR_CTRL                           DP2_SP_VR_CTRL_BASE + 0x000 /* 0x3abc */
#define DP_VR_IO_BASE                        DP2_SP_VR_CTRL_BASE + 0x001 /* 0x3abd */
#define DP2_DATA_AP_DATA_UL                  DP2_SP_VR_CTRL_BASE + 0x004 /* 0x3ac0 */
#define DP2_DATA_AP_DATA_DL                  DP2_SP_VR_CTRL_BASE + 0x005 /* 0x3ac1 */
#define SPH_PCM_ROUTER_CTRL                  DP2_SP_VR_CTRL_BASE + 0x006 /* 0x3ac2 */
#define SPH_THIRDMIC_CTRL                    DP2_SP_VR_CTRL_BASE + 0x007 /* 0x3ac3 */
#define SPH_FOURTHMIC_CTRL                   DP2_SP_VR_CTRL_BASE + 0x008 /* 0x3ac4 */
#define SPH_ANC_CTRL                         DP2_SP_VR_CTRL_BASE + 0x009 /* 0x3ac5 */
#define SPH_3G_D2M_UL_HDR                    DP2_SP_VR_CTRL_BASE + 0x00a /* 0x3ac6 */
#define SPH_3G_D2M_UL_HB_ADDR                DP2_SP_VR_CTRL_BASE + 0x00b /* 0x3ac7 */
#define SPH_3G_D2M_UL_USED_MODE              DP2_SP_VR_CTRL_BASE + 0x00c /* 0x3ac8 */
#define SPH_3G_D2M_UL_UNSYNC_STATUS          DP2_SP_VR_CTRL_BASE + 0x00d /* 0x3ac9 */

#define DP2_SP_AUDIO_CTRL_BASE               DP2_SP_VR_CTRL_BASE + 0x00e /* 0x3aca */

#define DP_TEST_SIM_SBSD_CTRL                DP2_SP_AUDIO_CTRL_BASE + 0x000 /* 0x3aca */
#define DP2_TEST_SIM_IND                     DP2_SP_AUDIO_CTRL_BASE + 0x000 /* 0x3aca */
#define AWB_LB_CHECKSUM_ADDR                 DP2_SP_AUDIO_CTRL_BASE + 0x001 /* 0x3acb */
#define AFE_BT_LB_CHECKSUM_ADDR              DP2_SP_AUDIO_CTRL_BASE + 0x001 /* 0x3acb */
#define SPE_RAM_LB_CHECK_ADDR                DP2_SP_AUDIO_CTRL_BASE + 0x001 /* 0x3acb */
#define DP_AUDIO_CTRL2                       DP2_SP_AUDIO_CTRL_BASE + 0x002 /* 0x3acc */
#define DP_ENHANCED_AUDIO_CTRL               DP2_SP_AUDIO_CTRL_BASE + 0x003 /* 0x3acd */
#define SPH_SCH_IMPROVE_CTRL                 DP2_SP_AUDIO_CTRL_BASE + 0x005 /* 0x3acf */
#define SPH_BGS_UL_GAIN                      DP2_SP_AUDIO_CTRL_BASE + 0x006 /* 0x3ad0 */
#define SPH_BGS_DL_GAIN                      DP2_SP_AUDIO_CTRL_BASE + 0x007 /* 0x3ad1 */
#define SPH_3G_SE_RATE_UPDATE                DP2_SP_AUDIO_CTRL_BASE + 0x008 /* 0x3ad2 */
#define SPH_3G_SD_RATE_UPDATE                DP2_SP_AUDIO_CTRL_BASE + 0x009 /* 0x3ad3 */
#define DP_AWB_SE_CTRL                       DP2_SP_AUDIO_CTRL_BASE + 0x00a /* 0x3ad4 */
#define DP_AWB_SE_STATUS                     DP2_SP_AUDIO_CTRL_BASE + 0x00a /* 0x3ad4 */
#define DP_AWB_SD_CTRL                       DP2_SP_AUDIO_CTRL_BASE + 0x00b /* 0x3ad5 */
#define DP_AWB_SD_STATUS                     DP2_SP_AUDIO_CTRL_BASE + 0x00b /* 0x3ad5 */
#define DP_G723_SE_CTRL                      DP2_SP_AUDIO_CTRL_BASE + 0x00c /* 0x3ad6 */
#define DP_G723_SD_CTRL                      DP2_SP_AUDIO_CTRL_BASE + 0x00d /* 0x3ad7 */
#define DP_NR_CTRL                           DP2_SP_AUDIO_CTRL_BASE + 0x00e /* 0x3ad8 */

#define DP2_SP_AUDIO_ENHANCE_BASE            DP2_SP_AUDIO_CTRL_BASE + 0x00f /* 0x3ad9 */

#define DP_DSP_DEAD_INT                      DP2_SP_AUDIO_ENHANCE_BASE + 0x000 /* 0x3ad9 */
#define DP_AMR_Mode_Error_Assert_Type        DP2_SP_AUDIO_ENHANCE_BASE + 0x001 /* 0x3ada */
#define DP_AMR_Mode_Error_Assert_Flag        DP2_SP_AUDIO_ENHANCE_BASE + 0x002 /* 0x3adb */
#define DP_Audio_Flexi_Ctrl                  DP2_SP_AUDIO_ENHANCE_BASE + 0x003 /* 0x3adc */
#define DP_VOICE_CHG_SCH                     DP2_SP_AUDIO_ENHANCE_BASE + 0x004 /* 0x3add */
#define DP_VOICE_CHG_TYPE                    DP2_SP_AUDIO_ENHANCE_BASE + 0x005 /* 0x3ade */
#define SPH_BT_CTRL                          DP2_SP_AUDIO_ENHANCE_BASE + 0x006 /* 0x3adf */
#define SPH_DM_ADDR_SE_PTR                   DP2_SP_AUDIO_ENHANCE_BASE + 0x007 /* 0x3ae0 */
#define SPH_DM_ADDR_SD_PTR                   DP2_SP_AUDIO_ENHANCE_BASE + 0x008 /* 0x3ae1 */
#define DP_UP_DOWN_SAMPL_CTRL                DP2_SP_AUDIO_ENHANCE_BASE + 0x009 /* 0x3ae2 */
#define DP_DL_NR_CTRL                        DP2_SP_AUDIO_ENHANCE_BASE + 0x00a /* 0x3ae3 */
#define DP_Audio_DAC_CTRL                    DP2_SP_AUDIO_ENHANCE_BASE + 0x00b /* 0x3ae4 */
#define DP_VBI_SYNC_BT_Earphone_CTRL         DP2_SP_AUDIO_ENHANCE_BASE + 0x00c /* 0x3ae5 */
#define SPH_SAL_CHIPDIFF_DUMMY               DP2_SP_AUDIO_ENHANCE_BASE + 0x00d /* 0x3ae6 */
#define SPH_SFE_CTRL                         DP2_SP_AUDIO_ENHANCE_BASE + 0x00f /* 0x3ae8 */
#define DP2_AUDIO_VIA_BT_CTRL                DP2_SP_AUDIO_ENHANCE_BASE + 0x010 /* 0x3ae9 */
#define DP2_AUDIO_VIA_BT_INT                 DP2_SP_AUDIO_ENHANCE_BASE + 0x011 /* 0x3aea */
#define DP2_AGC_CTRL                         DP2_SP_AUDIO_ENHANCE_BASE + 0x012 /* 0x3aeb */
#define DP2_AGC_GAIN                         DP2_SP_AUDIO_ENHANCE_BASE + 0x013 /* 0x3aec */
#define DP2_DATA_AP_CTRL                     DP2_SP_AUDIO_ENHANCE_BASE + 0x014 /* 0x3aed */
#define DP2_20ms_SP_CTRL                     DP2_SP_AUDIO_ENHANCE_BASE + 0x015 /* 0x3aee */
#define DP2_COMPEN_BLOCK_FILTER              DP2_SP_AUDIO_ENHANCE_BASE + 0x015 /* 0x3aee */
#define DP_605_Task_Id                       DP2_SP_AUDIO_ENHANCE_BASE + 0x016 /* 0x3aef */
#define DP_605_no_8k_Task_Id                 DP2_SP_AUDIO_ENHANCE_BASE + 0x017 /* 0x3af0 */

#define DP2_Reserve_BASE                     DP2_SP_AUDIO_ENHANCE_BASE + 0x018 /* 0x3af1 */

#define SPH_3G_CTRL                          DP2_Reserve_BASE + 0x000 /* 0x3af1 */
#define DP2_DDL_MCU_DSP_HAND_SHAKE           DP2_Reserve_BASE + 0x001 /* 0x3af2 */

#define DP2_AWB_8K_BASE                      DP2_Reserve_BASE + 0x002 /* 0x3af3 */

#define DP2_RINI_INT                         DP2_AWB_8K_BASE + 0x006 /* 0x3af9 */
#define DP2_DL_RIN_sig_energy                DP2_AWB_8K_BASE + 0x007 /* 0x3afa */
#define SPH_DYNA_FWLA_MODE_CTRL              DP2_AWB_8K_BASE + 0x008 /* 0x3afb */

#define DP2_EPL_BND_BASE                     DP2_AWB_8K_BASE + 0x009 /* 0x3afc */

#define SPH_EPL_BND                          DP2_EPL_BND_BASE + 0x000 /* 0x3afc */

#define DP2_AWB_LINK_EXTEND_BASE             DP2_EPL_BND_BASE + 0x001 /* 0x3afd */

#define DP_SD1_AWB_VM_0                      DP2_AWB_LINK_EXTEND_BASE + 0x000 /* 0x3afd */
#define DP_3G_RX_VM_0                        DP2_AWB_LINK_EXTEND_BASE + 0x000 /* 0x3afd */
#define SPH_3G_SD_DATA_HDR                   DP2_AWB_LINK_EXTEND_BASE + 0x000 /* 0x3afd */
#define SPH_AP_SD_DATA_HDR                   DP2_AWB_LINK_EXTEND_BASE + 0x000 /* 0x3afd */
#define SPH_C2K_SD_DATA_HDR                  DP2_AWB_LINK_EXTEND_BASE + 0x000 /* 0x3afd */
#define DP_Org_SD1_VM_1                      DP2_AWB_LINK_EXTEND_BASE + 0x001 /* 0x3afe */
#define SPH_3G_SD_DATA_HB                    DP2_AWB_LINK_EXTEND_BASE + 0x001 /* 0x3afe */
#define SPH_AP_SD_DATA_HB                    DP2_AWB_LINK_EXTEND_BASE + 0x001 /* 0x3afe */
#define SPH_C2K_SD_DATA_HB                   DP2_AWB_LINK_EXTEND_BASE + 0x001 /* 0x3afe */
#define DP_SD2_AWB_VM_0                      DP2_AWB_LINK_EXTEND_BASE + 0x020 /* 0x3b1d */
#define DP_3G_TX_VM_0                        DP2_AWB_LINK_EXTEND_BASE + 0x020 /* 0x3b1d */
#define SPH_3G_SE_DATA_HDR                   DP2_AWB_LINK_EXTEND_BASE + 0x020 /* 0x3b1d */
#define SPH_AP_SE_DATA_HDR                   DP2_AWB_LINK_EXTEND_BASE + 0x020 /* 0x3b1d */
#define SPH_C2K_SE_DATA_HDR                  DP2_AWB_LINK_EXTEND_BASE + 0x020 /* 0x3b1d */
#define DP_Org_SD2_VM_1                      DP2_AWB_LINK_EXTEND_BASE + 0x021 /* 0x3b1e */
#define SPH_3G_SE_DATA_HB                    DP2_AWB_LINK_EXTEND_BASE + 0x021 /* 0x3b1e */
#define SPH_AP_SE_DATA_HB                    DP2_AWB_LINK_EXTEND_BASE + 0x021 /* 0x3b1e */
#define SPH_C2K_SE_DATA_HB                   DP2_AWB_LINK_EXTEND_BASE + 0x021 /* 0x3b1e */
#define DP_SD2_AWB_VM_18                     DP2_AWB_LINK_EXTEND_BASE + 0x032 /* 0x3b2f */
#define DP_WB_DEBUG_INFO                     DP2_AWB_LINK_EXTEND_BASE + 0x040 /* 0x3b3d */

#define DP_SPEECH_OTHER_BASE                 DP2_AWB_LINK_EXTEND_BASE + 0x04c /* 0x3b49 */

#define DP_UL_COMFORT_NOISE_THRESHOLD        DP_SPEECH_OTHER_BASE + 0x000 /* 0x3b49 */
#define DP_UL_COMFORT_NOISE_SHIFT            DP_SPEECH_OTHER_BASE + 0x001 /* 0x3b4a */
#define DP_DTX_DSPK_FUN_CTRL_ADDR            DP_SPEECH_OTHER_BASE + 0x002 /* 0x3b4b */
#define DP_HR_BER_THD_ADDR                   DP_SPEECH_OTHER_BASE + 0x003 /* 0x3b4c */
#define DP_FR_BER_THD_ADDR                   DP_SPEECH_OTHER_BASE + 0x004 /* 0x3b4d */
#define DP_EFR_BER_THD_ADDR                  DP_SPEECH_OTHER_BASE + 0x005 /* 0x3b4e */
#define DP2_TIME_STAMP_2G_H                  DP_SPEECH_OTHER_BASE + 0x006 /* 0x3b4f */
#define DP2_TIME_STAMP_2G_L                  DP_SPEECH_OTHER_BASE + 0x007 /* 0x3b50 */
#define DP2_TIME_STAMP_3G_H                  DP_SPEECH_OTHER_BASE + 0x008 /* 0x3b51 */
#define DP2_TIME_STAMP_3G_L                  DP_SPEECH_OTHER_BASE + 0x009 /* 0x3b52 */
#define SPH_DBG_MOD                          DP_SPEECH_OTHER_BASE + 0x00a /* 0x3b53 */
#define DP2_ABF_CTRL_1                       DP_SPEECH_OTHER_BASE + 0x00b /* 0x3b54 */
#define DP2_ABF_CTRL_2                       DP_SPEECH_OTHER_BASE + 0x00c /* 0x3b55 */
#define SPH_DUMIC_CTRL                       DP_SPEECH_OTHER_BASE + 0x00d /* 0x3b56 */

#define DP_SPEECH_RESERVED_BASE              DP_SPEECH_OTHER_BASE + 0x00e /* 0x3b57 */

#define DP_SPH_COSIM_RESULT_REPORT           DP_SPEECH_RESERVED_BASE + 0x000 /* 0x3b57 */
#define DP_SPH_COSIM_FAIL_TYPE               DP_SPEECH_RESERVED_BASE + 0x001 /* 0x3b58 */
#define DP_SPH_SPECIAL_LOOPBACK_CTRL         DP_SPEECH_RESERVED_BASE + 0x002 /* 0x3b59 */
#define DP_TASK5_COSIM_HANDSHAKE             DP_SPEECH_RESERVED_BASE + 0x003 /* 0x3b5a */
#define DP_AGC_SW_GAIN1                      DP_SPEECH_RESERVED_BASE + 0x004 /* 0x3b5b */
#define DP_AGC_SW_GAIN2                      DP_SPEECH_RESERVED_BASE + 0x005 /* 0x3b5c */
#define SPH_DACA_CTRL                        DP_SPEECH_RESERVED_BASE + 0x006 /* 0x3b5d */
#define SPH_DM_ADDR_DACA_UL_BUF              DP_SPEECH_RESERVED_BASE + 0x007 /* 0x3b5e */
#define SPH_DM_ADDR_DACA_DL_BUF              DP_SPEECH_RESERVED_BASE + 0x008 /* 0x3b5f */
#define SPH_DACA_LEN_UL                      DP_SPEECH_RESERVED_BASE + 0x009 /* 0x3b60 */
#define SPH_DACA_LEN_DL                      DP_SPEECH_RESERVED_BASE + 0x00a /* 0x3b61 */
#define SPH_SW_TRANSC_SD_CNTR                DP_SPEECH_RESERVED_BASE + 0x00b /* 0x3b62 */
#define SPH_SW_TRANSC_TAF_CNTR               DP_SPEECH_RESERVED_BASE + 0x00c /* 0x3b63 */
#define SPH_APP_MODE                         DP_SPEECH_RESERVED_BASE + 0x00d /* 0x3b64 */
#define SPH_PLC_CTRL                         DP_SPEECH_RESERVED_BASE + 0x00e /* 0x3b65 */
#define SPH_8K_RESYNC_CTRL                   DP_SPEECH_RESERVED_BASE + 0x00f /* 0x3b66 */
#define SPH_8K_RESYNC_OFFSET_UL              DP_SPEECH_RESERVED_BASE + 0x010 /* 0x3b67 */
#define SPH_8K_RESYNC_OFFSET_DL              DP_SPEECH_RESERVED_BASE + 0x011 /* 0x3b68 */
#define SPH_DM_ADDR_BAND_FLAG                DP_SPEECH_RESERVED_BASE + 0x012 /* 0x3b69 */
#define SPH_WARN_MSG_UL                      DP_SPEECH_RESERVED_BASE + 0x013 /* 0x3b6a */
#define SPH_WARN_MSG_DL                      DP_SPEECH_RESERVED_BASE + 0x014 /* 0x3b6b */
#define SPH_ENH_DYNAMIC_SWITCH               DP_SPEECH_RESERVED_BASE + 0x015 /* 0x3b6c */
#define SPH_ENH_DEBUG_ADDR                   DP_SPEECH_RESERVED_BASE + 0x016 /* 0x3b6d */
#define SPH_EXTCOD_PEX_CTRL                  DP_SPEECH_RESERVED_BASE + 0x017 /* 0x3b6e */
#define SPH_EXTCOD_PEX_LEN                   DP_SPEECH_RESERVED_BASE + 0x018 /* 0x3b6f */

#define DP_SPEECH_C2K_BASE                   DP_SPEECH_RESERVED_BASE + 0x019 /* 0x3b70 */

#define SPH_C2K_UNSYNC_STATUS                DP_SPEECH_C2K_BASE + 0x000 /* 0x3b70 */
#define SPH_C2K_COD_STATE                    DP_SPEECH_C2K_BASE + 0x001 /* 0x3b71 */
#define SPH_C2K_ENC_MAX_RATE                 DP_SPEECH_C2K_BASE + 0x002 /* 0x3b72 */
#define SPH_C2K_COD_FUN_CTRL                 DP_SPEECH_C2K_BASE + 0x003 /* 0x3b73 */
#define SPH_C2K_COD_FEATURE_SWITCH           DP_SPEECH_C2K_BASE + 0x004 /* 0x3b74 */
#define SPH_C2K_COD_EBNT                     DP_SPEECH_C2K_BASE + 0x005 /* 0x3b75 */
#define SPH_C2K_COD_TTY_STATUS               DP_SPEECH_C2K_BASE + 0x006 /* 0x3b76 */
#define SPH_C2K_ENC_MIN_RATE                 DP_SPEECH_C2K_BASE + 0x007 /* 0x3b77 */

#define DP_SPEECH_MISC_BASE                  DP_SPEECH_C2K_BASE + 0x00a /* 0x3b7a */

#define DP_CC_FLAG                           DP_SPEECH_MISC_BASE + 0x000 /* 0x3b7a */
#define DYNAMIC_DOWNLOAD_MCU_STATUS          DP_SPEECH_MISC_BASE + 0x001 /* 0x3b7b */
#define DYNAMIC_DOWNLOAD_STATUS              DP_SPEECH_MISC_BASE + 0x002 /* 0x3b7c */
#define DDL_Task_Flag                        DP_SPEECH_MISC_BASE + 0x003 /* 0x3b7d */
#define DP2_Dynamic_Load_field               DP_SPEECH_MISC_BASE + 0x003 /* 0x3b7d */
#define DDL_Check_Position                   DP_SPEECH_MISC_BASE + 0x004 /* 0x3b7e */
#define DDL_Task_ID                          DP_SPEECH_MISC_BASE + 0x005 /* 0x3b7f */
#define SPH_DDL_STATUS                       DP_SPEECH_MISC_BASE + 0x006 /* 0x3b80 */
#define SPH_DDL_REQ                          DP_SPEECH_MISC_BASE + 0x007 /* 0x3b81 */
#define SPH_SCH_FEATURE_CTRL                 DP_SPEECH_MISC_BASE + 0x008 /* 0x3b82 */

#define DP_SPEECH_C2K_2_BASE                 DP_SPEECH_MISC_BASE + 0x00a /* 0x3b84 */

#define SPH_C2K_SOCM_QCELP13K                DP_SPEECH_C2K_2_BASE + 0x000 /* 0x3b84 */
#define SPH_C2K_SOCM_EVRCA                   DP_SPEECH_C2K_2_BASE + 0x001 /* 0x3b85 */
#define SPH_C2K_SOCM_EVRCB                   DP_SPEECH_C2K_2_BASE + 0x002 /* 0x3b86 */
#define SPH_C2K_SOCM_EVRCNW_NB               DP_SPEECH_C2K_2_BASE + 0x003 /* 0x3b87 */
#define SPH_C2K_SOCM_EVRCNW_WB               DP_SPEECH_C2K_2_BASE + 0x004 /* 0x3b88 */

#define SPH_ENH_INTERNAL_PAR_BASE            DP_SPEECH_C2K_2_BASE + 0x00a /* 0x3b8e */

#define SPH_ENH_INTERNAL_PAR_ADDR            SPH_ENH_INTERNAL_PAR_BASE + 0x000 /* 0x3b8e */

#define DP_SPEECH_EXPAND_BASE                SPH_ENH_INTERNAL_PAR_BASE + 0x010 /* 0x3b9e */

#define DP2_AGC2_CTRL                        DP_SPEECH_EXPAND_BASE + 0x000 /* 0x3b9e */
#define DP2_AGC3_CTRL                        DP_SPEECH_EXPAND_BASE + 0x001 /* 0x3b9f */
#define DP2_AGC4_CTRL                        DP_SPEECH_EXPAND_BASE + 0x002 /* 0x3ba0 */
#define ENH_DYNAMIC_STATE_MACHINE            DP_SPEECH_EXPAND_BASE + 0x003 /* 0x3ba1 */
#define ENH_FLAG_PARAMETER                   DP_SPEECH_EXPAND_BASE + 0x004 /* 0x3ba2 */
#define Sph_DL_Cur_Vol                       DP_SPEECH_EXPAND_BASE + 0x005 /* 0x3ba3 */
#define SPH_ATTACK_GAIN_DL_NB                DP_SPEECH_EXPAND_BASE + 0x006 /* 0x3ba4 */
#define SPH_RELEASE_GAIN_DL_NB               DP_SPEECH_EXPAND_BASE + 0x007 /* 0x3ba5 */
#define SPH_ATTACK_GAIN_DL_WB                DP_SPEECH_EXPAND_BASE + 0x008 /* 0x3ba6 */
#define SPH_RELEASE_GAIN_DL_WB               DP_SPEECH_EXPAND_BASE + 0x009 /* 0x3ba7 */

#define DP2_UNDEFINEDD_BASE                  DP_SPEECH_EXPAND_BASE + 0x00a /* 0x3ba8 */

#define SHERIF_SIZE                          552
// The statement checks whether generates SHERIF size exceeds limitation
#define MT6755_MD3_SHERIF_SIZE (640)
#if (SHERIF_SIZE > MT6755_MD3_SHERIF_SIZE)
    SHERIF SIZE exceeds limitation !! This line makes assembler stop??
#endif

#undef SHERIF_SIZE
#define SHERIF_SIZE (MT6755_MD3_SHERIF_SIZE)

// Macro for SHERIF to DM
#define IO2DM    dm
#define io2dm    dm

/* Sherif marco end */
#endif
