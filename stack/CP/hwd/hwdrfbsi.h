/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdrfbsi.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/
#ifndef _HWDRFBSI_H_
#define _HWDRFBSI_H_

/* BSI universal name */
typedef enum
{
    MIPI0,
    MIPI1,
    MIPI2,
    MIPI3,
    MIPI_END,
    BSI1 = MIPI_END,
    BSI_NAME_MAX,
    BSI_NAME_INVALID = BSI_NAME_MAX
} BsiNameT;

typedef enum
{
    BSI_DLY_SLOT  = 1,
    BSI_DLY_HSLOT = 2,
    BSI_DLY_TRIG_MAX
} BsiDlyBoundaryT;

#ifdef MTK_PLT_ON_PC 
#include "hwdrfser.h"
#endif

#ifdef MTK_PLT_DENALI
#include "hwdrfser.h"
#endif /* MTK_PLT_DENALI */

#if (SYS_BOARD >= SB_JADE)
#include "hwdbsi.h"
#endif

#ifdef MTK_DEV_HW_SIM_RF
#include "hwdbsihwsim.h"
#endif /* MTK_DEV_HW_SIM_RF */

#if (SYS_OPTION_RF_HW == SYS_RF_MT6176)
#define HWD_RFSPI_DEVICE_0          (uint8)0
#define HWD_TXSPI_DEVICE_0          (uint8)0

typedef uint8        HwdRfBsiIdT;
#else
typedef HwdRfSerIdT  HwdRfBsiIdT;
#endif

#if (defined MTK_DEV_DUMP_REG)
#ifndef M_HwdBsiRegLogRdAll
#define M_HwdBsiRegLogRdAll()
#endif
#endif
#endif /* _HWDRFBSI_H_ */


