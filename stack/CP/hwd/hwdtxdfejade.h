/*******************************************************************************
*  Modification Notice:
*  --------------------------
*  This software is modified by MediaTek Inc. and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*******************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdtxdfejade.h
 *
 * Project:
 * --------
 *   MTK OrionPlus Project
 *
 * Description:
 * ------------
 *   This file contains the DDPC Loop Related Functions written in C.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:$
 * $Modtime:$
 * $Log:$
 *
 *
 *
 *
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 ******************************************************************************/
#ifndef HWDTXDFEJADE_H
#define HWDTXDFEJADE_H

#include "hwddefs.h"
#include "hwdapi.h"
#include "hwdrfapi.h"
#include "hwdtxdfecommon.h"
#include "hwdmsapi.h"
#include "monids.h"

/*****************************************************************************
  STRUCT/ENUM DEFINES:
*****************************************************************************/
/** Define clock base*/
typedef enum
{
    SRC_1P2288MHZ,  /** 1.2288MHz */
    SRC_13MHZ,      /** 13MHz*/
    SRC_NUM
}TxModeT;

/** Define NCO module operation status */
typedef enum
{
    TX_NCO_DISEN = 0,   /** Enable NCO module */
    TX_NCO_EN,          /** Disable NCO module */
    TX_NCO_NUM
}TxNcoModeT;

/** Define NCO module operation status */
typedef enum
{
    TX_NCO_PHASE_JUMP_DISEN = 0,   /** Disable NCO Phase Jump */
    TX_NCO_PHASE_JUMP_EN,          /** Enable NCO Phase Jump */
    TX_NCO_PHASE_JUMP_NUM
}TxNcoPhaseJumpT;

typedef enum
{
    TX_DET_FIR_NOT_BYPASS = 0,
    TX_DET_FIR_BYPASS,
    TX_DET_FIR_BYPASS_NUM
}TxDetFirT;

typedef enum
{
    TX_DET_MEAS_NOT_TERM = 0,    /** Disable DCO module in detect path calculation */
    TX_DET_MEAS_TERM,            /** enable DCO module in detect path calculation */
    TX_DET_MEAS_TERM_NUM
}TxDetMeasTerm;




/*****************************************************************************
  MACRO DEFINES:
*****************************************************************************/

/** Define open loop GBBO dB to linear value mapping look-up table size */
#define GBB0_OL_DB2LIN_MAP_LUT_SIZE_JADE       449

/** Define open loop GBBO dB to linear value mapping look-up table offset */
#define GBB0_OL_DB2LIN_MAP_LUT_OFT_JADE        128
/** Define the inverse  for 1.2288Mhz*8, in Q34 */
#define INV_1P2288MHZ_MULTIPLY8_Q34       1748

/** Define the inverse  for 13Mhz, in Q34 */
#define INV_13MHZ_Q34       1322

/** Define 1.2288Mhz*8, divide 2*/
#define DIV2_1P2288MHZ_MULTIPLY8    1228800*4

/** Define the 13Mhz divide 2 */
#define DIV2_13MHZ      6500000


/** Define themax value for reg TTG setting,1/8  in Q34 */
#define MAX_FREQUENCY_SETTING_Q34       0x80000000

/* Define Macro to set Tx open loop GBB0 fine digital gain for 1st half slot, bit0~bit12 */
#define hwdDfeSetDoOlGbb0SlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_FINE_GAIN_GBB0_OL_1ST, (Data) & 0x1FFF)
       
/* Define Macro to set Tx open loop GBB0 fine digital gain for 2nd half slot, bit0~bit12 */
#define hwdDfeSetDoOlGbb0HalfSlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_FINE_GAIN_GBB0_OL_2ND, (Data) & 0x1FFF)
       
/* Define Macro to set Tx close loop GBB0 fine digital gain for 1st half slot, bit0~bit11 */
#define hwdDfeSetDoClGbb0SlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_FINE_GAIN_GBB0_CL_1ST, (Data) & 0xFFF)
           
/* Define Macro to set Tx close loop GBB0 fine digital gain for 2nd half slot, bit0~bit11 */
#define hwdDfeSetDoClGbb0HalfSlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_FINE_GAIN_GBB0_CL_2ND, (Data) & 0xFFF)


/** Define Macro to set TxUPC clip threshold, bit0~bit10 */
#define hwdDfeSetDoTxupcClipThrSlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TXUPC_CLIP_1ST, (Data) & 0x7FF)
       
/** Define Macro to set TxUPC clip threshold, bit0~bit10 */
#define hwdDfeSetDoTxupcClipThrHalfSlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TXUPC_CLIP_2ND, (Data) & 0x7FF)

/** Define Macro to set RF gain target for TxUPC for 1st half slot, bit0~bit11 */
#define hwdDfeSetDoRfGainTargetSlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_RF_GAIN_TARGET_1ST, (Data) & 0xFFF)

/** Define Macro to set RF gain target for TxUPC for 2nd half slot, bit0~bit11 */
#define hwdDfeSetDoRfGainTargetHalfSlotJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_RF_GAIN_TARGET_2ND, (Data) & 0xFFF)

/** Define Macro to enable or disable fine digital gain module, bit0 */
#define hwdDfeSetDoFineGainEnJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_CLOSE_LOOP_MODE, \
       (HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TX_CLOSE_LOOP_MODE) & 0xFFFE) | ((Data) & 0x1))

/** Define Macro to set DDPC loop mode, bit1 */
#define hwdDfeSetDoDdpcLoopModeJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_CLOSE_LOOP_MODE, \
       (HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TX_CLOSE_LOOP_MODE) & 0xFFFD) | (((Data) & 0x1) << TX_DO_LOOP_MODE_BIT_OFT))

/** Define Macro to set Tx clip scale mode, bit2 */
#define hwdDfeSetDoTxClipScaleModeJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_CLOSE_LOOP_MODE,\
       (HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TX_CLOSE_LOOP_MODE) & 0xFFFB) | (((Data) & 0x1) << TX_DO_CLIP_SCALE_MODE_BIT_OFT))

#define hwdDfeGetDoRfGainDetJade(DetGain, done)\
{\
   DetGain = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_RF_GAIN_DET);\
   done = ((DetGain) >> 12)&0x1;\
   DetGain = (int16)(((DetGain)<<4) & 0xFFF0)>>4;\
   if(!done)\
    MonTrace(MON_CP_HWD_RF_DBG_TRACE_ID, 3, DDPC_LOOP_TST_DBG,done,DetGain);\
}
       
/** Define Macro to get delta gain, bit0~bit11 */       
#define hwdDfeGetDoDeltaGainJade(Data) \
       Data = ((int16)((HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TX_FINE_GAIN_DELTA) << 4) & 0xFFF0) >> 4)


/** Define Macro to reset NCO/TxUPC/Fine digital gain module, existing register, add bit5~bit7 */
#define hwdDfeDoResetTxJade(Data) \
       HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TXHA_TX_RESET, (HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TXHA_TX_RESET) & 0xFF1F) | (((Data) & 0x7) << TX_DO_NCO_RSTN_BIT_OFT))

/** Define Macro Enable TTG and Set TTG frequencey1--Orionc+,0--Orionc*/
#define hwdDfeSetDoTxTTGJade(FreqHi,FreqLo) \
{\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_LO,(FreqLo) & 0xFFFF)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_HI, (1<<4 |((FreqHi)&0xF)))\
}

#if (SYS_ASIC <= SA_MT6750)
/** Define Macro  1--Orionc+,0--Orionc*/
#define hwdDfeSetDoTxOrionPlusEnJade(OrionPlusEn) \
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ORIONPLUS_EN, (OrionPlusEn) & 0x1)
#endif

/** Define Macro set tx frequency error*/
#define hwdDfeSet1xTxAfcDeltaJade(TxFoe)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_AFC_DELTA,(TxFoe) & 0xFFFF)
    
/** Define Macro Set Tx PATH PhaseErr/GainErr. PhaseErr (S-4,10), GanErr(S-4,10)*/
#define hwdDfeSetDoTxIQErrJade(PhaseErr, GainErr)\
{\
  HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_IQ_PHASE_EST, (PhaseErr) & 0x7F);\
  HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_IQ_GAIN_EST, (GainErr) & 0x7F);\
}

/**  Define Macro Set TXDCO s0.14*/
#define hwdDfeSetDoTxDcOJade(TxDcoI,TxDcoQ)\
{\
   HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_DC_OFFSETI, (TxDcoI) & 0x7FFF);\
   HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_DC_OFFSETQ, (TxDcoQ) & 0x7FFF);\
}

/** Define Macro En/Disable NCO*/
#define hwdDfeSetDoTxNcoEnJade(NcoEn)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_NCO_EN, (NcoEn) & 0x1);

/** Define Macro En/Disable NCO PHASE JUMP*/
#define hwdDfeSetDoTxNcoPhaseEnJade(NcoPhaseEn)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_NCO_PHASE_JUMP_EN, (NcoPhaseEn) & 0x1);

/** Define Macro Set Frequency Offset Compensaion.
  *  Backup function if adjust PLL solution abnormal.
  *  Default set to zero
  */
#define hwdDfeSetDoNcoPhiJade(NcoPhiHi,NcoPhiLo)\
{\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_NCO_PHI_LO,(NcoPhiLo) & 0xFFFF)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_NCO_PHI_HI,(NcoPhiHi) & 0x1)\
}
/** Define Macro Set NCO phase jump*/
#define hwdDfeSetDoTxNcoPhaseJumpJade(JumpThetaHi,JumpThetaLo)\
{\
     HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_NCO_JUMP_THETA_LO,(JumpThetaLo)&0xFFFF);\
     HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_NCO_JUMP_THETA_HI,((JumpThetaHi)&0x7F));\
}

/**Define Macro set txpath antidroop finlter coeff
  * coeff - [-1,-1,8,16,8,-1,-1]
  * Gain shift -[5,8,11]
  */
#define hwdDfeSetDoTxAntiDroopFilterJade()\
{\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ANTIDROOP_COEFF0, 0xFF);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ANTIDROOP_COEFF1, 0xFF);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ANTIDROOP_COEFF2, 0x8);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ANTIDROOP_COEFF3, 0x10);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ANTIDROOP_GAINII, 0x5);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ANTIDROOP_GAINJJ, 0x8);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_ANTIDROOP_GAINKK, 0xB);\
}

/**Define Macro set Detect path filter bypass or not */
#define hwdDfeSetDoDetFilterPassJade(Bypass)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_FIR_BYPASS,(Bypass) & 0x1)

/**Define Macro set Detect path IQ ERR, PhaseErr (S-4,10), GanErr(S-3,10)*/
#define hwdDfeSetDoDetIQErrJade(PhaseErr, GainErr)\
{\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_IQ_PHASE_EST,((PhaseErr)&0x7F));\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_IQ_GAIN_EST,((GainErr)&0xFF));\
}

/**Define Macro set detect path DCO*/
#define hwdDfeSetDoDetDcOJade(TxDcoI,TxDcoQ)\
{\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_DC_OFFSETI,((TxDcoI)&0xFFF));\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_DC_OFFSETQ,((TxDcoQ)&0xFFF));\
}


/**Define Macro set detect path filter coeff
  *  SRC_EN 0--1.2288MHz 1--13MHz
  * [-4,-5,35,81,25,-5,-4] for 1.2288MHz
  * [-4,-5,35,90,25,-5,-4] for 13MHz
  */
#define hwdDfeSetDoDetAntiDroopFilterJade(SrcEn)\
{\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_ANTIDROOP_COEFF0, 0xFC);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_ANTIDROOP_COEFF1, 0xFB);\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_ANTIDROOP_COEFF2, 35);\
    if(SrcEn)\
    {\
         HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_ANTIDROOP_COEFF3, 90);\
         }\
    else\
    {\
        HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_ANTIDROOP_COEFF3, 81);\
    }\
}

/**Define Macro set AntiDroop compensation scale
  *  13M--128/142 = 1846
  *  1.2288M -- 128/133 = 1971
  */
#define hwdDfeSetDoDetAntiDroopCompScaleJade(SrcEn)\
{\
    if(SrcEn)\
    {\
        HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_COMPENSATION_SCALE, 1846);\
    }\
    else\
    {\
        HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_COMPENSATION_SCALE, 1971);\
    }\
}
/**Define Macro set AntiDroop truncation bits */
#define hwdDfeSetDoDetAntiDroopTruncSelJade(TruncBits)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_ANTIDROOP_TRUNC_SEL, ((TruncBits)&0xF));
    
/**Define Macro set DDPC measure ticks (1.2288M or 13M) */
#define hwdDfeSetDoDdpcLenJade(SrcEn)\
{\
    if(SrcEn)\
    {\
        HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_LENGTH, (0x410));\
    }\
    else\
    {\
        HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_LENGTH, (0x313));\
    }\
}
/**Define Macro set DDPC measure term, default set to 1*/    
#define hwdDfeSetDoDdpcMeasTermJade(MeasTerm)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_MEAS_TERM,((MeasTerm)&0x1));

/**Define Macro set DDPC Path delay
  *  13M--25.72
  *  1.2288M -- 24.78
  */
#define hwdDfeSetDoDetDelayDiffJade(SrcEn)\
{\
    if(SrcEn)\
    {\
        HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_DELAY_DIFF, 26);\
    }\
    else\
    {\
        HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_DELAY_DIFF, 25);\
    }\
}

/**Define Macro set GBB0+OtherGain
  *Other gain 13M --0 1.2288--1 (in Q5)
  */    
#define hwdDfeSetDoDetGbb0AndOtherGainJade(Gain)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_GBB0_OTHER_GAIN,(Gain) & 0xFFF);

/**Define Macro set DetPathGain - CouplerLoss
  * s6,5
  */
#define hwdDfeSetDoDetPathGainJade(DetGain)\
    HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_DET_TXDET_GAIN,((DetGain)&0xFFF));



/**Define Macro to get Sum of reference I path in DDPC,for Debug*/
#define hwdDfeGetDdpcRefSumIJade(SumHi,SumLo)\
{\
    SumLo = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUMI_LO_REF);\
    SumHi = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUMI_HI_REF);\
}
/**Define Macro to get Sum of reference Q path in DDPC,for Debug*/
#define hwdDfeGetTxDdpcRefSumQJade(SumHi,SumLo)\
{\
    SumLo = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUMQ_LO_REF);\
    SumHi = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUMQ_HI_REF);\
}
/**Define Macro to get Sum of square of reference I path and Q path in DDPC,for Debug*/
#define hwdDfeGetTxDdpcRefSumProdJade(SumHi,SumMid,SumLo)\
{\
    SumLo = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUM_PROD_LO_REF);\
    SumMid = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUM_PROD_MI_REF);\
    SumHi = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUM_PROD_HI_REF);\
}
/**Define Macro to get Sum of detect I path in DDPC,for Debug*/
#define hwdDfeGetTxDdpcDetSumIJade(SumHi,SumLo)\
{\
    SumLo = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_SUMI_LO_DET);\
    SumHi = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_SUMI_HI_DET);\
}
/**Define Macro to get Sum of detect Q path in DDPC,for Debug*/
#define hwdDfeGetTxDdpcDetSumQJade(SumHi,SumLo)\
{\
    SumLo = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_SUMQ_LO_DET);\
    SumHi = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_SUMQ_HI_DET);\
}
/**Define Macro to get Sum of square of detect I path and Q path in DDPC,for Debug*/
#define hwdDfeGetTxDdpcDetSumProdJade(SumHi,SumMid,SumLo)\
{\
    SumLo = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUM_PROD_LO_DET);\
    SumMid = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUM_PROD_MI_DET);\
    SumHi = HwdReadJade(REG_TYPE_TXDFE,HWD_DO_DET_DDPC_SUM_PROD_HI_DET);\
}


/** Define Macro to set open loop GBB0 in immediate mode */
#define hwdSetDoGbb0ImmedJade()\
       HwdWriteJade(REG_TYPE_TXDFE,HWD_MXS_TX_IMMED_TRIG,HWD_MXS_TX_IMMED_TRIG_GBB0_OL)

/** Define Macro to disable TTG */
#define hwdDfeDisableTTGJade()\
   HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_HI,HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_HI)&0x0F);


/** Define Macro to enable  TTG */
#define hwdDfeEnableTTGJade()\
   HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_HI,(HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_HI)|0x10));

/** Define Macro to set the frequency to TTG */
#define hwdSetDoTTGFrequencyJade(freq)\
{\
  HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_LO,(freq)&0xFFFF);\
  HwdWriteJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_HI,(HwdReadJade(REG_TYPE_TXDFE,HWD_DO_TX_TTG_PHI_HI)&0x10)|(((freq)>>16)&0xF));\
}

/*****************************************************************************
  GLOBAL FUNCTION DEFINES:
*****************************************************************************/
void HwdTxAgcCfgDfeSlotJade(TxMode ddpcMode, HwdRfTxAgcParamT *rfTxAgcParamP, int16 targetGain);
void HwdTxAgcCfgDfeHalfSlotJade(TxMode ddpcMode, HwdRfTxAgcParamT *rfTxAgcParamP, int16 targetGain);
void HwdTxDfeInitJade(void);
void HwdTxAgcCfgDfeImmedJade(HwdRfTxAgcImmedParamT *rfTxAgcParamP);
void HwdTxSineCfgJade(HwdMsSinCfgT *cfgPtr);
void HwdTxSineGenJade(HwdMsSinGenT *sinGenPtr);

#define M_HwdTxAgcCfgDfeSlot(a,b,c)       HwdTxAgcCfgDfeSlotJade(a,b,c)
#define M_HwdTxAgcCfgDfeHalfSlot(a,b,c)   HwdTxAgcCfgDfeHalfSlotJade(a,b,c)
#define M_HwdTxDfeInit()                  HwdTxDfeInitJade()
#define M_HwdTxAgcCfgDfeImmed(a)          HwdTxAgcCfgDfeImmedJade(a)

int16 HwdTxDfeTxDcOrigResultTranToFinlResultJade(int32 dc_orig_result);
int16 HwdTxDfeDetDcOrigResultTranToFinlResultJade(int32 dc_orig_result);
int16 HwdTxDfeTxIqGainOrigResultTranToFinlResultJade(int32 iq_gain_orig_result);
int16 HwdTxDfeTxIqPhaseOrigResultTranToFinlResultJade(int32 iq_phase_orig_result);
int16 HwdTxDfeDetIqGainOrigResultTranToFinlResultJade(int32 iq_gain_orig_result);
int16 HwdTxDfeDetIqPhaseOrigResultTranToFinlResultJade(int32 iq_phase_orig_result);

#define M_HwdTxDfeTxDcOrigResultTranToFinlResult(a)       HwdTxDfeTxDcOrigResultTranToFinlResultJade(a)
#define M_HwdTxDfeDetDcOrigResultTranToFinlResult(a)      HwdTxDfeDetDcOrigResultTranToFinlResultJade(a)
#define M_HwdTxDfeTxIqGainOrigResultTranToFinlResult(a)   HwdTxDfeTxIqGainOrigResultTranToFinlResultJade(a)
#define M_HwdTxDfeTxIqPhaseOrigResultTranToFinlResult(a)  HwdTxDfeTxIqPhaseOrigResultTranToFinlResultJade(a)
#define M_HwdTxDfeDetIqGainOrigResultTranToFinlResult(a)  HwdTxDfeDetIqGainOrigResultTranToFinlResultJade(a)
#define M_HwdTxDfeDetIqPhaseOrigResultTranToFinlResult(a) HwdTxDfeDetIqPhaseOrigResultTranToFinlResultJade(a)

/*****************************************************************************
  MACRO DEFINES:
*****************************************************************************/
/** Define open loop GBBO dB to linear value mapping look-up table size */
#define GBB0_OL_DB2LIN_MAP_LUT_SIZE GBB0_OL_DB2LIN_MAP_LUT_SIZE_JADE

/** Define open loop GBBO dB to linear value mapping look-up table offset */
#define GBB0_OL_DB2LIN_MAP_LUT_OFT GBB0_OL_DB2LIN_MAP_LUT_OFT_JADE

#define M_OlGbb0dB2LinearMapLut  OlGbb0dB2LinearMapLutJade

#define hwdSetDoGbb0Immed        hwdSetDoGbb0ImmedJade

#define hwdDfeGetDoRfGainDet     hwdDfeGetDoRfGainDetJade

#define hwdDfeGetDoDeltaGain     hwdDfeGetDoDeltaGainJade

#define hwdDfeSetDoDdpcLoopMode  hwdDfeSetDoDdpcLoopModeJade

/*****************************************************************************
  Global Varible
*****************************************************************************/
extern const uint16 M_OlGbb0dB2LinearMapLut[GBB0_OL_DB2LIN_MAP_LUT_SIZE];


#ifdef MTK_DEV_DUMP_REG
void HwdTxDfeRegLogRdAllJade(void);
#define M_HwdTxDfeRegLogRdAll()  HwdTxDfeRegLogRdAllJade()
#endif

/*************************************************************************
      End of the file
*************************************************************************/
#endif

