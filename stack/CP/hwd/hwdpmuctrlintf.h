/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
/*****************************************************************************
 *
 * Filename:
 * ---------
 *   hwdpmuctrlintf.h
 *
 * Project:
 * --------
 *   C2K
 *
 * Description:
 * ------------
 *   PMU Ctrl interface API list Definition
 *
 * Author:
 * -------
 *   mtk08667
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:$
 * $Modtime:$
 * $Log:$
 * 
*****************************************************************************/

#ifndef   _HWDPMUCTRLINTF_H_
#define   _HWDPMUCTRLINTF_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "hwdapi.h"
#include "monapi.h"
#include "do_rcpapi.h"

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define MonPmuAssert(Expr)  MonAssert(Expr, MON_HWD_FAULT_UNIT, HWD_ERR_PMU_POLLING_FAILURE_ERR, 0, MON_HALT)

/*
** Bit definitions for PMIC_INT_STS and PMIC_INT_MSK
*/
#define IMED_DO1x_CONFLICT          (1 << 5)       /* bit5  - CP send a new immediate command when 1x/DO is busy transfering
                                                              VPA command, this interrupt maybe happen when debug */
#define PMIC_IMED_CONFLICT          (1 << 4)       /* bit4  - CP send a new command when pmic is not ready for a new command or fifo is full */
#define VPA_1x_DONE                 (1 << 3)       /* bit3  - 1x VPA transaction done */
#define VPA_DO_DONE                 (1 << 2)       /* bit2  - EVDO VPA transaction done */
#define PMIC_IMED_DONE              (1 << 1)       /* bit1  - PMIC ready for a new command assert when spi_c2k_pa_ack is back 
                                                              in cpu_mode or fifo become empty */
#define PMIC_DONE                   (1 << 0)       /* bit0  - PMIC ready for a new command assert when spi_c2k_pa_ack is back 
                                                              in cpu_mode or fifo become empty */
                                                     
/*                                                   
** Bit definitions for PMIC_IMMD_STATUS              
*/
#define TRANSAC_EN                  (1   << 18)       /* bit18-  */
#define C2K_SPI_PA_ACK_SYNC         (1   << 17)       /* bit17-  */
#define C2K_SPI_PA_REQ              (1   << 16)       /* bit16-  */
#define IMMDFIFO_RD_ADDR            (0x7 << 12)       /* bit14-12 */
#define IMMDFIFO_WR_ADDR            (0x7 << 8 )       /* bit10-8  */
#define IMMDFIFO_CNT                (0x7 << 4 )       /* bit6-4  */
#define IMMDFIFO_EMPTY              (1   << 1 )       /* bit1  */
#define IMMDFIFO_FULL               (1   << 0 )       /* bit0  */


/*
** Bit definitions for PMIC_DATA
*/
#define PMIC_CtrlAddr               (0xffff << 16)      /* bit 31:16 -  */
#define PMIC_CtrlData               (0xffff << 0)       /* bit 15:0  - */

/*
** Bit definitions for PMIC_CTRL
*/
#define SW_RESET                    (1 << 2)          /* bit 2 - */
#define FIFO_MODE                   (1 << 1)          /* bit 1 - */
#define FIFOMODE_EN                 (1 << 0)          /* bit 0 - */

/*
** Bit definitions for PMIC_INTERVAL_PERIOD
*/
#define INTERVAL_PERIOD             (0xff << 0)       /* bit7:0 -  */

/*
** Bit definitions for CMD_DO_1ST
*/
#define CMD_DO_1ST_ADDR             (0xffff << 16)     /* bit31-16  */
#define CMD_DO_1ST_DATA             (0xffff <<  0)      /* bit15-0  */

/*
** Bit definitions for CMD_DO_2ND
*/
#define CMD_DO_2ND_ADDR             (0xffff << 16)     /* bit31-16  */
#define CMD_DO_2ND_DATA             (0xffff <<  0)     /* bit15-0  */

/*
** Bit definitions for DO_1ST_STATUS and DO_2ND_STATUS
*/
#define FIFODO_RD_ADDR              (0x7 << 12)     /* bit14-12  */
#define FIFODO_WR_ADDR              (0x7 << 8)      /* bit10-8  */
#define FIFODO_CNT                  (0x7 << 4)      /* bit6-4  */
#define FIFODO_EMPTY                (0x1 << 1)      /* bit1 */
#define FIFODO_FULL                 (0x1 << 0)      /* bit0 */
                                    
/*
** Bit definitions for CMD_1X_ADDR, it's dspm register
*/
#define CMDADDR1X                   (0xffff << 0)   /* bit15-0  */

/*
** Bit definitions for CMD_1X_DATA, it's dspm register
*/
#define CMDDATA1X                   (0xffff << 0)   /* bit15-0  */

/*
** Bit definitions for VPA_1X_STATUS, it's dspm register
*/
#define FIFO1x_FULL                 (0x1 << 4)    /* bit4  */
#define VPA_1X_DONE                 (0x1 << 0)   /* bit0  */

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
typedef enum
{
    CP_IMMED,
    CP_FIFO_IMMED,
    CP_DELAYLOADER,
    DSP_DELAYLOADER,
    UNUSEDPATH
} PmicCtrlIntfPath;
  
typedef enum
{
    STATUS_FREE,
    STATUS_BUSY 
} PmicCtrlIntfStatus;

typedef enum
{
    FIFO_EMPTY,
    FIFO_NOEMPTY,
    FIFO_FULL 
} PmicCtrlFifoStatus;

typedef enum
{
    IMMED_FIFO,
    DO_1ST_HFSLOT_FIFO,
    DO_2ND_HFSLOT_FIFO
} PmicCtrlIntfFifo;
  
  
/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
extern void  HwdPmicCtrlWrapIntfInit(void);
extern bool  HwdPmicCtrlWrapIntfWriteImmed(uint32* CmdDataArray, uint32 CmdCnts);
extern void  HwdPmicCtrlWrapIntfWriteDlyedFifoDo(uint32 *CmdDataArray, uint32 CmdCnts, RcpTxLoadBoundaryT hslot);
extern bool  PmicWrapReadFromWACS(uint16 addr, uint16* regval);
extern void  HwdPmicCtrlWrapIntfLisr(void);

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
