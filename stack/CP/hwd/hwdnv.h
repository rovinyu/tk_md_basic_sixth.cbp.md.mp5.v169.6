/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdbpi.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _HWDNV_H_
#define _HWDNV_H_

#ifdef __cpluscplus
extern "C" {
#endif

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "dbmapi.h"
#include "hwdapi.h"


/*----------------------------------------------------------------------------
 Defines and Macros
----------------------------------------------------------------------------*/
#define FAULT_BIT_MAP_CLR           (0x10000000)
#define FAULT_BIT_MAP_ALL           (0x20000000)
#define FAULT_PROC_GET_BAND         (0x30000000)
#define FAULT_PROC_INIT             (0x40000000)
#define FAULT_PROC_MAILBOX          (0x50000000)
#define FAULT_PROC_FUNC             (0x60000000)
#define FAULT_PROC_CHK_SIZE         (0x70000000)
#define FAULT_PROC_INFO_INITED      (0x80000000)
#define FAULT_BIT_MAP_NULL          (0x90000000)
#define FAULT_SEG_NOT_AVAILABLE     (0xa0000000)
#define FAULT_SET                   (0xb0000000)
#define FAULT_PROC_TBL              (0xc0000000)
#define FAULT_SEG_IDX               (0Xd0000000)
#define FAULT_PROC_WRITE            (0xe0000000)
#define FAULT_PROC_INPUT_DATA       (0xf0000000)
#define FAULT_PROC_CUST_BUF_NULL    (0x11000000)
#define FAULT_CATEGORY              (0x12000000)
#define FAULT_INPUT_PTR             (0x13000000)
#define FAULT_INPUT_SIZE            (0x14000000)
#define FAULT_MAGIC_NOT_RIGHT       (0x15000000)
#define FAULT_PROC_READ             (0x16000000)

/* Maximum segment number in rf_cust.db, rf_cal.db or in concomitant dbm files */
#define HWD_DBM_SEG_MAX_NUM         (0x80) /* Confined by message enumarations */

#define HWD_DBM_SEG_MIN_NUM_REQUIRED     (1)
#define HWD_DBM_SEG_MAX_NUM_ALLOWED      (32 * 32)
#define NOT_PROC_BIT_MAP                 (0xFFFFFFFF)
#define M_DbmSegBMLowNum(nUM)            (((nUM) + 31) / 32)
#define HWD_DBM_SEG_BM_LOW_MAX_NUM       M_DbmSegBMLowNum(HWD_DBM_SEG_MAX_NUM)

#define HWD_NV_GET_DATA_PTR(mSGp)        HwdNvGetDataPtr(mSGp)

typedef struct
{
    uint32 high;
    uint32 low[HWD_DBM_SEG_BM_LOW_MAX_NUM];
} HwdDbmSegProcBitMapT;

typedef struct
{
    uint16 segId;
    uint32 segSize;
    uint32 msgId;
    uint32 wrRspMsgId;
    void (*processFunc)(void *);
    void *defaultDataPtr;
    void *inputDataPtr;
    uint32 dataSize;
    HwdRfBandT rfBand;
} HwdDbmSegProcessT;

typedef struct
{
    bool procInfoInited;
    bool writeAllOnce;
    bool supportCc; /* Support cross-core data processing or not */
    void *custPtr; /* Only work with malloc memory */
} HwdDbmSegProcessInternalInfoT;

typedef struct
{
    /* Main infomation needed by NV process function */
    const DbmDataBaseIdT dbId;
    const uint32 segNum;
    const ExeMailboxIdT mailBoxId;
    HwdDbmSegProcessT *procTbl;
    HwdDbmSegProcBitMapT *procBMPtr;

    /* Internal data, should be all zero when instantialized */
    HwdDbmSegProcessInternalInfoT procInter;
} HwdDbmSegProcessInfoT;


/* RF custom data init message type */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;             /* cmd/rsp info for acknowlegment */
   uint8       InitCategory;
   uint8       InitMode;
} PACKED_POSTFIX HwdDbmDataInitMsgT;

/* RF custom data init response message type */
typedef PACKED_PREFIX struct
{
   uint8       InitCategory;
   uint8       InitMode;
} PACKED_POSTFIX HwdDbmDataInitRspMsgT;

/* RF custom data set message type */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;             /* cmd/rsp info for acknowlegment */
   uint8       Category;
   uint8       UseDefaultData;
   uint8       SupportCc;
} PACKED_POSTFIX HwdDbmDataSetMsgT;

/* RF custom data set response message type */
typedef PACKED_PREFIX struct
{
   uint8       Category;
   uint8       UseDefaultData;
   uint8       SupportCc;
} PACKED_POSTFIX HwdDbmDataSetRspMsgT;

typedef struct
{
   uint16 msgId;
   void * msgPtr;
   uint32 msgSize;
} HwdNvMsgInfoT;

extern bool DspmReadyForGettingDbmData;

extern void HwdInitSegNotProcessBitMap(HwdDbmSegProcessInfoT *procInfoPtr);
extern void HwdClrSegNotProcessBitMap(const HwdDbmSegProcessInfoT *procInfoPtr, uint16 segId);
extern bool HwdSegIsAllProcessed(const HwdDbmSegProcessInfoT *procInfoPtr);
extern void HwdSegSetAllProcessed(HwdDbmSegProcessInfoT *procInfoPtr);
extern HwdRfBandT HwdDbmDataGetRfBand(const HwdDbmSegProcessInfoT *segInfoP, uint16 segId);
extern void HwdDbmDataInitialize(HwdDbmSegProcessInfoT *proc,
                           const HwdDbmDataInitMsgT *msgPtr,
                           void (*initialCall)(const HwdDbmSegProcessInfoT *));
extern bool HwdDbmDataSet(HwdDbmSegProcessInfoT *procP,
                           const HwdDbmDataSetMsgT *msgPtr);
extern bool HwdDbmDataProcessDbmReadRspMsg(const HwdDbmSegProcessInfoT *procP,
                           uint32 MsgId, void* MsgDataPtr, uint32 MsgSize,
                           void (*allDoneCallback)(const HwdDbmSegProcessInfoT *));
extern bool HwdDbmDataProcessDbmWriteRspMsg(HwdDbmSegProcessInfoT *procP,
                           uint32 MsgId, void* MsgDataPtr, uint32 MsgSize,
                           void (*allDoneCallback)(const HwdDbmSegProcessInfoT *));
extern void* HwdNvGetDataPtr(void* MsgP);
extern uint8 HwdNvGetBand(HwdDbmSegProcessInfoT *procP, void* MsgP);
extern uint16 HwdNvGetSegId(HwdDbmSegProcessInfoT *procP, void* MsgP);

extern bool HwdMailbox1Handler(uint32 MsgId, void* MsgDataPtr, uint32 MsgSize);

extern uint16 HwdNvLidToIdx(const HwdDbmSegProcessInfoT *procP, uint16 lId);
extern void HwdDbmReadAllData(uint8 initMode);

#include "hwdnvcust.h"

#ifdef __cpluscplus
}
#endif

#endif /* _HWDNV_H_ */

