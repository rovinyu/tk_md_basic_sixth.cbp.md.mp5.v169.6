/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdnvcust.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _HWDNVCUST_H_
#define _HWDNVCUST_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "hwdrf.h"


/*----------------------------------------------------------------------------
 Typedefs
----------------------------------------------------------------------------*/
typedef enum
{
    CATE_RF_PARAM,
    CATE_RF_CAL,
    CATE_MIPI,
    CATE_POC,
    CATE_LAST, ///TODO: use this category instead of POC

#if (SYS_BOARD >= SB_JADE)
    CATE_DRDI,
#endif
    CATE_NUM,

} HwdDbmDataInitCategoryT;

/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
extern HwdDbmSegProcessInfoT HwdRfCustSegProcessInfo;
extern HwdDbmSegProcessInfoT HwdRfCalSegProcessInfo;
#if (SYS_BOARD >= SB_JADE)
extern HwdDbmSegProcessInfoT HwdMipiSegProcessInfo;
extern HwdDbmSegProcessInfoT HwdPocSegProcessInfo;
extern HwdDbmSegProcessInfoT HwdDrdiSegProcessInfo;
#endif

extern bool HwdDbmSegMsgProcess(uint32 MsgId, void* MsgDataPtr, uint32 MsgSize);
extern void HwdDbmDataReInit(HwdDbmDataInitMsgT *initPtr);
extern void HwdDbmDataFlush(HwdDbmDataSetMsgT *setMsgPtr);
extern void HwdSendCustToDspm(void);
extern void HwdDbmDataCompileCheck(void);
extern void HwdDbmSegProcessFinal(const HwdDbmSegProcessInfoT * procPtr);

//extern HwdRfDevCfgT rfDevCfg;

#endif /* _HWDNVCUST_H_ */
