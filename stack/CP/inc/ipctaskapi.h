/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef IPCTASKAPI_H
#define IPCTASKAPI_H
/*****************************************************************************
 
  FILE NAME:   ipctaskapi.h

  DESCRIPTION: This file contains all constants and typedefs needed to 
               interface with the IPC unit.

*****************************************************************************/

#include "ipcapi.h"
#include "exeapi.h"
#include "sysdefs.h"

/*------------------------------------------------------------------------
* Declare global constants used in IPC unit
*------------------------------------------------------------------------*/

/* Define IPC task signals */
#define IPC_SIGNAL_DSPM_CTL_MBOX   EXE_SIGNAL_1
#define IPC_SIGNAL_DSPV_MBOX       EXE_SIGNAL_2
#define IPC_SIGNAL_DSPM_SYNC       EXE_SIGNAL_3
#define IPC_SIGNAL_DSPV_SYNC       EXE_SIGNAL_4
#define IPC_SIGNAL_WAKEUP          EXE_SIGNAL_5  /* Used to force a wakeup */
#define IPC_SIGNAL_LBK             EXE_SIGNAL_6

/* SBC Encoder definitions */
#define IPC_SBC_ENCODE_DATA_WORDS_MONO   (48) /* 48 words for mono */
#define IPC_SBC_ENCODE_DATA_WORDS_STEREO (96) /* 48 * 2 words for stereo, R & L channels */

#if 0
#define SYS_OPTION_IPC_DEBUG
#else
#undef SYS_OPTION_IPC_DEBUG
#endif


/* IPC task msg Ids */
typedef enum 
{
   IPC_SEND_AUDIO_SSO_CONNECT_MSG = 0,
   IPC_SEND_AUDIO_SSO_DISCONNECT_MSG,
   IPC_SET_AUDIO_LOOPBACK_MODE_MSG,
   IPC_START_ENCODER_DECODER_TEST_MSG,
   IPC_START_ENCODER_TEST_MSG,
   IPC_START_DECODER_TEST_MSG,
   IPC_CLEAR_FWD_STATS_MSG,
   IPC_SEND_AUDIO_RECORD_PLAYBACK_MODE_MSG,

   /* Multimedia Application commands/responses */
   IPC_APP_MODE_MSG,                /* Start/Stop cmd from MMI/UI              */
   IPC_APP_CONFIGURE_CHANNELS_MSG,  /* Config channel cmd from DSPV            */
   IPC_APP_CHAN_DATA_RSP_MSG,       /* Shared Memory Data response from MMI/UI */
   IPC_DYN_CODE_DNLD_MSG,           /* Dyn Code Download request from IPC ISR  */

   /* Dsp version msgs and responses */
   IPC_DSPM_VERSION_MSG,
   IPC_DSPV_VERSION_MSG,
   IPC_DSPM_VERSION_RSP_MSG,

#ifndef CBP7_IMS   
   IPC_DSPV_VERSION_RSP_MSG,

#else
   IPC_DSPV_VERSION_RSP_MSG,
   
   /* RTP related msgs */
   IPC_START_RTPRMI_MSG,
   IPC_STOP_RTPRMI_MSG,
   IPC_RTPRMI_SEND_TO_DSP_MSG,
#endif   

#ifdef SYS_OPTION_GSM
   IPC_DSPV_ROUTE_MSG,
   IPC_DSPV_ROUTE_NONE,
#endif  

   IPC_DSPV_ALIVE_MSG,
   IPC_DSPM_ALIVE_MSG,

   IPC_DSPM_CODE_CRASH_DBG_DATA_MSG,
   IPC_DSPV_LPBK_RECOVERY_MSG

} IpcMsgIdT;

/* Bit-Map for type of DSPv download, used by IpcDspvDnldActive() */
typedef enum     
{
    IPC_DSPV_CODE_DNLD = 0x01,
    IPC_DSPV_APP_DNLD  = 0x02
} IpcDSpvDnldT;

/* IPC_APP_CHAN_DATA_RSP_MSG  and IPC_DYN_CODE_DNLD_MSG Structures */
typedef PACKED_PREFIX struct
{
   uint16      ChannelId;
   uint16      DataSize;         /* Size in WORDS of data              */
   uint16      *DataP;           /* Pointer to data to be sent to DSPV */
} PACKED_POSTFIX  IpcAppChanDataRspMsgT;

typedef IpcAppChanDataRspMsgT IpcDynCodeDnldMsgT;

/* IPC_DSPM_VERSION_MSG Structure */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  IpcDspmVersionMsgT;

/* IPC_DSPV_VERSION_MSG Structure */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  IpcDspvVersionMsgT;

typedef struct
{
   uint16   LoopbackMode;   /* use IpcAudioLoopBackModeT */
   uint16   DelaySecs;      /* delay before looping back */
} IpcAudioLoopbackMsgT;

#if (SYS_BOARD == SB_BB7) /* vnguyen - TO-BE-REVISITED */
extern bool IpcDspmInterfaceDisabled;
extern bool IpcDspvInterfaceDisabled;
#endif

#ifdef MTK_DEV_ENABLE_DSP_DUMP
extern bool NeedDumpDspm;
extern bool DspmIsInForceFault;
#endif
extern bool IpcDspmResetIn2Second;


#ifndef MTK_PLT_AUDIO
void    IpcSharedMemCtrlDspv (bool CtrlToDspv);
void    IpcSharedMemSbcDataSize (uint16 SbcDataSize);
#endif
uint16  IpcSharedMemGetAppChanWordSize (void);

void    IpcDspvDnldActive (bool Downloading, IpcDSpvDnldT DnldType);
uint32 *IpcAppsGetChanDataAddr (void);
void    IpcDspmReset (uint32 FaultCode2);
void    IpcForceDspvOff (void);



/*****************************************************************************
 
  FUNCTION NAME: IpcReadDirectBuffer

  DESCRIPTION:

    This routine reads the contents of the direct buffer mailbox into memory
    supplied by the calling routine. The forward physical channel type is 
    returned as is the number of words in the channel. After the data is read 
    the direct buffer mailbox is made available again to the DSPM.

  PARAMETERS:

    DataP     - Pointer to memory into which the direct buffer mailbox's 
                contents are written.
    NumWordsP - Pointer to number of words in the direct buffer mailbox data area.
    PhysChanP - Pointer to forward physical channel type.
    DBufNum   - Identifies which direct buffer to be read

  RETURNED VALUES:
    
    None

*****************************************************************************/
extern void IpcReadDirectBuffer (uint16 *DataP, uint16 LoopbackSize, uint16 FwdOffset);

/*****************************************************************************
 
  FUNCTION NAME: IpcWriteDirectBuffer

  DESCRIPTION:

    This routine writes data to the direct buffer mailbox from memory
    supplied by the calling routine. The reverse physical channel type is 
    passed as is the number of words, both of these are written to the
    reverse channel signaling buffer. After the data is written 
    to the direct buffer mailbox the DSPM is notified through the handshake
    register.

  PARAMETERS:

    DataP    - Pointer to memory into which the direct buffer mailbox's 
               contents are written.
    NumWords - Number of words to be writeen to the direct buffer mailbox data area.
    PhysChan - Reverse channel physical channel type.
    FundRate - Fundamental channel rate.

  RETURNED VALUES:
    
    None

*****************************************************************************/
extern void IpcWriteDirectBuffer (uint16 *DataP, uint16 LoopbackSize,
                                  uint16 RevOffset, uint16 FwdOffset);

extern void IpcWriteDirectBufferMultiple(uint16 *DataP, uint16 NumWords, uint16 ChanRdy, uint16 FundRate);

/*****************************************************************************
 
  FUNCTION NAME: IpcClearFwdStats

  DESCRIPTION:

    This routine is called at power up or when an ETS command is received to
    clear the forward frame statistics (bad, total and FERs).

  PARAMETERS:

    None
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern void IpcClearFwdStats(void);

/*****************************************************************************
 
  FUNCTION NAME:  IpcUpdateFwdSchStats

  DESCRIPTION:    This routine updates the SCH statistics.  It is needed
                  because now LMD handles SCH data coming from the MuxPDU hwd.

  PARAMETERS:     TotalFrames:  TOTAL number of frames received
                  BadFrames:    number of BAD frames received

                  There is no need to call the spies right here because
                  there must be another active channel and IpcHisrDspmDBufMbox()
                  is already calling the spies there.

  RETURNED VALUES:  None

*****************************************************************************/
extern void IpcUpdateFwdSchStats ( uint16 TotalFrames, uint16 BadFrames );


/*****************************************************************************
 
  FUNCTION NAME: IpcFwdChannelDisable

  DESCRIPTION:

    This routine is used to notify IPC when DSPm forward channels
    are being torn down.

  PARAMETERS:

    Channel - bitmap indicating the channel being disabled
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern void IpcFwdChannelDisable(uint16 Channel);

/*****************************************************************************
 
  FUNCTION NAME: IpcSetWakeUpTime

  DESCRIPTION:
      This function is called when we wake up from deep sleep
      determine the system wakeup time. It's necessary to avoid fake forward
      frame missing errors.

  PARAMETERS:

    uint8 system: SYS_MODE_1xRTT or SYS_MODE_EVDO
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern void IpcSetWakeUpTime(uint8 system);

#ifndef MTK_PLT_AUDIO
/*****************************************************************************
 
  FUNCTION NAME: IpcLisrSharedMem

  DESCRIPTION:

    This routine is the Shared Memory LISR.  It simply activates the
    corresondent Hisr.

  PARAMETERS:

    None
    
  RETURNED VALUES:

    None

*****************************************************************************/
void IpcLisrSharedMem(void);
#endif

/*****************************************************************************
  FUNCTION NAME: IpcIsDspvCompatible

  DESCRIPTION:
    This routine returns the DSPV Compatibility status.
    
  PARAMETERS:      None
                   
  RETURNED VALUES: TRUE if DSPv is compatible, FALSE otherwise.
*****************************************************************************/
bool IpcIsDspvCompatible (void);

/*****************************************************************************
  FUNCTION NAME: IpcIsDspmCompatible

  DESCRIPTION:
    This routine returns the DSPM Compatibility status.
    
  PARAMETERS:      None
                   
  RETURNED VALUES: TRUE if DSPm is compatible, FALSE otherwise.
*****************************************************************************/
bool IpcIsDspmCompatible (void);

/*****************************************************************************
  FUNCTION NAME: IpcIsDspmInReset

  DESCRIPTION:   This routine returns the DspmIsInReset bool

  PARAMETERS:    None

  RETURNED VALUES:  bool
*****************************************************************************/
bool IpcIsDspmInReset(void);


/*****************************************************************************
 
  FUNCTION NAME: IpcLisrDspmDBufMbox

  DESCRIPTION:

    This routine is the direct buffer mailbox LISR for the DSPM processor. Since 
    the main function of the direct buffer mailbox isr will be to route the 
    data appropriately, exe services must be used. Since exe services cannot
    be called from an lisr, the hisr must be immediately activated.

  PARAMETERS:

    None
    
  RETURNED VALUES:

    None

*****************************************************************************/

extern void IpcLisrDspmDBufMbox(void);

/*****************************************************************************
 
  FUNCTION NAME: IpcLisrDspmCtlMbox

  DESCRIPTION:

    This routine is the control mailbox LISR for the DSPM processor. It 
    copies all messages in the control mailbox into the IPC circular buffer.
    This circular buffer of messages is then processed by the IPC task.

  PARAMETERS:

    None
    
  RETURNED VALUES:

    None

*****************************************************************************/

extern void IpcLisrDspmCtlMbox(void);

#ifndef MTK_PLT_AUDIO
/*****************************************************************************
 
  FUNCTION NAME: IpcLisrDspv

  DESCRIPTION:

    This routine is the LISR which services the interrupt from the
    DSPV processor.

  PARAMETERS:

    None
    
  RETURNED VALUES:

    None

*****************************************************************************/

extern void IpcLisrDspv(void);

/*****************************************************************************
 
  FUNCTION NAME: IpcLisrFiqDspvMbox

  DESCRIPTION:

    This routine is the FIQ mailbox LISR for the DSPV processor. 

  PARAMETERS:

    None
    
  RETURNED VALUES:

    None

*****************************************************************************/

extern void IpcLisrFiqDspvMbox(void);

/*****************************************************************************

  FUNCTION NAME: IpcDspvWatchdogTimerStop

  DESCRIPTION:   Stop the DSPv watchdog timer.

  PARAMETERS:    None

  RET VALUES:    None

*****************************************************************************/
void IpcDspvWatchdogTimerStop (void);

/*****************************************************************************

  FUNCTION NAME: IpcDspvFMBoxReset

  DESCRIPTION:   Reset the Fast Mailbox Cmd and Rsp back to power up values.
                 This is necessary after a DSPv reset, so CP and DSPv remain in sync.

  PARAMETERS:    None

  RET VALUES:    None

*****************************************************************************/
void IpcDspvFMBoxReset (void);

/*****************************************************************************

  FUNCTION NAME: IpcIsSsoDiscPending

  DESCRIPTION:   Return the flag indicating whether an SSO Disconnect is pending.

  PARAMETERS:    None

  RET VALUES:    TRUE if waiting for SSO DISC Response, FALSE otherwise.

*****************************************************************************/
bool IpcIsSsoDiscPending (void);
#endif

#ifdef MTK_CBP  // for remove build warning
/*****************************************************************************
  FUNCTION NAME: IpcDspmRecoveryStart

  DESCRIPTION:   This routine is called when we detect a crash caused by DSPM 
                 mailbox corruption

  PARAMETERS:    Code1 - MonFault code1
                 Code2 - MonFault code2

  RETURNED VALUES:  None.
*****************************************************************************/
void IpcDspmRecoveryStart(uint32 Code1, uint32 Code2);
#endif

#ifdef MTK_DEV_ENABLE_DSP_DUMP
/*****************************************************************************
 
  FUNCTION NAME: IpcReadMsgsFromDspm

  DESCRIPTION:

    This routine reads a message buffer from the control mailbox of 
    messages received from the Control Processor.

    Message Format: (MsgId | MsgSize | MsgData[])

  PARAMETERS:

    MsgId     - Pointer to the MsgId.
    MsgSize   - Pointer to the MsgSize
    MsgDataP  - Pointer to memory into which the control mailbox's 
                contents are written.
 
  RETURNED VALUES:
    
   None.

*****************************************************************************/
extern void IpcReadMsgsFromDspm (uint16 *MsgId, uint16 *MsgSize, uint16 *MsgDataP);

/*****************************************************************************
 
  FUNCTION NAME: IpcSendMsgsToDspm

  DESCRIPTION:

    This routine builds a message buffers in the HW mailbox and notifies theControl Processor.

  PARAMETERS:

    MsgId     - MsgId.
    MsgSize   -  MsgSize
    MsgDataP  - Pointer to memory from which the control mailbox's 
               contents are written..

  RETURNED VALUES:
    
    None

*****************************************************************************/
extern void IpcSendMsgsToDspm (uint16 MsgId, uint16 MsgSize, uint16 *MsgDataP);
#endif


#endif

/**Log information: \main\Trophy_SO73\1 2013-07-09 02:12:43 GMT jtzhang
** scbp#11737**/
/**Log information: \main\Trophy\1 2013-07-17 08:18:49 GMT jtzhang
** scbp#11737**/
