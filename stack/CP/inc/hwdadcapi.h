/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2001-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWD_ADC_API_H_
#define _HWD_ADC_API_H_
/*****************************************************************************
* 
* FILE NAME   :     HWDADCAPI.h
*
* DESCRIPTION :     ADC API constants, types, and function prototypes
*
* HISTORY     :     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Global Definitions
----------------------------------------------------------------------------*/

#define HWD_ADC_TX_OFF         0
#define HWD_ADC_TX_ON          1
#define HWD_ADC_TIMEOUT_ERROR  0xff

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
typedef enum
{
   HWD_ADC_0=0,
   HWD_ADC_1,
   HWD_ADC_2,
   HWD_ADC_3,
   HWD_ADC_4,
   HWD_ADC_5,
   HWD_ADC_6,
   HWD_ADC_7,
   HWD_ADC_8,
   HWD_ADC_MAX_ID = HWD_ADC_8, /* max external ID */
   HWD_ADC_INT_VBG=HWD_ADC_8,
   HWD_ADC_9,
   HWD_ADC_INT_VCM=HWD_ADC_9,
   HWD_ADC_TX_VCM,
   HWD_ADC_TX_I_P,
   HWD_ADC_TX_I_N,
   HWD_ADC_TX_Q_P,
   HWD_ADC_TX_Q_N,
   HWD_ADC_MIC_BIAS,
   HWD_ADC_MAX_CHANNELS
} HwdAdcChanIdT;

#define HWD_ADC_0_NUM_ACC               1
#define HWD_ADC_1_NUM_ACC               1
#define HWD_ADC_2_NUM_ACC               1
#define HWD_ADC_3_NUM_ACC               32
#define HWD_ADC_4_NUM_ACC               1
#define HWD_ADC_5_NUM_ACC               1
#define HWD_ADC_6_NUM_ACC               32
#define HWD_ADC_7_NUM_ACC               1

#define HWD_ADC_0_INTERVAL              200  /* ms */
#define HWD_ADC_1_INTERVAL              200  /* ms */
#define HWD_ADC_2_INTERVAL              200  /* ms */
#define HWD_ADC_3_INTERVAL              200  /* ms */
#define HWD_ADC_4_INTERVAL              200  /* ms */
#define HWD_ADC_5_INTERVAL              200  /* ms */
#define HWD_ADC_6_INTERVAL              200  /* ms */
#define HWD_ADC_7_INTERVAL              200  /* ms */

/* Typedef for CDMA and AMPS meas */
typedef PACKED_PREFIX struct
{
   HwdAdcChanIdT   ChanId;
   uint16          MeasResult;
   uint8           Status;
} PACKED_POSTFIX  HwdAdcMeasResponseMsgT;

typedef PACKED_PREFIX struct
{
    uint32          AdcResData;
    uint16          Interval;   /* measurement interval in ms */
    uint8           NumAcc;     /* Number of accumulations to average */
} PACKED_POSTFIX  HwdAdcMeasT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT      RspInfo;
   HwdAdcChanIdT   ChanId;
   bool            TxSync;
} PACKED_POSTFIX  HwdAdcMeasRequestMsgT;

typedef enum
{
   HWD_ADC_IDLE,
   HWD_ADC_REQUEST_PENDING,
   HWD_ADC_WAIT_FOR_TX_PCG,
   HWD_ADC_CONVERSION_IN_PROGRESS,
   HWD_ADC_CONVERSION_DONE_TX_OFF,
   HWD_ADC_CONVERSION_DONE_TX_ON,
   HWD_ADC_CONVERSION_LOCAL
} HwdAdcRequestStateT;

/* Periodic general purpose timer */ 
extern HwdAdcRequestStateT  HwdAdcRequestState;

extern HwdAdcChanIdT        HwdAdcChanId;
extern bool                 HwdAdcTxSync;

extern HwdAdcMeasRequestMsgT  HwdAdcReq[HWD_ADC_MAX_CHANNELS];
extern HwdAdcMeasT            HwdAdcMeas[HWD_ADC_MAX_CHANNELS];

/*----------------------------------------------------------------------------
 Global API Function Prototypes
----------------------------------------------------------------------------*/

/*****************************************************************************
 
  FUNCTION NAME:    HwdAdcMeasRequest

  DESCRIPTION:      This function is the API for ADC measurement requests 
                    in CDMA mode
                    
  
  PARAMETERS:       Msg -  Pointer to request data

  RETURNED VALUES:  None

*****************************************************************************/
extern void HwdAdcMeasRequest (HwdAdcMeasRequestMsgT *Msg);

/*****************************************************************************
* END OF FILE
*****************************************************************************/

#endif

/**Log information: \main\6 2012-03-23 07:08:59 GMT gliu
** update the ADC**/
