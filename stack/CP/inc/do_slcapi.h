/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
* 
* FILE NAME   : do_slcapi.h
*
* DESCRIPTION : API definition for SLC (Session Layer Control) task.
*
* HISTORY     :
*****************************************************************************/
#ifndef _DO_SLCAPI_H_
#define _DO_SLCAPI_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "do_msgdefs.h"
#include "sbpapi.h"


/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define CONFIGRSP_MESSAGEID_LEN      8
#define CONFIGRSP_TRANSID_LEN        8
#define CONFIGRSP_ATTR_LEN           8

#define CONFIGREQ_MESSAGEID_LEN      8
#define CONFIGREQ_TRANSID_LEN        8
#define CONFIGREQ_ATTR_LEN           8

#define DSA_FWD_MESSAGEID_LEN    8

#define SESSION_FILE_DIR  "sys/sf"
#define SESSION_FILE_NAME(filename)  SESSION_FILE_DIR"/"#filename

#define UATI_CONREQ_FIX		 		 

/*----------------------------------------------------------------------------
 Mailbox IDs
----------------------------------------------------------------------------*/
#define SLC_TASK_MAILBOX         EXE_MAILBOX_1_ID
/*----------------------------------------------------------------------------
     Command Message IDs, for SLC task, for SLC_CMD_MAILBOX, EXE_MAILBOX_1_ID 
     The message IDs for components shall also put in here.     
----------------------------------------------------------------------------*/
typedef enum  /*_Message_ID_define*/
{
    /* SLC segment */
    SLC_TIMER_EXPIRED_MSG = SLC_CMD_MSGID_START,
    SLC_STATE_GET_MSG,
    DHKEP_TIMER_EXPIRED_MSG,
    DHKEP_KEYCAL_RSP_MSG,
    DHKEP_SCP_RECONFIGURED_MSG,
    SLC_CMD_MSGID_LAST,

    /* SMP segment */
    SMP_CSS_ACTIVATE_MSG = SLC_CMD_MSGID_SMP_START,
    SMP_CSS_MPS_INPROGRESS_MSG,
    SMP_SCP_CONFIGURATION_FAILED_MSG,
    SMP_SCP_SESSION_INFO_MSG,
    SMP_SCP_ACTIVATE_CNF_MSG,
    SMP_SCP_GAUP_PRIOR_SESSION_CNF_MSG,
    SMP_AMP_OPENED_MSG,
    SMP_AMP_FAILURE_MSG,
    SMP_AMP_UATI_RCV_MSG,
    SMP_OMP_PRIOR_SESSION_GAUP_UPDATE_MSG,
    SMP_CCM_PERFORM_KEEPALIVE_TRANS_MSG,
    SMP_START_ETS_CMD,
    SMP_SCP_RECONFIGURED_MSG,
    SMP_SCP_COMMITTED_MSG,
    SMP_DSAR_TX_STATUS_MSG,
    SMP_ALMP_CONN_CLOSED_MSG,
    SMP_SESSION_CLOSE_ETS_CMD,
    SMP_ALMP_POWER_DOWN_MSG,
    SMP_ALMP_CONN_OPENED_MSG,
    SMP_SCP_PROT_RESET_DONE_MSG,
    SMP_SCP_SESSION_COMMITTING_IND,
    SMP_CSS_1X_CONNECTION_ST_IND,
    SMP_VAL_1X_PS_CHANGE_TO_EVDO_NOTIFY,    
#ifdef MTK_DEV_C2K_IRAT
    SMP_OMP_DEACTIVATE_MSG,
    SMP_CSS_EHRPD_SESSION_MSG,
    SMP_CSS_HRPD_SESSION_MSG,
#endif
    SMP_CMD_MSGID_LAST,

    /* SCP segment */
    SCP_SMP_ACTIVATE_MSG = SLC_CMD_MSGID_SCP_START,
    SCP_SMP_DEACTIVATE_MSG,
    SCP_SMP_GAUP_PRIOR_SESSION_REQ_MSG,
    SCP_IDP_CONNECTION_OPENED_MSG,
    SCP_CSP_CONNECTION_CLOSED_MSG,
    SCP_RUP_CONNECTION_LOST_MSG,

    SCP_PROT_CONFIG_REQ_MSG,
    SCP_PROT_RECONFIGURED_MSG,
    SCP_PROT_RECONFIG_CNF_MSG,
    SCP_PROT_CONFIG_FAIL_MSG,
    SCP_PROT_CONFIG_PAUSE_MSG,
    SCP_PROT_CONFIG_RESUME_MSG,

    SCP_SCP_NEGO_START_MSG,
    SCP_SCP_NEGO_CONTINUE_MSG,
    SCP_DSAR_TX_STATUS_MSG,

    SCP_ALMP_CONN_OPENED_MSG,
    SCP_ALMP_CONN_CLOSED_MSG,
    SCP_ALMP_CONN_RELEASED_MSG,
    SCP_ALMP_CONN_FAILED_MSG,

    SCP_CONFIG_ETS_CMD,
    SCP_SMP_SESSION_CLOSING_IND,
    SCP_EHRPD_DISABLE_ETS_CMD,
    SCP_EHRPD_ENABLE_ETS_CMD,
    SCP_CMD_MSGID_LAST,

    /* AMP segment */
    AMP_SMP_FAILED_MSG = SLC_CMD_MSGID_AMP_START,
    AMP_SMP_ACTIVATE_MSG,
    AMP_SMP_DEACTIVATE_MSG,
    AMP_RUP_IDLEHO_MSG,
    AMP_OMP_UPDATED_MSG,
    AMP_CSP_CONN_CLOSED_MSG,
    AMP_INSP_NETWORK_ACQ_MSG,
    AMP_DSAR_TX_STATUS_MSG,
    AMP_DSA_TEST_MSG,
    AMP_SCP_RECONFIGURED_MSG,
    AMP_SCP_COMMITTED_MSG,
    AMP_SMP_SESSION_CLOSING_IND,
    AMP_CSS_DATA_WAITING_MSG,
#ifdef MTK_CBP
    AMP_IDP_CHAN_CHANGED_COMPLETE_MSG,
#endif

#ifdef MTK_PLT_ON_PC_UT /* SLC UT Msgs */
    UT_SLC_DSAF_FWD_MSG,
    UT_SLC_SET_SBP_ID_MSG,
    UT_SLC_CONF_SBP_FEATURE_MSG,
#endif

    AMP_CMD_MSGID_LAST

} SlcCmdMsgT;

typedef enum {
   ATIType_BATI = 0x00,            /*Broadcast ATI*/
   ATIType_MATI,                   /*Multicast ATI*/
   ATIType_UATI,                   /*Unicast ATI*/
   ATIType_RATI,                   /*Random ATI*/
   ATIType_NULL=0xff
}ATIType;

typedef enum {
   ALIVESESSION    = 0x00,         /*retore an alive session*/
   PRIORSESSION,                   /*restore a prior session*/
   NEWSESSION,                     /*configure a new session*/
   SCFAIL,                         /*failed to configure smp required session*/
   NONSMPREQ                       /*non-smp requested session configure*/
}ActivateReqT;

typedef enum {
  SESSION_NORMAL_CLOSE = 0,
  SESSION_CLOSE_REPLY,
  SESSION_PROTOCOL_ERROR,
  SESSION_PROTOCOL_CONFIG_FAILURE,
  SESSION_PROTOCOL_NEG_ERROR,
  SESSION_CON_FAIL,
  SESSION_LOST,
  SESSION_UNREACHABLE,
  SESSION_ALL_BUSY,

  SESSION_ALL_CLOSE_REASON
}SessionCloseReasonT;

#define SECURITY_PACKET_LENGTH   64  /* security layer packet length */
/*----------------------------------------------------------------------------
     Message Formats structure
----------------------------------------------------------------------------*/

/*AMP_SMP_RESTORE_MSG: sent by the SMP==>AMP, for restoring UATI(s), from an
  unexpired but active session. The contents are read from a FILE. */

/* AMP_OMP_UPDATED_MSG: sent by the OMP==>AMP after getting sector parameters. */
typedef PACKED_PREFIX struct
{ 
   bool  bSectorParmsUpdated;
   uint8 SubnetMask;
   uint8 ColorCode;
   uint8 SectorID[16];
   bool  SecondaryColorCodeInclude;
   uint8 SecondaryColorCodeCnt;
   uint8 SecondaryColorCode[8];
   uint32 Latitude;
   uint32 Longitude;
 } PACKED_POSTFIX AmpOmpSectorParamsT;

/*SMP_SCP_SESSION_INFO_MSG: sent by SCP=>SMP to update session info */
typedef PACKED_PREFIX struct
{
  uint16 SessionConfigurationToken;
  uint8  SysRev;
} PACKED_POSTFIX SmpScpSessionInfoMsgT;

/*SMP_SCP_ACTIVATE_CNF_MSG: sent by SCP=>SMP to report the session config result */
typedef PACKED_PREFIX struct
{
  uint8   SCnf;      /* ActivateReqT */
  uint8   ConnectionVerified;  /* if SCP made a connection. In case of HARDLINK, SCP will not. 1:yes */
} PACKED_POSTFIX SmpScpActivateCnfMsgT;


/* SCP_SMP_ACTIVATE_MSG: sent by the SMP==>SCP to start session configuration */
typedef PACKED_PREFIX struct
{
  uint8        SReq; /* ActivateReqT */
  uint8        ScpVerifyConnection;
  uint16       SessionConfigurationToken;
  uint16       SessionId;
  uint8        UATI[16];
} PACKED_POSTFIX ScpSmpActivateMsgT;

/* SCP_SMP_GAUP_PRIOR_SESSION_REQ_MSG: sent by the SMP==>SCP to start GAUP prior session */
typedef PACKED_PREFIX struct
{
  uint16       SessionConfigurationToken;
  uint16       SessionId;
  uint8        UATI[16];
} PACKED_POSTFIX ScpSmpGAUPPSReqMsgT;

/* SMP_SCP_GAUP_PRIOR_SESSION_CNF_MSG: sent by SCP==>SMP to report GAUP prior session result */
typedef PACKED_PREFIX struct
{
  uint8    result;     /* 0: fail, 1: success */
} PACKED_POSTFIX SmpScpGAUPPSCnfMsgT;

/* SCP_PROT_CONFIG_FAIL_MSG: sent by protocol entities to report the configuration
   failure during negotiation */
typedef PACKED_PREFIX struct {
  uint8    protocolType;    
  uint16   subType;
  uint8    gcpFail;  /* 0xFA: gcp is the sender */
} PACKED_POSTFIX ScpProtConfigFailMsgT;
#define GCP_FAIL  0xFA


/* SCP_PROT_RECONFIGURED_MSG: sent by protocol entities to report that it has update
   its InUse instance by GAUP. SCP will update the corresponding personality */
typedef PACKED_PREFIX struct {
  uint8    protocolType;    
  uint16   subType;
} PACKED_POSTFIX ScpProtReConfiguredMsgT;

/* SCP_PROT_CONFIG_REQ_MSG: sent by protocol entities to request SCP to use GCP to
   negotiate attributes with AN. SCP will drive GCP to conduct negotiation */
typedef PACKED_PREFIX struct {
  uint8    protocolType;    
  uint16   subType;
  uint8    initRegSeq;
} PACKED_POSTFIX ScpProtConfigReqMsgT;

/* SCP_PROT_RECONFIG_CNF_MSG: sent by protocol entities to confirm its RECONFIGURED
   message process. SCP will start Commit all received these CNF from all protocols */
typedef PACKED_PREFIX struct {
  uint8    protocolType;    
  uint16   subType;
} PACKED_POSTFIX ScpProtReconfigCnfMsgT;

/* SMP_OMP_PRIOR_SESSION_GAUP_UPDATE_MSG: sent by OMP to update PriorSessionGAUP 
   flag received from overhead air message. SMP will update its local flag */
typedef PACKED_PREFIX struct {
  uint8    psGAUP;    
} PACKED_POSTFIX SmpOmpPSGAUPUpdateMsgT; 

/* SCP_PROT_CONFIG_PAUSE_MSG: sent by protocol entities to request SCP to pasuse
   sending/receiving ConfigReq/Rsp */
typedef PACKED_PREFIX struct {
  uint8    protocolType;    
  uint16   subType;
} PACKED_POSTFIX ScpProtConfigPauseMsgT;

/* SCP_PROT_CONFIG_RESUME_MSG: sent by protocol entities to request SCP to resume
   sending/receiving ConfigReq/Rsp */
typedef PACKED_PREFIX struct {
  uint8    protocolType;    
  uint16   subType;
} PACKED_POSTFIX ScpProtConfigResumeMsgT;

/* SCP_CONFIG_ETS_CMD: sent from(IOP) ETSprotocol to request SCP to setup attributes
   to specified values */
typedef PACKED_PREFIX struct {
  
  PACKED_PREFIX struct { uint16   protocolType;
                  uint8   numSubtyes;
                  uint16  subTypes[5]; } PACKED_POSTFIX  protSubtypes[5];

  uint8  configOthers;
  uint16 SupportGAUPSCToken;
  uint16 SupportConfigLock;
  uint16 PersonalityCount;
  uint8  numAppSubtypes;
  uint16 appSubtypes[12];
} PACKED_POSTFIX ScpEtsConfigCmdT;

typedef struct
{
  uint16 stream0Application;
  uint16 stream1Application;
  uint16 stream2Application;
  uint16 stream3Application;
}StreamConfigAttribT;

typedef enum
{
   SESSION_CLOSE,
   SESSION_AMPSETUP,
   SESSION_ATINIT,
   SESSION_ANINIT,
   SESSION_OPEN,
   SESSION_CLOSING
}MonSpySessionStateT;

extern MonSpySessionStateT  gSessionStateSpy;

typedef PACKED_PREFIX struct {
  uint8 status;  /* 0 - released; 1 - connected */
} PACKED_POSTFIX  Smp1XConnectionStT;

#ifdef MTK_PLT_ON_PC_UT /* SLC UT Msgs */
typedef struct
{
  c2k_sbp_id_enum sbpId;
}SlcSetSbpIdMsgT;

typedef struct
{
  c2k_sbp_status_enum feature;
  bool is_turned_on;
}SlcConfSbpFeatureMsgT;
#endif


/* global functions */
void AmpSendUATICompleteAndHWIdRsp(uint8 callerId);
uint8 GetAmpGetState(void);

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
bool AmpWaitUATIRequestTxStatus(void);
#endif

bool AmpUpdateSession(void);
void SmpDisableSessionVerification(void);

#ifdef CBP7_EHRPD
void DScpSetEHRPDConfig(bool disable);
void DScpResetEHRPDConfig(void);
#endif

#if defined (MTK_DEV_OPTIMIZE_EVL1)
/* NST: used in NST */
extern void AmpSendCcmAtiUpdateMsg(void);
extern void AmpUpdateTransList(ATIType Type, uint32 ATI);
extern void AmpAddATIToRcvList(ATIType Type, uint32 ATI);
#endif

#endif  /* _DO_SLCAPI_H_ */
/*****************************************************************************
* End of File
*****************************************************************************/


/**Log information: \main\Trophy\Trophy_yzhang_href22247\1 2013-07-30 06:37:43 GMT yzhang
** HREF#22247=>fix issue of concurrent UATIReq and ConnReq**/
/**Log information: \main\Trophy\1 2013-07-30 07:08:15 GMT jzwang
** href#22247**/
