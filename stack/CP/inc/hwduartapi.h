/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2002-2011 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDUART_H_
#define _HWDUART_H_
/*****************************************************************************
*
* FILE NAME   : hwduartapi.h
*
* DESCRIPTION : Interface definition for Uart divider
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"
#include "hwddefs.h"

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define HWD_JIT_DIV(x)      (HWD_JIT0_DIV    + (x)*0x0C)
#define HWD_JIT_CONST(x)    (HWD_JIT0_CONST  + (x)*0x0C)
#define HWD_JIT_OFFSET(x)   (HWD_JIT0_OFFSET + (x)*0x0C)

#define JIT_26_M_DIV_115200_BAUD            14
#define JIT_26_M_CONST_115200_BAUD          1
#define JIT_26_M_UART0_OFFSET_115200_BAUD   129
#define JIT_26_M_UART1_OFFSET_115200_BAUD   1

#define DIV_LSB_115200_BAUD          1
#define DIV_MSB_115200_BAUD          0

#define HWD_FCR_FIFO_TRIG_1  0x00


/*Baud Rate = (Jitter Clock)/(Div) = (Jitter Clock)/((DLM,DLL))*/
#define HWD_UART_BAUD_DIV_MSB(div) (((div)>> 8) & 0xff)
#define HWD_UART_BAUD_DIV_LSB(div) ((div) & 0xff)

#define IOP_OPT_UART_AUTO_CTS /*Move from IOP, keep the name IOP_XXX due to historical reasons */


/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
typedef int32 HwdUartDevHdl;

typedef enum
{
    UartJitter_Ref392m_57600 = 0,
    UartJitter_Ref392m_115200,
    UartJitter_Ref392m_153600,
    UartJitter_Ref392m_230400,
    UartJitter_Ref392m_460800,
    UartJitter_Ref392m_921600,
    UartJitter_Ref392m_Invalid
}HwdUartJitterRef392mT;/*Those Jitter Clocks based on 39.3216M Source Clock*/

typedef enum
{
    UartJitter_Ref260m_31M = 0, /*3.1M*/
    UartJitter_Ref260m_36M,     /*3.6M*/
    UartJitter_Ref260m_Invalid
}HwdUartJitterRef260mT;/*Those Jitter Clocks based on 260M Source Clock*/

typedef enum
{
    UartJitter_Ref32k_300 = 0,
    UartJitter_Ref32k_600,
    UartJitter_Ref32k_Invalid
}HwdUartJitterRef32kT;/*Those Jitter Clocks based on 32k Source Clock*/

typedef enum
{
   HWD_UART_19K_BAUD   = 0,
   HWD_UART_38K_BAUD,
   HWD_UART_57K_BAUD,
   HWD_UART_115K_BAUD,
   HWD_UART_153K_BAUD,
   HWD_UART_230K_BAUD,
   HWD_UART_460K_BAUD,
   HWD_UART_921K_BAUD,
   HWD_UART_9600_BAUD,
   HWD_UART_4800_BAUD,
   HWD_UART_2400_BAUD,
   HWD_UART_1200_BAUD,
   HWD_UART_600_BAUD,
   HWD_UART_300_BAUD,
   HWD_UART_31M_BAUD,
   HWD_UART_36M_BAUD,
   HWD_UART_INVALID_BAUD
} HwdUartBaudRateT_MTK;


/* ISRs procedure pointers */
typedef void (*pLISR_PROC_PTR)(uint32 IsrStatus, uint8 UartNum);
/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
/*****************************************************************************
  FUNCTION NAME:    	HwdSetBaudRate

  DESCRIPTION:     	This routine sets the requested UART's baud rate

  PARAMETERS:       	BaudRate  - Set baud rate to one of the following
                                   Uart      - Identifies which uart to be programmed

  RETURNED VALUES:  None

    Note:                      Baud Rate = (Jitter Clock)/(Div) = (Jitter Clock)/((DLM,DLL))

*****************************************************************************/
void HwdSetBaudRate( HwdUartBaudRateT_MTK BaudRate, uint8 UartNum );


/****************************************************************************
*
* Name:          Uart_Configure_TxRx_Pin()
*
* Description:    Configure TX and RX pin for proper UART port to connect with PC.
*
* Parameters:   [in]uint8                      UartNum;
*
* Returns:        None;
*
* Notes:
*
****************************************************************************/
void Uart_Configure_TxRx_Pin( uint8 UartNum );



/****************************************************************************
*
* Name:          UartAssertDTR()
*
* Description:    Assert DTR line, make it be low 0, to alow the RX flow.
*
* Parameters:   [in]uint8                      UartNum;
*
* Returns:        None;
*
* Notes:   1. USE CTS/DTR line on HW to act protocal CTS/RTS flow control function
*               2. if MCR bit 5 HWD_MCR_AUTO_CTS, CTS/DTR line default is 1, high, on HW.
*                   which means HW is defaultly porbidden transmitting if HWD_MCR_AUTO_CTS is set.
*                   (HWD_MCR_AUTO_CTS is a swtich of flow control function, make it on/off)
*
****************************************************************************/
void UartAssertDTR(uint8 UartNum);

/****************************************************************************
*
* Name:          UartDeassertDTR()
*
* Description:     De-Assert DTR line, make it be high 1, to forbid the RX flow.
*
* Parameters:   [in]uint8                      UartNum;
*
* Returns:        None;
*
* Notes:   1. USE CTS/DTR line on HW to act protocal CTS/RTS flow control function
*               2. if MCR bit 5 HWD_MCR_AUTO_CTS, CTS/DTR line default is 1, high, on HW.
*                   which means HW is defaultly porbid transmitting if HWD_MCR_AUTO_CTS is set.
*                   (HWD_MCR_AUTO_CTS is a swtich of flow control function, make it on/off)
*
****************************************************************************/
void UartDeassertDTR(uint8 UartNum);

/****************************************************************************
*
* Name:          UartEnableFlowCntl()
*
* Description:    This routine enables flow control when connect to PC, including setting flow control
*                        gpios to funtional mode. Function will start with RX flow stopped.
*
* Parameters:   [in]uint8                      UartNum;
*
* Returns:        None;
*
* Notes:
*
****************************************************************************/
void UartEnableFlowCntl(uint8 UartNum);
/*****************************************************************************

  FUNCTION NAME:   HwdUartJitterClkCtrl

  DESCRIPTION:     Enables or disables the particular Uart clock

  PARAMETERS:      uint8    UartNumber
                   bool     State

  RETURNED VALUES: None

*****************************************************************************/
extern void HwdUartJitterClkCtrl( uint8 UartNumber, bool State );

/*****************************************************************************
* $Log: hwduartapi.h $
* Revision 1.2  2004/03/25 11:45:56  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.2  2003/05/29 17:35:06  agontar
* Made changes to support high baud rate and 7-bit offset on uart 0.
* Revision 1.1  2003/05/12 15:26:19  fpeng
* Initial revision
*
*
* Revision 1.3  2002/06/04 08:07:15  mshaver
* Added VIA Technologies copyright notice.
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:17:47 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\1 2013-12-10 08:33:16 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
