/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2006-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef VALSMSAPI_H
#define VALSMSAPI_H

#ifdef __cplusplus
extern "C" {
#endif

/*===========================================================================

                 V A L   S M S   A P I   H E A D E R    F I L E

  This file contains the exported interfaces for the  SMS
  module of the VIA Abstraction Layer.


===========================================================================*/

#include "sysdefs.h"
#include "valapi.h"
#include "valsmsdefine.h"


#define VAL_SMS_MAX_REGISTERED_IDS           10      /*!< Max registered app */
#define VAL_SMS_TELESRVID_COUNT              20      /*!< Max registered teleservice id */
#define VAL_SMS_MAX_SMS_PDU_LEN             253      /*!< Max sms pdu length */
#define VAL_SMS_CLASS_SUCESS        0
#define VAL_SMS_CAUSECODE_SUCESS   32768
#define VAL_SMS_MOSEQ_ID_MAX   0xFE
#define VAL_MAX_SAVED_SMS_ID_PAIR 20
#define VAL_SMS_MO_TYPE_3GPP2    01
#define VAL_SMS_TRANS_LEN  10
#define VAL_SMS_RSP_MAX_SMS_PDU_LEN 255
#ifdef MTK_CBP_ENCRYPT_VOICE
#define MODE_MANUAL  0
#define MODE_AUTO    1
#define NUMBER_MAX_LEN  11

typedef enum
{
  VAL_SMS_MODE_ERROR,
  VAL_SMS_DECRYPT_ERROR,
  VAL_SMS_SIGNATURE_VERFY_ERROR,
  VAL_SMS_SEND_FAIL,
  VAL_SMS_OTHER_ERROR,
} ValSmsReportErrClassT;

#endif

#define VAL_SMS_CBS_CHA_MAX 4

#ifdef MTK_CBP
typedef struct {
    bool    CbsState;                           /* FALSE, CBS off; TRUE, CBS on */
    uint32  LanMask;                            /* lanuage bitmap */
    uint32  ChaMask[VAL_SMS_CBS_CHA_MAX];       /* channels bitmap */
    uint32  CmasMask;                           /* cmas bitmap */
} ValSmsCbsInfoT;
#endif

typedef enum
{
  VAL_SMS_CHANNEL_DEFAULT,
  VAL_SMS_CHANNEL_TC,
  VAL_SMS_MAX_CHANNEL
} ValSmsChannelT;

/*! Sms in PDU format*/
typedef  struct
{
  uint8 SmsState;
  #ifdef MTK_CBP_ENCRYPT_VOICE
  bool CryptSmsPres;
  #endif
  uint8 SmsLength;
  uint8 SmsData[VAL_SMS_MAX_SMS_PDU_LEN];
} ValSmsPduRecordT;

typedef struct
{
  uint16 SeqNum;
  ValSmsPduRecordT PduRecord;
}ValSmsRecordT;

/*! Val sms sending info */
typedef PACKED_PREFIX struct
{
  bool   Acked;         /*!< Acked flag */
  uint16 MoSeqNum;      /*!< Sending equence number */
#ifdef SYS_OPTION_NO_UI  
  uint16 StoredRecId;
#endif  
} PACKED_POSTFIX  ValSmsSendInfoT;

/*! Val sms Updating status */
typedef enum
{
  VAL_SMS_UPDATE_NONE,   /*!< Free for update */
  VAL_SMS_UPDATE_BUSY    /*!< Busy for update */
}ValSmsUpdateStatusIdT;

/*! Val sms update info */
typedef PACKED_PREFIX struct
{
  uint16 UpdateRecId;                   /*!< Update recored id */        
  ValSmsUpdateStatusIdT UpdateStatus;   /*!< Update status */
} PACKED_POSTFIX  ValSmsUpdateInfoT;

#ifdef SYS_OPTION_NO_UI
typedef enum
{
  VAL_SMS_EVENT_INCOMING_MESSAGE       = 0x00000001,
  VAL_SMS_EVENT_SEND_STATUS_MESSAGE,
  VAL_SMS_EVENT_MEM_STATUS_MESSAGE,
  VAL_SMS_EVENT_SMS_WRITE_ERR_MESSAGE,     /*incoming msg write error event*/
  VAL_SMS_EVENT_SMS_SRV_READY_MESSAGE,     /*the vsms service is ready*/
  VAL_SMS_EVENT_SMS_RECEIVED_BUT_MEM_FULL, /*received a msg, but memory full, so the msg lost*/
  VAL_SMS_MAX_EVENTS
} ValSmsEventIdT;
#else
typedef uint32 ValSmsEventIdT;
#endif
/*! SMS callback function structure */
typedef void (*ValSmsEventFunc) ( RegIdT RegId, ValSmsEventIdT Event, void *MsgP );

/*! SMS Event register table structure */
typedef struct
{
  bool            IsUse;            /*!< if this entry is in use */
  uint8           NumTeleSrvId;     /*!< Teleservice Id count */
  uint16          TeleSrvId[VAL_SMS_TELESRVID_COUNT]; /*!< Teleservice id */
  ValSmsEventFunc CallBack;         /*!< Function to call back */
} ValSmsRegTableT;

/*! VAL SMS general result type   */
typedef enum
{
  VAL_SMS_SUCCESS,
  VAL_SMS_ERR_SMS_NOT_READY,
  VAL_SMS_ERR_MSG_FORMAT,
  VAL_SMS_ERR_INVALID_PARAMETER,
  VAL_SMS_ERR_BUSY_SENDING, /* while a register application send a sms message while the previous was not acknowleged*/
  VAL_SMS_ERR_DESTINATION_BUSY,
  VAL_SMS_ERR_NO_MEMORY,
  VAL_SMS_FDN_APPROVE_REQ_SENT,
  VAL_SMS_FDN_APPROVE_FAILED = 14,
  VAL_SMS_ERR_OTHER_FAILED
} ValSmsResultT;

/*! Sms terminal status */
typedef enum
{
  VAL_SMS_NO_TERMINAL_PROBLEMS,
  VAL_SMS_DESTINATION_RESOURSE_SHORTAGE,
  VAL_SMS_DESTINATION_OUT_OF_SERVICE,
  VAL_SMS_MAX_TERM_STATUS
} ValSmsTermStatusT;

#define IS_VALID_SMS_TERM_STATUS(a)  ((VAL_SMS_NO_TERMINAL_PROBLEMS <= (a)) && ((a) < VAL_SMS_MAX_TERM_STATUS))

/*! Val sms cause code define */
typedef enum
{
  VAL_SMS_CC_ADDRESS_VACANT                   = 0,
  VAL_SMS_CC_ADDRESS_TRANSLATION_FAILURE      = 1,
  VAL_SMS_CC_NETWORK_RESOURCE_SHORTAGE        = 2,
  VAL_SMS_CC_NETWORK_FAILURE                  = 3,
  VAL_SMS_CC_INVALID_TELESERVICE_ID           = 4,
  VAL_SMS_CC_OTHER_NETWORK_PROBLEM            = 5,
  VAL_SMS_CC_OTHER_NETWORK_PROBLEM_MORE_FIRST = 6,
  VAL_SMS_CC_OTHER_NETWORK_PROBLEM_MORE_LAST  = 31,
  VAL_SMS_CC_NO_PAGE_RESPONSE_S               = 32,
  VAL_SMS_CC_DESTINATION_BUSY                 = 33,
  VAL_SMS_CC_NO_ACKNOWLEDGEMENT               = 34,
  VAL_SMS_CC_DESTINATION_RESOURCE_SHORTAGE    = 35,
  VAL_SMS_CC_SMS_DELIVERY_POSTPONED           = 36,
  VAL_SMS_CC_DESTINATION_OUT_OF_SERVICE       = 37,
  VAL_SMS_CC_DESTINATION_NO_LONGER_AT_THIS_ADDRESS = 38,
  VAL_SMS_CC_OTHER_TERMINAL_PROBLEM                = 39,
  VAL_SMS_CC_OTHER_TERMINAL_PROBLEM_FIRST          = 40,
  VAL_SMS_CC_OTHER_TERMINAL_PROBLEM_LAST           = 47,
  VAL_SMS_CC_SMS_DELIVER_POSTPONED_MORE_FIRST      = 48,
  VAL_SMS_CC_SMS_DELIVER_POSTPONED_MORE_LAST       = 63,
  VAL_SMS_CC_RADIO_INTERFACE_RESOURCE_SHORTAGE     = 64,
  VAL_SMS_CC_RADIO_INTERFACE_INCOMPATIBILITY       = 65,
  VAL_SMS_CC_OTHER_RADIO_INTERFACE_PROBLEM         = 66,
  VAL_SMS_CC_OTHER_RADIO_IF_PROBLEM_MORE_FIRST     = 67,
  VAL_SMS_CC_OTHER_RADIO_IF_PROBLEM_MORE_LAST      = 95,
  VAL_SMS_CC_ENCODING_PROBLEM                      = 96,
  VAL_SMS_CC_SMS_ORIGINATION_DENIED                = 97,
  VAL_SMS_CC_SMS_TERMINATION_DENIED                = 98,
  VAL_SMS_CC_SUPPLEMENTARY_SERVICE_NOT_SUPPORTED   = 99,
  VAL_SMS_CC_SMS_NOT_SUPPORTED                     = 100,
  VAL_SMS_CC_RESERVED                              = 101,
  VAL_SMS_CC_MISSING_EXPECTED_PARAMETER            = 102,
  VAL_SMS_CC_MISSING_MANDATORY_PARAMETER           = 103,
  VAL_SMS_CC_UNRECOGNIZED_PARAMETER_VALUE          = 104,
  VAL_SMS_CC_UNEXPECTED_PARAMETER_VALUE            = 105,
  VAL_SMS_CC_USER_DATA_SIZE_ERROR                  = 106,
  VAL_SMS_CC_OTHER_GENERAL_PROBLEMS                = 107,
  VAL_SMS_CC_OTHER_GENERAL_PROBLEMS_MORE_FIRST     = 108,
  VAL_SMS_CC_OTHER_GENERAL_PROBLEMS_MORE_LAST      = 255,
  VAL_SMS_CC_MESSAGE_DELIVERED_SUCCESSFULLY        = 32768,
  VAL_SMS_CC_WAITING_FOR_TL_ACK                    = 32769,
  VAL_SMS_CC_OUT_OF_RESOURCES                      = 32770,
  VAL_SMS_CC_ACCESS_TOO_LARGE                      = 32771,
  VAL_SMS_CC_DRC_TOO_LARGE                         = 32772,
  VAL_SMS_CC_NETWORK_NOT_READY                     = 32773,
  VAL_SMS_CC_PHONE_NOT_READY                       = 32774,
  VAL_SMS_CC_NOT_ALLOWED_IN_AMPS                   = 32775,
  VAL_SMS_CC_NOT_SUPPORTED                         = 32776,
  VAL_SMS_CC_INVALID_TRANSACTION                   = 32777,
  VAL_SMS_CC_MESSAGE_NOT_SENT                      = 32778,
  VAL_SMS_CC_MESSAGE_BEGIN_SENT                    = 32779,
  VAL_SMS_MAX_CC
} ValSmsCCT;

/*! Macro for validating ValSmsCauseCodeT */
#define IS_VALID_SMS_CAUSE_CODE(a)                              \
     (((VAL_SMS_CC_ADDRESS_VACANT == (a)) || ((VAL_SMS_CC_ADDRESS_VACANT <(a)) &&                    \
                                         ((a) <= VAL_SMS_CC_OTHER_NETWORK_PROBLEM))) || \
      ((VAL_SMS_CC_NO_PAGE_RESPONSE_S <= (a)) &&                \
                                       ((a) <= VAL_SMS_CC_OTHER_TERMINAL_PROBLEM))  || \
      ((VAL_SMS_CC_RADIO_INTERFACE_RESOURCE_SHORTAGE <= (a)) && \
                                             ((a) <= VAL_SMS_CC_SMS_NOT_SUPPORTED)) || \
      ((VAL_SMS_CC_MISSING_EXPECTED_PARAMETER <= (a)) && ((a) < VAL_SMS_MAX_CC)))


/*! Send status */
/*val sms event message type for ui*/
typedef PACKED_PREFIX struct
{
  ValSmsRecIdT recid;
} PACKED_POSTFIX  ValSmsIncomingMsgT;

typedef PACKED_PREFIX struct
{
  uint8 SeqNum;       /* The PSW-MMI SeqNum id used in msg assembly  */
  ValSmsErrClassT  ErrClass;
  ValSmsCauseCodeT CauseCode;
} PACKED_POSTFIX  ValSmsSendStatusMsgT;

/* val message type for sms uint */
typedef PACKED_PREFIX struct
{
  ValDeviceT device;
  bool       MemStatusFull;      /*TRUE for memory full, FALSE for memory enough */
} PACKED_POSTFIX  ValSmsMemStatMsgT;

typedef PACKED_PREFIX struct
{
  bool       SmsMemFull;   /*TRUE for no memory, FALSE for memory enough*/
  ValDeviceT device;
} PACKED_POSTFIX  ValSmsMemStatusMsgT;

typedef PACKED_PREFIX struct
{
  ValDeviceT device;
} PACKED_POSTFIX  ValSmsWriteErrMsgT;

/*----------------------------------
message storage type define
------------------------------------*/

typedef enum
{
 /*VAL SMS Msg type               teleservice value in IS-637:*/

  MSG_IS91_PAGE_MSG,
  MSG_IS91_VOICE_MAIL,        /* analog mode teleservice */
  MSG_IS91_SHORT_MESSAG,
  MSG_VOICE_MAIL_MWI,

  MSG_TELE_SRV_IS91,    /* IS91_TS_ID_IS91           = 4096, */
  MSG_TELE_SRV_PAGE,    /* VAL_SMS_TS_ID_PAGE        = 4097, */
  MSG_TELE_SRV_MESSAGE, /* VAL_SMS_TS_ID_MESSAGE     = 4098, */
  MSG_TELE_SRV_VMN,     /* VAL_SMS_TS_ID_VOICE_MAIL  = 4099, */
  MSG_TELE_SRV_WAP,     /* VAL_SMS_TS_ID_WAP,    */
  MSG_TELE_SRV_WEMT,    /* VAL_SMS_TS_ID_WEMT,   */
  MSG_TELE_SRV_SCPT,    /* VAL_SMS_TS_ID_SCPT,   */
  MSG_TELE_SRV_CATPT,   /* VAL_SMS_TS_ID_CATPT   */
  MSG_BROAD_CAST_SRV,   /* broadcast SMS service, not a teleservice, here for the purpose of  management for all SMS service */
#if defined(LGT_EXTENSIONS)
  MSG_TELE_SRV_UNKNOWN,   /* VAL_SMS_LGT_UNKNOWN   */
#endif
  MSG_TELE_SRV_UNICOM,
  MSG_AUTO_REG_SRV,
  MSG_AUTO_DM_PUSH,
  MSG_TELE_SRV_GPS,
  MSG_TELE_SRV_DANDCN = 4242,  
  MAX_REG_MSG
} ValSmsTeleSrvType;

typedef PACKED_PREFIX struct
{
  bool isUse;
#ifdef SYS_OPTION_NO_UI  
  ValSmsTeleSrvIdT TeleSrvId;
#else
  uint16 TeleSrvId;
#endif  
  uint8            RegIdCount;
  bool             NeedSave;
  uint8            NeedSaveCount;
} PACKED_POSTFIX  RegEvtInfoT;


/*the memory status*/
typedef PACKED_PREFIX struct
{
  uint16 nFlashMaxSmsRec;
  uint16 nFlashFreeSmsRec;
  uint16 nUimMaxSmsRec;
  uint16 nUimFreeSmsRec;
} PACKED_POSTFIX  ValSmsStorParamsT;


typedef struct
{
  int16 refNum;
  uint8  maxNum;
  uint8  seqNum;
  uint8  numSegments;
  uint16 recid;
}ValEmsHeaderT;

typedef enum
{
  Unused      =0,
  RxUnReadBox,
  RxReadBox,
  TxSentBox,
  DraftBox,
  NUM_ETS_SMS_BOX
} ValSmsEtsMsgTypeT;


typedef enum
{
  SMS_ETS_EVT_ADDED,
  SMS_ETS_EVT_DELETED,
  SMS_ETS_EVT_MODIFIED,
  SMS_ETS_EVT_RECEIVED,
  SMS_ETS_EVT_SENT
} ValSmsEtsEventT;

typedef enum
{
  /*=== General errors ===*/
  
  UI_General_SUCCESS,
  UI_General_Index_Out_Of_Range,
  UI_General_Not_Implemented,
  UI_General_Storage_Full,
  UI_General_Empty_Index,
  
  /*=== SMS errors ===*/
  
  /*Network Problems*/
  UI_SMS_ADDRESS_VACANT,
  UI_SMS_ADDRESS_TRANSLATION, 
  UI_SMS_NETWORK_RESOURCE_SHORTAGE,
  UI_SMS_NETWORK_FAILURE,  
  UI_SMS_INVALID_TELESERVICE_ID,
  UI_SMS_OTHER_NETWORK_PROBLEM,
  UI_SMS_OTHER_NETWORK_PROBLEM_MORE_FIRST,
  UI_SMS_OTHER_NETWORK_PROBLEM_MORE_LAST,
  
  /*General Problems*/
  UI_SMS_NO_PAGE_RESPONSE,  
  UI_SMS_DESTINATION_BUSY,  
  UI_SMS_NO_ACKNOWLEDGEMENT,  
  UI_SMS_DESTINATION_RESOURCE_SHORTAGE,  
  UI_SMS_DELIVERY_POSTPONED,  
  UI_SMS_DESTINATION_OUT_OF_SERVICE,
  UI_SMS_DESTINATION_NO_LONGER_AT_THIS_ADDRESS,
  UI_SMS_OTHER_TERMINAL_PROBLEM,
  UI_SMS_OTHER_TERMINAL_PROBLEM_MORE_FIRST,
  UI_SMS_OTHER_TERMINAL_PROBLEM_MORE_LAST,
  UI_SMS_DELIVER_POSTPONED_MORE_FIRST,
  UI_SMS_DELIVER_POSTPONED_MORE_LAST,
  UI_SMS_RADIO_INTERFACE_RESOURCE_SHORTAGE,
  UI_SMS_RADIO_INTERFACE_INCOMPATIBILITY,
  UI_SMS_OTHER_RADIO_INTERFACE_PROBLEM,
  UI_SMS_OTHER_RADIO_INTERFACE_PROBLEM_MORE_FIRST,
  UI_SMS_OTHER_RADIO_INTERFACE_PROBLEM_MORE_LAST,
  UI_SMS_ENCODING_PROBLEM,
  UI_SMS_SUPPLEMENTARY_SERVICE_NOT_SUPPORTED,
  UI_SMS_RESERVED,
  UI_SMS_MISSING_EXPECTED_PARAMETER,
  UI_SMS_MISSING_MANDATORY_PARAMETER,
  UI_SMS_UNRECOGNIZED_PARAMATER_VALUE,
  UI_SMS_UNEXPECTED_PARAMATER_VALUE,
  UI_SMS_OTHER_GENERAL_PROBLEMS,
  UI_SMS_OTHER_GENERAL_PROBLEMS_MORE_FIRST,
  UI_SMS_OTHER_GENERAL_PROBLEMS_MORE_LAST,
  
  /*SERVICE NOT AVAILABLE*/
  UI_SMS_ORIGINATION_DENIED,
  
  /*INVALID DESTINATION*/
  UI_SMS_TERMINATION_DENIED,
  
  /*SERVICE NOT SUPPORTED*/
  UI_SMS_NOT_SUPPORTED,
  
  /*MESSAGE TOO LONG FOR NETWORK*/
  UI_SMS_USER_DATA_SIZE_ERROR,
  
  /*Other Messages*/
  UI_SMS_WAITING_FOR_TL_ACK,
  UI_SMS_OUT_OF_RESOURCES,
  UI_SMS_ACCESS_TOO_LARGE,
  UI_SMS_DRC_TOO_LARGE,
  UI_SMS_NETWORK_NOT_READY,
  UI_SMS_PHONE_NOT_READY,
  UI_SMS_NOT_ALLOWED_IN_AMPS,
  UI_SMS_INVALID_TRANSACTION,
  UI_SMS_MESSAGE_NOT_SENT,
  UI_SMS_MESSAGE_BEGIN_SENT,
  UI_SMS_MAX_NUMBER_OF_DESTINATIONS_IS_10,
  UI_SMS_MESSAGE_DELIVERY_FAILED
} ValSmsEtsStatT;

/* VAL_SMS_GET_COUNT_MSG */
typedef PACKED_PREFIX struct
{
  uint8 Storage;
} PACKED_POSTFIX  ValSmsEtsGetCountMsgT;

typedef PACKED_PREFIX struct
{
  uint32 MsgCount[NUM_ETS_SMS_BOX];
} PACKED_POSTFIX  ValSmsEtsGetCountRspMsgT;

/* VAL_SMS_GET_FIRST_MSG */
typedef PACKED_PREFIX struct
{
  uint8 Type;
  uint8 Storage;
} PACKED_POSTFIX  ValSmsEtsGetFirstMsgT;

typedef PACKED_PREFIX struct
{
  uint8 Status;
  uint8 Type;
  uint8 Storage;
  uint16 Index;
  uint8 EmsFlag;
  uint8 EmsSegment;
  uint8 EmsLast;
  uint8 PhbType;
  uint8 PhbByteCount;
  uint8 Pb_Pdu_data[1];
} PACKED_POSTFIX  ValSmsEtsGetMsgRspMsgT;

/* VAL_SMS_GET_NEXT_MSG */

/* VAL_SMS_WRITE_MSG */
typedef PACKED_PREFIX struct
{
  uint8 Type;
  uint8 Storage;
  uint16 Index;    /* for EMS only? */
  uint8 EmsFlag;
  uint8 EmsSegment;
  uint8 EmsLast;
  uint8 Pdu[1];
} PACKED_POSTFIX  ValSmsEtsWriteMsgT;

typedef PACKED_PREFIX struct
{
  uint8  Status;
  uint8  Storage;
  uint16 Index;
  uint8  EmsSegment;
  uint16 RefNum;
} PACKED_POSTFIX  ValSmsEtsWriteRspMsgT;

/* VAL_SMS_DEL_ALL_MSG */
typedef PACKED_PREFIX struct
{
  uint8 Type;
  uint8 Storage;
} PACKED_POSTFIX  ValSmsEtsDelAllMsgT;

typedef PACKED_PREFIX struct
{
  uint8 Status;
  uint8 Type;
  uint8 Storage;
} PACKED_POSTFIX  ValSmsEtsDelAllRspMsgT;

/* VAL_SMS_DEL_MSG */
typedef PACKED_PREFIX struct
{
  uint16 Index;
  uint8  Storage;
} PACKED_POSTFIX  ValSmsEtsDelMsgT;

typedef PACKED_PREFIX struct
{
  uint8 Status;
  uint16 Index;
  uint8 Storage;
} PACKED_POSTFIX  ValSmsEtsDelRspMsgT;

/* VAL_SMS_SEND_MSG */
typedef PACKED_PREFIX struct
{
  uint8 Storage;
  uint16 Index;    /* for EMS only? */
  uint8 EmsFlag;
  uint8 EmsSegment;
  uint8 EmsLast;
  uint8 Save;
  uint8 Confirmation;
  uint8 Pdu[1];
} PACKED_POSTFIX  ValSmsEtsSendMsgT;

typedef PACKED_PREFIX struct
{   
  uint8  Status;
  uint8  Storage;
  uint16 Index;
  uint8  EmsSegment;
  uint16 RefNum;
#ifdef MTK_CBP
  uint8  ErrorClass;
#endif
} PACKED_POSTFIX  ValSmsEtsSendRspMsgT;

/* VAL_SMS_SEND_INDEX_MSG */
typedef PACKED_PREFIX struct
{
  uint8  Storage;
  uint16 Index;
  uint8 Save;
  uint8 Confirmation;
} PACKED_POSTFIX  ValSmsEtsSendIndexMsgT;

typedef PACKED_PREFIX struct
{
  uint8  Status;
  uint8  Storage;
  uint16 Index;
} PACKED_POSTFIX  ValSmsEtsSendIndexRspMsgT;

/* VAL_SMS_SET_STATUS_MSG */
typedef PACKED_PREFIX struct
{
  uint8  Storage;
  uint16 Index;
  bool   Read;
} PACKED_POSTFIX  ValSmsEtsSetStatusMsgT;

typedef PACKED_PREFIX struct
{
  uint8  Status;
  uint8  Storage;
  uint16 Index;
} PACKED_POSTFIX  ValSmsEtsSetStatusRspMsgT;

/* VAL_SMS_SPY_MSG */
typedef PACKED_PREFIX struct
{
  uint8  Event;
  uint8  Storage;
  uint16 Index;
} PACKED_POSTFIX  ValSmsEtsSpyMsgT;
#ifdef MTK_CBP_ENCRYPT_VOICE
typedef PACKED_PREFIX struct
{
   uint8 len;
   uint8 CallNum[NUMBER_MAX_LEN];
} PACKED_POSTFIX  ValSmsMocKeyReqMsgT;
typedef PACKED_PREFIX struct
{
   uint8 OpType;
   uint8 Result;
} PACKED_POSTFIX  ValSmsRemoteCtrlInsRspMsgT;
typedef ValSmsRemoteCtrlInsRspMsgT ValEncrySmsRemoteCtrlInsRspMsgT;
typedef PACKED_PREFIX struct
{
   uint8 len;
   uint8 CallNum[NUMBER_MAX_LEN];
} PACKED_POSTFIX  ValMtEncryptNotificationIndMsgT;

typedef PACKED_PREFIX struct
{
   uint8 keyReqtype;/*1 MO,0 MT*/
   uint8 mode; /*0: MANUAL,1: auto*/
   uint8 calledNum[NUMBER_MAX_LEN];/*number ,max =11*/
   uint8 calledBytes;                                            
} PACKED_POSTFIX  ValSmsEncryKeyReqMsgT;
typedef PACKED_PREFIX struct
{
   uint8 mode;/*1 MO,0 MT*/                                       
} PACKED_POSTFIX  ValSmsModuleClearContextMsgT;

#endif

typedef PACKED_PREFIX struct { 
   uint8  sms_format;    /* reply sequence used in bearer reply opt */
   uint8  sms_status;
   uint16 msg_id;
   uint8 seq_id; /* SMS sequence Number */
   BOOL  is_used;
   uint8 pduLen;
   uint8 *pduPt;
} PACKED_POSTFIX ValSmsTransAseT;

typedef PACKED_PREFIX struct { 
   uint16 msg_id;
   uint16 seq_id;
   BOOL is_used;
} PACKED_POSTFIX ValSmsIdPairT;

typedef PACKED_PREFIX struct {
    
   uint8         send_over_ims_cs;  
   uint16        length;   
   uint8         pdu[VAL_SMS_RSP_MAX_SMS_PDU_LEN];
   bool          is_no_error; //TRUE: no error, FALSE: has error
} PACKED_POSTFIX ValC2kSmsRspT;


/*****************************************************************************

  FUNCTION NAME: ValSmsGetStorParams

  DESCRIPTION:

      To get current storage memory statue parameters.

  PARAMETERS:

   pSmsStorParams: the buffer where to write the ValSmsStorParamsT info back

  RETURNED VALUES:
    success is true.failed is false

*****************************************************************************/
bool ValSmsGetStorParams( ValSmsStorParamsT* pSmsStorParams );

/*! Macro for valid sms service option */
#define IS_VALID_SMS_SO(a)   (((a) == 0) || ((a) == VAL_SERVICE_OPTION_6) || ((a) == VAL_SERVICE_OPTION_14))

void ValSmsInitUimSms( void );
void ValSmsInit( void );
bool ValSmsProcessMsg( uint32 MsgId, void *MsgBufP, uint32 MsgLen );
#ifdef MTK_CBP
void ValSmsSendCbsCfgToPsw(ValSmsCbsInfoT* CbsInfo);
#endif

RegIdT ValSmsRegister( ValSmsEventFunc CallBack );
//don't modify following Function, brew has referred to it
ValSmsResultT ValSmsUnregister( RegIdT RegId );
ValSmsResultT ValSmsStartRegSrv( RegIdT RegId, uint16  TeleSrvId);
ValSmsResultT ValSmsStopRegSrv( RegIdT RegId, uint16 TeleId );

//don't modify following Function, brew has referred to it
bool ValSmsGetSmsStatus( void );
void ValSmsSetSmsStatus(bool Flg);

void ValSmsSetChannel( bool channel );
ValSmsResultT ValSmsSetPrefSvcOpt( ValServiceOptionT SmsSo );
ValSmsResultT ValSmsSetBroadcastParams( uint32 SvcMask, uint32 LangMask, uint8  Priority );
ValSmsResultT ValSmsUpdateTxtMessage(ValSmsTxtRecordT *pSmsTxtMsg,uint16 recId);
ValSmsResultT ValSmsUpdatePduMessage(ValSmsPduRecordT *pSmsRecord,uint16 recId);
ValSmsResultT ValSmsReadTxtMessage(ValSmsTxtRecordT *pSmsMessage,uint16 recId);
ValSmsResultT ValSmsReadPduMessage(ValSmsPduRecordT *pSmsRecord,uint16 recId);

#ifdef  SYS_OPTION_NO_UI
ValSmsResultT ValSmsSendPduMessage( RegIdT RegId, ValSmsRecIdT*  recidP, uint8* MsgBufP, uint8 nbytes, uint16* MsgId, bool ToBeSave );
ValSmsResultT ValSmsSendTxtMessage( RegIdT   RegId, ValSmsRecIdT* recid,  ValSmsTxtRecordT*   SmsTxtMsgP, uint16* MsgId, bool ToBeSave);
#else
ValSmsResultT ValSmsSendPduMessage(RegIdT RegId, uint8 *MsgBufP, uint8 MsgLength);
ValSmsResultT ValSmsSendTxtMessage(RegIdT RegId, ValSmsTxtRecordT *TxtRecord);
#endif
bool ValSmsProcessMsg( uint32 MsgId, void*  MsgBuf, uint32 MsgLen );
void ValSmsSendAck(ValSmsResultT Result, uint32 SeqNumber);

ValSmsTeleSrvIdT ValSmsGetTeleSrvFromPdu(uint8 *DataP, uint8 Length);

bool CheckSmsPdu(uint8 *ValSmsPduP, uint8 Length);
ValDeviceT ValSmsGetStorage( void );
void ValSmsSetMultiSMSMode(bool IsMultiSMS);

int  ValSmsSendMessage( ValSmsTxtRecordT *MessageP );



#ifdef MTK_DEV_C2K_IRAT
void valProcSdmC2kSmsSendReqMsg(uint16 length,uint8* pdu);
void valProcSdmC2kSmsOverImsSendReqMsg(uint16 length,uint8 *pdu);
void valProcImcsmsC2kSmsSendReqMsg(uint16 length,uint8 *pdu);
void valProcSdmC2kSmsSendIndMsg(uint8 format,uint16 length,uint8* pdu);
void valProcSdmC2kSmsSendRspMsg(uint8 send_over_ims_cs,uint16 length,uint8 *pdu);
void valProcSdmC2kSmsRspMsg(uint16 len,ValC2kSmsRspT* pValSmsRsp);
void valProcUiccPPdownLoadRspMsg(uint8 *pdu,uint8 length,uint8 seq_id);
void valPreProcPPdownloadMsg(uint8 *pdu,uint8 length,uint8 seq_id);
void valProcUtkSmsMsg(uint8 *pdu,uint8 length);
void valProcImcsmsC2kSmsRecvReqMsg(uint16 length,uint8 *data);

#endif

void CNMASendSMSMsg(uint8 length, uint8 *pduBuf,uint16 seqNum);


#ifdef __cplusplus
}
#endif /* __cplusplus */


#endif  /* VALSMSAPI_H */


