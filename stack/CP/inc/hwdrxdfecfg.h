/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _HWDRXDFE_CFG_H_
#define _HWDRXDFE_CFG_H_

/***********************************************************************************
* 
* FILE NAME   :     hwddfecfg.h
*
* DESCRIPTION :     RX DFE signal interface defination
*
* HISTORY     :
*     See Log at end of file
*
************************************************************************************/
#ifdef MTK_CBP

#ifdef MTK_PLT_DENALI
#include "hwdrxdfedenali.h"
#endif

#if (SYS_BOARD >= SB_JADE)
#include "hwdrxdfejade.h"
#endif


/*----------------------------------------------------------------------------
 Global Defines 
----------------------------------------------------------------------------*/
#define HWD_RXDFE_PATH_NUM                  (HWD_RF_MAX_NUM_OF_RX)
#define HWD_RX_GAIN_STATE_MAX               (HWD_RF_RX_GAIN_STEP_MAX_NUM)

#define HWD_RXDFE_INVALID_GAINUP_THRESHOlD (0)

/*EVEREST DC work around for HW bug*/
/*Compensate -0.4dB, 10*log(29884/(2^15)) */
#if(SYS_BOARD == SB_EVEREST)
#define HWD_RXDFE_EVEREST_DC_FILTER_COMP 29884
#endif

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/

typedef enum
{
   HWD_RXDFE_PW_MODE_H = 0,
   HWD_RXDFE_PW_MODE_L,
   HWD_RXDFE_PW_MODE_NUM
}HwdRxDfePowerMode;


/* Definition for RX DFE Gain Up */
typedef struct 
{
    HwdRfMpaEnumT  path;
    int8           gainUp;
}HwdRxDfeGainUpAdsT;



/* Definition for RX DFE DCO CFG Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    uint8          gainState;
    /* Band to select related DCO table */
    HwdRfBandT     band;
    HwdRfRxEnumT   rxPath;     
}HwdRxDfeCfgDcoAdsT;

/* Definition for RX DFE DCO Reading Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    /* ADS value */
    int16          dcoI;
    /* ADS value */
    int16          dcoQ;
}HwdRxDfeRdDcoAdsT;

/* Definition for RX DFE DC IIR Coef CFG Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    uint16         dcCoef;
}HwdRxDfeCfgDcCoefAdsT;

/* Definition for RX DFE DC IIR Coef Read Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    uint16         dcCoef;
}HwdRxDfeRdDcCoefAdsT;

/* Definition for RX DFE DCO table update Action */
typedef struct 
{
    HwdRfMpaEnumT  path;    
    uint8          gainState;
    HwdRfPwrModeT  pwrMode;
    /* ADS value */
    int16          dcoI;
    /* ADS value */
    int16          dcoQ;
    /* ADS value */
    HwdRfBandT     rxBand;
    HwdRfRxEnumT   rxPath;    
}HwdRxDfeUpdDcoElementAdsT;

/* Definition for RX DFE Phase Jump CFG Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    int32          phaseJump;
}HwdRxDfeCfgPhaseJumpAdsT;

/* Definition for RX DFE AGC-DC CFG Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    HwdRfRxEnumT   rxPath; 
    uint8          gainState;
    HwdRfPwrModeT  pwrMode;
    int16          deltaStepGain;
    int32          phaseJump;
    HwdRfBandT     band;
}HwdRxDfeCfgAgcDcAdsT;

/* Definition for RX DFE Step Gain CFG Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    int16          deltaStepGain;
}HwdRxDfeCfgStepGainAdsT;

/* Definition for RX DFE Read Wide Band Rssi Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    uint16         wbRssi;
}HwdRxDfeRdWbRssiAdsT;

/* Definition for RX DFE Read Digital Gain Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    int16          digiGain;
}HwdRxDfeRdDigiGainAdsT;

/* Definition for RX DFE Read AGC-DC Action */
typedef struct 
{
    HwdRfMpaEnumT  path;
    int16          digiGain;
    uint16         wbRssi;
    /* ADS value */
    int16          dcoI;
    /* ADS value */
    int16          dcoQ;
    /*EVEREST DC work around for HW bug*/
#if(SYS_BOARD == SB_EVEREST)
    uint16         dcLoopCnt;
#endif
}HwdRxDfeRdAgcDcAdsT;

typedef struct
{
    /* ADC mv value --Q4 value */
    int16          dcoI;
    /* ADC mv value --Q4 value */
    int16          dcoQ;
}HwdRxDfeDcoElementT;

typedef struct
{
    /* IQ phase, in S-4.8, -theta/2, in radians */
    int32          phase;
    /* Q branch IQ gain in S3.7, it's a log2 value. I branch IQ gain is always 0(log2) */
    int32          gainQ;
}HwdRxDfeIrrAdsT;


typedef struct
{
    HwdRxDfeDcoElementT  dfeDco[HWD_RXDFE_PATH_NUM][HWD_RX_GAIN_STATE_MAX];
}HwdRxDfeDcoTableT;

typedef struct
{
    HwdRxDfeIrrAdsT      irr[HWD_RXDFE_PW_MODE_NUM][HWD_RXDFE_PATH_NUM]; /*2 for H/L power mode*/
}HwdRxDfeIrrTableT;


/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Variables
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
int16 HwdRxDfeRxIqGainOrigResultTranToFinlResultQ(int32 iq_gain_orig_result);
int16 HwdRxDfeRxIqPhaseOrigResultTranToFinlResult(int32 iq_phase_orig_result);
int16 HwdRxDfeRxDigDcOrigResultTranToFinlResult(int32 dc_orig_result);
void HwdRxDfePocDcInit(HwdRxDfeDcoTableT *dcoAdsP,HwdRfBandT rfBand);
void HwdRxDfePocIrrInit(HwdRxDfeIrrTableT *IrrAdsP,HwdRfBandT rfBand);
void HwdRxDfeCfgGainUp(HwdRxDfeGainUpAdsT *dcCoefAds);
void HwdRxDfeCfgGainUpMain(int16 gainUp);
void HwdRxDfeCfgGainUpDiv(int16 gainUp);
void HwdRxDfeRdGainUp(HwdRxDfeGainUpAdsT *dcCoefAds);
void HwdRxDfeGainUpSwitch(uint16 PathIndex, int16 InputPower, int16 ManualSwitchPower, int16 *GainUpAdj);
void HwdRxDfeCfgDcIirCoef(HwdRxDfeCfgDcCoefAdsT *dcCoefAds);
void HwdRxDfeCfgDcoInitValue(HwdRxDfeCfgDcoAdsT *dcoCfgAds);
void HwdRxDfeCfgPhaseJump(HwdRxDfeCfgPhaseJumpAdsT  *phaseJumpAds);
void HwdRxDfeCfgStepGain(HwdRxDfeCfgStepGainAdsT  *stepGainAds);
void HwdRxDfeCfgAgcDc(HwdRxDfeCfgAgcDcAdsT  *agcDcAds);
void HwdRxDfeRdDcIirCoef(HwdRxDfeRdDcCoefAdsT *dcCoefAds);
void HwdRxDfeRdDigiGain(HwdRxDfeRdDigiGainAdsT  *digiGainAds);
void HwdRxDfeRdWbRssi(HwdRxDfeRdWbRssiAdsT  *wbRssiAds);
void HwdRxDfeRdDcoValue(HwdRxDfeRdDcoAdsT  *dcoReadAds);
void HwdRxDfeRdDcoCali(HwdRxDfeRdDcoAdsT  *dcoReadAds);
void HwdRxDfeRdAgcDc(HwdRxDfeRdAgcDcAdsT  *agcDcRdAds);
void HwdRxDfeUpdDcoElement(HwdRxDfeUpdDcoElementAdsT  *updDcoAds);

void HwdRxDfeRdDcoTbl(uint8 path,
                      HwdRfBandT    band,
                      uint16        maxRxDcocToRead,
                      int16        *rxDcocI,
                      int16        *rxDcocQ);

void HwdRxDfeCfgDcCal(SysAirInterfaceT  Interface, HwdRfMpaEnumT RfPath);
void HwdRxDfeStartDcCal(SysAirInterfaceT  Interface, HwdRfMpaEnumT RfPath);
void HwdRxDfeStopDcCal(SysAirInterfaceT  Interface, HwdRfMpaEnumT RfPath);
void HwdRxDfeDcCalReset(HwdRfMpaEnumT RfPath);
void HwdRxDfeEnable(SysAirInterfaceT   Interface, HwdRfMpaEnumT RfPath);
void HwdRxDfeDisable(SysAirInterfaceT   Interface, HwdRfMpaEnumT RfPath);
void HwdRxDfeSwInit(void);
void HwdRxDfeMainInit(void);
void HwdRxDfeDivInit(void);
void HwdRxDfeRfInit(void);
void HwdRxDfeRestoreRegisters(SysAirInterfaceT Interface);
void HwdRxDfeSourceSelect(SysAirInterfaceT  Interface, 
                                    HwdRfMpaEnumT     RfPath, 
                                    HwdIcPathT        ICPath);
void HwdRxDfeResetDcParmsTimer(uint32 TimerId);
void HwdRxDfeSetDcOffsetParms(SysAirInterfaceT AirInterfaceType, uint8 RfPath, uint8 Mode, bool CallbackReq);
void HwdRxDfe1xTrafficStart( bool InTraffic);
int16 HwdRxDfeGainUpAdjGet(uint16 PathIndex);

#ifdef MTK_DEV_DUMP_REG
void HwdRxDfeMainRegLogRdAll(void);
void HwdRxDfeDivRegLogRdAll(void);
void HwdRxDfeRegLogRdRfInit(void);
void HwdRxDfeRegLogRdCfgDc(HwdRfMpaEnumT RfPath);
void HwdRxDfeRegLogRdPathOn(HwdRfMpaEnumT RfPath);
#endif

#endif

/*****************************************************************************
* End of File
*****************************************************************************/
#endif

/**Log information: \main\Trophy\Trophy_ylxiao_href22060\1 2013-03-26 07:38:04 GMT ylxiao
** HREF#22060, bugfix for DMA**/
/**Log information: \main\Trophy\1 2013-04-03 02:41:06 GMT hzhang
** HREF#22060 to merge code from Tropby 0.3.x**/
/**Log information: \main\Trophy_0.3.X\1 2013-03-26 02:24:09 GMT fwu
** HREF#21981.**/
/**Log information: \main\Trophy\Trophy_fwu_href22082\1 2013-04-03 02:26:28 GMT fwu
** HREF#22082, Modified to support UART1 to be the AT channel between AP and CP.**/
/**Log information: \main\Trophy\2 2013-04-03 02:56:59 GMT hzhang
** HREF#22082 to merge code.**/

