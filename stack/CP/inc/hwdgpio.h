/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDGPIO_H_
#define _HWDGPIO_H_

/*****************************************************************************
* 
* FILE NAME   : hwdgpio.h
*
* DESCRIPTION : GPIO interface definition file
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

#include "sysdefs.h"
#undef HWD_GPIO_TEST

#define NULL_ENTRY     -1
#define GPIO_OUTPUT    0
#define GPIO_INPUT     1

/* definitions for trace only */
#define GPIO_CLEAR       0
#define GPIO_SET         1
#define GPIO_TOGGLE      2

/* Defines must correspond to their register value */
#define GPIO_MODE_FUNCTIONAL    1
#define GPIO_MODE_GPIO          0

/************************************************************************
 NOTE: This should be the ONLY definition to change if the GPIO
       connected to Ckt109 is changed
*************************************************************************/
#define CKT_109_STATE HWD_GPIO_NUM  

/************************************************************************
 NOTE: This should be the ONLY definition to change if the GPIO
       connected to Ckt108 is changed
*************************************************************************/
#define CKT_108_GPIO  HWD_GPIO_NUM

#define HWD_GPIO_NA    0   /* This GPIO is not availble yet  */
#define HWD_GPIO_CP    1   /* This GPIO is at CP FPGA  */
#define HWD_GPIO_1X    2   /* This GPIO is at 1X FPGA  */

#ifdef SYS_OPTION_AP_INTERFACE

/*Used for 4-line wakeup*/
#define CP_WAKE_AP_GPIO             HWD_GPIO_53
#define AP_READY_GPIO               HWD_GPIO_6
#define AP_READY_GPINT              HWD_GPINT_6

#define AP_WAKE_CP_GPINT            HWD_GPINT_7

#define CP_READY_GPIO               HWD_GPIO_54

/*CBP use this pin for reset indication
                      reset         normal
                    |-------|
----------- |            |--------------
*/
#ifdef BOOT_BUILDING
#if (defined BOARD_TROPHY || defined BOARD_DENALI || defined MODEM_LOGOS_P0)
#define CP_RESET_IND_GPIO           HWD_GPIO_67
#else
#define CP_RESET_IND_GPIO           HWD_GPIO_NUM
#endif
#endif

/*Used for SDIO*/
#define GPIO_ACTIVE_HIGH (1)
#define GPIO_ACTIVE_LOW (0)
#define CP_ACK_GPIO                 HWD_GPIO_56
#define CP_ACK_GPIO_ACTIVE_POL GPIO_ACTIVE_LOW     /* ACK active low means AP can continue operation when ACK pin is low, 
                                                                                               ACK active high means AP can continue operation when ACK pin is high*/
#ifdef SDIO_HARDWARE_FLOW_CONTROL
#define CP_M2S_FCTL_GPIO            HWD_GPIO_55
#define CP_M2S_FCTL_GPIO_ACTIVE_POL GPIO_ACTIVE_LOW/* FLOW CONTROL active low means AP can write when FC pin is low, 
                                                                                               FLOW CONTROL  active high means AP can write when FC pin is high*/
#endif

/*ETS select GPIO/GPINT set*/
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))  /* AP config  */
#define CP_ETS_SEL_GPIO            HWD_GPIO_13
#define ETS_CTRL_GPINT             HWD_GPINT_13
#else
#define CP_ETS_SEL_GPIO            HWD_GPIO_8
#define ETS_CTRL_GPINT             HWD_GPINT_8
#endif
#define ETS_CTRL_GPINT_DEB         1000
/*Some AP's SDIO controller can't handle DAT1 interrupt properly, use GPIO to trigge the interrupt*/

#endif /*SYS_OPTION_AP_INTERFACE*/

/*------------------------------------------------------------------------
* Define typedefs used in GPIO module
*------------------------------------------------------------------------*/
typedef enum
{
   NOT_CONFIGURED = 0,
   CONFIGURED_GPIO,
   CONFIGURED_FUNC
}HwdGpioConfigT;

typedef struct 
{
   uint8*    FileName;
   uint32    LineNum;
   int32     GpioNum; 
   uint32    GpioWrReg; 
   uint32    GpioRdReg; 
   uint32    GpioDirReg; 
   uint16    GpioMaskBit;
   uint8     GpioIdx;
   uint8     GpioState;
   HwdGpioConfigT   GpioConfigured;
} GpioRoutingTableT;

/* GPIO Numbers */
typedef enum
{
   HWD_GPIO_0    = 0,
   HWD_GPIO_1,           
   HWD_GPIO_2,           
   HWD_GPIO_3,           
   HWD_GPIO_4,           
   HWD_GPIO_5,           
   HWD_GPIO_6,           
   HWD_GPIO_7,           
   HWD_GPIO_8,           
   HWD_GPIO_9,           
   HWD_GPIO_10,          
   HWD_GPIO_11,          
   HWD_GPIO_12,          
   HWD_GPIO_13,          
   HWD_GPIO_14,          
   HWD_GPIO_15,          
   HWD_GPIO_16,          
   HWD_GPIO_17,          
   HWD_GPIO_18,          
   HWD_GPIO_19,          
   HWD_GPIO_20,          
   HWD_GPIO_21,          
   HWD_GPIO_22,          
   HWD_GPIO_23,          
   HWD_GPIO_24,          
   HWD_GPIO_25,          
   HWD_GPIO_26,          
   HWD_GPIO_27,
   HWD_GPIO_28,
   HWD_GPIO_29,
   HWD_GPIO_30,
   HWD_GPIO_31,
   HWD_GPIO_32,
   HWD_GPIO_33,
   HWD_GPIO_34,
   HWD_GPIO_35,
   HWD_GPIO_36,
   HWD_GPIO_37,
   HWD_GPIO_38,
   HWD_GPIO_39,
   HWD_GPIO_40,
   HWD_GPIO_41,
   HWD_GPIO_42,
   HWD_GPIO_43,
   HWD_GPIO_44,
   HWD_GPIO_45,
   HWD_GPIO_46,
   HWD_GPIO_47,
   HWD_GPIO_48,
   HWD_GPIO_49,
   HWD_GPIO_50,
   HWD_GPIO_51,
   HWD_GPIO_52,
   HWD_GPIO_53,
   HWD_GPIO_54,
   HWD_GPIO_55,
   HWD_GPIO_56,
   HWD_GPIO_57,
   HWD_GPIO_58,
   HWD_GPIO_59,
   HWD_GPIO_60,
   HWD_GPIO_61,
   HWD_GPIO_62,
   HWD_GPIO_63,
   HWD_GPIO_64,
   HWD_GPIO_65,
   HWD_GPIO_66,
   HWD_GPIO_67,
   HWD_GPIO_68,
   HWD_GPIO_69,
   HWD_GPIO_NUM   /* max number of GPIOs */
} HwdGpioT;

#if (SYS_ASIC <= SA_CBP82)
extern const uint32 GpioNmRegTbl[];
#endif
extern uint32 HwdDirRegs[];
extern uint32 GpioWrRegTable[];

#if (SYS_ASIC <= SA_CBP82)
#define HwdGpFastSetGpio(Num) \
     HwdWrite(GpioNmRegTbl[Num/16], HwdRead(GpioNmRegTbl[Num/16]) & ~(1<<(Num%16)))
#define HwdGpFastSetFunc(Num) \
     HwdWrite(GpioNmRegTbl[Num/16], HwdRead(GpioNmRegTbl[Num/16]) | (1<<(Num%16)))
#else
#define HwdGpFastSetGpio(Num)
#define HwdGpFastSetFunc(Num)
#endif      
#define HwdGpFastSetOutput(Num)   \
     HwdWrite(HwdDirRegs[Num/16], HwdRead(HwdDirRegs[Num/16]) | (1<<(Num%16)))

#define HwdGpFastSetInput(Num)   \
     HwdWrite(HwdDirRegs[Num/16], HwdRead(HwdDirRegs[Num/16]) & ~(1<<(Num%16)))

#define HwdGpioFastClear(Num)   \
     HwdWrite(GpioWrRegTable[Num/16], HwdRead(GpioWrRegTable[Num/16]) & ~(1<<(Num%16)))
     
#define HwdGpioFastSet(Num)     \
     HwdWrite(GpioWrRegTable[Num/16], HwdRead(GpioWrRegTable[Num/16]) | (1<<(Num%16)))
     
#define HwdGpioFastRead(Num) ((HwdRead(GpioWrRegTable[Num/16]) >> (Num%16)) & 1)

/*****************************************************************************
 
  FUNCTION NAME: HwdGpioMonitor

  DESCRIPTION:

    This routine reads GPIO without changing its direction and
	also returns the direction of the GPIO in the second byte:
	xxxxxxxD xxxxxxxV, where D is a direction bit -
	GPIO_OUTPUT=0, GPIO_INPUT=1 (direction); and V is a value
	Return Value of 0xffff means GPIO is not in use

  PARAMETERS:

    GpioNum  -  GPIO number to read
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern uint32 HwdGpioMonitor(uint32 GpioNum);

/*****************************************************************************
 
  FUNCTION NAME: HwdGpioClear

  DESCRIPTION:

    This routine clears a specified GPIO register to zero.

  PARAMETERS:

    GpioNum  -  GPIO number to clear
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern void HwdGpioClear(uint32 GpioNum);

/*****************************************************************************
 
  FUNCTION NAME: HwdGpioSet

  DESCRIPTION:

    This routine sets a specified GPIO register to one.

  PARAMETERS:

    GpioNum  - GPIO number to set
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern void HwdGpioSet(uint32 GpioNum);

/*****************************************************************************
 
  FUNCTION NAME: HwdGpioRead

  DESCRIPTION:

    This routine reads a specified GPIO register and returns its
    value.

  PARAMETERS:

    GpioNum  - GPIO number to read
    
  RETURNED VALUES:

    bool     - TRUE if it is set, FLASE if it is not set

*****************************************************************************/
extern bool HwdGpioRead(uint32 GpioNum);

/*****************************************************************************
 
  FUNCTION NAME: HwdGpioToggle

  DESCRIPTION:

    This routine toggles a specified GPIO register.

  PARAMETERS:

    GpioNum  - GPIO number to toggle
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern void HwdGpioToggle(uint32 GpioNum);

/*****************************************************************************
 
  FUNCTION NAME: HwdGpioInit

  DESCRIPTION:

    This routine configures all GPIOs to GPIO mode or functional mode.

  PARAMETERS:

    None
    
  RETURNED VALUES:

    None

*****************************************************************************/
extern void HwdGpioInit(void);


/*****************************************************************************
 
  FUNCTION NAME: HwdGpioReadCkt108

  DESCRIPTION:

    This routine reads circuit 108 status for UART1 from one of GPIO ports.

  PARAMETERS:

    None
    
  RETURNED VALUES:

    bool   boolean state for desired port.

*****************************************************************************/
extern bool HwdGpioReadCkt108(void);

/*****************************************************************************
  FUNCTION NAME: HwdGpioWriteCkt109

  DESCRIPTION:

    This routine write a desired state to circuit 109 for UART1 to one of 
    GPIO ports.

  PARAMETERS:

    bool   logic state
    
  RETURNED VALUES:

    none

*****************************************************************************/
extern void HwdGpioWriteCkt109(bool);

extern void HwdGpioGpIntInit( void );
#if (SYS_ASIC <= SA_CBP82)
extern void HwdGpioNormalModeSet( HwdGpioT GpioNum );
extern void HwdGpioFunctionalModeSet( HwdGpioT GpioNum );
extern bool HwdGpioModeGet(HwdGpioT GpioNum);
extern void HwdGpioPullUpDownControl(uint32 GpioNum, bool Enable);
extern void HwdGpioConfigure(HwdGpioT GpioNum, uint8 GpioMode, uint8 GpioDir);
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define HwdGpioConfigure(GpioNum, GpioMode, GpioDir) \
        __HwdGpioConfigure(GpioNum, GpioMode, GpioDir, __MODULE__, __LINE__)

extern void __HwdGpioConfigure(HwdGpioT GpioNum, uint8 GpioMode, uint8 GpioDir,
                  const char *Filename, unsigned Linenumber); 

#else
extern void HwdGpioConfigure(HwdGpioT GpioNum, uint8 GpioMode, uint8 GpioDir);
#endif /* SYS_DEBUG_FAULT_FILE_INFO */
#endif

extern uint8 HwdGpioGetConfigMode(HwdGpioT GpioNum);
extern void HwdGpioDirCtrl(HwdGpioT GpioNum, uint8 Dir);


#ifdef SYS_OPTION_AP_INTERFACE
bool HwdGpioETSFixToUART(void);
#endif


/*****************************************************************************
* $Log: hwdgpio.h $
*****************************************************************************/

#endif
/**Log information: \main\2 2012-03-23 07:11:17 GMT yliu
** remove suspicious ^L character**/
/**Log information: \main\Trophy\Trophy_fwu_href22082\1 2013-04-03 02:26:26 GMT fwu
** HREF#22082, Modified to support UART1 to be the AT channel between AP and CP.**/
/**Log information: \main\Trophy\1 2013-04-03 02:56:57 GMT hzhang
** HREF#22082 to merge code.**/
/**Log information: \main\Trophy\Trophy_hxwang_href22199\1 2013-05-08 03:11:05 GMT hxwang
** HREF#22199, check in code from htc debug.**/
/**Log information: \main\Trophy\2 2013-05-08 03:35:05 GMT zlin
** HREF#22199, merge code.**/
/**Log information: \main\Trophy\Trophy_SPI\1 2014-01-08 04:09:27 GMT fwu
** HREF#0000: Modified  for ESPI solution.**/
/**Log information: \main\Trophy\Trophy_fwu_href22348\1 2014-01-08 06:17:16 GMT fwu
** HREF#22348. Merge ESPI related source code.**/
/**Log information: \main\Trophy\3 2014-01-09 06:34:38 GMT zlin
** HREF#22348, merge code.**/
