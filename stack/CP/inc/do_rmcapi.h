/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2006-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
* 
* FILE NAME   : do_rmcapi.h
*
* DESCRIPTION : API definition for Reverse Modem Controller (RMC) task.
*
* HISTORY     :
*****************************************************************************/
#ifndef _DO_RMCAPI_H_
#define _DO_RMCAPI_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "do_msgdefs.h"
#include "do_schapi.h"
#include "exeapi.h"
#include "hwdpll.h"
#include "sysapi.h"
#include "sysdefs.h"
#include "syscommon.h"
#include "hwdgpio.h"
#ifdef MTK_DEV_C2K_IRAT 
#ifdef MTK_DEV_C2K_SRLTE_L1
#include "iratapi.h"
#endif
#endif


#ifndef BOOL
#define BOOL bool
#endif


/*----------------------------------------------------------------------------
 Global typedefs
----------------------------------------------------------------------------*/

/* Number of DO Slots per frame */
#define MAX_DO_SLOTS_PER_FRAME  16

#define MAX_RX_PWR_TEST_SEQ_LEN (10)
#define DO_NUM_TX_POWER_LEVELS  (5)
#define  RMC_TX_POWER_LEVEL_0    ((int16)(5<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_1    ((int16)(10<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_2    ((int16)(15<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_3    ((int16)(20<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_4    ((int16)(25<<5))  //Q5

#if 1
#define RMC_PILOT_METRIC_TO_ECIO_Q27 8500
/*#define RMC_PILOT_METRIC_TO_ECIO_Q29 34000*/
#define RMC_PILOT_METRIC_TO_ECIO_Q29 29127 /* get from target as 3.5 bit*/
#else
#define RMC_PILOT_METRIC_TO_ECIO_Q29 23981
#endif


#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* The RxAGC difference between DO and 1xRTT for ORIONC 
 * the ditail value will be given after lab calibration
 */
#define RMC_RXAGC_DO_1X_DIFF_LOG2_Q7     (uint16)((1.54/20)*3.322*(1<<7))  

/* the filter chain for 1xRTT and EVDO is all the same, 
 * so there is no RXTX delay difference
 */
#define RMC_FMP_RXTX_DELAY_DIFF_TC16   (0)  

#else
#define RMC_FMP_RXTX_DELAY_DIFF_TC16   (4<<1)  /* The RXTX delay difference between DO and 1xRTT*/

#define RMC_RXAGC_DO_1X_DIFF_LOG2_Q7     (uint16)((1.54/20)*3.322*(1<<7))  /* The RxAGC difference between DO and 1xRTT*/
#endif
#define RXAGC_DBM_ADJ_PER_FIRTAP_STEP_LOG2_Q12   (uint16)((0.17/6.0206)*(1<<12)+0.5)  /* 0.17dB per FIR TAP step, to log2*/

#define RMC_FORCE_NORMAL_TARGET 

#define RMC_PAUSE_RESTORE_DISABLE      0xffffffff

/* convert system from SysTimeFullT format to uint64 format */
#define GetSystemTime64(fullTimep) ( (uint64)(((uint64)((fullTimep)->Upper6)<<36) | \
                ((uint64)((fullTimep)->Lower32)<<4) | (fullTimep)->Slot ) )

#define DIGIGAIN_SATURATION_REFLOG2Q7  0xFD34      /* -5.6 Q7 */
#define DIGIGAIN_SATURATION_FIR_REF    0x66


#ifdef SYS_OPTION_SINGLECHIP_MULTIMODE
#define RMC_SCH_SET_RXTX_DELAY               1
#endif

/* Define RMC Modem State variables */
typedef enum
{
   SYSTEM_1xRTT,
   SYSTEM_EVDO
}CDMA_SystemT;

typedef enum
{
   MAIN_ANT,
   DIV_ANT
}AntennaT;

/* Define RMC Rx AGC State variables */
typedef enum
{
   RMC_RXAGC_DISABLED = 0,     /* Rx AGC Inner/Outer-Loop processing disabled */
   RMC_RXAGC_INIT_ACQ,         /* Rx AGC Inner/Outer-Loop configured for continuous/init-acq mode */
   RMC_RXAGC_FAST_PREP,        /* Rx AGC Inner-Loop configured for Fast AGC, Outer Loop ISR disabled */
   RMC_RXAGC_FAST_ENABLED,     /* Rx AGC Inner/Outer-Loop ISR enabled for Fast AGC (i.e., slotted wakeup) */
   RMC_RXAGC_STEADY_STATE,     /* Rx AGC Inner/Outer-Loop configured for steady-state MAC/Pilot/MAC AGC sampling */
   RMC_RXAGC_TRAFFIC,          /* Rx AGC Inner/Outer-Loop configured for steady-state/traffic configuration
                               ** - NOTE: traffic mode currently not supported. */
   RMC_RXAGC_TEST_MODE         /* Rx AGC Inner/Outer-Loop configured for test purposes */

} RmcAgcStateT;

#if defined (MTK_DEV_OPTIMIZE_EVL1)
/* Define timing stamp record ID */
typedef enum
{
    RMC_TIMING_STAMP_ENTER_DO_SLOT_ISR = 0,
    RMC_TIMING_STAMP_LEAVE_DO_SLOT_ISR,
    RMC_TIMING_STAMP_AFTER_SLOT_TX_AGC,
    RMC_TIMING_STAMP_ENTER_FNG_ASSIGN_ISR,
    RMC_TIMING_STAMP_LEAVE_FNG_ASSIGN_ISR,
    RMC_TIMING_STAMP_ENTER_TX_DMA_MDM_ISR,
    RMC_TIMING_STAMP_LEAVE_TX_DMA_MDM_ISR,
    RMC_TIMING_STAMP_AFTER_RTC_GRANT,
    RMC_TIMING_STAMP_ENTER_RX_DMA_MDM_ISR,
    RMC_TIMING_STAMP_LEAVE_RX_DMA_MDM_ISR,
    RMC_TIMING_STAMP_ENTER_DO_HALFSLOT_ISR,
    RMC_TIMING_STAMP_LEAVE_DO_HALFSLOT_ISR,
    RMC_TIMING_STAMP_AFTER_HALFSLOT_TX_AGC,

} RmcTimingStampIdT;
#endif
/* Define the moving average filter length (as pow of 2) for the RMC RSSI values 
   NOTE: pow of 2 has following MIPS advantages: 
            1.  division for getting the average is just bit-shift operation
            2. Counter modulo operation is just Bitwise AND
*/
#define RMC_AGC_RSSI_AVG_LEN_POW2  3
#define RMC_AGC_RSSI_AVG_LEN  (1<<RMC_AGC_RSSI_AVG_LEN_POW2)

/* DC Offset data structure used for reporting periodic ETS DC Offset Spy reports */
typedef PACKED_PREFIX struct
{
   uint8  DcOffsetMode;
   int16  IChannelOffset;
   int16  QChannelOffset;

} PACKED_POSTFIX  RmcDcOffsetDataT;


/* Rx AGC Data structure used for tracking AGC variables and providing periodic 
** ETS AGC measurement reports */
typedef  struct
{
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
    RmcAgcStateT        AgcMode;
    /** The digital DC offset for I & Q path read from RxDFE in linear format, 
     *  convert to ADC point */
    RmcDcOffsetDataT    digitalDcVal;
    /** The RF Rx path analog gain in amplitude log2 format, Q7 */
    int16               stepGainAlog2;
    /** The wide band rssi read from RxDFE in amplitude format */
    uint16              wideBandRssi;
    /** store the back up value for the DC IIR coefficient */
    uint8               backupDcIirCoef;
    /** flag to indicate the DC fast tracking */
    bool                flagDcFastTracking;
    /** flag to indicate if ADC is saturated */
    bool                flagAdcSaturated;
    /** flag to indicate if ADC saturation flag is set fixedly, only for test */
    bool                flagAdcSaturatedFixed;
    /** flag to indicate if RX AGC is fixed, only for test */
    bool                flagAgcFix;
    /** The index of Rx power test sequence, only for test */
    uint8               RxPwrALog2TstSeqIdx;
    /** The length of Rx power test sequence, only for test */
    uint8               RxPwrALog2TstSeqLen;
    /** The Rx power test sequence, only for test */
    int16               RxPwrALog2TstSeq[MAX_RX_PWR_TEST_SEQ_LEN];
#else
    uint16              AgcMode;
#endif
    uint16              DigiGainHwVal;
    int16               DigiGainALog2;
    int16               DigiGain;      /* Digital gain in dB */
    uint8               RxGainState;
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
    HwdRfPwrModeT       pwrMode;
#endif
    uint8               GainTransitions;
    int16               UnadjPwrALog2;
    int16               TempAdjALog2;
    int16               FreqAdjALog2[HWD_MAX_NUM_DIGITAL_GAIN_STATES];
    int16               RxPwrALog2;
    int16               TempAdj;       /* Rx AGC adjustment in dB */
    int16               FreqAdj;       /* Rx AGC adjustment in dB */
    int16               TotalRxPwr;    /* Total adjusted Rx antenna power in dB */
    struct {
        //RX power in log2 domain (Q7) NOTE: const multiplier to get equivalent dBm value
        int16           PerSlotRxPwrALog2[MAX_DO_SLOTS_PER_FRAME];  
        int16           PerFrmRxPwrdBm[RMC_AGC_RSSI_AVG_LEN];  //RX power in dBm (Q7)
        uint8           SlotIdx;
        uint8           FrmIdx;
        bool            SlotBuffFull;
        bool            FrmBuffFull;
    } RssiAvg;
#if (SYS_BOARD == SB_EVEREST)/*EVEREST DC work around for HW bug*/
    uint16              DcFilterCnt;
#endif    
} RmcAgcDataT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))

typedef enum
{
    RMC_HALF_SLOT_ISR,
    RMC_SLOT_ISR
}RmcSlotIsrTypeT;

#endif

/* AFC Adjustment data structure used for tracking and reporting AFC Adjustment reports */
typedef  struct
{
   uint16  AfcState;
   /* AfcOffsetHz record the frequency offset of the AN's carrier compared with AT's */       
   int16   AfcOffsetHz;
   int16   AfcOffsetPpm; /* Q=RMC_AFC_PPM_Q=10, old fomat for spy only */
   uint16  AfcDacValue;   
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   uint8   CapId;
#endif
} RmcAfcAdjDataT;

/* AFC data structure used for tracking and reporting AFC Adjustment reports */
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef  struct
{
   uint16  AfcState;
   int16   AfcOffsetHz;
   uint16  AfcDacValue;
   uint16  MmAfc;
   uint8   GpsMode;
} RmcAfcDataT;
#endif

/* AFC Metrics data structure used for tracking and reporting AFC Metric data/reports */
typedef  struct
{
   uint16  AfcState;

   /* AFC metric hardware data */
   uint16 AFC_N_Acc;
   uint32 AFC_SNR_Acc;
   int32  AFC_Re_Fine_Acc;  
   int32  AFC_Im_Fine_Acc;
   int32  AFC_Re_Coarse_Acc;
   int32  AFC_Im_Coarse_Acc;

   /* Rotational SNR value */
   uint32 RSNR;

   /* Current AFC phase error/angle estimates */
   int16 FinePhaseError;     /* aka Pf */
   int16 CoarsePhaseError;   /* aka Pc */

   /* Current AFC frequency error calculation */
   int16 FE_fine;            /* fine frequency error */
   int16 FE_coarse;          /* coarse frequency error */
   int16 FineFreqErrorHz;    /* aka F_fine - applied after Beta filter correction factor */
   int16 CoarseFreqErrorHz;  /* aka F_coarse - applied after Beta filter correction factor */
   int16 FreqErrorHz;        /* aka F_est */

   int8  NyquistRegion;      /* aka NYG */
   uint8 PersistanceCnt;     /* aka CTR_persistance */

} RmcAfcMetricsDataT;

typedef PACKED_PREFIX struct
{
    int16  MainRxPwr;     
    int16  MainDigiGain;
    uint8  MainRxGainState; 
    uint8  MainTransitions; 

    int16  DivRxPwr;   
    int16  DivDigiGain;  
    uint8  DivRxGainState;  
    uint8  DivTransitions;  

    int16  TxTotalPwr; 
    uint16 TxPdmValue;      
    uint8  TxGainState;     
    uint8  TxTransitions;   

    uint16 RxPwrFilt;  
    uint16 CloseLoopAdj;
    uint16 TxPilotPwr; 
    uint16 RpcDrclockBits;  
} PACKED_POSTFIX  RmcRfcRxTxSpyT;

#define MAX_SAVE_FOR_RUN_AVERAGE    4
typedef struct
{
   int   index;
   int   got4;
   int   runSum;
   int   runAvg;
   int   prevData[MAX_SAVE_FOR_RUN_AVERAGE];
}RunAvgT;

typedef struct
{
   uint32   RxActiveSlot;
   uint32   TxActiveSlot[DO_NUM_TX_POWER_LEVELS];
}RxTxPeriodStatisticT;
/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
/* Define Rx AGC Rss Averaging Window macros */
#define DISCONTINUOUS_AGC_WINDOW_SIZE   (uint32)0x0c80137c  /* 224 chip window starting at 400th chip from half-slot boundary
                                                                ** to where the MAC/pilot/MAC burst can be found */
#define CONTINUOUS_AGC_WINDOW_SIZE      (uint32)0x00001ffc  /* continuous window */

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))

#if (SYS_BOARD >= SB_JADE)
/* Target window uses a 12-bit <12,5,u> fixed-point format using amplitude log2 units */
#define DISCONTINUOUS_AGC_TARGET_LEVEL  0x0320
#define CONTINUOUS_AGC_TARGET_LEVEL     0x03AC
#else
/* Target window uses a 12-bit <12,5,u> fixed-point format using amplitude log2 units */
/* the target level for measure window 224chip 
 * for the target level of -36.708dBFs
 */
#define DISCONTINUOUS_AGC_TARGET_LEVEL  0x0367  

/* the target level for measure window 1024chip 
 * for the target level of -36.708dBFs
 */
#define CONTINUOUS_AGC_TARGET_LEVEL     0x03F3  
#endif

/* the initial value for the Rx AGC loop
 * the default value is -60dBm, in Q7
 */
#define RMC_RXAGC_INIT_POWER            M_DbToAlog2Q7(-80)  /* TBD, -80dBm */

#else
/* Target window uses a 12-bit <12,5,u> fixed-point format using amplitude log2 units */
#define DISCONTINUOUS_AGC_TARGET_LEVEL  0x02b4
#define CONTINUOUS_AGC_TARGET_LEVEL     0x0340
#endif

#define HALF_SLOT_UPDATE_PERIOD    0x1fff  /* Update period every 1024 chips (i.e., half-slot) */
#define SLOT_UPDATE_PERIOD         0x3fff  /* Update period every 2048 chips (i.e., slot)      */

/* Loop Coefficients indicate 2^n updated periods */
#define SLOW_LOOP_TIME_CONSTANT    1  /* 0: means fast, 1 meas real slow*/
#define FAST_LOOP_TIME_CONSTANT    0

/* Shadow RMC-controlled HWD registers for correct PSO mode save-restore */
#define RMC_PSO_SHADOW_HWD_REGS_ENABLE     1

#define  RMC_TX_POWER_LEVEL_0    ((int16)(5<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_1    ((int16)(10<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_2    ((int16)(15<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_3    ((int16)(20<<5))  //Q5
#define  RMC_TX_POWER_LEVEL_4    ((int16)(25<<5))  //Q5


/*-------------------------------------------------------------------------*/
/* Define Outer-Loop ISR Management macros - (currently, Outer-Loop ISR are
** configured as FIQs) */
#define RmcDisableHalfSlotIsr() \
   HwdIntDisableH4(HWD_INT_DO_HALFSLOT)
   
#define RmcEnableHalfSlotIsr() \
{\
   HwdIntClrH4(HWD_INT_DO_HALFSLOT);\
   HwdIntEnableH4(HWD_INT_DO_HALFSLOT);\
}

#define RmcDisableSlotIsr() \
   HwdIntDisableH4(HWD_INT_DO_SLOT)

#define RmcEnableSlotIsr() \
{\
   HwdIntClrH4(HWD_INT_DO_SLOT);\
   HwdIntEnableH4(HWD_INT_DO_SLOT);\
}


#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#if (SYS_BOARD >= SB_JADE)

/* Define Digital Gain register Read macros */
#define RxAgcGetDigitalGain(Reg)    HwdReadJade(REG_TYPE_RXDFE,Reg)

#define RxAgcGetDigitalGain1X(Reg)  HwdReadJade(REG_TYPE_RXDFE,Reg)

/*-----------------------------------------*/
/* Define Digital Gain Latch/Timing macros */
#define RxAgcConfigDigiGainTiming(Reg, Data)    HwdWriteJade(REG_TYPE_RXDFE,Reg, Data)

/*-----------------------------------------*/
/* Define Digital Gain Latch/Timing macros */
/* Step Gain Latch values are the delay in chips before applying the Step Gain
** adjustment value due to a change in the external RF - this will most likely
** need to be fine tuned based on external RF control */
#define RxAgcSelectStepGainLatchTiming(Reg, Data)    HwdWriteJade(REG_TYPE_RXDFE,Reg, (Data))

/*------------------------------------------*/
/* Define Digital Gain Step Gain Adjust macros */
#define RxAgcAdjustStepGain(Reg, Data)    HwdWriteJade(REG_TYPE_RXDFE,Reg, (Data))

#define RxAgcConfigRssAvgLsb(Reg, Data)   HwdWriteJade(REG_TYPE_RXDFE,Reg, (uint16)((Data) & 0x0000ffff))
#define RxAgcConfigRssAvgMsb(Reg, Data)   HwdWriteJade(REG_TYPE_RXDFE,Reg, (uint16)(((Data) >> 16) & 0x0000ffff))

/*------------------------------------------*/
/* Define Rx AGC Window target level macros */
#define RxAgcConfigWindowTarget(Reg, Data)   HwdWriteJade(REG_TYPE_RXDFE,Reg, Data)

/*-----------------------------------------------*/
/* Define Rx AGC Digital Gain Update rate macros */
#define RxAgcConfigGainUpdatePeriod(Reg, Data)   HwdWriteJade(REG_TYPE_RXDFE,Reg, Data)

/*-----------------------------------------*/
/* Define Rx AGC Loop-Time constant macros */
#define RxAgcConfigLoopSpeed(Reg, Data)   HwdWriteJade(REG_TYPE_RXDFE,Reg, Data)

#define RxAgcDisableRssAccumulator(Reg) \
   HwdWriteJade(REG_TYPE_RXDFE,Reg, HwdReadJade(REG_TYPE_RXDFE,Reg) | HWD_RXAGC_CTRL_RSS_DISABLE)

#define RxAgcEnableRssAccumulator(Reg) \
   HwdWriteJade(REG_TYPE_RXDFE,Reg, HwdReadJade(REG_TYPE_RXDFE,Reg) & ~HWD_RXAGC_CTRL_RSS_DISABLE)

#define RxAgcDisableUpdtClear(Reg) \
   HwdWriteJade(REG_TYPE_RXDFE,Reg, HwdReadJade(REG_TYPE_RXDFE,Reg) | HWD_RXAGC_CTRL_UPDT_CLEAR)

#define RxAgcEnableUpdtClear(Reg) \
   HwdWriteJade(REG_TYPE_RXDFE,Reg, HwdReadJade(REG_TYPE_RXDFE,Reg) & ~HWD_RXAGC_CTRL_UPDT_CLEAR)

#endif

#ifdef MTK_PLT_DENALI
/* Define Digital Gain register Read macros */
#define RxAgcGetDigitalGainMtk(Reg)    MtkHwdRead(Reg)

#define RxAgcGetDigitalGain1XMtk(Reg)  MtkHwdRead(Reg)

/*-----------------------------------------*/
/* Define Digital Gain Latch/Timing macros */
#define RxAgcConfigDigiGainTimingMtk(Reg, Data)    MtkHwdWrite(Reg, Data)

/*-----------------------------------------*/
/* Define Digital Gain Latch/Timing macros */
/* Step Gain Latch values are the delay in chips before applying the Step Gain
** adjustment value due to a change in the external RF - this will most likely
** need to be fine tuned based on external RF control */
#define RxAgcSelectStepGainLatchTimingMtk(Reg, Data)    MtkHwdWrite(Reg, (Data))

/*------------------------------------------*/
/* Define Digital Gain Step Gain Adjust macros */
#define RxAgcAdjustStepGainMtk(Reg, Data)    MtkHwdWrite(Reg, (Data))

#define RxAgcConfigRssAvgLsbMtk(Reg, Data)   MtkHwdWrite(Reg, (uint16)((Data) & 0x0000ffff))
#define RxAgcConfigRssAvgMsbMtk(Reg, Data)   MtkHwdWrite(Reg, (uint16)(((Data) >> 16) & 0x0000ffff))

/*------------------------------------------*/
/* Define Rx AGC Window target level macros */
#define RxAgcConfigWindowTargetMtk(Reg, Data)   MtkHwdWrite(Reg, Data)

/*-----------------------------------------------*/
/* Define Rx AGC Digital Gain Update rate macros */
#define RxAgcConfigGainUpdatePeriodMtk(Reg, Data)   MtkHwdWrite(Reg, Data)

/*-----------------------------------------*/
/* Define Rx AGC Loop-Time constant macros */
#define RxAgcConfigLoopSpeedMtk(Reg, Data)   MtkHwdWrite(Reg, Data)

#define RxAgcDisableRssAccumulatorMtk(Reg) \
   MtkHwdWrite(Reg, MtkHwdRead(Reg) | HWD_RXAGC_CTRL_RSS_DISABLE)

#define RxAgcEnableRssAccumulatorMtk(Reg) \
   MtkHwdWrite(Reg, MtkHwdRead(Reg) & ~HWD_RXAGC_CTRL_RSS_DISABLE)

#define RxAgcDisableUpdtClearMtk(Reg) \
   HwdWrite(Reg, HwdRead(Reg) | HWD_RXAGC_CTRL_UPDT_CLEAR)

#define RxAgcEnableUpdtClearMtk(Reg) \
   HwdWrite(Reg, HwdRead(Reg) & ~HWD_RXAGC_CTRL_UPDT_CLEAR)

#endif
#else
/* Define Digital Gain register Read macros */
#define RxAgcGetDigitalGain(Reg)    HwdRead(Reg)

#define RxAgcGetDigitalGain1X(Reg)  HwdRead(Reg)

/*-----------------------------------------*/
/* Define Digital Gain Latch/Timing macros */
#define RxAgcConfigDigiGainTiming(Reg, Data)    HwdWrite(Reg, Data)

/*-----------------------------------------*/
/* Define Digital Gain Latch/Timing macros */
/* Step Gain Latch values are the delay in chips before applying the Step Gain
** adjustment value due to a change in the external RF - this will most likely
** need to be fine tuned based on external RF control */
#define RxAgcSelectStepGainLatchTiming(Reg, Data)    HwdWrite(Reg, (Data))

/*------------------------------------------*/
/* Define Digital Gain Step Gain Adjust macros */
#define RxAgcAdjustStepGain(Reg, Data)    HwdWrite(Reg, (Data))

#define RxAgcConfigRssAvgLsb(Reg, Data)   HwdWrite(Reg, (uint16)((Data) & 0x0000ffff))
#define RxAgcConfigRssAvgMsb(Reg, Data)   HwdWrite(Reg, (uint16)(((Data) >> 16) & 0x0000ffff))

/*------------------------------------------*/
/* Define Rx AGC Window target level macros */
#define RxAgcConfigWindowTarget(Reg, Data)   HwdWrite(Reg, Data)

/*-----------------------------------------------*/
/* Define Rx AGC Digital Gain Update rate macros */
#define RxAgcConfigGainUpdatePeriod(Reg, Data)   HwdWrite(Reg, Data)

/*-----------------------------------------*/
/* Define Rx AGC Loop-Time constant macros */
#define RxAgcConfigLoopSpeed(Reg, Data)   HwdWrite(Reg, Data)

#define RxAgcDisableRssAccumulator(Reg) \
   HwdWrite(Reg, HwdRead(Reg) | HWD_RXAGC_CTRL_RSS_DISABLE)

#define RxAgcEnableRssAccumulator(Reg) \
   HwdWrite(Reg, HwdRead(Reg) & ~HWD_RXAGC_CTRL_RSS_DISABLE)

#endif   

#if defined (MTK_DEV_OPTIMIZE_EVL1)
#define  EVDO_CHIP_NUM_PER_SLOT          (2048)   /** Define chip number per slot */

#define GetEvChipNum    ((HwdRead32(HWD_STDO_SYS_CNT) >> 3) & 0x7FF) /* Get the chip number from system timer */
#endif

#define M_GetEvSysTime(EvdoSlotNum, EvdoChipNum) \
{ \
   uint32 temp = HwdRead32(HWD_STDO_SYS_CNT);\
   EvdoSlotNum = (uint8)((temp >> 14)& 0xf); \
   EvdoChipNum  = (uint16)((temp >> 3)& 0x7FF); \
}

/* Defines used by ETS to identify Main/Diversity receiver blocks */
#define RXAGC_MAIN_BLOCK  0x01
#define RXAGC_DIV_BLOCK   0x02

#define RMC_RCP_GAIN_ALOG2_Q  8

/*----------------------------------------------------------------------------
 Mailbox IDs
----------------------------------------------------------------------------*/
#define RMC_CMD_MAILBOX         EXE_MAILBOX_1_ID
#define RMC_TEST_MAILBOX        EXE_MAILBOX_2_ID
#define RMC_OTAMSG_MAILBOX      EXE_MAILBOX_3_ID

/*----------------------------------------------------------------------------
     Command Message IDs, for RMC task, for RMC_CMD_MAILBOX, EXE_MAILBOX_1_ID 
     The message IDs for components shall also be put in here.     
----------------------------------------------------------------------------*/
#include "do_rmcmsg.h"


/*----------------------------------------------------------------------------
    Data Message IDs , for RMC_TEST_MAILBOX, EXE_MAILBOX_2_ID
----------------------------------------------------------------------------*/
typedef enum
{
   /*---------------*/
   /* SCH Test Msgs */
   /*---------------*/
   SCH_ETS_CAPTURE_INPUT_BUFFER_MSG = RMC_TEST_MSGID_START,
   SCH_MINIACQ_WINSIZE_MSG,

   /*---------------*/
   /* RFC Test Msgs */
   /*---------------*/  
   RFC_ETS_CONFIG_MODEM_RESOURCES_MSG,
   RFC_ETS_EMULATE_DSPM_RFC_SPY_MSG,
   RFC_ETS_AGC_ISR_ST_MATCH_GET_MSG, 
   RFC_ETS_AGC_ISR_ST_MATCH_SET_MSG,   
   RFC_ETS_SETTLE_TIMES_GET_MSG,
   RFC_ETS_SETTLE_TIMES_SET_MSG,   
   RFC_ETS_RX_AGC_MODE_SET_MSG,
   RFC_ETS_RX_AGC_GAIN_SET_MSG,
   RFC_ETS_RX_AGC_CONFIG_MSG,
   RFC_ETS_RX_AGC_GET_CONFIG_MSG,
   RFC_ETS_RX_AGC_GET_DIGIGAIN_MSG, 
   RFC_ETS_AFC_PDM_DAC_CONTROL_MSG,
   RFC_ETS_AFC_MODE_SET_MSG,
   RFC_ETS_AFC_METRIC_CONFIG_GET_MSG,
   RFC_ETS_AFC_METRIC_CONFIG_SET_MSG,
   RFC_ETS_EMULATE_DSPM_AFC_SPY_MSG,
   RFC_ETS_DC_OFFSET_CONFIG_MSG,
   RFC_ETS_MAIN_RX_AGC_TEST_CASE_MSG,
   RFC_ETS_DIV_RX_AGC_TEST_CASE_MSG,
   RFC_ETS_AFC_TEST_CASE_MSG,
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))   
   RFC_ETS_AFC_CTRL_MTK_MSG,
   RFC_ETS_AGC_CFG_MTK_MSG,
   RFC_ETS_AGC_CFG_RSSI_SCAN_MTK_MSG,
   RFC_ETS_DIG_GAIN_START_MTK_MSG,
   RFC_ETS_AFC_FOE_CONFIG_MTK_MSG,
   RFC_ETS_AGC_GAIN_UP_MTK_MSG,
#endif
   /*---------------*/
   /* FMP Test Msgs */
   /*---------------*/
   FMP_TEST_HWTESTMODE_MSG,                        
   FMP_TEST_UPDATEFINGERS_MSG,
   FMP_TEST_UPDATEMACFINGERS_MSG,
   FMP_TEST_INSP_SYNC_MSG,
   FMP_TEST_SCHPROC_MSG,
   FMP_TEST_SCHPROGFNG_CALLBK_MSG,
   FMP_TEST_SLOTFOUND_MSG,
   FMP_TEST_UPDATEPWR_MSG,
   FMP_TEST_UPDATEFNGPOS_MSG,
   FMP_TEST_INITACQFNGASSIGN_CALLBK_MSG,
   FMP_TEST_CONNSETUP_CALLBK_MSG,
   FMP_TEST_FNHW_MSG,
   FMP_FNALGOPARMS_GET_CMD,
   FMP_FNALGOPARMS_SET_CMD,
   FMP_FINGER_SCAN_CMD,
   FMP_FINGER_COMB_CFG_CMD,
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC)) 
   FMP_FINGER_ANTCOMB_CFG_CMD,
#endif
   FMP_EQFNALGOPARMS_SET_CMD,
   FMP_EQFEATURE_SET_CMD,
   FMP_EQ_MANUAL_ASSIGN_CMD,

   /*-------------------------------*/
   /* FMP MMSE MRC Ctrl Config Cmds */
   /*-------------------------------*/
   CP_RMC_MAC_TARGET_TEST_CMD,   

   /*---------------*/
   /* CSM Test Msgs */
   /*---------------*/
   CSM_TEST_CELL_SWITCH_MSG,
   CSM_ETS_PROG_ACTIVE_MACID_MSG,
   CSM_ETS_PROG_SWITCH_THD_MSG,
   
   /*---------------*/
   /* RUP Msgs */
   /*---------------*/
   RUP_ETS_TCA_MSG,
   RUP_ETS_SET_SUPPORT_CDMACHAN_MSG,
   RUP_ETS_SET_SIMPLE_ATTR_MSG,
   RUP_ETS_SET_SEARCH_PARM_MSG,
   RUP_ETS_SET_MANCHAN_PARM_MSG,
   RUP_ETS_OTA_MSG,
   RUP_TST_SN_TCA_MSG,
   RUP_SNROUTEUPDATE_CMD_MSG,
   RUP_ETS_SET_PNPHASE,
   RUP_ETS_SET_MAC_PARM,
   
   RMC_TEST_RMC_DEACTIVATE_CMD, 
   RMC_TEST_RMC_SPY_DECI_CMD,  
   RMC_TEST_DO_DFS_CMD,
   RMC_TEST_CTRL,
#ifdef MTK_CBP
   RMC_TEST_DO_DFS_SERVCH_CMD,
   RMC_TEST_FMP_MD_CHG_AFT_RXDEACT_CMD,
   RMC_TEST_DO_SET_DFS_CHCNT_CMD,
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   RMC_TEST_DO_DFS_MEET_1X_WAKEUP_CMD,
#endif
#endif
   /*---------------*/
   /* RMC IMD Msgs */
   /*---------------*/
   CP_RMC_IMD_CONFIG_SET_MSG,
   CP_RMC_IMD_CONFIG_GET_MSG,

   /*---------------*/
   /* RMC EQ Msgs */
   /*---------------*/
   CP_RMC_EQ_W_WL_SELECT_SET_MSG,
   CP_RMC_EQ_W_WL_SELECT_GET_MSG,
   
   /*---------------*/
   /* MBP Msgs */
   /*---------------*/
   MBP_ETS_PROC_MCD_RPC_TH,
   MBP_ETS_PROC_MCD_RPC_TH_C2I,

   /*-----Adaptive RX power mode ----*/
	RMC_ETS_DO_ADAPTIVE_RX_CFG_CMD,

   RMC_TEST_MSGID_LAST
} RmcTestMsgT;

/*----------------------------------------------------------------------------
    OTA Message IDs, fro RMC_OTAMSG_MAILBOX, EXE_MAILBOX_3_ID
----------------------------------------------------------------------------*/
typedef enum
{
    RMC_XXX_OTAMSG_MSG = RMC_OTAMSG_MSGID_START,

    RMC_OTAMSG_MSGID_LAST
}RmcOTAMsgT;

/*----------------------------------------------------------------------------
     define signals used by RMC task 
----------------------------------------------------------------------------*/
#define RMC_MAIN_RF_TUNE_COMPLETE_SIGNAL          EXE_SIGNAL_1
#define RMC_DIV_RF_TUNE_COMPLETE_SIGNAL           EXE_SIGNAL_2
#define RMC_SRCH_RESULTS_RDY_SIGNAL               EXE_SIGNAL_3
#define RMC_GENERATE_DECIMATED_TRACE_INFO_SIGNAL  EXE_SIGNAL_4
#define RMC_AFC_UPDATE_SIGNAL                     EXE_SIGNAL_5
#define RMC_FMP_SNR_UPDATE_SIGNAL                 EXE_SIGNAL_6
#define RMC_PROC_DELAYED_SCH_RESULT_SIGNAL        EXE_SIGNAL_7
#define RMC_FMPFASTTIMETRK_ALLFNG_SIGNAL          EXE_SIGNAL_9
#define RMC_FMPSYNCTIME_SIGNAL                    EXE_SIGNAL_10
#define RMC_CSM_CELLSWRDY_SIGNAL                  EXE_SIGNAL_11
#define RMC_FMPFASTTIMETRK_SIGNAL                 EXE_SIGNAL_12
#define RMC_FMPTEST_INSPSYNC_SIGNAL               EXE_SIGNAL_13
#define RMC_FMPTEST_SCHPROC_SIGNAL                EXE_SIGNAL_14
#define RMC_FMPTEST_FNHWDLLSTAT_SIGNAL            EXE_SIGNAL_15
#define RMC_GENERATE_DECIMATED_TRACE_INFO1_SIGNAL EXE_SIGNAL_16  /* for txagc spy */
#if defined (MTK_DEV_OPTIMIZE_EVL1)
#define RMC_SCH_LOGIQ_SIGNAL                      EXE_SIGNAL_18   /* for log IQ */
#endif

/*----------------------------------------------------------------------------
     Message Formats structure
----------------------------------------------------------------------------*/
typedef enum
{
   AUTOMATIC_CTRL_MODE,
   MANUAL_CTRL_MODE /* assign LNA mode */
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   ,
   RSSI_SCAN_MODE
#endif   
   ,
   MANUAL_CTRL_ASSIGN_PWR_MODE /* assign cell power */
}CtrlModeT;

/* RMC_INSP_PILOT_ACQ_MSG */
typedef PACKED_PREFIX struct
{
   uint8    RmcRFPath;
   uint8    CdmaBand;                /* CDMA band class to perform init acq */
   uint16   FreqChan;                /* Frequency channel to perform init acq */

} PACKED_POSTFIX  RmcInspPilotAcqMsgT;

typedef struct
{
   uint8             Count;
   uint16            PilotPN[SYS_MAX_NEIGHBOR_LIST_PILOTS+1];      /* 1A+40N */
   uint8             SrhWinSize[SYS_MAX_NEIGHBOR_LIST_PILOTS+1];   /* 1A+40N */
   uint8             SrhWinOffset[SYS_MAX_NEIGHBOR_LIST_PILOTS+1]; /* 1A+40N */
} Pilot1XInfoT;
/* RMC_INSP_1XASSIST_DOACQ_MSG */
typedef  struct
{
  uint8          RmcRFPath;
  uint8          CdmaBand;         
  uint16         FreqChan; 
  Pilot1XInfoT   PilotInfo; 
  uint8          quickScan;
} RmcInsp1XAssistPilotAcqMsgT;

#ifdef SYS_OPTION_SINGLECHIP_MULTIMODE
typedef PACKED_PREFIX struct
{
  uint8  MeasType;          /* 0: LTE active meas; 1: LTE idle meas */
  uint8  CdmaBand;          /* CDMA bandclass */
  uint16 FreqChan;          /* CDMA channel number */
  uint8  SchWinSize;        /* CDMA pilot search window size. 0xff means not provided */
  uint8  NumOfPN;           /* number of PN included. value 0 means meas done */
  uint16 PilotPN[1];        /* CDMA pilot PN list to measure */
} PACKED_POSTFIX  RmcIratMeasReqT;

typedef struct 
{  
  uint8  reason;          /* 0: non-abort; 1: abort */ 
} RmcIratDOInactivateReqT;  

typedef enum
{
   IRAT_LTE_ABORT     = 0,
   IRAT_DO_INACTIVE   = 1
} DoHwDSOwnerT;

typedef PACKED_PREFIX struct
{
  DoHwDSOwnerT   DoHwDeepSleepCtrlOwner;    /* API causes HW deep sleep, LTE Abort or DoInActive */
  uint8  DoMeasLteMode;                 /* DO measurement LTE mode, 0: LTE active, 1: LTE ilde */
  bool   DoMeasIsRunning;                /* A flag to indicate DO IRAT measurement module is running TRUE or not FALSE */
  bool   DoMeasHscOosaPending;           /* A flag to indicate ProcessHscOosaRequestMsg is pending or not due to DO IRAT meas is running TRUE or not FALSE */
  bool   DoMeasResultSent;              /* Valid DO measurement results are sent, handle timer expires */
  bool   DoMeasResultIsValid;           /* DO measurement results, used to indicate results valid or not */
  bool   DoAbortInProgress;             /* DO measurement Abort is running or not */
  bool   DoInActiveInProgress;          /* DO measurement goes to inactive is running or not */
  bool   DoHwInDeepSleep;               /* Falg to indicate DO goes to deep sleep or not */
  bool   LteAct35MsTimer1stHalfExpired; /* LTE Active, 1st half of 35 ms supervision timer */
  bool   LteAct35MsTimer2ndHalfExpired; /* LTE Active, 2nd half of 35 ms supervision timer */
                                        /* The CTS timer can only handle max 20ms each time */
} PACKED_POSTFIX  RmcDoMeasCrtlT;
#endif

/* RMC_HSC_STOP_TX_MSG */
typedef PACKED_PREFIX struct
{
  uint32 Resync32kCnt;
  bool   Resync32kCntValid;
} PACKED_POSTFIX  RmcHscStopTxMsgT;

typedef PACKED_PREFIX struct
{
/* RMC- HWD interface : actual RFPath index per MpaMdmAntennaTypeT in HSC/MPA grant 
 * Bitmap definition for each RmcMdmAntToRfPathMap[i], refer to 
    MPA_RF_PATH_RX_1 
    MPA_RF_PATH_RX_2 
    MPA_RF_PATH_RX_3  
    MPA_RF_PATH_TX_1 
    MPA_RF_PATH_TX_2  
 */
  uint8 RfAntenna;

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
/* Need to release semaphore when RMC received this mesage */
  bool  NeedRelSema;
#endif
} PACKED_POSTFIX  RmcHscRxActivateMsgT;

/* RFC_ETS_AGC_ISR_ST_MATCH_GET_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  RfcEtsAgcIsrStMatchGetMsgT;

typedef PACKED_PREFIX struct
{
   uint16 SlotStMatch;
   uint16 HalfslotStMatch;
} PACKED_POSTFIX  RfcEtsAgcIsrStMatchGetRspT;

/* RFC_ETS_AGC_ISR_ST_MATCH_SET_MSG */
typedef PACKED_PREFIX struct
{
   uint16 SlotStMatch;
   uint16 HalfslotStMatch;
} PACKED_POSTFIX  RfcEtsAgcIsrStMatchSetMsgT;

/* RFC_ETS_SETTLE_TIMES_GET_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  RfcEtsSettleTimesGetMsgT;

typedef PACKED_PREFIX struct
{
   uint16 RxAgcSlowUsec;
   uint16 RxAgcFastUsec;
} PACKED_POSTFIX  RfcEtsSettleTimesGetRspT;

/* RFC_ETS_SETTLE_TIMES_SET_MSG */
typedef PACKED_PREFIX struct
{
   uint16 RxAgcSlowUsec;
   uint16 RxAgcFastUsec;
} PACKED_POSTFIX  RfcEtsSettleTimesSetMsgT;

/* RFC_ETS_CONFIG_MODEM_RESOURCES_MSG */
typedef PACKED_PREFIX struct
{
   uint8  AirInterfaceType;
} PACKED_POSTFIX  RfcEtsConfigModemResourcesMsgT;

/* RFC_ETS_EMULATE_DSPM_RFC_SPY_MSG */
typedef PACKED_PREFIX struct
{
   uint8  EnableFlag;
} PACKED_POSTFIX  RfcEtsEmulateDspmRfcSpyMsgT;

/* RFC_ETS_AGC_MODE_SET_MSG */
typedef enum
{
   RFC_AGC_TST_MODE_AUTO = 0,
   RFC_AGC_TST_MODE_DISABLED,
   RFC_AGC_TST_MODE_INITACQ,
   RFC_AGC_TST_MODE_FAST,
   RFC_AGC_TST_MODE_STEADY_STATE,
   RFC_AGC_TST_MODE_TRAFFIC
} RfcAgcTstModeT;

typedef PACKED_PREFIX struct
{
   uint8  MainRxAgcTstMode;
   uint8  DivRxAgcTstMode;
} PACKED_POSTFIX  RfcEtsAgcModeSetMsgT; 

/* RFC_ETS_AGC_GAIN_SET_MSG */
typedef PACKED_PREFIX struct
{
   CtrlModeT   CtrlMode;
   uint8       RmcRFPath;
   uint8       RxAgcGainState;  
} PACKED_POSTFIX  RfcEtsAgcGainSetMsgT;

/* RFC_ETS_AGC_CONFIG_MSG */
typedef PACKED_PREFIX struct
{
   uint8  RxAgcBlock;
   uint16 PeriodMask;
   uint32 Window;
   uint16 Target;
   uint8  LoopCoef;
   uint16 DigiGainTiming;
   uint16 StepGainTiming;
   uint8  SlotIsrEnable;
   uint8  HalfSlotIsrEnable;
} PACKED_POSTFIX  RfcEtsAgcConfigMsgT;

/* RFC_ETS_AGC_GET_CONFIG_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8      RxAgcBlock;
} PACKED_POSTFIX  RfcEtsAgcGetConfigMsgT;

/* RMC_ETS_AGC_GET_CONFIG_RSP */
typedef PACKED_PREFIX struct
{
   uint8  RxAgcBlock;
   uint16 PeriodMask;
   uint32 Window;
   uint16 Target;
   uint8  LoopCoef;
   uint16 DigiGainTiming;
   uint16 StepGainTiming;
} PACKED_POSTFIX  RfcEtsAgcGetConfigRspT;

/* RFC_ETS_AGC_GET_DIGIGAIN_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  RfcEtsAgcGetDigiGainMsgT;

/* RMC_ETS_AGC_GET_CONFIG_RSP */
typedef PACKED_PREFIX struct
{
   uint16 DigiGain;
   uint16 DigiBits;
   int16  RxPwr;
   uint16 DivDigiGain;
   uint16 DivDigiBits;
   int16  DivRxPwr;
} PACKED_POSTFIX  RfcEtsAgcGetDigiGainRspT;

/* RFC_ETS_AFC_PDM_DAC_CONTROL_MSG */
typedef PACKED_PREFIX struct
{
   uint8  Mode;
   uint16 AfcPdmDacValue;
} PACKED_POSTFIX  RfcEtsAfcPdmDacControlMsgT;

/* RFC_ETS_AFC_MODE_SET_MSG */
typedef PACKED_PREFIX struct
{
   uint8  AfcTstMode;
} PACKED_POSTFIX  RfcEtsAfcModeSetMsgT;

/* RFC_ETS_AFC_METRIC_CONFIG_GET_MSG */
/* RFC_ETS_AFC_METRIC_CONFIG_SET_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  RfcEtsAfcMetricConfigGetMsgT;

typedef PACKED_PREFIX struct
{
   uint8  NumActiveFingers;
   uint8  Sel_AccumLength;
   uint8  Track_AccumLength;
   uint8  Sel_RSNRThresh;
   uint8  Track_RSNRThresh;
   uint16 Slow_AlphaFilterGain;
   uint16 Fast_AlphaFilterGain;
   uint16 Slow_BetaFilterGain;
   uint16 Fast_BetaFilterGain;
   uint8  Nyquist_Limit;   
   uint8  AFC_DFlag;
} PACKED_POSTFIX  RfcEtsAfcMetricConfigParamsT;

/* RFC_ETS_EMULATE_DSPM_AFC_SPY_MSG */
typedef PACKED_PREFIX struct
{
   uint8  EnableFlag;
} PACKED_POSTFIX  RfcEtsEmulateDspmAfcSpyMsgT;

/* RFC_ETS_DC_OFFSET_CONFIG_MSG */
typedef PACKED_PREFIX struct
{
   uint8  RxAgcBlock;
   uint8  Mode;
   int16  IChOffset;
   int16  QChOffset;
} PACKED_POSTFIX  RfcEtsDcOffsetConfigMsgT;

/* RFC_ETS_RX_AGC_TEST_CASE_MSG */
typedef enum
{
   AGC_TEST_CASE_01 = 0,
   AGC_TEST_CASE_02,
   AGC_TEST_CASE_03a,
   AGC_TEST_CASE_03b,
   AGC_TEST_CASE_04a,
   AGC_TEST_CASE_04b,
   AGC_TEST_CASE_05,
   AGC_TEST_CASE_06a,
   AGC_TEST_CASE_06b,
   AGC_TEST_CASE_07a,
   AGC_TEST_CASE_07b,
   AGC_TEST_CASE_07c,
   AGC_TEST_CASE_07d,
   AGC_TEST_CASE_07e
} RxAgcTestCaseT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8      RxAgcTestCase; /* uses RxAgcTestCaseT enum format */
} PACKED_POSTFIX  RmcRxAgcTestCaseMsgT;

/* RFC_ETS_RX_AGC_TEST_CASE_RSP */
typedef PACKED_PREFIX struct
{
   uint8  TestCase;          /* uses RxAgcTestCaseT enum format */
   uint16 RxDigiGain0;
   uint16 RxDigiBits0;
   uint16 DcOffsetI0;
   uint16 DcOffsetQ0;
   uint16 RxDigiGain1;
   uint16 RxDigiBits1;
   uint16 DcOffsetI1;
   uint16 DcOffsetQ1;
} PACKED_POSTFIX  RmcRxAgcTestCaseRspT;

/* RFC_ETS_AFC_TEST_CASE_MSG */
typedef enum
{
   AFC_METRICS_TEST_PILOT_ACQ_MODE = 0, /* AFC Fast course/fine adjustments with a wide capture range */
   AFC_METRICS_TEST_SLOTTED_MODE,       /* AFC Fast couse/fine adjustments at a fast update period  */
   AFC_METRICS_TEST_ONLINE_MODE         /* AFC fine adjustments at a normal update period */
} RmcAfcTstInitialModeT;

typedef PACKED_PREFIX struct
{
   uint8  Iterations;
   uint8  InitialAfcTestMode;            /* uses RmcAfcTstInitialModeT enum format */
   uint8  NumberActiveFingers;
   uint32 BigBufferInputVectorsOffset;
} PACKED_POSTFIX  RmcAfcTestCaseMsgT;

/* RMC_FMP_MODE_CHANGE_MSG */
typedef enum
{
   FMP_INITACQ_FNG_ASSIGNED, 
   FMP_INITACQ_TO_IDLE, 
   FMP_IDLE_TO_TRAFFIC, 
   FMP_TRAFFIC_TO_IDLE, 
   FMP_IDLE_TO_INITACQ, 
   FMP_TRAFFIC_TO_INITACQ 
} RmcFmpModeChangeT;

typedef PACKED_PREFIX struct
{
   RmcFmpModeChangeT transition;            /* The transition type */
} PACKED_POSTFIX  RmcFmpModeChangeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   FreqChan;                /* Frequency channel to perform init acq */
   uint8    CdmaBand;                /* CDMA band classe to perform init acq */
} PACKED_POSTFIX  RmcIdpRxActivateMsgT;

/*IRAT*/
#ifdef MTK_DEV_C2K_IRAT
#ifdef MTK_DEV_C2K_SRLTE_L1

/* RMC_RUP_DFS_INFO_MSG */
typedef struct
{
   uint8 FreqNumber;                 /* The number of DFS frequency,max is equal 10 */
} RmcRupDfsInfoMsgT;

/*RMC_IRATM_SET_RAT_REQ*/
typedef struct
{
    srlteRatTypeT ratType;   
}RmcIratmSetRatReqT;


/* RMC_IRATM_SYNC_MSG */
typedef struct
{
    bool      bMsgDecodeSuccess;     /* Indicate whether sync message is decoded successful or not. If not, all the items below is invalid*/  
    uint16    SyncMsgPN;             /* The PilotPN from the acquired AN, from SyncMsg */
    uint32    Lower32;               /* Lower 32 bit */
    uint8     Upper6;                /* Upper 6 bit  */
    uint16    FirstCcTimeStamp;      /* The actual time 1stMacPkt was rx from hw, from TimeStamp in RxClt (0-48slots) */
} RmcIratmSyncMsgT;

/*RMC_IRATM_ACQ_CGI_REQ*/
typedef struct
{
    uint8                 tid;                  /* tid is used for EAS to filter the corresponding CNF from CAS, range: 0..255
                                                       the EAS_REPORT_CGI_CNF corresponding to this EAS_REPORT_CGI_REQ should return the same tid */
    report_cgi_req_type_enum  reportCgiReqType;  /* identify the request for CGI report is started or stopped */
    uint8                 band;                 /* indicate the band class of the measured frequency, range: 0..21 */
    uint16                channel;              /* indicate the ARFCN of the measured frequency, range: 0..2047 */
    uint8                 searchWinSize;      /* the search window size used by CDMA for the measurement, range: 0..15
                                                       it should be set according to the value defined in MeasObjectCDMA2000 */
    uint16                pn;                   /* indicate the C2K cell which needs to acquire CGI */
} RmcIratmAcqCgiReqT;


/*RUP_IRATM_DFS_FREQ_NUM_SENT_REQ*/
typedef  struct
{
    bool                 action;               /*0-stop; 1-start;*/   
} RupIratmDfsFreqNumSentReqT;

/*RUP_IRATM_CUR_SECTOR_SIG_MON_REQ*/
typedef  struct
{
    uint8   action;          /*0-stop; 1-start; 2-update threshold only*/
    uint16  threshold;       /*-2x10xlogPS, PS is the pilot strength*/
} RupIratmCurSectorSigMonReqT;
#endif
#endif
/*IRAT*/
#if 0
/* RF Paths */
#define NO_RF_PATH      0
#define MAIN_RF_PATH    1<<0
#define DIV_RF_PATH     1<<1
#define MAIN_DIV_RF_PATHS  (MAIN_RF_PATH | DIV_RF_PATH)
#endif

typedef PACKED_PREFIX struct
{
   SysCdmaBandT   Band;
   uint16         Chan;
} PACKED_POSTFIX  RmcRfT;

/* Rxp Trace Cmd Enum */
typedef enum
{
   Commit,
   Deactivate,
   NetwkRst,
   FmpSchCfg,
   Rxpinit,
   DmaEnb,
   DmaDisb,
   RxpCfg,
   RxpDisb,
   PambEnb,
   PambInit,
   PambIdle,
   PambTrf,
   ClcAct,
   ClcDeact,
   FngAssInitAcq,
   FmpInit2Idle,
   FmpTrf2Idle,
   FmpIdle2Init,
   MpaShutoff,
   MpaPreempt,
   HscAct,
   HscStop,
   HscStopTx,
   RfDone, 
   FngDisb,
   SchRst, 
   Schinit, 
   SchInitOnline, 
   SchOffAdj, 
   SchVerif, 
   SchMini, 
   SchFast,
   SchOnline,
   RfTuneMain, 
   RfTuneDiv, 
   RfAgcSettleM, 
   RfAgcSettleD, 
   RfMainSet, 
   RfDivSet, 
   SchFastFull,
   RfMainRel, 
   RfDivRel, 
   RfMainOff,
   RfDivOff,
   SchDFS,
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   SchOnline6ms,
   SchOnlineScan,	
#endif
} RmcRxpTraceCmdT; 

#ifdef MTK_DEV_C2K_IRAT
typedef PACKED_PREFIX struct 
{  
  uint16 PnCgi;        /* cell to acquire its sector ID. 0xffff means not specified */
  uint8 Band;          /* CDMA bandclass */
  uint16 Channel;      /* CDMA channel number */  
  uint8 SearchWinSize; /* CDMA pilot search window size. 0xff means not provided */
  uint8 NumPn;         /* number of PN included. value 0 means meas done */
  uint16 Pn[1];        /* CDMA pilot PN list */
} PACKED_POSTFIX  RupIdpIratMeasReportReqT;

typedef PACKED_PREFIX struct 
{  
  uint8 Band;          /* target band/channel: if same as current, IDP need not swicth channel. otherwise, switch to this channel */
  uint16 Channel;      
  uint8 NumPn;         /* number of PN included. value 0 means not provided by IRAT(switch freq only) */
  uint16 Pn[1];        /* CDMA pilot PN list. This list need to send to RUP to act as neighbor pilot set */
} PACKED_POSTFIX  RupIdpIratHandoffReqT;

typedef struct 
{  
  uint8 Action;   /* 0-stop; 1-start; 2-update threshold only */
  int8 Threshold; /* -2x10xlogPS, PS is the pilot strength */
} RupIratRATSigMonReqT;
typedef enum
{
    IHO_ALL = 0x00,   /* Indicates both for intra-freq iho and inter_freq iho.*/
    IHO_INTRA_FREQ,   /* Indicates intra-freq iho.*/
    IHO_INTER_FREQ    /* Indicates inter-freq iho.*/
} RupIhoTypeT;

/*RUP_CSS_IHO_DISABLE_REQ*/
typedef struct 
{  
  RupIhoTypeT  IhoType;
} RupCssIhoDisableReqT;

/*RUP_CSS_IHO_ENABLE_REQ*/
typedef struct 
{  
  RupIhoTypeT  IhoType;
} RupCssIhoEnableReqT;
 
#endif /* MTK_DEV_C2K_IRAT */

typedef struct 
{  
  uint16 SlotNum; /*6-normal acq, 2-fast-acq;*/
} RmcCssDoScanSlotNumInd;

typedef enum
{
   RX_DEACT_IDP_HO,
   RX_DEACT_IDP_CHAN_ASSIGN,
   RX_DEACT_IRAT_MEAS_REQ,
   RX_DEACT_IRAT_HO_REQ,
   RX_DEACT_IRAT_CHAN_HASH,
   RX_DEACT_RUP_CHAN_ASSIGN,
   RX_DEACT_DFS_START,
   RX_DEACT_DFS_RESTORE_SERVING,
   RX_DEACT_DFS_IDLE_HO,
   RX_DEACT_DAT_CFG_ENABLE,
   RX_DEACT_MAX_CAUSE_CODE
} RxDeactivateCause;

typedef PACKED_PREFIX struct
{
   RxDeactivateCause Cause;
} PACKED_POSTFIX  RmcRupRxDeactivateMsgT;

typedef PACKED_PREFIX struct
{
   RxDeactivateCause Cause;
} PACKED_POSTFIX  RmcIdpDeactivateMsgT;

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
typedef PACKED_PREFIX struct
{
   bool Protection;
} PACKED_POSTFIX  RmcIdpAccessProtectionMsgT;

typedef PACKED_PREFIX struct
{
   uint32   slotCyclePeriod; /* EVDO slot cycle period, unit in slots */
} PACKED_POSTFIX  RmcClcIdleModeCfgMsgT;
#endif

typedef PACKED_PREFIX struct
{
   RxDeactivateCause Cause;
} PACKED_POSTFIX  RmcAlmpDeactivateMsgT;
/*----------------------------------------------------------------------------
 define for RMC_[TEMP/FREQ]_ADJ_UPDATE_MSG
----------------------------------------------------------------------------*/
typedef struct
{
   uint8  MainDivRx;    /* Main=0, Div=1 */
   int16  TempAdjALog2;
}RmcTempAdjUpdateT;

typedef struct
{
   uint8  MainDivRx;    /* Main=0, Div=1 */
   int16  FreqAdjALog2[HWD_MAX_NUM_DIGITAL_GAIN_STATES];
}RmcFreqAdjUpdateT;

/*----------------------------------------------------------------------------
 define for RMC_LEC_MOBSYSTEMOFFSET_QUERY_MSG
----------------------------------------------------------------------------*/
typedef enum {
   START,
   END
} LecQueryCmdTypeT;
 
typedef struct
{
   LecQueryCmdTypeT lecQueryCmdType;
} LecMobSystemOffsetQueryCmdT;

/*----------------------------------------------------------------------------
 define for LEC_RMC_MOBSYSTEMOFFSET_RSP_MSG
----------------------------------------------------------------------------*/
typedef struct {
   uint16 fingerPos;
   uint16 pwrEstQ16;
   int16 offsetTc16;
   uint16 pilotPN;
} FingerInfoT;

typedef struct
{
   uint8 StartFlag;//Temp added
   int16 SysTimeOffsetQ4;
   FingerInfoT EarliestFinger;
   FingerInfoT StrongestFinger;
} LecMobSystemOffsetRspT;


/*----------------------------------------------------------------------------
     define RXSD Register Table 
----------------------------------------------------------------------------*/
#if (SYS_ASIC == SA_MT6735)
#define MTK_MAIN_RXSD_REGISTERS \
   MTK_HWD_M_RXSD_REGSEL, \
   MTK_HWD_M_RXSD_ENABLES, \
   MTK_HWD_M_RXSD_ADC_MODE, \
   MTK_HWD_M_RXSD_DC_IIR_COEF, \
   MTK_HWD_M_RXSD_GAIN_UP, \
   MTK_HWD_M_RXSD_OFFSET_I, \
   MTK_HWD_M_RXSD_OFFSET_Q, \
   MTK_HWD_M_RXSD_CTRL, \
   MTK_HWD_M_RXSD_RXAGC_DIGIGAIN, \
   MTK_HWD_M_RXSD_DIGIGAIN_TIMING, \
   MTK_HWD_M_RXSD_STEPGAIN_TIMING, \
   MTK_HWD_M_RXSD_STEPGAINADJ, \
   MTK_HWD_M_RXSD_WINDOW_LSB, \
   MTK_HWD_M_RXSD_WINDOW_MSB, \
   MTK_HWD_M_RXSD_LOG2TARGET, \
   MTK_HWD_M_RXSD_PERIODMASK, \
   MTK_HWD_M_RXSD_LOOPGAIN, \
   MTK_HWD_M_RXSD_DCO_TP_I, \
   MTK_HWD_M_RXSD_DCO_TP_Q, \
   MTK_HWD_M_RXSD_RXAGC_BITSELDIG_I, \
   MTK_HWD_M_RXSD_FIRTAP

#define MTK_DIV_RXSD_REGISTERS \
   MTK_HWD_D_RXSD_REGSEL, \
   MTK_HWD_D_RXSD_ENABLES, \
   MTK_HWD_D_RXSD_ADC_MODE, \
   MTK_HWD_D_RXSD_DC_IIR_COEF, \
   MTK_HWD_D_RXSD_GAIN_UP, \
   MTK_HWD_D_RXSD_OFFSET_I, \
   MTK_HWD_D_RXSD_OFFSET_Q, \
   MTK_HWD_D_RXSD_CTRL, \
   MTK_HWD_D_RXSD_RXAGC_DIGIGAIN, \
   MTK_HWD_D_RXSD_DIGIGAIN_TIMING, \
   MTK_HWD_D_RXSD_STEPGAIN_TIMING, \
   MTK_HWD_D_RXSD_STEPGAINADJ, \
   MTK_HWD_D_RXSD_WINDOW_LSB, \
   MTK_HWD_D_RXSD_WINDOW_MSB, \
   MTK_HWD_D_RXSD_LOG2TARGET, \
   MTK_HWD_D_RXSD_PERIODMASK, \
   MTK_HWD_D_RXSD_LOOPGAIN, \
   MTK_HWD_D_RXSD_DCO_TP_I, \
   MTK_HWD_D_RXSD_DCO_TP_Q, \
   MTK_HWD_D_RXSD_RXAGC_BITSELDIG_I, \
   MTK_HWD_D_RXSD_FIRTAP
#elif (SYS_ASIC >= SA_MT6755)
#define MAIN_RXSD_REGISTERS \
   HWD_JADE_RXSD_REGSEL_M, \
   HWD_JADE_RXSD_ENABLES_M, \
   HWD_JADE_RXSD_ADC_MODE_M, \
   HWD_JADE_RXSD_DC_IIR_COEF_M, \
   HWD_JADE_RXSD_GAIN_UP_M, \
   HWD_JADE_RXSD_OFFSETI_M, \
   HWD_JADE_RXSD_OFFSETQ_M, \
   HWD_JADE_RXSD_CTRL_M, \
   HWD_JADE_RXSD_RXAGC_DIGIGAIN_M, \
   HWD_JADE_RXSD_DIGIGAIN_TIMING_M, \
   HWD_JADE_RXSD_STEPGAIN_TIMING_M, \
   HWD_JADE_RXSD_STEPGAINADJ_M, \
   HWD_JADE_RXSD_WINDOW_LSB_M, \
   HWD_JADE_RXSD_WINDOW_MSB_M, \
   HWD_JADE_RXSD_LOG2TARGET_M, \
   HWD_JADE_RXSD_PERIODMASK_M, \
   HWD_JADE_RXSD_LOOPGAIN_M, \
   HWD_JADE_RXSD_DCO_TP_I_M, \
   HWD_JADE_RXSD_DCO_TP_Q_M, \
   HWD_JADE_RXSD_RXAGC_BITSELDIG_I_M, \
   HWD_JADE_RXSD_FIRTAP_M

#define DIV_RXSD_REGISTERS \
   HWD_JADE_RXSD_REGSEL_D, \
   HWD_JADE_RXSD_ENABLES_D, \
   HWD_JADE_RXSD_ADC_MODE_D, \
   HWD_JADE_RXSD_DC_IIR_COEF_D, \
   HWD_JADE_RXSD_GAIN_UP_D, \
   HWD_JADE_RXSD_OFFSETI_D, \
   HWD_JADE_RXSD_OFFSETQ_D, \
   HWD_JADE_RXSD_CTRL_D, \
   HWD_JADE_RXSD_RXAGC_DIGIGAIN_D, \
   HWD_JADE_RXSD_DIGIGAIN_TIMING_D, \
   HWD_JADE_RXSD_STEPGAIN_TIMING_D, \
   HWD_JADE_RXSD_STEPGAINADJ_D, \
   HWD_JADE_RXSD_WINDOW_LSB_D, \
   HWD_JADE_RXSD_WINDOW_MSB_D, \
   HWD_JADE_RXSD_LOG2TARGET_D, \
   HWD_JADE_RXSD_PERIODMASK_D, \
   HWD_JADE_RXSD_LOOPGAIN_D, \
   HWD_JADE_RXSD_DCO_TP_I_D, \
   HWD_JADE_RXSD_DCO_TP_Q_D, \
   HWD_JADE_RXSD_RXAGC_BITSELDIG_I_D, \
   HWD_JADE_RXSD_FIRTAP_D
#else
#define MAIN_RXSD_REGISTERS \
   HWD_M_RXSD_REGSEL, \
   HWD_M_RXSD_ENABLES, \
   HWD_M_RXSD_ADC_MODE, \
   HWD_M_RXSD_DC_IIR_COEF, \
   HWD_M_RXSD_GAIN_UP, \
   HWD_M_RXSD_OFFSETI, \
   HWD_M_RXSD_OFFSETQ, \
   HWD_M_RXSD_CONTROL, \
   HWD_M_RXSD_RXAGC_DIGIGAIN, \
   HWD_M_RXSD_DIGIGAIN_TIMING, \
   HWD_M_RXSD_STEPGAIN_TIMING, \
   HWD_M_RXSD_STEPGAINADJ, \
   HWD_M_RXSD_WINDOW_LSB, \
   HWD_M_RXSD_WINDOW_MSB, \
   HWD_M_RXSD_LOG2TARGET, \
   HWD_M_RXSD_PERIODMASK, \
   HWD_M_RXSD_LOOPGAIN, \
   HWD_M_RXSD_DCOFFSET_TP_I, \
   HWD_M_RXSD_DCOFFSET_TP_Q, \
   HWD_M_RXSD_RXAGC_BITSELDIG_I, \
   HWD_M_RXSD_FIRTAP

#define DIV_RXSD_REGISTERS \
   HWD_D_RXSD_REGSEL, \
   HWD_D_RXSD_ENABLES, \
   HWD_D_RXSD_ADC_MODE, \
   HWD_D_RXSD_DC_IIR_COEF, \
   HWD_D_RXSD_GAIN_UP, \
   HWD_D_RXSD_OFFSETI, \
   HWD_D_RXSD_OFFSETQ, \
   HWD_D_RXSD_CONTROL, \
   HWD_D_RXSD_RXAGC_DIGIGAIN, \
   HWD_D_RXSD_DIGIGAIN_TIMING, \
   HWD_D_RXSD_STEPGAIN_TIMING, \
   HWD_D_RXSD_STEPGAINADJ, \
   HWD_D_RXSD_WINDOW_LSB, \
   HWD_D_RXSD_WINDOW_MSB, \
   HWD_D_RXSD_LOG2TARGET, \
   HWD_D_RXSD_PERIODMASK, \
   HWD_D_RXSD_LOOPGAIN, \
   HWD_D_RXSD_DCOFFSET_TP_I, \
   HWD_D_RXSD_DCOFFSET_TP_Q, \
   HWD_D_RXSD_RXAGC_BITSELDIG_I, \
   HWD_D_RXSD_FIRTAP

#define SEC_RXSD_REGISTERS \
   HWD_S_RXSD_REGSEL, \
   HWD_S_RXSD_ENABLES, \
   HWD_S_RXSD_ADC_MODE, \
   HWD_S_RXSD_DC_IIR_COEF, \
   HWD_S_RXSD_GAIN_UP, \
   HWD_S_RXSD_OFFSETI, \
   HWD_S_RXSD_OFFSETQ, \
   HWD_S_RXSD_CONTROL, \
   HWD_S_RXSD_RXAGC_DIGIGAIN, \
   HWD_S_RXSD_DIGIGAIN_TIMING, \
   HWD_S_RXSD_STEPGAIN_TIMING, \
   HWD_S_RXSD_STEPGAINADJ, \
   HWD_S_RXSD_WINDOW_LSB, \
   HWD_S_RXSD_WINDOW_MSB, \
   HWD_S_RXSD_LOG2TARGET, \
   HWD_S_RXSD_PERIODMASK, \
   HWD_S_RXSD_LOOPGAIN, \
   HWD_S_RXSD_DCOFFSET_TP_I, \
   HWD_S_RXSD_DCOFFSET_TP_Q, \
   HWD_S_RXSD_RXAGC_BITSELDIG_I, \
   HWD_S_RXSD_FIRTAP

#define MAIN_RXSD_MASKS \
   HWD_CF_RXSD_DIS_M

#define DIV_RXSD_MASKS \
   HWD_CF_RXSD_DIS_D

#define SEC_RXSD_MASKS \
   HWD_CF_RXSD_DIS_S

typedef struct
{
   uint16   cf_rxsd_dis;
} RmcRxSdMaskT;

#endif


typedef struct
{
   uint32   RxsdSelectReg;
   uint32   RxSdEnableReg;
   uint32   AdcModeReg;
   uint32   DcIirCoefReg;
   uint32   GainUpReg;
   uint32   OffsetIReg;
   uint32   OffsetQReg;
   uint32   AgcControlReg;
   uint32   DigiGainReg;
   uint32   DigiGainTimingReg;
   uint32   StepGainTimingReg;
   uint32   StepGainAdjReg;
   uint32   RssAvgLsbReg;
   uint32   RssAvgMsbReg;
   uint32   Log2TargetReg;
   uint32   UpdatePeriodReg;
   uint32   LoopGainReg;
   uint32   OffsetTPIReg;
   uint32   OffsetTPQReg;
   uint32   BitSelDigIReg;
   uint32   FirTapReg;
} RmcRxSdRegT;


typedef struct
{
   SysCdmaBandT  CurrentBand;
   int16         RssiValMain;
   int16         RssiValDiv;
}RmcStatusParametersT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))

/** define AFC configure message for calibraiton
*/
typedef PACKED_PREFIX struct
{
    /* Response info*/
    ExeRspMsgT  RspInfo;

    /* Manual for auto mode*/
    uint8       CtrlMode;

    /* Cap Id valid indication*/
    uint8       CapIdValid;
    
    /* Cap Id*/
    uint16       CapId;
    
    /* AFC DAC valid indication*/
    uint8        DacValid;
    
    /*AFC DAC value*/ 
    uint16     DacValue; 
} PACKED_POSTFIX  RfcEtsAfcControlMtkMsgT;

/* RFC_ETS_AFC_FOE_CONFIG_MSG */
typedef PACKED_PREFIX struct
{
   int16 AfcFoeValue;
} PACKED_POSTFIX  RfcEtsAfcFoeConfigMsgT;

/** define AGC configure message for calibraiton
*/
typedef PACKED_PREFIX struct
{
    /* Response info*/
    ExeRspMsgT  RspInfo; 

   /* Manual or auto, manual for CAL*/
   uint8   CtrlMode;

   /* Rx path bitmap*/
   uint8   PathBitmap; 

   /* for Denali: LNA mode 0: high, 1: middle, 2: low */
   /* for Jade:   0: stage 0, 1: stage 1, 2: stage 2, 3: stage 3, 4: stage 4, 5: stage5 */
   uint8   LnaMode; 

   /* Cal Input power*/
   int16   InputPwr;

#if (SYS_BOARD >= SB_JADE)
    /* 0: high power mode, 1: low power mode */
    uint8  PwrMode;
#endif

    /* Dump Iq in test mode indication*/
    bool   DumpIqTst;
} PACKED_POSTFIX  RfcEtsAgcCfgMtkMsgT;

/** define AGC configure message for RSSI scan
*/
typedef PACKED_PREFIX struct
{
    /* Response info*/
    ExeRspMsgT  RspInfo; 

   /* Rx path bitmap*/
   uint8   PathBitmap; 

} PACKED_POSTFIX RfcEtsAgcCfgRssiScanMtkMsgT;

/** define AGC gain up threshold configure message
*/
typedef PACKED_PREFIX struct
{
    uint8    CtrlMode;
    int16    GainUpThreshold;
} PACKED_POSTFIX  RfcEtsAgcGainUpThresholdMsgT;


/** Start/Stop digital gain report (For TRC)*/
typedef PACKED_PREFIX struct
{
    /* Response info*/
    ExeRspMsgT  RspInfo; 

    /** On/Off reporting digital gain */
    uint8  Action;

} PACKED_POSTFIX  RfcEtsDigGainStartMtkMsgT;


#endif

#if defined(__DYNAMIC_ANTENNA_TUNING__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)

#define DAT_OFF (-1)

typedef struct
{
    int16    featureIndex;
    int16    scenaryIndex;
}RfcDatSarParaT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT        RspInfo;/*rsp:task id,mail box ,Msgid*/
    RfcDatSarParaT       datsarPara;          
    int16  SeqNum;
}PACKED_POSTFIX RmcRfcValDatSarCfgMsgT;

typedef PACKED_PREFIX struct
{
  int16  featureIndex;
  bool   DoDatSarCfgRsp;  /*TRUE:DO SUCESS CFG RF   Fail:DO do not  CFG RF */             
  int16  SeqNum;
}PACKED_POSTFIX ValRmcRfcDatSarCfgRspMsgT;
#endif

/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/

/* RMC RF band/channel tracking variables */
extern RmcRfT   RmcRfMain[SYS_MODE_MAX];
extern RmcRfT   RmcRfDiv[SYS_MODE_MAX];

/* RMC RF Control AGC Management Boolean flags */
extern bool RmcMainRxAgcEnabled, RmcDivRxAgcEnabled;

/* Rx AGC Data structures for ISR tracking variables */
extern RmcAgcDataT RmcMainAgcData;
extern RmcAgcDataT RmcDivAgcData;
extern RmcAfcAdjDataT RmcAfcAdjData;
extern int32 rmcAfcOffsetHzQ4;
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern RmcAfcDataT RmcAfcData;
extern bool RmcRxAgcGainUpManual;
extern int16 RmcRxAgcGainUpThreshold;
#endif


/* FMP */
extern uint16   FmpServingPN;		             /* Serving Sec pilotPN */
extern uint16   FmpServingPNNew;		         /* Target Sec pilotPN */
extern uint16   FmpServingMacId;                /* Serving Sec pilotPN */
extern uint8    FmpServingDrcCover;            /* Serving Sec Drc Cover*/ 
extern uint8    FmpServingDscValue;            /* Serving Sec Dsc Value*/ 
extern uint16   FmpServingMacIdNew;             /* New Serving Sec pilotPN from CellSwitch decision */
extern uint32   FmpServingPilotStrength;	    /* Serving Sec pilot pwr and snr info packed in one */
extern uint16   RxsdFirTapDefaultValue;
extern uint16   RxAgcFirTapAdjLog2Q7;
extern int16   RxAgcMGainUpAdjLog2Q7;
extern int16   RxAgcDGainUpAdjLog2Q7;


extern bool   RmcMacHwMacJammerSatEcoFixFlag;

extern bool gRmcFmpDivPwrOn;

/* RMC */
typedef enum
{
  PREEMPT_NONE, 
#ifdef MTK_CBP
  PREEMPT_INACTIVATE,
#endif
  PREEMPT_INITACQ, 
  PREEMPT_MINIACQ, 
  PREEMPT_ONLINE_IDLE,
  PREEMPT_ONLINE_TRAFFIC,
  PREEMPT_WITH_NETWORK_RESET
} RmcPreemptT;

extern uint8 RmcMpaMdmPaths;
extern uint8 RmcMdmPaths;

extern uint8 RmcRFMode;     /* 0=No RF, i.e. bypass the RF for big buffer testing */
extern bool  RmcSlottedMode;
extern RmcPreemptT       RmcRFPreamptMode;
extern bool FmpMacReliabilityFlag;
extern int16 MainfiltSnrEstQ16Sum;
extern uint8 DelayedFastFullResultRdy;
extern  uint16  PmbEnbPamMAll[2];    
extern  uint16  PmbSnrBackOff[2][4]; 
extern  uint16  PMB_ENB_PAMB_M_ALL;  
extern  uint16  PMB_SNRBACKOFF_AWGN; 
extern  uint16	PMB_SNRBACKOFF_SLOW; 
extern  uint16	PMB_SNRBACKOFF_FAST; 
extern  uint16	PMB_SNRBACKOFF_FFAST;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#if (SYS_BOARD >= SB_JADE)
extern RmcRxSdRegT RmcRxSdRegTbl[];
extern RmcRxSdRegT *MainRxSdRegP, *DivRxSdRegP;
#endif
#ifdef MTK_PLT_DENALI
extern RmcRxSdRegT RmcRxSdRegTblMtk[];
extern RmcRxSdRegT *MainRxSdRegMtkP, *DivRxSdRegMtkP;
#endif
#else
extern RmcRxSdRegT RmcRxSdRegTbl[];
extern RmcRxSdRegT *MainRxSdRegP, *DivRxSdRegP;
#endif

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/

/* rmctask.c */
extern void RmcTask(uint32 argc, void *argv);
extern void RmcNetworkReset( void );
extern void RmcCommit(uint8 SubType);
extern void RmcMbpCommit(uint8 SubType);

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
extern void ProcessMpaRfDivReleaseReq(void);
#endif
extern void ProcessMpaRfPreemptMsg(void);
extern void ProcessRcpRfDoneMsg(void);
extern void RmcFmpSNRUpdateTrigger(void);
extern void RmcProcDelayedSchResultSignal(void);
extern void RmcGenerateTraceSignal(void);
extern void RmcRfTxPathSetFlag(void);
extern void RmcRfTxPathSetHw(uint8 forcedPath);
extern void RmcRfPathsTurnOff(uint8 RfPaths, bool bitmask_updt_flag);
extern bool RmcMpaMdmPathsChkTxStatus(bool isTraffic);
#if defined (MTK_DEV_OPTIMIZE_EVL1)
extern void RmcRecordTimingStamp(RmcTimingStampIdT  StampId);
extern void RmcCheckTxAgcSlotIsrTiming(HwdRfConstantsT *RfConstants);
extern void RmcCheckTxAgcHalfSlotIsrTiming(HwdRfConstantsT *RfConstants);
#endif
/* Shadow RMC-controlled HW registers */
extern uint32 HwdRead_shadow(uint32 reg);
extern void HwdWrite_shadow(uint32 reg, uint32 value);
extern void HwdWrite_shadow_all(void);

/* Rxpath Apis */
extern void RxpInit( bool resetFlag );
extern void RxpReset( bool config );
extern void RxpMdmDmaEnable( void );
extern void RxpMdmDmaDisable( void );
extern void RxpConfig( void );
extern void RxpDisable( bool initFlag );
extern void RxpPambEnable( void );
extern void RxpPambConfigInitAcq( void );
extern void RxpPambConfigIdle( void );
extern void RxpPambConfigMU( bool enable );
extern void RxpPambConfigDOF( uint8 DOF );
extern void RxpPambConfigSNRBkOff( uint8 SnrBkOff, uint8 SnrBkOffNew);
extern void RxpRxcConfigMinContSpan( uint8 MinContSpan );


/* rmcagc.c */
extern void RmcConfigMainRxAgc(RmcAgcStateT AgcState);
extern void RmcConfigMainRxAgcForDiversityOnly(RmcAgcStateT AgcState);
extern void RmcConfigDivRxAgc(RmcAgcStateT AgcState);
extern void RmcHwdSetMainRxRefLevelALog2(HwdRfBandT  RfBand, int16 RxRefLevelDb);
extern void RmcHwdSetDivRxRefLevelALog2(HwdRfBandT  RfBand, int16 RxRefLevelDb);
extern void RmcHwdSetMainNoIMDRxRefLevelALog2(HwdRfBandT  RfBand, int16 RxRefLevelDb);
extern void RmcHwdSetDivNoIMDRxRefLevelALog2(HwdRfBandT  RfBand, int16 RxRefLevelDb);
extern void RmcHwdSetMainNoIMD(bool NoIMD);
extern void RmcHwdSetDivNoIMD(bool NoIMD);


/* rmcrfc.c */
extern void RmcRfMainDiversityPathsRelease(uint8 RfPaths, bool bitmask_updt_flag);

/* rmcsch.c */
extern void SchHwDoneLisr(void);

/* rmccsm.c */
/* CSM interface Enums */
typedef enum
{
   CSM_UPDATE_C2I_CMD,
   CSM_UPDATE_SECTOR_INFO_CMD,
   CSM_SWITCH_DONE_CMD,
   CSM_ACTIVATE_CMD,
   CSM_DEACTIVATE_CMD,
   CSM_ENTER_FIXED_RATE_MODE_CMD,
   CSM_EXIT_FIXED_RATE_MODE_CMD,
   CSM_PREEMPT_DO_CMD,
   CSM_RESUME_DO_CMD
} CsmCmdIdT;

extern void CsmProcessor(CsmCmdIdT cmdId, void* cmdData);
extern void CsmSessionConfigCommit(uint16 subtype);
extern void CsmCellSwitchReadyLisr(void);
extern bool CsmGetCellSwitchStatus(void);
extern void CsmInfoRetrieve(int16* CurrCsmSvPnP);

/* for ACMAC to read resync time for Rx overlap test */
extern bool Get1XScheduledWakeupTime(uint64 *time1xWakeupInDOSlots);

/* rmcfmp.c */
extern void FmpHwSlotFoundLisr( void );
extern uint32 FmpGetDOPilotStrength(void);
extern void DoGet1XPilotPN(bool bActive, Pilot1XInfoT* pInfo);
extern int16 DoGet1XStrongestEcIo(void);
extern void RupSendRTCMACActivateMsg(void);
extern void RupSendFTCMACActivateMsg(void);
extern void RupSendACMACActivateMsg(uint8 reason);
extern void RupSend2DsarRemoveRupUpdateMsg(bool bNeedTxStatus);
extern void SetFmpDllR1ThreshTc16( uint16 value );
extern void RmcAfcEnableGpsMode(bool Enable);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern int32 RmcGetConnModeAfcDcxoFoe(void);
#endif

extern void RmcRupGetSchParms(SchParmUpdateMsgT* pParm);

extern void RmcRpcSlotIsr(void);
extern void RmcRpcFastAgcIsr(void);

extern bool RmcRupGetActivePilotStrength(UINT16 *PilotPN,UINT16 *PnPhase,UINT16 *Strength);

#ifdef MTK_DEV_C2K_IRAT
#if defined (MTK_DEV_C2K_SRLTE_L1) || defined (MTK_DEV_C2K_SRLTE)
extern bool RmcRupGetActivePilotInfo(UINT8 *Band, UINT16 *Channel, UINT16 *PilotPN,INT16 *PnPhase,UINT16 *Strength);
#endif
#endif

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/* rmcagc.c */
extern void RmcTempAdjUpdateProcess(void *MsgPtr);
extern void RmcFreqAdjUpdateProcess(void *MsgPtr);

/* rmcimd.c */
extern bool RmcImdInitCalInImdMode(void);
extern void RmcImdGainTableCalibSet(HwdImdTableCalibConfigSetMsgT *MsgP);

#endif

extern void RmcLecMobSysOffsetQueryProcessPending(void);

/* rcptxh.c */
extern bool RcpTrafficIsOn(void);

extern RmcStatusParametersT* RmcGetStatusParameters(void);
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
extern bool RmcRatStateIsStb(void);
extern void RmcCopy1xResyncTime(uint32 Frc);
#endif /* MTK_DEV_C2K_IRAT */


/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:15:25 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:19:43 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
