/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
* 
* FILE NAME   : valkeypad.h
*
* DESCRIPTION :
*
*   This is the internal interface include file for Keypad.
*
* HISTORY     :
*
*   See Log at end of file
*
*****************************************************************************/
#ifndef _VAL_KEYPAD_H_
#define _VAL_KEYPAD_H_

#include "sysKeydef.h"
#include "valapi.h"
#include "hwdkpad.h"


#ifdef __cplusplus
extern "C"
{
#endif

//don't modify following Enum, brew has referred to it
/* define status in VAL */
typedef enum
{
    VAL_KEY_PRESS = HWD_KEY_PRESS,
    VAL_KEY_RELEASE = HWD_KEY_RELEASE,
    VAL_KEY_HOLD = HWD_KEY_HOLD,
    VAL_KEY_STATUS_NUM
}ValKeyStatusT;

//don't modify following Struct, brew has referred to it
typedef PACKED_PREFIX struct
{
    ValKeyStatusT   Status;
    SysKeyIdT       KeyId;
} PACKED_POSTFIX ValKeypadMsgT;

/*****************************************************************************
    FUNCTION NAME: ValKeypadInit

    DESCRIPTION:
        this method is used to initialize keypad in VAL.

    PARAMETERS:
        MultiKey        if it is TRUE, then multi key mode, otherwise single key
                        mode.

        KeyHoldDuration Key hold time, unit is 100ms. if 0, there is no hold status.

        colNum          number of keypad column, now it must be either 4 or 5
    RETURNED VALUES:
        None.
*****************************************************************************/
void    ValKeypadInit(bool MultiKey, uint8 KeyHoldDuration, uint8 colNum );   /* VAL keypad init  */

//don't modify following Function, brew has referred to it
/*****************************************************************************
    FUNCTION NAME: ValKeypadRegister

    DESCRIPTION:
        this method is used to register keypad service by application.

    PARAMETERS:
        EventFuncP          Callback function provided by the application.

    RETURNED VALUES:
        RegIdT = -1         Failure.
                            otherwise, successful and register id.
*****************************************************************************/
RegIdT  ValKeypadRegister(ValEventFunc EventFuncP);             /* VAL keypad register */

//don't modify following Function, brew has referred to it
/*****************************************************************************
    FUNCTION NAME: ValKeypadUnregister

    DESCRIPTION:
        this method is used to un-register keypad service by application.

    PARAMETERS:
        RegId              the valid register id through ValKeypadRegister()

    RETURNED VALUES:
        None.
*****************************************************************************/
void    ValKeypadUnregister(RegIdT RegId);                      /* VAL keypad unregister */

/*****************************************************************************
    FUNCTION NAME: ValKeypadProcess

    DESCRIPTION:
        this method is to process val keypad messsage.

    PARAMETERS:
        MsgDataP            the data pointer of msg
        MsgSize             the size of msg

    RETURNED VALUES:
        None.
*****************************************************************************/
void    ValKeypadProcess(ValKeypadMsgT* MsgDataP, uint32 MsgSize);

#ifndef MTK_CBP
/*****************************************************************************
    FUNCTION NAME: ValKeyGetFlipKeyStatus

    DESCRIPTION:
        this method is to get the status of the flip key. This method should 
        be called by task in initialization and after keypad init.

        This method should be call once and then all status of the key is 
        notify by register function of application.

    PARAMETERS:
        None.

    RETURNED VALUES:
        VAL_KEY_PRESS / VAL_KEY_RELEASE
*****************************************************************************/
ValKeyStatusT ValKeypadGetFlipKeyStatus (void);                 /* VAL get flip key status */


void ValKeypadDisable(bool flag);

/*****************************************************************************
    FUNCTION NAME: ValKeypadMultikeyEn

    DESCRIPTION:
        The routine can enable or disable keypad driver support multi-key mode. After call the routine, after
         all key are released, then the new setting will work.

    PARAMETERS:
        En, TRUE or FALSE.

    RETURNED VALUES:
        None.
*****************************************************************************/
void ValKeypadMultikeyEn(bool En);
#endif

#ifdef __cplusplus
}
#endif

#endif
/*****************************************************************************
* $Log: valkeypad.h $
* Revision 1.3  2006/01/07 12:11:54  wavis
* Merging in VAL.
* Revision 1.2.1.3  2005/12/16 10:15:11  wavis
* Moved some message handlers from VAL to UI.
* Revision 1.2.1.2  2005/11/22 11:54:02  wavis
* Add keypad register/unregister function prototypes.
* Revision 1.2.1.1  2005/11/10 10:22:57  wavis
* Duplicate revision
* Revision 1.2  2005/11/10 10:22:57  wavis
* Add more key definitions.
* Revision 1.1  2005/11/07 14:59:27  wavis
* Initial revision
* Revision 1.1  2005/10/13 14:11:52  dorloff
* Initial revision
* Initial revision
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_zlin_href17211\1 2011-06-10 06:12:58 GMT zlin
** HREF#17211. Support multi key dynamic.**/
/**Log information: \main\CBP7FeaturePhone\4 2011-06-10 06:19:30 GMT zlin
** Merge href 17211.**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\1 2011-07-04 08:26:09 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\2 2011-07-08 06:57:21 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\5 2011-07-12 09:40:57 GMT marszhang
** HREF#17384**/
