/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _IPCMBOX_H_
#define _IPCMBOX_H_
/*****************************************************************************
 
  FILE NAME: ipcmbox.h

  DESCRIPTION: 

    This file contains CP IPC mailbox definitions 

*****************************************************************************/

/* Define IPC mailbox names */
#define IPC_MAILBOX_DSPM_ASYN     EXE_MAILBOX_1_ID   
#define IPC_MAILBOX_DSPV_ASYN     EXE_MAILBOX_2_ID      
#define IPC_MAILBOX_CMD           EXE_MAILBOX_3_ID
#define IPC_MAILBOX_DSPV_APPS     EXE_MAILBOX_4_ID
   

/* Macros which set the CP to DSP direct buffer or control mailbox status */

/* Indicate to the DSPM that the CP has finished reading the direct buffer mailbox */
#define IPC_C2M_D_RRDY            (HwdWrite(HWD_MBM_C2M_D_RRDY, HWD_MMB_RHS))

/* Indicate to the DSPM that the CP has finished writing the direct buffer mailbox */
#define IPC_C2M_D_WRDY            (HwdWrite(HWD_MBM_C2M_D_WRDY, HWD_MMB_WHS))

/* Indicate to the DSPM that the CP has finished reading the control mailbox */
#define IPC_C2M_C_RRDY            (HwdWrite(HWD_MBM_C2M_C_RRDY, HWD_MMB_RHS))

/* Indicate to the DSPM that the CP has finished writing the control mailbox */
#define IPC_C2M_C_WRDY            (HwdWrite(HWD_MBM_C2M_C_WRDY, HWD_MMB_WHS))
#ifndef MTK_PLT_AUDIO
/* Indicate to the DSPV that the CP has finished reading the mailbox */
#define IPC_C2V_RRDY              (HwdWrite(HWD_MBV_C2V_RRDY, HWD_VMB_RHS))

/* Indicate to the DSPV that the CP has finished writing the mailbox */
#define IPC_C2V_WRDY              (HwdWrite(HWD_MBV_C2V_WRDY, HWD_VMB_WHS))

/* Indicate to the DSPV that the CP has finished reading the DSPV fast mailbox */
#define IPC_C2V_F_RRDY            (HwdWrite(HWD_MBV_C2V_FMB_INT, HWD_VMB_F_RHS))
#endif

/* Indicate to the DSPM that the CP has finished reading the DSPM fast mailbox */
#define IPC_C2M_F_RRDY            (HwdWrite(HWD_MBV_C2M_FMB_INT, HWD_VMB_F_RHS))

/* Macros which test the current DSP to CP mailbox status */

/* Test if the DSPM direct buffer mailbox can be written by the CP */
#define IPC_IS_M2C_D_WRDY         (HwdRead(HWD_CP_ISR0_L) & HWD_INT_M2CDW)

/* Test if the DSPM control mailbox can be written by the CP */
#define IPC_IS_M2C_C_WRDY         (HwdRead(HWD_CP_ISR0_L) & HWD_INT_M2CCW)

/* Test if the DSPM direct buffer mailbox contains data that should be read by the CP */
#define IPC_IS_M2C_D_RRDY         (HwdRead(HWD_CP_ISR0_L) & HWD_INT_M2CDR)

/* Test if the DSPM control mailbox contains data that should be read by the CP */
#define IPC_IS_M2C_C_RRDY         (HwdRead(HWD_CP_ISR0_L) & HWD_INT_M2CCR)

#ifndef MTK_PLT_AUDIO
/* Test if the DSPV mailbox can be written by the CP */
#define IPC_IS_V2C_WRDY           (HwdRead(HWD_CP_ISR0_L) & HWD_INT_V2CW)

/* Test if the DSPV mailbox should be read by the CP */
#define IPC_IS_V2C_RRDY           (HwdRead(HWD_CP_ISR0_L) & HWD_INT_V2CR)
#endif

/* Macros which clear the DSP to CP mailbox CP status bits */

/* Clear the CP bit indicating the CP may write to the DSPM direct buffer mailbox */
#define IPC_CLR_M2C_D_WRDY        (HwdIntClrL0(HWD_INT_M2CDW))

/* Clear the CP bit indicating the CP may write to the DSPM control mailbox */
#define IPC_CLR_M2C_C_WRDY        (HwdIntClrL0(HWD_INT_M2CCW))

/* Clear the CP bit indicating the CP must read the DSPM direct buffer mailbox */
#define IPC_CLR_M2C_D_RRDY        (HwdIntClrL0(HWD_INT_M2CDR))

/* Clear the CP bit indicating the CP must read the DSPM control mailbox */
#define IPC_CLR_M2C_C_RRDY        (HwdIntClrL0(HWD_INT_M2CCR))

#ifndef MTK_PLT_AUDIO
/* Clear the CP bit indicating the CP may write to the DSPV mailbox */
#define IPC_CLR_V2C_WRDY          (HwdIntClrL0(HWD_INT_V2CW))

/* Clear the CP bit indicating the CP may write to the DSPV mailbox */
#define IPC_CLR_V2C_RRDY          (HwdIntClrL0(HWD_INT_V2CR))
#endif

/*****************************************************************************
* $Log: ipcmbox.h $
* Revision 1.2  2004/03/25 11:45:58  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.3  2003/11/26 16:26:06  vxnguyen
* Added DSPM fast mailbox read-ready handshaking macro.
* Revision 1.2  2003/09/04 15:33:29  vxnguyen
* Updated the use of some HWD definitions according to the new hwddefs.h.
* Revision 1.1  2003/05/12 15:26:22  fpeng
* Initial revision
*
*
* Revision 1.7  2003/05/07 11:59:15  wfu
* . Fix CR 1917: Remove the DSPM/DSPV synchronous and Tst mailboxes.
*****************************************************************************/

#endif
