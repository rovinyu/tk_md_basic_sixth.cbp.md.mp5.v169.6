/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef  _HWDSLEEP_H_
#define  _HWDSLEEP_H_
/*****************************************************************************
 
	FILE NAME: hwdsleep.h

	DESCRIPTION:
		Interface to hwdsleep.c

*****************************************************************************/

#include "sysdefs.h"
#include "hwdapi.h"

/*------------------------------------------------------------------------
 * exported data
 *------------------------------------------------------------------------*/
#if (SYS_OPTION_32K_CLK_SOURCE != SYS_OPTION_32K_CLK_DIV_TCXO)
#define HWD_CLK_FREQ_32KHZ               (32768L)
#else
#define HWD_CLK_FREQ_32KHZ               (37500L)
#endif
#define HWD_CS_CLK32K_CNT_MAX_TICKS      ((uint32)1 << 24)
#define HWD_CS_CLK32K_CNT_MAX_CNT        (HWD_CS_CLK32K_CNT_MAX_TICKS - 1)
#define HWD_CS_CLK32K_CNT_TICKS_PER_10MS (10 * HWD_CLK_FREQ_32KHZ / 1000) /* number of sleep counts in a 10ms period
                                                                          * (10ms * Sleep Clk Freq in Hz)  */
#define HWD_CS_CLK32K_CNT_TICKS_PER_20MS (20 * HWD_CLK_FREQ_32KHZ / 1000) /* number of sleep counts in a 20ms period
                                                                          * (20ms * Sleep Clk Freq in Hz)  */
#define HWD_CS_CLK32K_CNT_TICKS_PER_80MS (80 * HWD_CLK_FREQ_32KHZ / 1000) /* number of sleep counts in a 80ms period
                                                                          * (80ms * Sleep Clk Freq in Hz)  */

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define HWD_SLP_CLK_CYCLE_PER_MS   33       /* approx number of sleep clock cycles per millisecond 
                                             * (32.768)
                                             */
#if (SYS_OPTION_32K_CLK_SOURCE != SYS_OPTION_32K_CLK_DIV_TCXO)
#define MICROSEC_PER_TICK 31    /* MICROSEC_PER_TICK = 1000000/HWD_CLK_FREQ_32KHZ */
#else
#define MICROSEC_PER_TICK 27 
#endif

#define MicroSecTo32KTicks(MicroSec) \
   ( ( ((MicroSec) + (MICROSEC_PER_TICK/2)) / MICROSEC_PER_TICK) + 1 )

#define HwdWriteSleepCounters(Interface, Count) \
   {  if (Interface == SYS_MODE_1xRTT) { \
         HwdWrite( HWD_CS_SLP_TIME_1X_HI, ((Count >> 16) & 0xffff) ); \
         HwdWrite( HWD_CS_SLP_TIME_1X_LO, (Count & 0xffff) ); } \
      else { \
         HwdWrite( HWD_CS_SLP_TIME_DO_HI, ((Count >> 16) & 0xffff) ); \
         HwdWrite( HWD_CS_SLP_TIME_DO_LO, (Count & 0xffff) ); } \
   }

#define HwdReadSleepCounters(Interface) \
   (Interface == SYS_MODE_1xRTT)? ((HwdRead(HWD_CS_SLP_TIME_1X_HI) << 16) | HwdRead(HWD_CS_SLP_TIME_1X_LO)) : \
                                  ((HwdRead(HWD_CS_SLP_TIME_DO_HI) << 16) | HwdRead(HWD_CS_SLP_TIME_DO_LO))

#define HwdWriteResyncCounters(Interface, Count) \
   {  if (Interface == SYS_MODE_1xRTT) { \
         HwdWrite( HWD_CS_RESYNC_CTL, HwdRead(HWD_CS_RESYNC_CTL) & (~HWD_RESYNC_CTL_1X_RESYNC_EN_MASK) ); \
         HwdWrite( HWD_CS_RESYNC_TIME_1X_HI, ((Count >> 16) & 0xffff) ); \
         HwdWrite( HWD_CS_RESYNC_TIME_1X_LO, (Count & 0xffff) ); } \
      else { \
         HwdWrite( HWD_CS_RESYNC_CTL, HwdRead(HWD_CS_RESYNC_CTL) & (~HWD_RESYNC_CTL_DO_RESYNC_EN_MASK) ); \
         HwdWrite( HWD_CS_RESYNC_TIME_DO_HI, ((Count >> 16) & 0xffff) ); \
         HwdWrite( HWD_CS_RESYNC_TIME_DO_LO, (Count & 0xffff) ); } \
   }

#define HwdReadResyncCounters(Interface) \
   (Interface == SYS_MODE_1xRTT)? ((HwdRead(HWD_CS_RESYNC_TIME_1X_HI) << 16) | HwdRead(HWD_CS_RESYNC_TIME_1X_LO)) : \
                                  ((HwdRead(HWD_CS_RESYNC_TIME_DO_HI) << 16) | HwdRead(HWD_CS_RESYNC_TIME_DO_LO))

#define  HwdSleepRead32KCnt() HWD_CLK32_READ_CNT()

/* Maximum allowable deep sleep duration, in seconds */
#define MON_DEEP_SLEEP_MAX_TIME_SECONDS  (HWD_CS_CLK32K_CNT_MAX_CNT/HWD_CLK_FREQ_32KHZ) 

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define MAX_9P8M_CNT                (0xbffff)
#define MicroSecToTo9P8MTicks(uS)   ((uS * 98) / 10 + 1)
#endif


typedef struct
{
   uint32    SlpOvrIntSrc;
   uint32    SlpOvrLisrBgn;
   uint32    SlpOvrLisrEnd;
   uint32    SlpOvrHisrBgn;
   uint32    SlpOvrHisrEnd;
   uint32    ResyncLisrBgn;
   uint32    ResyncLisrEnd;
   uint32    ResyncHisrBgn;
   uint32    ResyncHisrEnd;
   uint32    ResyncSlpCtl;
   uint32    ResyncSlpCnt;
#ifdef MTK_CBP
   /* Add tracing info for resync INT missing debug */
   uint32    Debug[32];
   uint32    SlpWakeRecord[16];
#endif
} HwdSleepDbgT;


#ifdef MTK_DEV_SHARED_DBG
typedef struct
{
   uint16   Isr;
   uint16   Mask;
} CpiInfoT;

typedef struct
{
   uint16   SlpCtrl;    /* HWD_CS_SLP_CTRL */
   uint16   ResyncCtrl; /* HWD_CS_RESYNC_CTL */
   uint16   SlpStatus;  /* HWD_CS_SLP_STATUS */
   uint16   RfPor;      /* HWD_RFPOR_VRF18_EN */
   uint32   WakeEn1x;   /* (HWD_CS_EXT_WAKE_EN2_1X << 16) | HWD_CS_EXT_WAKE_EN_1X */
   uint32   WakeEnDo;   /* (HWD_CS_EXT_WAKE_EN2_DO << 16) | HWD_CS_EXT_WAKE_EN_DO */
} CsInfoT;

#define MAX_SLEEP_WAKE_TIME_SAMPLE (120)
typedef struct
{
   uint32              WriteIdx;
   uint32              SleepWakeTime[MAX_SLEEP_WAKE_TIME_SAMPLE][2];
} SlpTimeT;

typedef struct
{
   CpiInfoT            CpiInfo[HWD_INT_REGISTER_MAX];
   CsInfoT             CsInfo;
   SlpTimeT            SlpTime;    
} SharedDbgSleepInfoT;

typedef enum
{
    SLEEP_DBG_CPI,
    SLEEP_DBG_CS,
    SLEEP_DBG_SLPTIME
}SleepDbgInfoTypeT;

#ifdef SYS_OPTION_PSO_ENABLED
typedef enum
{
    EVT_FLIGHT_MODE,
    EVT_SAVE_INT_MASK,
    EVT_INT_RESTORE_WAKEUP,
    EVT_INT_RESTORE_MD1,
    EVT_INT_RESTORE_AP,
    EVT_SLEEP,
    EVT_INT_STATE,
    EVT_WAKEUP,
    EVT_DBG_MAX
}SleepEvtT;

typedef struct
{    
    uint32 EvtTs[EVT_DBG_MAX];
    uint32 WakeUpSrc1x;
    uint32 WakeUpSrcDo;
    uint32 CpsrValue;
    uint16 IntMaskRecord[HWD_INT_REGISTER_MAX];
    uint16 IntCpiSt[HWD_INT_REGISTER_MAX * 2];
}SharedDbgPsoInfoT;
#endif

#endif
extern HwdSleepDbgT HwdSleepDbg;
extern uint32 HwdSleepOver32kHz[];
extern uint32 HwdSleep32kHzMatch[];  

/*------------------------------------------------------------------------
 * exported functions
 *------------------------------------------------------------------------*/

extern void     HwdSleepInit( void );
extern void     HwdSleepOverLisr(uint16 IntSrc);
extern void     HwdSleepVetoTimerFunction( uint32 TimerId );
extern void     HwdSleepWakeupEventRegister(SysAirInterfaceT Interface, uint32 WakeupEventsMask);
extern void     HwdSleepWakeupEventCancel(SysAirInterfaceT Interface, uint32 WakeupEventsMask);
extern bool     HwdSleepWakeupEventCheck(SysAirInterfaceT Interface, uint32 WakeupEventsMask);
extern void     HwdSleepWakeupAlwaysEventCancel(SysAirInterfaceT Interface, uint32 WakeupEventsMask);
extern void     HwdSleepWakeupAlwaysEventRegister(SysAirInterfaceT Interface, uint32 WakeupEventsMask);
extern bool     HwdDeepSleepPrepare( SysAirInterfaceT Interface );
extern void     HwdDeepSleepEnter( void ); 
extern void     HwdSleepWakeupEnable(SysAirInterfaceT Interface, bool Enable);
extern void     HwdDeepSleepActivate( SysAirInterfaceT Interface );
extern void     Hwd32kDelay(uint32 delay_cnt);
extern void     HwdMsDelay(uint16 ms);
extern uint32   Hwd32kGetTimeWithOld(uint32 oldCnt);
extern bool     HwdEnterCbpDeepSleepCheck( SysAirInterfaceT Interface ) ;
#ifndef BOOT_BUILDING 
extern void     HwdSleepWakeupCS( SysAirInterfaceT Interface ) ;
extern bool     HwdIsBlockConfigValid( HwdBlkControlBlocksT Block );
extern void     HwdSleepOver1xLisr(uint16 IntSrc); 
extern void     HwdSleepOverEvdoLisr(uint16 IntSrc); 
extern bool     HwdCheckDeepSleepMode( SysAirInterfaceT Interface);
extern bool     HwdCheckDeepSleepHWMode(SysAirInterfaceT Interface);
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern void HwdUsDelay(uint16 microSec);

#endif

#ifdef SYS_OPTION_PSO_ENABLED
extern void HwdSleepReadCpiReg( void );
extern void HwdSleepIntLRestore( uint8 Type );
extern bool HwdSleepEnterContinue( SysAirInterfaceT Interface );
#ifdef MTK_DEV_SHARED_DBG
extern void HwdSaveDbgPsoInfo(uint32 Evt, void* Datap, uint32 Size);
#endif
#endif

/*****************************************************************************
 * End of file
 *****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_ylxiao_href22207\1 2013-05-17 02:21:50 GMT ylxiao
** HREF#22207**/
/**Log information: \main\Trophy\1 2013-05-17 02:56:54 GMT hzhang
** HREF#22207 to remove compiling warning.**/
/**Log information: \main\Trophy\Trophy_czhang_handroid1957_fix1\1 2013-05-20 01:57:24 GMT czhang
** HANDROID#1957**/
/**Log information: \main\Trophy\2 2013-05-20 01:58:02 GMT czhang
** HANDROID#1957**/
