/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDERROR_H_
#define _HWDERROR_H_
/*****************************************************************************
* 
* FILE NAME   : hwderror.h
*
* DESCRIPTION : Error definitions for the hardware drivers
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/

/* Hardware Driver Errors */
typedef enum
   {
   HWD_ERR_TST_UNKNOWN_CMD,       /* 0x00 Test task received unknown command          */
   HWD_ERR_TST_UNKNOWN_EVENT,     /* 0x01 Test task received unknown event            */
   HWD_ERR_TST_UNKNOWN_SIGNAL,    /* 0x02 Test task received unknown signal           */

   HWD_ERR_TST_INVALID_PLL,       /* 0x03 Invalid PLL selection                       */
   HWD_ERR_TST_INVALID_DAC,       /* 0x04 Invalid DAC index                           */
   
   HWD_ERR_SER_LOAD_TIMEOUT,      /* 0x05 Serializer immediate load time out          */
   
   HWD_ERR_MS_PROG_TYPE,          /* 0x06 Invalid mixed signal device                 */
   
   HWD_ERR_AUX_IMMED_WRITE,       /* 0x07 Aux immediate interrupt ready timeout       */
   HWD_ERR_AUX_TIMED_WRITE,       /* 0x08 Aux timed interrupt ready timeout error     */
   HWD_ERR_PDM_TIMED_WRITE,       /* 0x09 PDM timed interrupt ready timeout error     */
   HWD_ERR_PDM_IMMED_WRITE,       /* 0x0A PDM immediate interrupt ready timeout error */

   HWD_ERR_CHAN_INVALID,          /* 0x0B Invalid Rx channel type                     */
   
   HWD_ERR_STARTUP_BAD_MSG,       /* 0x0C Invalid mail message at startup time        */
   HWD_ERR_STARTUP_BAD_SIGNAL,    /* 0x0D Invalid signal at startup time              */
   HWD_ERR_STARTUP_TIMEOUT,       /* 0x0E Timeout condition at startup time           */
   HWD_ERR_STARTUP_CAL_PARMS,     /* 0x0F not all cal parms were received             */
   HWD_ERR_STARTUP_SIGNAL,        /* 0x10 Startup signal not received                 */
   
   HWD_ERR_FLASH_ERASE,           /* 0x11 Flash memory erase error                    */
   HWD_ERR_FLASH_PROGRAM,         /* 0x12 Flash memory program error                  */
   HWD_ERR_FLASH_SECTOR_PROTECT,  /* 0x13 Flash sector is protected                   */
   
   HWD_ERR_GPIO_ILLEGAL,          /* 0x14 Illegal GPIO selected error                 */
   
   HWD_ERR_REQUEST_ILLEGAL,       /* 0x15 Illegal request...                          */

   HWD_ERR_CALIB_PARM_INVALID,    /* 0x16 calibration parameter exceeds limits        */
   
   HWD_ERR_ADC_TIMEOUT,           /* 0x17 ADC measurement conversion timeout          */

   HWD_ERR_GPIO_CONFIG_COLLISION, /* 0x18 GPIO configuration collision                */

   HWD_ERR_FILT_BW_CAL,           /* 0x19 Invalid signal while calibrating Filter BW  */
   HWD_ERR_IQ_GAIN_CAL,           /* 0x1A Invalid signal while calibrating IQ gain imbalance */
   HWD_ERR_TX_DAC_OFST_CAL,       /* 0x1B Tx DAC DC offset calibration failure        */
   HWD_ERR_RX_ADC_OFST_CAL,       /* 0x1C Rx ADC DC offset calibration failure        */
   HWD_KEY_SCAN_ERR,              /* 0x1D Key Scan error                              */
   HWD_ERR_PLL_LOCK_LOST,         /* 0x1E RF PLL Lock Lost                            */

   HWD_ERR_DSPV_IS_CLOCKED_OFF,   /* 0x1F Dspv is clocked off                         */
   HWD_ERR_EDAI_ALREADY_ENABLED,  /* 0x20 EDAI mode is already enabled                */
   HWD_ERR_EDAI_ALREADY_DISABLED, /* 0x21 EDAI mode is already disabled               */
   HWD_ERR_READ_TEMPERATURE,      /* 0x22 Read wrong temperature from Aux Adc         */
   HWD_ERR_READ_BATTERY_VOLTAGE,  /* 0x23 Read wrong battery voltage from Aux Adc     */

   HWD_ERR_RFFUNCPTR_NULL,        /* 0x24 RF Function Pointer Table Has Null Entry    */

   HWD_ERR_TUNECNT_READ_FAILURE,  /* 0x25 Sigma-Delta Tune Count read failure         */ 
   HWD_ERR_TX_DC_OFST_CAL_IDIFF_FAILURE, /* 0x26 Tx DC offset cal I-channel failure   */
   HWD_ERR_TX_DC_OFST_CAL_QDIFF_FAILURE,  /* 0x27 Tx DC offset cal Q-channel failure   */
   
   HWD_ERR_INVALID_UART_BAUD_RATE,/* 0x28 Invalid Uart Baud Rate detected             */ 
   HWD_ERR_LCD_DRIVER_FAILURE,    /* 0x29 Lcd driver error                            */
   HWD_ERR_CMR_DRIVER_FAILURE,    /* 0x2A Camera driver error                         */
   HWD_ERR_UNUSED,                /* 0x2B                                             */
   HWD_ERR_DAC_DRIVER_FAILURE,    /* 0x2C External Audio DAC Driver error             */
   HWD_ERR_NFC_DRIVER_FAILURE,    /* 0x2D NFC Driver error                            */
   HWD_ERR_MMC_INIT,              /* 0x2E Failed to init the card                     */
   HWD_ERR_MMC_INVALID_STATE,     /* 0x2F Invalid state                               */
   HWD_ERR_MMC_CARD_NOT_PRESENT,  /* 0x30 Card is not present                         */
   HWD_ERR_MMC_SET_BLK_COUNT,     /* 0x31 Failed to set block count                   */
   HWD_ERR_MMC_STOP_TRAN,         /* 0x32 Failed to stop the transmission             */
   HWD_ERR_MMC_READ,              /* 0x33 Failed to read from the card                */
   HWD_ERR_MMC_WRITE,             /* 0x34 Failed to write to the card                 */
   HWD_ERR_MMC_WRITE_PROTECTED,   /* 0x35 Card is write protected                     */
   HWD_ERR_MMC_SET_START_ADDR,    /* 0x36 Failed to set erase start address           */
   HWD_ERR_MMC_SET_END_ADDR,      /* 0x37 Failed to set erase end address             */
   HWD_ERR_MMC_ERASE,             /* 0x38 Failed to start erase operation             */
   HWD_ERR_MMC_CARD_STATUS,       /* 0x39 Failed to get card status                   */
   HWD_ERR_MMC_INVALID_BUS_MODE,  /* 0x3A Invalid bus mode                            */
   HWD_ERR_MMC_SET_BUS_MODE,      /* 0x3B Failed to set bus mode                      */
   HWD_ERR_MMC_SET_BLK_LEN,       /* 0x3C Failed to set block length                  */
   HWD_ERR_MMC_INACT_CARD,        /* 0x3D Failed to inactivate the card               */
   HWD_ERR_USB_CFGCORE_ERR,       /* 0x3E core gonfig did not complete */
   HWD_ERR_AUDIO_MODE_UNSUPPORTED,/* 0x3F Audio mode requested is not supported */
   HWD_ERR_GAIN_FIQ_CONTENTION,   /* 0x40 Gain State FIQ Resource contention error    */
   HWD_ERR_MMC_CARD_STATUS_ERR,   /* 0x41 Card Status reported error                  */
   HWD_ERR_RF_TABLE_FORMAT_ERR,    /* 0x42 RF mapping assignments format error         */        
   HWD_ERR_RF_GPS_SER_LOAD_FAIL,  /* 0x43 Serial load failed for Multi-media device   */

   /* MIDI ringer errors */
   HWD_ERR_MIDI_FILE_EMPTY,                     /* 0x44 */
   HWD_ERR_MIDI_NOT_INITIALIZED,                /* 0x45 */
   HWD_ERR_MIDI_BUFF_EMPTY_SIG_WHILE_IDLE,      /* 0x46 */
   HWD_ERR_MIDI_DRUM_NOTE_INVALID,              /* 0x47 */
   HWD_ERR_MIDI_PATCH_IDX_INVALID,              /* 0x48 */
   HWD_ERR_MIDI_FILE_IDX_INVALID,               /* 0x49 */
   HWD_ERR_MIDI_CMD_INV_WHILE_RNGR_ACTIVE,      /* 0x4A */
   HWD_ERR_MIDI_CMD_INV_IN_CURR_STATE,          /* 0x4B */
   HWD_ERR_MIDI_FILE_OPEN_ERR,                  /* 0x4C */
   HWD_ERR_MIDI_ENABLE_INV_IN_CURR_STATE,       /* 0x4D */
   HWD_ERR_MIDI_DSPV_INIT_FAILED,               /* 0x4E */
   HWD_ERR_MIDI_TONE_FREQ_INVALID,              /* 0x4F */
   HWD_ERR_MIDI_BANK_SELECT_NOT_SUPPORTED,      /* 0x50 */
   HWD_ERR_MIDI_LOADING_NON_EMPTY_BUFF,         /* 0x51 */
   HWD_ERR_MIDI_EVENT_DATA_TOO_LARGE,           /* 0x52 */

   /* Multimedia Applications errors */
   HWD_ERR_MMAPPS_INVAL_MODE_RSP_IN_CURR_STATE, /* 0x53 */ 
   HWD_ERR_MMAPPS_INVAL_MUSIC_PARAMS,           /* 0x54 */
   HWD_ERR_MMAPPS_INVAL_MUSIC_TYPE,             /* 0x55 */
   HWD_ERR_MMAPPS_INVAL_TEST_FILE_SIZE,         /* 0x56 */
   HWD_ERR_MMAPPS_READ_REQ_WHILE_NOT_ACTIVE,    /* 0x57 */
   HWD_ERR_MMAPPS_WRITE_REQ_WHILE_NOT_ACTIVE,   /* 0x58 */
   HWD_ERR_MMAPPS_FILE_INFO_REQ_WHILE_IDLE,     /* 0x59 */
   HWD_ERR_MMAPPS_MP3_STATUS_WHILE_IDLE,        /* 0x5A */
   HWD_ERR_MMAPPS_WRONG_STATE_FOR_FAST_FWD,     /* 0x5B */
   HWD_ERR_MMAPPS_WRONG_STATE_FOR_REWIND,       /* 0x5C */
   HWD_ERR_MMAPPS_WRONG_STATE_FOR_SUSPEND,      /* 0x5D */
   HWD_ERR_MMAPPS_WRONG_STATE_FOR_RESUME,       /* 0x5E */
   HWD_ERR_MMAPPS_INVAL_APP_CHAN_MSG_ID,        /* 0x5F */
   HWD_ERR_MMAPPS_INVAL_WRITE_REQ_APP_ID,       /* 0x60 */
   HWD_ERR_MMAPPS_APP_ID_ALREADY_ACTIVE,        /* 0x61 */
   HWD_ERR_MMAPPS_MUSIC_APP_ALREADY_ACTIVE,     /* 0x62 */
   HWD_ERR_MMAPPS_MAX_APPS_ALREADY_ACTIVE,      /* 0x63 */
   HWD_ERR_MMAPPS_INVALID_INPUT_FILE_ID,        /* 0x64 */
   HWD_ERR_MMAPPS_INVALID_INPUT_APP_ID,         /* 0x65 */
   HWD_ERR_MMAPPS_FILE_ID_APP_ID_MISMATCH,      /* 0x66 */
   HWD_ERR_MMAPPS_INVALID_NUM_APP_MSGS,         /* 0x67 */
   HWD_ERR_MMAPPS_INVALID_FAST_FWD_PARMS,       /* 0x68 */
   HWD_ERR_MMAPPS_INVALID_REWIND_PARMS,         /* 0x69 */
   HWD_ERR_MMAPPS_CANT_FASTFWD_WITHOUT_STATUS,  /* 0x6A */
   HWD_ERR_MMAPPS_CANT_REWIND_WITHOUT_STATUS,   /* 0x6B */
   HWD_ERR_MMAPPS_UNEXPECT_POS_ADJUST_TIMER,    /* 0x6C */
   HWD_ERR_MMAPPS_FILE_SEEK_FAIL,               /* 0x6D */
   HWD_ERR_MMAPPS_FILE_READ_FAIL,               /* 0x6E */
   HWD_ERR_MMAPPS_FILE_WRITE_FAIL,              /* 0x6F */
   HWD_ERR_MMAPPS_MODE_START_RESP_ERR,          /* 0x70 */
   HWD_ERR_MMAPPS_MODE_STOP_RESP_ERR,           /* 0x71 */

   HWD_ERR_MMAPPS_TX_BUFFER_OVERFLOW = 0x79,    /* 0x79 */
   HWD_ERR_MMAPPS_MISALIGNED_CMD_ARGS_STRUCT,   /* 0x7A */
   HWD_ERR_MMAPPS_INVALID_SRVC_OPT,             /* 0x7B */
   HWD_ERR_MMAPPS_VOCODER_DNLD_REQ_DURING_VOICE,/* 0x7C */
   HWD_ERR_MMAPPS_APP_START_TIMEOUT,            /* 0x7D */
   HWD_ERR_MMAPPS_CANT_DO_APP_DURING_DOWNLOAD,  /* 0x7E */
   HWD_ERR_MMAPPS_TOO_MANY_SIDETONE_CACHE_REQ,  /* 0x7F */

   /* External Micronas MP3 errors */
   HWD_ERR_EXT_MP3_INVALID_FILE_SIZE = 0x80,    /* 0x80 */
   HWD_ERR_EXT_MP3_INVALID_PLAY_PARAMS,         /* 0x81 */
   HWD_ERR_EXT_MP3_PLAY_REQ_WHILE_NOT_IDLE,     /* 0x82 */
   HWD_ERR_EXT_MP3_AUDIO_CLEANUP_IN_WRONG_STATE,/* 0x83 */
   HWD_ERR_EXT_MP3_FILE_SEEK_ERR,               /* 0x84 */
   HWD_ERR_EXT_MP3_FILE_READ_ERR,               /* 0x85 */
   HWD_ERR_EXT_MP3_I2C_WRITE_ERR,               /* 0x86 */
   HWD_ERR_EXT_MP3_I2C_READ_ERR,                /* 0x87 */
   HWD_ERR_EXT_MP3_APPL_NOT_RUNNING,            /* 0x88 */
   HWD_ERR_EXT_MP3_CTRL_MEM_INIT_FAIL,          /* 0x89 */
   HWD_ERR_MMAPPS_AAC_STATUS_WHILE_IDLE,        /* 0x8A */
   HWD_ERR_MMAPPS_INVALID_DSPV_APP_REQ,         /* 0x8B */
   HWD_ERR_MMAPPS_BS_COP_PKT_MISMATCH,          /* 0x8C */

   HWD_ERR_I2C_FAILURE = 0x90,                  /* 0x90 */
   HWD_ERR_AUTO_BATTERY_CALIBR_FAILURE,         /* 0x91 */
   HWD_ERR_PSAVING_MODE_CFG_INVALID,            /* 0x92 */
   HWD_ERR_AUDIO_SET_MODE_SEM_TIMEOUT_ERR,      /* 0x93 */
   HWD_ERR_AUDIO_SET_VOL_SEM_TIMEOUT_ERR,       /* 0x94 */
   HWD_ERR_AUDIO_SET_PATH_SEM_TIMEOUT_ERR,      /* 0x95 */

   HWD_ERR_IMELODY_EVENT_INVALID,               /* 0x96 */
   HWD_ERR_GEN_DMA_STATUS_ERROR,                /* 0x97 */
   HWD_ERR_DMA_REG_WRITE,                       /* 0x98 */
   HWD_ERR_PSAVING_SWITCHING_INVALID,           /* 0x99 */
   HWD_ERR_DIVERSITY_RF_UNSUPPORTED,            /* 0x9A */
 
   HWD_ERR_SIDETONE_NOT_SENT_TO_DSPV,           /* 0x9B */
   HWD_ERR_MP3_FRAME_SYNC_NOT_FOUND,            /* 0x9C */
   HWD_ERR_MIDI_INVALID_PITCH_RANGE,            /* 0x9D */
   HWD_ERR_AUDIO_TONE_SEM_TIMEOUT_ERR,          /* 0x9E */
   HWD_ERR_MIDI_MAX_TRACKS_EXCEEDED,            /* 0x9F */

   /* Tone errors */
   HWD_ERR_TONE_INVALID_TONE_ID = 0xA0,         /* 0xA0 */
   HWD_ERR_TONE_VOCODER_APP_NOT_DOWNLOADED,     /* 0xA1 */

   /* CMF errors */
   HWD_ERR_CMF_UNEXPECTED_SUBCHUNK_TYPE,        /* 0xA2 */
   HWD_ERR_CMF_TOO_MANY_JUMP_POINTS,            /* 0xA3 */
   HWD_ERR_CMF_JUMP_CMD_WITH_ZERO_JUMPS,        /* 0xA4 */
   HWD_ERR_CMF_NO_EMPTY_NOTEOFF_TBL_ENTRIES,    /* 0xA5 */
   HWD_ERR_CMF_PITCH_BEND_OVERFLOW,             /* 0xA6 */
   HWD_ERR_CMF_DLS_FEATURE_NOT_SUPPORTED,       /* 0xA7 */
   /* Misc errors added to end */
   HWD_ERR_STARTUP_NO_RF_DB_FILE,               /* 0xA8 */

   HWD_ERR_STARTUP_CDMA1_CAL_PARMS,             /* 0xA9 not all cdma1 cal parms were received */
   HWD_ERR_STARTUP_CDMA2_CAL_PARMS,             /* 0xAA not all cdma2 cal parms were received */
   HWD_ERR_STARTUP_CDMA3_CAL_PARMS,             /* 0xAB not all cdma3 cal parms were received */
   HWD_ERR_POWER_DETECT_MEAS_FAIL,              /* 0xAC we got wrong power detect measurement  */
   HWD_ERR_RFBAND_NOT_SUPPORTED,                /* 0xAD Requested RF Band is not supported */
   HWD_ERR_STARTUP_CDMA4_CAL_PARMS,             /* 0xAE not all cdma4 cal parms were received */

   HWD_ERR_SDIO_UNIT,                           /* 0xAF */

   HWD_ERR_INVALID_DLY_CTRL_UNIT,               /* 0xB0 */
   HWD_ERR_RF_ID_MISMATCH,                      /* 0xB1 */
   HWD_ERR_MMAPPS_EXCEEDED_MAX_RESIDUAL_SIZE,   /* 0xB2 */
   HWD_ERR_MMAPPS_MUSIC_MALLOC_FAIL,            /* 0xB3 */

   HWD_ERR_MXS_SPI_WRITE_ERROR,                 /* 0xB4 */
   HWD_ERR_MXS_SPI_READ_ERROR,                  /* 0xB5 */
   HWD_ERR_MXS_SPI_QFULL_ERROR,                 /* 0xB6 */
   HWD_ERR_MIDI_FILE_READ_FAIL,                 /* 0xB7 */
   HWD_ERR_MIDI_TRACK_BUF_NOT_READY,            /* 0xB8 */
   HWD_ERR_MIDI_TOO_MANY_TEMPO_EVENTS,          /* 0xB9 */

   HWD_ERR_MSG_ID_MISMATCH,                     /* 0xBA means that there is mismatch between the some libraries msg id numbers */
   HWD_ERR_PWRSAVE_SEM_TIMEOUT_ERR,             /* 0xBB */

   HWD_ERR_PMU_POLLING_FAILURE_ERR,             /* 0xBC */
   HWD_ERR_I2C_INTERRUPT_TIMEOUT,               /* 0xBD */
   HWD_ERR_I2C_OPERATION,                       /* 0xBE */
   HWD_ERR_I2C_LOCK_SEM_ERR,                    /* 0xBF */
   
   HWD_ERR_AUDIO_TUNING_MALLOC_FAILED = 0xC0,   /* 0xC0 */
   HWD_ERR_AUDIO_TUNING_PARAMS_NOT_LOADED,      /* 0xC1 */
   HWD_ERR_AUDIO_TUNING_TYPE_INVALID,           /* 0xC2 */
   HWD_ERR_AUDIO_TUNING_FILE_GET_LEN_FAIL,      /* 0xC3 */
   HWD_ERR_AUDIO_TUNING_FILE_OPEN_FAIL,         /* 0xC4 */
   HWD_ERR_AUDIO_TUNING_FILE_READ_FAIL,         /* 0xC5 */
   HWD_ERR_AUDIO_TUNING_FILE_WRITE_FAIL,        /* 0xC6 */
   HWD_ERR_AUDIO_TUNING_SIZE_INVALID,           /* 0xC7 */
   HWD_ERR_AUDIO_TUNING_FILE_FORMAT_INVALID,    /* 0xC8 */
   HWD_ERR_AUDIO_TUNING_FILE_LEN_MISMATCH,      /* 0xC9 */
   HWD_ERR_AUDIO_TUNING_MODE_INVALID,           /* 0xCA */

   HWD_ERR_MIDI_DISABLE_NOT_RCVD,               /* 0xCB */
   HWD_ERR_RF_PIN_TYPE_INVALID,                 /* 0xCC */
   HWD_ERR_BAND_SUPPORTED_REQUEST_FAILED,       /* 0xCD Requested RF Band supported data failed */
   HWD_ERR_BAND_SUPPORTED_INVALID,              /* 0xCE */
   HWD_ERR_MUXSEL0_CONFLICT_ERR,                /* 0xCF */

   HWD_ERR_PLL_LOCK_FAILURE = 0xD0,             /* 0xD0 */
   HWD_ERR_VECTOR_DEMUX_DUP_ENTRY_ERR,          /* 0xD1 */
   HWD_ERR_CP_PLL_LOCK_FAILURE = 0xD2,          /* 0xD2 */
   HWD_ERR_ESPI_TX_TIMEOUT,                     /* 0xD3 */
   HWD_ERR_ESPI_ETS_FAIL,                       /* 0xD4 */
   HWD_ERR_ESPI_DATA_ALIGN_ERROR,               /* 0xD5 */
   HWD_ERR_ESPI_HEADER_ERROR,                   /* 0xD6 */
   HWD_ERR_ALLOC_FAIL,                          /* 0xD7 */
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   HWD_ERR_TST_UNSUPPORTED_CMD,                 /* 0xD8 Test task received unsupport command*/
   HWD_ERR_TEMP_CMD,                            /* 0xD9 Temperature meas err command*/
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   /* All Delay Loader Failures */
   HWD_ERR_DLYLDR_FAIL,                         /* 0xDA */
   /* All SPI Failures */
   HWD_ERR_SPI_FAIL,                            /* 0xDB */
   /* Assert checks in RF Driver */
   HWD_ERR_RF_FAIL,                             /* 0xDC */
   /* MixedSys failures */
   HWD_ERR_MS_FAIL,                             /* 0xDD */
#endif
#if defined (MTK_DEV_OPTIMIZE_EVL1)
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   /* the delay loader count to configure EVDO PA is too samll */
   HWD_ERR_RF_PA_DELAY_ERROR,                   /* 0xDE */
#endif
#endif
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
  /* PMIC INTERFACE WRITE NOT Finished ERR */
   HWD_ERR_PMICCTRL_WRITE_NOTFINISHED,			  /* 0xDF */
  /* PMIC CTRL INTERFACE DELAY PATH ERR */
   HWD_ERR_PMICCTRL_DELAYLDPATH_ERROR,	 	    /* 0xE0 */
  /* Assert PMICCTRL_IMME  */
   HWD_ERR_PMICCTRL_CPUIMMEDPATH_ERROR,				  /* 0xE1 */
  /* Assert PMICCTRL_IMME  */
   HWD_ERR_PMICCTRL_FIFOIMMEDPATH_ERROR,				/* 0xE2 */
  /* PMICWRAPPER_READ_ACKTIMEOUT  */
   HWD_ERR_PMICWRAPPER_READ_ACKTIMEOUT,		  	/* 0xE3 */
  /* PMIC_ENABLE_LDO_ERR  */
   HWD_ERR_PMIC_ENABLE_LDO_ERR,		  	        /* 0xE4 */
  /* PMIC_SETVOLTAGE_LDO_ERR  */
   HWD_ERR_PMIC_SETVOLTAGE_LDO_ERR,  	        /* 0xE5 */
  /* PMIC_LDOTYPE_LDO_ERR  */
   HWD_ERR_PMIC_LDOTYPE_LDO_ERR,  	          /* 0xE6 */

#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   HWD_ERR_TST_RX_PATH           = 0xE7,       /* 0xE7 */
   HWD_ERR_DC_IIR_COEF           = 0xE8,       /* 0xE8 - the DC IIR coefficient is 0 */
   HWD_ERR_RF_CONFLICT           = 0xE9,
   HWD_ERR_HWD_RF_CUST_PARAM_ERR = 0xF0,       /* 0xF0 - RF custom file parameter is wrong */
   HWD_ERR_DBM_DATA_ERR          = 0xF1,
#if (SYS_BOARD >= SB_JADE)
   HWD_ERR_MIPI                  = 0xF2,
   HWD_ERR_BSI                   = 0xF3,
   HWD_ERR_BPI                   = 0xF4,
   HWD_ERR_HWSIM                 = 0xF5,
#endif
#endif
#if ((!defined MTK_PLT_DENALI)) && (!defined(MTK_PLT_ON_PC))
   HWD_ERR_POC                   = 0xF6,
#endif
   HWD_ERR_CRYSTAL_TYPE          = 0xF7,
   HWD_ERR_DRDI                  = 0xF8,
   HWD_ERR_CC                    = 0xF9,  
   HWD_ERR_HWD_WATCHDOG_RESET    = 0xFF   ,           /* 0xFF - watchdog simlulation time-out */
   HWD_ERR_PLL_BAND                  = 0x100          ,
   HWD_ERR_PLL_MAIN_MODE                              ,
   HWD_ERR_PLL_MAIN_CURRENTRFSTATUS                   ,
   HWD_ERR_PLL_DIV_MODE                               ,
   HWD_ERR_PLL_DIV_CURRENTRFSTATUS                    ,
   HWD_ERR_PLL_RF_PTR_BAND                            ,
   HWD_ERR_PLL_RF_PTR_FREQ                            ,
   HWD_ERR_PLL_RF_PTR_FOE                             ,
   HWD_ERR_PLL_RF_PTR_FOE_PPB                         ,
   HWD_ERR_PLL_RXChan2Freq_CHAN                       ,
   HWD_ERR_PLL_RXChan2Freq_BAND                       ,
   HWD_ERR_PLL_TXChan2Freq_CHAN                       ,
   HWD_ERR_PLL_TXChan2Freq_BAND                       ,
   HWD_ERR_PLL_GETSRX_RFPATH                          ,
   HWD_ERR_PLL_GETSRX_BAND                            ,
   HWD_ERR_PLL_GETSTX_BAND                            ,
   HWD_ERR_PLL_GETSXAFC_CW_RFPATH                     ,
   HWD_ERR_PLL_GETSXAFC_CW_BAND                       ,
   HWD_ERR_PLL_RFCONNECTIONMATRIX                     ,
   HWD_ERR_PLL_BUILD_SRXAFC_SCRIPT                    ,
   HWD_ERR_BPI_WIN_ACT                                ,
   HWD_ERR_BPI_WIN_BAND                               ,
   HWD_ERR_BPI_GATE_ACT                               ,
   HWD_ERR_BPI_GATE_BAND                              ,
   HWD_ERR_BPI_GATE_IMM_ACT                           ,
   HWD_ERR_BPI_GATE_IMM_BAND                          ,
   HWD_ERR_BPI_RX_MAIN_DATA                           ,
   HWD_ERR_BPI_RX_DIV_DATA                            ,
   HWD_ERR_BPI_TX_DATA                                ,
   HWD_ERR_BPI_TX_DATA_IMM                            ,
   HWD_ERR_BPI_INIT_CUST_DATACHECK                    ,
   HWD_ERR_TAS_BPISET_ANTIDX                          ,
   HWD_ERR_TAS_BPIINIT_CUST_DATACHECK                 ,
   HWD_ERR_TAS_SWITCH_ANTTXON_CHECK                   ,
   HWD_ERR_TAS_SWITCH_ANTTXON                         ,
   HWD_ERR_TAS_SWITCH_ANTTXOFF_CEHCK                  ,
   HWD_ERR_TAS_SWITCH_ANTTXOFF                        ,
   HWD_ERR_TAS_GET_ANTIDX_CHECK                       ,
   HWD_ERR_TAS_GET_ANTIDX                             ,
   HWD_ERR_TAS_SWITCH_ANT_CHECK                       ,
   HWD_ERR_TAS_SWITCH_ANT                             ,
   HWD_ERR_RF_GETMAX_TXAGC_EVTTIME                    ,
   HWD_ERR_RF_CFG_INIT_DATA_CHECK                     ,
   HWD_ERR_RF_POR_SAVE_POR_PARAM_SIZE_INVALID         ,
   HWD_ERR_RF_TXON_POC_CW                             ,
   HWD_ERR_RF_TXOFF_POC_CW                            ,
   HWD_ERR_RF_TXPOC_CW_BAND                           ,
   HWD_ERR_RF_RXPOC_CW_BAND                           ,
   HWD_ERR_RF_CURRENTRFSTATUS                         ,
   HWD_ERR_RF_PWR_SAVE_RFPATH                         ,
   HWD_ERR_RXTX_RECEIVEONOFF                            ,
   HWD_ERR_RX_DCOC_RXPATH                             ,
   HWD_ERR_RX_MPA2DEV_CURRXFSM                        ,
   HWD_ERR_RX_MPA2DEV_RFPATH                          ,
   HWD_ERR_RX_MPA2RX_MPAPATH                          ,
   HWD_ERR_RX_DEV2RX_DEVPATH                          ,
   HWD_ERR_RX_RX2DEV_RXPATH                           ,
   HWD_ERR_RX_GETRX_REFINFO_RXPATH                    ,
   HWD_ERR_RX_GAIN_INDEX                              ,
   HWD_ERR_RX_GETAGC_COMPENSATE                       ,
   HWD_ERR_RX_GET_RXWARMUP_CW_RFPATH                  ,
   HWD_ERR_RX_GET_RXWARMUP_CW_BAND                    ,
   HWD_ERR_RX_GET_RXBURST_RFPATH                      ,
   HWD_ERR_RX_GET_RXGAIN_DEVPATH                      ,
   HWD_ERR_RX_GET_RXPMODE_H_RFPATH                    ,
   HWD_ERR_RX_GET_RXPMODE_L_RFPATH                    ,
   HWD_ERR_RX_BUILD_RXWARMUP_SCRIPT_RFPATH            ,
   HWD_ERR_RX_BUILD_RXBURST_SCRIPT_RFPATH             ,
   HWD_ERR_RX_BUILD_RXAGC_SCRIPT_RFPATH               ,
   HWD_ERR_RX_BUILD_RXSLEEP_SCRIPT_RFPATH             ,
   HWD_ERR_RX_BUILD_RXPMODE_SCRIPT_RFPATH             ,
   HWD_ERR_RX_UPDATE_DCOCTABLE_RXPWRMODE              ,
   HWD_ERR_RX_UPDATE_DCOCTABLE_RXPATH                 ,
   HWD_ERR_RX_GET_DCOCTABLE_RXPATH                    ,
   HWD_ERR_RX_GET_LPM_DCOCTABLE_RXPATH                ,
   HWD_ERR_RX_GET_DCOCTABLESIZE_RXPATH                ,
   HWD_ERR_TX_GET_TXBAND                              ,
   HWD_ERR_TX_OFF_PLLBAND                             ,
   HWD_ERR_TX_OFF_BAND                                ,
   HWD_ERR_TX_RFSET_PA_MODE_HSLOTBOUNDARY             ,
   HWD_ERR_TX_GET_TXGAINCFG_DELTAGAIN                 ,
   HWD_ERR_TX_ON_BAND                                 ,
   HWD_ERR_TX_ONSET                                   ,
   HWD_ERR_TX_ONCLEAR                                 ,
   HWD_ERR_TX_GET_WARMUP_CW_BAND                      ,                            
   HWD_ERR_RF_PTR                                     ,
   HWD_ERR_ETM
} HwdErrorT;


/* Code2 values for HWD_ERR_SIDETONE_NOT_SENT_TO_DSPV error Code1 */
enum
{
    HWD_MANUAL_VOL_CTRL_SIDETONE_ERR,
    HWD_NOT_IN_VOICE_SIDETONE_ERR,
    HWD_VOICE_MUTED_SIDETONE_ERR,
    HWD_MAINRF_ERR                      ,
    HWD_DIVRF_ERR                       ,
    HWD_TX_ERR                          ,
    HWD_FORCETXANTENABLE_ERR            ,
    HWD_TASENABLE_ERR                   ,
    HWD_RFCUSTOMDATA_ERR                ,
    HWD_GETSCENARIOCW_ERR               ,
    HWD_GETSERCONF_ERR                  ,
    HWD_RFSAVEPORPARAM_ERR              ,
    HWD_GETFINALPOCRESULT_ERR           ,
    HWD_SETORIGPOCRESULT_ERR            , 
    HWD_GETFINALPOCRESULTSIZE_ERR       ,
    HWD_ALREADY_RXON_1X_ERR             ,
    HWD_ALREADY_RXON_DO_ERR             ,
    HWD_ALREADY_RXON_DIV_1X_ERR         ,
    HWD_ALREADY_RXON_DIV_DO_ERR         ,
    HWD_ALREADY_TXON_1X_ERR             ,
    HWD_ALREADY_TXON_DO_ERR             ,
    HWD_RFGETRXGAINPARAM_ERR            ,
    HWD_GETANTCFG_ERR                   ,
    HWD_PATABLEPTR_ERR                  ,
    HWD_PATHRESHTBLPTR_ERR              
                
};

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\4 2012-03-16 02:10:03 GMT gliu
** update per SD feedback**/
/**Log information: \main\Trophy_SO73\1 2013-07-09 02:12:27 GMT jtzhang
** scbp#11737**/
/**Log information: \main\Trophy\1 2013-07-17 08:17:51 GMT jtzhang
** scbp#11737**/
/**Log information: \main\Trophy\Trophy_yanliu_href22332\1 2013-12-12 09:26:09 GMT yanliu
** HREF#22332: CP PLL unlock and DSPM halt fix**/
/**Log information: \main\Trophy\2 2013-12-12 09:35:40 GMT yanliu
** HREF#22332 merged**/
/**Log information: \main\Trophy\Trophy_fwu_href22348\1 2014-01-08 06:17:09 GMT fwu
** HREF#22348. Merge ESPI related source code.**/
/**Log information: \main\Trophy\3 2014-01-09 06:34:32 GMT zlin
** HREF#22348, merge code.**/
