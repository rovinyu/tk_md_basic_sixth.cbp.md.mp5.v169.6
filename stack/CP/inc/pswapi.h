/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _PSWAPI_H_
#define _PSWAPI_H_
/*****************************************************************************
*
* FILE NAME   : pswapi.h
*
* DESCRIPTION : Protocol stack interface
*
*     This include file provides system wide global type declarations and
*     constants
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/

#include "sysdefs.h"
#include "pswnam.h"
#include "exeapi.h"
#include "pswcustom.h"
#include "pswsmstune.h"
#include "hlpapi.h"
#include "lmdapi.h"
#include "gpsctapi.h"
#include "hwdapi.h"
#include "syscommon.h"
#include "cssapi.h"
#include "dbmapi.h"
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
#include "iratapi.h"
#endif
#ifdef MTK_PLT_ON_PC_UT
#include "Sbpapi.h"
#endif
/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define PSW_STARTUP_SIGNAL   EXE_SIGNAL_1
#define PSW_STARTUP_SIGNAL2  EXE_SIGNAL_2

#define PSW_MAILBOX_CMD      EXE_MAILBOX_1_ID
#define PSW_MAILBOX2_CMD     EXE_MAILBOX_2_ID

#define PSW_SYNC_DATA_SIZE   4

#define PSW_MAX_SMS_SIZE     256
#define PRM_MAX_SIZE         256

#define PSW_NUM_MUX1_COUNTER_INDEX 14
#define PSW_NUM_MUX2_COUNTER_INDEX 26

/* Data service page acceptance bit map. */
#define PSW_ASYNC_DATA_PAGE_ACCEPT   1
#define PSW_FAX_PAGE_ACCEPT          2
#define PSW_ASYNCDATA_AND_FAX_PAGE_ACCEPT	3

/* Updated NAM Data Mask Bits Defined (whatChangedInNam) */
#define SLOT_CYCLE_INDEX_UPDATED     0x0001
#define SCM_UPDATED                  0x0002
#define MOB_TERM_HOME_UPDATED        0x0004
#define MOB_TERM_FOR_SID_UPDATED     0x0008
#define MOB_TERM_FOR_NID_UPDATED     0x0010
#define AKEY_UPDATED                 0x0040
#define SYSTEMSELECT_UPDATED         0x0080

#define PSW_AKEY_UINT64

/* IMSI Character String - Input sizes */
#define MAX_IMSI_S1_DIGITS 7
#define MAX_IMSI_S2_DIGITS 3
#define MAX_IMSI_S_DIGITS 10
#define MAX_MNC_DIGITS 2
#define MAX_MCC_DIGITS 3
#define FIELD_TEST_DATA

#define PSW_GPS7560_DATA_SIZE_MAX   760

/*MS-Based*/
#define NMEA_SV 16
#define MAX_SVINVIEW 16

  /* A21 Subscription Info record */
#define PSW_A21_MS_SUBS_INFO_ELEMENT_ID                0x0B
#define PSW_A21_MS_SUBS_INFO_ELEMENT_ID_WIDTH             8
#define PSW_A21_MS_SUBS_INFO_ELEMENT_LEN_WIDTH            8
#define PSW_A21_MS_SUBS_INFO_BC_REC_ID                 0x00
#define PSW_A21_MS_SUBS_INFO_BC_REC_ID_WIDTH              8
#define PSW_A21_MS_SUBS_INFO_BC_REC_LEN_WIDTH             8
#define PSW_A21_MS_SUBS_INFO_BC_ALL_INCLUDE_WIDTH         1
#define PSW_A21_MS_SUBS_INFO_BC_CURRENT_SUBCLASS_WIDTH    7
#define PSW_A21_MS_SUBS_INFO_BC_BANDCLASS_WIDTH           8
#define PSW_A21_MS_SUBS_INFO_BC_RESERVED1_WIDTH           3
#define PSW_A21_MS_SUBS_INFO_BC_SUBCLASS_LEN_WIDTH        4
#define PSW_A21_MS_SUBS_INFO_BC_SUBCLASS_WIDTH            8
#define PSW_A21_MS_SUBS_INFO_HEADER_LEN          ((PSW_A21_MS_SUBS_INFO_ELEMENT_ID_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_ELEMENT_LEN_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_REC_ID_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_REC_LEN_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_ALL_INCLUDE_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_CURRENT_SUBCLASS_WIDTH) / 8)

#define PSW_A21_MS_SUBS_INFO_BANDCLASS_LEN      ((PSW_A21_MS_SUBS_INFO_BC_BANDCLASS_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_ALL_INCLUDE_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_RESERVED1_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_SUBCLASS_LEN_WIDTH \
                                                  + PSW_A21_MS_SUBS_INFO_BC_SUBCLASS_WIDTH * 4) / 8)

#define PSW_MAX_A21_MS_SUBS_INFO_LEN    (PSW_A21_MS_SUBS_INFO_HEADER_LEN + PSW_A21_MS_SUBS_INFO_BANDCLASS_LEN * NAM_BANDS_SUPPORTED)
#define MAX_SMS_ACK_PDU_LEN								255

#ifdef MTK_CBP
#define PSW_SMS_CBS_CHA_MAX 4
#endif
/****************************************************************************
 * Other Global declarations.
 ***************************************************************************/
extern bool VoicePrivacy;

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/

extern const RetrievableAndSettableParametersT parameterIDTable[];

/* Task message Id's */
typedef enum
{
   /* from multiple sources */
   PSW_CMD_STATUS_MSG = 0,

   /* from L1D */
   PSW_PMRM_REQ_MSG,
   PSW_CAND_FREQ_TIMES_MSG,
   PSW_CAND_FREQ_MEASUREMENTS_MSG,
   PSW_SCAN_MEASUREMENTS_MSG,
   PSW_IDLE_HANDOFF_MSG,
   PSW_START_CF_MEASURE_MSG,
   PSW_END_CF_MEASURE_MSG,
   PSW_INITIAL_ACQ_REPORT_MSG,

   PSW_SET_IDLE_HO_PARAMETERS_MSG = 10,

   PSW_OUTER_LOOP_REPORT_REQ_MSG = 13,
   PSW_GET_PS_INFO_MSG,
   PSW_L1D_ACCESS_PREAMBLE_COMPLETE_MSG,
   PSW_L1D_ACCESS_COMPLETE_MSG,
   PSW_L1D_ACCESS_PROBE_COMPLETE_MSG,
   PSW_OOSA_WAKEUP_IND_MSG,
   PSW_L1D_RSSI_RPT_MSG,
   PSW_STATUS_REQUEST_RSSI_RSP_MSG = 20,
#if defined(MTK_CBP)
   PSW_L1D_DEACTIVE_CNF_MSG,
#endif
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
   PSW_L1D_ACCESS_PREAMBLE_DELAY_MSG,
#endif

   /* from LMD */
   PSW_FORWARD_SYNC_CHAN_FRAME_MSG = 40,
   PSW_FORWARD_PAGE_CHAN_FRAME_MSG,
   PSW_FORWARD_BCCH_FRAME_MSG,
   PSW_FORWARD_FCCCH_FRAME_MSG,
   PSW_DSCH_CHAN_FRAME_MSG,
   PSW_REVERSE_STATISTICS_PARMS_MSG,
   PSW_SERVICE_OPTION_CONTROL_RESP_MSG,
   PSW_RSCH_REL_IND_MSG,
   PSW_STAT_CNT_MSG,
   PSW_FORWARD_PAGE_CHAN_MSG,
   PSW_FORWARD_FCCCH_MSG = 50,
   PSW_FORWARD_BCCH_MSG,
   PSW_FORWARD_SYNC_CHAN_MSG,
   PSW_DSCH_CHAN_MSG,

   /* from MMI */
   PSW_POWER_MSG = 71,
   PSW_CALL_INITIATE_MSG,
   PSW_CALL_INITIATE_WITH_INFO_REC_MSG,
   PSW_GET_PRL_INFO_MSG,
   PSW_E911_MODE_MSG,
   PSW_CALL_ANSWER_MSG,
   PSW_CALL_HANGUP_MSG,
   PSW_SEND_CONT_DTMF_ON_MSG,
   PSW_SEND_CONT_DTMF_OFF_MSG,
   PSW_SEND_BURST_DTMF_MSG = 80,
   PSW_HOOKFLASH_MSG,
   PSW_SET_PRIVACY_MODE_MSG,

   PSW_SET_RETRIEVABLE_PARAMETER = 85,
   PSW_GET_RETRIEVABLE_PARAMETER,
   PSW_INJECT_HANDOFF_MSG,

   PSW_GET_FULL_SYS_TIME_MSG = 90,
   PSW_SET_SILENT_RETRY_MSG,
   PSW_GET_SILENT_RETRY_STATUS_MSG,
   PSW_SET_MOB_PREV_MSG,
   PSW_SET_CUSTOM_FEATURE_MSG,
   PSW_SET_PRL_ENABLE_MSG,


   /* from VAL - SMS related */
#if defined(LGT_EXTENSIONS)
   PSW_SMS_UNKNOWN_CONNECT_MSG = 120,
   PSW_SMS_UNKNOWN_DISCONNECT_MSG,
   PSW_SMS_UNKNOWN_PARMS_MSG,
#endif
   PSW_SMS_PREF_SRV_OPT_MSG = 130,
   PSW_SMS_CONNECT_MSG,
   PSW_SMS_DISCONNECT_MSG,
   PSW_SMS_BCAST_CONNECT_MSG,
   PSW_SMS_BCAST_DISCONNECT_MSG,
   PSW_SMS_BCAST_PARMS_MSG,
   PSW_SMS_SUBMIT_PARMS_MSG,
   PSW_SMS_SUBMIT_DATA_MSG,
   PSW_SMS_CANCEL_MSG,
   PSW_SMS_ACK_PARMS_MSG = 140,
   PSW_SMS_ACK_DATA_MSG,
   PSW_SMS_TERMINAL_STATUS_MSG,
   PSW_SMS_TIMER_EXPIRED_MSG,
   PSW_SMS_CAUSE_CODE_STATUS_MSG,
   PSW_SMS_RETX_AMOUNT_MSG,
   PSW_SMS_CAUSE_CODE_SET_MSG,
   PSW_SMS_REPLY_TL_ACK_MSG,
   PSW_TIMER_EXPIRED_MSG,
   PSW_SMS_PREF_REQ_MSG,
   PSW_SMS_REL_REQ_MSG = 150,
   PSW_SMS_TX_REQ_MSG,
   PSW_SMS_RESEND_TIMER_EXPIRED_MSG,
#ifdef MTK_CBP
   PSW_SMS_MMS_TIMER_EXPIRED_MSG,
   PSW_SMS_MOREL_TIMER_EXPIRED_MSG,
   PSW_SMS_TLACK_REQ_MSG,
   PSW_SMS_RETRY_PERIOD_TIMER_EXPIRED_MSG,
#endif

   #ifdef CBP7_IMS
   PSW_IMS_SMS_READY_MSG,
   PSW_IMS_SMS_RSP_MSG,
   PSW_IMS_SMS_NOTIFY_MSG,
   #endif
   PSW_SET_MULTI_SMS_MODE_MSG,
   #ifdef MTK_PLT_ON_PC_UT
   PSW_SMS_BROADCAST_EXPECTED_MSG,
   PSW_SMS_MT_DATA_BURST_MSG,
   #endif
   PSW_VAL_PREF_VOICE_SERVICEOPTION_SET_MSG =160,
   PSW_VAL_REG_RESUME,
#ifdef MTK_CBP
   PSW_SMS_CBS_INFO_CFG,
#endif
   PSW_VAL_SET_SO_SUPPORT_MSG,

#ifdef MTK_CBP_ENCRYPT_VOICE
   PSW_VAL_ENCRYPT_VOICE_REQ_MSG,
   PSW_VAL_SET_KS_KEY_REQ_MSG,
   PSW_VAL_CLEAR_TEMP_KEY_REQ_MSG,
   PSW_VAL_CANCEL_CIPHER_SMS_SEND_REQ_MSG,
#endif

   /* from DBM */
   PSW_DBM_DATA_NAM_MSG = 170,
   PSW_DBM_DATA_NAM_WRITE_ACK_MSG,
   PSW_DBM_DATA_MSG,
   PSW_DBM_DATA_WRITE_ACK_MSG,
   PSW_DBM_FLUSH_ACK_MSG,
   PSW_DBM_DATA_PRL_MSG,
   PSW_DBM_DATA_PRL_WRITE_MSG,
   PSW_DBM_DATA_MS_CAP_DB_MSG,
   PSW_DBM_DATA_MS_CAP_DB_WRITE_ACK_MSG,
   PSW_DBM_DATA_MISC_MSG,
   PSW_NVM_CHANGED_BY_ETS_MSG = 180,
   PSW_NAM_UNLOCK_ACK_MSG,
   PSW_DBM_DATA_MISC_WRITE_ACK_MSG,
   PSW_DBM_SECURE_DATA_WRITE_ACK_MSG,
   PSW_DBM_SECURE_DATA_MSG,
   PSW_GET_NAM_MSG,
   PSW_NAM_CHANGED_MSG,
   PSW_DBM_INIT_MSG,
#ifdef MTK_CBP_ENCRYPT_VOICE
   PSW_DBM_CRYPT_DATA_READ_ACK_MSG,
   PSW_DBM_CRYPT_DATA_WRITE_ACK_MSG,
#endif
#ifdef MTK_CBP
   PSW_DBM_SYNC_MEID_MSG,
#ifdef MTK_DEV_C2K_IRAT
   PSW_DBM_INIT_ON_STACK_DOWN_MSG,
#endif
#endif

   /* from R-UIM */
   PSW_UIM_GET_NAM_DATA_MSG = 210,
   PSW_UIM_GET_UIM_MEID_FEATURE_MSG,
   PSW_UIM_UPDATE_NAM_DATA_MSG,
   PSW_UIM_STORE_ESN_MSG,
   PSW_UIM_DATA_PRL_MSG,
   PSW_UIM_BS_CHALLENGE_ACK_MSG,
   PSW_UIM_CONFIRM_SSD_ACK_MSG,
   PSW_UIM_RUN_CAVE_ACK_MSG,
   PSW_UIM_GENERATE_KEY_VPM_ACK_MSG,
   PSW_UIM_STATUS_NOTIFY_MSG,
   PSW_UIM_DATA_OTAFeature_MSG = 220,
   PSW_UIM_MS_KEY_REQUEST_ACK_MSG,
   PSW_UIM_KEY_GENERATION_REQUEST_ACK_MSG,
   PSW_UIM_COMMIT_ACK_MSG,
   PSW_UIM_VALIDATE_ACK_MSG,
   PSW_UIM_CONFIGURATION_REQUEST_ACK_MSG,
   PSW_UIM_DOWNLOAD_REQUEST_ACK_MSG,
   PSW_UIM_SSPR_CONFIGURATION_REQUEST_ACK_MSG,
   PSW_UIM_SSPR_DOWNLOAD_REQUEST_ACK_MSG,
   PSW_UIM_OTAPA_REQUEST_ACK_MSG,
   PSW_UIM_UPDATE_BINARY_ACK_MSG = 230,
   PSW_UIM_OTA_RUN_CAVE_ACK_MSG,
   PSW_UIM_STORE_MECONFIG_MSG,
#ifdef MTK_CBP
   PSW_UIM_SMS_CAP_MSG,
#endif
   /* from RLP */
   PSW_DATA_BUFFER_STATUS_MSG = 250,

   /* from ets or generic PS interfaces */
   PSW_ACCESS_SIGNAL_ETS_MSG = 260,
   PSW_TEST_MODE_TIMERS_USED_MSG = 261,
#ifdef MTK_PLT_ON_PC_UT
   PSW_UNIT_TEST_REVERSE_SIG_MSG,
   PSW_SET_IRAT_PARAM_MSG,
#endif

   PSW_ENGINE_TEST_MODE_MSG = 265,
   PSW_STATUS_REQUEST_MSG,
   PSW_PHONE_STATE_MSG,
   PSW_INIT_NAM_NOT_PROVISIONED_MSG,
   PSW_INIT_NAM_ALREADY_PROVISIONED_MSG,

   PSW_INIT_MS_CAP_DB_MSG                   = 270,
   PSW_HSPD_ACTIVE_CONTROL_HOLD_MSG,
   PSW_RSCH_REQUEST_MSG,
   PSW_GET_P_REV_IN_USE_MSG,
   PSW_CLEAR_MRU_MSG,
   PSW_GET_ACTIVE_NAM_MSG,
   PSW_SELECT_ACTIVE_NAM_MSG,
   PSW_FORCE_ANALOG_MODE_MSG,
   PSW_ENTER_AKEY_MSG,
   PSW_SET_ACCESS_HO_PARAMETERS_MSG,
   PSW_DEBUG_SCREEN_INFO_REQUEST = 280,
   PSW_DEBUG_PILOT_STRENGTH_INFO_REQUEST,
   PSW_DEBUG_SCRN_LMD_RSP_MSG,
   PSW_DEBUG_SCRN_L1D_RSP_MSG,
   PSW_EXIT_EMERGENCY_MODE_MSG,
   PSW_GET_GPS_DEBUG_INFO_MSG,
   PSW_SET_MOBILE_ID_MSG,
   PSW_GET_MOBILE_ID_MSG,
   PSW_OTTS_TIMER_SIMULATION,
   PSW_HLW_MIP_KEYS_UPDATE_MSG,
   PSW_READ_RAW_IMSI_MSG = 290,
   PSW_WRITE_RAW_IMSI_MSG,
   PSW_SLOTTED_VETO_MSG,
   PSW_SET_MIP_PASSWORD_MSG,
   PSW_GET_MIP_KEYS_MSG,
   PSW_SET_SIP_PASSWORD_MSG,
   PSW_GET_SIP_PASSWORD_MSG,
   PSW_SET_SPC_MSG,
   PSW_GET_SPC_MSG,
   PSW_GET_SSD_MSG,
   PSW_INIT_SECURE_DB_MSG = 300,
   PSW_GET_CP_BC_TO_CDMA_BC_MSG,
   PSW_BAND_SUPPORTED_QUERY_RSP_MSG,
   PSW_SET_FLY_MODE_ENABLE_MSG,
   PSW_ENABLE_PRL_MSG,
   PSW_SET_PARM_MSG,
   PSW_GET_PARM_MSG,
   PSW_WRITE_RAW_MDN_MSG,
   PSW_TA_TIMER_EXPIRED_MSG,
   PSW_DATA_CALL_ANSWER_MSG = 310,
   PSW_UICC_FACTORY_MODE_MSG,
   PSW_VAL_PILOT_PWR_ACK_MSG,

   PSW_DS_CALL_REL_MSG = 313,
   PSW_PS_CALL_REL_MSG,
   PSW_LS_CALL_REL_MSG,
   PSW_DS_ENABLE_MSG,
   PSW_PS_DORMANT_REQ_MSG,
   PSW_DATA_PREF_REQ_MSG,
   PSW_VS_PREF_REQ_MSG,
   PSW_LS_PREF_REQ_MSG = 320,
   PSW_VS_SWITCH_SO_MSG,
   PSW_VS_DS_PREARANG_MSG,
   PSW_SET_SERVICE_CONFIG_MSG,
   PSW_SET_PKZID_HYSTER_TMR_MSG,
   PSW_INIT_PSW_MISC_DB_MSG,
   PSW_CUSTOM_LOCK_FEATURE_REG,
   PSW_CUSTOM_LOCK_TRIGGER,
#ifdef MTK_CBP
   PSW_PS_SUSPEND_REQ_MSG,
   PSW_PS_RESUME_REQ_MSG,
#endif

   /* System Selection Messages */
   PSW_CSS_SELECT_RSP_MSG = 400,
   PSW_CSS_VALIDATE_RSP_MSG,
   PSW_CSS_SYS_ACQ_REQ_MSG,
   PSW_CSS_ACQ_ABORT_REQ_MSG,
   PSW_CSS_OOSA_SLEEP_REQ_MSG,
   PSW_CSS_OOSA_CONT_SLEEP_REQ_MSG,/*used for continuous sleep cmd after OOSA timer expired*/
   PSW_CSS_OOSA_WAKEUP_REQ_MSG,
   PSW_CSS_CONN_START_CNF_MSG,
   PSW_CSS_OTASP_SCAN_FINISHED_MSG,
   PSW_CSS_INDICATE_IN_SERVICE_MSG,
#ifdef MTK_CBP
   PSW_CSS_SET_EMERGENCY_CALLBACK_MODE_MSG,
   PSW_NOTIFY_SRV_STATUS_TO_UPPER_LAYER_MSG,
   PSW_CSS_END_E911_CALL_REQ_MSG,
#endif

   PSW_CSS_VAL_SYNC_RSP_MSG = 430,
   PSW_SET_LOST_SCAN_PING_FREQUENCY_MSG,
   PSW_IRAT_MCC_REQ,
   PSW_IRAT_CUR_SYS_MEAS_REQ,
   PSW_IRAT_RAT_SIG_MON_REQ,
   PSW_IRAT_1X_REG_REQ,

   /* OTASP IS683 */
   PSW_OTASP_RCVD_MSG = 440,
   PSW_OTASP_REL_MSG,
   PSW_OTASP_TX_REQ_MSG,
   PSW_OTASP_SO_PREF_MSG,
   PSW_EXP_MOD_MS_RESULT_MSG,
   PSW_EXP_MOD_BS_RESULT_MSG,
   PSW_FILL_SSPR_RSP_MSG,
   PSW_IOTA_IS683_MSG,
   PSW_IOTA_PRL_MSG,
   PSW_OTA_SPL_UNLOCK_FAIL_MSG,
#ifdef MTK_CBP
   PSW_OTA_SET_POWER_UP_MODE_MSG,
   PSW_VERIFY_SPC_MSG,
#endif

   /* IS801 Location Services */
   PSW_POS_AFLT_PPM_REQ_MSG = 458,
   PSW_POS_AFLT_MEASUREMENTS_MSG = 460,
   PSW_POS_AFLT_ACK_ABORT_MSG,
   PSW_POS_AFLT_ABORT_REQ_MSG,
   PSW_IS801_TCPB_REV_DATA_RESP_INFO_MSG,
   PSW_IS801_TCPB_FWD_DATA_RESP_INFO_MSG,
   PSW_IS801_SESSION_START_MSG ,
   PSW_IS801_TCP_CONN_STATUS_MSG,
   PSW_IS801_RESET_ASSIST_MSG,
   PSW_LOC_START_MODE_SET_MSG,
   PSW_LOC_FIX_MODE_GET_MSG,

   PSW_IS801_MS_CANCEL_REQ_MSG = 470,
   PSW_POS_REJECT_MSG,
   PSW_IS801_EPH_ALM_TIME_MSG,
   PSW_IS801_MSS_DONE_MSG,
   PSW_IS801_AFLT_INFO_GET_MSG,
   PSW_LOC_PREF_REQ_MSG = 475,
   PSW_PDE_SESSION_ABORT_REQ_MSG,
   PSW_PDE_PSEUDORANGE_MSMT_RSP_MSG,
   PSW_PDE_SESSION_END_RSP_MSG,
   PSW_PDE_GPS_INOP_MSG,
   PSW_GPS_ENABLE_REPORTS_MSG,
   PSW_TEST_LOC_PDDM_MSG,
   PSW_TEST_POWER_CONTROL_MSG,
   PSW_L1D_ALTERNATE_PILOTS_MSG,
   PSW_LOCATION_SVC_CFG_MSG,
   PSW_IS801_RESTART_FIX_MSG,
   /*MS-Based*/
   PSW_ASSIST_REQ_MSG,
   PSW_GPS_NMEA_STREAM_MSG,
   PSW_IS801_NMEA_SETTING_MSG,
   PSW_IS801_MPC_CONN_MSG,
   PSW_IS801_GET_SECTOR_INFO_MSG = 490,
   PSW_LOC_STATE_CHANGE_ETS_TEST_CMD,
   PSW_IS801_PPM_TIMER_EXP_MSG,
   PSW_IS801_SESS_TIMER_EXP_MSG,
   PSW_IS801_PRM_TIMER_EXP_MSG,
   PSW_IS801_PREF_RESP_QUAL_TIMER_EXP_MSG,
   PSW_IS801_OUT_REQ_TIMER_EXP_MSG,
   /****MS-Based*********/
   /*From LEC*/
   PSW_LEC_LOC_RSP_MSG,
   PSW_GPS_READY_REQ_MSG,
   PSW_GPS_READY_MSG,
   PSW_GPS_SIXTY_TIMER_MSG,
   PSW_GET_TRANSMIT_INFO_MSG,
   PSW_SET_TRANSMIT_INFO_MSG,
   PSW_SYSTEM_READY_REQ_MSG,
   PSW_IS801_CUSTOMER_SETTING_MSG,
#ifdef MTK_CBP
   PSW_LEC_SESSION_END_RSP_MSG,
#endif

   /* AMPS related message ID's */
   PSW_AMPS_BB_IND_TX_STAT_MSG,
   PSW_AMPS_BB_IND_CHAN_FORMAT_MSG,
   PSW_AMPS_BB_IND_SAT_CC_MSG,
   PSW_AMPS_BB_IND_FOCC_DATA_MSG,
   PSW_AMPS_BB_IND_FVC_DATA_MSG,
   PSW_AMPS_RELAY_RADIO_TUNE_STAT_MSG,
   PSW_AMPS_RELAY_RADIO_FSU_LOCK_MSG,
   PSW_AMPS_RELAY_RADIO_RSSI_VAL_MSG,
   PSW_AMPS_RELAY_RADIO_CARRIER_STAT_MSG,
   PSW_AMPS_RELAY_RADIO_POWER_LVL_MSG,
   PSW_AMPS_RELAY_RADIO_AFC_COMPLETE_MSG,
   PSW_AMPS_CP_RQ_ENABLE_MSG,
   PSW_AMPS_CP_RQ_POWER_DOWN_MSG,
   PSW_AMPS_CP_RQ_CALL_INITIATE_MSG,
   PSW_AMPS_CP_RQ_CALL_ANSWER_MSG,
   PSW_AMPS_CP_RQ_CALL_HANGUP_MSG,
   PSW_AMPS_AUDIO_VOICE_MSG,
   PSW_AMPS_READ_DATA_ACK_MSG,
   PSW_AMPS_POWER_MSG,
   PSW_AMPS_WBIOS_INIT_COMPLETE_MSG,
   PSW_AMPS_SET_RSSI_TH_MSG,
   PSW_AMPS_WORD_SYNC_RSP_MSG,

   PSW_GPS_FRAME_SYNC_DONE_MSG=800,
   PSE_LEC_CDMA_SYSTEM_REQ_MSG,
   PSW_USERMODE_MSG,
   PSW_TESTMODE_MSG,
   PSW_LTE_MSG,
   PSW_NST_POWERUP_MSG,
   PSW_NST_TRANSMIT_TCH_MSG,
   PSW_NST_EXIT_TEST_MODE_MSG,
   PSW_NST_LIST_SET_MSG,
#ifdef MTK_CBP
   PSW_NST_RX_PWR_REQ_MSG,
   PSW_L1D_RX_PWR_CNF_MSG,
   PSW_NST_TX_PWR_LEVEL_SET_MSG,
#endif
   PSW_SET_EMERGENCY_CARD_INIT_MSG,
   PSW_MANUAL_AVOID_NETWORK_MSG,
   PSW_CSFB_CDMA2000PARM_MSG,
   PSW_CSFB_SIB8_PARM_MSG,
   PSW_CSFB_GCSNA_DL_MSG,
   PSW_CSFB_MEAS_REQ_MSG,
   PSW_CSFB_REDIRECT_MSG,
   PSW_CSFB_HO_EUTRA_PREP_MSG,
   PSW_CSFB_MOBFROM_EUTRA_MSG,
   PSW_CSFB_SERVICE_REJECT_MSG,
   PSW_ANTENNA_CFG_MSG,
   PSW_CSFB_GET_STATUS_MSG,
#ifdef MTK_CBP
   PSW_HLP_DORMANT_CNF_MSG,
#endif
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
   PSW_CSS_RSVAS_SUSPEND_CNF_MSG,
   PSW_CSS_RSVAS_DO_VRTL_SUSP_CNF_MSG,
   PSW_RSVAS_VIRTUAL_SUSPEND_REQ_MSG,
   PSW_RSVAS_RESUME_REQ_MSG,
   PSW_RSVAS_OCCUPY_IND_MSG,
#ifndef MTK_PLT_ON_PC_IT
   RSVAS_CAS_SUSPEND_SERVICE_REQ,
   RSVAS_CAS_RESUME_SERVICE_REQ,
   RSVAS_CAS_VIRTUAL_SUSPEND_SERVICE_REQ,
   RSVAS_CAS_VIRTUAL_RESUME_SERVICE_REQ,
   RSVAS_CAS_SERVICE_RES_OCCUPY_IND,
   RSVAS_CAS_ABORT_SERVICE_REQ,
   RSVAS_CAS_VIRTUAL_SUSPEND_SERVICE_COMPLETE_IND,
   /* ESR */
   C2K_LTE_CSFB_CNF,
   C2K_LTE_CSFB_STOP_CNF,
#endif
   PSW_IDP_RSVAS_SERVICE_OCCUPY_REQ_MSG,
   PSW_IDP_RSVAS_SERVICE_RELEASE_REQ_MSG,
   PSW_C2K_RSVAS_OCCUPY_IND_MSG,
   PSW_C2K_SIM_INDEX_MSG,
#endif
#if defined(MTK_PLT_ON_PC_UT)
   PSW_SEND_SMS_CONFIRM_MSG,
   PSW_SEND_SBP_CONFIG_MSG,
#endif
#ifdef MTK_CBP
   PSW_SMS_WAIT_L3ACK_TIMER_EXPIRED_MSG,
   PSW_SMS_WAIT_RSP_TIMER_EXPIRED_MSG,
#endif
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
   PSW_HLP_RSVAS_ABORT_CNF_MSG,
#endif
   PSW_VAL_SET_DDTM_MSG,
   PSW_NUM_MSG_IDS
} PswMsgIdT;

typedef enum
{
   PSW_PILOT_ACQUIRE,
   PSW_NO_PILOT,
   PSW_SYNC_ACQUIRE_FAIL,
   PSW_SYNC_TO_SYS_TIME,
   PSW_SYNC_TO_SYS_TIME_FAIL,
   PSW_PAGE_MONITOR,
   PSW_PAGE_MONITOR_FAIL,
   PSW_BCCH_MONITOR,              /* BSR 3/12/04 */
   PSW_BCCH_MONITOR_FAIL,         /* BSR 3/12/04 */
   PSW_FCCCH_MONITOR,             /* BSR 3/16/04 */
   PSW_FCCCH_MONITOR_FAIL,        /* BSR 3/16/04 */
   PSW_SEND_ACCESS_MSG_FAIL,
   PSW_SEND_TC_MSG,
   PSW_SEND_TC_MSG_FAIL,
   PSW_TC_CONFIG,
   PSW_LONG_CODE_CHANGED,
   PSW_RADIO_TUNED,
   PSW_RADIO_NOT_TUNED,
   PSW_PILOT_LOST,
   PSW_PUF_PROBE_SENT,
   PSW_PUF_PROBE_SENT_MAX_PWR,
   PSW_PUF_PROBE_NOT_SENT,
   PSW_IND_WAKEUP,
   PSW_IND_PILOT_SET,
   PSW_SOFT_HO_COMPLETE,
   PSW_OOSA_WAKEUP,
   PSW_AMPS_HO_COMPLETE,
   PSW_RSCH_STARTED,
   PSW_FSCH_STARTED,
   PSW_FSCH_TERMINATED,
   PSW_RSR_UPDATED,
   PSW_HARD_HO_COMPLETE,
   PSW_IND_FREEZE,
   PSW_IND_THAW,
   PSW_IND_RESYNC,
   PSW_IND_RESYNC_DENIED,
   PSW_ANTENNA_REQ_GRANTED,
   PSW_ANTENNA_REQ_DENIED,
   PSW_ANTENNA_RELEASED,
   PSW_MODEM_FAILURE,
   PSW_MODEM_READY,
   PSW_IRAT_RESET_1XCDMA,
   PSW_NUM_CMD_STATUS
} PswCmdStatusT;

typedef enum
{
   PSW_FRAME_CATAGORY_RS1_9600_FULL_PRI = 1,
   PSW_FRAME_CATAGORY_RS1_9600_DIM_BURST_HALF_PRI_SIG = 2,
   PSW_FRAME_CATAGORY_RS1_9600_DIM_BURST_QUARTER_PRI_SIG = 3,
   PSW_FRAME_CATAGORY_RS1_9600_DIM_BURST_EIGHTH_PRI_SIG = 4,
   PSW_FRAME_CATAGORY_RS1_9600_BLANK_BURST_SIG = 5,
   PSW_FRAME_CATAGORY_RS1_4800_FULL_PRI = 6,
   PSW_FRAME_CATAGORY_RS1_2400_FULL_PRI = 7,
   PSW_FRAME_CATAGORY_RS1_1200_FULL_PRI = 8,
   PSW_FRAME_CATAGORY_RS1_9600_FULL_PRI_BIT_ERRORS = 9,
   PSW_FRAME_CATAGORY_RS1_INSUFFICIENT_FRAME_QUALITY = 10,
   PSW_FRAME_CATAGORY_RS1_9600_DIM_BURST_HALF_PRI_SEC = 11,
   PSW_FRAME_CATAGORY_RS1_9600_DIM_BURST_QUARTER_PRI_SEC = 12,
   PSW_FRAME_CATAGORY_RS1_9600_DIM_BURST_EIGHTH_PRI_SEC = 13,
   PSW_FRAME_CATAGORY_RS1_9600_BLANK_BURST_SEC = 14,
   PSW_FRAME_CATAGORY_RS1_9600_DCCH_NULL_FRAME = 15,
   PSW_FRAME_CATAGORY_RS2_14400_FULL_PRI = 1,
   PSW_FRAME_CATAGORY_RS2_14400_DIM_BURST_HALF_PRI_SIG = 2,
   PSW_FRAME_CATAGORY_RS2_14400_DIM_BURST_QUARTER_PRI_SIG = 3,
   PSW_FRAME_CATAGORY_RS2_14400_DIM_BURST_EIGHTH_PRI_SIG = 4,
   PSW_FRAME_CATAGORY_RS2_14400_BLANK_BURST_SIG = 5,
   PSW_FRAME_CATAGORY_RS2_14400_DIM_BURST_HALF_PRI_SEC = 6,
   PSW_FRAME_CATAGORY_RS2_14400_DIM_BURST_QUARTER_PRI_SEC = 7,
   PSW_FRAME_CATAGORY_RS2_14400_DIM_BURST_EIGHTH_PRI_SEC = 8,
   PSW_FRAME_CATAGORY_RS2_14400_BLANK_BURST_SEC = 9,
   PSW_FRAME_CATAGORY_RS2_14400_DIM_BURST_EIGHTH_PRI_SEC_SIG = 10,
   PSW_FRAME_CATAGORY_RS2_7200_FULL_PRI = 11,
   PSW_FRAME_CATAGORY_RS2_7200_DIM_BURST_QUARTER_PRI_SIG = 12,
   PSW_FRAME_CATAGORY_RS2_7200_DIM_BURST_EIGHTH_PRI_SIG = 13,
   PSW_FRAME_CATAGORY_RS2_7200_BLANK_BURST_SIG = 14,
   PSW_FRAME_CATAGORY_RS2_7200_DIM_BURST_QUARTER_PRI_SEC = 15,
   PSW_FRAME_CATAGORY_RS2_7200_DIM_BURST_EIGHTH_PRI_SEC = 16,
   PSW_FRAME_CATAGORY_RS2_7200_BLANK_BURST_SEC = 17,
   PSW_FRAME_CATAGORY_RS2_7200_DIM_BURST_EIGHTH_PRI_SEC_SIG = 18,
   PSW_FRAME_CATAGORY_RS2_3600_FULL_PRI = 19,
   PSW_FRAME_CATAGORY_RS2_3600_DIM_BURST_EIGHTH_PRI_SIG = 20,
   PSW_FRAME_CATAGORY_RS2_3600_BLANK_BURST_SIG = 21,
   PSW_FRAME_CATAGORY_RS2_3600_DIM_BURST_EIGHTH_PRI_SEC = 22,
   PSW_FRAME_CATAGORY_RS2_3600_BLANK_BURST_SEC = 23,
   PSW_FRAME_CATAGORY_RS2_1800_FULL_PRI = 24,
   PSW_FRAME_CATAGORY_RS2_1800_BLANK_BURST_SEC = 25,
   PSW_FRAME_CATAGORY_RS2_INSUFFICIENT_FRAME_QUALITY = 26,
   PSW_FRAME_CATAGORY_RS2_14400_DCCH_NULL_FRAME = 27,

   PSW_FRAME_CATAGORY_RS_LAST = PSW_FRAME_CATAGORY_RS2_14400_DCCH_NULL_FRAME

} PswFrameCatagoryT;

typedef enum
{
   PSW_BAD_FRAME = 0,
   PSW_GOOD_FRAME
} PswFrameQuality;

typedef PACKED_PREFIX struct
{
   bool     Stale;
   uint16   PnPhase;
   uint16   Strength;
} PACKED_POSTFIX  PswPilotMeasurementT;

typedef PACKED_PREFIX struct
{
   uint16   PilotPN;
   bool     Stale;
   uint16   PnPhase;
   uint16   Strength;
} PACKED_POSTFIX  PswRemainPilotMeasurementT;

/*------------------------------------------------------------------------
  Define funtion prototypes
-------------------------------------------------------------------------*/

extern PswIs95NamT* PswNamGet (void);
extern void PswDbmInit( void );
#ifdef MTK_DEV_C2K_IRAT
extern void PswDbmInitOnStackDown( void );
#endif


/*******************/
/* DBM definitions */
/*******************/

/*------------------------------------------------------------------------
* Nam Data Structures
*  The name of the structure is misleading. It should support everything
*  IS95, T53 and AMPS.
*
*  This structure should exactly match the structure "IS95Nam" in the
*   protocol engine header file "namdata.h"
*------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
   SysCdmaBandT  CdmaBand[MAX_MRU_RECORDS];
   uint16        FreqChan[MAX_MRU_RECORDS];
} PACKED_POSTFIX  PswDbmDataT;

typedef enum
{
   PSW_AYNC_DATA = 1,   /* bit 0 */
   PSW_FAX = 2,         /* bit 1 */
   PSW_UPB_ASYNC = 64   /* bit 6 */
} PswDsEnableBitMapT;

typedef PACKED_PREFIX struct
{
   bool CssSyncValOk;
} PACKED_POSTFIX  PswCssValSyncRspMsgT;

/***********************/
/* Message Definitions */
/***********************/
typedef PACKED_PREFIX struct
{
   uint8    numAlt;
   uint16   phase[SYS_CP_MAX_CANDIDATE_LIST_PILOTS];
   uint16   strength[SYS_CP_MAX_CANDIDATE_LIST_PILOTS];
} PACKED_POSTFIX  AlternatePilotsInfoT;

typedef PACKED_PREFIX struct
{
   bool     pilotFound;     /* Pilot found */
   uint16   pilotStrength;  /* Pilot Ec/Io (linear Q6) */
   int16    rxPower;        /* RSSI (dBm)  */
#if defined(MTK_CBP_SYNC_OPTIMIZE)
   bool     retried;        /*For mini pilot acq, this channel retries normal ICS or not*/
#endif
   AlternatePilotsInfoT altPilots;
} PACKED_POSTFIX  PswInitialAcqReportMsgT;
typedef enum
{
   PI_OFF     = 0,
   PI_ON      = 1,
   PI_UNDEF   = 2,
   PI_ERASURE = 3
} QpchPiStat;

typedef enum
{
   PSW_PCH   = 0,
   PSW_QPCH1 = 1,
   PSW_QPCH2 = 2,
   PSW_CCI   = 3,
   PSW_FCCCH = 4,
   PSW_BC    = 5
} WakeModeT;

typedef PACKED_PREFIX struct
{
   WakeModeT WakeMode;            /* Status of a command previously sent by PS */
   int16    QpchInd;              /* OFF = 0, ON = 1, Erasure = -1             */
   int16    QpchMetric;           /* X1 if first indicator, X2 if second indicator, Q0 */
   int16    PilotEnergy;          /* Usable path combined Pilot Energy, Q0 */
   uint16   EcIo;                 /* Mini-Acq Pilot Ec/Io */
   uint16   NumPath;              /* Number of usable path */
   uint16   QpchPilotPn;          /* Pilot PN Offset */
   uint32   SysTime;              /* Current CDMA time 20ms */
} PACKED_POSTFIX  PswWakeStatusT;

typedef PACKED_PREFIX struct
{
   PswCmdStatusT  CmdStatus;               /* Status of a command previously sent by PS */
   PswWakeStatusT WakeStatus;              /* Status of a command previously sent by PS */
} PACKED_POSTFIX  PswCmdStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint16   PnPhase;
   uint16   Strength;
} PACKED_POSTFIX  PswCandFreqMeasT;

typedef PACKED_PREFIX struct
{
   int16 cfTotalRxPwr;   /* Total Rx Power on CF */
   uint8 NumPilots;      /* Number of pilot measurements */
   PswCandFreqMeasT CfPilotList[SYS_CP_MAX_NEIGHBOR_LIST_PILOTS];
} PACKED_POSTFIX  PswCandFreqMeasurementsMsgT;

typedef PACKED_PREFIX struct
{
   uint8 TotalOffTimeFwd;                  /* Total time spent off of forward channel */
   uint8 MaxOffTimeFwd;                    /* Max time spent off of forward channel   */
   uint8 TotalOffTimeRev;                  /* Total time spent off of reverse channel */
   uint8 MaxOffTimeRev;                    /* Max time spent off of reverse channel   */
} PACKED_POSTFIX  PswCandFreqTimesMsgT;

typedef PACKED_PREFIX struct
{
   uint16   PilotPN;                       /* Pilot PN to handoff to */
} PACKED_POSTFIX  PswIdleHandoffMsgT;

typedef PACKED_PREFIX struct
{
   SysSystemTimeT SysTime;                 /* System time the frame was received at */
   uint16         DataSize;                /* Data size in bits*/
   uint8          Data[1];                 /* Starting address of frame data */
} PACKED_POSTFIX  PswForwardPageChanFrameMsgT;

typedef PACKED_PREFIX struct
{
    SysSystemTimeT      SysTime;            /* System time the frame was received at */
    uint8               MuxPduType;
    uint8               Category;
    PswFrameQuality     Quality;
    uint16              SigBits;
    uint8               Data[1];            /* Starting address of frame data */
} PACKED_POSTFIX PswDschFrameMsgT;

typedef PACKED_PREFIX struct
{
    uint8       SchId;
    uint16      SchBadFrames;
    uint16      SchTotFrames;
} PACKED_POSTFIX  PswPrmSchT;

typedef PACKED_PREFIX struct
{
    bool        FchMeasIncl;      /* FCH measurement included */
    uint8       FchBadFrames;     /* BAD_FRAMESs in IS2000A.5 2.6.4.1.1*/
    uint16      FchTotFrames;     /* TOT_FRAMESs in IS2000A.5 2.6.4.1.1*/
    bool        DcchMeasIncl;     /* DCCH measurement included */
    uint8       DcchBadFrames;    /* BAD_FRAMESs in IS2000A.5 2.6.4.1.1*/
    uint16      DcchTotFrames;    /* TOT_FRAMESs in IS2000A.5 2.6.4.1.1*/
    uint8       NumForSch;        /* Number of FSCH to report */
    PswPrmSchT  SchPmr[ SYS_MAX_FSCH ];
} PACKED_POSTFIX PswPmrMsgT;

/*AKK 01-26-01 Added the following for phase2 Outer Loop Report Message */
typedef PACKED_PREFIX struct
{
    uint8 SchId;
    uint8 FpcSchCurrSetPt;
} PACKED_POSTFIX PswFpcSchReportT;

typedef PACKED_PREFIX struct
{
   bool          FchIncl;
   uint8         FpcFchCurrSetPt;
   bool          DcchIncl;
   uint8         FpcDcchCurrSetPt;
   uint8         NumSup;
   PswFpcSchReportT FpcSch[2];
} PACKED_POSTFIX PswOuterLoopReportReqMsgT;
/*END--AKK 01-26-01 Added the following for phase2 Outer Loop Report Message */

typedef PACKED_PREFIX struct
{
   SysSystemTimeT SysTime;                 /* System time the frame was received at */
   uint16         DataSize;                /* Data size in bits     */
   uint8          Data[1];                 /* Starting address of frame data        */
} PACKED_POSTFIX PswFCCCHFrameMsgT;

typedef PACKED_PREFIX struct
{
   SysSystemTimeT SysTime;                 /* System time the frame was received at */
   uint16         DataSize;                /* Data size in bits   */
   uint8          Data[1];                 /* Starting address of frame data        */
} PACKED_POSTFIX PswBCCHFrameMsgT;

typedef PACKED_PREFIX struct
{
   SysSystemTimeT FrameNum;                /* Frame number this signaling data */
                                           /* was received. Note: There is no system */
                                           /* time while on the sync channel so a */
                                           /* counter is used for the frame time */
   uint8          Data[PSW_SYNC_DATA_SIZE];  /* This is a fixed 24 bit size */
} PACKED_POSTFIX PswForwardSyncChanFrameMsgT;

typedef PACKED_PREFIX struct
{
   SysSystemTimeT SysTime;         /* System time the frame was received at */
   uint16         DataSize;        /* Data size in bits */
   uint8          Data[1];         /* Starting address of frame data */
} PACKED_POSTFIX PswForwardBCCHFrameMsgT;

typedef PACKED_PREFIX struct
{
   SysSystemTimeT SysTime;         /* System time the frame was received at */
   uint16         DataSize;        /* Data size in bits */
   uint8          Data[1];         /* Starting address of frame data */
} PACKED_POSTFIX PswForwardFCCCHFrameMsgT;

typedef PACKED_PREFIX struct
{
   SysSystemTimeT       SysTime;        /* System time the frame was received at */
   SysMultiplexOptionT  MultiplexOpt;   /* multiplex option being used */
                                        /* (i.e. rate set) */
   uint16               Strength;       /* Power of the pilot in Q16 linear units */
   PswFrameCatagoryT    Catagory;       /* Frame category - size of data derived from this */
   uint8                Data[1];        /* Starting address of frame data */
} PACKED_POSTFIX PswForwardTrafficChanFrameMsgT;

typedef PACKED_PREFIX struct
{
   bool     enable;                       /* TRUE power up, FALSE power down */
} PACKED_POSTFIX  PswFlyModeMsgT;

typedef PACKED_PREFIX struct
{
   bool     PowerUp;                       /* TRUE power up, FALSE power down */
} PACKED_POSTFIX  PswPowerMsgT;

typedef PACKED_PREFIX struct
{
   bool     EnablePrl;   /* FALSE if PRL is not to be used in system selection */
} PACKED_POSTFIX  PswEnablePrlMsgT;

typedef PACKED_PREFIX struct
{
   uint8 NumActive;                        /* Number of active set pilot measurements */
   uint8 NumCandidate;                     /* Number of candidate set pilot measurements */
   uint8 NumNeighbor;                      /* Number of neighbor set pilot measurements */
   uint8 NumRemaining;                      /* Number of neighbor set pilot measurements */
   PswPilotMeasurementT Active [SYS_CP_MAX_ACTIVE_LIST_PILOTS];
   PswPilotMeasurementT Candidate [SYS_MAX_CANDIDATE_LIST_PILOTS];
   PswPilotMeasurementT Neighbor [SYS_CP_MAX_NEIGHBOR_LIST_PILOTS];
   PswPilotMeasurementT Remaining [SYS_CP_MAX_REMAINING_LIST_PILOTS];
} PACKED_POSTFIX  DebugPilotInfo;

typedef PACKED_PREFIX struct
{
   int16  PilotSeq;
   uint16 ReferencePN;
   uint8 NumActive;                        /* Number of active set pilot measurements */
   uint8 NumCandidate;                     /* Number of candidate set pilot measurements */
   uint8 NumNeighbor;                      /* Number of neighbor set pilot measurements */
   PswPilotMeasurementT Active [SYS_CP_MAX_ACTIVE_LIST_PILOTS];
   PswPilotMeasurementT Candidate [SYS_MAX_CANDIDATE_LIST_PILOTS];
   PswPilotMeasurementT Neighbor [SYS_CP_MAX_NEIGHBOR_LIST_PILOTS];
} PACKED_POSTFIX  PswScanMeasurementsMsgT;

 typedef enum
  {
    TM_CRC = 0,                                 /* Enable/Disable CRC Checking                 */
    TM_REMAIN_IN_SYNC_STATE = 1,                /* Force MS to stay on Sync Channel            */
    TM_ENABLE_IDLE_HANDOFF  = 2,                /* Enable Idle Handoff */
    TM_ENABLE_MSG_INJECTION = 3,                /* Message Injection Test Mode */
    TM_ENABLE_SLOTTED_MODE  = 4,                /* Enable Slotted Mode */
    TM_ENABLE_REGISTRATION  = 5,                /* enable/disable registration */
    TM_ENABLE_PAGE_RESPONSE = 6,                /* enable/disable page response */

    TM_ENABLE_QPCH_MODE          = 17,
    TM_DISABLE_ACCESS_AUTH_PARMS = 18,   /* Don't include auth parameters in access msgs */
    TM_DISPLAY_LOC_DATA          = 19,    /* dump Location Services Data */
    TM_ENABLE_REMAINING_SET_SEARCH= 20,    /* Controls the remaining set search */
    TM_IGNORE_OTA_AKEY_PREV      = 21,    /* Ignore A_KEY_PREV in MS Key Req Msg */

    TM_QPCH_SET_PI1              = 22,    /* Set Paging Indicator 1 override */
    TM_QPCH_SET_PI2              = 23,    /* Set Paging Indicator 2 override */
    TM_CONTINUOUS_BCCH           = 24,    /* Continuously receive the BCCH channel */
    TM_START_BCCH                = 25,    /* Used to start or stop the BCCH channel */
    TM_SLOTTED_DELAY             = 26,     /* Used to delay before going to sleep in slotted paging */

    TM_REMAIN_IN_PILOT_STATE            = 30,    /* Force MS to stay on Pilot Acquisition state */
    TM_REMAIN_IN_IDLE_SLOTTED_OFF_STATE = 31,    /* Force MS to stay on Idle state, slotted off */
    TM_REMAIN_IN_IDLE_SLOTTED_ON_STATE  = 32,    /* Force MS to stay on Idle state, slotted on  */
    TM_REMAIN_IN_TRAFFIC_STATE          = 33,    /* Disable fade timer  */
    TM_SECURE_DATA_SPY_DUMP             = 34     /* Trigger Dump of Secure Data Spy */
  }PswTestModeT, TestModeMsgT;


typedef PACKED_PREFIX struct
{
   uint8  NumAlt;
   uint16 Phase[SYS_CP_MAX_CANDIDATE_LIST_PILOTS];
   uint16 Strength[SYS_CP_MAX_CANDIDATE_LIST_PILOTS];
} PACKED_POSTFIX PswL1dAlternatePilotsMsgT;

/* PSW_LOCATION_SVC_CFG_MSG */
typedef PACKED_PREFIX struct {
   bool Mode; /* location privacy enabled */
} PACKED_POSTFIX  PswLocationSvcCfgMsgT;

typedef PACKED_PREFIX struct {
   uint8 event;
   uint8 *MsgDataP;
} PACKED_POSTFIX  PswLocStateChangedMsgT;

typedef enum
{
   PSW_REQUEST_MODE_CDMA_ONLY = 1,
   PSW_REQUEST_MODE_WIDE,
   PSW_REQUEST_MODE_CDMA_WIDE,
   PSW_REQUEST_MODE_NARROW_ONLY,
   PSW_REQUEST_MODE_CDMA_NARROW_ONLY,
   PSW_REQUEST_MODE_NARROW_WIDE,
   PSW_REQUEST_MODE_NARROW_WIDE_CDMA
} PswRequestModeT;

typedef enum
{
   PSW_NT_UNKNOWN        = 0,
   PSW_NT_INTERNATIONAL,
   PSW_NT_NATIONAL,
   PSW_NT_NETWORK_SPECIFIC,
   PSW_NT_SUBSCRIBER,
   PSW_NT_RESERVED,
   PSW_NT_ABBREVIATED    = 6
} PswNumberTypeT;

typedef enum
{
   PSW_NP_UNKNOWN = 0,
   PSW_NP_ISDN_TELEPHONY,
   PSW_NP_RESERVED2,
   PSW_NP_DATA,
   PSW_NP_TELEX,
   PSW_NP_RESERVED5,
   PSW_NP_RESERVED6,
   PSW_NP_RESERVED7,
   PSW_NP_RESERVED8,
   PSW_NP_PRIVATE = 9
} PswNumberPlanT;

typedef enum  /* This section needs to be defined in an order as it appears in SCCGEN.H */
{
   PSW_UNKNOWN_SERVICE_TYPE=0,
   PSW_VOICE_SERVICE_TYPE,
   PSW_ANALOG_VOICE_SERVICE_TYPE,
   PSW_ASYNC_FAX_DATA_SERVICE_TYPE,
   PSW_ASYNC_DATA_SERVICE_TYPE,
   PSW_FAX_DATA_SERVICE_TYPE,
   PSW_UP_ASYNC_DATA_SERVICE_TYPE,
   PSW_PACKET_DATA_SERVICE_TYPE,
   PSW_QNC_PACKET_DATA_SERVICE_TYPE,
   PSW_UP_PACKET_DATA_SERVICE_TYPE,
   PSW_LPBK_SERVICE_TYPE,
   PSW_SMS_SERVICE_TYPE,
   PSW_OTASP_SERVICE_TYPE,
   PSW_TCP_BROWSER_DATA_SERVICE_TYPE,
   PSW_LOC_SERVICE_TYPE
} PswServiceT;

typedef enum
{
  PSW_PI_ALLOWED,
  PSW_PI_RESTRICTED,
  PSW_PI_NUMBER_NA,
  PSW_PI_RSVD
} PswPresentationIndicatorT;

typedef enum
{
  PSW_SI_USER_PROVIDED_NOT_SCREENED,
  PSW_SI_USER_PROVIDED_VERIFIED_PASSED,
  PSW_SI_USER_PROVIDED_VERIFIED_FAILED,
  PSW_SI_NETWORK_PROVIDED
} PswScreeningIndicatorT;

typedef PACKED_PREFIX struct
{
   PswNumberTypeT Type;
   PswNumberPlanT Plan;
   PswPresentationIndicatorT Pi;
   PswScreeningIndicatorT    Si;
   int8 Number[CP_MAX_CALLING_PARTY_NUMBER_SIZE + 1];  /* Inc NULL terminator */
} PACKED_POSTFIX  PswCallingPartyNumberT;

/* move these two for CBP3_MERGE */
typedef enum
{
  PSW_SUBADDRESS_TYPE_NSAP,
  PSW_SUBADDRESS_TYPE_USER_SPECIFIED = 2
} PswSubaddressTypeT;

#ifdef MTK_CBP
/* registration type enum */
typedef enum
{
  TIMER_BASED_REGISTRATION = 0,
  POWER_UP_REGISTRATION = 1,
  ZONED_BASED_REGISTRATION = 2,
  POWER_DOWN_REGISTRATION = 3 ,
  PARAMETER_CHANGE_REGISTRATION = 4,
  ORDERD_REGISTRATION = 5,
  DISTANCE_BASED_REGISTRATION = 6
} reg_type_enum;
#endif

typedef PACKED_PREFIX struct
{
  bool                Extension;
  PswSubaddressTypeT  Type;
  bool                Indicator;
  int8            Address[CP_MAX_SUBADDRESS_SIZE + 1];/*Inc NULL terminator*/
} PACKED_POSTFIX  PswSubaddressT;

typedef PACKED_PREFIX struct
{
  bool num_incl;
  bool digitMode;
  PswNumberTypeT    type;
  PswNumberPlanT    plan;
  uint8   NumChar;
  uint8 ms_orig_pos_loc_ind;
  uint8 number[CP_MAX_CALLING_PARTY_NUMBER_SIZE + 1];
} PACKED_POSTFIX PswGlobalEmergencyCall;

typedef PACKED_PREFIX struct
{
  bool num_info_incl;
  PswNumberTypeT    type;
  PswNumberPlanT    plan;
  uint8 numFields;
  uint8 number[CP_MAX_KEYPAD_FACILITY_SIZE+1];
} PACKED_POSTFIX PswExtKeypadFacility;

typedef PACKED_PREFIX struct
{
  bool  Mode;
} PACKED_POSTFIX  PswSetPrivacyModeMsgT;

typedef enum
{
   CALL_MODE_UNKNOWN,
   NORMAL_VOICE,
   EMERGENCY_E911,
   WPS,
   WPS_E911,
   OTASP,
   CALL_MODE_END
} PswCallModeT;

typedef PACKED_PREFIX struct
{
   PswRequestModeT   RequestMode;         /* requested call type          */
   PswCallModeT      CallMode;            /* call type (e.g. emergency, OTASP, WPS ...) */
   PswServiceOptionT ServiceOption;       /* requested service option     */
   bool              Encoded;
   bool              DigitMode;           /* digit encoding indicator     */
   PswNumberTypeT    NumberType;          /* type of number               */
   PswNumberPlanT    NumberPlan;          /* numbering plan               */
   uint8             NumDigits;           /* number of digits to be dialled */
   uint8             OtaspMode;           /* target band or system for OTASP Orig */
   uint8             Digits[CP_MAX_CALLING_PARTY_NUMBER_SIZE +1];/* ascii digits, null terminated string */
   PswCallingPartyNumberT CPNInfo;         /* Calling Party Number Info   */
   PswServiceT       ServiceType;          /* service types.              */
   bool              MultipleCallingPartyNum; /* MS has > 1 calling party num */
   bool              SendCallingPartySubaddr; /* subaddress included?     */
   PswSubaddressT    CallingPartySubaddr;
   bool              SendCalledPartySubaddr;  /* subaddress included?     */
   PswSubaddressT    CalledPartySubaddr;
   uint16            pktDataIncTimer;           /* in seconds*/
   NspeAppT          pktDataAppType;
} PACKED_POSTFIX  PswCallInitiateMsgT;

typedef PACKED_PREFIX struct
{
   bool enabled; /* TRUE: DDTM enabled, FALSE: DDTM disabled */
} PACKED_POSTFIX  PswValSetDDTMMsgT;

typedef PACKED_PREFIX struct
{
   uint32 TimerId;                         /* Expired timer Id */
} PACKED_POSTFIX  PswTimerExpiredMsgT;

typedef PACKED_PREFIX struct
{
   uint32 Mux1Stats[PSW_NUM_MUX1_COUNTER_INDEX];
   uint32 Mux2Stats[PSW_NUM_MUX2_COUNTER_INDEX];
} PACKED_POSTFIX PswRevStatParmsMsgT;

typedef PACKED_PREFIX struct
{
   bool  Encoded;
   uint8 Digit;
} PACKED_POSTFIX  PswSendContDTMFMsgT;

typedef PACKED_PREFIX struct
{
   bool  Encoded;
   uint8 NumDigits;
   uint8 DtmfOnLength;
   uint8 DtmfOffLength;
   uint8 Digits[ CP_MAX_BURST_DTMF_DIGITS + 1 ]; /* Incl NULL terminator */
} PACKED_POSTFIX  PswSendBurstDTMFMsgT;

typedef PACKED_PREFIX struct
{
   PswNumberTypeT Type;
   PswNumberPlanT Plan;
   int8           Number[CP_MAX_CALLED_PARTY_NUMBER_SIZE + 1];/*Inc NULL terminator*/
} PACKED_POSTFIX   PswCalledPartyNumberT;


typedef PACKED_PREFIX struct
{
   bool   SendFeatureIndicator;
   uint8  Feature;
   bool   SendKeypadFacility;
   int8   KeypadFacility[CP_MAX_DISPLAY_SIZE  +  1];  /* Incl NULL terminator */
   bool   SendCalledPartyNumber;
   PswCalledPartyNumberT CalledPartyNumber;
   bool   SendCallingPartyNumber;
   PswCallingPartyNumberT CallingPartyNumber;
   bool   SendConnectedNumber;
   PswCallingPartyNumberT ConnectedNumber;   /* Same format as CallingPartyNumber */
   bool   Send;
   bool   SendCalledPartySubaddress;
   PswSubaddressT CalledPartySubaddress;
   bool   SendCallingPartySubaddress;
   PswSubaddressT CallingPartySubaddress;
   bool   SendConnectedSubaddress;
   PswSubaddressT ConnectedSubaddress;
   bool sendGlobalEmergencyCall;
   PswGlobalEmergencyCall  EmergencyNum;
   bool   sendExtKeypadFacility;
   PswExtKeypadFacility ExtKeypadFac;
#ifdef MTK_CBP
   bool   isEccWith3WC;  /* to indicate that this is a ECC throurh 3WC or not */
#endif
} PACKED_POSTFIX  PswHookflashMsgT;

typedef PACKED_PREFIX struct
{
   uint8 NumFrames;  /* Number of frames spent off the serving frequency */
} PACKED_POSTFIX  PswEndCFMeasureMsgT;

typedef PACKED_PREFIX struct
{
   uint16   EnableBits;
} PACKED_POSTFIX  PswDsEnableMsgT;

typedef enum
{
   PSW_MDR_DISABLED = 0,
   PSW_MDR_IF_AVAIL,
   PSW_MDR_ONLY
} PswPktSvcPrefT;

typedef PACKED_PREFIX struct
{
   uint8           pageAcceptMap;
   uint16          ayncDataSOPref;
   uint16          faxSOPref;
   bool            dormantBySCC;
   uint16          ldrSOPrefer;
   uint16          mdrSOPrefer;
   PswPktSvcPrefT  pktSvcPref;
   uint8           forRc;
   uint8           revRc;
} PACKED_POSTFIX  PswDataPrefReqMsgT;

typedef enum
{
   PSW_PREFER_MUX1 = 1,
   PSW_PREFER_MUX2
} PswMuxPrefMapT;

typedef PACKED_PREFIX struct
{
   uint16           voiceSO;
   PswMuxPrefMapT   MuxOptPref;
   bool             ToNegotiate;
   uint8            forRc;
   uint8            revRc;
} PACKED_POSTFIX  PswVsPrefReqMsgT;

typedef PACKED_PREFIX struct
{
   uint8            pageAccptMap;
   uint16           lpbkSO;
   uint16           markovSO;
   uint16           supplSO;
   bool             ToNegotiate;
   uint8            forRc;
   uint8            revRc;
} PACKED_POSTFIX  PswLsPrefReqMsgT;

typedef PACKED_PREFIX struct
{
  uint16 ServiceOption;
  bool   ToNegotiate;
  uint8  forRc;
  uint8  revRc;
} PACKED_POSTFIX  PswSetServiceConfigMsgT;

/* SNR SR */
typedef PACKED_PREFIX struct
{
   bool  enable;
   uint8 numAttempts;         /* See MAX_SILENT_RETRIES */
   uint8 timeBetweenAttempts; /* See SILENT_RETRY_TIMEOUT */
   uint8 timeBeforeAbort;     /* See SILENT_RETRY_RESET_TIMEOUT */
} PACKED_POSTFIX  PswSetSilentRetryMsgT;

typedef PACKED_PREFIX struct
{
   uint32 CustomFeature;
   /*each bit is defined for every cumtomfeafure */
} PACKED_POSTFIX  PswCustomFeatureMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  PswGetSilentRetryStatusMsgT;

/* Get Active NAM response message */
typedef PACKED_PREFIX struct
{
  bool enable;
} PACKED_POSTFIX  PswGetSilentRetryStatusResponseMsgT;

typedef PACKED_PREFIX struct
{
   bool     E911Mode;  /* TRUE=EN, FALSE=DIS */
} PACKED_POSTFIX  PswE911ModeMsgT;

typedef PACKED_PREFIX struct
{
  uint8 PRev;
} PACKED_POSTFIX  PswSetPRevMsgT;

typedef PACKED_PREFIX struct
{
   NamNumber   namNumber;
} PACKED_POSTFIX  PswGetPrlMsgT;

typedef enum
{
   PSW_PAM_DISABLE,
   PSW_PAM_FAX_NEXT_CALL,
   PSW_PAM_FAX_TILL_PWDN,
   PSW_PAM_DATA_NEXT_CALL,
   PSW_PAM_DATA_TILL_PWDN
} DsPreArangT;

typedef PACKED_PREFIX struct
{
   DsPreArangT paType;
} PACKED_POSTFIX  PswVsDsPreArangMsgT;

typedef PACKED_PREFIX struct
{
   uint8  HATtmrValue;
   uint8  HTtmrValue;
} PACKED_POSTFIX  PswSetPkzidHysterTmrMsgT;

typedef PACKED_PREFIX struct
{
   uint8  reason;
} PACKED_POSTFIX  PswPsCallRelMsgT;

typedef PACKED_PREFIX struct
{
   uint8  ConRef;
   uint16 SrvOpt;
   uint8  Length;
   uint8  CtrlRec[1];
} PACKED_POSTFIX  PswServiceOptionControlRespMsgT;

typedef PACKED_PREFIX struct
{
   uint16  RemainDuration;       /* Remaining RSCH duration as assigned by BS */
} PACKED_POSTFIX  PswRschRelIndMsgT;

/* RLP Message Definitions */
typedef PACKED_PREFIX struct
{
   uint8        SrId;
   uint16       DataSize;
} PACKED_POSTFIX  PswDataBufferStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint8        SoPref;
   uint8        forRc;
   uint8        revRc;
} PACKED_POSTFIX  PswSmsPrefReqMsgT;

typedef PACKED_PREFIX struct
{
  uint8  DataByte[PSW_MAX_SMS_SIZE];   /* contents of SMS.     */
  uint16 len;        /* the length of data in DataByte[ ]      */
  bool   confirm;    /* confirm-required indocator             */
  uint8  Digits[CP_MAX_CALLING_PARTY_NUMBER_SIZE +1]; /* ascii
                                 digits, null terminated string*/
  bool   IsE911;    /* IsE911         */
} PACKED_POSTFIX  PswSmsTxReqMsgT;
typedef PACKED_PREFIX struct
{
  UINT8  len;   /* contents of SMS.     */
  UINT8  addr[PSW_MAX_SMS_SIZE/4]; /* addr*/
} PACKED_POSTFIX  PswSmsBroadcastExpectedMsgT;
typedef PACKED_PREFIX struct
{
  UINT8  len;   /* contents of SMS.     */
  UINT8  data[PSW_MAX_SMS_SIZE/2]; /* data*/
} PACKED_POSTFIX  PswSmsMtDataBurstMsgT;


typedef PACKED_PREFIX struct
{
   bool         Success;
} PACKED_POSTFIX  PswOtaspRcvdMsgT;

typedef PACKED_PREFIX struct
{
   uint16       Length;
   uint8*       Data;
} PACKED_POSTFIX  PswOtaspTxReqMsgT;

typedef PACKED_PREFIX struct
{
   uint16       SoPref;
   uint8        forRc;
   uint8        revRc;
} PACKED_POSTFIX  PswOtaspSoPrefMsgT;

#ifdef MTK_CBP
typedef PACKED_PREFIX struct
{
   uint8        mode;
} PACKED_POSTFIX  PswOtaSetPowerUpModeMsgT;
#endif

typedef PACKED_PREFIX struct
{
   uint16       NewSo;
} PACKED_POSTFIX  PswVsSwitchSoMsgT;

typedef PACKED_PREFIX struct
{
   NamNumber   namNumber;
} PACKED_POSTFIX  PswGetNamMsgT;

typedef PACKED_PREFIX struct
{
   DbmCPSegmentT dataBaseId;
} PACKED_POSTFIX  PswNvmChangedByEtsMsgT;

typedef PACKED_PREFIX struct
{
   NamNumber namNumber;
   bool      ScmIncl;
   uint8     Scm[NAM_BANDS_SUPPORTED];
   bool      SlotCycleIncl;
   uint8     SlotCycleIndex;
   bool      LckRsnIncl;
   uint8     LckRsn;
   bool      MaintRsnIncl;
   uint8     MaintRsn;
   bool      prefServiceIncl;
   uint8     prefBand;
   uint8     prefMode;
   uint8     prefBlockBand1;
   uint8     prefServBand0;
   bool      ImsiMIncl;
   bool      ImsiMValid;
   IMSIType  IMSI_M;

   bool      SidNidIdexIncl;
   uint8     SidNidIdex;
   bool      SidIncl;
   uint16    Sid;
   bool      NidIncl;
   uint16    Nid;
   bool      AccolcIncl;
   uint8     Accolc;
   bool      TermInfoIncl;
   uint8     MobTermHome;
   uint8     MobTermSid;
   uint8     MobTermNid;
   bool      SpcIncl;
   uint32    Spc;
   bool      MdnIncl;
   uint8     MdnNumDigits;
   uint8     Mdn[16];
   bool      AllowOtapaIncl;
   bool      AllowOtapa;
   bool      AnalogParmIncl;
   uint16    HomeSid;
   uint16    Sids;
   uint16    LocaIds;
   bool      Puregs;
   bool      systemSelectIncl;
   uint8     SystemSelect;

   bool      ImsiTIncl;
   bool      ImsiTValid;
   IMSIType  IMSI_T;

   bool      CpcaCscaIncl;
   uint16    CPCA, CSCA;      /* CDMA Primary (and Secondary) Channel A   */

   bool      CpcbCscbIncl;
   uint16    CPCB, CSCB;      /* CDMA Primary (and Secondary) Channel B   */

} PACKED_POSTFIX  PswNamChangedMsgT;

typedef PACKED_PREFIX struct
{
  uint8 msResult[64];
  UINT8 NumBytes;
} PACKED_POSTFIX  PswModExpMSResultMsgT;

typedef PACKED_PREFIX struct
{
      uint8 otaK[8];
} PACKED_POSTFIX  PswModExpBSResultMsgT;

typedef PACKED_PREFIX struct
{
   bool SSPRListFillOK;
} PACKED_POSTFIX  PswFillSSPRRspMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 *IS683Msg;
   UINT16 Length;
} PACKED_POSTFIX  PswIOTAIS683MsgT;

typedef PACKED_PREFIX struct
{
   UINT8 *IOTAPrl;
   UINT16 Length;
} PACKED_POSTFIX  PswIOTAPRLMsgT;

/* from MMI - SMS related */
#ifdef SYS_OPTION_SMS_ENGINE

typedef PACKED_PREFIX struct
{
   PswServiceOptionT SrvOpt;
} PACKED_POSTFIX  PswSmsPrefSrvOptMsgT;

typedef PACKED_PREFIX struct
{
   bool  DigitMode;
   bool  NumberMode;
   uint8 NumberType;
   uint8 NumberPlan;
   uint8 NumFields;
   uint8 Char[ SMS_MAX_ADDRESS_CHARS ];
} PACKED_POSTFIX  PswSmsMCAddrT;

typedef PACKED_PREFIX struct
{
   uint8 Type;
   bool  Odd;
   uint8 NumFields;
   uint8 Char[ SMS_MAX_SUBADDRESS_CHARS ];
} PACKED_POSTFIX  PswSmsMCSubaddrT;

typedef PACKED_PREFIX struct
{
   uint16 Category;
   uint8  Language;
   uint8  CategoryResult;
} PACKED_POSTFIX  PswCategoryT;

typedef PACKED_PREFIX struct
{
   uint8 NumCategories;
   PswCategoryT Cat[ SMS_MAX_NUM_SERVICE_CATEGORIES ];
} PACKED_POSTFIX  PswSrvCatProgResultT;

typedef PACKED_PREFIX struct
{
   uint8 DispMode;
   uint8 reserved;
} PACKED_POSTFIX  PswDispModeT;

typedef PACKED_PREFIX struct
{
   uint16 TeleSrvId;                       /* Teleservice ID                  */
   PswSmsMCAddrT DefaultDestAddr;          /* Message center address of all   */
                                           /* submitted messages              */
   bool DefaultSubaddrPres;                /* Indicator if subaddress present */
   PswSmsMCSubaddrT DefaultSubaddr;        /* Message center subaddress       */
} PACKED_POSTFIX  PswSmsConnectMsgT;

typedef PACKED_PREFIX struct
{
   uint16 TeleSrvId;                       /* Teleservice ID                  */
} PACKED_POSTFIX  PswSmsDisconnectMsgT;

/*
** PSW_GET_STAT_CNT_MSG,
*/
typedef PACKED_PREFIX struct
{
   StatCounterT   StatCounter;         /* Statistics counter */
   uint8          NumElement;          /* Number of elements in StatCounter   */
   uint32         *MuxStat;
   #ifdef MTK_PLT_ON_PC_UT
   uint32         StatCnt[255];
   #endif
} PACKED_POSTFIX  PswStatCntMsgT;

#define PSW_SMS_MAX_USER_DATA_CHARS 256

typedef PACKED_PREFIX struct
{
   uint8 Year;
   uint8 Month;
   uint8 Day;
   uint8 Hours;
   uint8 Minutes;
   uint8 Seconds;
} PACKED_POSTFIX  PswSmsAbsTimeT;

typedef enum
{
   PSW_CAT_UNKNOWN,
   PSW_CAT_EMERGENCIES,
   PSW_CAT_ADMINISTRATIVE,
   PSW_CAT_MAINTENANCE,
   PSW_CAT_GEN_NEWS_LOCAL,
   PSW_CAT_GEN_NEWS_REGIONAL,
   PSW_CAT_GEN_NEWS_NATIONAL,
   PSW_CAT_GEN_NEWS_INTERNATIONAL,
   PSW_CAT_FINANCE_NEWS_LOCAL,
   PSW_CAT_FINANCE_NEWS_REGIONAL,
   PSW_CAT_FINANCE_NEWS_NATIONAL,
   PSW_CAT_FINANCE_NEWS_INTERNATIONAL,
   PSW_CAT_SPORTS_NEWS_LOCAL,
   PSW_CAT_SPORTS_NEWS_REGIONAL,
   PSW_CAT_SPORTS_NEWS_NATIONAL,
   PSW_CAT_SPORTS_NEWS_INTERNATIONAL,
   PSW_CAT_ENTERTAINMENT_NEWS_LOCAL,
   PSW_CAT_ENTERTAINMENT_NEWS_REGIONAL,
   PSW_CAT_ENTERTAINMENT_NEWS_NATIONAL,
   PSW_CAT_ENTERTAINMENT_NEWS_INTERNATIONAL,
   PSW_CAT_LOCAL_WEATHER,
   PSW_CAT_TRAFFIC,
   PSW_CAT_FLIGHT_SCHEDULE,
   PSW_CAT_RESTAURANTS,
   PSW_CAT_LODGINGS,
   PSW_CAT_RETAIL_DIRECTORY,
   PSW_CAT_ADVERTISEMENT,
   PSW_CAT_STOCK_QUOTES,
   PSW_CAT_EMPLOYMENT_OPPORTUNITIES,
   PSW_CAT_MEDICAL_HEALTH,
   PSW_CAT_TECH_NEWS,
   PSW_CAT_MULTI_CATEGORY
} PSW_SMS_SERVICE_CAT;

typedef enum
{
   PSW_NORMAL,
   PSW_INTERACTIVE,
   PSW_URGENT,
   PSW_EMERGENCY
} PSW_SMS_PRIORITY;

typedef enum
{
   PSW_SMS_NOT_RESTRICTED,
   PSW_SMS_RESTRICTED,
   PSW_SMS_CONFIDENTIAL,
   PSW_SMS_SECRET
} PSW_SMS_PRIVACY;

typedef enum
{
   PSW_SMS_MOBILE_DEFAULT,
   PSW_SMS_LOW_PRIORITY,
   PSW_SMS_MEDIUM_PRIORITY,
   PSW_SMS_HIGH_PRIORITY
} PSW_SMS_ALERT;

typedef enum
{
   PSW_SMS_UNKNOWN,
   PSW_SMS_ENGLISH,
   PSW_SMS_FRENCH,
   PSW_SMS_SPANISH,
   PSW_SMS_JAPANESE,
   PSW_SMS_KOREAN,
   PSW_SMS_CHINESE,
   PSW_SMS_HEBREW
} PSW_SMS_LANG;

typedef PACKED_PREFIX struct
{
  uint8 ErrorClass;
  uint8 MsgStatusCode;
} PACKED_POSTFIX  PSW_MSG_STATUS;

typedef enum
{
   PSW_SMS_DELIVERY_ACK = 4,
   PSW_SMS_USER_ACK,
   PSW_SMS_READ_ACK
} PSW_MSG_TYPE;

typedef PACKED_PREFIX struct
{
   uint16 TeleSrvId;                       /* Teleservice ID                  */
   uint16 SeqNum;                          /* Unique number to coordinate the */
                                           /* parameter and user data messages*/
   bool SendOnTraffic;                     /* Force SMS on Traffic or let CP  */
                                           /* decide                          */

   bool SrvCatPres;                        /* If service catagory is present  */
   PSW_SMS_SERVICE_CAT SrvCat;             /* Service Catagory                */
   uint16 MsgId;                           /* Message ID of submitted message */
   bool HeaderInd;                         /* Header Indicator                */
   bool BearerReplyOptPres;
   uint8 ReplySeq;
   uint8 NumUserDataMsgs;                  /* Number of User data messages    */
   bool AbsValPres;                        /* Absolute validity period present*/
   PswSmsAbsTimeT AbsVal;                  /* Absolute validity period        */
   bool RelValPres;                        /* Relative validity period present*/
   uint8 RelVal;                           /* Relative validity period        */
   bool AbsDelPres;                        /* Absolute deferred delivery time */
                                           /* present                         */
   PswSmsAbsTimeT AbsDel;                  /* Absolute deferred delivery time */
   bool RelDelPres;                        /* Relative deferred delivery time */
                                           /* present                         */
   uint8 RelDel;                           /* Relative deferred delivery time */
   bool PriPres;                           /* Priority indicator present      */
   PSW_SMS_PRIORITY Pri;                   /* Priority indicator              */
   bool PrvPres;                           /* Privacy indicator present       */
   PSW_SMS_PRIVACY Prv;                    /* Privacy indicator               */
   bool ReplyOptPres;                      /* If reply option is present      */
   bool UserAck;                           /* User acknowledgement requested  */
   bool DeliverAck;                        /* delivery acknowledgement        */
                                           /* requested from message center   */
   bool ReadAck;                           /* read acknowledgement            */
   bool AlertPres;                         /* Alert on message delivery present*/
   PSW_SMS_ALERT Alert;                    /* Alert on message delivery       */
   bool LangPres;                          /* Language indicator present      */
   PSW_SMS_LANG Lang;                      /* Language indicator              */
   bool CallbackNumPres;                   /* Callback number present         */
   PswSmsMCAddrT CallbackNum;              /* Callback number                 */
   bool OverrideAddrPres;                  /* Override Address Present        */
   PswSmsMCAddrT OverrideAddr;             /* Override Address                */
   bool OverrideSubaddrPres;               /* Override Subaddress Present     */
   PswSmsMCSubaddrT OverrideSubaddr;       /* Override Subaddress             */
   bool MsgDepIndexPres;                   /* Message Deposit Index present   */
   uint16 MsgDepIndex;                     /* Message Deposit Index           */
   bool SrvCatProgResultPres;              /* Service Category Program Results*/
   PswSrvCatProgResultT SrvCatProgResult;  /* Service Category Program Results*/
   bool DispModePres;                      /* Service Category Program Results*/
   PswDispModeT DispMode;                  /* Service Category Program Results*/
} PACKED_POSTFIX  PswSmsSubmitParmsMsgT;

typedef PACKED_PREFIX struct
{
   uint16 SeqNum;
   uint8  MsgType;
   uint8  NumMsgs;
   uint8  MsgEncoding;
   uint8  NumFields;
   uint8  Char[ SMS_MAX_USERDATA_LENGTH ]; /* User Data field of message      */
} PACKED_POSTFIX  PswSmsUserDataMsgT;

typedef PACKED_PREFIX struct
{
   uint16 TeleSrvId;                       /* Teleservice ID                  */
   uint16 MsgId;                           /* Message ID of submitted message */
   bool   HeaderInd;                       /* Header Indicator                */
   bool OverrideAddrPres;                  /* Override address present        */
   PswSmsMCAddrT OverrideAddr;             /* Override address                */
   bool SubaddrPres;                       /* Subaddress Present              */
   PswSmsMCSubaddrT Subaddr;               /* Subaddress                      */
   bool SendSmsCancelDb;                   /* Send SMS Cancel Databurst if needed */
   uint16 SeqNum;
} PACKED_POSTFIX  PswSmsCancelMsgT;

typedef PACKED_PREFIX struct
{
   uint16 TeleSrvId;                       /* Teleservice ID                  */
   uint16 SeqNum;                          /* Unique number to cordinate the  */
                                           /* parameters and user data messages*/
   PswSmsMCAddrT Addr;                     /* Message ceneter address         */
   bool SubaddrPres;                       /* Subaddress Present              */
   PswSmsMCSubaddrT Subaddr;               /* Subaddress                      */
   uint16 MsgId;                           /* Message ID of submitted message */
   PSW_MSG_TYPE MsgType;                   /* User/Delivery/Read ack type     */
   bool HeaderInd;                         /* Header Indicator                */
   uint8 NumUserDataMsgs;                  /* Number of User data messages    */
   bool RespCodePres;                      /* User response code present      */
   uint8 RespCode;                         /* User response code              */
   bool TimeStampPres;                     /* Time Stamp present              */
   PswSmsAbsTimeT TimeStamp;               /* Message Center Time Stamp       */
   bool MsgDepIndexPres;                   /* Message Deposit Index present   */
   uint16 MsgDepIndex;                     /* Message Deposit Index           */
   bool MsgStatusPres;                     /* Message Status present          */
   PSW_MSG_STATUS MsgStatus;               /* Message Status                  */
} PACKED_POSTFIX  PswSmsAckMsgT;

typedef PACKED_PREFIX struct
{
   uint32 SvcMask;  /* Refer to scc_msc.c / mscGetSvcBitMap() for mapping */
   uint32 LangMask; /* Refer to scc_msc.c / mscGetLangBitMap() for mapping */
   PSW_SMS_PRIORITY Priority;
} PACKED_POSTFIX  PswSmsBCastParmsMsgT;

#if defined(LGT_EXTENSIONS)
typedef PACKED_PREFIX struct
{
   uint32 Param1;
   uint32 Param2;
   PSW_SMS_PRIORITY Priority;
} PACKED_POSTFIX  PswSmsUnknownParmsMsgT;
#endif

typedef enum
{
   PSW_NO_TERMINAL_PROBLEMS,
   PSW_DESTINATION_RESOURSE_SHORTAGE,
   PSW_DESTINATION_OUT_OF_SERVICE
}PswTermStatusT;

typedef PACKED_PREFIX struct
{
   PswTermStatusT Status;
} PACKED_POSTFIX  PswSmsTerminalStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint8 NumTempRetries;   /* Max # of retries if BS returns errorCode = Temporary */
   uint8 NumPermRetries;   /* Max # of retries if BS returns errorCode = Permanent */
   uint8 NumTimeoutRetries;/* Max # of retries if no BS SMS Ack received and timeout
                              occurs */
} PACKED_POSTFIX  PswSmsRetxAmountMsgT;

typedef PACKED_PREFIX struct
{
   uint8 ErrorClass;
   uint8 CasueCode;
} PACKED_POSTFIX  PswSmsCauseCodeMsgT;

typedef PACKED_PREFIX struct
{
   uint16 SeqNum;
   uint8 ErrorClass;
   uint8 CauseCode;
   bool SendTlAck;
} PACKED_POSTFIX  PswSmsReplyTlAckMsgT;

 typedef PACKED_PREFIX struct
 {
 	bool MultiSMSMode;
 } PACKED_POSTFIX PswSMSModeMsgT;
 /*
	 This primitive is sent from md3-val to md3-psw to reply an tlack when receives SMS MT message.
 */
#ifdef MTK_CBP
 typedef struct
 {
	 uint8 len;
	 uint8 Pdu[MAX_SMS_ACK_PDU_LEN];
     uint16 seqNum;
 }PswSMSTlackReqMsgT;


typedef struct {
    bool    CbsState;                           /* FALSE, CBS off; TRUE, CBS on */
    uint32  LanMask;                            /* lanuage bitmap */
    uint32  ChaMask[PSW_SMS_CBS_CHA_MAX];       /* channels bitmap */
    uint32  CmasMask;                           /* cmas bitmap */
} PswSmsCbsInfoT;

#endif
#endif

/*-------------------------------------------------------------------**
  PSW_POS_AFLT_MEASUREMENTS_MSG
  PswPosAfltMeasurementMsgT
  L1D reports various position AFLT pilot set list information needed
  by BS. This list is not necessarily a complete list of position AFLT
  pilot set list but only those measurements that are completed as of
  the time the message is generated.
 *-------------------------------------------------------------------**/

typedef PACKED_PREFIX struct
{
   uint16  PilotPN;
   bool    Stale;
   /* PILOT_PN_PHASE sent in PROVIDE PILOT PHASE MEASUREMENTS */
   int32   PnPhase;
   /* PILOT_STRENGTH sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16  Strength;
   /* RMS_ERR_PHASE sent in PROVIDE PILOT PHASE MEASUREMENTS */
} PACKED_POSTFIX  AFLTPilotMeasurementT;

typedef PACKED_PREFIX struct
{
   uint8 SeqNum;
   /* TIME_REF_MS sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16 TimeRefMs;
   bool OffsetIncl;
   int16 MobOffset;
   /* REF_PN sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16 RefPN;
   /* REF_PILOT_STRENGTH sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16 RefPilotStrength;
   /* NUM_PILOTS_P sent in PROVIDE PILOT PHASE MEASUREMENTS */
   /* actual number of pilot measurements in AFLTPilotMeasurement */
   uint8  NumPilots;
   AFLTPilotMeasurementT PosAFLT[SYS_MAX_AFLT_LIST_PILOTS];
} PACKED_POSTFIX  PswPosAfltMeasurementMsgT;

/*-------------------------------------------------------------------**
  PSW_POS_AFLT_ACK_ABORT_MSG
  PswPosAfltAckAbortMsgT
  L1D, after receiving L1D_POS_AFLT_ABORT_MSG, acknowledges its
  position search measurements aborted for given sequence number.
 *-------------------------------------------------------------------**/

typedef PACKED_PREFIX struct
{
  uint16 SeqNum;
} PACKED_POSTFIX  PswPosAfltAckAbortMsgT;

/*-------------------------------------------------------------------**
  PSW_POS_AFLT_ABORT_REQ_MSG
  PswPosAfltAbortReqMsgT
  L1D, requests for position search measurements aborted for given
  sequence number.
 *-------------------------------------------------------------------**/

typedef PACKED_PREFIX struct
{
  uint16 SeqNum;
} PACKED_POSTFIX  PswPosAfltAbortReqMsgT;

/* mclee location services request element messages */
typedef struct
{
   uint8 SessID;
   uint8 RejReqType; /* BS Request Element type */
   bool RejReason;
} PswPosRejectMsgT;

typedef PACKED_PREFIX struct
{
   uint8 RecLen;
   uint8 ProvPseudorangeRec[PRM_MAX_SIZE];
} PACKED_POSTFIX  PswPseudorangeMsmtRspMsgT;

typedef PACKED_PREFIX struct
{
   uint8       SoPref;
   uint8       forRc;
   uint8       revRc;
} PACKED_POSTFIX  PswLocPrefReqMsgT;

typedef PACKED_PREFIX struct
{
   bool  Enable;
   uint8 N1;
} PACKED_POSTFIX  PswGPSEnableReportsMsgT;

typedef PACKED_PREFIX struct
{
   uint16 Size;
   uint8    Sap;
   uint8  DataP[200];
} PACKED_POSTFIX  PswIs801TcpbFwdDataRespInfoMsgT;

typedef PACKED_PREFIX struct
{
   uint8 SessionId;
} PACKED_POSTFIX  PswIs801TcpBRevDataRespMsgT;


typedef enum
{
   PSW_GPS_ASSISTANCE_DATA_ALMANAC,
   PSW_GPS_ASSISTANCE_DATA_EPHEMERIS,
   PSW_GPS_ASSISTANCE_DATA_LAST_LOCATION,
   PSW_GPS_ASSISTANCE_DATA_BASE_STATION_ALMANAC,
   PSW_GPS_ASSISTANCE_DATA_ALMANAC_CORRECTION,
   PSW_GPS_ASSISTANCE_DATA_SV_HEALTH_INFO,
   PSW_GPS_ASSISTANCE_DATA_NAVIGATION_MSG_BITS,
   PSW_GPS_ASSISTANCE_DATA_ALL_GPS_DATA
}IS801GpsResetAssistanceDataT;

typedef PACKED_PREFIX struct
{
  IS801GpsResetAssistanceDataT AssisData;
} PACKED_POSTFIX PswGpsResetAssistMsgT;

typedef struct
{
   uint16 DataLen;
   uint8  Data[PSW_GPS7560_DATA_SIZE_MAX];
} PswGpsNmeaStreamMsgT;

typedef PACKED_PREFIX struct
{
  SysSystemTimeT  SendTime;
} PACKED_POSTFIX  PswL1dAccessPreambleCompleteMsgT;

typedef PACKED_PREFIX struct
{
  SysSystemTimeT  SendTime;
} PACKED_POSTFIX  PswL1dAccessCompleteMsgT;

typedef PACKED_PREFIX struct
{
  SysSystemTimeT  finishTime;
} PACKED_POSTFIX  PswL1dAccessProbeCompleteMsgT;

typedef struct {
   HwdSupportedCDMABandT *SupportedBands;
}PswBandSupportedQueryRspMsg;

/****************/
/* ETS Messages */
/****************/

typedef PACKED_PREFIX struct
{
   PswTestModeT Mode;                      /* Test mode */
   uint16       Enabled;                   /* Mode Qualifier, typically equivalent to ON/OFF */
   uint16       Value;                     /* Mode value*/
} PACKED_POSTFIX  PswEngineTestModeMsgT;


typedef PACKED_PREFIX struct
{
   bool TimersUsed;                        /* Timers turned (on = TRUE) or (off = FALSE) */
} PACKED_POSTFIX  PswTestModeTimersUsedMsgT;

typedef PACKED_PREFIX struct
{
    uint32            MaxCapSize;          /* System time to start sending message */
    uint16            power_correction;    /* Tx power level less mean input power */
    bool              use_curr_mip;        /* Use current mean input power or not  */
    uint8             data_len;            /* Length of data to be sent            */
    uint8             data[30];            /* l3 message data, length, data, crc   */
    bool              repeat_data;         /* Message data is repeated flag        */
    uint8             lc_mask[6];          /* Access channel long code mask        */
    uint8             pam_size;            /* Preamble size in frames              */
    uint16            RN;                  /* Random chip delay : 0 - 511          */
} PACKED_POSTFIX  PswAccessSigMsgT;

#ifdef MTK_PLT_ON_PC_UT
/* For PSW UT   */
typedef struct
{
    uint8 lengthInBits;
    uint8 reverseSigData[CPBUF_SIZE_REV];
}PswUnitTestReverseSigMsgT;
typedef struct
{
    BOOL paramPsServStatusPresent;
    BOOL paramIratModePresent;
    BOOL psRegistered;
    srlteRatTypeT c2kRatType;
    ValIratModeT iratMode;
}PswSetIratParamMsgT;
typedef struct
{
    BOOL success;
    BOOL resend;
}PswSendSmsConfirmMsgT;

typedef struct
{
    c2k_sbp_id_enum sbpId;
}PswSetSbpIdMsgT;

#endif

/* Status request message - Request current PS status */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;                    /* Response routing information */
} PACKED_POSTFIX  PswStatusRequestMsgT;

/* Status request, response */
typedef PACKED_PREFIX struct
{
   uint32   Esn;
   uint8    RFMode;
   uint8    MIN_Digits;
   uint8    Min[15];
   uint8    CPState;
   uint8    CDMAGoodFrames;
   uint16   AMPSGoodFrames;
   uint16   AMPSBadFrames;
   uint8    CPEntranceReason;
   uint16   Channel;
   uint16   PilotPN;
   uint16   CurSID;
   uint16   CurNID;
   uint16   LocAreaID;
   int16    RSSI;
   uint8    PowerLevel;
   bool     Slotted_Mode_Flag;
} PACKED_POSTFIX  PswStatusResponseMsgT;

/* Debug Pilot Strength request message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;                    /* Response routing information */
   bool enable;
} PACKED_POSTFIX  PswDebugPilotStrengthMsgT;

typedef struct
{
  DebugPilotInfo *PilotReport;
} PswDebugPilotStrengthRspMsgT;

/* Debug Screen request message - Request debug screen info */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;                    /* Response routing information */
} PACKED_POSTFIX  PswDebugScrnRequestMsgT;

/* Debug Screen Response */
typedef struct
{
   uint16          sid;
   uint16          nid;
   uint16          badframes;
   uint16          totframes;
   uint16          ActPilotPN   [SYS_CP_MAX_ACTIVE_LIST_PILOTS];
   uint16          ActPilotEcIo [SYS_CP_MAX_ACTIVE_LIST_PILOTS];
   uint16          CandPilotPN  [SYS_CP_MAX_CANDIDATE_LIST_PILOTS];
   uint16          CandPilotEcIo[SYS_CP_MAX_CANDIDATE_LIST_PILOTS];
   uint16          NghbrPilotPN [SYS_CP_MAX_NEIGHBOR_LIST_PILOTS];
   uint16          NghbrPilotEcIo [SYS_CP_MAX_NEIGHBOR_LIST_PILOTS];
   int16           txPower;
   int16           rxPower;
   uint8           current_slot_cycle_index;
   bool            slotted_enabled;
   bool            qpch_enabled;
   uint8           forfchRc;
   uint8           revfchRc;
   uint16          serviceOption;
   PswServiceT     serviceType;
} PswDebugScrnResponseT;

/* Phone State request message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;                    /* Response routing information */
} PACKED_POSTFIX  PswPhoneStateRequestMsgT;

/* Phone State request, response */
typedef PACKED_PREFIX struct
{
  uint8       State;
  uint32      total_msg;        /* Number of paging ch messages received  */
  uint32      error_msg;        /* Number of paging ch messages with bad crc  */
  uint16      acc_1;             /* number of layer 3 requests messages generated */
  uint16      acc_2;             /* number of layer 3 response messages generated */
  uint16      acc_8;             /* number of unsuccessful access attempts        */
  uint16      dpchLoss_count;         /* Count of paging ch declared    */
  uint16      dtchLoss_count;         /* Count of dedicated traffic CH Loss*/
  uint32      idleHO_count;           /* Count of idle handoff*/
  uint16      hardHO_count;           /* Count of hard handoff*/
  uint16      interFreqIdleHO_count; /* Count of inter_Freq idle handoff*/
  uint16      MO_count;             /* Count of successful MS ORIGINATED      */
  uint16      MT_count;             /* Count of successful MS TERMINATED      */
  uint16      powerDownReg_count;  /* Count of power down registration      */
  uint16      timeBasedReg_count;  /* Count of time based registration       */
  uint16      parameterReg_count;  /* Count of parameter change registration   */
  uint16      powerUpReg_count;    /* Count of power up registration    */
  uint16      orderedReg_count;    /* Count of ordered registration     */
  uint16      zoneBasedReg_count;  /* Count of zone based registration */
  uint16      distanceBasedReg_count; /* Count of distance based registration */
  uint16      silentryRetryTimeout_count; /* Count of silentry retry timeout */
  uint16      T40_count;  /* Count of T40m timeout */
  uint16      T41_count;  /* Count of T41m timeout */
  uint16      T50_count;  /* Count of T50m timeout */
  uint16      T51_count;  /* Count of T51m timeout */
  uint16      T52_count;  /* Count of T52m timeout */
  uint16      T55_count;  /* Count of T55m timeout */
} PACKED_POSTFIX  PswPhoneStateResponseMsgT;

/* Set Retreivable Statistics Parameter Request Type */
typedef PACKED_PREFIX struct
{
    uint16  ParameterID;
    uint32 Value;
} PACKED_POSTFIX  PswSetRetrievableParameterMsgT;

/* Get Retreivable Statistics Parameter Request Type */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   StatCounterT   StatCounter;         /* Statistics counter */
} PACKED_POSTFIX  PswGetRetrievableParameterMsgT;

/* Get Retreivable Statistics Parameter Response Message Type */
typedef PACKED_PREFIX struct
{
    StatCounterT  StatCounter;         /* Statistics counter */
    UINT32        Value[MAX_STAT_ELEMENT];
} PACKED_POSTFIX  GetRetrievableParameterRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;                    /* Response routing information */
} PACKED_POSTFIX  PswGetFullSysTimeMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;                    /* Response routing information */
} PACKED_POSTFIX  PswGetPRevInUseRequestMsgT;

typedef PACKED_PREFIX struct
{
   uint8  P_Rev_In_Use;
} PACKED_POSTFIX  PswGetPRevInUseResponseMsgT;

/* aak_akey */
#ifdef PSW_AKEY_UINT64
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;                    /* Response routing information */
  uint64      akey;
  uint32      checksum;
} PACKED_POSTFIX  PswEnterAKeyMsgT;
#else
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;                    /* Response routing information */
  uint32      akey_low;
  uint32      akey_high;
  uint32      checksum;
} PACKED_POSTFIX  PswEnterAKeyMsgT;
#endif

typedef PACKED_PREFIX struct
{
   uint8 NoiseFloor;
   uint8 ThrMin;
   uint8 MinAct;
   uint8 ThrSlope;
} PACKED_POSTFIX  PswSetAccessHOParmsMsgT;

typedef PACKED_PREFIX struct
{
   uint8 ThrMinImmed;
   uint8 MinActImmed;
   uint8 ThrSlopeImmed;
   uint8 ThrMinDelayed;
   uint8 MinActDelayed;
   uint8 ThrSlopeDelayed;
   uint8 DelayCount;
   uint8 MinNghbrThr;
} PACKED_POSTFIX  PswSetIdleHOParmsMsgT;

typedef PACKED_PREFIX struct
{
   bool PiStatus;
} PACKED_POSTFIX  PswQpchPiStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint8   nomPwrExt;
   int8    nomPwr;
   uint8   numPreamble;
   uint8   bandClass;
   uint16  cdmaFreq;
   uint16  pilotPN;
   uint8   codeChanFCH;
} PACKED_POSTFIX  PswInjectHandoffMsgT;

/*****************************************************************************
 * Defines
 ****************************************************************************/
typedef enum
{
  PSW_UI_ENCRYP_DISABLED,
  PSW_UI_ORYX_ENABLED
} PSW_UI_ENCRYPT_MODE_TYPE;

typedef enum
{
   PSW_BASIC_ENCRYPT_SUPP,
   PSW_BASIC_AND_ENHANCED_SUPP
} PSW_ENCRYPT_MODE_TYPE;

/* define set of frame sizes */
typedef enum
{
   PSW_DCCH_FS_20MS = 1,          /* 20 ms frame size only         */
   PSW_DCCH_FS_5MS = 2,           /* 5 ms frame size only          */
   PSW_DCCH_FS_5MS_AND_20MS = 3   /* both 5 and 20 ms frame size   */
} PSW_DCCH_FRAME_SIZE_TYPE;

typedef enum
{
   PSW_FCH_FS_20MS = 0,
   PSW_FCH_FS_5MS_AND_20MS = 1
} PSW_FCH_FRAME_SIZE_TYPE;

/* define set of RC settings */
typedef enum
{
   PSW_FOR_RC_1 = 0x100,
   PSW_FOR_RC_2 = 0x80,
   PSW_FOR_RC_3 = 0x40,
   PSW_FOR_RC_4 = 0x20,
   PSW_FOR_RC_5 = 0x10,
   PSW_FOR_RC_6 = 0x8,
   PSW_FOR_RC_7 = 0x4,
   PSW_FOR_RC_8 = 0x2,
   PSW_FOR_RC_9 = 0x1
} PSW_FOR_RC_TYPE;

typedef enum
{
   PSW_REV_RC_1 = 0x20,
   PSW_REV_RC_2 = 0x10,
   PSW_REV_RC_3 = 0x8,
   PSW_REV_RC_4 = 0x4,
   PSW_REV_RC_5 = 0x2,
   PSW_REV_RC_6 = 0x1
} PSW_REV_RC_TYPE;

/* define set of RC Preference settings */
typedef enum
{
   PSW_RC_PREF_1 = 1,
   PSW_RC_PREF_2 = 2,
   PSW_RC_PREF_3 = 3,
   PSW_RC_PREF_4 = 4,
   PSW_RC_PREF_5 = 5,
   PSW_RC_PREF_6 = 6,
   PSW_RC_PREF_7 = 7,
   PSW_RC_PREF_8 = 8,
   PSW_RC_PREF_9 = 9
} PSW_RC_PREF_TYPE;

/* define set of Geo-location support */
typedef enum
{
   PSW_GL_NO_SUPPORT = 0,
   PSW_GL_AFLT = 1,
   PSW_GL_AFLT_AND_GPS = 2,
   PSW_GL_GPS = 3
} PSW_GEO_LOC_TYPE;

typedef enum
{
   PSW_GATING_RATE_SET_00  =  0x00,     /* Gating rates 1                */
   PSW_GATING_RATE_SET_01  =  0x01,     /* Gating rates 1 and 1/2        */
   PSW_GATING_RATE_SET_10  =  0x02,     /* Gating rates 1, 1/2 and 1/4   */
   PSW_GATING_RATE_SET_11  =  0x03      /* reserved                      */
} PSW_GATING_RATE_SET_TYPE;


/* FOR SCH Multiplex options --------------------------------------- */
typedef enum
{
   PSW_FOR_SCH_0x03_MUX_OPTION  = 0x0001,   /* For SCH 0x03 Mux options,  bit,   */
   PSW_FOR_SCH_0x809_MUX_OPTION = 0x0002,   /* For SCH 0x809 Mux options, bit,   */
   PSW_FOR_SCH_0x811_MUX_OPTION = 0x0004,   /* For SCH 0x811 Mux options, bit,   */
   PSW_FOR_SCH_0x821_MUX_OPTION = 0x0008,   /* For SCH 0x821 Mux options, bit,   */
   PSW_FOR_SCH_0x905_MUX_OPTION = 0x0010,   /* For SCH 0x905 Mux options, bit,   */
   PSW_FOR_SCH_0x909_MUX_OPTION = 0x0020,   /* For SCH 0x909 Mux options, bit,   */
   PSW_FOR_SCH_0x911_MUX_OPTION = 0x0040,   /* For SCH 0x911 Mux options, bit,   */
   PSW_FOR_SCH_0x921_MUX_OPTION = 0x0080,   /* For SCH 0x921 Mux options, bit,   */

   PSW_FOR_SCH_0x04_MUX_OPTION  = 0x0100,   /* For SCH 0x04 Mux options,  bit,   */
   PSW_FOR_SCH_0x80a_MUX_OPTION = 0x0200,   /* For SCH 0x80a Mux options, bit,   */
   PSW_FOR_SCH_0x812_MUX_OPTION = 0x0400,   /* For SCH 0x812 Mux options, bit,   */
   PSW_FOR_SCH_0x822_MUX_OPTION = 0x0800,   /* For SCH 0x822 Mux options, bit,   */
   PSW_FOR_SCH_0x906_MUX_OPTION = 0x1000,   /* For SCH 0x906 Mux options, bit,   */
   PSW_FOR_SCH_0x90a_MUX_OPTION = 0x2000,   /* For SCH 0x90a Mux options, bit,   */
   PSW_FOR_SCH_0x912_MUX_OPTION = 0x4000,   /* For SCH 0x912 Mux options, bit,   */
   PSW_FOR_SCH_0x922_MUX_OPTION = 0x8000    /* For SCH 0x922 Mux options, bit,   */
} PSW_FOR_SCH_MUX_OPTION;


/* REV SCH Multiplex options --------------------------------------- */
typedef enum
{
   PSW_REV_SCH_0x03_MUX_OPTION  = 0x0001,   /* Rev SCH 0x03 Mux options,  bit,   */
   PSW_REV_SCH_0x809_MUX_OPTION = 0x0002,   /* Rev SCH 0x809 Mux options, bit,   */
   PSW_REV_SCH_0x811_MUX_OPTION = 0x0004,   /* Rev SCH 0x811 Mux options, bit,   */
   PSW_REV_SCH_0x821_MUX_OPTION = 0x0008,   /* Rev SCH 0x821 Mux options, bit,   */
   PSW_REV_SCH_0x905_MUX_OPTION = 0x0010,   /* Rev SCH 0x905 Mux options, bit,   */
   PSW_REV_SCH_0x909_MUX_OPTION = 0x0020,   /* Rev SCH 0x909 Mux options, bit,   */
   PSW_REV_SCH_0x911_MUX_OPTION = 0x0040,   /* Rev SCH 0x911 Mux options, bit,   */
   PSW_REV_SCH_0x921_MUX_OPTION = 0x0080,   /* Rev SCH 0x921 Mux options, bit,   */

   PSW_REV_SCH_0x04_MUX_OPTION  = 0x0100,   /* Rev SCH 0x04 Mux options,  bit,   */
   PSW_REV_SCH_0x80a_MUX_OPTION = 0x0200,   /* Rev SCH 0x80a Mux options, bit,   */
   PSW_REV_SCH_0x812_MUX_OPTION = 0x0400,   /* Rev SCH 0x812 Mux options, bit,   */
   PSW_REV_SCH_0x822_MUX_OPTION = 0x0800,   /* Rev SCH 0x822 Mux options, bit,   */
   PSW_REV_SCH_0x906_MUX_OPTION = 0x1000,   /* Rev SCH 0x906 Mux options, bit,   */
   PSW_REV_SCH_0x90a_MUX_OPTION = 0x2000,   /* Rev SCH 0x90a Mux options, bit,   */
   PSW_REV_SCH_0x912_MUX_OPTION = 0x4000,   /* Rev SCH 0x912 Mux options, bit,   */
   PSW_REV_SCH_0x922_MUX_OPTION = 0x8000    /* Rev SCH 0x922 Mux options, bit,   */
} PSW_REV_SCH_MUX_OPTION;

/* FCH & DCCH Multiplex options -------------------------------------- */
typedef enum
{
   PSW_MUX_OPTION_0x1   = 0x1,
   PSW_MUX_OPTION_0x2   = 0x2,
   PSW_MUX_OPTION_0x704 = 0x4
} PSW_MUX_OPTIONS_TYPES;

/* define set of bitmap indicator */
typedef enum
{
   PSW_SO_0_BITS = 0,
   PSW_SO_4_BITS = 1,
   PSW_SO_8_BITS = 2
} PSW_SERV_OPT_BITMAP_IND;

typedef enum
{
   PSW_CH_IND_RESERVED = 0x00,
   PSW_CH_IND_FCH      = 0x01,
   PSW_CH_IND_DCCH     = 0x02,
   PSW_CH_IND_FCH_AND_DCCH = 0x03
} PSW_CH_IND_TYPE;

typedef PACKED_PREFIX struct
{
   PSW_FCH_FRAME_SIZE_TYPE  FchFrameSize;           /* FCH_FRAME_SIZE, FCH frame Size capability indicator   */
   uint8                    ForFchLen;              /* FOR_FCH_LEN, Fwd Fundemental Channel Config Info. Len */
   uint16                   ForFchRcMap;            /* FOR_FCH_RC_MAP Forward Fundemental Radio Config. Info.*/
   uint8                    RevFchLen;              /* REV_FCH_LEN, Rev Fundemental Channel Config Info. Len */
   PSW_REV_RC_TYPE          RevFchRcMap;            /* REV_FCH_RC_MAP Reverse Fundemental Radio Config. Info.*/
} PACKED_POSTFIX  PSW_FCH_TYPE_SPECIFIC_FIELDS_TYPE;

typedef PACKED_PREFIX struct
{
   PSW_DCCH_FRAME_SIZE_TYPE DcchFrameSize;   /* DCCH_FRAME_SIZE, DCCH frame Size capability indicator */
   uint8                    ForDcchLen;      /* FOR_DCCH_LEN, Fwd Dedicated Ctrl Ch. Config Info. Len */
   uint16                   ForDcchRcMap;    /* FOR_DCCH_RC_MAP Fwd Dedicated Ch. Radio Config. Info. */
   uint8                    RevDcchLen;      /* REV_DCCH_LEN, Rev Dedicated Ctrl Ch. Config Info. Len */
   PSW_REV_RC_TYPE          RevDcchRcMap;    /* REV_DCCH_RC_MAP Rev Dedicated Ch. Radio Config. Info. */
} PACKED_POSTFIX  PSW_DCCH_TYPE_SPECIFIC_FIELDS_TYPE;

typedef PACKED_PREFIX struct
{
   uint8             ForSchLen;              /* FOR_SCH_LEN, Fwd Supplemental length in 3 bit uints   */
   uint16            ForSchRcMap;            /* FOR_SCH_RC_MAP, Fwd Supplemental Channel Radio Config Information. */
   uint8             ForSchNum;              /* FOR_SCH_NUM, Number of Forward Supplemental Cannel    */
   bool              ForTurboSupportedSch1;  /* FOR_TURBO_SUPPORTED, 1 Ch Fwd Turbo Coding Supported  */
   bool              ForTurboSupportedSch2;  /* FOR_TURBO_SUPPORTED, 2 Ch Fwd Turbo Coding Supported  */
   uint8             ForMaxTurboBlockSize;   /* FOR_MAX_TURBO_BLOCK_SIZE, Fwd Max Turbo Block Size    */
   bool              ForConvSupportedSch1;   /* FOR_CONV_SUPPORTED, 1 Ch Fwd convolutional Coding Supported */
   bool              ForConvSupportedSch2;   /* FOR_CONV_SUPPORTED, 2 Ch Fwd convolutional Coding Supported */
   uint8             ForMaxConvBlockSize;    /* FOR_MAX_CONV_BLOCK_SIZE, Fwd Max Conv. Code Block Size*/
} PACKED_POSTFIX  PSW_FOR_SCH_TYPE_SPECIFIC_FIELDS_TYPE;

typedef PACKED_PREFIX struct
{
   uint8             RevSchLen;              /* REV_SCH_LEN, Rev Supplemental length in 3 bit uints   */
   uint8             RevSchRcMap;            /* REV_SCH_RC_MAP Rev Dedicated Ch. Radio Config. Info.  */
   uint8             RevSchNum;              /* REV_SCH_NUM, Number of Reverse Supplemental Cannel    */
   bool              RevTurboSupportedSch1;  /* REV_TURBO_SUPPORTED, 1 Ch Rev Turbo Coding Supported  */
   bool              RevTurboSupportedSch2;  /* REV_TURBO_SUPPORTED, 2 Ch Rev Turbo Coding Supported  */
   uint8             RevMaxTurboBlockSize;   /* REV_MAX_TURBO_BLOCK_SIZE, Rev Max Turbo Block Size    */
   bool              RevConvSupportedSch1;   /* REV_CONV_SUPPORTED, 1 Ch Rev convolutional Coding Supported */
   bool              RevConvSupportedSch2;   /* REV_CONV_SUPPORTED, 2 Ch Rev convolutional Coding Supported */
   uint8             RevMaxConvBlockSize;    /* REV_MAX_CONV_BLOCK_SIZE, Rev Max Conv. Code Block Size*/
} PACKED_POSTFIX  PSW_REV_SCH_TYPE_SPECIFIC_FIELDS_TYPE;

typedef PACKED_PREFIX struct
{
   uint8             MaxMsNakRoundsFwd;      /* MAX_MS_NAK_ROUNDS_FWD Maximum number of RLP_NAK rounds
                                                supported by the MS on the forward traffic channel    */
   uint8             MaxMsNakRoundsRev;      /* MAX_MS_NAK_ROUNDS_REV Maximum number of RLP_NAK rounds
                                                supported by the MS on the reverse traffic channel    */
} PACKED_POSTFIX  PSW_RLP_CAP_BLOB_LEN_TYPE;

/* Status request, response */
typedef PACKED_PREFIX struct
{
  uint8 MsCapData[1];
} PACKED_POSTFIX  PswMsCapDbWriteMsgT;

typedef enum
{
  PSW_HSPD_CONTROL_HOLD_STATE = 0,
  PSW_HSPD_ACTIVE_STATE = 1
} PswHspdActiveControlHoldT;

typedef PACKED_PREFIX struct
{
  PswHspdActiveControlHoldT HspdNewState;
} PACKED_POSTFIX  PswHspdActiveControlHoldMsgT;

typedef enum
{
  PSW_SCH_9_6_KBPS   = 0,
  PSW_SCH_19_2_KBPS  = 1,
  PSW_SCH_38_4_KBPS  = 2,
  PSW_SCH_76_8_KBPS  = 3,
  PSW_SCH_153_6_KBPS = 4,
  PSW_SCH_307_2_KBPS = 5,
  PSW_SCH_614_4_KBPS = 6
} PswSchPrefRateT;

typedef PACKED_PREFIX struct
{
  uint8             SrId;
  PswSchPrefRateT   PrefRate;
  uint16            Duration;
} PACKED_POSTFIX  PswRevSchRequestMsgT;

/* PSW MISC parameters reported by DBM */
typedef enum
{
  PSW_SIM_QPCH_FEATURE         = 0,
  PSW_DYNAMIC_QPCH_FEATURE,
  PSW_QPCH_CCI_FEATURE,
  PSW_RESERVED_5_FEATURE,
  PSW_RESERVED_4_FEATURE,
  PSW_RESERVED_3_FEATURE,
  PSW_RESERVED_2_FEATURE,
  PSW_RESERVED_1_FEATURE
} PswFeatureBitNumT;


typedef enum
{
  PHY_1X_DIVERSITY_ENABLED = 0,
  PHY_RESERVED_3_FEATURE   = 1,
  PHY_RESERVED_2_FEATURE   = 2,
  PHY_RESERVED_1_FEATURE   = 3,
  PHY_1X_IC_CONTROL        = 4, /* IC Control occupies 4 bits */
} PhyFeatureBitNumT;


typedef enum
{
  PSW_SO73_ENABLED          = 0,
  PSW_SO73_WBENABLED,
  PSW_VOICE_SERVICE_ENABLED,
  PSW_DDTM_ENABLED,
  PSW_PRL_ENABLED,
  PSW_RESERVED_3_ENABLED,
  PSW_RESERVED_2_ENABLED,
  PSW_RESERVED_1_ENABLED
} PswEnableFlagsBitNumT;

typedef NV_PACKED_PREFIX struct
{
   uint8  StartupActiveNam;
   uint8  PswFeatureFlags;
   uint8  PhyFeatureFlags;
   uint8  PswMiscEnableFlags;
   uint8  pendingReg;
   uint16 PrefHomeMOSO;
   uint16 PrefHomeMTSO;
   uint16 PrefRoamMOSO;
   bool   IS683NamLock;
   uint8  DfsEcIoThresh;
   uint16 qpchPilotEcIoThreshLow;
   uint16 qpchPilotEcIoThreshHigh;
   uint8  reserved_0;
   uint16 checksum;
} NV_PACKED_POSTFIX  PswMiscT;

typedef PACKED_PREFIX struct
{
   uint8  StartupActiveNam;
   uint8  PswFeatureFlags;
   uint8  PhyFeatureFlags;
   uint8  PswMiscEnableFlags;
   uint8  pendingReg;
   uint16 PrefHomeMOSO;
   uint16 PrefHomeMTSO;
   uint16 PrefRoamMOSO;
   bool   IS683NamLock;
   uint8  DfsEcIoThresh;
   uint16 qpchPilotEcIoThreshLow;
   uint16 qpchPilotEcIoThreshHigh;
   uint8  reserved_0;
   uint16 checksum;
} PACKED_POSTFIX  PackedPswMiscT;

#ifdef MTK_CBP_ENCRYPT_VOICE
#define KMC_PUBLIC_KEY_LEN 48/* KMC public key length by byte = 48 */

typedef NV_PACKED_PREFIX struct
{
   uint8     KMCKeyVersion;/* KMC key version */
   uint8     KMCPublicKey[KMC_PUBLIC_KEY_LEN];/* KMC public key */
   uint8     reserved_0;
   uint16    checksum;
} NV_PACKED_POSTFIX  PswDbmCryptDataT;
#endif

/*#ifdef VERIZON_GPS_DEBUG */
#define MAX_DEBUG_INFO 64
#define MAX_NUM_SATELLITES 9

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  PswGpsDebugInfoCmdMsgT;

typedef PACKED_PREFIX struct
{
  bool   Valid;
  uint32 num_pilots_in_aflt_msg;
  uint32 SystemTime;
  uint32 LAT;
  uint32 LONG;
  uint32 FixType;
  uint8  SatelliteNumber[MAX_NUM_SATELLITES];
} PACKED_POSTFIX  PswGpsDebugInfoRspMsgT;

/*#endif */

typedef enum
{
   ID_NOT_SET = 0,
   ESN = 1,
   MEID = 2,
   UIMID = 3,
   SF_EUIMID
} MobileIDType;

/* Set ESN message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
   MobileIDType Id;
   uint64       value;
} PACKED_POSTFIX  PswSetMobileIDMsgT;

/* Get ESN message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  PswGetMobileIDMsgT;

#ifdef MTK_CBP
/* Verify SPC message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;         /* Response routing information */
   uint32       SPC;
   bool         reset;
} PACKED_POSTFIX  PswVerifySPCMsgT;
#endif

/* Get ESN response message */
typedef PACKED_PREFIX struct
{
   MobileIDType Id;
   uint64       value;
} PACKED_POSTFIX  PswGetMobileIDRspMsgT;

/* Get Active NAM message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  PswGetActiveNamMsgT;

/* Get Active NAM response message */
typedef PACKED_PREFIX struct
{
  uint8       CurrentActiveNam;
} PACKED_POSTFIX  PswGetActiveNamResponseMsgT;

/* Select Active NAM message */
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
  uint8       SelectActiveNam;
} PACKED_POSTFIX  PswSelectActiveNamMsgT;

typedef enum
{
  DUAL_NAM_NOT_SUPPORTED = 0,
  NAM1_SELECTED          = 1,
  NAM2_SELECTED          = 2,
  PS_ACTIVE_NAM1_LOCKED  = 3,
  PS_ACTIVE_NAM2_LOCKED  = 4
} ActiveNamResultT;

/* Select Active NAM response message */
typedef PACKED_PREFIX struct
{
  uint8       ActiveNamResult;
} PACKED_POSTFIX  PswSelectActiveNamResponseMsgT;

typedef PACKED_PREFIX struct
{
  uint8  PWR_CNTL_STEP;
  bool   USE_TIME;
  uint8  ACTION_TIME;
  bool   FPC_INCL;
  uint8  FPC_MODE;
  bool   FPC_PRI_CHAN;
  bool   FPC_OLPC_FCH_INCL;
  uint8  FPC_FCH_FER;
  uint8  FPC_FCH_MIN_SETPT;
  uint8  FPC_FCH_MAX_SETPT;
  bool   FPC_OLPC_DCCH_INCL;
  uint8  FPC_DCCH_FER;
  uint8  FPC_DCCH_MIN_SETPT;
  uint8  FPC_DCCH_MAX_SETPT;
  bool   FPC_SEC_CHAN;
  uint8  NUM_SUP;
  bool   SCH_ID;
  uint8  FPC_SCH_FER;
  uint8  FPC_SCH_MIN_SETPT;
  uint8  FPC_SCH_MAX_SETPT;
  bool   FPC_THRESH_INCL;
  uint8  FPC_SETPT_THRESH;
  bool   FPC_THRESH_SCH_INCL;
  uint8  FPC_SETPT_THRESH_SCH;
  bool   RPC_INCL;
  uint8  RPC_NUM_REC;
  uint8  RPC_ADJ_REC_TYPE_0;
  uint8  RPC_ADJ_REC_LEN_0;
  bool   PCM_FCH_INCL;
  uint8  FCH_CHAN_ADJ_GAIN;
  bool   PCM_DCCH_INCL;
  uint8  DCCH_CHAN_ADJ_GAIN;
  bool   SCH0_INCL;
  uint8  SCH0_CHAN_ADJ_GAIN;
  bool   SCH1_INCL;
  uint8  SCH1_CHAN_ADJ_GAIN;
  uint8  RPC_ADJ_REC_TYPE_1;
  uint8  RPC_ADJ_REC_LEN_1;
  bool   RL_ATT_ADJ_GAIN_TYPE_1;
  bool   RC3_RC5_20MS_INCL_1;
  uint8  RL_ATT_ADJ_GAIN_1500;
  uint8  RL_ATT_ADJ_GAIN_2700;
  uint8  RL_ATT_ADJ_GAIN_4800;
  uint8  RL_ATT_ADJ_GAIN_9600;
  bool   RC4_RC6_20MS_INCL_1;
  uint8  RL_ATT_ADJ_GAIN_1800;
  uint8  RL_ATT_ADJ_GAIN_3600;
  uint8  RL_ATT_ADJ_GAIN_7200;
  uint8  RL_ATT_ADJ_GAIN_14400;
  bool   MS5_INCL;
  uint8  NORM_ATT_GAIN_9600_5MS;
  uint8  RPC_ADJ_REC_TYPE_2;
  uint8  RPC_ADJ_REC_LEN_2;
  bool   CODE_TYPE;
  bool   RL_ATT_ADJ_GAIN_TYPE_2;
  bool   RC3_RC5_20MS_INCL_2;
  uint8  RL_ATT_ADJ_GAIN_19200;
  uint8  RL_ATT_ADJ_GAIN_38400;
  uint8  RL_ATT_ADJ_GAIN_76800;
  uint8  RL_ATT_ADJ_GAIN_153600;
  uint8  RL_ATT_ADJ_GAIN_307200;
  uint8  RL_ATT_ADJ_GAIN_614400;
  bool   RC4_RC6_20MS_INCL_2;
  uint8  RL_ATT_ADJ_GAIN_28800;
  uint8  RL_ATT_ADJ_GAIN_576600;
  uint8  RL_ATT_ADJ_GAIN_115200;
  uint8  RL_ATT_ADJ_GAIN_230400;
  uint8  RL_ATT_ADJ_GAIN_460800;
  uint8  RL_ATT_ADJ_GAIN_1036800;
} PACKED_POSTFIX  PswTestPowerControlMsgT;

typedef enum
{
  HOME_ONLY,
  AUTOMATIC,
  AUTOMATIC_A,
  AUTOMATIC_B
} SystemSelectType;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;
} PACKED_POSTFIX  PswReadRawIMSIMsgT;

typedef PACKED_PREFIX struct
{
  uint8   ValidIMSImap;      /* bit0: IMSI_M, bit1:IMSI_T */
  uint8   M_MCC[3];          /* MCC raw digits */
  uint8   M_IMSI_11_12[2];   /* M_IMSI_11_12 raw digits */
  uint8   M_IMSI_S[10];      /* M_IMSI_S raw digits */
  uint8   M_IMSI_Class;
  uint8   M_AddrNum;
  uint8   T_MCC[3];          /* MCC raw digits */
  uint8   T_IMSI_11_12[2];   /* M_IMSI_11_12 raw digits */
  uint8   T_IMSI_S[10];      /* M_IMSI_S raw digits */
  uint8   T_IMSI_Class;
  uint8   T_AddrNum;
  uint8   Mdn[NAM_MAX_MDN_DIGITS];  /* MDN max 16 digits */
  uint8   MDN_NUM_DIGITS;           /* MDN number digits */
  uint8   ACCOLCp;           /* access overload class */
} PACKED_POSTFIX  PswReadRawIMSIRspT;

/* PSW_OOSA_WAKEUP_IND_MSG */
typedef PACKED_PREFIX struct
{
  OosaWakeupTypeT OosaWakeupType;
} PACKED_POSTFIX  PswOosaWakupIndMsgT;

typedef PACKED_PREFIX struct
{
  CHANNEL_DESC  Sys;
  bool          LastChannelInSRSL;
} PACKED_POSTFIX  PswCssSelectRspMsgT;

typedef enum
{
   CP_SYS_REJECTED,
   CP_SYS_ACCEPTED,
   CP_SYS_MCC_MNC_NEEDED
} PswCssValidationRsltT;

typedef enum
{
   CP_REG_NOT_ROAMING,
   CP_REG_NID_ROAMING,
   CP_REG_SID_ROAMING,
   CP_REG_UNKNOWN_ROAMING
} PswRegRoamStatus;

typedef PACKED_PREFIX struct
{
  PswCssValidationRsltT ValidationRslt;
  PswRegRoamStatus         RegRoamStatus;
} PACKED_POSTFIX  PswCssValidateRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  PswGetNamPrefereceMsgT;

typedef enum
{
   FORCESLEEP,
   ACQUISITION,
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
   ExitPs       /* Release 1x PS during C2L MPSR */
#endif
} CssForceInitReasonT;

typedef struct
{
   CssForceInitReasonT  reason;
} PswCssSysAcqReqMsgT;

typedef PACKED_PREFIX struct
{
   uint16  duration;    /*in the unit of 100ms*/
} PACKED_POSTFIX  PswCssOosaSleepReqMsgT;

typedef PACKED_PREFIX struct
{
   bool bActive;
} PACKED_POSTFIX  PswRlpActivityStatusReportMsgT;

#ifdef MTK_CBP
/* PSW_CSS_SET_EMERGENCY_CALLBACK_MODE_MSG */
typedef PACKED_PREFIX struct
{
  bool  isInCallBackMode;
} PACKED_POSTFIX  PswCssSetEmergencyCallbackModeMsgT;
#endif

typedef enum {
   PSW_SLOTTED_TST_MODE_VETO_FLAG  = 0x0001,  /* Test mode veto */
   PSW_SLOTTED_PE_SMS_VETO_FLAG    = 0x0002,  /* PE SMS: disabling slotted for paging SMS activity */
   PSW_SLOTTED_PE_SR_VETO_FLAG     = 0x0004,  /* PE Silent retry: disable slotted betwen retries */
   PSW_SLOTTED_PE_LOC_VETO_FLAG    = 0x0008,  /* PE LOC: Loc srv activity (AFLT, ...) */
   PSW_SLOTTED_LEC_VETO_FLAG       = 0x0010,  /* LEC: GPS activity (time sync, ...) */
   PSW_SLOTTED_L1D_VETO_FLAG       = 0x0020,  /* L1D: bad 32k slotted veto */
   PSW_SLOTTED_SPARE_VETO_FLAG10   = 0x0040,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG9    = 0x0080,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG8    = 0x0100,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG7    = 0x0200,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG6    = 0x0400,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG5    = 0x0800,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG4    = 0x1000,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG3    = 0x2000,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG2    = 0x4000,  /* Spare   */
   PSW_SLOTTED_SPARE_VETO_FLAG1    = 0x8000,  /* Spare   */
} PswSlottedVetoFlagT;

typedef PACKED_PREFIX struct
{
   PswSlottedVetoFlagT  flag;
   bool                 vetoSlotted;
} PACKED_POSTFIX  PswSlottedVetoMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
   uint8       SIP_PASSWORD[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  PswSetSIPpasswordMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  PswGetSIPpasswordMsgT;

typedef PACKED_PREFIX struct
{
   bool        Valid;
   uint8       Pswd[HLP_MAX_PSWD_LEN];
} PACKED_POSTFIX  PswGetSIPpasswordRspMsgT;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  PswGetMIPkeysMsgT;

typedef PACKED_PREFIX struct
{
   bool        Valid;
   uint8       MN_AAA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE]; /* MIP AAA Shared Secret*/
   uint8       MN_HA_PASSWORD[HLP_MN_PASSWD_MAX_SIZE]; /* MIP HA Shared Secret  */
} PACKED_POSTFIX  PswGetMIPkeysRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
   uint32      SPC;
} PACKED_POSTFIX  PswSetSPCMsgT;

/* PSW_VAL_SET_SO_SUPPORT_MSG */
typedef PACKED_PREFIX struct
{
   uint16 ServiceOption;
   bool   Supported;
} PACKED_POSTFIX  PswSetSoSupportMsgT;

/* Must match ETS ps_msg.txt enum
   IP Password Update Response.
 */
typedef enum
{
   SUCCESS_0,
   PS_NOT_POWERED_UP,
   DATA_SERVICES_NOT_SUPPORTED,
   MOBILE_IP_NOT_SUPPORTED,
   PREVIOUS_WRITE_NOT_FINISHED,
   DATA_OUT_OF_RANGE,
   CHECKSUM_MISMATCH,
   SECURITY_CODE_OR_SPC_MISMATCH
#ifdef MTK_CBP
   ,UICC_UPDATE_PROHIBITED
#endif
} PswSetSecureDataRspCode;

typedef PACKED_PREFIX struct
{
   bool        Result;
   PswSetSecureDataRspCode ResponseCode;
} PACKED_POSTFIX  PswSetSecureDataRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  PswGetSpcMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  PswGetSSDMsgT;

typedef PACKED_PREFIX struct
{
   bool        Valid;
   uint32     SPC;
} PACKED_POSTFIX  PswGetSpcRspMsgT;

typedef PACKED_PREFIX struct
{
   bool        Valid;
   uint8       SSD_A[8];
   uint8       SSD_B[8];
} PACKED_POSTFIX  PswGetSSDRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  PswGetCpBcToCdmaBcMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 namIndex;
   UINT8 bandClass;
   UINT32 subBandMask;
   UINT8 calibTable;
} PACKED_POSTFIX  BandInfoT;

typedef PACKED_PREFIX struct
{
   UINT8 numBandsSupported;
   BandInfoT bandInfo[NAM_BANDS_SUPPORTED];
} PACKED_POSTFIX  PswGetCpBcToCdmaBcRspMsgT;

/* Set Lost Scan Ping Frequency message */
typedef PACKED_PREFIX struct
{
   uint8 Geo_Scan_Ping_Frequency;
   uint8 Non_Geo_Scan_Ping_Frequency;
} PACKED_POSTFIX  PswSetLostScanPingFreqMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
   HwdCtrlModeT CtrlMode;
   uint8        Band;
   uint16       Channel;
} PACKED_POSTFIX  PswTstPllChannelMsgT;


typedef enum
{
 PSW_SET_MDN_SUCCESS,
 PSW_SET_MDN_FAIL_INVALID_CHARS,
 PSW_SET_MDN_FAIL_INVALID_NUM_DIGITS,
 PSW_SET_MDN_END_RESULT
}PswSetMdnResponse;

typedef enum
{
 PSW_SET_IMSI_SUCCESS,
 PSW_SET_IMSI_FAIL_INVALID_CHARS = 7,
 PSW_SET_IMSI_FAIL_INVALID_NUM_DIGITS = 13,
 PSW_SET_IMSI_FAIL_INVALID_MCC_CHARS = 14,
 PSW_SET_IMSI_FAIL_INVALID_IMSI_11_12_CHARS = 15,
 PSW_SET_IMSI_END_RESULT
}PswWriteRawImsiResponse;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
   char    Mdn_Digits[NAM_MAX_MDN_DIGITS]; /* A NULL Terminated String */
} PACKED_POSTFIX  PswWriteRawMDNMsg;

typedef PACKED_PREFIX struct
{
  PswSetMdnResponse result;
} PACKED_POSTFIX  PswSetRawMDNRspMsg;

typedef PACKED_PREFIX struct
{
   BOOL Mode;
} PACKED_POSTFIX  PswUiccFactoryModeMsgT;

typedef enum
{
  PSW_PARM_SET_ESN_MEID,                                 /* 0 */
  PSW_PARM_GET_ESN_MEID,                                 /* 1 */
  PSW_PARM_SET_SPC,                                      /* 2 */
  PSW_PARM_GET_SPC,                                      /* 3 */
  PSW_PARM_SET_AKEY,                                     /* 4 */
  PSW_PARM_GET_AKEY,                                     /* 5 */
  PSW_PARM_SET_MSID,                                     /* 6 */
  PSW_PARM_GET_MSID,                                     /* 7 */
  PSW_PARM_SET_PREV,                                     /* 8 */
  PSW_PARM_GET_PREV,                                     /* 9 */
  PSW_PARM_SET_POSITIVE_SID_NID_LIST,                    /* 10 */
  PSW_PARM_GET_POSITIVE_SID_NID_LIST,                    /* 11 */
  PSW_PARM_SET_NEGATIVE_SID_NID_LIST,                    /* 12 */
  PSW_PARM_GET_NEGATIVE_SID_NID_LIST,                    /* 13 */
  PSW_PARM_SET_PRI_SEC_CH,                               /* 14 */
  PSW_PARM_GET_PRI_SEC_CH,                               /* 15 */
  PSW_PARM_SET_SYSTEM_SELECT,                            /* 16 */
  PSW_PARM_GET_SYSTEM_SELECT,                            /* 17 */
  PSW_PARM_SET_SLOT_CYCLE_INDEX,                         /* 18 */
  PSW_PARM_GET_SLOT_CYCLE_INDEX,                         /* 19 */
  PSW_PARM_SET_MDN,                                      /* 20 */
  PSW_PARM_GET_MDN,                                      /* 21 */
  PSW_PARM_SET_PRL_ENABLE,                               /* 22 */
  PSW_PARM_GET_PRL_ENABLE,                               /* 23 */
  PSW_PARM_SET_HOME_SYS_REGISTRATION,                    /* 24 */
  PSW_PARM_GET_HOME_SYS_REGISTRATION,                    /* 25 */
  PSW_PARM_SET_FOREIGN_SID_REGISTRATION,                 /* 26 */
  PSW_PARM_GET_FOREIGN_SID_REGISTRATION,                 /* 27 */
  PSW_PARM_SET_FOREIGN_NID_REGISTRATION,                 /* 28 */
  PSW_PARM_GET_FOREIGN_NID_REGISTRATION,                 /* 29 */
  PSW_PARM_SET_ACCESS_OVERLOAD_CLASS,                    /* 30 */
  PSW_PARM_GET_ACCESS_OVERLOAD_CLASS,                    /* 31 */
  PSW_PARM_SET_CDMA_PREF_BAND,                           /* 32 */
  PSW_PARM_GET_CDMA_PREF_BAND,                           /* 33 */
  PSW_PARM_SET_CDMA_PREF_AorB,                           /* 34 */
  PSW_PARM_GET_CDMA_PREF_AorB,                           /* 35 */
  PSW_PARM_SET_SCM,                                      /* 36 */
  PSW_PARM_GET_SCM,                                      /* 37 */
  PSW_PARM_SET_PRL_INFO,                                 /* 38 */
  PSW_PARM_GET_PRL_INFO,                                 /* 39 */
  PSW_PARM_SET_OTAPA_ENABLE,                             /* 40 */
  PSW_PARM_GET_OTAPA_ENABLE,                             /* 41 */
  PSW_PARM_SET_OTA_POWERUP_MODE,                         /* 42 */
  PSW_PARM_GET_OTA_POWERUP_MODE,                         /* 43 */
  PSW_PARM_SET_IMSI_S1,                                  /* 44 */
  PSW_PARM_GET_IMSI_S1,                                  /* 45 */
  PSW_PARM_SET_IMSI_S2,                                  /* 46 */
  PSW_PARM_GET_IMSI_S2,                                  /* 47 */
  PSW_PARM_SET_SPC_CHANGE_ENABLE,                        /* 48 */
  PSW_PARM_GET_SPC_CHANGE_ENABLE,                        /* 49 */
  PSW_PARM_SET_SPASM_ENABLE,                             /* 50 */
  PSW_PARM_GET_SPASM_ENABLE,                             /* 51 */
  PSW_PARM_SET_SID_NID_LIST_SP,                          /* 52 */
  PSW_PARM_GET_SID_NID_LIST_SP,                          /* 53 */
  PSW_PARM_SET_DISTANCE_BASED_PARMS,                     /* 54 */
  PSW_PARM_GET_DISTANCE_BASED_PARMS,                     /* 55 */
  PSW_PARM_SET_ENCODED_MSID,                             /* 56 */
  PSW_PARM_GET_ENCODED_MSID,                             /* 57 */
  PSW_PARM_SET_ENABLE_OTA,                               /* 58 */
  PSW_PARM_GET_ENABLE_OTA,                               /* 59 */
  PSW_PARM_SET_PREF_HOME_MO_SO,                          /* 60 */
  PSW_PARM_GET_PREF_HOME_MO_SO,                          /* 61 */
  PSW_PARM_SET_PREF_ROAM_MO_SO,                          /* 62 */
  PSW_PARM_GET_PREF_ROAM_MO_SO,                          /* 63 */
  PSW_PARM_SET_PREF_HOME_MT_SO,                          /* 64 */
  PSW_PARM_GET_PREF_HOME_MT_SO,                          /* 65 */
  PSW_PARM_SET_UNUSED_1,                               /* 66 AVAILABLE for use */
  PSW_PARM_GET_UNUSED_1,                               /* 67 AVAILABLE for use */
  PSW_PARM_SET_A21_MS_SUBSCRIPTION_INFO,                 /* 68 */
  PSW_PARM_GET_A21_MS_SUBSCRIPTION_INFO,                 /* 69 */
  PSW_PARM_GET_SLOTTED_MODE_ALLOWED_OBSOLETE,            /* 70 */
  PSW_PARM_SET_VOICE_SERVICE_ALLOWED,                    /* 71 */
  PSW_PARM_GET_VOICE_SERVICE_ALLOWED,                    /* 72 */
  PSW_PARM_SET_ROAM_SETTING_DOMESTIC_VOICE,              /* 73 */
  PSW_PARM_GET_ROAM_SETTING_DOMESTIC_VOICE,              /* 74 */
  PSW_PARM_SET_ROAM_SETTING_DOMESTIC_DATA,               /* 75 */
  PSW_PARM_GET_ROAM_SETTING_DOMESTIC_DATA,               /* 76 */
  PSW_PARM_SET_ROAM_SETTING_INTERNATIONAL_VOICE,         /* 77 */
  PSW_PARM_GET_ROAM_SETTING_INTERNATIONAL_VOICE,         /* 78 */
  PSW_PARM_SET_ROAM_SETTING_INTERNATIONAL_DATA,          /* 79 */
  PSW_PARM_GET_ROAM_SETTING_INTERNATIONAL_DATA,          /* 80 */
  PSW_PARM_SET_DDTM_MODE_ENABLED,                        /* 81 */
  PSW_PARM_GET_DDTM_MODE_ENABLED,                        /* 82 */
  PSW_PARM_SET_SO_SUPPORT,                               /* 83 */
  PSW_PARM_GET_SO_SUPPORT,                               /* 84 */
  PSW_PARM_SET_SLOTTED_MODE_ALLOWED = 85, /* for ETS compatibility with platform not having 71 to 84 in ETS definitions */
  PSW_PARM_GET_SLOTTED_MODE_ALLOWED,
  PSW_PARM_SET_SIM_QPCH_ENABLED,
  PSW_PARM_GET_SIM_QPCH_ENABLED,
  PSW_PARM_SET_DIV_SUPPORT,
  PSW_PARM_GET_DIV_SUPPORT,
  PSW_PARM_SET_QPCH_ENABLE_DYNAMIC_CTRL,
  PSW_PARM_GET_QPCH_ENABLE_DYNAMIC_CTRL,
  PSW_PARM_SET_QPCH_CCI_ENABLED,
  PSW_PARM_GET_QPCH_CCI_ENABLED,
  PSW_PARM_SET_1XADV_ENABLED,
  PSW_PARM_GET_1XADV_ENABLED,
  PSW_PARM_SET_SO73_CONTROL,
  PSW_PARM_GET_SO73_CONTROL,
  PSW_PARM_SET_DFS_ECIO_THRESH,
  PSW_PARM_GET_DFS_ECIO_THRESH,
  PSW_PARM_SET_IC_MODE,
  PSW_PARM_GET_IC_MODE,
  PSW_PARM_VALAT_SET_PRL_ENABLE,
  PSW_PARM_VALAT_GET_PRL_ENABLE,
#ifdef MTK_CBP
  PSW_PARM_SET_ROAM_SETTING_BITMAP,          /* bitmap of all roaming setting */
  PSW_PARM_GET_ROAM_SETTING_BITMAP,          /* bitmap of all roaming setting */
#endif

/* IMPORTANT:
   when adding new parameters to the list, add BOTH SET and GET,
   even if one of them is not used/implemented */
  PSW_PARM_OPERATION_ID_END_LIST
} PswParmOperationId;

typedef enum
{
 PSW_PARM_OPERATION_SUCCESS,
 PSW_PARM_OPERATION_FAIL_READ_NOT_ALLOWED,
 PSW_PARM_OPERATION_FAIL_WRITE_NOT_ALLOWED,
 PSW_PARM_OPERATION_NOT_ALLOWED_IN_PS_STATE,
 PSW_PARM_OPERATION_FAIL_INVALID_PTR,
 PSW_PARM_OPERATION_FAIL_INVALID_LENGTH,
 PSW_PARM_OPERATION_FAIL_INVALID_AKEY_CHECKSUM,
 PSW_PARM_OPERATION_FAIL_SET_MSID_INVALID_CHARS,
 PSW_PARM_OPERATION_FAIL_INVALID_CHANNEL,
 PSW_PARM_OPERATION_GENERAL_FAILURE,
 PSW_PARM_OPERATION_FAIL_SET_MDN_INVALID_CHARS,
 PSW_PARM_OPERATION_FAIL_SET_MDN_INVALID_NUM_DIGITS,
 PSW_PARM_OPERATION_NO_CHANGE_IN_VALUE,
 PSW_PARM_OPERATION_FAIL_VALUE_OUT_OF_RANGE,
 PSW_PARM_OPERATION_FAIL_SET_MSID_INVALID_NUM_DIGITS,
 PSW_PARM_OPERATION_FAIL_SET_MSID_INVALID_MCC_CHARS,
 PSW_PARM_OPERATION_FAIL_SET_MSID_INVALID_IMSI_11_12_CHARS,
 PSW_PARM_OPERATION_FAIL_PRL_INVALID,
 PSW_PARM_OPERATION_FAIL_IMSI_DIGITS_OUT_OF_RANGE,
 PSW_PARM_OPERATION_FAIL_OP_TYPE_NOT_SUPPORTED,
 PSW_PARM_OPERATION_FAIL_DEFAULT_NOT_DEFINED,
 PSW_PARM_OPERATION_FAIL_DEFAULT_NOT_SUPPORTED_FOR_PARM,
 PSW_PARM_OPERATION_FAIL_SERVICE_OPTION_NOT_SUPPORTED,
#ifdef MTK_CBP
 PSW_PARM_OPERATION_FAIL_UICC_WRITE_ERROR,
 PSW_PARM_OPERATION_FAIL_UICC_PARM_NOT_UPDATEABLE,
#endif
 PSW_PARM_OPERATION_RESULT_END_LIST
}PswParmAccessResultCode;

typedef enum
{
  UPDATE_MSID_IMSI_M,
  UPDATE_MSID_IMSI_T,
  UPDATE_MSID_LIST_END
} PswMsidFieldToUpdate;

typedef enum
{
  IMSI_M_VALID = 1,
  IMSI_T_VALID,
  IMSI_VALID_END
} PswImsiValid;

typedef enum
{
  PSW_PARM_ESN,
  PSW_PARM_MEID,
  PSW_PARM_SPC,
  PSW_PARM_POS_SID_NID_LIST,
  PSW_PARM_NEG_SID_NID_LIST,
  PSW_PARM_SYSTEM_SELECT,
  PSW_PARM_PRI_SEC_CH,
  PSW_PARM_AKEY,
  PSW_PARM_MSID,
  PSW_PARM_ID_END
} PswParmID;

typedef enum
{
  PSW_PARM_MIN_VALUE,
  PSW_PARM_MAX_VALUE,
  PSW_PARM_DEFAULT_VALUE,
  PSW_PARM_CUSTOM_VALUE,
  PSW_PARM_OP_TYPE_LIST_END
} PswParmOperationType;

typedef PACKED_PREFIX struct
{
  uint64 meid;
  uint32 esn;
#ifdef MTK_CBP
  uint64 akey;
#endif
  MobileIDType type;
} PACKED_POSTFIX  PswEsnMeid_APIStruct;

typedef PACKED_PREFIX struct
{
  uint64 Akey;
  uint32 checkSum;
} PACKED_POSTFIX  PswAkey_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 PRev;
} PACKED_POSTFIX  PswPRev_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 SCM[NAM_BANDS_SUPPORTED];
} PACKED_POSTFIX  PswSCM_APIStruct;

typedef PACKED_PREFIX struct
{
  uint16 SID[MAX_POSITIVE_SIDS];
  uint16 NID[MAX_POSITIVE_SIDS];
  uint32 max_count_in_List;
} PACKED_POSTFIX  PswSIDNIDList_APIStruct;

typedef PACKED_PREFIX struct
{
  uint32 SPC;
} PACKED_POSTFIX  PswSPC_APIStruct;

typedef PACKED_PREFIX struct
{
  SystemSelectType systemSelect;
} PACKED_POSTFIX  PswSysSelect_APIStruct;

typedef PACKED_PREFIX struct
{
  bool setA;
  uint16 CPC_A;
  uint16 CSC_A;
  bool setB;
  uint16 CPC_B;
  uint16 CSC_B;
} PACKED_POSTFIX  PswPriSecCh_APIStruct;

typedef PACKED_PREFIX struct
{
   PswMsidFieldToUpdate UpdateField;
   char    Imsi_Digits[MAX_IMSI_S_DIGITS]; /* A NULL Terminated String */
   uint8   UpdateIMSI_11_12;
   char    IMSI_11_12[MAX_MNC_DIGITS];     /* A NULL Terminated String */
   uint8   UpdateMCC;
   char    MCC[MAX_MCC_DIGITS];            /* A NULL Terminated String */
   uint8   overwrite_mdn_with_imsi_s;
} PACKED_POSTFIX  PswSetRawMSID_APIStruct;

typedef PACKED_PREFIX struct
{
   PswMsidFieldToUpdate UpdateField;
   char    Imsi_Digits[MAX_IMSI_S1_DIGITS]; /* A NULL Terminated String */
} PACKED_POSTFIX  PswSetIMSIS1_APIStruct;

typedef PACKED_PREFIX struct
{
   PswMsidFieldToUpdate UpdateField;
   char    Imsi_Digits[MAX_IMSI_S2_DIGITS]; /* A NULL Terminated String */
} PACKED_POSTFIX  PswSetIMSIS2_APIStruct;

typedef PACKED_PREFIX struct
{
   PswMsidFieldToUpdate Field;
} PACKED_POSTFIX  PswGetIMSIS12_APIStruct;

typedef PACKED_PREFIX struct
{
   PswMsidFieldToUpdate Field;
   uint8    Imsi_Digits[MAX_IMSI_S1_DIGITS]; /* A NULL Terminated String */
} PACKED_POSTFIX  PswGetIMSIS1Rsp_APIStruct;

typedef PACKED_PREFIX struct
{
   PswMsidFieldToUpdate Field;
   uint8    Imsi_Digits[MAX_IMSI_S2_DIGITS]; /* A NULL Terminated String */
} PACKED_POSTFIX  PswGetIMSIS2Rsp_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 PrefMode;
} PACKED_POSTFIX  PswPrefMode_APIStruct;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
   PswSetRawMSID_APIStruct RawImsi;
} PACKED_POSTFIX  PswWriteRawIMSIMsgT;

typedef PACKED_PREFIX struct
{
  PswWriteRawImsiResponse result;
} PACKED_POSTFIX  PswSetRawIMSIRspMsg;

typedef PACKED_PREFIX struct
{
   uint8   ValidIMSImap;      /* bit0: IMSI_M, bit1:IMSI_T */
   uint8   M_MCC[3];          /* MCC raw digits */
   uint8   M_IMSI_11_12[2];   /* M_IMSI_11_12 raw digits */
   uint8   M_IMSI_S[10];      /* M_IMSI_S raw digits */
   uint8   M_IMSI_Class;
   uint8   M_AddrNum;
   uint8   T_MCC[3];          /* MCC raw digits */
   uint8   T_IMSI_11_12[2];   /* M_IMSI_11_12 raw digits */
   uint8   T_IMSI_S[10];      /* M_IMSI_S raw digits */
   uint8   T_IMSI_Class;
   uint8   T_AddrNum;
   uint8   Mdn[NAM_MAX_MDN_DIGITS];  /* MDN max 16 digits */
   uint8   MDN_NUM_DIGITS;           /* MDN number digits */
   uint8   ACCOLCp;           /* access overload class */
} PACKED_POSTFIX  PswGetRawMSID_APIStruct;

typedef PACKED_PREFIX struct
{
   PswMsidFieldToUpdate Field;
} PACKED_POSTFIX  PswGetEncodedMSIDMsg_APIStruct;

typedef PACKED_PREFIX struct
{
    PswMsidFieldToUpdate Field;  /*IMSI_M or IMSI_T*/
    uint16 mcc;                  /* Mobile Country Code                */
    uint8  imsi_11_12;           /* 7 bit  IMSI_11_12                  */
    uint16 imsi_s2;              /* 10 bit IMSI_S2 value               */
    uint32 imsi_s1;              /* 24 bit IMSI_S1 value               */
    uint8  imsiClass;            /* CP_IMSI_CLASS_0/1 indication       */
    uint8  addrNum;              /* number of digits in NMSI - 4       */
} PACKED_POSTFIX  PswGetEncodedMSIDRsp_APIStruct;

typedef PACKED_PREFIX struct
{
    PswMsidFieldToUpdate Field;  /*IMSI_M or IMSI_T*/
    bool   updateMcc;
	uint16 mcc;                  /* Mobile Country Code                */
    bool   updateImsi_11_12;
    uint8  imsi_11_12;           /* 7 bit  IMSI_11_12                  */
    bool   updateImsi_s2;
    uint16 imsi_s2;              /* 10 bit IMSI_S2 value               */
    bool   updateImsi_s1;
    uint32 imsi_s1;              /* 24 bit IMSI_S1 value               */
    bool   updateImsiClass;
    uint8  imsiClass;            /* CP_IMSI_CLASS_0/1 indication       */
    bool   updateAddrNum;
    uint8  addrNum;              /* number of digits in NMSI - 4       */
} PACKED_POSTFIX  PswSetEncodedMSID_APIStruct;


typedef PACKED_PREFIX struct
{
  uint8 Slot_Cycle_Index;
} PACKED_POSTFIX  PswSlotCycleIndex_APIStruct;

typedef PACKED_PREFIX struct
{
  char Mdn_Digits[NAM_MAX_MDN_DIGITS]; /* A NULL Terminated String - Max length
                                          of 16 */
} PACKED_POSTFIX  PswRawMdn_APIStruct;

typedef PACKED_PREFIX struct
{
  bool Prl_Enable;
} PACKED_POSTFIX  PswPrlEnable_APIStruct;

typedef PACKED_PREFIX struct
{
  bool Home_System_Registration;
} PACKED_POSTFIX  PswHomeSysReg_APIStruct;

typedef PACKED_PREFIX struct
{
  bool Foreign_Sid_Registration;
} PACKED_POSTFIX  PswForeignSidReg_APIStruct;

typedef PACKED_PREFIX struct
{
  bool Foreign_Nid_Registration;
} PACKED_POSTFIX  PswForeignNidReg_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 Accolc;
} PACKED_POSTFIX  PswAccolc_APIStruct;

typedef enum
{
  ENG_PREF_SYS_A,
  ENG_PREF_SYS_B,
  ENG_PREF_SYS_A_ONLY,
  ENG_PREF_SYS_B_ONLY,
  CDMA_PREF_AorB_END
} PswCdmaPrefAorB;

typedef PACKED_PREFIX struct
{
  PswCdmaPrefAorB CDMAAorB;
} PACKED_POSTFIX  PswPrefCDMA_AorB_APIStruct;

typedef enum
{
  ENG_PREF_BAND_0,
  ENG_PREF_BAND_1,
  ENG_PREF_BAND_0_ONLY,
  ENG_PREF_BAND_1_ONLY,
  ENG_PREF_BAND_2_ONLY,
  ENG_PREF_BAND_3_ONLY,
  ENG_PREF_BAND_4_ONLY,
  ENG_PREF_BAND_5_ONLY,
  ENG_PREF_BAND_6_ONLY,
  ENG_PREF_BAND_7_ONLY,
  ENG_PREF_BAND_8_ONLY,
  ENG_PREF_BAND_9_ONLY,
  ENG_PREF_BAND_10_ONLY,
  ENG_PREF_BAND_11_ONLY,
  ENG_PREF_BAND_12_ONLY,
  ENG_PREF_BAND_13_ONLY,
  ENG_PREF_BAND_14_ONLY,
  ENG_PREF_BAND_15_ONLY,
  ENG_PREF_BAND_16_ONLY,
  ENG_NO_PREF_BAND=255,
  CDMA_PREF_BAND_END
} PswCdmaPrefBand;

typedef PACKED_PREFIX struct
{
  PswCdmaPrefBand prefBand;
} PACKED_POSTFIX  PswPrefCDMA_Band_APIStruct;

typedef PACKED_PREFIX struct
{
  bool   pref_only;
  uint16 prlId;
  uint8  prl_prev;
  uint8  defaultRoamInd;
} PACKED_POSTFIX  PswPrlInfo_APIStruct;

typedef PACKED_PREFIX struct
{
  bool OTAPAEnable;
} PACKED_POSTFIX  PswOTAPAEnable_APIStruct;

typedef PACKED_PREFIX struct
{
  bool SPCChangeEnable;
} PACKED_POSTFIX  PswSPCChangeEnable_APIStruct;

typedef PACKED_PREFIX struct
{
  bool SPASMEnable;
} PACKED_POSTFIX  PswSPASMEnable_APIStruct;

typedef PACKED_PREFIX struct
{
  bool EnableOTA;
} PACKED_POSTFIX PswEnableOTA_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 Mode; /* See enum OtaCompletePowerupModeType */
} PACKED_POSTFIX  PswOTAPowerupMode_APIStruct;

typedef PACKED_PREFIX struct
{
  int32 BASE_LAT_REGsp;
  int32 BASE_LONG_REGsp;
  uint16 REG_DIST_REGsp;
} PACKED_POSTFIX  PswDistanceBasedParms_APIStruct;

typedef PACKED_PREFIX struct
{
  bool ValidSidNidEntry;
  uint16    sid;          /* system id                   */
  uint16    nid;          /* network id                  */
  uint16    ageTimer;     /* age limit in seconds        */
  bool      timerEnabled;
  uint8     block;        /* PCS block or serving system */
  SysCdmaBandT bandClass;
} PACKED_POSTFIX  PswSidNidListsp_APIStruct;


typedef PACKED_PREFIX struct
{
  uint8 Len;
  uint8 A21MsSubscriptionInfo[PSW_MAX_A21_MS_SUBS_INFO_LEN];
} PACKED_POSTFIX  PswA21MsSubscriptionInfo_APIStruct;

typedef PACKED_PREFIX struct
{
  BOOL RoamingSetting;
} PACKED_POSTFIX  PswRoamingSetting_APIStruct;

#ifdef MTK_CBP
typedef PACKED_PREFIX struct
{
  /*
    *   ROAM_SETTING_DOMESTIC_VOICE 0x01  Sprint Allow voice roaming on domestic system
    *   ROAM_SETTING_DOMESTIC_DATA  0x02  Sprint Allow data roaming on domestic system
    *   ROAM_SETTING_INT_VOICE      0x04  Sprint Allow voice romaing on international system
    *   ROAM_SETTING_INT_DATA       0x08  Sprint Allow data roaming on international system
    *   ROAM_SETTING_LTE_DATA       0x10  Sprint Allow LTE data roaming, not used in C2K saved only for AP get */
  uint8 RoamingSettingBitMap;
} PACKED_POSTFIX  PswRoamingSettingBitMap_APIStruct;
#endif

typedef PACKED_PREFIX struct
{
  uint16 ServiceOption;
  bool   Supported;
} PACKED_POSTFIX  PswSOSupport_APIStruct;

typedef PACKED_PREFIX struct
{
  uint16 ServiceOption;
} PACKED_POSTFIX  PswGetSOSupport_APIStruct;

typedef enum
{
   DIV_OFF,
   DIV_ON_DYNAMIC,
   DIV_UNDEFINED,
   DIV_ON_STATIC
}PswPhyDiversityModeT; /* must map into DSPM API IC Mode */

typedef PACKED_PREFIX struct
{
  PswPhyDiversityModeT   EnabledDiv;
} PACKED_POSTFIX  PswDivSupport_APIStruct;

typedef PACKED_PREFIX struct
{
  bool SO73Enabled;
  bool SO73WBEnabled;
} PACKED_POSTFIX  PswSO73Control_APIStruct;

typedef PACKED_PREFIX struct
{
  bool   Cdma1xAdvEnabled;
} PACKED_POSTFIX  Psw1xAdvEnabled_APIStruct;

typedef PACKED_PREFIX struct
{
  uint8 Strength;
} PACKED_POSTFIX  PswDfsEcIoThresh_APIStruct;

typedef enum
{
   IC_NORMAL,
   IC_DBG_SRCH_DLY,
   IC_DBG_IC_DLY,
   IC_DBG_BYPASS,
   IC_OFF = 0xF
}PswPhyICModeT; /* must map into DSPM API IC Mode */

typedef PACKED_PREFIX struct
{
  PswPhyICModeT  ICMode;
} PACKED_POSTFIX  PswPhyICMode_APIStruct;
typedef enum
{
   MS_MODE_UNKNOWN,
   MSA,
   MSB,
   MSS,
   /*ControlPlane=4 is changed to ControlPlne=8, for klocwork warning,corresponding to ValGpsFixModeT*/
   ControlPlane=8,
#ifdef MTK_CBP
   SUPL_MSA,
   SUPL_MSB,
#endif
}GPS_FIX_MODE;

typedef PACKED_PREFIX struct
{
    uint32  InstanceID;
    GPS_FIX_MODE FixMode;
    UINT32  NumFixes;
    uint32  TimeBFixes;
    uint32  HorizontalAccuracy;
    uint32  VerticalAccuracy;
#ifdef MTK_CBP
    uint32  PseudorangeAccuracy;
    bool    OnCTNtwk;
#ifdef MTK_DEV_GPSONE_ON_LTE
    int32   RatMode;
#endif
#endif
    uint32  Performance;
    uint8   GpsMode;
    uint8   flag_gps;    //0--AP, 1--3rd,  2--others
} PACKED_POSTFIX  PswIs801SessionStartMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint8     flag_gps;    //0--AP, 1--3rd,  2--others
} PACKED_POSTFIX PswIs801SessCancelReqMsgT;

typedef PACKED_PREFIX struct
{
   uint32 Index;
} PACKED_POSTFIX PswIs801TimerExpMsgT;

typedef PACKED_PREFIX struct
{
   bool Success;
} PACKED_POSTFIX PswIs801TcpConnStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint32 EphSysTime;
   uint32 AlmSysTime;
   uint32 EphTimeInterval;
   uint32 AlmTimeInterval;
} PACKED_POSTFIX PswIs801EphAlmSystimeMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX PswIs801MssDoneMsgT;

typedef PACKED_PREFIX struct
{
   uint8 enable;
} PACKED_POSTFIX PswIs801NmeaOutputSettingMsgT;

typedef PACKED_PREFIX struct
{
   uint8 Conn;
   uint32 InstID;
} PACKED_POSTFIX PswIs801MpcConnStatusMsgT;

typedef struct
{
   uint8 CustomerId;
}PswIs801CustomerSettingMsgT;

#ifdef MTK_CBP
typedef struct
{
   uint16 refPn;
   uint32 refTime;
   uint16 timeOffset;
}PswLecSessionEndRspMsgT;
#endif

typedef PACKED_PREFIX struct
{
   uint16 PrefHomeMOServiceOption;
} PACKED_POSTFIX PswPrefHomeMOSO_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16 PrefRoamMOServiceOption;
} PACKED_POSTFIX PswPrefRoamMOSO_APIStruct;

typedef PACKED_PREFIX struct
{
   uint16 PrefHomeMTServiceOption;
} PACKED_POSTFIX PswPrefHomeMTSO_APIStruct;


typedef PACKED_PREFIX struct
{
   bool isVoiceServiceAllowed;
} PACKED_POSTFIX  PswVoiceMode_APIStruct;

typedef PACKED_PREFIX struct
{
   bool isDdtmEnabled;
} PACKED_POSTFIX  PswDdtmEnable_APIStruct;

typedef PACKED_PREFIX struct
{
   bool Enabled;
} PACKED_POSTFIX  PswFeatureEnabled_APIStruct;

typedef PACKED_PREFIX struct
{
   bool   Enabled;
   UINT16 ThreshLow;
   UINT16 ThreshHigh;
} PACKED_POSTFIX  PswQpchEnableDynamicCtrl_APIStruct;

#ifdef FIELD_TEST_DATA

typedef PACKED_PREFIX struct
{
  uint16  SchMux;
  uint8   SchRc;
  uint8   SchStatus;
  uint16  SchDuration;
  uint8   SchRate;
} PACKED_POSTFIX  PswSchCcT;

typedef PACKED_PREFIX struct
{
  uint8       cpState;          /*CP state*/
#ifdef MTK_DEV_ENGINEER_MODE
 #if defined(SYS_FLASH_LESS_SUPPORT) && defined(MTK_DEV_VERSION_CONTROL)
  RfFileCalStateT rfFileCalState;  /* rfFileCalState */
 #endif
#endif
  uint16      channel;          /* Current channel number */
  uint16      bandClass;        /* Current band class  */
  uint16      pilotPNOffset;    /* Pilot PN offset  */
  uint8       pageChannel;      /* Current page channel Walsh code*/
  uint8       pageChanRate;     /*page channel rate, 0 is full rate(9600), 1 is half rate(4800)*/
  uint8       block;             /* Current CDMA block */
  uint16      sid;                /* System ID */
  uint16      nid;                /* Network ID */
  uint8       SysDetIndication; /* Reason for entering System Determination */
  uint8       roam;              /* Roam status */
  uint16      baseId;            /* Base station ID */
  uint16      regZone;           /* registration zone  */
  uint32      baseLat;           /* latitude from base last registered on    */
  uint32      baseLong;          /* longitude from base last registered on   */
  uint8       P_REV;             /* Protocol Revision Level (from Sync Channel Message)*/
  uint8       MIN_P_REV;         /* Minimum protocol revision required by MS (from Sync Channel Message)*/
  uint8       networkPrefSCI;   /* max slot cycle index value */
  bool        slotMode;          /* slotted Mode (Enabled Or Disable) */
  bool        qpchMode;          /* qpch Mode (Enabled Or Disable) */
  uint16      mcc;               /* Mobile Country Code (from extended system parameter message)*/
  uint8       imsi_11_12;        /* Mobile Network Code (from extended system parameter message)*/
  uint8       SSPR_P_REV_INUSE;  /* Current PRL Protocol Revision */

  uint8       currentPacketZoneID; /* Current packet zone ID */
  uint16      serviceOption;    /* Current Service Option */
  PswSchCcT   forSch0Cc;        /* Channel configuration of forward supplement channel */
  PswSchCcT   revSch0Cc;        /* channel configuration of reverse supplement channel */

  int16       rxPower;          /* Rx power */
  int16       txPower;          /* Tx power */
  uint16      pilotPN[SYS_CP_MAX_ACTIVE_LIST_PILOTS];   /* Pilot PN of the list in active set */
  uint16      pilotEcIo[SYS_CP_MAX_ACTIVE_LIST_PILOTS]; /* Pilot Ec/Io of the list in active set */
  uint16      pilotPhase[SYS_CP_MAX_ACTIVE_LIST_PILOTS]; /* Pilot phase of the list in active set*/
  uint8       numInActiveSet;  /* Number of records in active set */
  uint16      candPilotPN[SYS_CP_MAX_CANDIDATE_LIST_PILOTS];   /* Pilot PN of the list in candidate set */
  uint16      candPilotEcIo[SYS_CP_MAX_CANDIDATE_LIST_PILOTS]; /* Pilot Ec/Io of the list in candidate set */
  uint16      candPilotPhase[SYS_CP_MAX_CANDIDATE_LIST_PILOTS]; /* Pilot phase of the list in candidate set*/
  uint8       numInCandSet;  /* Number of records in candidate set */
  uint16      nghbrPilotPN[SYS_CP_MAX_NEIGHBOR_LIST_PILOTS];   /* Pilot PN of the list in neighbor set */
  uint16      nghbrPilotEcIo[SYS_CP_MAX_NEIGHBOR_LIST_PILOTS]; /* Pilot Ec/Io of the list in neighbor set */
  uint16      nghbrPilotPhase[SYS_CP_MAX_NEIGHBOR_LIST_PILOTS]; /* Pilot phase of the list in neighbor set*/
  uint8       numInNghbrSet;  /* Number of records in neighbor set */
  uint16      FER;                      /* Frame Error Rate per thousand */
  uint16      TxAdj;              /* Adjust gain of the Tx power */

  uint32      total_msg;        /* Number of paging ch messages received  */
  uint32      error_msg;        /* Number of paging ch messages with bad crc  */
  uint16      acc_1;             /* number of layer 3 requests messages generated */
  uint16      acc_2;             /* number of layer 3 response messages generated */
  uint16      acc_8;             /* number of unsuccessful access attempts        */

  uint16      dpchLoss_count;         /* Count of paging ch declared    */
  uint16      dtchLoss_count;         /* Count of dedicated traffic CH Loss*/
  uint32      idleHO_count;           /* Count of idle handoff*/
  uint16      hardHO_count;           /* Count of hard handoff*/
  uint16      interFreqIdleHO_count; /* Count of inter_Freq idle handoff*/
  uint16      MO_count;             /* Count of successful MS ORIGINATED      */
  uint16      MT_count;             /* Count of successful MS TERMINATED      */
  uint16      powerDownReg_count;  /* Count of power down registration      */
  uint16      timeBasedReg_count;  /* Count of time based registration       */
  uint16      parameterReg_count;  /* Count of parameter change registration   */
  uint16      powerUpReg_count;    /* Count of power up registration    */
  uint16      orderedReg_count;    /* Count of ordered registration     */
  uint16      zoneBasedReg_count;  /* Count of zone based registration */
  uint16      distanceBasedReg_count; /* Count of distance based registration */
  uint16      silentryRetryTimeout_count; /* Count of silentry retry timeout */
  uint16      T40_count;  /* Count of T40m timeout */
  uint16      T41_count;  /* Count of T41m timeout */
  uint16      T50_count;  /* Count of T50m timeout */
  uint16      T51_count;  /* Count of T51m timeout */
  uint16      T52_count;  /* Count of T52m timeout */
  uint16      T55_count;  /* Count of T55m timeout */

  uint16      T_ADD;
  uint16      T_DROP;
  uint16      T_COMP;
  uint16      T_tDROP;
 } PACKED_POSTFIX PswFTDParaT;
#endif

#ifdef MTK_DEV_ENGINEER_MODE
/* This structure is defined for Engineer mode developement, all items in it is 1xEV-DO related and needs to be reported to AP.*/
typedef PACKED_PREFIX struct
{
  uint8       bandClass;  /* 1xEV-DO current band class. */
  uint16      channel;  /* 1xEV-DO current channel number. */
  uint16      pilotPN;  /* 1xEV-DO  pilot PN. */
  uint8       phySubtype;  /* 1xEV-DO phy subtype. */
  double      rssi_dbm;  /* 1xEV-DO RSSI_dbm.  */
  uint8       sectorID[16];  /* 1xEV-DO Sector ID. */
  uint8       subnetMask;  /* 1xEV-DO subnetmask. */
  uint8       colorCode;  /* 1xEV-DO color Code. */
  uint8       uati[16];  /* 1xEV-DO UATI 024. */
  uint8       pilotInc;  /* 1xEV-DO pilotInc. */
  uint8       activeSetSchWin;  /* 1xEV-DO activeSet Search Window. */
  uint8       neighborSetSchWin;  /* 1xEV-DO neighborSet Search Window. */
  uint8       remainSetSchWin;  /* 1xEV-DO remainSet Search Window. */
  uint8       sameFreq_T_ADD;  /* 1xEV-DO T_ADD with same channel. */
  uint8       sameFreq_T_DROP;  /* 1xEV-DO T_DROP with same channel. */
  uint8       sameFreq_T_tDROP;  /* 1xEV-DO T_tDROP with same channel. */
  uint8       diffFreq_T_ADD;  /* 1xEV-DO T_ADD with different channel. */
  uint8       diffFreq_T_DROP;  /* 1xEV-DO T_DROP with different channel. */
  uint8       diffFreq_T_tDROP;  /* 1xEV-DO T_tDROP with different channel. */

  uint8       numInActiveSet;  /* 1xEV-DO active set number. */
  uint16      activePilotPN[SYS_MAX_ACTIVE_LIST_PILOTS];  /* 1xEV-DO activePilotPN. */
  int16       activePilotEcIo[SYS_MAX_ACTIVE_LIST_PILOTS];  /* 1xEV-DO activePilotEcIo. */
  uint8       activeDrcCover[SYS_MAX_ACTIVE_LIST_PILOTS];  /* 1xEV-DO activeDrcCover. */

  uint8       numInCandSet;  /* 1xEV-DO candidate set number. */
  uint8       candBand[SYS_MAX_CANDIDATE_LIST_PILOTS];  /* 1xEV-DO band of candidate set.  */
  uint16      candChannel[SYS_MAX_CANDIDATE_LIST_PILOTS];  /* 1xEV-DO channel of candidate set. */
  uint16      candPilotPN[SYS_MAX_CANDIDATE_LIST_PILOTS];  /* 1xEV-DO pilotPN of candidate set. */
  int16       candPilotEcIo[SYS_MAX_CANDIDATE_LIST_PILOTS];  /* 1xEV-DO pilotEcIo of candidate set. */

  uint8       numInNghbrSet;  /* 1xEV-DO neighbor set numer.  */
  uint8       nghbrBand[SYS_MAX_NEIGHBOR_LIST_PILOTS];  /* 1xEV-DO band of neighbor set. */
  uint16      nghbrChannel[SYS_MAX_NEIGHBOR_LIST_PILOTS];  /* 1xEV-DO channel of neighbor set. */
  uint16      nghbrPilotPN[SYS_MAX_NEIGHBOR_LIST_PILOTS];  /* 1xEV-DO pilotPN of neighbor set. */
  int16       nghbrPilotEcIo[SYS_MAX_NEIGHBOR_LIST_PILOTS];  /* 1xEV-DO pilotEcIo of neighbor set. */

  int32       c_i;  /* 1xEV-DO C/I.  */
  uint8       drcAverageValue;  /* 1xEV-DO drcAverageValue. */
  uint32      ftcCrcErrorCount;  /* 1xEV-DO ftcCrcErrorCount. */
  uint32      ftcTotalCount;  /* 1xEV-DO ftcTotalCount. */
  uint8       syncCrcErrorRatio;  /* 1xEV-DO syncCrcErrorRatio. */

  uint16      averageTbsize;  /* 1xEV-DO averageTbsize. */
  uint32      rtcRetransmitCount;  /* 1xEV-DO rtcRetransmitCount. */
  uint32      rtcTransmitTotalCount;  /* 1xEV-DO rtcTransmitTotalCount. */
  int16       txPower;  /* 1xEV-DO txPower.*/
  int16       pilotPower;  /* 1xEV-DO pilotPower. */
  uint8       rab_1_ratio;  /* 1xEV-DO rab_1_ratio. */

  uint8       sessionState;  /* 1xEV-DO Session state. */
  uint8       atState;  /* 1xEV-DO AT state. */
  uint8       almpState;  /* 1xEV-DO ALMP state. */
  uint8       inspState;  /* 1xEV-DO INSP state. */
  uint8       idpState;  /* 1xEV-DO IDP state. */
  uint8       ompState;  /* 1xEV-DO OMP state. */
  uint8       cspState;  /* 1xEV-DO CSP state. */
  uint8       rupState;  /* 1xEV-DO RUP state. */
} PACKED_POSTFIX PswFTDDoParaT;
#endif

MobileIDType ValGetMobileIdType(void);
/**********MS-Based*********************/
typedef PACKED_PREFIX struct _GPS_POSITION_ERROR {
DWORD	dwHorizontalErrorAlong;
DWORD dwHorizontalErrorAngle;
DWORD dwHorizontalErrorPerp;
DWORD dwVerticalError;
DWORD dwHorizontalConfidence;
} PACKED_POSTFIX  GPS_POSITION_ERROR, PACKED_POSTFIX *PGPS_POSITION_ERROR;

typedef enum
{
	GPS_FIX_INVALID,
	GPS_FIX_NORMAL,
GPS_FIX_DIEF
} GPS_FIX_QUALITY;



typedef enum
{
   GPS_FIX_NONE,
   GPS_FIX_ESTIMATED,
   GPS_FIX_2D,
   GPS_FIX_DIFF_2D,
   GPS_FIX_3D,
   GPS_FIX_DIFF_3D

}  GPS_FIX_TYPE;


typedef enum
{
	GPS_FIX_3D_AUTO,
	GPS_FIX_2D_MANU
}GPS_FIX_SELECTION;


typedef  struct GPS_Nav_Data_Struct   /*GN GPS Navigation solution data*/
{
   DWORD Local_TTag;             /* Local receiver time-tag since start-up [msec]*/
   DWORD OS_Time_ms;             /* Local Operating System Time [msec]*/
   WORD Year;                   /* UTC Year A.D.                     [eg 2006]*/
   WORD Month;                  /* UTC Month into the year           [range 1..12]*/
   WORD Day;                    /* UTC Days into the month           [range 1..31]*/
   WORD Hours;                  /* UTC Hours into the day            [range 0..23]*/
   WORD Minutes;                /* UTC Minutes into the hour         [range 0..59]*/
   WORD Seconds;                /* UTC Seconds into the hour         [range 0..59]*/
   WORD Milliseconds;           /* UTC Milliseconds into the second  [range 0..999]*/
   signed short Gps_WeekNo;             /* GPS Week Number*/
#if !(defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT))
   double Gps_TOW;                /* Corrected GPS Time of Week [seconds]*/
   double UTC_Correction;         /* Current (GPS-UTC) time difference [seconds]*/
   double X;                      /* WGS84 ECEF X Cartesian coordinate [m]*/
   double Y;                      /* WGS84 ECEF Y Cartesian coordinate [m]*/
   double Z;                      /* WGS84 ECEF Z Cartesian coordinate [m]*/
   double Latitude;               /* WGS84 Latitude  [degrees, positive North]*/
   double Longitude;              /* WGS84 Longitude [degrees, positive East]*/
#else
    float Gps_TOW;                /* Corrected GPS Time of Week [seconds]*/
    float UTC_Correction;         /* Current (GPS-UTC) time difference [seconds]*/
    float X;                      /* WGS84 ECEF X Cartesian coordinate [m]*/
    float Y;                      /* WGS84 ECEF Y Cartesian coordinate [m]*/
    float Z;                      /* WGS84 ECEF Z Cartesian coordinate [m]*/
    float Latitude;               /* WGS84 Latitude  [degrees, positive North]*/
    float Longitude;              /* WGS84 Longitude [degrees, positive East]*/
#endif
   float Altitude_Ell;           /* Altitude above WGS84 Ellipsoid [m]*/
   float Altitude_MSL;           /* Altitude above Mean Sea Level [m]*/
   float SpeedOverGround;        /* 2-dimensional Speed Over Ground [m/s]*/
   float CourseOverGround;       /* 2-dimensional Course Over Ground [degrees]*/
   float VerticalVelocity;       /* Vertical velocity [m/s]*/
   float N_AccEst;               /* Northing 1-sigma (67%) Accuracy estimate [m]*/
   float E_AccEst;               /* Easting  1-sigma (67%) Accuracy estimate [m]*/
   float V_AccEst;               /* Vertical 1-sigma (67%) Accuracy estimate [m]*/
   float H_AccMaj;               /* Horizontal error ellipse semi-major axis [m]*/
   float H_AccMin;               /* Horizontal error ellipse semi-minor axis [m]*/
   float H_AccMajBrg;            /* Bearing of the semi-major axis [degrees]*/
   float HVel_AccEst;            /* Horizontal Velocity 1-sigma (67%) Accuracy estimate [m/s]*/
   float VVel_AccEst;            /* Vertical Velocity 1-sigma (67%) Accuracy estimate [m/s]*/
   float PR_ResRMS;              /* Standard dev of the PR a posteriori residuals [m]*/
   float H_DOP;                  /* Horizontal Dilution of Precision*/
   float V_DOP;                  /* Vertical Dilution of Precision*/
   float P_DOP;                  /* 3-D Position Dilution of Precision*/

   GPS_FIX_TYPE        FixType; /* Position fix type*/
  BOOL Valid_2D_Fix;           /* Is the published 2D position fix "valid"*/
                              /*    relative to the required Horizontal*/
                              /*    accuracy masks ?*/
   BOOL Valid_3D_Fix;           /* Is the published 3D position fix "valid"*/
                              /*    relative to both the required Horizontal*/
                              /*    and Vertical accuracy masks ?*/
   unsigned char  FixMode;                /* Solution Fixing Mode*/
                              /*    1 = Forced 2-D at MSL,*/
                              /*    2 = 3-D, with automatic fall back to 2-D*/

   unsigned char  SatsInView;             /* Satellites in View count*/
   unsigned char  SatsUsed;               /* Satellites in Used for Navigation count*/

   unsigned char  SatsInViewSVid[NMEA_SV];/* Satellites in View SV id number [PRN]*/
   unsigned char  SatsInViewSNR[NMEA_SV]; /* Satellites in Signal To Noise Ratio [dBHz]*/
   unsigned char  SatsInViewJNR[NMEA_SV]; // Satellites in View Jammer to Noise Ratio [dBHz]
   WORD SatsInViewAzim[NMEA_SV];/* Satellites in View Azimuth [degrees]*/
   signed char  SatsInViewElev[NMEA_SV];/* Satellites in View Elevation [degrees]*/
                              /*    if = -99 then Azimuth & Elevation angles*/
                              /*         are currently unknown*/
   BOOL SatsInViewUsed[NMEA_SV];/* Satellites in View Used for Navigation ?*/

} s_GPS_Nav_Data;          /* GN GPS Navigation solution data*/



typedef  struct
{
	bool MSBFix;
	s_GPS_Nav_Data GPS_NAV_Data;
}PswLocRspDataT;




typedef PACKED_PREFIX enum
{
GPSENG_FIX_NOT_VALID,
GPSENG_FIX_ESTIMATED,
GPSENG_FIX_2D,
GPSENG_FIX_3D,
GPSENG_FIX_DGPS_2D,
GPSENG_FIX_DGPS_3D
} PACKED_POSTFIX  PswGpsEngFixTypeT;

typedef PACKED_PREFIX struct
{
bool	Used;
uint8	Id;
uint8	Cn0;
int8	Elev;
uint16	Azimuth;
} PACKED_POSTFIX  PswGpsEngSvInViewT;

typedef PACKED_PREFIX struct {
PswGpsEngFixTypeT     FixType;
uint32	OsTime;
uint32	LocalTimeTag;
uint16	UtcYear;
	uint16	UtcMonth;
uint16	UtcDay;
uint16	UtcHour;
	uint16	UtcMin;
uint16	UtcSec;
uint16	UtcMilliSec;		/* UTC Millisec into Sec */
uint16	WeekNo;
uint32	Tow;			/* milliseconds */
	uint8	UtcDifference;		/* (GPS-UTC) seconds */
	double	Lat;
	double Long;
	double	AltitudeMSL;		/* Mean Sea level */
	double AltitudeElli;		/* ? */
	double	X;
	double Y;
double	Z;
	float	SpeedGround;
	float	CourseGround;
	float	VerticalVelocity;	/* Vertical velocity */
float	LocUncAng;		/* Location uncertainty angle */
	float	LocUncA;		/* ? */
float	LocUncP;
float	LocUncV;
float	VelHUnc;		/* Horizontal Velocity RMS 1-sigma (67%) Accuracy estimate */
float	VelVUnc;		/* Vertical Velocity RMS 1-sigma (67%) Accuracy estimate */
float	Pdop;
float	Hdop;
float	Vdop;
uint8	SvInViewNum;
uint8	SvUsed;
PswGpsEngSvInViewT  SvInView[MAX_SVINVIEW];
} PACKED_POSTFIX  PswGpsEngWholeNavDataMsgT;





/*MS-Based*/

typedef PACKED_PREFIX struct
{
bool GpsSysReady;

} PACKED_POSTFIX LecGpsSysReadyT;

typedef PACKED_PREFIX struct
{
	bool  Ref_Time_Req;
	bool  Ref_Pos_Req;
	bool  Ion_Req;
	bool  UTC_Req;
	bool  SV_Health_Req;
	bool  Bad_SV_List_Req;
	bool  Alm_Req;
	bool  Eph_Req;

} PACKED_POSTFIX PswAssistReqT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
   uint8 mode;
} PACKED_POSTFIX PswValRestartMsgT;

typedef enum
{
    PSW_LOC_HOT_START  = 1,
    PSW_LOC_WARM_START,
    PSW_LOC_COLD_START
}PswLocStartMode;

typedef PACKED_PREFIX struct
{
    PswLocStartMode startMode;
} PACKED_POSTFIX PswLocStartModeSetMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT  oldRspInfo;
} PACKED_POSTFIX PswValLocFixModeGetMsgT;

typedef PACKED_PREFIX struct
{
    GPS_FIX_MODE FixMode;
    ExeRspMsgT  oldRspInfo;
} PACKED_POSTFIX PswValLocFixModeGetRspMsgT;

typedef struct
{
  uint16 PrefHomeMOServiceOption;
  uint16 PrefHomeMTServiceOption;
  uint16 PrefRoamMOServiceOption;
} PswValPrefVoiceServiceOptionMsgT;

typedef PACKED_PREFIX struct
{
  bool SlottedModeAllowed;
} PACKED_POSTFIX  PswSlottedModeAllowed_APIStruct;

typedef PACKED_PREFIX struct
{
  UINT8 Mode; /* 0=Stop/Disable, 0xFF=report on thresholds/state transition only */
              /* other values = frequency of reporting when below low thresh in  */
              /* units of about 200 ms (i.e set to 5 for 1 sec reporting)        */
} PACKED_POSTFIX  PswSetTransmitInfoMsgT;

typedef enum
{
  TRANS_INFO_IDLE, /* Stopped transmitting */
  TRANS_INFO_ACCESS, /* Transmitting on access channel */
  TRANS_INFO_TRAFFIC /* Transmitting on traffic channel */
} TransInfoState;

typedef PACKED_PREFIX struct
{
  TransInfoState State; /* Fer is not valid in TRANS_INFO_ACCESS state */
  SysCdmaBandT Band; /* Current CDMA band class */
  UINT16 Channel; /* Current CDMA channel number */
  INT16 Rssi; /* Receive signal level dBm */
  UINT8 PilotEcIo; /* Pilot energy to total receive power 1/8 unit dB */
  INT16 TxPwr; /* Transmit Power dBm */
  UINT16 TotalFrames;
  UINT16 BadFrames;
} PACKED_POSTFIX  PswGetTransmitInfoRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   SysCdmaBandT band;
   uint16 channel;
   uint8  codeChan;
   uint8  radioConfig;
   uint16 numFrames;
   bool   enableAFC;
} PACKED_POSTFIX  PswNstPowerupMsgT;

#define MAX_NST_ACK_CHAR  3
typedef PACKED_PREFIX struct
{
   char ackResponse[MAX_NST_ACK_CHAR];
} PACKED_POSTFIX  PswNstPowerupAckRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 badFrames;
   uint16 totalFrames;
} PACKED_POSTFIX  PswNstPowerupRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8 fwdRc;
   uint8 pwrCtrlMode;
   int16 txPwr;
} PACKED_POSTFIX  PswNstTransmitTchMsgT;

typedef PACKED_PREFIX struct
{
   char ackResponse[MAX_NST_ACK_CHAR];
} PACKED_POSTFIX  PswNstTransmitTchAckMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  PswNstExitMsgT;

#ifdef MTK_CBP
#define MAX_NUM_LISTS       50
#define MAX_NUM_RSSI_LEVEL  10
#define MAX_NUM_TX_PWR_LEVEL  10
#else
#define MAX_NUM_LISTS  10
#endif

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8 count;  /* Total of list */
   uint8 index[MAX_NUM_LISTS];  /* Index of list */
   uint8 protocol[MAX_NUM_LISTS];
   uint8 offset[MAX_NUM_LISTS];   /* Num of frame offset */
   SysCdmaBandT band[MAX_NUM_LISTS];
   uint16 channel[MAX_NUM_LISTS];
   uint8 codeChan[MAX_NUM_LISTS];
   uint8 radioConfig[MAX_NUM_LISTS];
   uint16 numFrames[MAX_NUM_LISTS];
   uint8 pwrCtrlMode[MAX_NUM_LISTS];
   int16 txPwr[MAX_NUM_LISTS];
} PACKED_POSTFIX  PswNstListSetMsgT;



#ifdef MTK_CBP
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   SysCdmaBandT band;
   uint16 channel;
} PACKED_POSTFIX  PswNstRxPwrReqMsgT;


typedef PACKED_PREFIX struct
{
   bool          isSuccess;
   uint16        PnOffset;
   int16         Strength;
   int16         MainRxPwrQ5;
   int16         DivRxPwrQ5;
} PACKED_POSTFIX  PswNstRxPwrRespMsgT;


typedef PACKED_PREFIX struct
{
   uint8         Count;
   uint8         Index;
   uint8         TotalLevel;
   int16         MainRxPwrQ5[MAX_NUM_LISTS];
   int16         DivRxPwrQ5[MAX_NUM_LISTS];
} PACKED_POSTFIX  PswNstListRxpwrRptMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8      txPwrCount;     /**txPwrCount should less than MAX_NUM_TX_PWR_LEVEL*/
   int16      txPwrLevel[MAX_NUM_TX_PWR_LEVEL]; /**unit of txPwrLevel is Q6 dBm*/
} PACKED_POSTFIX  PswNstTxPwrLevelSetMsgT;

typedef PACKED_PREFIX struct
{
   bool          isSuccess;
} PACKED_POSTFIX  PswNstTxPwrLevelSetRespMsgT;

#endif

/* 1XCSFB, 3GPP2 C.S0097, Section 3 */
typedef PACKED_PREFIX struct
{
  BOOL    SIDIncluded;
  UINT16  SID;
  BOOL    NIDIncluded;
  UINT16  NID;
  BOOL    MULT_SIDSIncluded;
  BOOL    MULT_SIDS;
  BOOL    MULT_NIDSIncluded;
  BOOL    MULT_NIDS;
  BOOL    REG_ZONEIncluded;
  UINT16  REG_ZONE;
  BOOL    TOTAL_ZONESIncluded;
  UINT8   TOTAL_ZONES;
  BOOL    ZONE_TIMERIncluded;
  UINT8   ZONE_TIMER;
  BOOL    PACKET_ZONE_IDIncluded;
  UINT8   PACKET_ZONE_ID;
  BOOL    PZIDHystParametersIncluded;
  BOOL    PZ_HYST_ENABLED;
  BOOL    PZ_HYST_INFO_INCL;
  BOOL    PZ_HYST_LIST_LEN;
  UINT8   PZ_HYST_ACT_TIMER;
  UINT8   PZ_HYST_TIMER_MUL;
  UINT8   PZ_HYST_TIMER_EXP;
  BOOL    P_REVIncluded;
  UINT8   P_REV;
  BOOL    MIN_P_REVIncluded;
  UINT8   MIN_P_REV;
  BOOL    NEG_SLOT_CYCLE_INDEX_SUPIncluded;
  BOOL    NEG_SLOT_CYCLE_INDEX_SUP;
  BOOL    ENCRYPT_MODEIncluded;
  UINT8   ENCRYPT_MODE;
  BOOL    ENC_SUPPORTEDIncluded;
  BOOL    ENC_SUPPORTED;
  BOOL    SIG_ENCRYPT_SUPIncluded;
  UINT8   SIG_ENCRYPT_SUP;
  BOOL    MSG_INTEGRITY_SUPIncluded;
  BOOL    MSG_INTEGRITY_SUP;
  BOOL    SIG_INTEGRITY_SUP_INCLIncluded;
  BOOL    SIG_INTEGRITY_SUP_INCL;
  BOOL    SIG_INTEGRITY_SUPIncluded;
  UINT8   SIG_INTEGRITY_SUP;
  BOOL    AUTHIncluded;
  UINT8   AUTH;
  BOOL    MAX_NUM_ALT_SOIncluded;
  UINT8   MAX_NUM_ALT_SO;
  BOOL    USE_SYNC_IDIncluded;
  BOOL    USE_SYNC_ID;
  BOOL    MS_INIT_POS_LOC_SUP_INDIncluded;
  BOOL    MS_INIT_POS_LOC_SUP_IND;
  BOOL    MOB_QOSIncluded;
  BOOL    MOB_QOS;
  BOOL    BAND_CLASS_INFO_REQIncluded;
  BOOL    BAND_CLASS_INFO_REQ;
  BOOL    BAND_CLASSIncluded;
  UINT8   BAND_CLASS;
  BOOL    BYPASS_REG_INDIncluded;
  UINT8   BYPASS_REG_IND;
  BOOL    ALT_BAND_CLASSIncluded;
  UINT8   ALT_BAND_CLASS;
  BOOL    MAX_ADD_SERV_INSTANCEIncluded;
  UINT8   MAX_ADD_SERV_INSTANCE;
  BOOL    HOME_REGIncluded;
  BOOL    HOME_REG;
  BOOL    FOR_SID_REGIncluded;
  BOOL    FOR_SID_REG;
  BOOL    FOR_NID_REGIncluded;
  BOOL    FOR_NID_REG;
  BOOL    POWER_UP_REGIncluded;
  BOOL    POWER_UP_REG;
  BOOL    POWER_DOWN_REGIncluded;
  BOOL    POWER_DOWN_REG;
  BOOL    PARAMETER_REGIncluded;
  BOOL    PARAMETER_REG;
  BOOL    REG_PRDIncluded;
  UINT8   REG_PRD;
  BOOL    REG_DISTIncluded;
  UINT16  REG_DIST;
  BOOL    PREF_MSID_TYPEIncluded;
  UINT8   PREF_MSID_TYPE;
  BOOL    EXT_PREF_MSID_TYPEIncluded;
  UINT8   EXT_PREF_MSID_TYPE;
  BOOL    MEID_REQDIncluded;
  BOOL    MEID_REQD;
  BOOL    MCCIncluded;
  UINT16  MCC;
  BOOL    IMSI_11_12Included;
  UINT8   IMSI_11_12;
  BOOL    IMSI_T_SUPPORTEDIncluded;
  BOOL    IMSI_T_SUPPORTED;
  BOOL    RECONNECT_MSG_INDIncluded;
  BOOL    RECONNECT_MSG_IND;
  BOOL    RER_MODE_SUPPORTEDIncluded;
  BOOL    RER_MODE_SUPPORTED;
  BOOL    TKZ_MODE_SUPPORTEDIncluded;
  BOOL    TKZ_MODE_SUPPORTED;
  BOOL    TKZ_IDIncluded;
  BOOL    TKZ_ID;
  BOOL    PILOT_REPORTIncluded;
  BOOL    PILOT_REPORT;
  BOOL    SDB_SUPPORTEDIncluded;
  BOOL    SDB_SUPPORTED;
  BOOL    AUTO_FCSO_ALLOWEDIncluded;
  BOOL    AUTO_FCSO_ALLOWED;
  BOOL    SDB_IN_RCNM_INDIncluded;
  BOOL    SDB_IN_RCNM_IND;
  BOOL    FPC_FCH_Included;
  UINT8   FPC_FCH_INIT_SETPT_RC3;
  UINT8   FPC_FCH_INIT_SETPT_RC4;
  UINT8   FPC_FCH_INIT_SETPT_RC5;
  UINT8   FPC_FCH_INIT_SETPT_RC11;
  UINT8   FPC_FCH_INIT_SETPT_RC12;
  BOOL    T_ADD_Included;
  UINT8   T_ADD;
  BOOL    PILOT_INC_Included;
  UINT8   PILOT_INC;
  BOOL    RAND_Included;
  UINT32  RAND;
  BOOL    LP_SEC_Included;
  UINT8   LP_SEC;
  BOOL    LTM_OFF_Included;
  UINT8   LTM_OFF;
  BOOL    DAYLT_Included;
  BOOL    DAYLT;
  BOOL    GCSNAL2AckTimer_Included;
  UINT8   GCSNAL2AckTimer;
  BOOL    GCSNASequenceContextTimer_Included;
  UINT8   GCSNASequenceContextTimer;
} PACKED_POSTFIX  PswCDMA2000Parm;

typedef PACKED_PREFIX struct
{
  UINT16  sid;
  UINT16  nid;
  BOOL    multipleSID;
  BOOL    multipleNID;
  BOOL    homeReg;
  BOOL    foreignSIDReg;
  BOOL    foreignNIDReg;
  BOOL    parameterReg;
  BOOL    powerUpReg;
  UINT8   registrationPeriod;
  UINT16  registrationZone;
  UINT8   totalZone;
  UINT8   zoneTimer;
  BOOL    powerDownReg;
} PACKED_POSTFIX  PswIratCSFBRegParm1xRTT;

typedef PACKED_PREFIX struct
{
  UINT8   ac_Barring0to9;
  UINT8   ac_Barring10;
  UINT8   ac_Barring11;
  UINT8   ac_Barring12;
  UINT8   ac_Barring13;
  UINT8   ac_Barring14;
  UINT8   ac_Barring15;
  UINT8   ac_BarringMsg;
  UINT8   ac_BarringReg;
  UINT8   ac_BarringEmg;
} PACKED_POSTFIX  PswAC_BarringConfig1xRTT;

typedef PACKED_PREFIX struct
{
  BOOL    sysTimeInfoIncl;
  UINT8   systemTimeInfo[SYS_SYSTIME_SIZE];
  BOOL    srchWinSizeIncl;
  UINT16  searchWindowSize;
  BOOL    csfb_RegParam1XRTTIncl;
  PswIratCSFBRegParm1xRTT csfb_RegParm1XRTT;
  BOOL    longCodeStateIncl;
  UINT8   longCodeState1XRTT[6];
  BOOL    cellReselParm1XRTTIncl;
  BOOL    csfb_SupportForDualRxUEIncl;
  BOOL    csfb_SupportForDualRxUE;
  BOOL    ac_BarringConfig1XRTTIncl;
  PswAC_BarringConfig1xRTT ac_BarringConfig1XRTT;
  UINT8   ListSize;
#ifdef MTK_DEV_C2K_IRAT
  IratSIB8MeasCellListT cellReselParm1XRTT[1];
#endif /* MTK_DEV_C2K_IRAT */
} PACKED_POSTFIX  PswSIB8Parm1xRTT;

typedef PACKED_PREFIX struct
{
   BOOL   inclRand;
   UINT32 rand;
   BOOL   inclMobParms;
   PswCDMA2000Parm *mobP;
} PACKED_POSTFIX  PswCsfbCDMA2000ParmMsgT;

typedef PACKED_PREFIX struct
{
   PswSIB8Parm1xRTT *sib8P;
} PACKED_POSTFIX  PswCsfbSib8ParmMsgT;

typedef PACKED_PREFIX struct
{
   UINT16 len;
   UINT8  *data;
} PACKED_POSTFIX  PswCsfbGcsnaDLMsgT;

typedef PACKED_PREFIX struct
{
   PswSIB8Parm1xRTT *sib8P;
} PACKED_POSTFIX  PswCsfbMeasReqMsgT;

typedef PACKED_PREFIX struct
{
   BOOL   redir;
#ifdef MTK_DEV_C2K_IRAT
   IratReSelectionCellListT *redirL;
#endif /* MTK_DEV_C2K_IRAT */
} PACKED_POSTFIX  PswCsfbRedirectMsgT;

typedef PACKED_PREFIX struct
{
   BOOL   inclRand;
   UINT32 rand;
   BOOL   inclMobParms;
   PswCDMA2000Parm *mobP;
} PACKED_POSTFIX  PswCsfbHoEutraPrepMsgT;

typedef PACKED_PREFIX struct
{
   BOOL   inclTime;
   UINT8  systemTimeInfo[SYS_SYSTIME_SIZE];
   UINT16 len;
   UINT8  *data;
} PACKED_POSTFIX  PswCsfbMobFromEutraMsgT;

typedef enum
{
   RF_UNDEFINED,
   ONE_RX_ONE_TX,
   TWO_RX_ONE_TX,
   TWO_RX_TWO_TX
} UEAntennaCfgType;

typedef struct
{
   UEAntennaCfgType UEAntCfg;
} PswAntennaCfgMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  PswCsfbGetStatusMsgT;

typedef struct
{
   UINT8 csfbState;
   UINT8 antenna;
} PswCsfbGetStatusRspMsgT;

#ifdef CHINATELECOM_SMS_REG_CP
/*SMS auto reg info*/
typedef PACKED_PREFIX struct
{
   MobileIDType Id;
   uint64      value;
   IMSIType    Imsi;
} PACKED_POSTFIX  PswSetAutoRegInfoMsgT;
#endif

#ifdef MTK_DEV_C2K_IRAT
typedef struct
{
  uint8 Action;   /* 0-stop; 1-start; 2-update threshold only */
  int8 Threshold; /* -2x10xlogPS, PS is the pilot strength */
} PswIratRATSigMonReqT;
#endif
/* MTK_DEV_C2K_IRAT */

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)

typedef enum {
  RSVAS_ID_INVALID, /* Invalid C2K RSVAS Id */
  RSVAS_ID_1XRTT,   /* C2K RSVAS Id is 1xRTT */
  RSVAS_ID_EVDO,    /* C2K RSVAS Id is EVDO */
} C2kRsvasIdT;

/* RSVAS State Machine States */
typedef enum
{
  RSVAS_NORMAL_STATE,           /* RSVAS  normal mode */
  RSVAS_CONNECT_STATE,          /* RSVAS  connect mode  */
  RSVAS_VIRTUAL_STATE,          /* RSVAS  virtual suspend mode */
  RSVAS_SUSPEND_STATE,          /* RSVAS  suspend mode */

  RSVAS_NUM_STATES
} RsvasStateT;

typedef PACKED_PREFIX struct
{
  bool rsvasServiceOccupyResult;
} PACKED_POSTFIX  PswRsvasServiceOccupyResultT;

typedef PACKED_PREFIX struct
{
  C2kRsvasIdT id;
  rsvas_service_type_enum servType;
} PACKED_POSTFIX  PswRsvasServiceOccupyRequestT;

typedef PACKED_PREFIX struct
{
  C2kRsvasIdT id;
  rsvas_service_type_enum servType;
} PACKED_POSTFIX  PswRsvasServiceReleaseT;


typedef PACKED_PREFIX struct
{
  cs_reg_cause_enum  cause;
  bool               is_for_silent_redial;
} PACKED_POSTFIX  PswIrat1xRegReqMsgT;

#endif

#ifdef MTK_CBP_ENCRYPT_VOICE
typedef PACKED_PREFIX struct
{
   bool isMoc ;
} PACKED_POSTFIX  PswKeyReqMsgT;

typedef PACKED_PREFIX struct
{
   uint16 SeqNum;
} PACKED_POSTFIX  PswCancelCipherSmsSendReqT;

#endif



/******************PswCustomLockDefination***************************************/

#define PSW_FEATURE_TATA       0x80000000
#define PSW_FEATURE_RELIANCE   0x40000000
#define PSW_FEATURE_UNICOM     0x20000000
#define PSW_FEATURE_UIMSUPPORT 0x00000001
#define PSW_FEATURE_CUSTOM_PRILOCK   0x00000002


  typedef enum {
    PSW_CUSTOM_LOCK_FEATURE_OFF =0,
    PSW_CUSTOM_LOCK_FEATURE_IMSI =0x01,
    PSW_CUSTOM_LOCK_FEATURE_SIDNID =0x02,
    PSW_CUSTOM_LOCK_FEATURE_SYSTEM_VALIDATE = 0x04,
    PSW_CUSTOM_LOCK_FEATURE_SUBNET =0x08,
    PSW_CUSTOM_LOCK_FEATURE_EVDO_SYSTEM_VALIDATE = 0x10
  }PSW_CUSTOM_LOCK_FEATURE;


 typedef PACKED_PREFIX struct
{
   uint16 min_highest3;
   uint16 min_middle3;
   uint8   min_middle1;
   uint16 min_lowest3;
   /*each bit is defined for every cumtomfeafure */
} PACKED_POSTFIX  PswMinDigitalValue;

 typedef PACKED_PREFIX struct
 {
  UINT32 PswCustomLockSet;

  bool (*PswCustomImsiLock)(uint16 PhoneMcc,uint8 PhoneMnc,PswMinDigitalValue  PhoneMin);
  bool (*PswCustomSidNidLock)(uint16 PhoneSid,uint16 PhoneNid,bool PrlPrefonly);
  bool (*PswCustomSubnetLock)(uint8 *Subnet,uint8 mask);
  bool (*PswCustomSysValidate)(uint16 SysSid,uint16 SysNid)   ;
  bool (*PswCustomDOSysValidate)(uint8 *Subnet, uint8 mask)   ;

 } PACKED_POSTFIX PswCustomLockCallBackT;

  typedef enum
 {
     VAL_PSW_CKECK_Custom_Locking_Feature_OK = 0x00,
     VAL_PSW_CKECK_Custom_IMSI_ERROR,
     VAL_PSW_CKECK_Custom_DefaultMIN_OK,
     VAL_PSW_CKECK_Custom_SIDNID_ERROR,
     VAL_PSW_CKECK_Custom_Locking_Feature_NUM
 }PswCheckForCustomLockStateT;


typedef PACKED_PREFIX struct
{
    uint8 nCheckRst;
} PACKED_POSTFIX PswCheckLockingFeatureRptT;

void PswCustomLockProcedure(void);
BOOL cssCheckSidNidLock(void);
void PswCustomFeatureMsg(uint32 * MsgP);
BOOL PswIsCustomFeatureSet(uint32 CustomFeatureMacro);
void PswTransformPswMiscDataType(uint8* srcData ,uint8* desData);
void PswTransformNamDataType(uint8* srcData ,uint8* desData);
uint16 PswTransformPswMiscDataPackedType(uint8* srcData ,uint8* desData);
uint16 PswTransformNamDataPackedType(uint8* srcData ,uint8* desData);
uint16 PswGetNamNumBytes();
uint16 PswGetPswMiscBytes();



#ifdef MTK_CBP
void PswCustomFeatureReg(PswCustomLockCallBackT* CustomReg);
uint8 PswGetDefaultRoamSetting();
PswParmAccessResultCode PswRoamSettingBitMapHandler(uint8 RoamSettingBitMap);
#endif /* MTK_CBP */
bool PswCustomFeatureCheck(UINT32  FeatureItem);

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
uint32 PswIratMd1MsgHandler(uint32 MsgId, void* MsgBufferP, uint32 MsgSize);
#endif

/*********************PswCustomLockDefination************************************/

/*****************************************************************************
* $Log: pswapi.h $
* Revision 1.28  2006/03/16 11:54:18  mclee
* - Add ScanTime fields to PswOutOfServiceAreaParamsMsgT.
* Revision 1.27  2006/03/13 16:48:29  vsethuraman
* CR7330 :- PN310 E911 changes
* Revision 1.26  2006/03/07 16:45:09  winston
* CR7319: OTA Verizon features implementation
* Revision 1.25  2006/03/03 13:30:23  vsethuraman
* CR7207 :- PN310 CSS and ERI changes.
* Revision 1.24  2006/01/12 11:05:08  wavis
* SMS changes merged from 4.05.
* Revision 1.23  2006/01/03 10:53:06  wavis
* Merging in VAL.
* Revision 1.22  2005/12/21 11:33:33  hans
* Updated qpch algorithm and new L1 interface.
* Revision 1.21  2005/10/04 10:53:27  ckackman
* Fix revision number for promotion.
* Revision 1.20  2005/09/23 15:38:48  ckackman
* Removed pde include file.
* Revision 1.19  2005/08/18 17:02:30  chinh
* - Re-structure the TestModeMsgT to match with the DLL messages numbers.
* Revision 1.18  2005/08/17 14:13:57  hans
* CR6900 - Enhanced qpch indicator detection algorithm [DSPM/L1D/PSW]. CR6902 - Immediate PSW-L1D wake message for PCH/FCCCH
* Revision 1.17  2005/08/02 16:10:08  wfu
* - Add PSW_TEST_POWER_CONTROL_MSG to PswMsgIdT.
*    Add PswTestPowerControlMsgT structure.
* Revision 1.16  2005/06/06 15:13:01  hans
* Revision 1.15  2005/05/26 12:46:21  hans
* See revision 1.15.
* Revision 1.14  2005/05/26 11:04:24  hans
* TM_DISABLE_SUPERVISION added to testmode struct.
* Revision 1.13  2005/05/09 17:20:20  wfu
* Fix CR 6202:  add new parameters in PswSetSilentRetryMsgT;
* Revision 1.12  2005/05/06 18:14:10  hans
* FCCCH_SLOT define added to FCCCH slot code.
* Revision 1.11  2005/05/06 15:46:55  ckackman
* Added Fccch request message
* Revision 1.10  2005/04/27 14:08:19  blee
* Added test fields to TestModeMsgT
* Revision 1.9  2005/04/22 12:13:11  ssriniva
* Gps merge. Refer release notes.
* Revision 1.8  2005/04/06 15:46:03  dstecker
* Added new continuous BCCH test mode
* Revision 1.7  2005/03/18 10:04:18  dstecker
* Modifications for the 4.05 merge
* Revision 1.6  2004/09/28 15:54:17  chinh
* Merged changes from cbp6.0.
* Revision 1.4  2004/04/23 10:48:36  asharma
* CBP6.1 (Phase-1) DV Data and Voice Call Setup changes.
* Revision 1.3.2.2  2004/04/21 11:41:09  asharma
* Revision 1.3.2.1  2004/04/06 14:00:29  asharma
* Duplicate revision
* Revision 1.3  2004/04/06 14:00:29  dstecker
* CBP6.1 release 1
* Revision 1.2  2004/03/25 11:46:03  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.3  2004/02/10 15:17:02  bcolford
* Merged CBP4.0 Release 8.05.0 changes.
* Revision 1.99  2003/12/11 15:13:10  hhong
* Added reason for PswPsCallRelMsgT.
* Revision 1.98  2003/12/04 17:33:11  blee
* The number of pilots for remain set search has changed from 1 to 20
* Revision 1.97  2003/11/06 11:05:55  ameya
* Entered structures for PswGetSilentRetryStatusMsgT and PswGetSilentRetryStatusResponseMsgT. Also added PSW_GET_SILENT_RETRY_STATUS_MSG and PSW_FORCE_ANALOG_MODE_MSG.
* Revision 1.96  2003/08/22 11:34:05  mclee
* Uim Feature Introduction in SW Base
* Revision 1.95  2003/08/12 17:15:32  fpeng
* Merged yzhang's fix in 1.94.0.1 - fixed [ with { in typedef PSW_CH_IND_TYPE.  This is added by bsharma on 3/18/03 in Rev. 1.87 for CR 2000.
* Revision 1.94  2003/06/27 12:14:44  mclee
* Fix CR 2386 by removing RmsErrPhase
* Revision 1.93  2003/06/26 13:48:14  byang
* CR#2394: Added MinNghbrThr field to data structure
* PswSetNewIHOparametersMsgT.
* Revision 1.92  2003/06/17 14:07:00  dstecker
* Removed PSW_SMS_READ_ACK_PARMS_MSG &    PSW_SMS_READ_ACK_DATA_MSG because being sent with    PSW_SMS_READ_ACK_PARMS_MSG & PSW_SMS_ACK_DATA_MSG
* Revision 1.91  2003/05/08 18:28:31  hans
* SCRM Duration and Duration Units added to Ms Cap DB
* Revision 1.90  2003/04/30 09:25:26  mclee
* Remove SYS_OPTION_SCC compile condition since always defined.
* Revision 1.89  2003/04/25 14:05:38  byang
* . For CR2171:  Add two command status:
*    PSW_FSCH_STARTED and PSW_FSCH_TERMINATED.
* Revision 1.88  2003/04/04 15:12:41  dstecker
* Rearranged member sequence in PswSmsSubmitParmsMsgT for clarity
* Revision 1.87  2003/03/18 17:14:47  bsharma
* Fied CR2000: Added CH_IND and DRS fields to PswMsCapDbWriteMsgT and declared PSW_CH_IND_TYPE
* Revision 1.86  2003/03/07 11:45:02  byang
* CR#1810: Add PSW_SET_ACCESS_HO_PARAMETERS_MSG
* ID and message definition.
* Revision 1.85  2003/02/28 16:42:57  dstecker
* Upgrade SMS to make it 637B compliant
* Revision 1.84  2002/11/26 19:50:58  hans
* Structure for psw data buffer status msg added
* Revision 1.83  2002/10/31 18:22:14  amala
* Changed RMS Error Phase parameter type to UINT8.
* Revision 1.82  2002/10/29 10:56:29  mclee
* Add Ignore Akey Prev to Test Mode enum
* Revision 1.81  2002/10/25 16:03:26  amala
* In struct PswPosAfltMeasurementMsgT, changed TimeRefMs type
* from uint32 to uint16 for AFLT MS Time Reference calculation
* correction.
* Revision 1.80  2002/10/23 16:41:28  ameya
* Added PSW_ENTER_AKEY_MSG and PswEnterAKeyMsgT structure.
* Revision 1.79  2002/10/22 09:57:47  mclee
* Add note to remove rlpCapBlopLen from MsCapDb
* Revision 1.78  2002/10/21 09:39:24  mshaver
* Changes to support OTTS incorportated into the baseline.
* These changes enabled only by #ifdef OTTS.
* Normal CP operation is unaffected.
* Revision 1.77  2002/09/30 14:55:36  mshaver
* Replaced references to the unpacked NAM data structure
* type IS95Nam with the packed type PswIs95NamT as the
* unpacked type has been removed in an effort to define the NAM
* in a single data structure.
* Revision 1.76  2002/09/20 11:01:45  bsharma
* Added PSW_SMS_RETX_AMOUNT_MSG to PswMsgIdT and typedef PswSmsRetxAmountMsgT.
* Revision 1.75  2002/09/17 18:18:06  akayvan
* PswWakeStatusT was updated for Spirent's QPCH Spy.
* Revision 1.74  2002/09/03 18:26:24  bcolford
* Add a test mode to control AMPS Slotted Mode operation.
* Revision 1.73  2002/08/27 16:59:16  byang
* Add new command status for PSW_RSR_UPDATED, and new
* frame categories 15 and 27 from LMD.  Add ping timer period
* for MS capability database interface.
* Revision 1.72  2002/08/25 19:55:41  amala
* Fixed AFLT CRs 1107, 1109
* Revision 1.71  2002/08/23 10:14:45  akayvan
* Per system's request added ETS capabilty to modify all QPCH thresholds.
* Revision 1.70  2002/08/19 10:57:31  dstecker
* Added new candidate frequency message definitions
* Revision 1.69  2002/08/13 13:02:42  dstecker
* Hanrd handoff injection from ETS
* Revision 1.68  2002/08/08 15:13:40  mclee
* Add & clean up Msgs used for Location Servies. Added
* new LocServ Stats to Test Mode Message
* Revision 1.67  2002/08/02 17:55:27  bsharma
* Added SmsTlMaxRetry to NAM structure
* Revision 1.66  2002/07/31 18:49:40  hans
* PSW_NAM_NUM_BANDS_SUPPORTED increased from 4 to 5, Band Class 4 (cr726) and support for other band classes added to PswBandClassT tupe definition.
* Revision 1.65  2002/07/30 11:48:32  mmalopy
* - PswMsgIdT modified to include message identifiers for the MISC
*   PSW data segment, Get Active NAM, Select Active NAM, and
*   the MISC PSW Data Write Ack message identifiers.
* - PswMiscT created for the DBM PSW MISC parameters.
* - PswSelectActiveNamMsgT and PswGetActiveNamMsgT structs
*   created to support dual NAM message requests from ETS/MMI.
* - PswSelectActiveNamReponseMsgT and the
*   PswGetActiveNamResponseMsgT structs created to support
*   dual NAM message responses to ETS/MMI.
* Revision 1.64  2002/07/26 14:01:43  ameya
* Added 20 positive sids/nids and 10 negative sids/nids and a stored negative sid/nid parameter to the NAM structure.
* Revision 1.63  2002/07/25 10:04:58  etarolli
* Added Rev Tunneling and SPI fields to NAM MIP data.
* Revision 1.62  2002/07/23 17:29:18  mshaver
* Include syscust.h to provide definitions now required by sysparm.h
* Revision 1.61  2002/07/17 09:14:27  mclee
* Added Auth Mode Disable to Test Modes Msg Enum
* Revision 1.60  2002/07/16 10:38:34  dstecker
* Added  new message id PSW_TA_TIMER_EXPIRED_MSG
* Revision 1.59  2002/07/15 11:26:51  chinh
* Changed SYS_CP_MAX_CANDIDATE_LIST_PILOTS to SYS_MAX_CANDIDATE_LIST_PILOTS (CR257)
* Revision 1.58  2002/06/18 18:30:18  akayvan
* QPCH Phase2 mods.  Updated PswEngineTestModeMsgT, and
* TestModeMsgT types for the new QPCH ETS CP test mode commands.
* Revision 1.57  2002/06/18 09:20:52  bcolford
* Added a new message ID for the StatusRequest related RSSI
* response from L1A.
* Revision 1.56  2002/06/14 17:05:47  chinh
* Added new message id and message structure for L1d Access Complete.
* Revision 1.55  2002/06/04 18:54:28  hans
* Not used TM_RETRIEVE_STATISTICS removed from TestModeMsgT enum.
* Revision 1.54  2002/06/04 08:07:21  mshaver
* Added VIA Technologies copyright notice.
* Revision 1.53  2002/05/31 20:11:29  akayvan
* QPCH/PI mods.
* Revision 1.52  2002/05/29 15:05:58  ameya
* Added msg id PSW_CSS_VAL_SYNC_RSP_MSG and structure defn for PswCssValSyncRspMsgT.
* Revision 1.51  2002/04/18 09:47:00  mclee
* Add structs for new IS801 PS messages
* Revision 1.50  2002/04/03 15:15:36  byang
* Add OP_MODE for customer usage to data structure
* PswMsCapDbWriteMsgT.
* Revision 1.49  2002/04/02 17:12:50  mclee
* Add Clear MRU command and changed Status Response to CPstate.
* Revision 1.48  2002/03/28 18:42:35  hans
* is2000 rev 0 add 2, retrievable and settable parameters implemented.
*
*
* Revision 1.47  2002/03/13 16:59:49  byang
* Removed PSW_MS_CAP_DB_READ_MSG and modified
* structure PswMsCapDbWriteMsgT to conform to
* MsCapDb defined in Ms_Cap_Db.h.
* Revision 1.46  2002/03/03 21:57:43  bsharma
* Prototypes for IS-801-1 Position Determination module.
* Revision 1.45  2002/03/01 16:25:57  AMALA
* Added Location Services AFLT functionality.
* Revision 1.44  2002/02/27 20:48:22  mclee
* Created three new messages, PSW_IS801_TCPB_REV_DATA_RESP_INFO_MSG, PSW_IS801_TCPB_FWD_DATA_RESP_INFO_MSG, and PSW_IS801_SESSION_START_MSG in support of the new Is801 location services feature. Also modified the PswIs95NamT type to add the new Is801 related fields to the Nam.
* Revision 1.43  2002/02/27 10:49:54  etarolli
* Added PswNamGet() function prototype. Also fixed spelling of MN_NAI NAM field.
* Revision 1.42  2002/02/21 15:44:51  ameya
* Added psw task id and message structure for Protocol Revision in use ETS message.
* Revision 1.41  2002/02/13 16:28:30  ameya
* Added request and response structures for get phone state and get full system time messages and also task id for get phone state message.
* Revision 1.40  2002/02/06 12:59:35  mshaver
* Added PSW_NAM_UNLOCK_ACK_MSG to the PswMsgIdT enum
* list to handle the ack from DBM for NAM lock/unlock messages.
* Revision 1.39  2002/02/05 11:08:58  mclee
* Add AKEY_UPDATED to Nam Data MaskBits.  Add AkeyValid, VP_Enable, and Future Expand Fields to PSW Nam struct. Remove SSDA & SSDB from PswNamChangedMsgT. Modify MS_Cap_DB for encryption supported.
* Revision 1.38  2002/01/28 11:40:25  byang
* Add PSW_RSCH_REQUEST_MSG to PswMsgIdT, PswHspdActiveControlHoldMsgT, and PswRevSchRequestMsgT.
* Revision 1.37  2002/01/17 17:33:58  akayvan
* Implemented Slotted Mode mods.
* Revision 1.36  2002/01/10 17:06:19  AMALA
* Added emergency mode processing for new system selection.
* Revision 1.35  2002/01/02 10:05:17  AMALA
* Added new system selection changes.
* Revision 1.34  2001/12/19 15:08:17  bcolford
* Make FrameIndex a 32 bit value so message cracking will work
* better.
* Revision 1.33  2001/12/14 16:59:14  mclee
* Add MsgId PSW_GET_PS_INFO_MSG and struct PswStatusResponseMsgT
* Revision 1.32  2001/12/07 14:40:15  bcolford
* Change RSSI to unsigned 16 bit value.
* Revision 1.31  2001/11/29 13:50:42  kmar
* Added boolean flag, SendOnTraffic, to PswSmsSubmitParmsMsgT so MMI can tell PSW to send SMS on Traffic.
* Revision 1.30  2001/11/20 12:07:24  mclee
*  Add Nam Data Mask Bits
*         Add new fields to PSWIS95Nam structure
*         Add following messages
*             PSW_GET_NAM_MSG
*             PSW_NAM_CHANGED_BY_ETS_MSG
*             PSW_NAM_CHANGED_MSG
*             PSW_EXP_MOD_BS_RESULT_MSG
*             PSW_EXP_MOD_MS_RESULT_MSG
*             PSW_FILL_SSPR_RSP_MSG
*             PSW_IOTA_IS683_MSG
*             PSW_IOTA_PRL_MSG
* Revision 1.29  2001/11/17 17:19:42  snreddy
* Added psw power report msg (for Ameya)
* Revision 1.28  2001/11/13 18:04:50  byang
* Updated for F-SCH Integration.
* Revision 1.27  2001/11/08 11:51:42  AMALA
* Modified typedef PACKED struct PswDbmDataT.
* Revision 1.26  2001/10/17 17:01:24  bcolford
* Add Amps FOCC Duty Cycle power saving
* Revision 1.25  2001/10/08 14:43:13  mclee
* extern voice privacy, define PSW_GET_PRL_INFO_MSG
* Revision 1.24  2001/09/24 15:05:09  kmar
* Update SMS to latest in CBP3.
* Added PSW_SMS_CAUSE_CODE_STATUS_MSG into
* PswMsgIdT.
* Revision 1.23  2001/09/24 09:25:03  mclee
* PSW Scc 3 to 4 merge
* Revision 1.22  2001/09/17 14:09:38  AMALA
* CBP3 System Selection port
* Revision 1.21  2001/09/07 18:21:44  akayvan
* Updated data type PswMsgIdT, and added new data structures per FPC.
* Revision 1.20  2001/09/04 16:58:22  byang
* Updated PswPmrMsgT for FSCH.
* Revision 1.19  2001/08/29 14:53:09  vxnguyen
* - Added PSW_SERVICE_OPTION_CONTROL_RESP_MSG and
*   PswServiceOptionControlRespMsgT structure definition.
* Revision 1.18  2001/07/23 14:22:22  byang
* Added message IDs for PSW_MS_CAP_DB_WRITE_MSG and
* PSW_MS_CAP_DB_READ_MSG.  Defined message structures
* PswMsCapDbReadMsgTand PswMsCapDbReadRspMsgT.
* Revision 1.17  2001/07/18 15:20:02  chinh
* cbp3 porting for Idle and Access
* Revision 1.16  2001/07/02 14:19:41  kmar
* Update SCC and SMS to CBP3 v360.
* Added PSW_CALL_INTIATE_WITH_INFO_REC_MSG
* to PSW message enum.
* Revision 1.15  2001/06/12 17:08:20  byang
* Changed the maximum neighbor list from 20 to 40.
* Promoted PSW constants to SYS constants.
* Revision 1.14  2001/06/12 17:00:23  chinh
* Added variables and structures to support the Subaddress information records.
* Revision 1.13  2001/05/25 10:18:32  bcolford
* Added AMPS functionality.
* Revision 1.12  2001/05/16 10:44:50  yfang
* added data service back
* Revision 1.11  2001/04/27 16:52:20  byang
* Added functionality to set the MS capability database from ETS
* and to stored the database into DBM when the MS is powerd-
* down.  On power-up, the MS Cap. Db. is read from DBM.
* Revision 1.10  2001/04/26 12:09:09  yfang
* added data services
* Revision 1.9  2001/01/17 14:59:32  plabarbe
* Renamed SysRspMsgT to ExeRspMsgT.  Definition is now in Exeapi.h instead of Sysapi.h
* Revision 1.8  2001/01/09 15:37:18  plabarbe
* CDMA Band and Mux Options are now SYS typedefs.  Changed names accordingly and there is no longer a need to include lmdapi.h or l1dapi.h.
* Revision 1.7  2001/01/03 11:24:19  plabarbe
* Updated to be consistent with initial PS release.
* Revision 1.6  2000/12/28 14:10:46  wfu
* Make no change, just check out.
* Revision 1.5  2000/12/19 11:59:23  plabarbe
* Removed tabs.
* Revision 1.4  2000/12/19 10:07:36  plabarbe
* First cut at resolving PS & L1 discrepancies.  Made this a complete  API for all existing IS-95 interfaces plus Phase 1 for CBP4.  Also, now includes two other global files for PSW.
* Revision 1.2  2000/11/13 14:49:01  hans
* Check-in for cbp4 phase 1 integration
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif /* #ifndef _PSWAPI_H_ */
/**Log information: \main\CBP80\cbp80_gdeng_scbp10099\1 2012-07-27 08:59:51 GMT gdeng
** SCBP#10099 **/
/**Log information: \main\CBP80\cbp80_gdeng_scbp10099\2 2012-07-30 08:20:37 GMT gdeng
** SCBP#10099**/
/**Log information: \main\CBP80\cbp80_gdeng_scbp10325\1 2012-09-20 06:13:14 GMT gdeng
** SCBP#10325**/
/**Log information: \main\CBP80\cbp80_gdeng_scbp10325\2 2012-09-20 06:23:16 GMT gdeng
** SCBP#10325**/
/**Log information: \main\SMART\1 2013-04-25 09:46:39 GMT yxma
** HREF#22182, add custom lock feature to smartfren
|**/
/**Log information: \main\Trophy\Trophy_zjiang_href22256\1 2013-08-21 07:39:32 GMT zjiang
** HREF#22256.1.crts21316:模块短信自注册编译版本里存在的自注册短信多余上报修改;2.+CPIN命令优化.**/
/**Log information: \main\Trophy\2 2013-08-21 07:42:24 GMT cshen
** href#22256**/
/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:18:03 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\3 2013-12-10 08:33:39 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
