/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2005-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************

  FILE NAME:  valapi.h

  DESCRIPTION:

   This file contains all constants and typedefs needed to interface
   with the VAL unit via the Exe mail service routines.


*****************************************************************************/
#ifndef VALAPI_H
#define VALAPI_H

#include "ipcapi.h"
#include "sysdefs.h"
#include "pswnam.h"
#include "hlpapi.h"
#include "valatdata.h"
#include "cpbuf.h"
#include "pswapi.h"
#include "pswvalapi.h"


#ifdef MTK_PLT_ON_PC
#include "..\val\atc\valatcdefs.h"
#include "valattune.h"
#include "sbpapi.h"
#endif /* MTK_PLT_ON_PC */




#ifndef VAL_MAX_SIZE_DATA
#define VAL_MAX_SIZE_DATA        64
#define VAL_MAX_MON_SPY_LEN      64

#define VAL_MAX_CFG_STR_SIZE     249 /* MAX_CFG_LEN in Ai_data.h */
#define VAL_MAX_BRSWR_DGT_SIZE   (32 +1)
                                 /* NULL-terminated. */
#define VAL_MAX_FDL_STR_SIZE     95  /* Sect 4.4.3 IS-707-A.3 */
#define VAL_MAX_GCAP_STR_SIZE    64  /* Ai_cmd is using 65535 bytes!! */
#define VAL_MAX_FLI_STR_SIZE     21  /* MAX_FLI_LEN in Ai_data.h */
#define VAL_MAX_FPA_STR_SIZE     21  /* MAX_FPA_LEN in Ai_data.h */
#define VAL_MAX_FPI_STR_SIZE     21  /* MAX_FPI_LEN in Ai_data.h */
#define VAL_MAX_FPW_STR_SIZE     21  /* MAX_FPW_LEN in Ai_data.h */
#define VAL_MAX_FSA_STR_SIZE     21  /* MAX_FSA_LEN in Ai_data.h */
#define VAL_MAX_GMI_STR_SIZE     64  /* Sect 4.1.1 IS-131 says 2048 */
#define VAL_MAX_GMM_STR_SIZE     64  /* Sect 4.1.2 IS-131 says 2048 */
#define VAL_MAX_GMR_STR_SIZE     64  /* Sect 4.1.3 IS-131 says 2048 */
#define VAL_MAX_GOI_STR_SIZE     64  /* Sect 4.1.4 IS-131 says 2048 */
#define VAL_MAX_GSN_STR_SIZE     64  /* Sect 4.1.5 IS-131 says 2048 */
#ifdef SYS_OPTION_ATCMD_CH_3
#define VAL_MAX_NUMBER_OF_TIMERS 40  /* 27 timers for all AT channels (4*6+3)*/
#else
#define VAL_MAX_NUMBER_OF_TIMERS 20
#endif
#define VAL_MAX_USRID_LEN        HLP_MAX_USRID_LEN
#define VAL_MAX_PSWD_LEN         HLP_MAX_PSWD_LEN
#define VAL_MAX_FIF_LEN          90
#define VAL_MAX_NUM_LINE_IN_RSP  ATC_MAX_NUM_LINE_IN_RSP
#define VAL_MAX_ATPARMS          8
#define VAL_MAX_COMMAND_BODY_LEN 16
#ifdef SYS_OPTION_ENHANCEDAT
#define VAL_ENHANCED_AT_CMD_LEN  ATC_ENHANCED_AT_CMD_LEN
#endif
#endif

#ifdef MTK_CBP
#define CT_MCC 460
#define MCC_WILDCARD_AI 0x3FF /* 10bit all '1's */
#define MNC_WILDCARD_AI 0x7F /* 7bit all '1's */
#define GPS_MAX_SECURITY_DATA          16
extern uint8 FakePowerOnInProgess;


#define VAL_WILD_CHAR '?'

#endif
/*------------------------------------------------------------------------
 * Define constants used in VAL API
 *------------------------------------------------------------------------*/

/* undefined troublesome consts */
#undef TRUE
#undef FALSE

typedef enum
{
  VAL_MEM_POOL_SYS,
  VAL_MEM_POOL_NOHALT
}ValMemPoolTypeT;

#define HLP_VZW_PCO_MCCMNC_TO_SEND_LEN 3

typedef  struct
{
    bool       MtuReq;
    uint16     PcoCode;
    uint8      PcoMccMnc[HLP_VZW_PCO_MCCMNC_TO_SEND_LEN];
} ValPsdmPdpSpecPco;

#define NUM_UI_LOCK_CODE_DIGITS 7
//#undef FEATURE_UTK

/* VAL signals */
#define VAL_STARTUP_1_SIGNAL            EXE_SIGNAL_1   /* from HWD MS, after dspv alive is rcvd */
#define VAL_STARTUP_2_SIGNAL            EXE_SIGNAL_2   /* from L1D after 32kHz Osc is validated */
#define VAL_STARTUP_3_SIGNAL            EXE_SIGNAL_3   /* from IOP task */
#define VAL_VREC_TIMEOUT_SIGNAL         EXE_SIGNAL_4   /* timeout in training or recogn */
#define VAL_REV_TX_REQ_PPP_SIGNAL       EXE_SIGNAL_5   /* signal for rev PPP data on interface 0 */
#define VAL_FWD_TX_RSP_PPP_SIGNAL       EXE_SIGNAL_6   /* signal for fwd PPP data on interface 0 */

#define VAL_REV_TX_REQ_AT_1_SIGNAL       EXE_SIGNAL_7   /* signal for rev ATC data on logical AT channel 1 */
#define VAL_FWD_TX_RSP_AT_1_SIGNAL       EXE_SIGNAL_8   /* signal for fwd ATC data on logical AT channel 1 */
#define VAL_REV_TX_REQ_AT_2_SIGNAL       EXE_SIGNAL_9   /* signal for rev ATC data on logical AT channel 2 */
#define VAL_FWD_TX_RSP_AT_2_SIGNAL       EXE_SIGNAL_10  /* signal for fwd ATC data on logical AT channel 2 */

#ifdef SYS_OPTION_ATCMD_CH_3
#define VAL_REV_TX_REQ_AT_3_SIGNAL       EXE_SIGNAL_11   /* signal for rev ATC data on logical AT channel 3 */
#define VAL_FWD_TX_RSP_AT_3_SIGNAL       EXE_SIGNAL_12   /* signal for fwd ATC data on logical AT channel 3 */
#endif

#ifdef SYS_OPTION_ATCMD_CH_4
#define VAL_REV_TX_REQ_AT_4_SIGNAL       EXE_SIGNAL_13   /* signal for rev ATC data on logical AT channel 4 */
#define VAL_FWD_TX_RSP_AT_4_SIGNAL       EXE_SIGNAL_14   /* signal for fwd ATC data on logical AT channel 4 */
#endif


#ifdef SYS_OPTION_MORE_AT_CHANNEL
#define VAL_REV_TX_REQ_AT_5_SIGNAL       EXE_SIGNAL_15   /* signal for rev ATC data on logical AT channel 5 */
#define VAL_FWD_TX_RSP_AT_5_SIGNAL       EXE_SIGNAL_16   /* signal for fwd ATC data on logical AT channel 5 */
#define VAL_REV_TX_REQ_AT_6_SIGNAL       EXE_SIGNAL_17   /* signal for rev ATC data on logical AT channel 6*/
#define VAL_FWD_TX_RSP_AT_6_SIGNAL       EXE_SIGNAL_18   /* signal for fwd ATC data on logical AT channel 6 */
#define VAL_REV_TX_REQ_AT_7_SIGNAL       EXE_SIGNAL_19   /* signal for rev ATC data on logical AT channel 7 */
#define VAL_FWD_TX_RSP_AT_7_SIGNAL       EXE_SIGNAL_20   /* signal for fwd ATC data on logical AT channel 7 */
#define VAL_REV_TX_REQ_AT_8_SIGNAL       EXE_SIGNAL_3    /* signal for rev ATC data on logical AT channel 8 */
#define VAL_FWD_TX_RSP_AT_8_SIGNAL       EXE_SIGNAL_4    /* signal for fwd ATC data on logical AT channel 8 */

#define VAL_ALL_ATC_SIGNALS             (VAL_REV_TX_REQ_AT_1_SIGNAL | VAL_FWD_TX_RSP_AT_1_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_2_SIGNAL | VAL_FWD_TX_RSP_AT_2_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_3_SIGNAL | VAL_FWD_TX_RSP_AT_3_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_4_SIGNAL | VAL_FWD_TX_RSP_AT_4_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_5_SIGNAL | VAL_FWD_TX_RSP_AT_5_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_6_SIGNAL | VAL_FWD_TX_RSP_AT_6_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_7_SIGNAL | VAL_FWD_TX_RSP_AT_7_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_8_SIGNAL | VAL_FWD_TX_RSP_AT_8_SIGNAL)
#elif defined(SYS_OPTION_ATCMD_CH_3)
#ifdef SYS_OPTION_ATCMD_CH_4
#define VAL_ALL_ATC_SIGNALS             (VAL_REV_TX_REQ_AT_1_SIGNAL | VAL_FWD_TX_RSP_AT_1_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_2_SIGNAL | VAL_FWD_TX_RSP_AT_2_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_3_SIGNAL | VAL_FWD_TX_RSP_AT_3_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_4_SIGNAL | VAL_FWD_TX_RSP_AT_4_SIGNAL)
#else
#define VAL_ALL_ATC_SIGNALS             (VAL_REV_TX_REQ_AT_1_SIGNAL | VAL_FWD_TX_RSP_AT_1_SIGNAL |\
									     VAL_REV_TX_REQ_AT_2_SIGNAL | VAL_FWD_TX_RSP_AT_2_SIGNAL |\
										 VAL_REV_TX_REQ_AT_3_SIGNAL | VAL_FWD_TX_RSP_AT_3_SIGNAL)
#endif
#else
#define VAL_ALL_ATC_SIGNALS             (VAL_REV_TX_REQ_AT_1_SIGNAL | VAL_FWD_TX_RSP_AT_1_SIGNAL |\
                                         VAL_REV_TX_REQ_AT_2_SIGNAL | VAL_FWD_TX_RSP_AT_2_SIGNAL)
#endif /* SYS_OPTION_MORE_AT_CHANNEL*/

#ifdef MTK_DEV_ETS_ENHANCEMENT
#define VAL_FWD_TX_RSP_AT_ETS_SIGNAL    EXE_SIGNAL_22
#endif

#if defined(MTK_PLT_AUDIO)
#define VAL_SPH_REV_AP_CMD_SIGNAL           EXE_SIGNAL_23
#endif // defined(MTK_PLT_AUDIO)

#ifdef MTK_CBP
#define VAL_CSS_READY_SIGNAL           EXE_SIGNAL_24
#endif
#ifdef TCPIP_ATC


#define VAL_REV_TX_REQ_IP_SIGNAL       EXE_SIGNAL_21   /* signal for rev ip data on logical AT channel 0 */

#endif
/* other signals are defined in valdefs.h, and start from _11 up */

/* VAL command mailbox id */
#define VAL_MAILBOX        EXE_MAILBOX_1_ID
#define VAL_MAILBOX_WAIT_ID             EXE_MAILBOX_1

#define VAL_HAL_MAILBOX    EXE_MAILBOX_2_ID
#define VAL_HAL_MAILBOX_WAIT_ID      EXE_MAILBOX_2

#define VAL_STORAGE_MAILBOX          EXE_MAILBOX_3_ID
#define VAL_STORAGE_MAILBOX_WAIT_ID  EXE_MAILBOX_3

#define VAL_ATC_MAILBOX    EXE_MAILBOX_4_ID
#define VAL_ATC_MAILBOX_WAIT_ID  EXE_MAILBOX_4

#define VAL_CUST_MAILBOX          EXE_MAILBOX_5_ID
#define VAL_CUST_MAILBOX_WAIT_ID  EXE_MAILBOX_5


/* macros */
#define VAL_MAKE_RECID(Index,DevType)           ((Index) | (DevType))
#define VAL_GET_INDEX_FROM_RECID(PhyAddress)    ((PhyAddress) & (0x0FFF))
#define VAL_GET_DEVTYPE_FROM_RECID(PhyAddress)  ((PhyAddress) & (0xF000))

#define VAL_MISC_MAX_REG_TASK      (5)

/* timer for L1d/RMC RSSI reports */
#define VAL_RSSI_REPORT_TIMER (HWD_CLK_FREQ_32KHZ/10)   /* 100 ms */

/*-----------------------------------------------------------------
 *  uistate.cpp interface
 *----------------------------------------------------------------*/
#define POWER_DOWN_WAIT             1000    /* msec */


/*------------------------------------------------------------------------
 * VAL message data structures (grouped like and in the same order of messages)
 *------------------------------------------------------------------------*/
/*------------------------------------------------------------------------
 * VAL message ids, grouped by originator
 *
 * When adding/removing/modifying VAL messages:
 * - add (or remove) associated data structure; put new structures in the
 *   same section as the message id, and in the same relative order within
 *    its section
 *  - update the VAL section in msg_id.txt for ETS if the message should be
 *    usable from ETS (VAL-subsections on ETS are the same as the ones in
 *    VALapi.h). Try to keep the msg name and ETS command as similar as
 *    possible.
 *  - always update the enum VAL MsgIds in val\val_msg.txt on ETS; the enum is used
 *    to display incoming VAL messages, and should always be kept in synch
 *    with ValMsgIdT.
 *  - update val_msg.txt with the data structure associated with the new
 *    message (optiona; do it only if ETS is supposed to send this message)
 *
 *------------------------------------------------------------------------*/


/* note:  please use the value from 5000, message id from 0 to 4999 is used by pswapi.h, please donnot use these values  */
typedef enum
{
   /*-----------------------------------------------------------------
   * AIWHandler
   *----------------------------------------------------------------*/
   VAL_FLUSH_FINAL_MSG = 0,
   VAL_NWK_RPT_DATA_SVC_STATUS_WORD_MSG,

   /*-----------------------------------------------------------------
   * HLWHandler
   *----------------------------------------------------------------*/
   VAL_TCPB_DATA_RECV_MSG = 100,
   VAL_TCPB_DATA_SEND_RSP_MSG,

   VAL_UPB_RECV_DATA_MSG,
   VAL_UPB_SENT_DATA_RSP_MSG,

   VAL_HLP_PPP_CONN_STATUS_MSG,
   VAL_HLP_PPP_CLOSE_STATUS_MSG,
   VAL_HLP_SOCKET_CREATE_STATUS_MSG,
   VAL_HLP_SOCKET_BIND_STATUS_MSG,
   VAL_HLP_SOCKET_CONN_STATUS_MSG,
   VAL_HLP_SOCKET_CLOSE_STATUS_MSG,
   VAL_HLP_SOCKET_INACT_TMO_MSG,

   VAL_TCPB_SEND_DATA_MSG,
   VAL_UDPB_SEND_DATA_MSG,
   VAL_UPB_CONN_STATUS_MSG,
   VAL_TCPB_CONN_STATUS_MSG,
   VAL_UPB_IDLE_TMO_MSG,
   VAL_HLP_SOCKET_LISTEN_STATUS_MSG,
   VAL_HLP_SOCKET_OPT_STATUS_MSG,
   VAL_HLP_SOCKET_SHUT_STATUS_MSG,
   VAL_HLP_SOCKET_LINGER_STATUS_MSG,
   VAL_DNS_NET_MSG,
   VAL_DNS_QUERY_MSG,
   VAL_HLP_DATA_STATE_MSG,

   VAL_HLP_SESS_ACTIVE_MSG,

   VAL_HLP_IPV6_SOCKET_CREATE_STATUS_MSG,
   VAL_HLP_IPV6_SOCKET_BIND_STATUS_MSG,
   VAL_HLP_IPV6_SOCKET_CONN_STATUS_MSG,
#ifdef MTK_PLT_ON_PC
   VAL_IP_DATA_SEND_CNF_MSG,
   VAL_IP_DATA_RECV_IND_MSG,
#endif /* MTK_PLT_ON_PC */
#ifdef MTK_CBP
   VAL_NET_PING_EXPIRY_MSG,
   VAL_HLP_PCMT_PARA_RSP_MSG,
#endif /*MTK_CBP*/
#ifdef MTK_DEV_BUG_FIX_HLP
   VAL_HLP_MTU_URC_MSG,
#endif /* MTK_DEV_BUG_FIX_HLP */

   VAL_HLP_IP_DNS_ADDR_GET_MSG=195,
   VAL_RPT_DATA_PREARRANGE_MSG,

   /*-----------------------------------------------------------------
   * OTASPHandler
   *----------------------------------------------------------------*/
   VAL_IOTA_683_RSP_MSG = 200,
   VAL_IOTA_PRL_RSP_MSG,
   VAL_OTASP_CONN_IND_MSG,
   VAL_OTASP_DISCONN_IND_MSG,
   VAL_OTASP_IND_MSG,      /* ??? nobody is sending this message */
   VAL_OTASP_PREMATURE_TERM_IND_MSG,
   VAL_OTASP_RX_IND_MSG,
   VAL_OTASP_START_IND_MSG,
   VAL_OTASP_STOP_IND_MSG,
   VAL_OTASP_SYSTEM_ERROR_IND_MSG,
   VAL_OTASP_TX_RSP_MSG,

   /*-----------------------------------------------------------------
   * 1xCSFB Handler
   *----------------------------------------------------------------*/
   VAL_CSFB_GCSNA_UL_MSG = 250,
   VAL_CSFB_REGISTER_FAIL_MSG,
   VAL_CSFB_MEASURE_RSP_MSG,
   VAL_CSFB_HO_PREP_XFER_MSG,
   VAL_CSFB_EVENT_MSG,
   VAL_CSFB_TST_SET_BUFFER_MSG,
   VAL_CSFB_TST_PARM_RSP_MSG,
   VAL_CSFB_TST_SIB8_MSG,
   VAL_CSFB_TST_GCSNA_DL_MSG,
   VAL_CSFB_TST_MEAS_REQ_MSG,
   VAL_CSFB_TST_HO_EUTRA_PREP_MSG,
   VAL_CSFB_TST_MOB_FROM_EUTRA_MSG,

   /*-----------------------------------------------------------------
   * LocHandler
   *----------------------------------------------------------------*/
   VAL_LOC_RESP_MSG = 300,
   VAL_LOC_ERROR_MSG,
   VAL_LOC_PILOT_PHASE_MEAS_MSG,
   VAL_LOC_PSEUDO_RANGE_MSG,
   VAL_LOC_GET_SECTOR_INFO_RSP_MSG,
   VAL_LOC_DONE_MSG,
   VAL_LOC_TCP_CONN_REQ_MSG,
   VAL_LOC_TCP_CLOSE_REQ_MSG,
   VAL_GPS_NET_EVT_MSG,
   VAL_GPS_TCPB_SEND_DATA_MSG,
   VAL_GPS_ALM_EPH_UPDATE_MSG,
   VAL_SEC_AESCRYPT_RSP_MSG,
   VAL_RESET_RSP_MSG,
   VAL_LOC_PPP_RECONN_REQ_MSG,
   VAL_LOC_ACTIVE_IND_MSG,

   VAL_GPS_WRITE_OFFSET_INFO_ETS,
   VAL_GPS_READ_OFFSET_INFO_ETS,

   VAL_GPS_FIX_MODE_GET_RSP_MSG,

   VAL_GPS_FACTORY_RF_CNO_MSG,

   VAL_GPS_CT_MPC_IP_CFG_MSG,

   VAL_LOC_PPP_DELAY_REQ_MSG,
   VAL_GPS_CT_MPC_USERNME_PSWRD_CFG_MSG,

   VAL_GPS_READY_MSG,
   VAL_LOC_MPC_CONN_REQ_MSG,

   VAL_MPC_TIME_CALLBACK_MSG=350,
   VAL_MPC_GPS_CALLBACK_MSG=355,
   VAL_MPC_GPS_NET_EVT_MSG,
   VAL_MPC_CLOSE_CONN_MSG,
   VAL_LOC_STATE_GET_MSG,
   VAL_GPS_AFLT_DATA_MSG,


   VAL_TLS_CONN_RSP=360,
   VAL_TLS_SEND_RSP,
   VAL_TLS_RX_IND,
   VAL_TLS_CLOSE_RSP,
   /*-----------------------------------------------------------------
   * PSWAmpsHandler
   *----------------------------------------------------------------*/
   VAL_DTMF_TONE_GEN_MSG = 400,
   VAL_AMPS_EXT_PROTO_MSG,

   /*-----------------------------------------------------------------
   * DbmHandler
   *----------------------------------------------------------------*/
   VAL_DBM_HSPD_SEG_READ_MSG = 450,
   VAL_DBM_GET_EHRPD_MODE_RSP_MSG,
   /*-----------------------------------------------------------------
   * PSWHandler
   *----------------------------------------------------------------*/
   VAL_ACCESS_MAX_CAPSULE_SIZE_MSG = 500,
   VAL_BS_CO_ORD_MSG,
   VAL_NAM_MSG,
   VAL_NAM_RPT_ACTIVE_NAM_MSG,
   VAL_ACTIVE_NAM_RSP_MSG,
   VAL_CHANGE_AKEY_MSG,
   VAL_GET_AKEY_CHECKSUM_RSP_MSG,
   VAL_AKEY_CHANGE_RSP_MSG,
   VAL_NWK_RETRIEVE_CHANNEL_MSG,
   VAL_SET_MIP_PASSWORD_RSP_MSG,
   VAL_ADC_MEAS_RESPONSE_MSG,

   VAL_NWK_RPT_ALERT_MSG,
   VAL_NWK_RPT_ALERT_MORE_INFO_REC_MSG,
   VAL_NWK_RPT_BURST_DTMF_MSG,
   VAL_NWK_RPT_CP_EVENT_MSG,  /* 514 */
   VAL_NWK_RPT_CP_RESPONSE_MSG,
   VAL_NWK_RPT_CP_STATUS_MSG,
   VAL_NWK_RPT_DATA_BURST_MSG,
   VAL_NWK_RPT_FEAT_NOTIF_MORE_INFO_REC_MSG,
   VAL_NWK_RPT_FEATURE_NOTIFICATION_MSG,
   VAL_NWK_RPT_FLASH_MORE_INFO_REC_MSG, /* 520 */
   VAL_NWK_RPT_FLASH_MSG,
   VAL_NWK_RPT_HANDOFF_MSG,
   VAL_NWK_RPT_LOCK_MSG,
   VAL_NWK_RPT_MAINTENANCE_REQUIRED_MSG,
   VAL_NWK_RPT_ORDER_MSG,
   VAL_NWK_RPT_REGISTRATION_ACCEPT_MSG,
   VAL_NWK_RPT_REGISTRATION_REJECT_MSG,
   VAL_NWK_RPT_SERVICE_CONFIG_MSG,
   VAL_NWK_RPT_SERVICE_READY_MSG,
   VAL_NWK_RPT_SERVICE_NEGOTIATION_MODE_MSG, /* 530 */
   VAL_NWK_RPT_START_CONTINUOUS_DTMF_MSG,
   VAL_NWK_RPT_STOP_CONTINUOUS_DTMF_MSG,
   VAL_NWK_RPT_SYSTEM_TIME_MSG,
   VAL_NWK_RPT_UNLOCK_MSG,
   VAL_NWK_RPT_PKT_STATE_MSG,
   VAL_NWK_RPT_PKT_DORM_TMR_MSG,
   VAL_NWK_RPT_PREV_PREVINUSE_MSG,

   VAL_PRL_INFO_MSG,
   VAL_PSW_E911_MODE_MSG,
   VAL_ROAM_INDICATION_MSG,  /* 540 */
   VAL_RPT_CQA_MSG,
   VAL_ORIG_FINISHED_MSG,
   VAL_SET_DEFAULT_SO_MSG,

   VAL_SET_PSW_PARM_RSP_MSG,
   VAL_GET_PSW_PARM_RSP_MSG,
   VAL_PLL_CHANNEL_CONFIG_RSP_MSG,
   VAL_LOCK_FEATURE_CHECK_ERR_MSG,
   VAL_NWK_RPT_MCC_MNC_MSG,

   VAL_PSW_CP_PWR_CTRL,
   VAL_CSS_MARK_CURRENT_1X_SYSTEM_AS_NEGATIVE_MSG,

   VAL_AT_SET_PSW_PARM_RSP_MSG,
   VAL_AT_GET_PSW_PARM_RSP_MSG,
   VAL_AT_HLP_PARM_SET_RSP_MSG,
   VAL_AT_SET_MOBILE_ID_RSP_MSG,
   VAL_RLP_RPT_STAT_MSG,
   VAL_ALL_DATA_INIT_MSG,
   VAL_SET_UI_PARM_RSP_MSG,
   VAL_GET_ALERT_STATUS_MSG,
   VAL_PSW_CP_PWR_CYCLE,
   VAL_CLC_CELL_INFO_MSG,
   VAL_AT_SET_CSS_PARM_RSP_MSG,
   VAL_AT_GET_CSS_PARM_RSP_MSG,
#ifdef MTK_CBP
   VAL_CSS_CP_PWR_CTRL_REQ_MSG,
#endif
#ifdef MTK_DEV_C2K_IRAT
   VAL_CSS_POWER_DOWN_DONE_MSG,
   VAL_CSS_PRL_READ_FINISHED_MSG,
#endif
#ifdef MTK_CBP
   VAL_NWK_EXISTENCE_INFO_MSG,
   VAL_CSS_PRL_IND_MSG,
   VAL_AT_VERIFY_SPC_RSP_MSG,
#endif
   VAL_AT_GET_DO_PARM_RSP_MSG,
#ifdef MTK_CBP
   VAL_PSW_RPT_SERVICE_OPTION_MSG,
#endif
#ifdef MTK_DEV_C2K_IRAT
   VAL_CSS_CS_REG_STATUS_NOTIFY_IND_MSG,
#endif
#ifdef MTK_CBP_ENCRYPT_VOICE
   VAL_PSW_ENCRYPT_VOICE_RSP_MSG,
#endif

   /*-----------------------------------------------------------------
   * SMSHandler
   *----------------------------------------------------------------*/
   VAL_SMS_BCAST_ERROR_MSG = 600,
   VAL_SMS_BCAST_IND_PARMS_MSG,
   VAL_SMS_CAUSE_CODE_STATUS_MSG,
   VAL_SMS_DELIVER_IND_DATA_MSG,
   VAL_SMS_DELIVER_IND_PARMS_MSG,
   VAL_SMS_ERROR_MSG,
   VAL_SMS_RX_IND_MSG,
   VAL_SMS_TX_RSP_MSG,
   VAL_SMS_CAUSE_CODE_MSG,
   VAL_SMS_ACK_L2_ACKED_MSG,

   VAL_SMS_TST_CONNECT_MSG =610,
   VAL_SMS_TST_BCAST_CONNECT_MSG,
   VAL_SMS_TST_BCAST_DISCONNECT_MSG,
   VAL_SMS_TST_BCAST_PREF_MSG,
   VAL_SMS_TST_CANCEL_MSG,
   VAL_SMS_TST_CAUSE_CODE_STATUS_MSG,
   VAL_SMS_TST_DISCONNECT_MSG,
   VAL_SMS_TST_PREF_SRV_OPT_MSG,
   VAL_SMS_TST_SUBMIT_MSG,
   VAL_SMS_TST_TERM_STATUS_MSG,
   VAL_SMS_TST_USER_ACK_MSG = 620,
   VAL_SMS_TST_RETX_AMOUNT_MSG,
   VAL_SMS_TST_CAUSE_CODE_MSG,
   VAL_SMS_GET_COUNT_MSG,
   VAL_SMS_GET_FIRST_MSG,
   VAL_SMS_GET_NEXT_MSG,
   VAL_SMS_WRITE_MSG,
   VAL_SMS_DEL_ALL_MSG,
   VAL_SMS_DEL_MSG,
   VAL_SMS_SEND_MSG,
   VAL_SMS_SEND_INDEX_MSG = 630,
   VAL_SMS_SET_STATUS_MSG,
   VAL_SMS_OVER_C2K_SEND_REQ,
   VAL_SMS_OVER_IMS_SEND_REQ,
   VAL_SMS_IMCSMS_SEND_REQ,
   VAL_SMS_SDM_SEND_RSP,
   VAL_SMS_IMCSMS_RECV_REQ,
#if defined(LGT_EXTENSIONS)
   VAL_SMS_UNKNOWN_ERROR_MSG = 640,
   VAL_SMS_UNKNOWN_IND_PARMS_MSG,
#endif

   /*-----------------------------------------------------------------
   * HLP Power down Handler
   *-----------------------------------------------------------------*/
   VAL_HLP_POWER_DOWN_DONE_MSG = 650,

   /*-----------------------------------------------------------------
   * EVDO stack Handler
   *----------------------------------------------------------------*/
   VAL_HRPD_SESSION_STATUS_MSG = 680,
   VAL_HRPD_NTWK_ACQD_MSG,
   VAL_HRPD_CONN_STATUS_MSG,
   VAL_HRPD_STAT_PEEK_RSP_MSG,
   VAL_CLC_POWER_EV_MSG,
   VAL_CLC_AT_STATUS_MSG,
#ifdef MTK_DEV_C2K_IRAT
    VAL_CLC_NBR_FREQS_RPT_MSG,
#endif
   VAL_HRPD_CHAN_CHANGE_IND,

   VAL_HRPD_A12_STATUS_MSG = 690,
   /*-----------------------------------------------------------------
   * HWD Handler
   *----------------------------------------------------------------*/
   VAL_KEYPAD_MSG = 700,
   VAL_PWR_KEY_TIMEOUT,
   VAL_AUDIO_EDAI_MIC_CTRL_DATA_MSG,
   VAL_HWD_BATTERY_READ_DATA_MSG,
   VAL_HWD_TEMPERATURE_READ_DATA_MSG,
   VAL_HWD_RX_TX_POWER_INFO_DATA_MSG,
   VAL_PSW_PILOT_PWR_RPT_MSG,
   VAL_DO_RX_TX_POWER_INFO_DATA_MSG,

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
    VAL_HWD_RX_TX_POWER_DETECTOR_MSG,
#endif
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
    RMC_VAL_DO_RX_TX_TIME_INFO_DATA_REQ_MSG,
    VAL_RMC_DO_RX_TX_TIME_INFO_DATA_RSP_MSG,
    VAL_L1D_RX_TX_TIME_INFO_DATA_REQ_MSG,
    VAL_L1D_RX_TX_TIME_INFO_DATA_RSP_MSG,
#endif

#if defined(__DYNAMIC_ANTENNA_TUNING__)||defined(__SAR_TX_POWER_BACKOFF_SUPPORT__)
    VAL_RMC_DO_DAT_CFG_RSP_MSG,
    VAL_L1D_1X_DAT_CFG_RSP_MSG,
#endif

   /*-----------------------------------------------------------------
   * VMEMO Handler
   *----------------------------------------------------------------*/
   /* VMEMO messages used by ETS */
   VAL_VMEMO_REC_START_TST_MSG = 800,
   VAL_VMEMO_STOP_TST_MSG,       /*stop for record and playing*/
   VAL_VMEMO_REC_PAUSE_TST_MSG,
   VAL_VMEMO_REC_RESUME_TST_MSG,
   VAL_VMEMO_PLAY_START_TST_MSG,
   VAL_VMEMO_PLAY_PAUSE_TST_MSG,
   VAL_VMEMO_PLAY_RESUME_TST_MSG,

   /* VMEMO DBM Ack messages */
   VAL_VMEMO_REC_INIT_ACK_MSG,     /* dbm ack to erase command before start of recording */
   VAL_VMEMO_WRITE_ACK_MSG,        /* dbm ack to data write */
   VAL_VMEMO_PLAY_INIT_ACK_MSG,    /* dbm ack to header read before playback */
   /* Messages for TEST purposes only (via ETS) */
   VAL_VMEMO_TEST_REC_RAW_PCM_MSG,
   VAL_VMEMO_TEST_REC_SBC_ENC_MSG,

   VAL_VMEMO_MIXER_START_TST_MSG,  /* Start PCM Mixer playback */
   VAL_VMEMO_MIXER_STOP_TST_MSG,

   /*-----------------------------------------------------------------
   * VREC Handler
   *----------------------------------------------------------------*/
   VAL_VREC_READ_INIT_ACK_MSG = 900,
   VAL_VREC_ERASE_ACK_MSG,
   VAL_VREC_COMP_DATA_WRITE_ACK_MSG,
   VAL_VREC_TRAINING_DATA_WRITE_ACK_MSG,
   VAL_VREC_HEADER_DATA_WRITE_ACK_MSG,

   VAL_VREC_MIXED_DATA_MSG,
   VAL_VREC_PCM_DATA_MSG,

   VAL_VREC_PLAYBACK_COMPLETE_MSG,
   VAL_VREC_PLAYBACK_CANCEL_MSG,
   VAL_VREC_TRAINING_START_MSG,
   VAL_VREC_TRAINING_ACCEPT_MSG,
   VAL_VREC_TRAINING_CANCEL_MSG,
   VAL_VREC_RECOGNITION_START_MSG,
   VAL_VREC_RECOGNITION_CANCEL_MSG,
   VAL_VREC_RESET_BUFFERS_MSG,

   VAL_VREC_CAPTURE_CANCEL_MSG,
   VAL_VREC_MATCHING_CANCEL_MSG,

   VAL_VREC_READ_VOCABULARY_MSG,
   VAL_VREC_READ_TRAINCB_MSG,
   VAL_VREC_TEST_FUNCTION_MSG,

   /*-----------------------------------------------------------------
   * VSTRM Handler
   *----------------------------------------------------------------*/
   VAL_VSTRM_PCM_DATA_MSG  = 950,

	VAL_SPH_CCCI_QUEUE_PROC_MSG = 960,
	VAL_SPH_HWDPART_INIT_DONE_MSG,
	VAL_SPH_VALPART_INIT_DONE_MSG,
	VAL_SPH_EPOF_PROC_MSG,
	VAL_SPH_EPOF_DONE_MSG,
	VAL_SPH_CCCIPART_INIT_DONE_MSG,
	VAL_SPH_SERVICE_OPTION_NOTIFY_MSG,
	VAL_SPH_RATE_REDUCE_NOTIFY_MSG,
	VAL_SPH_AUDIO_CUST_DUMP_REQ,

   /*-----------------------------------------------------------------
   * USERINFO Handler
   *----------------------------------------------------------------*/
   VAL_KPAD_DISABLED_MSG = 1000,
   VAL_EMERGENCY_MODE_TEST_MSG,
   VAL_SET_AUTO_ANSWER_DELAY_MSG,
   VAL_GET_RSSI_ACK_MSG,
   VAL_L1D_RSSI_RPT_MSG,
   VAL_HRPD_RSSI_RPT_MSG,

   /*-----------------------------------------------------------------
    * Sound API Handler
    *----------------------------------------------------------------*/
   VAL_SND_SET_DEVICE_MSG = 1100,          /* Sound Set Device from ETS */
   VAL_SND_SET_VOLUME_MSG,                 /* Sound Set Volume from ETS */
   VAL_SND_SOUND_STOP_MSG,                 /* Sound Stop from ETS */
   VAL_SND_TONE_PLAY_MSG,                  /* Play comfort tone from ETS */
   VAL_SND_MUSIC_PLAY_MSG,                 /* MUSIC Play from ETS */
   VAL_SND_MUSIC_FAST_FWD_MSG,             /* MUSIC Fast Fwd from ETS */
   VAL_SND_MUSIC_REWIND_MSG,               /* MUSIC Rewind from ETS */
   VAL_SND_MUSIC_SUSPEND_MSG,              /* MUSIC Suspend from ETS */
   VAL_SND_MUSIC_RESUME_MSG,               /* MUSIC Resume from ETS */
   VAL_SND_VIBRATE_START_MSG,              /* Start Vibrate */
   VAL_SND_VIBRATE_STOP_MSG,               /* Stop Vibrate  */
   VAL_SND_TEST_MSG,                       /* Sound testing for CDS6 */
   VAL_SND_FLIP_OPEN_VOL_MSG,              /* Set to flip open volume from ETS */
   VAL_SND_MUSIC_STATUS_MSG,
   VAL_SND_SET_CHAN_MUTE_MSG,              /* Mutes/unmutes individual spkr channels */
   VAL_SND_TOTALTIME_GET_MSG,
   VAL_SND_USER_TONE_PLAY_MSG,             /* Play user-defined tone from ETS */
   VAL_SND_MIDI_SET_CFG_MSG,               /* Set MIDI config params from ETS */
   VAL_SND_PCM_DATA_TX_RSP_MSG,            /* Response from DSPv for PCM Data transmitted */
   VAL_SND_SET_ABE_MODE_MSG,               /* Sound set Arbitrary Bandwidth Extension mode */
   VAL_SND_GET_VOICE_MODE_INFO_MSG,        /* Get info about current voice audio mode */
   VAL_SND_SET_BT_DEV_TYPE_MSG,            /* Set bluetooth device type */
   VAL_SND_SET_EQUALIZER_PARAMS_MSG,       /* Set audio decoder equalizer parameters */

   /* Audio Tuning messages */
   VAL_SND_INIT_AUDIO_TUNING_PARAMS_MSG,
   VAL_SND_STORE_AUDIO_TUNING_FILE_MSG,
   VAL_SND_READ_AUDIO_TUNING_PARAMS_MSG,
   VAL_SND_UPDATE_AUDIO_TUNING_PARAMS_MSG,

   VAL_SND_SRV_OPT_CONN_MSG,               /* Indicates that voice srv opt is now connected */
   VAL_SND_MUSIC_ENDED_MSG,


   /*-----------------------------------------------------------------
    * Display API Handler
    *----------------------------------------------------------------*/
   VAL_DISP_IMAGE_SHOW_MSG = 1140,
   VAL_DISP_IMAGE_CONVERT_FORMAT_MSG,
   VAL_DISP_IMAGE_GET_PROPERTIES_MSG,
   VAL_DISP_DISP_GET_DEVICE_INFO_MSG = 1150,
   VAL_DISP_BITMAP_GET_INFO_MSG,
   VAL_DISP_BITMAP_GET_DATA_MSG,

   /*-----------------------------------------------------------------
    * Datacard Sw Support API Handler
    *----------------------------------------------------------------*/
   VAL_DATACARD_GET_NETWORK_INFO_MSG = 1250,
   VAL_DATACARD_OTA_ACTIVATE_MSG = 1251,
   VAL_DATACARD_SET_DEV_PARAM_ETS=1252,
   VAL_DATACARD_GET_DEV_PARAM_ETS=1253,
   VAL_DATACARD_GET_CONN_INFO_MSG=1254,
   VAL_DATACARD_GET_NTWK_ERR_STR_MSG=1255,

   VAL_HSPD_HLP_PARM_SET_ETS = 2000,        /* Individual HL HSPD param - menu choice */
   VAL_HSPD_HLP_PARM_GET_ETS = 2001,
   VAL_SET_PSW_PARM_MSG,
   VAL_GET_PSW_PARM_MSG,
   VAL_UI_PARM_SET_MSG,
   VAL_UI_PARM_GET_MSG,
   VAL_PSW_MDN_UPDATED_MSG,
   VAL_PSW_ESN_UPDATED_MSG,
   VAL_SET_UI_LOCK_CODE_RSP_MSG,
   VAL_SLEEPOVER_INDICATOR_MSG,
   VAL_UICC_PARM_SET_MSG,
   VAL_UICC_PARM_GET_MSG,
   VAL_SET_UICC_PARM_RSP_MSG,
   VAL_GET_UICC_PARM_RSP_MSG,
   VAL_GET_TRANSMIT_INFO_RSP_MSG,
   VAL_SET_SYS_PARM_MSG,
   VAL_GET_SYS_PARM_MSG,
   VAL_AUTO_POWER_UP_SET_MSG,
   VAL_AUTO_POWER_UP_GET_MSG,
   VAL_SET_CSS_PARM_MSG,
   VAL_GET_CSS_PARM_MSG,
   VAL_SET_CSS_PARM_RSP_MSG,
   VAL_GET_CSS_PARM_RSP_MSG,
   VAL_HLP_HSPD_SEG_INIT_RSP_MSG,
   VAL_HLP_HSPD_SEG_PROFILE_INIT_RSP_MSG,

  /* Other EVDO Params */
   VAL_DO_PARM_SET_ETS=2070,            /* EVDO params */
   VAL_DO_PARM_GET_ETS,
  /*CUSTOMIZED flags*/
#ifdef CHINATELECOM_SMS_REG_CP
   VAL_SET_AUTOREG_INFO_MSG,
   VAL_CP_AUTOREG_SET_ETS,
   VAL_CP_AUTOREG_GET_ETS,
   VAL_CP_DB_WRITE_ACK_MSG,
   VAL_CP_DB_READ_ACK_MSG,
   VAL_CP_SMS_AUTOREG_MSG,
/*   VAL_CP_SMS_NWINIT_REG_MSG,*/
   VAL_CP_SMSREG_TIMER_EXPIRE_MSG,
   VAL_CP_SMSREG_TRIGGER_REQ_MSG,
#endif
   VAL_GPS_OPEN_DEVICE_HW_INIT_ETS = 2080,
   VAL_GPS_CLOSE_DEVICE_ETS,
   VAL_GPS_POSITION_GET_ETS,
   VAL_GPS_START_FIX_ETS,
   VAL_GPS_QUERY_FIX_ETS,
   VAL_GPS_STOP_FIX_ETS,
   VAL_GPS_CONFIG_FIX_MODE_SET_ETS,
   VAL_GPS_CONFIG_FIX_RATE_SET_ETS,
   VAL_GPS_CONFIG_QOS_SET_ETS,
   VAL_GPS_WRITE_SERVER_CONFIG_ETS,
   VAL_GPS_ENABLE_SECURITY_ETS,
   VAL_GPS_WRITE_SECURITY_CONFIG_ETS,
   VAL_GPS_READ_SECURITY_CONFIG_ETS,
   VAL_GPS_READ_ENCRYPT_CONFIG_ETS,
   VAL_GPS_LBS_PRIVACY_SETTING_GET_ETS,
   VAL_GPS_LBS_PRIVACY_SETTING_SET_ETS,
   VAL_GPS_BASESSD_GET_ETS,
   VAL_GPS_RAND_PERIOD_SET_ETS,
   VAL_GPS_RESET_ASSIST_ETS,

   VAL_LOC_TCP_CONN_ETS,
   VAL_LOC_TCP_CLOSE_ETS,

   VAL_NMEA_CFG_SET_ETS,
   VAL_NMEA_CFG_GET_ETS,
   VAL_GPS_RESTART_FIX_ETS,

   VAL_GPS_SEC_SET_ETS,
   VAL_GPS_CONFIG_TECH_ETS,

   VAL_GPS_DBM_READ_RSP_MSG,
   VAL_GPS_BASESSD_SET_MSG,

   VAL_GPS_GET_FIX_MODE_MSG,
   VAL_GPS_START_MODE_SET_MSG,
   VAL_GPS_SESSION_OPEN_MSG,

   VAL_GPS_READ_SECURITY_CONFIG_W_STATUS_ETS,
   VAL_GPS_READ_ENCRYPT_CONFIG_W_STATUS_ETS,
   VAL_GPS_NMEA_OUTPUT_SETTING_ETS,
   VAL_GPS_CONFIG_FIX_RATE_GET_ETS,
   VAL_GPS_SESSION_CANCEL_MSG,
   VAL_GPS_PROCESS_RX_MSG,
   VAL_GPS_READ_SERVER_CONFIG_ETS,
   VAL_GPS_PPM_RSP_MSG,
   VAL_GPS_SUPL_CELL_INFO_GET_MSG,

    /* VALAPI_GPS response messages */
    VAL_GPS_OPEN_DEVICE_HW_INIT_CONF_MSG=2150,
    VAL_GPS_START_MODE_SET_CONF_MSG,
    VAL_GPS_SECURITY_SET_CONF_MSG,
    VAL_GPS_BASE_SSD_SET_CONF_MSG,
    VAL_GPS_WRITE_SERVER_CONFIG_CONF_MSG,
    VAL_GPS_GET_FIX_MODE_CONF_MSG,
    VAL_GPS_SESSION_OPEN_CONF_MSG,
    VAL_GPS_SESSION_ENABLE_SECURITY_CONF_MSG,
    VAL_GPS_SESSION_RANDOM_PERIOD_SET_CONF_MSG,
    VAL_GPS_SESSION_CONFIGURE_FIX_MODE_CONF_MSG,
    VAL_GPS_SESSION_CONFIGURE_FIX_RATE_CONF_MSG,
    VAL_GPS_SESSION_CONFIGURE_QOS_CONF_MSG,
    VAL_GPS_SESSION_START_FIX_CONF_MSG,
    VAL_GPS_SESSION_GET_POSITION_CONF_MSG,
    VAL_GPS_SESSION_STOP_FIX_CONF_MSG,
    VAL_GPS_SESSION_READ_SECURITY_CONFIG_CONF_MSG,
    VAL_GPS_SESSION_WRITE_SECURITY_CONFIG_CONF_MSG,
    VAL_GPS_SESSION_READ_ENCRYPT_CONFIG_CONF_MSG,
    VAL_USERMODE_MSG,
    VAL_TESTMODE_MSG,
    VAL_APMODE_MSG,
    VAL_GPS_RESET_ASSISTDATA_CONF_MSG,
    VAL_GPS_SUPL_CELL_INFO_MSG,
    VAL_GPS_TEST_MODE_SET_MSG,
    VAL_GPS_RFDEALY_SET_MSG,
#ifdef MTK_PLT_ON_PC
   VAL_CONFIG_SBP_INFO_MSG,
#endif
    /* PGPS Messages */

    VAL_PGPS_DBM_READ_RSP_MSG = 2200,
    VAL_PGPS_DBM_WRITE_RSP_MSG,
    VAL_PGPS_CFG_SET_ETS,
    VAL_PGPS_CFG_GET_ETS,
    VAL_PGPS_START_ETS,
    VAL_PGPS_PDA_TIMER_MSG,
    VAL_PGPS_PDA_NETWORK_START,
    VAL_PGPS_SOCKET_CONNECTED,
    VAL_PGPS_SOCKET_READ,
    VAL_PGPS_PDA_PROCESS,
    VAL_PGPS_SOCKET_CLOSED,  // 2210
    VAL_PGPS_NET_UNREGISTER,

    /* PGPS on AP Messages */
    VAL_PGPS_SYS_TIME_REQ_TO_AP_MSG,
    VAL_PGPS_GET_EE_REQ_TO_AP_MSG,
    VAL_PGPS_SET_EE_REQ_TO_AP_MSG,

    VAL_PGPS_GET_CELLID_INFOR_MSG,
    VAL_PGPS_GET_CELLID_INFOR_UPDATE_MSG,

    /* GPS on AP Messages */
    VAL_AP_GPS_DEVICE_POWER_ON_REQ_MSG,
    VAL_AP_GPS_PRECISE_TIME_AIDING_RSP_MSG,
    VAL_AP_GPS_ASSISTACQUIST_DATA_MSG,
    VAL_AP_GPS_FREQUENCY_AIDING_RSP_MSG,  // 2220
    VAL_AP_GPS_CANCELLATION_REQ_MSG,
    VAL_AP_GPS_CP_LOCATION_RESPONSE_MSG,
    VAL_AP_GPS_REFERENCE_LOCATION_RESP_MSG,
    VAL_AP_GPS_ASSIST_DATA_REQ_MSG,
    VAL_AP_GPS_SETEPH_PRN_MSG,
    VAL_AP_GPS_SETALM_PRN_MSG,
    VAL_AP_GPS_SETION_MSG,
    VAL_AP_GPS_SET_REFLOC_MSG,

    VAL_AP_GPS_ASSIST_FINISH_MSG,
    VAL_AP_GPS_START_ACK,  // 2230
    VAL_AP_GPS_STOP_ACK,   //CP ACK
    VAL_AP_GPS_STOP_REQ,
    VAL_AP_GPS_CANCEL_IND,
#ifdef MTK_CBP
    VAL_AP_GPS_DELETE_ASSIST_DATA_MSG,
    VAL_AP_GPS_FAKE_STOP_REQ_MSG,
    VAL_AP_GPS_MS_INFO_REQ_RECVD_MSG,
#endif
#ifdef MTK_DEV_GPSONE_ON_LTE
#ifdef GPSONE_ON_LTE_PRE_OPTIMIZATION
   VAL_AP_GPS_SOCKET_CREATE_REQ_MSG,
   VAL_AP_GPS_SOCKET_CREATE_RSP_MSG,
   VAL_AP_GPS_TCP_SEND_DATA_REQ_MSG,
   VAL_AP_GPS_TCP_SEND_RSP_MSG,
   VAL_AP_GPS_TCP_DATA_RECV_MSG,
   VAL_AP_GPS_TCP_DATA_RECV_RSP_MSG,
   VAL_AP_GPS_SOCKET_CLOSE_REQ_MSG,
   VAL_AP_GPS_SOCKET_CLOSE_RSP_MSG,
   VAL_AP_GPS_RAT_MODE_QUERY_MSG,
   VAL_AP_GPS_RAT_MODE_RSP_MSG,
   VAL_AP_GPS_CONNECTION_STATUS_IND,
#endif
#endif


#ifdef MTK_GPS_SYNC_DEV
   VAL_AP_GPS_TIME_IND,
#endif
#ifdef MTK_CBP
   VAL_AP_GPS_FAKE_POWER_ON_REQ_MSG,
   VAL_AP_GPS_EMERGENCY_CALL_IND_MSG,
   VAL_AP_GPS_SENSIASSIST_DATA_MSG,
#endif
#ifdef MTK_PLT_ON_PC
   VAL_GPS_RPC_REV_TIME_AIDING_REQ_MSG,
   VAL_GPS_RPC_REV_PSEUDORANGE_MEAS_RPT_MSG,
   VAL_GPS_RPC_REV_ASSIST_DATA_REQ_MSA_MSG,
   VAL_GPS_RPC_REV_ASSIST_DATA_REQ_MSB_MSG,
   VAL_GPS_RPC_REV_STOP_ACK_MSG,
   VAL_GPS_RPC_REV_POWER_ON_ACK_MSG,
   VAL_GPS_RPC_REV_NI_POS_REQ_MSG,
   VAL_GPS_RPC_REV_RAT_MODE_SETTING_MSG,
   VAL_GPS_RPC_REV_RAT_MODE_RSP_MSG,
   VAL_GPS_RPC_REV_SOCKET_CREATE_RSP_MSG,
   VAL_GPS_RPC_REV_SOCKET_CLOSE_RSP_MSG,
   VAL_GPS_RPC_REV_TCP_DATA_SEND_RSP_MSG,
#ifdef MTK_CBP
   VAL_GPS_RPC_REV_SUPL_TCP_DATA_SEND_RSP_MSG,
#endif
   VAL_GPS_RPC_REV_TCP_DATA_RECV_MSG,
#ifdef MTK_CBP
   VAL_GPS_RPC_REV_SUPL_TCP_DATA_RECV_MSG,
#endif
   VAL_GPS_RPC_REV_DATA_CONN_STATUS_MSG,
   VAL_GPS_RPC_REV_POWER_ON_MSG,
   VAL_GPS_RPC_REV_STOP_MSG,
#ifdef MTK_CBP
   VAL_GPS_RPC_REV_SUPL_CELL_ID_REQ_MSG,
   VAL_GPS_RPC_REV_SUPL_GET_NETWORK_TYPE_MSG,
   VAL_GPS_RPC_REV_SUPL_GET_NETWORK_MODE_MSG,
#endif

   VAL_GPS_RPC_FWD_PARSED_POS_RPT_MSG,
   VAL_GPS_RPC_FWD_STATUS_RPT_MSG,
   VAL_GPS_RPC_FWD_AFLT_LOC_RPT_MSG,
   VAL_GPS_RPC_FWD_ASSIST_EPH_RPT_MSG,
   VAL_GPS_RPC_FWD_ASSIST_ALM_RPT_MSG,
   VAL_GPS_RPC_FWD_ASSIST_ION_RPT_MSG,
   VAL_GPS_RPC_FWD_ASSIST_DATA_DONE_MSG,
   VAL_GPS_RPC_FWD_EVT_MSG,
   VAL_GPS_RPC_FWD_DELETE_ASSIST_DATA_REQ_MSG,
   VAL_GPS_RPC_FWD_INJECT_ASSIST_DATA_REQ_MSG,
   VAL_GPS_RPC_FWD_POWER_ON_REQ_MSG,
   VAL_GPS_RPC_FWD_DATA_CALL_REQ_MSG,
   VAL_GPS_RPC_FWD_FRAME_SYNC_MSG,
   VAL_GPS_RPC_FWD_NI_POS_REQ_MSG,
   VAL_GPS_RPC_FWD_STOP_REQ_MSG,
   VAL_GPS_RPC_FWD_STOP_ACK_MSG,
   VAL_GPS_RPC_FWD_POWER_ON_ACK_MSG,
   VAL_GPS_RPC_FWD_SOCKET_CREATE_REQ_MSG,
   VAL_GPS_RPC_FWD_SOCKET_CLOSE_REQ_MSG,
   VAL_GPS_RPC_FWD_TCP_DATA_SEND_REQ_MSG,
   VAL_GPS_RPC_FWD_TCP_DATA_RECV_RSP_MSG,
   VAL_GPS_RPC_FWD_QUERY_RAT_MODE_MSG,
#ifdef MTK_CBP
   VAL_GPS_RPC_FWD_GET_NT_TYPE_MSG,
   VAL_GPS_RPC_FWD_GET_OP_MODE_MSG,
   VAL_GPS_RPC_FWD_GET_CELL_INFO_MSG,
   VAL_GPS_RPC_FWD_PPM_RPT_MSG,
   VAL_GPS_RPC_FWD_INJECT_SENSIT_ASSIST_DATA_REQ_MSG,
#endif
#endif

    /*-----------------------------------------------------------------
    * Silent Logging
    *----------------------------------------------------------------*/
    VAL_SILENT_LOG_CFG_CREATE_MSG = 4850,
    VAL_SILENT_LOG_PARMS_SET_MSG,
    VAL_SILENT_LOG_PARMS_GET_MSG,
    VAL_SILENT_LOG_DATA_INFO_GET_MSG,
    VAL_SILENT_LOG_PAUSE_MSG,
    VAL_SILENT_LOG_RESUME_MSG,
    VAL_SILENT_LOG_PARMS_UPDATE_MSG,

   /*-----------------------------------------------------------------
   * DBM Handler
   *----------------------------------------------------------------*/
   VAL_FLUSH_ACK_MSG = 4900,
   VAL_FLUSH_RF_ACK_MSG,
   VAL_NV_MSG_ACK_MSG,
   VAL_L1DTST_GETPHONESTATUS,
   VAL_SET_MOBILE_ID_MSG,
   VAL_GET_ERI_VERSION_MSG = 5000,

   /* PHB Msg */
   VAL_UIM_GET_PHB_REC_PARAMS_MSG = 6000,
   VAL_UIM_GET_PHB_REC_MSG,
   VAL_UIM_UPDATE_PHB_REC_MSG,
   VAL_UIM_ERASE_PHB_REC_MSG,
   VAL_UIM_GET_SDN_REC_PARAMS_MSG,
   VAL_UIM_GET_SDN_REC_MSG,

   VAL_AT_UIM_ERASE_PHB_REC_MSG,
   VAL_AT_UIM_UPDATE_PHB_REC_MSG,

   /* Sms Msg */
   VAL_UIM_GET_SMS_REC_PARAMS_MSG,
   VAL_UIM_GET_SMS_REC_MSG,
   VAL_UIM_UPDATE_SMS_REC_MSG,
   VAL_UIM_ERASE_SMS_REC_MSG,

   VAL_UTK_PROAVTIVE_CMD_AT_UTC_MSG,
#ifdef MTK_CBP
   VAL_UIM_GET_FDN_REC_MSG,
   VAL_UIM_GET_FDN_STATUS_MSG,
   VAL_AT_UIM_SET_FDN_MSG,
   VAL_AT_UIM_ERASE_FDN_REC_MSG,
   VAL_AT_UIM_UPDATE_FDN_REC_MSG,
   VAL_UIM_UPDATE_FDN_REC_MSG,
   VAL_UIM_ERASE_FDN_REC_MSG,
#endif

   /* Chv Msg */
   VAL_CHV_GET_STATUS_MSG         = 6020,
   VAL_CHV_VERIFY_MSG,
   VAL_CHV_CHANGE_MSG,
   VAL_CHV_ENABLE_MSG,
   VAL_CHV_DISABLE_MSG,
   VAL_CHV_UNBLOCK_MSG,
#ifdef MTK_CBP
   VAL_UIM_ECC_LIST_IND_MSG,
#endif
#ifdef MTK_DEV_C2K_IRAT
   VAL_CHV_STATUS_CHANGE_MSG,
   VAL_CHV_LOCAL_VERIFY_MSG,
#endif
#ifdef FEATURE_UTK
   /*Utk Msg */
   VAL_UTK_TERMINAL_PROFILE_MSG   = 6030,
   VAL_UTK_MENU_SELECTION_MSG,
   VAL_UTK_SMS_PP_DOWNLOAD_MSG,
   VAL_UTK_TERMINAL_RESPONSE_MSG,
   VAL_UTK_REQUEST_UIMVER_MSG,
   VAL_UTK_REQUEST_IMG_MSG,
   VAL_UTK_REQUEST_IMG_DATA_MSG,
   VAL_UTK_PROAVTIVE_CMD_MSG,
   VAL_UTK_CALL_CONTROL_MSG,
   /* Event download Msg */
   VAL_UTK_DATA_AVAILABLE_MSG,
   VAL_UTK_MT_CALL_EVT_MSG,
   VAL_UTK_CALL_CONN_EVT_MSG,
   VAL_UTK_CALL_DISC_EVT_MSG,
   VAL_UTK_LOC_STATUS_EVT_MSG,
   VAL_UTK_ACCTECH_CHANGE_EVT_MSG,
   VAL_UTK_APP_INIT_TERM_MSG,
   VAL_UTK_IMS_REG_STATUS_UPDATE_IND,
   VAL_UTK_IMS_TIMER_EXPIRED_MSG,

   /* Utk test */
   VAL_UTK_TST_GET_INPUT_DONE_MSG,
   VAL_UTK_TST_SELECT_ITEM_DONE_MSG,
   VAL_UTK_TST_COMMON_DONE_MSG,
   VAL_UTK_TST_SELECT_MENU_MSG,
   VAL_UTK_TST_INIT_MSG,
   VAL_UTK_TST_SMS_DOWN_MSG,
   VAL_UTK_TST_EXIT_MSG,
#endif

   /* VAL UIM MDN messages  */
   VAL_UIM_GET_MDN_REC_NUM_MSG    = 6060,
   VAL_UIM_READ_MDN_REC_MSG,
   VAL_UIM_UPDATE_MDN_REC_MSG,
   VAL_UIM_DEL_MDN_REC_MSG,
   VAL_UIM_NOTIFY_REGISTER_MSG,
   VAL_UIM_GET_SP_NAME_MSG,
   VAL_UIM_IN_CALL_DETECT_MSG,

   VAL_UIM_GET_GSM_IMSI_MSG,
   VAL_UIM_GET_ICCID_MSG,
   VAL_UIM_RESET_RESPONSE_MSG,

   VAL_UIM_GET_SMSVP_REC_NUM_MSG    = 6070,
   VAL_UIM_READ_SMSVP_REC_MSG,
   VAL_UIM_UPDATE_SMSVP_REC_MSG,
   VAL_UIM_GET_PREF_LANG,
   VAL_UIM_GET_UIM_CARD_ID,
   VAL_UIM_GET_PROVIDER_NAME,

   VAL_UIM_STORE_ESN_MSG,
   VAL_UIM_GET_ESN_MSG,
   VAL_UIM_GET_CST_MSG,
   VAL_UIM_GET_EST_MSG,

   /* VAL UIM Detect message*/
   VAL_UIM_CARD_REMOVED_MSG,
   VAL_UIM_PROACTIVE_CMD_MSG,
   VAL_UIM_SMS_PP_DOWNLOAD_RSP_MSG,
   VAL_UIM_UTK_REFRESH_RSP_MSG,
   VAL_UIM_HLP_FILE_CHANGE_COMPLETED,
   VAL_UIM_CHV_STATUS_CHANGE_IND_MSG,
   VAL_UIM_OP01_STATUS_MSG,
   VAL_UIM_PUK_LOCK_MSG,

   /* VAL sms memory status Msg*/
   VAL_SMS_MEMORY_STATUS_MSG     = 6090,

#ifdef __CARRIER_RESTRICTION__
   /*get IMSI_M, IMSI_T, SPN, GID1, GID2 from UIM*/
   VAL_UIM_GET_UML_DATA_MSG,
   VAL_CARRIER_RESTRICTION_SYNC_IND_MSG,
   VAL_CARRIER_RESTRICTION_STATUS_IND_MSG,
   VAL_UIM_ERROR_IND_MSG,
#endif

   VAL_GET_DEBUG_INFO_MSG        = 6200,

   /* VAL ATC handle*/
   VAL_AT_FSM_RESET_REQ_MSG = 6204,
   VAL_ASYNC_ALERT_IND_MSG,

   VAL_EIA617_BREAK_REQ_MSG,
   VAL_C108_IND_MSG,
   VAL_ONLINE_CMD_REQ_MSG,
   VAL_BAUD_RATE_IND_MSG,
   VAL_REV_TX_REQ_MSG = 6210,
   VAL_FWD_TX_REQ_MSG,
   VAL_TIMER_EXPIRED_MSG,
   VAL_AT_DBM_WRITE_RSP_MSG,
   VAL_AT_DBM_READ_RSP_MSG,
   VAL_AT_SET_DEFAULT_REQ_MSG,
   VAL_AT_INIT_REQ_MSG,
   VAL_AT_FLUSH_REQ_MSG,
   VAL_AT_SYS_INIT_MSG,
   VAL_SET_QNC_DIAL_STR_MSG,
#ifdef SYS_OPTION_ENHANCEDAT
   VAL_ENHANCED_AT_CMD_REQ_MSG = 6220,
   VAL_ENHANCED_CMD_BUFFER_ACK_MSG,
   VAL_ENHANCED_AT_CMD_RSP_MSG,

   VAL_ATC_POWERUP_IND_MSG,
   VAL_ATC_NVM_INITED_MSG,

   VAL_AT_UIM_RUN_CAVE_ACK_MSG,

   VAL_AT_CSIM_ACK_MSG,

   VAL_ENHANCED_AT_GGSMIMSI,
#endif
   VAL_AT_CRSM_ACK_MSG = 6228,             /* used by both AT and generic uim API */
   VAL_DO_PARM_SET_SUC_MSG,
   VAL_NO_SVC_IND_MSG = 6230,               /* To indicate no service.         */
   VAL_IOP_FWD_TX_RSP_MSG,
   VAL_IOP_SET_BAUD_RSP_MSG,
   VAL_IOP_MUX_EVEVT_MSG,
   VAL_AT_L1D_RSSI_MSG,
   VAL_AT_BATTERY_STATUS_MSG,
   VAL_SET_IPSERVICE_TYPE_MSG, /*To set the IPService type from VAL or ETS*/
   VAL_GET_IPSERVICE_TYPE_MSG,
   VAL_HLP_PARM_SET_RSP_MSG,
   VAL_HLP_PARM_GET_RSP_MSG,
   VAL_DO_PARM_SET_RSP_MSG,
   VAL_DO_PARM_SET_NO_RSP,
   VAL_DO_PARM_GET_RSP_MSG,
   VAL_AT_PENDSMS_TM_EXPIRE,
   VAL_AT_UTK_SMS_PP_DOWNLOAD_RSP_MSG,
#ifdef MTK_DEV_ETS_ENHANCEMENT
   VAL_AT_CHAN_CTRL_ETS,
   VAL_AT_FWD_TX_REQ_ETS,
   VAL_AT_REV_TX_REQ_ETS,
#endif

   VAL_HLP_CTA_UPDATE_MSG,
   VAL_IOP_CHAN_ONOFF_MSG,
   VAL_IOP_CHAN_SWITCH_MSG,
   VAL_IOP_CHAN_QUERY_ON_MSG,
   VAL_IOP_CHAN_QUERY_SW_MSG,

   /* uicc messages */
   VAL_UIM_EXT_INFO_GET_RSP_MSG,
   VAL_UICC_LOGI_CHAN_OPEN_RSP_MSG,
   VAL_UICC_LOGI_CHAN_CLOSE_RSP_MSG,
   VAL_UICC_RESTRIC_ACCESS_RSP_MSG,
   VAL_UICC_GENERIC_ACCESS_RSP_MSG,  /* 6257 */

#ifdef MTK_CBP
   VAL_AT_CCSS_ACK_MSG,
#endif
#ifdef MTK_DEV_ENGINEER_MODE
   VAL_ENG_INFO_RPT_TIMER_EXPIRED_MSG,
#endif
#ifdef MTK_DEV_C2K_IRAT
   VAL_DEEPSLEEP_CALLBACK_EXPIRED_MSG,
   VAL_UIM_UTK_TIMER_EXPIRED_MSG,
   VAL_UIM_NO_SERVICE_EXPIRED_MSG,
#endif
#ifdef MTK_CBP
   VAL_ESWLA_TIMER_EXPIRED_MSG,
#endif
#ifdef MTK_DEV_C2K_IRAT
   VAL_MD1_EFUN_STATE_IND,
   VAL_MD1_EMDSTATUS_FLOW_VERSION_IND,
   VAL_MD1_1X_LOOP_BACK_CALL_REQ,
#endif
#ifdef MTK_PLT_ON_PC_UT
   VAL_SEND_TX_IND_MSG = 6290,
   VAL_UT_GLOBAL_VAL_SET_MSG,
#endif

   /* VAL Test Mode Msgs */
   VAL_DSPM_VER_RSP_MSG          = 6300,
   VAL_DSPV_VER_RSP_MSG,
   VAL_CP_VER_RSP_MSG,
   VAL_START_REFURBISH_TEST,

   VAL_FAKE_RESET_NOTIFY,       /* For pass CT test, SHOULD BE REMOVED */
   VAL_IMS_CONN_SETUP_MSG = 6400,
   VAL_IMS_CONN_RELEASE_MSG,
   VAL_IMS_APP_SETUP_MSG,
   VAL_IMS_APP_RELEASE_MSG,
   VAL_IMS_EHRPD_RESOURCE_SET_MSG,

   APP_IMS_NETWK_CONN_RSP,
   APP_IMS_NETWK_END_RSP,
   APP_IMS_SETUP_RSP,
   APP_IMS_REL_RSP,
   APP_IMS_SIP_REG_RSP,
   APP_IMS_SETUP_IND,
   APP_IMS_REL_IND,
   APP_IMS_NETWK_REL_IND,
   APP_IMS_PPP_CONN_IND,

   VAL_AT_UIM_CAVE_RSP = 6500,
   VAL_AT_UIM_SSDUPD_RSP,
   VAL_AT_UIM_SSDUPDCFM_RSP,
   VAL_AT_UIM_VPM_TIMER_EXP,
   VAL_AT_UIM_MD5_RSP,

   VAL_IOP_ETS_RPC_CMD,
   VAL_RPC_GPS_RX_MSG,
   VAL_RPC_GPS_CALL_TMO_MSG,
   VAL_RPC_GPS_DEFFER_ACTION_MSG,
   VAL_RPC_IRAT_RX_MSG,
   VAL_RPC_IRAT_CALL_TMO_MSG,
   VAL_RPC_IRAT_DEFFER_ACTION_MSG,
   VAL_CSS_IRAT_MSG,
   VAL_MMC_RPC_MSG,

#ifdef TCPIP_ATC
   VAL_TCPIP_RECV_RSP_MSG,    /*used by tcpip module*/
   VAL_TCPIP_RECV_DATA_MSG,
   VAL_TCPIP_C108LOW_MSG,        /*used by tcpip module*/
   VAL_TCPIP_SOCKET_EVT_MSG,
#endif
   VAL_IOP_DATAMGR_INIT_DONE_MSG,

   VAL_AT_TURN_OFF_DTMF_SOUND_MSG,
   VAL_AT_CRING_CALLBACK_MSG,
   VAL_AT_CNMA_RP_ERROR_CALLBACK_MSG,
   VAL_AT_VSER_CALLBACK_MSG,

   VAL_AT_GET_DEBUG_INFO_MSG,
   VAL_AT_NAM_WRITE_ACK_MSG,

   VAL_FAULT_HEADER_READ_MSG,
   VAL_FAULT_RECORD_READ_MSG,
   VAL_FAULT_LOG_ERASE_MSG,

   VAL_PPP_A12_AUTH_NOT_FAIL_MSG,
   VAL_PPP_A12_AUTH_FAIL_MSG,
   VAL_ATC_C109_IND_MSG,

   VAL_AT_SUSPEND_CALLBACK_MSG,

   VAL_GET_MOBILE_ID_RSP_MSG=6600,
   VAL_PSW_GET_IMSI_MSG,
   Valat_1x_MRU_WRITE_ACK_MSG,
   Valat_DO_MRU_WRITE_ACK_MSG,
   VAL_UIM_GET_NAM_DATA_MSG,
   VAL_AT_DIAL_NUM_CHECK_REG_MSG,

   VAL_NST_LIST_SET_MSG = 6700,
   VAL_PSW_NST_LIST_SET_RSP_MSG,

   VAL_SET_LOCK_FEATURE_MSG,
   VAL_UIM_GET_UIMID_EUIMID_MSG,
   VAL_HLP_PPP_RECONNECT_MSG,
   VAL_DATA_ROAMING_CHG_MSG,
   VAL_LIGHT_CTRL_MSG = 7000,
   VAL_DBM_DATA_NAM_MSG,
   VAL_STORAGE_DATA_INIT_MSG,

   VAL_PEN_MSG,
   VAL_SCREEN_CALIBRATION_CNF_MSG,
   VAL_AT_VECIOURC_CALLBACK_MSG,

#if defined(MTK_PLT_ON_PC_UT) && defined(MTK_DEV_C2K_IRAT)
   LTE_C2K_RAT_CHANGE_REQ,
   C2K_LTE_RAT_CHANGE_CNF,
   VAL_IRAT_EVT_TRIGGER_PS_REG_RSP,
#ifdef MTK_DEV_C2K_SRLTE
   VAL_IRAT_EVT_RESEL_CNF,
   VAL_IRAT_EVT_REDIRECT_CNF,
   VAL_IRAT_EVT_RAT_CHANGE_CMP_CNF,
   VAL_IRAT_EVT_RESEL_TO_LTE_IND,
#endif /* MTK_DEV_C2K_SRLTE*/
   VAL_IRAT_EVT_NETWK_STATE_CHANGE_NOTIFY,
   VAL_IRAT_EVT_EHRPD_DEFAULT_BEAER_CNF,
   VAL_IRAT_EVT_EHRPD_BEARER_DISC_CNF,
   VAL_IRAT_EVT_EHRPD_DEFAULT_BEARER_DISC_IND,
   VAL_IRAT_EVT_EHRPD_BEARER_DISC_IND,
   VAL_IRAT_EVT_CDMA_DATA_STATE_IND,
   VAL_IRAT_EVT_PS_RAT_CHANGE_IND,
   VAL_IRAT_EVT_ABORT_C2L_RESEL_IND,
   VAL_IRAT_EVT_PS_STATUS_CHANGE_NOTIFY,
   VAL_IRAT_EVT_EHRPD_DETACH_CNF,
   VAL_IRAT_EVT_RAT_MODE_NOTIFY,
   VAL_IRAT_EVT_ABORT_IND,
#endif /* MTK_DEV_C2K_IRAT */

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
   VAL_RPT_C2K_RSVAS_EVENT_MSG,
   VAL_CSS_IRAT_POWER_CTRL_CNF,
#endif

#ifdef MTK_DEV_C2K_IRAT
   VAL_UIM_BTSAP_CONNECT_MSG,
   VAL_UIM_BTSAP_DISCONNECT_MSG,
   VAL_UIM_BTSAP_POWER_ON_MSG,
   VAL_UIM_BTSAP_POWER_OFF_MSG,
   VAL_UIM_BTSAP_RESET_MSG,
   VAL_UIM_BTSAP_TRANSFER_APDU_MSG,
   VAL_UIM_EMDSTATUS_UPDATE_IND_MSG,
   VAL_UIM_PRE_STORED_CARD_ESNME_IND_MSG,
#endif /* MTK_CBP */
#if defined(MTK_PLT_ON_PC_UT)
   VAL_SMS_MOC_KEY_REQ_MSG,
   VAL_SMS_REMOTE_CTRL_INS_RSP_MSG,
#endif
   VAL_LINK2SDRV_MSG,
#ifdef MTK_CBP_ENCRYPT_VOICE
   VAL_SMS_T101_EXPIRE_MSG,
   VAL_SMS_T201_EXPIRE_MSG,
   VAL_SMS_T301_EXPIRE_MSG,
   VAL_SMS_T401_EXPIRE_MSG,
   VAL_SMS_ENCRY_KEY_REQ_MSG,
   VAL_SMS_ENCRY_CLEAR_CONTEXT_MSG,
   VAL_SMS_ENCRY_REMOTE_CTRL_INS_RSP_MSG,
#endif
   VAL_AT_PENDING_SMS_EXPIRE,
#if defined(MTK_PLT_ON_PC_UT)
   VDM_VAL_SEND_DCN_REQ,
   VDM_VAL_SEND_DCN_START_IND,
   VDM_VAL_SEND_DCN_START_RSP,
   VDM_VAL_SEND_DCN_END_IND,
   VDM_VAL_IMS_REG_STATUS_IND,
#endif

   VAL_DANDCN_HYSTMR_EXPR_MSG,
   /* DAN DCN msg */
   VAL_SMS_DANDCN_READ_NVRAM_RSP,
   VAL_SMS_DANDCN_WRITE_NVRAM_RSP,
#ifdef MTK_CBP
   VAL_UIM_VALIDATE_SPC_MSG,
   VAL_UIM_IMSI_M_PROGRAM_STATE_IND_MSG,
   VAL_UIM_OTA_UPDATE_NAM_DATA_MSG,
#endif
#ifdef MTK_PLT_ON_PC_UT
   VAL_UT_CONFIGURE_REQ_MSG,
#endif

#ifdef MTK_CBP
   VAL_IPC_RATE_REDUCTION_MSG,
#endif
   VAL_NUM_MESSAGES
} ValMsgIdT;


typedef PACKED_PREFIX struct {
	bool	C109On;	/* status of C109, active high */
	uint8 chan;
} PACKED_POSTFIX  AtcValC109IndMsgT;


typedef enum
{
    VAL_PSW_CKECK_Locking_Feature_OK = 0x00,
    VAL_PSW_CKECK_MIN_ERROR,
    VAL_PSW_CKECK_DefaultMIN_OK,
    VAL_PSW_CKECK_SID_ERROR,
    VAL_PSW_CKECK_MCC_ERROR,
    VAL_PSW_CKECK_FOR_RELIANCE_PrefNO_ERROR,
    VAL_PSW_CKECK_MIN_ERROR_OVER_MAX_RETRY_TIMES,
    VAL_PSW_CKECK_Locking_Feature_NUM
}PswCheckForMinLockStateT;

/* Generic ETS Message
 */
typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
  uint32     RegId;
} PACKED_POSTFIX  ValGenericMsgT;

/*-----------------------------------------------------------------
 * HLPHandler
 *----------------------------------------------------------------*/

typedef enum
{
  VAL_HLP_CONNECT_SUCCESS,         /* connection success       */
  VAL_HLP_CONNECT_FAIL_TCP,        /* connection fails on TCP  */
  VAL_HLP_CONNECT_FAIL_PPP,        /* connection fails on PPP  */
  VAL_HLP_CONNECT_FAIL_RLP,        /* connection fails on RLP  */
  VAL_HLP_CONNECT_FAIL_UART,       /* not use                  */
  VAL_HLP_CONNECT_FAIL_TRAFFIC,    /* connection fails on traffic */
  VAL_HLP_CONNECT_FAIL_NOSVC,      /* connection fails on no src  */
  VAL_HLP_DISCONNECT_NORMAL,       /* disconnect from BS       */
  VAL_HLP_DISCONNECT_MS,           /* disconnect from MS       */
  VAL_HLP_DISCONNECT_FADE,         /* disconnect due to fading */
  VAL_HLP_CONNECT_DORMANT,         /* connection dormant       */
  VAL_HLP_CONNECT_RECONNECT,       /* reconnect after dormant  */
  VAL_HLP_CONNECT_FAIL_LOCKED,     /* hspd calls locked out    */
  VAL_HLP_CONNECT_TCH,             /* dial up connect to TCH for 1st time. */
  VAL_HLP_CONNECT_THROTTLED,       /* connection throttled     */
  VAL_HLP_OTHER_DATA_CALL_EXIT,
  VAL_HLP_NUM_CONNECT_STATUS
} HlpConnStatusT;


typedef PACKED_PREFIX struct
{
  HlpConnStatusT status;
  UINT32         LocalIPAddr;
  UINT32         RemoteIPAddr;
  UINT32         PriDNSAddr;
  UINT32         SecDNSAddr;
} PACKED_POSTFIX  ValHlpPppConnStatusMsgT;  /* for Um interface only. */

#ifdef MTK_CBP /*MTK_DEV_C2K_IRAT*/
typedef PACKED_PREFIX struct
{
  uint16         Mtu;
} PACKED_POSTFIX ValHlpMtuUrcMsgT;
#endif /* MTK_DEV_C2K_IRAT */

typedef PACKED_PREFIX struct
{
  HlpConnStatusT status;
} PACKED_POSTFIX  ValHlpPppCloseStatusMsgT;

typedef PACKED_PREFIX struct
{
    int16            socketId;
    uint8            sap;
    ValSocketStatusT status;
} PACKED_POSTFIX  ValHlpSocketCreateStatusMsgT;

typedef PACKED_PREFIX struct
{
    uint8            sap;
    ValSocketStatusT status;
} PACKED_POSTFIX  ValHlpSocketBindStatusMsgT;

typedef PACKED_PREFIX struct
{
    uint8  sap;
    ValSocketStatusT status;
    uint32 srcIP;
    uint32 destIP;
    uint16 srcPort;
    uint16 dstPort;
} PACKED_POSTFIX  ValHlpSocketConnStatusMsgT;

typedef PACKED_PREFIX struct
{
    int16 SockListenfd;
    uint8 SocketNum;
    int16 sockfd[5];
    uint8 sap[5];
} PACKED_POSTFIX ValHlpSocketListenStatusMsgT;

typedef PACKED_PREFIX struct
{
    uint8 sap;
    uint8 opt_type;
    uint16 size;
    ValSocketStatusT status;
} PACKED_POSTFIX ValHlpSocketOptStatusMsgT;

typedef PACKED_PREFIX struct
{
    uint8            sap;
    ValSocketStatusT status;
    int8           how;
} PACKED_POSTFIX  ValHlpSocketShutDownStatusMsgT;

typedef PACKED_PREFIX struct
{
    uint8            sap;
    bool             lingerOnOff;
    uint32           lingerTime;
    ValSocketStatusT status;
} PACKED_POSTFIX  ValHlpSocketLingerStatusMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  ValHlpIpDnsAddrGetMsgT;

typedef PACKED_PREFIX struct
{
    UINT32         LocalIPAddr;
    UINT32         PriDNSAddr;
    UINT32         SecDNSAddr;
} PACKED_POSTFIX  ValHlpIpDnsAddrGetRspMsgT;
/*-----------------------------------------------------------------
 * OTASPHandler
 *----------------------------------------------------------------*/

/*-----------------------------------------------------------------
 * LocHandler
 *----------------------------------------------------------------*/

/* Refer to PswPosAfltMeasurementMsgT in pswapi.h */
 /*-----------------------------------------------------------------
 * PSWAmpsHandler
 *----------------------------------------------------------------*/

/* VAL_AMPS_EXT_PROTO_MSG */
typedef enum
{
    VAL_AMPS_MST_VMAIL,
    VAL_AMPS_MST_CLI,
    VAL_AMPS_MST_SMS
} ValAmpsExtProtoType;

typedef PACKED_PREFIX struct
{
    ValAmpsExtProtoType ExtProtoType;
    bool                SoundAudibleAlertFlag;
    uint16              MsgLen;
    uint8               MsgData[1];
} PACKED_POSTFIX  ValAmpsExtProtoMsgT;


typedef enum
{
   CSFB_TST_CDMA_PARM,
   CSFB_TST_SIB8,
   CSFB_TST_REDIR
} ValCsfbTstBuffType;

typedef PACKED_PREFIX struct
{
   ValCsfbTstBuffType buffID;
   UINT8 size;
   UINT8 buff[255];
} PACKED_POSTFIX  ValCsfbTstSetBufferMsgT;

typedef PACKED_PREFIX struct
{
   BOOL  inclRand;
   UINT32 Rand;
   BOOL  inclMobParms;
} PACKED_POSTFIX  ValCsfbTstParmRspMsgT;

typedef PACKED_PREFIX struct
{
   BOOL   inclRand;
   UINT32 Rand;
   BOOL   inclMobParms;
} PACKED_POSTFIX  ValCsfbTstHOEutraPrepMsgT;

typedef PACKED_PREFIX struct
{
   BOOL sysTimeIncl;
   UINT8 sysTime[5];
} PACKED_POSTFIX  ValCsfbTstMobFromEutraMsgT;

/* VAL_NWK_RPT_PREV_PREVINUSE_MSG */
typedef PACKED_PREFIX struct
{
    uint8 PRev;
    uint8 PRevInUse;
} PACKED_POSTFIX ValNetworkPrevAndPrevInUseMsgT;


/* VAL_DO_RX_TX_POWER_INFO_DATA_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;      /* Response routing information */
   int16    MainRxPowerDbmQ6;
   int16    DivRxPowerDbmQ6;
   uint16   MainRxGainState;
   uint16   DivRxGainState;
   int16    TxPowerDbmQ6;
   uint16   TxGainState;
   int16    TxClosedLoopAdjQ6;
   int16    TxAnt;
#ifdef MTK_DEV_C2K_IRAT
   int16    EcIo;
   uint16   SINR;          /* uint:Linear, convert it to dB = 10*lg(SINR/512) */
#endif
} PACKED_POSTFIX  ValDoRxTxPwrInfoMsgT;

/* VAL_MISC_RX_TX_PWR_INFO_EVT - this typedef is for UI callback only */
typedef PACKED_PREFIX struct
{
   int16    RxPowerDbmQ6;
   int16    DivRxPowerDbmQ6;
   int16    TxPowerDbmQ6;
   uint16   RxGainState;
   uint16   DivRxGainState;
   uint16   TxGainState;
   int16    AvgActiveEcIoQ6;
#ifdef MTK_CBP
   int16    LastAvgActiveEcIoQ6_1;
   int16    LastAvgActiveEcIoQ6_2;
#endif
   uint16   TxAgcPdmRep;
   int16    TxClosedLoopAdjQ6;
   int16    RxGainIntegral;
   int16    RxGainCoarse;
   int16    RxGainFine;
   int16    RxGainPdmQ3;
   int16    TxConstPowerOffsetQ6;
#ifdef MTK_CBP
   int16    TxAnt;
#endif
} PACKED_POSTFIX ValUiRxTxPowerDataT;

#ifdef MTK_CBP
/*Tx power level of <5, <10, <15, <20, <25*/
#define VAL_NUM_TX_POWER_LEVELS             5
#define VAL_RCV_DO_RX_TX_TIME_INFO_RSP      0X01
#define VAL_RCV_L1D_RX_TX_TIME_INFO_RSP     0X02

/* VAL_RMC_DO_RX_TX_TIME_INFO_DATA_RSP_MSG */
/* VAL_DO_RX_TX_TIME_INFO_DATA_RSP_MSG */
typedef PACKED_PREFIX struct
{
    /*period(in ms) for which Tx is active*/
    uint32  do_tx_time_ms[VAL_NUM_TX_POWER_LEVELS];
    /*period (in ms) for which Rx is active*/
    uint32  do_rx_time_ms;
} PACKED_POSTFIX  ValDoRxTxtimeInfoRspMsgT;

/* VAL_L1D_RX_TX_TIME_INFO_DATA_RSP_MSG */
typedef PACKED_PREFIX struct
{
    /*period(in ms) for which Tx is activ*/
    uint32  L1D_tx_time_ms[VAL_NUM_TX_POWER_LEVELS];
    uint32  L1D_rx_time_ms;
} PACKED_POSTFIX  ValL1dRxTxtimeInfoRspMsgT;

#endif


#ifdef MTK_CBP
typedef enum
{
    VAL_HWD_START_TX_POWER = 0,
    VAL_HWD_STOP_TX_POWER,
    VAL_HWD_DETECT_RX_POWER,
    VAL_HWD_DETECT_TX_POWER,
    VAL_HWD_START_TX_POWER_MODU_SIGNAL,
    VAL_HWD_STOP_TX_POWER_MODU_SIGNAL,
    VAL_HWD_DETECT_RX_POWER_DBM_UNIT,
    VAL_HWD_POWER_REDUCTION_LEVEL,
    VAL_HWD_POWER_REDUCTION_LEVEL_BY_BAND
}ValHwdTstReqModeT;

typedef enum
{
    VAL_HWD_TX_TONE_SIGNAL = 0,
    VAL_HWD_TX_MODU_SIGNAL,
    VAL_HWD_TX_INVALID_SIGNAL_TYPE = 255
}ValHwdTstTxSignalTypeT;

typedef PACKED_PREFIX struct
{
    ValHwdTstReqModeT  mode;
    int16           TxPower;    /* Need to convert from Q5 to Q3 */
    int16           MainRxPower;/* Need to convert from Q7 to Q3 */
    int16           DivRxPower; /* Need to convert from Q7 to Q3 */
}ValHwdRxTxPowerDetectorMsgT;
typedef enum
{
    VAL_L1D_TX_PWR_REDUCTION = 0,
    VAL_L1D_TX_PWR_REDUCTION_BY_BAND
}ValTxPwrReducModeT;
#endif


 /*-----------------------------------------------------------------
    * MULTIMEDIA APPLICATION Handler
    *----------------------------------------------------------------*/

/* Status & Error codes used for VAL Apps callback functions (Music, JPEG) */
typedef enum
{
   VAL_APP_OK = 0,
   VAL_APP_FILE_FORMAT_INVALID,
   VAL_APP_FILE_IO_ERROR,
   VAL_APP_FAILED_ERROR,
   VAL_APP_WRONG_PLAY_FORMAT,
   VAL_APP_CONFLICT,
   VAL_APP_MALLOC_ERROR,
   VAL_APP_DATA_SIZE_INVALID,
   VAL_APP_TONE_ID_INVALID,

   VAL_APP_IMAGE_TYPE_INVALID,
   VAL_APP_IMAGE_CONTEXT_NOT_INIT,
   VAL_APP_IMAGE_WIDTH_INVALID,
   VAL_APP_IMAGE_ACTUAL_SIZE_UNKNOWN,

   VAL_APP_VMEMO_WRONG_STATE_ERR,
   VAL_APP_VMEMO_INVALID_REC_TYPE,
   VAL_APP_VMEMO_RECORD_DEVICE_FULL,
   VAL_APP_VMEMO_INVALID_NUM_PKTS,
   VAL_APP_VMEMO_INVALID_SRVC_OPT,
   VAL_APP_VMEMO_SRVC_OPT_MISMATCH,
   VAL_APP_VMEMO_WRONG_REC_WHILE_ON_TRAFFIC,
   VAL_APP_VMEMO_INTERNAL_ERR,
   VAL_APP_VMEMO_INVAL_TYPE_FOR_FILE_REC,
   VAL_APP_VMEMO_INVAL_PLAYBACK_PARMS,
   VAL_APP_VMEMO_INVAL_PACKET_RATE,

   VAL_APP_A2DP_STREAM_OPERATION_ERR,
   VAL_APP_MUSIC_NOT_SUPPORTED_ERR,
   VAL_APP_CMD_NOT_SUPPORTED_ERR,
   VAP_APP_SAMPLING_RATE_INVALID,
   VAL_APP_NUM_CHANS_INVALID,
   VAL_APP_WRONG_RECORD_FORMAT
} ValAppStatusT;

/* VAL audio sampling rates, used in Music and Voice Memo API's */
typedef enum
{
    VAL_SAMP_RATE_8000,
    VAL_SAMP_RATE_11025,
    VAL_SAMP_RATE_12000,
    VAL_SAMP_RATE_16000,
    VAL_SAMP_RATE_22050,
    VAL_SAMP_RATE_24000,
    VAL_SAMP_RATE_32000,
    VAL_SAMP_RATE_44100,
    VAL_SAMP_RATE_48000,
    VAL_NUM_SAMPLING_RATES
} ValSamplingRatesT;

/*-----------------------------------------------------------------
* VSTRM Handler
*----------------------------------------------------------------*/
/* Used by Vstrm, but could be used for generic passing of CpBuff */
typedef  struct
{
   CpBufferT*     pCpBuf;
   uint16         offset;
   uint16         Len;
} ValCpBuffMsgT;


/*-----------------------------------------------------------------
* MISC Handler
*----------------------------------------------------------------*/

/* VAL_L1DTST_GETPHONESTATUS; rspmsg data structure defined by L1D */
typedef ExeRspMsgT ValL1DTstGetPhoneStatusMsgT;

/* VAL_HRPD_RSSI_RPT_MSG */
typedef PACKED_PREFIX struct
{
   int16    Rssi;
} PACKED_POSTFIX  ValHrpdRssiRptMsgT;

/* typedef for expressing time in user format */
typedef struct
{
   uint8  Secs, Mins, Hours;     /* 00:00:00 to 23:59:59 */
   uint8  Day, Month;            /* 1...31, 1...12 */
   uint8  DoW;                   /* 0 (Sun)...6 (Sat) */
   uint16 Year;                  /* 1980...2043 */
   uint16 MilliSecs;
} ValCalendarTimeT;

/*-----------------------------------------------------------------
 * ETS Handler
 *----------------------------------------------------------------*/

/* VAL_SEND_DATA_BURST_MSG */
/* ??? how about going to a header + data dual structure */
#define VAL_CP_MAX_DATA_BURST_CHAR 249 /* 255 - Data Burst Header */
typedef PACKED_PREFIX struct
{
   uint8 MsgNumber;
   uint8 BurstType;
   uint8 NumMsgs;
   uint8 NumFields;
   uint8 Data[249];  /* ??? */
   bool  Encoded;
   bool  DigitMode;
   uint8 NumDigits;
   uint8 Digits[30]; /* ??? */
} PACKED_POSTFIX  ValSendDataBurstMsgT;

typedef PACKED_PREFIX struct
{
  bool  Mode; /* TRUE: disabled */
} PACKED_POSTFIX  ValKPadDisabledMsgT;

/* VAL_EMERGENCY_MODE_TEST_MSG */
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;
  bool        value;
} PACKED_POSTFIX  ValEmergencyModeTestingMsgT;

typedef PACKED_PREFIX struct
{
  bool  mode;
} PACKED_POSTFIX  ValEmergencyModeTestingResponseMsgT;

typedef enum
{
  VAL_DEV_NONE        = 0x0000,
  VAL_DEV_UIM         = 0x1000,
  VAL_DEV_RAM         = 0x2000,
  VAL_DEV_FLASH       = 0x3000,
  VAL_DEV_UIM_FLASH   = 0x4000,
  VAL_DEV_FLASH_UIM = 0x5000
#ifdef MTK_CBP
  ,VAL_DEV_UIM_FDN = 0x6000,
  VAL_DEV_UIM_ADN_FDN = 0x7000
#endif
} ValDeviceT;

 /* VAL Event typedefs */
typedef int16 RegIdT;
typedef void  (*ValEventFunc)  ( RegIdT RegId,
                                 uint32 MsgId,
                                 void*  MsgBufferP );

 /* Structure for registering events */
typedef struct
{
  bool         IsUse;    /* if this entry is in use */
  ValEventFunc CallBack; /* function to call back */
} ValRegTableT;

/* Event handler */
typedef struct
{
  ValRegTableT*  RegTableP; /* Register table pointer */
  ExeSemaphoreT* SemaphoreP;
  int16          MaxRegIds; /* Maximum RegId count Register table supported */
} ValEventHandlerT;

/* index used to access the CallTxtTable[] */
typedef enum {
   VAL_CT_DEFAULT,
   VAL_CT_AMPS_CALL,
   VAL_CT_DATA_CALL,
   VAL_CT_FAX_CALL,
   VAL_CT_SSO1,
   VAL_CT_SSO2,
   VAL_CT_SSO3,
   VAL_CT_SSO6,
   VAL_CT_SSO9,
   VAL_CT_SSO14,
   VAL_CT_SSO17,
   VAL_CT_SSO32768,
   VAL_CT_SSO18,
   VAL_CT_SSO19,
   VAL_CT_SSO32,
   VAL_CT_SSO33,
   VAL_CT_SSO35,
   VAL_CT_SSO36,
   VAL_CT_SSO54,
   VAL_CT_SSO55,
   VAL_CT_SSO68,
   VAL_CT_SSO32798,
   VAL_CT_SSO32799,
   VAL_CT_SSO32858,
   VAL_CT_SSO32859,
   VAL_CT_MAX
} ValCallTypeT;

/* powerdown sequence signals */
typedef enum
{
  VAL_PWRDWN_START,
  VAL_PWRDWN_PSDOWN,
  VAL_PWRDWN_DBMFLUSH,
  VAL_PWRDWN_AIWFLUSH
} ValPowerDownIdT;

typedef enum
{
  VAL_MISC_PWRDWN_FLUSH_EVT,
  VAL_MISC_BATTERY_READING_EVT,
  VAL_MISC_TEMP_READING_EVT,
  VAL_MISC_RX_TX_PWR_INFO_EVT,
  VAL_MISC_REFURBISH_AGING_TX_OFF_IND,
  VAL_MISC_REFURBISH_AGING_TX_ON_IND,
  VAL_MISC_GET_SMS_COUNT_EVT,
  VAL_MISC_GET_SMS_EVT
} ValMiscEventIdT;

#ifdef MTK_PLT_ON_PC_UT
typedef enum
{
  VAL_UT_GV_CSS_OP_MODE,
  VAL_UT_GV_CSS_NV_READ_COMPLETE,
} ValUtGloblaValTypeE;
#endif

enum _ValAtService
{
  VAL_ATS_AsyncData,
  VAL_ATS_Fax,
  VAL_ATS_Rejected,
  VAL_ATS_PacketRelayRm,
  VAL_ATS_SingleStackQNC,
  VAL_ATS_UpBrowser,
  VAL_ATS_AsyncUpBrowser,
  VAL_ATS_TcpCktBrowser,
  VAL_ATS_TcpPktBrowser,
  VAL_ATS_PacketNtwkRm,
  VAL_ATS_PPPOnly,
  VAL_NUM_ATSs
};

typedef UINT8 ValAtService;

typedef struct
{
  UINT8* buf;
  UINT16 offset;
  UINT16 len;
} ValAtBuf;

/* IWF-intermediate result codes need not appear in this list. */
enum
{
  /* IWF-final and MT2 generated codes */
  VAL_ARC_OK,
  VAL_ARC_CON,
  VAL_ARC_ERR,
  VAL_ARC_CPKT,
  VAL_ARC_CBWSR,

  /* IWF-final generated codes */
  VAL_ARC_BSY,
  VAL_ARC_NAN,
  VAL_ARC_NCR,
  VAL_ARC_NDT,
  VAL_ARC_CEIF,

  /* MT2 generated codes */
  VAL_ARC_NUL,
  VAL_ARC_CEBR,
  VAL_ARC_CELF,
  VAL_ARC_CENDS,
  VAL_ARC_CENFS,
  VAL_ARC_CENS,
  VAL_ARC_CEPF,
  VAL_ARC_CEPG,
  VAL_ARC_CERL,
  VAL_ARC_CERT,
  VAL_ARC_CRGA,
  VAL_ARC_CRGF,

  VAL_NUM_RCs
};
typedef UINT8 ValAtResultCode;

typedef PACKED_PREFIX struct
{
    uint8   *dataPtr;
    uint16   offset;
    uint16   dataLen;
} PACKED_POSTFIX  FwdDataInfoT;

/* cmd id for ValUserInfoControl() */
typedef enum
{
    VAL_USERINFO_INIT_CMD    =  0x01,
    VAL_USERINFO_RESTART_CMD,
    VAL_USERINFO_STOP_CMD
} ValUserInfoCmdT;

typedef struct
{
    bool  Mode;
} ValSetPrivacyModeMsgT;

typedef PACKED_PREFIX struct
{
    uint8 NetPre;
} PACKED_POSTFIX  ValReportNetPre;

typedef PACKED_PREFIX struct
{
   uint8             BreakLen;      /* in 10 msec units                 */
} PACKED_POSTFIX  ValEia617BreakReqMsgT;

typedef PACKED_PREFIX struct
{
   bool              C108On;        /* the state of C108, DTE ready.    */
   UINT8           chan;
   bool              CableUnplugged ;
} PACKED_POSTFIX  ValC108IndMsgT;

typedef PACKED_PREFIX struct
{
   uint32            Rate;          /* Bit rate on both Tx and Rx side. */
} PACKED_POSTFIX  ValRateIndMsgT;

typedef PACKED_PREFIX struct
{
   CpBufferT*        pCpBuf;      /* points to the first byte of data */
   uint16            offset;       /* data size in bytes.              */
   uint16            Len;
   uint8             RlpFlow;
   UINT8             chan;
} PACKED_POSTFIX  ValRevTxReqMsgT;

#ifdef MTK_PLT_ON_PC_UT
typedef PACKED_PREFIX struct
{
   uint8                len;
   ValUtGloblaValTypeE  type[16];      /* points to the first byte of data */
   uint32               val[16];
} PACKED_POSTFIX  ValUtGlobalValSetMsgT;

typedef PACKED_PREFIX struct
{
   CpBufferT*        pCpBuf;      /* points to the first byte of data */
   uint16            offset;       /* data size in bytes.              */
   uint16            Len;
   uint8             RlpFlow;
   UINT8             chan;
   char              cmdBuf[1024];
} PACKED_POSTFIX  ValRevTxReqMsgUtT;

typedef PACKED_PREFIX struct
{
   uint16            Len;
   UINT8             chan;
   char              cmdBuf[1024];
} PACKED_POSTFIX ValSendTxIndMsgT;

typedef PACKED_PREFIX struct
{
    uint8   value;
} PACKED_POSTFIX ValConfigureReqMsgT;

#endif

#ifdef MTK_DEV_ETS_ENHANCEMENT
typedef PACKED_PREFIX struct
{
   bool              on;
} PACKED_POSTFIX  ValATChanCtrlMsgT;

typedef PACKED_PREFIX struct
{
   char             cmdBuf[1];
} PACKED_POSTFIX  ValATFwdTxReqMsgT;

typedef PACKED_PREFIX struct
{
   char             cmdBuf[1];
} PACKED_POSTFIX  ValATRevTxReqMsgT;
#endif

typedef PACKED_PREFIX struct
{
   uint8*            DataBufP;      /* points to the first byte of data */
   uint16            DataLen;       /* data size in bytes.              */
} PACKED_POSTFIX  ValFwdTxReqMsgT;

typedef PACKED_PREFIX struct
{
   UINT8            chan;
} PACKED_POSTFIX  ValFwdTxRspMsgT;

/* VAL_RLP_REV_DATA_RSP_MSG */
typedef PACKED_PREFIX struct
{
  uint8 srId;
} PACKED_POSTFIX  ValRlpRevDataRspMsgT;

/* IOP_RLP_FWD_DATA_IND_MSG */
typedef PACKED_PREFIX struct {
 uint8   *dataPtr;
 uint16   offset;
 uint16   dataLen;
} PACKED_POSTFIX  ValRlpFwdDataFrameT;

#define NUM_VAL_RLP_FRAMES_PER_INDICATION     20
typedef PACKED_PREFIX struct {
 uint16   numRlpFrames;
 ValRlpFwdDataFrameT rlpFrames[NUM_VAL_RLP_FRAMES_PER_INDICATION];
} PACKED_POSTFIX  ValRlpFwdDataIndMsgT;


typedef PACKED_PREFIX struct
{
   ValAtResultCode ResultCode;
} PACKED_POSTFIX  ValDsIssueResultMsgT;

typedef PACKED_PREFIX struct
{
   ValAtService Svc;
} PACKED_POSTFIX  ValDsConnIndMsgT;

typedef PACKED_PREFIX struct
{
   uint8             QNCStr[VAL_MAX_BRSWR_DGT_SIZE]; /* null-terminated */
} PACKED_POSTFIX  ValSetQNCDialStrMsgT;

#ifdef SYS_OPTION_VOICE_RECOGNITION
typedef enum
{
  VAL_VREC_TRAINING_START_EVT,
  VAL_VREC_TRAINING_COMPLETE_EVT,
  VAL_VREC_SPEECH_PLAYBACK_EVT,
  VAL_VREC_START_RECOGNITION_EVT,
  VAL_VREC_NOT_RECOGNIZED_EVT,
  VAL_VREC_RECOGNIZED_EVT,
  VAL_VERC_CANCEL_EVT,
  VAL_VREC_CAPTURE_CONFIRM_EVT,
  VAL_VREC_CAPTURE_PLAYBACK_EVT,
  VAL_VREC_CMD_DONE_EVT,
  VAL_VREC_TRAIN_CANCEL_EVT,
  VAL_VREC_CANCEL_EVT,
  VAL_VREC_TEMPLATE_COMPLETE_EVT
} ValVrecEventT;
#endif

/* Please, note!!!! This type HAS to match HWD definitions */
typedef enum
{
   VAL_RF_PCS_BAND, /* = HWD_RF_PLL_BAND_PCS_1900,*/
   VAL_RF_AMPS_BAND, /* = HWD_RF_PLL_BAND_AMPS, */
   VAL_RF_CELL_BAND, /* = HWD_RF_PLL_BAND_CDMA_CELLULAR, */
   VAL_RF_T53_BAND, /* = HWD_RF_PLL_BAND_CDMA_T53, */
   VAL_RF_KPCS_BAND, /* = HWD_RF_PLL_BAND_KOREAN_PCS, */
   VAL_RF_UNDEF_BAND /* = HWD_RF_PLL_BAND_UNDEFINED */
} ValRfPllBandT;

/* Define VAL Display Device Info msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  ValDispDeviceInfoMsgT;

/* Define VAL Display Device response Info msg */
#define VAL_DISP_MAX_DEVICE_INFO_LEN  20
typedef PACKED_PREFIX struct
{
   char        Vendor [VAL_DISP_MAX_DEVICE_INFO_LEN];
   char        ModelId [VAL_DISP_MAX_DEVICE_INFO_LEN];
   uint16      BitsPerPixel;
   uint16      WidthInPixels;
   uint16      HeightInPixels;
} PACKED_POSTFIX  ValDispDeviceInfoRspMsgT;

/*The following definitions are used for the datacard application software support*/

#define VAL_MAX_ROAMING_NETWORK_LEN 34
#define VAL_MAX_MDN_LEN 16
#define VAL_MAX_ACTIVATION_CODE_LEN 8
#define VAL_MAX_MNHA_LEN 16
#define VAL_MAX_MNAAA_LEN 16
#define VAL_MAX_PRL_SIZE      8192
#define VAL_MAX_NTWK_ERR_STR_LEN 240

typedef enum
{
  VAL_WMC_SERVICE_NONE = 0,
  VAL_WMC_SERVICE_AMPS,
  VAL_WMC_SERVICE_IS95A,
  VAL_WMC_SERVICE_IS95B,
  VAL_WMC_SERVICE_GSM,
  VAL_WMC_SERVICE_GPRS,
  VAL_WMC_SERVICE_1XRTT,
  VAL_WMC_SERVICE_1XEVDO,
  VAL_WMC_SERVICE_UMTS,
  VAL_WMC_SERVICE_1XEVDOrA
}ValWmcServiceTypeT;

typedef enum
{
  VAL_WMC_NETWORK_HOME = 0,
  VAL_WMC_NETWORK_EXTENDED,
  VAL_WMC_NETWORK_ROAM
}ValWmcNetworkTypeT;

typedef enum
{
  VAL_WMC_RC_SUCCESS = 0,
  VAL_WMC_RC_BAD_PARAMETER,
  VAL_WMC_RC_NOT_STARTED,
  VAL_WMC_RC_BUFFER_TOO_SMALL,
  VAL_WMC_RC_DEVICE_NOT_AVAILABLE,
  VAL_WMC_RC_ACTIVATE_FAILED,
  VAL_WMC_RC_ALREADY_ACTIVATED,
  VAL_WMC_RC_DEVICE_CONNECTED,
  VAL_WMC_RC_SERVICE_NOT_AVAILABLE,
  VAL_WMC_RC_ACTIVATION_INITIATED

}ValWmcResultCodeT;

typedef enum {
  VAL_WMC_STATE_UNKNOWN,
  VAL_WMC_STATE_IDLE,
  VAL_WMC_STATE_CONNECTING,
  VAL_WMC_STATE_AUTH,
  VAL_WMC_STATE_CONNECTED,
  VAL_WMC_STATE_DORMANT,
  VAL_WMC_STATE_UPDATING_NAM,
  VAL_WMC_STATE_UPDATING_PRL,
  VAL_WMC_STATE_DISCONNECTING,
  VAL_WMC_STATE_ERROR,
  VAL_WMC_STATE_NUM
}ValWmcConnStatusT;

typedef enum {
  WMC_STATE_OTASP_NONE,
  WMC_OTASP_STATE_IN_PROGRESS,
  WMC_OTASP_STATE_SPL_UNLOCKED,
  WMC_OTASP_STATE_NAM_DOWNLOADED,
  WMC_OTASP_STATE_MDN_DOWNLOADED,
  WMC_OTASP_STATE_IMSI_DOWNLOADED,
  WMC_OTASP_STATE_PRL_DOWNLOADED,
  WMC_OTASP_STATE_COMMIT_SUCCESS,
  WMC_OTASP_STATE_PROG_SUCCESS,
  WMC_OTASP_STATE_PROG_FAILED
}ValWmcOtaspStateT;

typedef PACKED_PREFIX struct
{
  uint16 Year;
  uint16 Month;
  uint16 Day;
  uint16 Hour;
  uint16 Minute;
  uint16 Second;
  uint16 MillionSeconds;
  int32 MinutesFromUTC;
} PACKED_POSTFIX ValWmcDateTimeT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  ValDatacardGetNetworkInfoMsgT;

/* Get Network Information response message */
typedef PACKED_PREFIX struct
{
  int32 Rssi;
  ValWmcServiceTypeT CurrentService;
  ValWmcNetworkTypeT NetworkType;
  ValWmcDateTimeT NetworkTime;
  int32 RssiDbm;
  int32 RssiEvdoDbm;
  uint32 PrlRoamInd;
  char RoamingNetwork[VAL_MAX_ROAMING_NETWORK_LEN+1];
} PACKED_POSTFIX  ValDatacardGetNetworkInfoRspMsgT;

 typedef enum
 {
   PHONE_ESN_UIMID,
   PHONE_ESN_EUIMID,
   PHONE_MEID_UIMID,
   PHONE_MEID_EUIMID
 } ValPhoneIdTypeT;

 typedef struct
{
    ValPhoneIdTypeT  PhoneIdType;
    uint32 MeEsn;
    uint32 UimId;
    uint8   MeMeid[NAM_MEID_SIZE];
    uint8   EUimId[NAM_MEID_SIZE];
    uint32 MePesn;
    uint32 UimPesn;
} ValDbmMeidAndUimIdT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
  bool Automatic;
  char MDN[VAL_MAX_MDN_LEN+1];
  uint64 MIN;
  char ActivationCode[VAL_MAX_ACTIVATION_CODE_LEN+1];
  uint32 HomeSID;
  bool SetMNHA;
  uint32 MNHASize;
  uint8 MNHA[VAL_MAX_MNHA_LEN+1];
  bool SetMNAAA;
  uint32 MNAAASize;
  uint8 MNAAA[VAL_MAX_MNHA_LEN+1];
} PACKED_POSTFIX ValDataCardOTAActivateMsgT;

/* OTA-Activate response message */
typedef PACKED_PREFIX struct
{
  ValWmcResultCodeT result;
} PACKED_POSTFIX  ValDatacardOTAActivateRspMsgT;

typedef enum
{
  VAL_WMC_MIP_OFF = 0,
  VAL_WMC_MIP_PREFERRED,
  VAL_WMC_MIP_ONLY
} ValWmcMipTypeT;

typedef enum
{
  VAL_WMC_PREF_MODE_AUTO = 0,
  VAL_WMC_PREF_CDMA_ONLY,
  VAL_WMC_PREF_EVDO_ONLY
} ValWmcPrefModeT;

typedef PACKED_PREFIX struct
{
  ValWmcMipTypeT Mip;
  ValWmcPrefModeT PrefMode;
  uint16 Accolc;
} PACKED_POSTFIX  ValWmcMiscParameterT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
  ValWmcMiscParameterT MsgData;
} PACKED_POSTFIX  ValDataCardSetDevParamMsgT;

/* Set device paramters response message */
typedef PACKED_PREFIX struct
{
  ValWmcResultCodeT result;
} PACKED_POSTFIX  ValDataCardSetDevParamRspMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  ValDataCardGetDevParamMsgT;

typedef PACKED_PREFIX struct
{
  ValWmcMiscParameterT MsgData;
} PACKED_POSTFIX  ValDataCardGetDevParamRspMsgT;

typedef enum
{
  /*1xRTT Error Codes*/
  VAL_WMC_NETWORK_ERROR_1X_START = 0,
  VAL_WMC_NETWORK_ERROR_1X_CO_NO_SERVICE = VAL_WMC_NETWORK_ERROR_1X_START,
  VAL_WMC_NETWORK_ERROR_1X_CO_ACCESS_FAILURE,
  VAL_WMC_NETWORK_ERROR_1X_CO_CANNOT_ORIGINATE,
  VAL_WMC_NETWORK_ERROR_1X_CO_REDIRECTION,
  VAL_WMC_NETWORK_ERROR_1X_CO_HANDOFF,
  VAL_WMC_NETWORK_ERROR_1X_CO_IN_PROGRESS,
  VAL_WMC_NETWORK_ERROR_1X_CO_PRIORITY_INBOUND,
  VAL_WMC_NETWORK_ERROR_1X_CO_LOCKED,
  VAL_WMC_NETWORK_ERROR_1X_CO_INCOMPATIBLE_SERVICES,
  VAL_WMC_NETWORK_ERROR_1X_CO_CONCURRENT_NOT_SUPPORTED,
  VAL_WMC_NETWORK_ERROR_1X_CO_NO_RESPONSE,
  VAL_WMC_NETWORK_ERROR_1X_CO_REJECT,
  VAL_WMC_NETWORK_ERROR_1X_CO_SO_NOT_SUPPORTED,
  VAL_WMC_NETWORK_ERROR_1X_CO_CHANNEL_OPEN,
  VAL_WMC_NETWORK_ERROR_1X_CO_ALERT_STOP,
  VAL_WMC_NETWORK_ERROR_1X_CO_MAX_ACCESS,
  VAL_WMC_NETWORK_ERROR_1X_CO_ACTIVATION_NOT_AVAILABLE,
  VAL_WMC_NETWORK_ERROR_1X_CO_INTERCEPT,
  VAL_WMC_NETWORK_ERROR_1X_CO_REORDER,
  VAL_WMC_NETWORK_ERROR_1X_CO_OTHER,
  VAL_WMC_NETWORK_ERROR_1X_RELEASE_FADE,
  VAL_WMC_NETWORK_ERROR_1X_RELEASE_NO_REASON,
  VAL_WMC_NETWORK_ERROR_1X_RELEASE_SO_NOT_SUPPORTED,
  VAL_WMC_NETWORK_ERROR_1X_PROTOCOL_FAILURE,
  VAL_WMC_NETWORK_ERROR_1X_REDIRECT_TO_EVDO,
  VAL_WMC_NETWORK_ERROR_1X_FADE,
  VAL_WMC_NETWORK_ERROR_1X_USER_DISCONNECTED,
  VAL_WMC_NETWORK_ERROR_1X_OTASP_ENDED,
  VAL_WMC_NETWORK_ERROR_1X_ENDED_FOR_VOICE,
  VAL_WMC_NETWORK_ERROR_1X_E911_CALL_ENDED,
  VAL_WMC_NETWORK_ERROR_1X_E911_EMERGENCY_CALL,
  VAL_WMC_NETWORK_ERROR_1X_E911_GPS_FIX,
  VAL_WMC_NETWORK_ERROR_1X_END,

  /*GSM/WCDMA Error Codes*/
  VAL_WMC_NETWORK_ERROR_WCDMA_START = 100,
  VAL_WMC_NETWORK_ERROR_WCDMA_PROTOCOL_FAILURE = VAL_WMC_NETWORK_ERROR_WCDMA_START,
  VAL_WMC_NETWORK_ERROR_WCDMA_ORIGINATION_FAILURE,
  VAL_WMC_NETWORK_ERROR_WCDMA_INCOMMING_REJECTED,
  VAL_WMC_NETWORK_ERROR_WCDMA_NETWORK_DISCONNECTED,
  VAL_WMC_NETWORK_ERROR_WCDMA_NO_SERVICE,
  VAL_WMC_NETWORK_ERROR_WCDMA_USER_DISCONNECTED,
  VAL_WMC_NETWORK_ERROR_WCDMA_END,

  /*1xEV-DO Error Codes*/
  VAL_WMC_NETWORK_ERROR_EVDO_START = 150,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_NO_SERVICE = VAL_WMC_NETWORK_ERROR_EVDO_START,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_ACCESS_FAILURE,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_REDIRECTION,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_NOT_PREFERRED,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_MODE_HANDOFF,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_IN_PROGRESS,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_SETUP_TIMEOUT,
  VAL_WMC_NETWORK_ERROR_EVDO_CO_SESSION_NOT_OPEN,
  VAL_WMC_NETWORK_ERROR_EVDO_RELEASE_NO_REASON,
  VAL_WMC_NETWORK_ERROR_EVDO_PROTOCOL_FAILURE,
  VAL_WMC_NETWORK_ERROR_EVDO_DENY_NO_REASON,
  VAL_WMC_NETWORK_ERROR_EVDO_DENY_NETWORK_BUSY,
  VAL_WMC_NETWORK_ERROR_EVDO_DENY_AUTHENTICATION,
  VAL_WMC_NETWORK_ERROR_EVDO_REDIRECT_TO_1X,
  VAL_WMC_NETWORK_ERROR_EVDO_FADE,
  VAL_WMC_NETWORK_ERROR_EVDO_USER_DISCONNECTED,
  VAL_WMC_NETWORK_ERROR_EVDO_GPS_FIX,
  VAL_WMC_NETWORK_ERROR_EVDO_END
} ValWmcNtwkErrCodeT;

typedef  PACKED_PREFIX struct
{
   ValWmcConnStatusT connStatus;
   ValWmcNtwkErrCodeT DisconnectError;
   ConnStateT    ConnState;
   ValPswDataTypeT DataType;
} PACKED_POSTFIX  ValHlpConnStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint8 SectorId[16];
   uint32 Latitude;
   uint32 Longitude;
   uint8  SubnetMask;
} PACKED_POSTFIX ValClcCellInfoMsgT;

typedef PACKED_PREFIX struct
{
  uint32 uTotalSessionMS;
  uint32 uActiveSessionMS;
  uint32 uTotalTxBytes;
  uint32 uTotalRxBytes;
  uint32 uPreTotalTxBytes;
  uint32 uPreTotalRxBytes;
} PACKED_POSTFIX  ValStatPeekRspMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  ValDatacardGetConnInfoMsgT;

typedef PACKED_PREFIX struct
{
  ValWmcConnStatusT connStatus;
  ValWmcNtwkErrCodeT DisconnectError;
  ValWmcOtaspStateT otaspState;

  uint32 uTotalSessionMS;
  uint32 uActiveSessionMS;
  uint32 uTotalTxBytes;
  uint32 uTotalRxBytes;
} PACKED_POSTFIX  ValConnInfoRspMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  RspInfo;         /* Response routing information */
  ValWmcNtwkErrCodeT ErrCode;
} PACKED_POSTFIX  ValDataCardGetNtwkErrStrMsgT;

typedef PACKED_PREFIX struct
{
  uint8 ErrStr[VAL_MAX_NTWK_ERR_STR_LEN];
} PACKED_POSTFIX  ValDataCardGetNtwkErrStrRspMsgT;

typedef enum
{
  VAL_DO_POWERDOWN,
  VAL_DO_POWERUP
} ValPowerupStatusT;

typedef PACKED_PREFIX struct
{
   ValPowerupStatusT powerup;
} PACKED_POSTFIX  ValPowerupStatusMsgT;

typedef PACKED_PREFIX struct
{
    int8 RecId;
} PACKED_POSTFIX  ValDnsQueryMsgT;

typedef PACKED_PREFIX struct
{
   uint8 DNSEvent;
   int16 DNSParam;
} PACKED_POSTFIX  ValDnsNetMsgT;

typedef enum
{
  FACTORY_NOT_PROVISIONED,
  FACTORY_ALREADY_PROVISIONED
} DataInitMode;

typedef PACKED_PREFIX struct
{
   DataInitMode Mode;
} PACKED_POSTFIX  ValAllDataInitMsgT;

typedef enum
{
   /* AT_CARD_NOT_READY = 0,*/
   /* UIM_STATE_VALID=1,*/
   AT_UIM_CARD = 1,
   AT_SIM_CARD = 2,
   AT_UIM_SIM_CARD = 3,
   AT_UNKNOWN_CARD = 4,
   AT_CT_UIM_CARD = 5,
   AT_CT_UIM_SIM_CARD = 6,
   AT_NEED_PIN_CARD = 7,  /* This card need to input PIN to determin the type */
  #ifndef MTK_CBP
   AT_CT_UICC_CARD = 8,
   AT_NOT_CT_UICC_CARD = 9,

  #else
   AT_CT_UICC_CARD = 8,     /* This enum is no more used, it's split into AT_CT_CSIM_CARD, AT_CT_CSIM_USIM_CARD, AT_CT_CSIM_USIM_ISIM_CARD */
   AT_NOT_CT_UICC_CARD = 9, /* This enum is no more used, it's split into AT_CSIM_CARD, AT_CSIM_USIM_CARD, AT_CSIM_USIM_ISIM_CARD, AT_USIM_CARD, AT_USIM_ISIM_CARD */
   AT_CSIM_CARD = 10,
   AT_CSIM_USIM_CARD = 11,
   AT_CSIM_USIM_ISIM_CARD = 12,
   AT_USIM_CARD =13,
   AT_USIM_ISIM_CARD =14,
   AT_CT_CSIM_CARD = 15,
   AT_CT_CSIM_USIM_CARD = 16,
   AT_CT_CSIM_USIM_ISIM_CARD = 17,
  #endif
#ifdef MTK_DEV_C2K_IRAT
   AT_CARD_LOCKED_CARD = 18,
   AT_CARD_IVSR_CARD = 19,
#endif
   AT_ROMSIM=240,
#ifdef __CARRIER_RESTRICTION__
   AT_RESTRICTED_CARD = 20,
   AT_CARD_REBOOT = 21,
#endif
 /*   NO_UIM_CARD=255*/
    AT_CARD_NOT_READY = 255
}ATUimStateT;

typedef enum
{
   UIM_STATE_VALID=1,
   ROMSIM=240,
   NO_UIM_CARD=255
}CTATUimStateT;

typedef enum
{
    UIM_UPDATE_PHB,
    UIM_Del_PHB,
    UIM_UPDATE_SMS,
    UIM_Del_SMS,

    ME_UPDATE_PHB,
    ME_Del_PHB,
    ME_UPDATE_SMS,
    ME_Del_SMS
}UimSmsPhbOpT;

typedef PACKED_PREFIX struct
{
   int16    RxPowerDbmQ6;
   uint16   RxGainState;
   int16    DivRxPowerDbmQ6;
   uint16   DivRxGainState;
   int16    TxPowerDbmQ6;
   uint16   TxGainState;
   int16    TxClosedLoopAdjQ6;
#ifdef MTK_DEV_C2K_IRAT
   int16    LastEcIo;
   int16    StableEcIo;
   int16    EcIo;
#endif
#ifdef MTK_CBP
   int16    TxAnt;
#endif
#if defined (MTK_CBP) && defined (MTK_DEV_C2K_IRAT)
    uint16  Sinr;
#endif

} PACKED_POSTFIX ValUiDoRxTxPowerDataT;

 /*------------------------------------------------------------------------
 * IOP Mux events
 *------------------------------------------------------------------------*/
typedef enum
{
  VAL_IOP_MUX_CLOSE_EVENT_ID
} ValIopMuxEvtT;

typedef PACKED_PREFIX struct
{
    ValIopMuxEvtT event;
} PACKED_POSTFIX  ValIopMuxEvtMsgT;

/*------------------------------------------------------------------------
 * GPS events
 *------------------------------------------------------------------------*/
typedef enum
{
   GPS_SUCCESS,
   GPS_FAIL
}ValGpsStatusT;

typedef enum
{
   CP_VAL_GPS_POSITION_UTC_TIME = 0x1,
   CP_VAL_GPS_POSITION_LATITUDE = 0x2,
   CP_VAL_GPS_POSITION_LONGITUDE = 0x4,
   CP_VAL_GPS_POSITION_SPEED = 0x8,
   CP_VAL_GPS_POSITION_HEADING = 0x10,
   CP_VAL_GPS_POSITION_MAGNETIC_VARIATION = 0x20,
   CP_VAL_GPS_POSITION_WRT_SEA_LEVEL = 0x40,
   CP_VAL_GPS_POSITION_WRT_ELLIPSOID = 0x80,
   CP_VAL_GPS_POSITION_DILUTION_OF_PRECISION = 0x100,
   CP_VAL_GPS_POSITION_HORIZONTAL_DILUTION_OF_PRECISION = 0x200,
   CP_VAL_GPS_POSITION_VERTICAL_DILUTION_OF_PRECISION = 0x400,
   CP_VAL_GPS_POSITION_VALID_SATELLITE_COUNT = 0x800,
   CP_VAL_GPS_POSITION_VALID_SATELLITE_USED_PRNS = 0x1000,
   CP_VAL_GPS_POSITION_VALID_SATELLITE_IN_VIEW = 0x2000,
   CP_VAL_GPS_POSITION_VALID_SATELLITE_IN_VIEW_PRNS = 0x4000,
   CP_VAL_GPS_POSITION_VALID_SATELLITE_IN_VIEW_ELEVATION = 0x8000,
   CP_VAL_GPS_POSITION_VALID_SATELLITE_IN_VIEW_AZIMUTH = 0x10000,
   CP_VAL_GPS_POSITION_VALID_SATELLITE_IN_VIEW_SIGNAL_TO_NOISE_RATIO = 0x20000,
   CP_VAL_GPS_POSITION_UNCERTAINTY_ERROR = 0x40000,
   CP_VAL_GPS_POSITION_FIX_MODE = 0x80000,
   CP_VAL_GPS_POSITION_FIX_ERROR = 0x100000,
   CP_VAL_GPS_POSITION_ALL
}GpsPositionValidityMaskT;

typedef enum
{
   GPS_FIX_QUALITY_UNKNOWN, /*Fix uses information from GPS satellites only.*/
   GPS_FIX_QUALITY_GPS, /*Fix uses information from GPS satellites and also a differential GPS (DGPS) station. */
   GPS_FIX_QUALITY_DGPS
}ValGpsFixQualityT;

typedef enum
{
   VAL_GPS_FIX_UNKNOWN,
   VAL_GPS_FIX_2D,
   VAL_GPS_FIX_3D
}ValGpsFixTypeT;

typedef enum
{
   GPS_FIX_SELECTION_UNKNOWN,
   GPS_FIX_SELECTION_AUTO,
   GPS_FIX_SELECTION_MANUAL
}ValGpsSelectionTypeT;

typedef enum
{
   CP_VAL_GPS_FIX_MODE_UNKNOWN,
   CP_VAL_GPS_FIX_MODE_MSA,
   CP_VAL_GPS_FIX_MODE_MSB,
   CP_VAL_GPS_FIX_MODE_MSS,
   CP_VAL_GPS_FIX_MODE_AFLT,
   CP_VAL_GPS_FIX_MODE_SPEED_OPTIMAL,
   CP_VAL_GPS_FIX_MODE_ACCURACY_OPTIMAL,
   CP_VAL_GPS_FIX_MODE_DATA_OPTIMAL,
   CP_VAL_GPS_FIX_MODE_CONTROL_PLANE,
#ifdef MTK_CBP
   CP_VAL_GPS_FIX_MODE_SUPL_MSA_HYBRID_AGPS,
   CP_VAL_GPS_FIX_MODE_SUPL_MSA_AFLT_ONLY_AGPS,
   CP_VAL_GPS_FIX_MODE_SUPL_MSB_AGPS,
#endif
   CP_VAL_GPS_FIX_MODE_COUNT /* must be last entry*/
}ValGpsFixModeT;

typedef enum
{
   CP_VAL_GPS_SUCCESS,
   CP_VAL_GPS_ERROR_INVALID_PARM,
   CP_VAL_GPS_ERROR_INVALID_SECURITY_STATUS,
   CP_VAL_GPS_ERROR_LOCATION_REQ_QUEUE_FULL,
   CP_VAL_GPS_ERROR_PREV_LOCATION_REQ_PENDING,
   CP_VAL_GPS_ERROR_CP_SESSION_PENDING
}ValGpsFixOperationStatusT;

typedef enum
{
   CP_VAL_GPS_PRIVACY_SETTING_VALUE_UNKNOWN = 0,
   CP_VAL_GPS_PRIVACY_SETTING_VALUE_E911_ONLY,
   CP_VAL_GPS_PRIVACY_SETTING_VALUE_LOCATION_ON,
   CP_VAL_GPS_PRIVACY_SETTING_VALUE_LOCATION_MI_ONLY,
   CP_VAL_GPS_PRIVACY_SETTING_VALUE_LOCATION_MT_ONLY
}ValGpsPrivacySettingT;

typedef enum
{
   CP_VAL_GPS_ASSISTANCE_DATA_ALMANAC,
   CP_VAL_GPS_ASSISTANCE_DATA_EPHEMERIS,
   CP_VAL_GPS_ASSISTANCE_DATA_LAST_LOCATION,
   CP_VAL_GPS_ASSISTANCE_DATA_BASE_STATION_ALMANAC,
   CP_VAL_GPS_ASSISTANCE_DATA_ALMANAC_CORRECTION,
   CP_VAL_GPS_ASSISTANCE_DATA_SV_HEALTH_INFO,
   CP_VAL_GPS_ASSISTANCE_DATA_NAVIGATION_MSG_BITS,
   CP_VAL_GPS_ASSISTANCE_DATA_ALL_GPS_DATA
}ValGpsAssistanceDataT;

typedef enum
{
   CP_VAL_GPS_DEVICE_HW_STATE = 0x1,
   CP_VAL_GPS_DEVICE_EPH_SV_MASK = 0x2,
   CP_VAL_GPS_DEVICE_ALM_SV_MASK = 0x4,
   CP_VAL_GPS_DEVICE_SAT_IN_VIEW_PRN = 0x8,
   CP_VAL_GPS_DEVICE_SAT_IN_CARRIER_TO_NOISE_RATIO = 0x10,
   CP_VAL_GPS_DEVICE_ERROR = 0x20
} ValGpsDeviceValidityMaskT;

typedef enum
{
   CP_VAL_GPS_HW_STATE_UNKNOWN,
   CP_VAL_GPS_HW_STATE_ON,
   CP_VAL_GPS_HW_STATE_IDLE
} ValGpsHwStateT;

typedef enum
{
   SESS_CLOSE_NORMAL,
   TCP_OPEN_FAIL,
   PPM_PREF_QUAL_TIMER_OUT,
   SESS_OPEN_ERROR,
   BS_REJ_ERROR,
   OUT_REQ_TIMEROUT,
   ORIG_CANCEL,
   BS_DISCONN_EV,
   BS_CLOSE_SESS,
   MANUAL_BS_ALMANAC_FAIL,
   SESS_MSS_FALLBACK_EV,
   MPC_FAIL,
   NUM_SESS_STATUS
}SessionStatusE;

/*GPS_OPEN_DEVICE_HW_INIT_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
} PACKED_POSTFIX  ValGpsOpenDeviceHwInitMsgT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsOpenDeviceHwInitRspMsgT;

/*GPS_CLOSE_DEVICE_ETS */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
} PACKED_POSTFIX  ValGpsCloseDeviceMsgT;

typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsCloseDeviceRspMsgT;

/*GPS_POSITION_GET_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsPositionGetMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsPositionGetReqT;

typedef PACKED_PREFIX struct
{
   uint16 Year;
   uint16 Month;
   uint16 DayOfWeek;
   uint16 Day;
   uint16 Hour;
   uint16 Minute;
   uint16 Second;
   uint16 Milliseconds;

   /*; GPS week as the number of whole weeks since GPS time zero*/
   uint32 GPSweek;
   /*; GPS time of week in milliseconds*/
   uint32 GPSTimeOfWeek;
} PACKED_POSTFIX  ValGpsSystemTimeT;

typedef PACKED_PREFIX struct
{
   /*; Horizontal position uncertainty in meters of axis aligned with the angle
  ; specified in dWHorizontalErrorAngle of a two-dimension horizontal error
  ; ellipse. The value dwHorizontalConfidence gives the percentage of positions
  ; expected to fall within this ellipse, e.g. dwHorizontalConfidence = 39
  ; indicates a 1-sigma error ellipse is given.*/
   uint32 HorizontalErrorAlong;
   uint32 HorizontalErrorAngle;
   uint32 HorizontalErrorPerp;
   uint32 VerticalError;
   uint32 HorizontalConfidence;

   /*; Horizontal velocity uncertainty in m/s*/
   uint32 HorizontalVelocityError;
   /*; Vertical velocity uncertainty in m/s*/
   uint32 VerticalVelocityError;
  /*; Horizontal heading uncertainty in degrees*/
   uint32 HorinzontalHeadingError;
  /*; Latitude uncertainty*/
   uint32 LatitudeUncertainty;
  /*; Longitude Uncertainty*/
   uint32 LongitudeUncertainty;
} PACKED_POSTFIX  VALGpsPositionErrorT;

typedef PACKED_PREFIX struct
{
  uint32 ValidityMask;

  ValGpsSystemTimeT UTCTime;

  int32     Latitude; /*in degrees, positive number indicate north latitude*/
  int32 Longitude; /*in degrees, positive number indicate east longitude*/
  double  Speed; /*in knots (nautical miles)*/
  double  Heading; /*in degrees, a heading of zero is true north*/

/*the difference between the bearing to true north and the bearing shown on a magnetic compass, positive numbers indicate east*/
  double  MagneticVariation;
  double  AltitudeWRTSeaLevel; /*in meters, with respect to sea level*/
  double  AltitudeWRTEllipsoid; /*in meters, with respect to the WGS84 ellipsoid*/

  ValGpsFixQualityT FixQuality;
  ValGpsFixTypeT  FixType;
  ValGpsSelectionTypeT  SelectionType;

  /*; degree to which the overall position is affected by positional dilution of posisiont (PDOP).  PDOP is caused by the location of the satellites providing
  the GPS fix.  Lower number indicates a more accurate position.  A value of 1.
  0 indeicates the least dilution (highest accuracy), a value of 50 indicates
  the most dilution (lowest accuracy).*/
  double  PositionDilutionOfPrecision;
  double  HorizontalDilutionOfPrecision;
  double  VerticalDilutionOfPrecision;
  uint32  SatelliteCount; /*number of satellites used to obtain the position*/

  uint32  SatellitesUsedPRNs[12];
  uint32  SatellitesInView;
  uint32  SatellitesInViewPRNs[12];
  uint32  SatellitesInViewElevation[12];
  uint32  SatellitesInViewAzimuth[12];
  uint32  SatellitesInViewSNR[12];

  VALGpsPositionErrorT  GPSPositionError;

  ValGpsFixModeT  FixMode;

  /*; the number of GPS fixes attempted*/
  uint32 GPSSessionCount;
  /*; the number of positions*/
  uint32 NumberOfPositions;
  /*; Horizontal velocity in m/s*/
  uint32 HorizontalVelocity;
  /* Vertical velocity in m/s*/
  uint32 VerticalVelocity;
} PACKED_POSTFIX ValGpsLocRespMsgT;

typedef PACKED_PREFIX struct
{
  MonSysTimeT SysTime;
  uint32 InstanceID;
  ValGpsLocRespMsgT LocResp;
} PACKED_POSTFIX  ValGpsPositionGetRspMsgT;

/*GPS_OPEN_SESSION_ETS*/

typedef enum
{
   VAL_GPS_SESSION_OPEN_SUCCESS,
   VAL_GPS_SESSION_OPEN_ERROR_SESSION_ALREADY_EXISTS,
   VAL_GPS_SESSION_OPEN_ERROR_MAX_SESSION_LIMIT_REACHED
} ValGpsSessionOpenStatusT;

typedef enum

{
      VAL_USERMODE,
      Val_TESTMODE,
      VAL_APMODE

}ValLBS_ModeT;

typedef PACKED_PREFIX struct
{
  uint8 ResultCode;
} PACKED_POSTFIX ValTlsOpenResponseMsgT;

typedef struct
{
  uint16 len;
  uint8 *buf;
}ValTlsRxIndT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT rspInfo;
  uint32 InstanceID;
} PACKED_POSTFIX  ValGpsSessionOpenMsgT;

typedef PACKED_PREFIX struct
{
  uint32 InstanceID;
} PACKED_POSTFIX  ValGpsSessionOpenReqT;

typedef PACKED_PREFIX struct
{
  uint32 InstanceID;
  ValGpsSessionOpenStatusT Status;
} PACKED_POSTFIX  ValGpsSessionOpenRspMsgT;

/*GPS_START_FIX_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
   uint32 GpsStartFixFlag;
} PACKED_POSTFIX  ValGpsStartFixMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint32 GpsStartFixFlag;
   uint8    LBSMode;
} PACKED_POSTFIX  ValGpsStartFixReqT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsFixOperationStatusT Status;
} PACKED_POSTFIX  ValGpsStartFixRspMsgT;

/*GPS_QUERY_FIX_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsQueryFixMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsFixOperationStatusT Status;
} PACKED_POSTFIX  ValGpsQueryFixRspMsgT;

/*GPS_STOP_FIX_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsStopFixMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsStopFixReqT;

typedef PACKED_PREFIX struct
{
   uint32 action;
} PACKED_POSTFIX  ValGpsSessionCancelReqT;


typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsFixOperationStatusT Status;
} PACKED_POSTFIX  ValGpsStopFixRspMsgT;

/*GPS_CONFIG_FIX_MODE_SET_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
   ValGpsFixModeT Mode;
} PACKED_POSTFIX  ValGpsFixModeConfigMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsFixModeT Mode;
} PACKED_POSTFIX  ValGpsFixModeConfigReqT;


typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsFixOperationStatusT Status;
} PACKED_POSTFIX  ValGpsFixModeConfigRspMsgT;

/*GPS_CONFIG_FIX_MODE_GET_MSG*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
} PACKED_POSTFIX ValGpsFixModeGetMsgT;

typedef PACKED_PREFIX struct
{
   ValGpsFixOperationStatusT Status;
   ValGpsFixModeT Mode;
} PACKED_POSTFIX ValGpsFixModeGetRspMsgT;

/*GPS_CONFIG_FIX_RATE_SET_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
   uint32 NumFixes;
   uint32 TimeBFixes;
} PACKED_POSTFIX  ValGpsFixRateConfigMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint32 NumFixes;
   uint32 TimeBFixes;  /*in seconds*/
} PACKED_POSTFIX  ValGpsFixRateConfigReqT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsFixOperationStatusT Status;
} PACKED_POSTFIX  ValGpsFixRateConfigRspMsgT;

/*GPS_CONFIG_QOS_SET_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
   uint32 HorizontalAccuracy;
   uint32 VerticalAccuracy;
#ifdef MTK_CBP
    uint32  PseudorangeAccuracy;
#endif
   uint32 Performance;
} PACKED_POSTFIX  ValGpsQosConfigMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint32 HorizontalAccuracy;
   uint32 VerticalAccuracy;
   uint32 Performance;
} PACKED_POSTFIX  ValGpsQosConfigReqT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsFixOperationStatusT Status;
} PACKED_POSTFIX  ValGpsQosConfigRspMsgT;

/*GPS_WRITE_SERVER_CONFIG_ETS*/
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  rspInfo;
  bool bAddrValid;
  bool  IPType; /*0: IPV4; 1: IPV6*/
  uint32 Ipv4Addr;
  uint32 IPv6Addr[4];
  uint32 PortNum;
  bool   bURLValid;
  uint8  URLAddr[256];
} PACKED_POSTFIX  ValGpsWriteServerConfigMsgT;


/*GPS_READ_SERVER_CONFIG_ETS*/
typedef PACKED_PREFIX struct
{
  bool bAddrValid;
  bool  IPType; /*0: IPV4; 1: IPV6*/
  uint32 Ipv4Addr;
  uint32 IPv6Addr[4];
  uint32 PortNum;
  bool   bURLValid;
  uint8  URLAddr[256];
} PACKED_POSTFIX  ValGpsReadServerConfigMsgT;


typedef PACKED_PREFIX struct
{
   bool bAddrValid;
  bool  IPType; /*0: IPV4; 1: IPV6*/
  uint32 Ip4Addr;
  uint32 IP6Addr[4];
   uint32 PortNum;
   bool   bURLValid;
   uint8  URLAddr[256];
} PACKED_POSTFIX  ValGpsWriteServerConfigReqT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsWriteServerConfigRspMsgT;


typedef struct
{
    UINT8 id_s;
    UINT8 pref_resp_qual;
    UINT8 offset_req;
}VAL_LCS_PPM_REQ_T;


/*GPS_ENABLE_SECURITY_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsEnableSecurityMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsEnableSecurityReqT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsEnableSecurityRspMsgT;

/*GPS_WRITE_SECURITY_CONFIG_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
   uint8 SecurityDataID;
   uint8  SecurityDataLen;
   uint8  SecurityData[20];
} PACKED_POSTFIX  ValGpsWriteSecurityConfigMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint8 SecurityDataID;
   uint8  SecurityDataLen;
   uint8  SecurityData[20];
} PACKED_POSTFIX  ValGpsWriteSecurityConfigReqT;

typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   uint32 InstanceID;
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsWriteSecurityConfigRspMsgT;

/*GPS_READ_SECURITY_CONFIG_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsReadSecurityConfigMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsReadSecurityConfigReqT;


typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   uint32 InstanceID;
   uint32 HashAlgorithm;
   uint8 SecurityDataID;
   uint8  SecurityDataLen;
   uint8  SecurityData[20];
} PACKED_POSTFIX  ValGpsReadSecurityConfigRspMsgT;

typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   ValGpsStatusT Status;
   uint32 InstanceID;
   uint32 HashAlgorithm;
   uint8 SecurityDataID;
   uint8  SecurityDataLen;
   uint8  SecurityData[20];
} PACKED_POSTFIX ValGpsReadSecurityConfigRspWithStatusMsgT;

/*GPS_READ_ENCRYPT_CONFIG_ETS*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsReadEncryptConfigMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsReadEncryptConfigReqT;

typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   uint32 InstanceID;
   uint32 EncryptAlgorithm;
   ValGpsSystemTimeT UTCTime;

   uint8  EncryptDataLen;
   uint8  EncryptData[20];
} PACKED_POSTFIX  ValGpsReadEncryptConfigRspMsgT;

typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   ValGpsStatusT Status;
   uint32 InstanceID;
   uint32 EncryptAlgorithm;
   ValGpsSystemTimeT UTCTime;
   uint8  EncryptDataLen;
   uint8  EncryptData[20];
} PACKED_POSTFIX  ValGpsReadEncryptConfigRspWithStatusMsgT;


/*GPS_LBS_PRIVACY_SETTING_GET_ETS*/
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  rspInfo;
} PACKED_POSTFIX  ValGpsLbsPrivacySettingGetMsgT;

typedef PACKED_PREFIX struct
{
  MonSysTimeT SysTime;
   ValGpsStatusT Status;
   ValGpsPrivacySettingT PrivacySetting;
} PACKED_POSTFIX  ValGpsLbsPrivacySettingGetRspMsgT;

/*GPS_LBS_PRIVACY_SETTING_SET_ETS*/
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  rspInfo;
  uint8 privacySetting;
} PACKED_POSTFIX  ValGpsLbsPrivacySettingsSetMsgT;

typedef PACKED_PREFIX struct
{
  ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsLbsPrivacySettingsSetRspMsgT;

/*GPS_BASESSD_GET_ETS*/
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  rspInfo;
  uint32 InstanceID;
} PACKED_POSTFIX  ValGpsBaseSSDGetMsgT;

typedef PACKED_PREFIX struct
{
  MonSysTimeT SysTime;
   ValGpsStatusT Status;
   uint32 InstanceID;
   uint8  BaseSSD[16];
   ValGpsSystemTimeT UTCTime;
} PACKED_POSTFIX  ValGpsBaseSSDGetRspMsgT;

/*GPS_BASESSD_SET_MSG*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint8  BaseSSD[16];
   ValGpsSystemTimeT UTCTime;
} PACKED_POSTFIX ValGpsBaseSSDSetMsgT;

typedef PACKED_PREFIX struct
{
   uint8  BaseSSD[16];
   ValGpsSystemTimeT UTCTime;
} PACKED_POSTFIX ValGpsBaseSSDSetReqT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
} PACKED_POSTFIX ValGpsBaseSSDSetRspMsgT;

/*GPS_RAND_PERIOD_SET_ETS*/
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  rspInfo;
   uint32 InstanceID;
   uint32 RandPeriod;
} PACKED_POSTFIX  ValGpsRandPeriodSetMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint32 RandPeriod;
} PACKED_POSTFIX  ValGpsRandPeriodSetReqT;

typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   uint32 InstanceID;
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsRandPeriodSetRspMsgT;

/*GPS_START_MODE_SET_ETS*/

typedef enum
{
  VAL_GPS_START_MODE_HOT=1,
  VAL_GPS_START_MODE_WARM,
  VAL_GPS_START_MODE_COLD
} ValGpsStartMode;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT rspInfo;
  ValGpsStartMode startMode;
} PACKED_POSTFIX  ValGpsStartModeSetMsgT;


typedef PACKED_PREFIX struct
{
  ValGpsStartMode startMode;
} PACKED_POSTFIX  ValGpsStartModeSetReqT;

typedef PACKED_PREFIX struct
{
  ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsStartModeSetRspMsgT;


/*GPS_RESET_ASSIST_ETS*/
typedef PACKED_PREFIX struct
{
  ExeRspMsgT  rspInfo;
   ValGpsAssistanceDataT AssisData;
} PACKED_POSTFIX  ValGpsResetAssistMsgT;


typedef PACKED_PREFIX struct
{
  ValGpsAssistanceDataT AssisData;
} PACKED_POSTFIX  ValGpsResetAssistDataReqMsgT;


typedef PACKED_PREFIX struct
{
   MonSysTimeT SysTime;
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsResetAssistRspMsgT;

typedef PACKED_PREFIX struct
{
   bool OnOff; /*0: Off; 1: On*/
} PACKED_POSTFIX ValGpsNmeaOuputCfgMsgT;

 /*Val GPS Location Update ETS Spy*/
typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint8 LocationInfoIncl;
} PACKED_POSTFIX  ValGpsLocationUpdateSpy1MsgT;

 typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint8 LocationInfoIncl;
   ValGpsLocRespMsgT LocResp;
} PACKED_POSTFIX  ValGpsLocationUpdateSpy2MsgT;

/*Val GPS State Change ETS Spy*/
typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsStateChangeSpyMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT  rspInfo;
   uint8  Enable; /*0: close;1: open*/
} PACKED_POSTFIX ValGpsNmeaOuputSettingsMsgT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsNmeaOuputSettingsRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  rspInfo;
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsFixRateConfigGetMsgT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
   uint32 NumFixes;
   uint32 TimeBFixes;
} PACKED_POSTFIX  ValGpsFixRateConfigGetRspMsgT;

 #define RXN_PGPS_NUM_SERVERS 3

typedef PACKED_PREFIX struct
{

  uint8 feature_enable;
  uint8 download_clock_enable;
  char vendor_id[50+1];
  char device_id[50+1];
  char model_id[50+1];
  char  servers[RXN_PGPS_NUM_SERVERS][72];
  uint16 server_ports[RXN_PGPS_NUM_SERVERS];
} PACKED_POSTFIX ValPgpsCfgT;

typedef struct
{
   ExeRspMsgT  rspInfo;
} ValGpsSuplCellInfoMsgT;

typedef PACKED_PREFIX struct
{
   uint16   NID;                 /* Network ID, Range [0..0xFFFF] */
   uint16   SID;                 /* System ID, Range [0..0x7FFF] */
   uint16   Band;                /* Band information; enum SysCdmaBandT  */
   uint16   BaseID;              /* Base Station ID and Range [0..0xFFFF] */
   uint16   RefPN;               /* Base Station PN number, Range[0..0x1FF]*/
   uint32   BaseLat;             /* Base Station Latitudue, Range[0..0x3FFFFF] */
   uint32   BaseLong;            /* Base Station Longitude, Range[0..0x7FFFFF] */
   uint16   GPSWeekNumber;       /* GPS Week number, Range[0..0xFFFF] */
   uint32   GPSSeconds;          /* GPS Seconds, Range[0..0x3FFFFF] */
} PACKED_PREFIX ValGpsSuplCDMAInformationT;

typedef PACKED_PREFIX struct
{
   uint8    SectorID[16];        /* Sector ID, Length 128bits */
   uint16   Band;                /* Band information; enum SysCdmaBandT  */
   uint32   BaseLat;             /* Base Station Latitudue, Range[0..0x3FFFFF] */
   uint32   BaseLong;            /* Base Station Longitude, Range[0..0x7FFFFF] */
   uint16   GPSWeekNumber;       /* GPS Week number, Range[0..0xFFFF] */
   uint32   GPSSeconds;          /* GPS Seconds, Range[0..0x3FFFFF] */
} PACKED_PREFIX ValGpsSuplHRPDInformationT;

typedef PACKED_PREFIX struct
{
   uint8    ValidCellIDInform;   /* 0: Not Valid, 1: CDMA, 2: HRPD */
   ValGpsSuplCDMAInformationT CDMAInfo;
   ValGpsSuplHRPDInformationT HRPDInfo;
} PACKED_PREFIX ValGpsSuplCellInformationRspMsgT;

#define ValPgpsCfgGetMsgT  ValPgpsCfgT


/*
typedef PACKED_PREFIX struct
{
  uint8 feature_enable;
  uint8 download_clock_enable;
  char vendor_id[50];
  char device_id[50];
  char model_id[50];
  char  server1[72];
  char server2[72];
  char server3[72];
} PACKED_POSTFIX  ValPgpsCfgGetMsgT;

*/


/*Val GPS State Change ETS Spy*/
typedef PACKED_PREFIX struct
{
   /* indicate the validity of each field in this record */
   ValGpsDeviceValidityMaskT ValidityMask;
   ValGpsHwStateT  Status;
   /*; Ephemeris Satellite Vehicles validaty mask. Each bit indicates the validity
  ; of the Ephemeris for the associated SV, bit-0 indicates the validity for SV1,
  ; bit-1 for SV2, ? bit-31 for SV32.*/
   uint32 EphemerisSVMask;
   uint32 AlmanacSVMask;

    /*; PRN numbers of the satellites in view of the GPS hardware.  The order of
  ;the elements in the next array, Satellites In View Carrier to Noise Ratio,
  ;must match the ordering of this list of SV PRNs.*/
   uint32 SatellitesInViewPRNs[12];
   uint32 SatellitesInViewCarriertoNoiseRatio[12];
   uint32 DeviceError;
} PACKED_POSTFIX  ValGpsStateChangeParamsSpyMsgT;

 /*Val GPS Rand Num1 Update Spy*/
typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsRandNum1UpdateSpyMsgT;

typedef PACKED_PREFIX struct
{
   uint8 *RspData;
} PACKED_POSTFIX  ValEtsGpsParmSetRspMsgT;

typedef PACKED_PREFIX struct
{
  uint8  *dataP;
} PACKED_POSTFIX  ValEtsGpsParmGetRspMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   GPS_FIX_MODE Mode;
   uint8  Status;
   uint8  SessStatus;
} PACKED_POSTFIX  ValPswIs801SessDoneMsgT;

#ifdef MTK_DEV_GPSONE_ON_LTE
typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   int32 RatMode;
} PACKED_POSTFIX  ValPswIs801TcpConnReqMsgT;
#endif

typedef PACKED_PREFIX struct
{
#ifdef MTK_CBP
   uint32 InstanceId; /* add for supl, retrieve fix mode by instance id. */
#endif
#if defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT)
    uint8   data[255];
#endif
   uint8 *DataP;
   uint16 Size;

} PACKED_POSTFIX  ValGpsTcpbSendDataMsgT;

typedef struct
{
    uint32 Instance;
    uint16 TimeRefCDMA;
#if !(defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT))
    double LAT;
    double LONG;
#else
    float  LAT;
    float  LONG;
#endif
    uint8  LocUncrtnyAng;
    uint8  LocUncrtnyA;
    uint8  LocUncrtnyP;
    uint8  FixType;
    bool   VelocityIncl;
    uint16 VelocityHor;
    uint16 Heading;
    uint8  VelocityVer;
    bool   ClockIncl;
    uint32 ClockBias;
    uint16 ClockDrift;
    bool   HeightIncl;
    uint16 Height;
    uint8  LocUncrtnyV;
} ValLocRespDataT;

typedef struct
{
  uint32 ValidityMask;
  ValGpsSystemTimeT UTCTime;

  bool bOldPosDataSaved;

  int32   Latitude; /*in degrees, positive number indicate north latitude*/
  int32   Longitude; /*in degrees, positive number indicate east longitude*/
#if !(defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT))
  double  Speed; /*in knots (nautical miles)*/
  double  Heading; /*in degrees, a heading of zero is true north*/

  double  MagneticVariation;
  double  AltitudeWRTSeaLevel; /*in meters, with respect to sea level*/
  double  AltitudeWRTEllipsoid; /*in meters, with respect to the WGS84 ellipsoid*/
#else
    float  Speed; /*in knots (nautical miles)*/
    float  Heading; /*in degrees, a heading of zero is true north*/

    float  MagneticVariation;
    float  AltitudeWRTSeaLevel; /*in meters, with respect to sea level*/
    float  AltitudeWRTEllipsoid; /*in meters, with respect to the WGS84 ellipsoid*/
#endif
  ValGpsFixQualityT FixQuality;
  ValGpsFixTypeT  FixType;
  ValGpsSelectionTypeT  SelectionType;

#if !(defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT))
  double  PositionDilutionOfPrecision;
  double  HorizontalDilutionOfPrecision;
  double  VerticalDilutionOfPrecision;
#else
    float  PositionDilutionOfPrecision;
    float  HorizontalDilutionOfPrecision;
    float  VerticalDilutionOfPrecision;
#endif
  uint32  SatelliteCount; /*number of satellites used to obtain the position*/

  uint32  SatellitesUsedPRNs[12];
  uint32  SatellitesInView;
  uint8     totalGsvNum;
  uint8     seqGsvNum;
  uint32  SatellitesInViewPRNs[12];
  uint32  SatellitesInViewElevation[12];
  uint32  SatellitesInViewAzimuth[12];
  uint32  SatellitesInViewSNR[12];

  VALGpsPositionErrorT  GPSPositionError;

  ValGpsFixModeT  FixMode;
}ValNMEADataT;

typedef struct{
  uint32 Instance;
  GPS_FIX_MODE FixMode;
  union {
     ValLocRespDataT MsaCPLocRespMsg;
     PswLocRspDataT  MsbMssLocRespMsg;
     ValNMEADataT  MssNmeasStreamMsg;
  }LocRespDataT;
}ValLocRespMsgT;

typedef struct{
  uint32 Instance;
  GPS_FIX_MODE FixMode;
    union
    {
     ValLocRespDataT MsaCPLocRespMsg;
     PswLocRspDataT  MsbMssLocRespMsg;
     PswGpsNmeaStreamMsgT  MssNmeasStreamMsg;
    } LocRespDataT;
}ValPswLocRespMsgT;

typedef PACKED_PREFIX struct {
  uint32 Instance;
} PACKED_POSTFIX ValPswSessActiveIndMsgT;

typedef PACKED_PREFIX struct {
   uint32 Instance;
#ifdef MTK_DEV_GPSONE_ON_LTE
   int32 RatMode;
#endif
} PACKED_POSTFIX ValPswMpcConnReqMsgT;

typedef PACKED_PREFIX struct {
   uint32 Interval;
   bool bGPGGA;
   bool bGPGSV;
   bool bGPGSA;
   bool bGPRMC;
   bool bGPGST;
   bool bGPGLL;
   bool bGPVTG;
} PACKED_POSTFIX ValNMEACfgSetMsgT;

typedef PACKED_PREFIX struct {
   ExeRspMsgT  rspInfo;
} PACKED_POSTFIX ValNmeaCfgGetReqMsgT;

typedef PACKED_PREFIX struct {
   uint32 Interval;
   bool bGPGGA;
   bool bGPGSV;
   bool bGPGSA;
   bool bGPRMC;
   bool bGPGST;
   bool bGPGLL;
   bool bGPVTG;
} PACKED_POSTFIX ValNmeaCfgGetRspMsgT;

typedef PACKED_PREFIX struct {
  bool bEncryptDecryptResult;
} PACKED_POSTFIX  ValSecAesCryptRspMsgT;

typedef PACKED_PREFIX struct {
  ExeRspMsgT  rspInfo;
  uint32 InstanceID;
  uint8   Mode;
} PACKED_POSTFIX ValGpsRestartFixMsgT;

typedef PACKED_PREFIX struct {
   ValGpsStatusT Status;
} PACKED_POSTFIX ValGpsRestartFixRspMsgT;

typedef enum
{
   VAL_GPS_TECHNOLOGY_CELL_ID_BASED,
   VAL_GPS_TECHNOLOGY_GPS_BASED,
   VAL_GPS_TECHNOLOGY_AFLT_BASED,
   VAL_GPS_TECHNOLOGY_GPS_AND_AFLT_BASED
}ValGpsSearchTechE;

typedef PACKED_PREFIX struct {
  ExeRspMsgT  rspInfo;
  uint32 InstanceID;
  ValGpsSearchTechE SearchTech;
} PACKED_POSTFIX ValGpsConfigTechMsgT;

typedef PACKED_PREFIX struct {
  uint32 InstanceID;
   ValGpsStatusT Status;
} PACKED_POSTFIX ValGpsConfigTechRspMsgT;

typedef struct {
  uint32 Instance;
}ValPswMpcCloseConnMsgT;

typedef enum
{
   CP_VAL_GPS_SECURITY_DISABLE,
   CP_VAL_GPS_SECURITY_ENABLE
}ValGpsSecOperE;

typedef PACKED_PREFIX struct {
  ExeRspMsgT  rspInfo;
 /* ; Enable or disable Verizon security call flow*/
   ValGpsSecOperE Status;
/*; The Security Code must be correct before the GPS security status can be modified*
; The Security Code should be the last 4 digits of the MDN in reverse order
; If the incorrect security code is entered 5 times consecutively then this command should
; be rejected until the device is re-flashed and the fault cleared*/
   uint32 SecCode;
} PACKED_POSTFIX ValGpsSetSecMsgT;

typedef PACKED_PREFIX struct {
 /* ; Enable or disable Verizon security call flow*/
   ValGpsSecOperE Status;
/*; The Security Code must be correct before the GPS security status can be modified*
; The Security Code should be the last 4 digits of the MDN in reverse order
; If the incorrect security code is entered 5 times consecutively then this command should
; be rejected until the device is re-flashed and the fault cleared*/
   uint32 SecCode;
} PACKED_POSTFIX ValGpsSetSecReqT;

typedef enum
{
   CP_VAL_GPS_SET_SECURITY_SUCCESS,
  /*; when security code received does not match current code*/
   CP_VAL_GPS_SET_SECURITY_INCORRECT_CODE,
 /*when incorrect security code is received 5 consecutive time
    it should persist even after a power cycle */
   CP_VAL_GPS_SET_SECURITY_REJECTED
}ValGpsSecStatusE;

typedef PACKED_PREFIX struct {
   MonSysTimeT SysTime;
   ValGpsSecStatusE Status;
} PACKED_POSTFIX ValGpsSetSecRspMsgT;

typedef PACKED_PREFIX struct {
   uint32 EphSysTime;
   uint32 AlmSysTime;
} PACKED_POSTFIX ValAlmEphUpdateMsgT;

typedef PACKED_PREFIX struct {
   uint16 T_Dormancy;
   bool bSaveToDbm;
} PACKED_POSTFIX ValCtaUpdateMsgT;

typedef PACKED_PREFIX struct {
   UINT8 N_DIGITS;
   UINT8 MDN[OTA_MAX_MDN_DIGITS];
} PACKED_POSTFIX  ValPswMDNUpdatedMsgT;
typedef PACKED_PREFIX struct {
   uint32 ESN;
} PACKED_POSTFIX  ValPswESNUpdatedMsgT;

#define HASH_DATA_NUM      20

typedef PACKED_PREFIX struct {
   uint32 BeginOffset;
   uint32 EndOffset;
   uint8 Hash[HASH_DATA_NUM];
} PACKED_POSTFIX  ValGpsOffsetHashDataT;

typedef PACKED_PREFIX struct {
   ExeRspMsgT  rspInfo;
   uint32   InstanceID;
   uint32 OffsetsSize;
   uint8 *pHashData;
} PACKED_POSTFIX  ValGpsWriteOffsetInfoMsgT;

typedef PACKED_PREFIX struct {
   ExeRspMsgT  rspInfo;
   uint32   InstanceID;
} PACKED_POSTFIX  ValGpsReadOffsetInfoMsgT;

typedef PACKED_PREFIX struct {
   uint32   InstanceID;
   ValGpsStatusT State;
   uint32 OffsetsSize;
   uint8 *pHashData;
} PACKED_POSTFIX  ValGpsReadOffsetInfoRspMsgT;

typedef PACKED_PREFIX struct {
   uint32   InstanceID;
   ValGpsStatusT State;
} PACKED_POSTFIX  ValGpsWriteOffsetInfoRspMsgT;

typedef struct
{
   uint8   SubnetMask;
   uint8   ColorCode;
   uint8   SectorID[16];
   uint8   UserServed;
 } ValEvdoOvhdInfoT;

#ifdef SYS_OPTION_EVDO
extern uint8    Rev0RevRate;
extern uint8    RevPerfStatEnable;
#endif

typedef PACKED_PREFIX struct
{
   uint32 SystemTime;
} PACKED_POSTFIX  ValHrpdSessStartMsg;

/* Get Sprint PCS Mode message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;         /* Response routing information */
} PACKED_POSTFIX  ValGetAlertStatusMsgT;

typedef struct
{
   uint32 TimeId;
}ValMpcCallbackMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint32 IpAddr;
   uint16 IpPort;
} PACKED_POSTFIX ValGpsCTMpcCfgMsgT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsCTMpcCfgRspMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   SessionStatusE State;
} PACKED_POSTFIX  ValGpsSessStatusMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8 SIP_NAI[72];
   uint8 SIP_PASSWORD[16];
} PACKED_POSTFIX ValGpsCTMpcPasswrdUsernameCfgMsgT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
} PACKED_POSTFIX  ValGpsCTMpcPasswrdUsernameCfgRspMsgT;

typedef enum
{
   MODEM_FAILURE_1x = 0x01,
   MODEM_FAILURE_DO = 0x02,
   MODEM_FAILURE    = (MODEM_FAILURE_1x | MODEM_FAILURE_DO)
}ValGeneralModemFailureT;

/*-------------------------------------------------------------------------
 * Silent Logging message definitions
 *------------------------------------------------------------------------*/
#define VAL_SILENT_LOG_FILENAME_LEN   128

typedef enum
{
    VAL_SILENT_LOG_CMD_SUCCESS = 0,
    VAL_SILENT_LOG_FILE_OPEN_ERR,
    VAL_SILENT_LOG_FILE_WRITE_ERR
} ValSilentLogStatusT;

/* Silent Log Config Create cmd/resp */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT RspInfo;
    char       FileName [VAL_SILENT_LOG_FILENAME_LEN];
} PACKED_POSTFIX  ValSilentLogCfgCreateMsgT;

typedef PACKED_PREFIX struct
{
    ValSilentLogStatusT  Status;
} PACKED_POSTFIX  ValSilentLogCfgCreateRspMsgT;

/* Set Silent Log Parms cmd/resp:
 *     saves silent logging parameters in flash, takes affect at next power up.
 */
#define VAL_COUNT_NO_CHANGE  0xFFFF    /* Leave count as is */
#define VAL_COUNT_FOREVER    0xFFFE    /* Count not to decrement, log forever */

typedef enum
{
    VAL_PARM_NO_CHANGE,
    VAL_PARM_DISABLE,
    VAL_PARM_ENABLE
} ValParmChgT;

typedef enum
{
    LOG_OFF_STATE,         /* ETS connected, no more logging until next power up */
    LOG_INACTIVE_STATE,    /* No valid config file or count is zero */
    LOG_ACTIVE_STATE,      /* Logging enabled with valid config file and non-zero count */
    LOG_PAUSED_STATE       /* Logging was ACTIVE but was paused by user */
} ValSilentLogStateT;

typedef PACKED_PREFIX struct
{
    uint16       Count;       /* Use VAL_COUNT_NO_CHANGE if count to remain as is */
    ValParmChgT  Continuous;  /* Use VAL_PARM_NO_CHANGE if value to remain as it  */
    ValParmChgT  LogFaults;   /* Use VAL_PARM_NO_CHANGE if value to remain as it  */
} PACKED_POSTFIX  ValSilentLogParmsT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT         RspInfo;
    ValSilentLogParmsT Parms;
    char               FileName [VAL_SILENT_LOG_FILENAME_LEN];
} PACKED_POSTFIX  ValSilentLogSetParmsMsgT;

typedef PACKED_PREFIX struct
{
    ValSilentLogStatusT  Status;
} PACKED_POSTFIX  ValSilentLogSetParmsRspMsgT;

/* Get Silent Log Get Parms cmd/resp */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT RspInfo;
} PACKED_POSTFIX  ValSilentLogGetParmsMsgT;

typedef PACKED_PREFIX struct
{
    ValSilentLogStateT CurrState;
    ValSilentLogParmsT Parms;
    char               FileName [VAL_SILENT_LOG_FILENAME_LEN];
} PACKED_POSTFIX  ValSilentLogGetParmsRspMsgT;

/* Update Silent Log Parms without affecting Parms file in flash;
 * logging will start immediately.
 * More customized logging types can be added as needed.
 */
typedef enum
{
    VAL_PCM_DATA_LOGGING = 1
} ValSilentLogTypeT;

typedef PACKED_PREFIX struct
{
    ValSilentLogTypeT Type;
    bool         ClearActive; /* TRUE if active spies/traces are to be cleared */
    ValParmChgT  Continuous;  /* Use VAL_PARM_NO_CHANGE if value to remain as it  */
    ValParmChgT  LogFaults;   /* Use VAL_PARM_NO_CHANGE if value to remain as it  */
} PACKED_POSTFIX  ValSilentLogUpdateParmsMsgT;

/* Silent Log Get Data Info cmd/resp */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT RspInfo;
} PACKED_POSTFIX  ValSilentLogGetDataInfoMsgT;

typedef PACKED_PREFIX struct
{
    uint8  *BufP;
    uint32  UploadSizeWords;
    uint32  MaxSizeBytes;
    uint32  CurrIdx;
} PACKED_POSTFIX  ValSilentLogGetDataInfoRspMsgT;

/*------------------------------------------------------------------------
 * GPS on AP message definitions
 *------------------------------------------------------------------------*/

/* Defalut SVs Number */
#define  MAX_SV_NUM      (32)     /* It is depend on GPS solution */
#define  MAX_EPH_PRN_NUM (16)     /* Maximum PRNs in one Message according to IS-801-1 Spec */
#define  MAX_ALM_PRN_NUM (32)     /* Actually it is 64 and current PDE sent 7 PRNs to MS    */

#define GPS_MAX_NUM_SA_NAV_BYTES  128
#define GPS_MAX_NUM_SA_SV         2
#define GPS_MAX_NUM_SA_DATA_RECS  9

typedef enum
{
   TOW_AA,
   TOW_ONLY
} AssistDataTypeT;

/* Define for ToAP_MSG: VAL_AP_GPS_DEVICE_POWER_ON_REQ_MSG w/ CONFIG MSG */
typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   GPS_FIX_MODE FixMode;         /* Unkown:0, MSA:1, MSB:2, MSS:3, and Control Plane:4 */
   uint32   FixRateNumFixes;     /* A value of 1 means is interested in only one fix,  */
                                 /* A value > 1,multiple fixes with some time in btw the attempts */
   uint32   FixRateTimeBeFixes;  /* Time, in seconds, btw position fix attempts.       */
                                 /* A default of 30 seconds is used.                   */
   uint32   QoSHAccuracy;        /* Horizontal Accuracy, in meters, meaningful for MSB */
   uint32   QoSVAccuracy;        /* Vertical Accuracy, in meters, meaningful for MSB */
#ifdef MTK_CBP
   uint32   QoSPRAccuracy;       /* Pseudorange Accuracy, in meters, meaningful for MSA */
#endif
   uint32   QoSPerformance;      /* Performance response quality in terms of time, in seconds, meaningful for MSA & MSB  */
} PACKED_POSTFIX  ValGpsPowerOnMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsFakePowerOnMsgT;

typedef PACKED_PREFIX struct
{
   uint32 IsActive;
} PACKED_POSTFIX  ValGpsEmergencyCallIndMsgT;

#ifdef MTK_CBP  /*for mode switch optimization*/
typedef PACKED_PREFIX struct
{
   OperationModeT    OpMode;
   bool              Is1XPowerOn;
   bool              IsDOPowerOn;
} PACKED_POSTFIX  ValRatModeChgCnfMsgT;
#endif

typedef struct
{

   uint8 duration;
}ValGpsFreqAidReqT;



/* Define for ToAP_MSG: VAL_AP_GPS_FREQUENCY_AIDING_RSP_MSG */
typedef PACKED_PREFIX struct
{
   uint32   FrequencyDataType;  /* 00: Should not be used, */
                                /* 01: Absolute center freq of the ECLK (Nominal Freq + delta) */
                                /* 02: Delta from the nominal frequency                */
   uint32   AccuracyDataType;   /* 00: Should not be used, 01: in units of PPM         */
                                /* 02: in units of Hz      */
   uint32   OS_time;            /* OS Time [milliseconds]  */
   int32    Cal_ppb;            /* Clock Frequency Calibration value [ppb]     */
   double   Cal_RMS_ppb;        /* Frequency Calibration RMS  [ppb]            */
   double   Frequency;          /* in unit of Hz. ex, 19.6608Mhz => (19.6608 * 1000000)*/
   double   Accuracy;
} PACKED_POSTFIX  ValGpsFreqAidingMsgT;

/* Define for ToAP_MSG: VAL_AP_GPS_PRECISE_TIME_AIDING_RSP_MSG */
typedef PACKED_PREFIX struct
{
   uint8     ValidRefTime;        /* Not Available: 0, Available: 1*/
#if !(defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT))
   double   TOW;                 /* GPS Time of Week [seconds]    */
#else
    float    TOW;
#endif
   uint16   WeekNum;             /* GPS Week Number,              */
   uint32   OS_Time;             /* OS Time [milliseconds]        */
   uint32   Absolute_RMS_Acc;    /* Absolute Pulse RMS Accuracy [microseconds] */
   uint32   Relative_RMS_Acc;    /* Relative Pulse RMS Accuracy [nanoseconds]  */
} PACKED_POSTFIX  ValGpsFrameSyncMsgT;

/* Define for ToAP_MSG: VAL_AP_GPS_PSEUDORANGE_MSMT_REQ_MSG w/ AA Data */
typedef  PACKED_PREFIX struct
{
   uint8  SVID;
   int8  Doppler1;        /*  Doppler 1st order term  Hz/s)    */
   uint8  Dopp_Win;        /* Satellite Doppler Uncertainty     */
   uint8  SV_CodePh_int;   /* GPS Integer Code Phase since the last GPS bit edge */
   uint8  GPS_BitNum;      /*  GPS Bit Number  relative to GPS_TOW */
   uint8  SV_CodePh_Win;   /*  Code Phase Search Window         */
   uint8  Azimuth;         /* Satellite Azimuth   degrees       */
   uint8  Elevation;       /* Satellite Elevation  degrees      */
   uint16 SV_CodePh;       /* GPS Code Phase  0..1022 chips     */
   int16  Doppler0;        /* Doppler 0th order term ( Hz)      */
} PACKED_POSTFIX ValGpsAADataT;

typedef  PACKED_PREFIX struct
{
   uint32 AA_Ref_TOW;      /* Acq Assist Reference TOW */
   uint8  AA_Num;          /* Num of AA elements       */
   uint8  DopIncl;         /* Doppler 0, included or not, 1---doppler 0 included */
   uint8  AddDopIncl;      /* 1  add doppler included1          */
   uint8  Code_ph_incl;    /* 1 Code phase information included */
   uint8  Az_El_incl;      /* 1    Azimuth and elevation angle included.*/
   ValGpsAADataT   AA_Data[MAX_SV_NUM];     /*AA data array */
} PACKED_POSTFIX ValGpsAADataMsgT;


typedef PACKED_PREFIX struct
{
   uint16  DataRecSizeInBits;  /* size of each data rec in bits; range 0 to 1020 */  
   uint8   NavMsgBits[GPS_MAX_NUM_SA_NAV_BYTES];
   uint8   NumSvDr;
   uint8   SvNumArray[GPS_MAX_NUM_SA_SV];
} PACKED_POSTFIX ValGpsSADataT;

typedef PACKED_PREFIX struct          
{
   uint16	RefBitNum;
   uint8   	NumDataRecs;  /* num of data recs */   
   ValGpsSADataT	SADataRecArray[GPS_MAX_NUM_SA_DATA_RECS];
} PACKED_POSTFIX ValGpsSADataMsgT;

/* Define for ToCP_MSG: PSEUDORANGE_MSMT RSP MSG w/ Measurement Data */
typedef  PACKED_PREFIX struct
{
   uint8  SVID;            /* Range [1..32]                   */
   uint8  SV_CN0;          /* Satellite C/N0.                 */
   uint8  MultiPath_Ind;   /* Pseudorange Multipath Indicator */
   uint8  PS_Range_RMS_ER; /* Pseudorange RMS Error           */
   int16  PS_Dopp;         /* Satellite Doppler               */
   uint16 SV_Code_Ph_Wh;   /* Satellite code phase - whole chips */
   uint16 SV_Code_Ph_Fr;   /* SV Code Phase Fractional Chips  */
} PACKED_POSTFIX  ValGpsPRMeasDataT;

typedef PACKED_PREFIX struct
{
   uint8 prm_valid;        /*0--not valid, 1--valid*/
   uint32   Meas_TOW;      /* Measurement GPS Time of Week  */
   uint8    Meas_TOW_Unc;  /* Meas GPS Time of Week Uncertainty */
   uint8    Num_Meas;      /* Number of measurement 0-16    */
   ValGpsPRMeasDataT  MeasData[MAX_SV_NUM];
} PACKED_POSTFIX  ValGpsPRMeasMsgT;

/* Defined PDE's Location Response Structure for Control Plane. */
/* Note: For LAT/Long, Just bypass PDE's data to AP, NOT multiple 1M number */
typedef PACKED_PREFIX struct
{
   ValGpsSystemTimeT UTCTime;    /* Current System Date and Time */
   double   Latitude;            /* In units degree, computed (LAT * 180/2^25) degrees */
                                 /*   Range [-90..+90x(1-2^-24)]degrees */
                                 /*   positive angles north of the equator and negative angles south of the equator.*/
   double   Longitude;           /* In units degrees, computed (LONG * 360/2^26) degrees */
                                 /*   Range [-180 .. +180x(1-2^-25)] degrees */
                                 /*   Positive angles east of the Greenwich meridian and negative angles west */
   float    LocUncAng;           /* In units degrees, computed (ANG * 5.625) degrees */
                                 /*   Range [0..84.375] degrees. */
   float    LocUncAx;            /* In uints meters, Converted Position Table 4.2.4.2-6 */
   float    LocUncPe;            /* In uints meters, Converted Position Table 4.2.4.2-6 */
   uint8    FixType;             /* 0: For 2D Fix, 1: 3D fix */
   float    VelocityHor;         /* In units of meter/seconds, computed (VH x 0.25) meter/seconds */
                                 /*   Range [0..127.75] meter/seconds */
   float    Heading;             /* In units degrees, computed (Heading * (360/2^10)) */
                                 /*   Range [0..360x (1-2^-10)] degrees and a heading of zero is true north*/
   float    VelocityVer;         /* In units of meter/seconds, computed (VV x 0.5) meter/seconds */
                                 /*   Range [-64..+63.5] meter/seconds */
   int32    Height;              /* In units of meter, Binary value of the field conveys the hight plus 500m */
   float    LocUncVe;            /* In uints meters, Converted Position Table 4.2.4.2-6 */
} PACKED_POSTFIX  ValGpsCpLocRespMsgT;

/* This structure is for only ETS's Spy. */
typedef PACKED_PREFIX struct
{
   ValGpsSystemTimeT UTCTime;    /* Current System Date and Time */
   double   Latitude;            /* In units degree, computed (LAT * 180/2^25) degrees */
                                 /*   Range [-90..+90x(1-2^-24)]degrees */
                                 /*   positive angles north of the equator and negative angles south of the equator.*/
   double   Longitude;           /* In units degrees, computed (LONG * 360/2^26) degrees */
                                 /*   Range [-180 .. +180x(1-2^-25)] degrees */
                                 /*   Positive angles east of the Greenwich meridian and negative angles west */
   double   LocUncAng;           /* In units degrees, computed (ANG * 5.625) degrees */
                                 /*   Range [0..84.375] degrees. */
   double   LocUncAx;            /* In uints meters, Converted Position Table 4.2.4.2-6 */
   double   LocUncPe;            /* In uints meters, Converted Position Table 4.2.4.2-6 */
   uint8    FixType;             /* 0: For 2D Fix, 1: 3D fix */
   double   VelocityHor;         /* In units of meter/seconds, computed (VH x 0.25) meter/seconds */
                                 /*   Range [0..127.75] meter/seconds */
   double   Heading;             /* In units degrees, computed (Heading * (360/2^10)) */
                                 /*   Range [0..360x (1-2^-10)] degrees and a heading of zero is true north*/
   double   VelocityVer;         /* In units of meter/seconds, computed (VV x 0.5) meter/seconds */
                                 /*   Range [-64..+63.5] meter/seconds */
   int32    Height;              /* In units of meter, Binary value of the field conveys the hight plus 500m */
   double   LocUncVe;            /* In uints meters, Converted Position Table 4.2.4.2-6 */
} PACKED_POSTFIX  ValGpsETSCpLocRespMsgT;

/* This structure is for Reference Location Response. */
typedef PACKED_PREFIX struct
{
   uint8    ValidRefLoc;         /* 0: Not Valid, 1: Valid TimeZone only, 2: Valid Time Zone and BS location */
   float    TimeZoneLat;         /* Ex)Default location is Kansas and (39.164253,-94.544503) */
   float    TimeZoneLong;        /* FYI, Time Zone Range is [-16h ~ +15.5h]  */
   uint16   SID;                 /* System ID and Range [0..32767] */
   uint16   NID;                 /* Network ID and and Range [0..65535] */
   uint16   BaseID;              /* Base Station ID and Range [0..65535] */
   float    BaseLat;             /* WGS84 Geodetic Latitude [degrees],latitude from base last registered on */
   float    BaseLong;            /* WGS84 Geodetic Longitude[degrees],Longitude from base last registered on */
} PACKED_POSTFIX  ValGpsRefLocRespMsgT;

typedef PACKED_PREFIX struct
{
   uint8    SVID;          /* Range [1..32]                             */
   int8     Af2;           /* Apparent satellite clock correction af2.  */
   uint8    IODE;          /* Issue of data                             */
   uint16   TOC;           /* Clock data reference time.                */
   uint16   TOE;           /* ephemeris reference time.                 */
   int16    Af1;           /* Apparent satellite clock correction af1.  */
   int16    Delta_n ;      /* Mean motion difference from the computed value. */
   int16    IDOT;          /* Rate of inclination angle, If negative number and Masked with 0xE000         */
   int16    C_RS;          /* Amplitude of the sine harmonic correction term to the orbit radius           */
   int16    C_RC;          /* Amplitude of the cosine harmonic correction term to the orbit radius.        */
   int16    C_US;          /* Amplitude of the sine harmonic correction term to the argument of latitude.  */
   int16    C_UC;          /* Amplitude of the cosine harmonic correction term to the argument of latitude.*/
   int16    C_IS;          /* Amplitude of the sine harmonic correction term to the angle of inclination.  */
   int16    C_IC;          /* Amplitude of the cosine harmonic correction term to the angle of inclination.*/
   int32    Af0;           /* Apparent satellite clock correction af0,If negative number and Masked with 0xFFE00000*/
   int32    M0;            /* Mean anomaly at the reference time.       */
   uint32   A_SQRT;        /* Square root of the semi-major axis.       */
   uint32   Eccentricity;  /* Eccentricity.                             */
   int32    I_angle;       /* Inclination angle at the reference time.  */
   int32    Omega_0;       /* Longitude of ascending node of orbit plane at weekly epoch. */
   int32    Omega;         /* Argument of perigee.                      */
   int32    OmegaDOT;      /* Rate of right ascension,If negative number and Masked with 0xFF000000  */
} PACKED_POSTFIX ValGpsEphPrnDataT;

typedef PACKED_PREFIX struct
{
   uint8    TotalPart;     /* Range [0.. 31]        */
   uint8    PartNum;       /* Range [0..TotalParts] */
   uint8    NumSV;         /* Range [1..32]         */
   ValGpsEphPrnDataT EPHData[MAX_EPH_PRN_NUM]; /* Set Max 16 PRNs, 3 + (57 * 16) = 915 Bytes */
} PACKED_POSTFIX ValGpsEphPrnMsgT;

typedef PACKED_PREFIX struct
{
   uint8    SVID;          /* Range [1..32]                             */
   int16    Delta_I;       /* Correction to inclination.                */
   int16    af0;           /* Apparent satellite clock correction af0.If negative number and Masked with 0xF800 */
   int16    af1;           /* Apparent satellite clock correction af1.If negative number and Masked with 0xF800 */
   int16    OmegaDOT;      /* Rate of right ascension. */
   uint16   Eccentricity;  /* Eccentricity.                            */
   uint32   A_SQRT;        /* Square root of the semi-major axis       */
   int32    Omega_0;       /* Longitude of ascending node of orbit plane.If negative number and Masked with 0xFF000000*/
   int32    Omega;         /* Argument of perigee. If negative number and Masked with 0xFF000000 */
   int32    M0;            /* Mean anomaly at reference time.If negative number and Masked with 0xFF000000.*/
} PACKED_POSTFIX ValGpsAlmPrnDataT;

typedef PACKED_PREFIX struct
{
   uint8    TotalPart;  /* Range [0.. 31]       */
   uint8    PartNum;    /* Range [0..TotalParts]*/
   uint8    NumSV;      /* Range [1..32]        */
   uint8    Week_Num;   /* GPS week number. Range [0..255] */
   uint8    TOA;        /* Time of almanac. in units of 4096s and Range [0..602112] */
   ValGpsAlmPrnDataT ALMData[MAX_ALM_PRN_NUM];  /* Set Max 32 PRNs, 5 + (27 * 32)= 869 bytes */
} PACKED_POSTFIX ValGpsAlmPrnMsgT;

typedef  PACKED_PREFIX  struct
{
   uint8    AbPar_Incl; /* Inclusion of the alpha and beta parameters. 0 or 1 */
   int8     Alpha0;     /* Ionospheric correction parameter. */
   int8     Alpha1;     /* Ionospheric correction parameter. */
   int8     Alpha2;     /* Ionospheric correction parameter. */
   int8     Alpha3;     /* Ionospheric correction parameter. */
   int8     Beta0;      /* Ionospheric correction parameter. */
   int8     Beta1;      /* Ionospheric correction parameter. */
   int8     Beta2;      /* Ionospheric correction parameter. */
   int8     Beta3;      /* Ionospheric correction parameter. */
   uint32   Z_Count;    /* Z-Count                           */
} PACKED_POSTFIX  ValGpsIonMsgT;

/* Decoded PDE's data for AP based on IS-801-1 Spec */
typedef PACKED_PREFIX struct
{
   uint8    Velocity_Incl; /* Velocity information included   */
   uint8    Height_Incl;   /* 1: include Heigth and Loc_Unc_V */
   uint8    Clock_incl;    /* 1: include Clock Information    */
   uint8    FixType;       /* 0: For 2D Fix, 1: 3D fix        */
   int16    Loc_Unc_ang;   /* in units Degrees                */
   int32    Clock_bias;    /*                                 */
   int16    Clock_drift;   /*                                 */
   float    Latitude;      /* in units Degrees north of equator */
   float    Longitude;     /* in uints Degrees west of Greenwich meridian */
   float    Loc_Unc_A;     /* in units Meters                 */
   float    Loc_Unc_P;     /* in units Meters                 */
   float    Velocity_Hor;  /* Horizontal velocity magnitude.  */
   float    Heading;       /*                                 */
   float    Height;        /* in units Meters                 */
   float    Vvelocity;     /* Vertical velocity.              */
   float    Loc_Unc_V;     /* Standard deviation of vertical error for position uncertainty */
} PACKED_POSTFIX  ValGpsLocMsgT;

typedef PACKED_PREFIX struct
{
   uint8  Ref_Pos_Req;     /*1: need assist, 0:doesn't need  */
   uint8  Ion_Req;         /*1: need assist, 0:doesn't need  */
   uint8  Alm_Req;         /*1: need assist, 0:doesn't need  */
   uint8  Eph_Req;         /*1: need assist, 0:doesn't need  */
} PACKED_POSTFIX  ValGpsAssistReqT;

typedef struct
{
   uint16 datalen;
   uint16 chanId;          /* IopDataChannel */
} ValRpcRxMsgHeadT;

typedef PACKED_PREFIX struct
{
   uint16  PilotPN;
   /* PILOT_PN_PHASE sent in PROVIDE PILOT PHASE MEASUREMENTS */
   int32   PnPhase;
   /* PILOT_STRENGTH sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16  Strength;
   /* RMS_ERR_PHASE sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint8 RmsErrPhase;
} PACKED_POSTFIX  ValAfltPilotMeasurementT;

typedef struct
{
   uint8 SeqNum;
   /* TIME_REF_MS sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16 TimeRefMs;
   bool OffsetIncl;
   int16 MobOffset;
   /* REF_PN sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16 RefPN;
   /* REF_PILOT_STRENGTH sent in PROVIDE PILOT PHASE MEASUREMENTS */
   uint16 RefPilotStrength;

   uint8 BAND_CLASS;
   uint16 CDMA_FREQ;
   uint16 BASE_ID;
   uint16 SID;
   uint16 NID;
   uint16 TOTAL_RX_PWR;

   /* NUM_PILOTS_P sent in PROVIDE PILOT PHASE MEASUREMENTS */
   /* actual number of pilot measurements in AFLTPilotMeasurement */
   uint8  NumPilots;
   ValAfltPilotMeasurementT PosAFLT[SYS_MAX_AFLT_LIST_PILOTS];
} ValPswPosAfltMeasurementMsgT;


typedef enum
{
  UNKOWN_MODE,
  NORMAL_GPS_MODE,
  VGTT,
  THIRD_PARTY_LBS,
  AT_GPS
}ValGpsModeT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT rspInfo;
   uint8 Mode;
} PACKED_POSTFIX  ValGpsTestModeSetT;

typedef PACKED_PREFIX struct
{
   ValGpsFixOperationStatusT Status;
} PACKED_POSTFIX  ValGpsTestModeSetRspMsgT;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8        Band;
   int32      RFDelay1X;
   int32      RFDelayDO;

} PACKED_POSTFIX ValGpsRfDelayMsgT;

typedef PACKED_PREFIX struct
{
   ValGpsStatusT Status;
   uint8        Band;
   int32      RFDelay1X;
   int32      RFDelayDO;
} PACKED_POSTFIX  ValGpsRfDelayRspMsgT;

#ifdef MTK_PLT_ON_PC
typedef struct
{
   c2k_sbp_id_enum sbp_id;
   c2k_sbp_status_enum feature;
} ValConfigSbpInfoMsgT;
#endif

typedef enum
{
   BYPASS_USER_VERIFICATION,
   WAIT_USER_VERIFICATION,
   NOTIFY_USER_BYPASS_VERFICATION
}ValMpcNotifIndE;

typedef enum
{
   START_IS801_SESSION,
   SID_NID_RETURN,
   CACHED_POS_USED,
   RESERVE_POS_TECH_IND
}ValMpcPostionTechIndE;

typedef enum
{
  VAL_MPC_MSA,
  VAL_MPC_MSB,
  VAL_MPC_MSA_PREFED_MSB_ALLOWD,
  VAL_MPC_MSB_PREFED_MSA_ALLOWD,
  VAL_MPC_MODE_RESERVE
}ValMpcIs801ModeE;

typedef struct
{
   uint8 Length;
   ValMpcNotifIndE NotificationAndVerificationInd;
   ValMpcPostionTechIndE PositionTechInd;
   uint8 PositionQosInc;
   uint8 PositionQos;
   uint16 NumOfFixes;
   uint16 TimeBtwFixes;
   ValMpcIs801ModeE Is801PositionMode;
   uint8 CorrelationId;
   uint8 RequestIDEnc;
   uint8 RequestIDLen;
   char RequestID[256];
}ValMpcPositionRequestMsgT;

typedef struct
{
   uint8 Length;
   ValMpcNotifIndE NotificationAndVerificationInd;
   ValMpcPostionTechIndE PositionTechInd;

   uint8 PositionQosInc;
   uint8 PositionQos;
   uint16 NumOfFixes;
   uint16 TimeBtwFixes;
   ValMpcIs801ModeE Is801PositionMode;
   uint8 CorrelationId;
   char action; /* 0: agree; 1 cancel; 2 timeout */
}ValMpcPositionRequestParaRespT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsStopAckMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
} PACKED_POSTFIX  ValGpsStopReqMsgT;

typedef PACKED_PREFIX struct
{
   uint32 InstanceID;
   uint8 Status;
} PACKED_POSTFIX ValGpsCancelIndMsgT;

typedef struct
{
   uint32 sess_id;
   uint8 Length;
   ValMpcNotifIndE NotificationAndVerificationInd;
   ValMpcPostionTechIndE PositionTechInd;

   uint8 PositionQosInc;
   uint8 PositionQos;
   uint16 NumOfFixes;
   uint16 TimeBtwFixes;
   ValMpcIs801ModeE Is801PositionMode;
   uint8 CorrelationId;
   char action; /* 0: agree; 1 cancel; 2 timeout */
}ValIPCMpcPositionRequestParaRespT;

void ValMpc3rdPartyParamStore(ValMpcPositionRequestParaRespT *Resp, uint32 Inst);
void ValMpcUserCancelLbs(uint8 CorrelationId);

typedef  struct
{
   uint32 TimerId;
}ValCpSmsRegTimerExpiredMsgT;

typedef enum
{
  PowerUpReg,
  MoSmsReg,
  MoVoiceReg,
  MtSmsReg,
  MtVoiceReg,
  MoDataReg,
  NwInitReg,
  UtkMenuSelReg,
  UtkMenuMeidReg,
  NUM_MAX_REGTYPE
}ValSmsAutoRegTypeT;

typedef struct
{
   ValSmsAutoRegTypeT Trigger;

}ValCpSmsRegTriggerReqMsgT;
 /*------------------------------------------------------------------------
 * NST List Mode prototypes
 *------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8 count;  /* Total of list */
   uint8 index[MAX_NUM_LISTS];  /* Index of list */
   uint8 protocol[MAX_NUM_LISTS];
   uint8 offset[MAX_NUM_LISTS];   /* Num of frame offset */
   SysCdmaBandT band[MAX_NUM_LISTS];
   uint16 channel[MAX_NUM_LISTS];
   uint8 codeChan[MAX_NUM_LISTS];
   uint8 radioConfig[MAX_NUM_LISTS];
   uint16 numFrames[MAX_NUM_LISTS];
   uint8 pwrCtrlMode[MAX_NUM_LISTS];
   int16 txPwr[MAX_NUM_LISTS];
} PACKED_POSTFIX  ValNstListSetReqMsgT;

typedef PACKED_PREFIX struct
{
   uint8 count;  /* Total of list */
   uint8 index[MAX_NUM_LISTS];  /* Index of list */
   SysCdmaBandT band[MAX_NUM_LISTS];
   uint16 channel[MAX_NUM_LISTS];
   uint16 badFrames[MAX_NUM_LISTS];
   uint16 totalFrames[MAX_NUM_LISTS];
} PACKED_POSTFIX  ValNstListSetRspMsgT;

typedef PACKED_PREFIX struct
{
   UINT16 len;
   UINT8  *data;
} PACKED_POSTFIX  ValCsfbGcsnaULMsgT;

#ifdef MTK_DEV_C2K_IRAT
typedef struct
{
   IratSIB8MeasResultDataT rsp;

} ValCsfbMeasRspMsgT;

#define MAX_EUTRA_FREQS_NUM         8

typedef PACKED_PREFIX struct
{
   UINT8    NumEUTRAFrequencies;
   UINT16   EARFCN[MAX_EUTRA_FREQS_NUM];
} PACKED_POSTFIX  ValClcNbrFreqsRptMsgT;
typedef PACKED_PREFIX struct
{
  uint8 EfunState;
} PACKED_POSTFIX ValEfunStateIndMsgT;

typedef PACKED_PREFIX struct
{
  uint8 src_id;
  c2k_1x_loop_back_call_mode_enum operation_mode;
} PACKED_POSTFIX ValMd11xLoopBackCallReqMsgT;

typedef PACKED_PREFIX struct
{
  ValIratModemStatusT     MdStatus;
  uim_access_option_enum  UimAccessOption;
} PACKED_POSTFIX ValUimEmdstatusUpdateIndMsgT;

typedef PACKED_PREFIX struct
{
  ValEmdstatusFlowVersionT EmdstatusFlowVersion;
}PACKED_POSTFIX ValEmdstatusFlowVersionIndMsgT;

typedef PACKED_PREFIX struct
{
  uint8 cardEsnMeOld[8];
  uint8 cardEsnMeNew[8];
}PACKED_POSTFIX ValUimPreStoredCardEsnmeIndMsgT;
typedef PACKED_PREFIX struct
{
  UINT8 is_ims_registred;
}PACKED_POSTFIX ValImsRegStatusUpdateIndMsgT;

typedef PACKED_PREFIX struct
{
bool     locked;                       /* TRUE lock, FALSE unlock */
} PACKED_POSTFIX  ValUimOp01StatusMsgT;
#endif /* MTK_DEV_C2K_IRAT */

typedef PACKED_PREFIX struct
{
   UINT16 len;
   UINT8 *data;
} PACKED_POSTFIX  ValCsfbHoPrepXferMsgT;

typedef enum
{
   VAL_IRAT1X_CSFB_PARM_RSP_ACK,
   VAL_IRAT1X_CSFB_SIB8_PARM_ACK,
   VAL_IRAT1X_CSFB_GCSNA_DL_ACK,
   VAL_IRAT1X_CSFB_MEAS_REQ_ACK,
   VAL_IRAT1X_CSFB_CONN_REL_ACK,
   VAL_IRAT1X_CSFB_HO_EUTRA_PREP_ACK,
   VAL_IRAT1X_CSFB_MOB_FROM_EUTRA_ACK,

   VAL_IRAT1X_EVENT_MAX
} ValIrat1xCsfbEventIdT;

typedef PACKED_PREFIX struct
{
   ValIrat1xCsfbEventIdT event;
} PACKED_POSTFIX  ValCsfbEventMsgT;

#ifdef MTK_PLT_ON_PC
#define MAX_IP_TEST_DATA_LEN    (AT_MAX_AT_CMD_LEN)

typedef PACKED_PREFIX struct
{
   kal_uint16 size;
   /* the 1st byte in data is Iopchannel */
   kal_uint8  data[MAX_IP_TEST_DATA_LEN];
} PACKED_POSTFIX  ValIpDataSendReqMsgT;

typedef PACKED_PREFIX struct
{
   uint16 status;
   uint16 len;
} PACKED_POSTFIX  ValIpDataSendCnfMsgT;

typedef PACKED_PREFIX struct
{
   uint8 *data;
   uint16 size;
} PACKED_POSTFIX  ValIpDataRecvIndMsgT;

typedef PACKED_PREFIX struct
{
   uint32  SrcAddr;
   uint32  DstAddress;
   uint8   Data[MAX_IP_TEST_DATA_LEN];   /* point to the first byte of data.    */
   uint16  Size;    /* The size of data in byte.           */
} PACKED_POSTFIX  HlpIpDataSendReqMsgT;
#endif /* MTK_PLT_ON_PC */

#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
typedef PACKED_PREFIX struct
{
    HlpValPcmtModeT mode;
    bool            result;
    uint32          pcmtEhrpdValue;
    uint32          pcmtIratValue;
} PACKED_POSTFIX ValHlpPcmtParaRspMsgT;

typedef enum
{
    VAL_EHRPD_OFF = 0,
    VAL_EHRPD_ON,
    VAL_EHRPD_MODE_NUM
}ValEhrpdModeE;
#endif
#ifdef MTK_DEV_ENGINEER_MODE
typedef enum
{
    RF_TST_STOP_TRANSMIT = 0,
    RF_TST_START_TRANSMIT = 1
}RfTstControlActionT;

typedef enum
{
    RF_TST_BAND_CLASS_0 = 0,
    RF_TST_BAND_CLASS_1 = 1,
    RF_TST_BAND_CLASS_2 = 2,
    RF_TST_BAND_CLASS_3 = 3,
    RF_TST_BAND_CLASS_4 = 4,
    RF_TST_BAND_CLASS_5 = 5,
    RF_TST_BAND_CLASS_6 = 6,
    RF_TST_BAND_CLASS_7 = 7,
    RF_TST_BAND_CLASS_8 = 8,
    RF_TST_BAND_CLASS_9 = 9,
    RF_TST_BAND_CLASS_10 = 10,
    RF_TST_BAND_CLASS_11 = 11,
    RF_TST_BAND_CLASS_12 = 12,
    RF_TST_BAND_CLASS_13 = 13,
    RF_TST_BAND_CLASS_14 = 14,
    RF_TST_BAND_CLASS_15 = 15
}RfTstControlBandT;

typedef enum
{
    RF_TST_1XRTT = 0,
    RF_TST_EVDO = 1,
    RF_TST_NONE
}RfTstControlModulationT;

typedef enum
{
    ECTM_MODE_NONE = 0,
    ECTM_MODE_SPIRENT = 1
}EctmMode;

typedef enum
{
    VAL_DISABLE_FORCE_TX_ANTENNA = 0,
    VAL_ENABLE_FORCE_TX_ANTENNA,
    VAL_FORCE_TX_ANTENNA_INVALID_MODE = 255
}ValForceTxAntModeT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
    ECLSC_ENABLE_MODE = 0,
    ECLSC_DISABLE_MODE = 1
}EclscMode;
#endif

/* move here to pass intercross header file invoke which is really bad... */
typedef PACKED_PREFIX struct
 {
    RfTstControlActionT  Action;
    uint8                Channel;
    RfTstControlBandT    Band;
    int8                 PowerLevel;
 } PACKED_POSTFIX RcpRfTstPowerControlMsgT;
#endif

#ifdef MTK_DEV_GPSONE_ON_LTE
typedef enum gps_rpc_rat_mode_t {
    GPSRPC_RAT_C2K = 0,
    GPSRPC_RAT_LTE = 1,
	GPSRPC_RAT_MODE_UNKNOWN = 2
}gps_rpc_rat_mode_t;

typedef enum
{
    STATUS_IE_SOCKET_FAILED     =  7,        /* Socket created failed or connection failed*/
    STATUS_IE_CLOSEPENDINGWAIT  =  6,        /* TCP close by peers, wait for user */
    STATUS_IE_CLOSEPENDING      =  5,        /* TCP graceful close in progress  */
    STATUS_IE_RECVMOREDATA      =  4,        /* more UDP or raw IP data         */
    STATUS_IE_NODATA            =  3,        /* no data available for receive   */
    STATUS_IE_CONNECTPENDING    =  2,        /* TCP connect attempt in progress */
    STATUS_IE_LISTENPENDING     =  1,        /* listening for remote connect    */
    STATUS_IE_SUCCESS           =  0,        /* request successful              */
    STATUS_IE_INVALIDRQST       = -1,        /* invalid or unknown request      */
    STATUS_IE_INVALIDSAP        = -2,        /* invalid service access point    */
    STATUS_IE_INVALIDPORT       = -4,        /* invalid listen or connect port  */
    STATUS_IE_INVALIDADDR       = -5,        /* invalid connect host address    */
    STATUS_IE_NOMOREMBUFS       = -6,        /* no mbufs available              */
    STATUS_IE_NOMORETCBS        = -7,        /* no tcbs available               */
    STATUS_IE_NOLOCALADDR       = -8,        /* local host address not set      */
    STATUS_IE_INVALIDSIZE       = -9,        /* invalid send or receive size    */
    STATUS_IE_INVALIDDATA       = -10,       /* invalid request field           */
    STATUS_IE_INVALIDOPT        = -11,       /* option incorrectly specified    */
    STATUS_IE_INVALIDFLAGS      = -12,       /* invalid send/sento flags        */
    STATUS_IE_INVALIDSTATE      = -13,       /* invalid TCP state               */
    STATUS_IE_TCP_TIME_OUT         = -14,       /* TCP connecting time out         */
    STATUS_IE_TCP_RESET            = -15,       /* TCP reset by remote             */
    STATUS_IE_TCP_ABORT            = -16,       /* TCP reset by local              */
    STATUS_IE_TCP_DISCNT_NORMAL    = -17        /* TCP Disconnect normal           */
} GpsSocketStatusT;

#define MAX_TCP_DATA_SIZE (1500/4)

typedef enum{
    service_tcp = 0,
    service_udp = 1,
    service_unknown = 2,
    service_type_max = 0x10000000
}SocketServiceType;
typedef struct
{
  uint32   IpAddress;    /* 32-bit IP address. */
  uint16   PortNumber;   /* UDP port number.   */
} ValSocketAddrT;

typedef struct
{
    int16             SocketId;/* input socket Id, will be used in the response message */
    SocketServiceType TcpType;
    ValSocketAddrT    DestAddr;
}GpsSocketCreateMsgT;


typedef struct
{
    int32      SocketHandle;
    uint16     Size;
    //uint8      *DataP;
    uint32      Data[MAX_TCP_DATA_SIZE];
}GpsTcpbSendDataMsgT;




typedef struct
{
    int32           SocketHandle;
    GpsSocketStatusT Status;
} GpsTcpbRecvRspMsgT;
typedef struct
{
    int32           SocketHandle;
    bool            Graceful;
}GpsSocketCloseMsgT;




#endif
#ifdef MTK_DEV_C2K_IRAT
typedef struct
{
  uint32 Id;
}ValUtkTimerExpiryMsgT;
#endif
#ifdef MTK_CBP
typedef enum
{
    ENWINFO_CALL_DROP = 402,
    ENWINFO_SMS_FAIL = 403
}EnwinfoEventE;

typedef enum
{
    EVOCD_VOICE_SPEECH_CODEC_NONE_V02       = 0,
    EVODE_VOICE_SPEECH_CODEC_QCELP13K_V02   = 1,
    EVODE_VOICE_SPEECH_CODEC_EVRC_V02       = 2,
    EVODE_VOICE_SPEECH_CODEC_EVRC_B_V02     = 3,
    EVODE_VOICE_SPEECH_CODEC_EVRC_WB_V02    = 4,
    EVODE_VOICE_SPEECH_CODEC_EVRC_NW_V02    = 5,
    EVODE_VOICE_SPEECH_CODEC_AMR_NB_V02     = 6,
    EVODE_VOICE_SPEECH_CODEC_AMR_WB_V02     = 7,
    EVODE_VOICE_SPEECH_CODEC_GSM_EFR_V02    = 8,
    EVODE_VOICE_SPEECH_CODEC_GSM_FR_V02     = 9,
    EVODE_VOICE_SPEECH_CODEC_GSM_HR_V02     = 10,
    EVODE_VOICE_SPEECH_CODEC_INVALID
}EvocdCodec;

typedef struct
{
    uint16 Mcc;
    uint16 SidMin;
    uint16 SidMax;
}MccSidT;
#ifdef MTK_GPS_SYNC_DEV
typedef struct
{
    double TOW;
    uint16 WeekNum;
	uint8 FreqBiasValid;
    int32 FreqBias;
}ValGpsTimeIndMsgT;
#endif

typedef struct
{
    bool  DomDataRoaming_enabled; /* true means enable data roaming, attach */
    bool  IntlDataRoaming_enabled;
}ValDataRoamingChgMsgT;


typedef enum
{
   VAL_GPS_SESS_INIT_STATE,
   VAL_GPS_SESS_LBS_HASH_STATE,
   VAL_GPS_SESS_SEC_REG_STATE,
   VAL_GPS_SESS_IDLE_STATE,
   VAL_GPS_SESS_IDLE_SSD_AUTH_CHAN_STATE,
   VAL_GPS_MPC_OPENNING_STATE,
   VAL_GPS_SESS_SUSPEND_STATE,
   VAL_GPS_SESS_OPEN_STATE,
   VAL_GPS_SESS_OPEN_SDD_AUTH_CHAN_STATE,
   VAL_GPS_SESS_OPEN_MSB_MPC_OPENNING_STATE,
   VAL_GPS_SESS_STATE_NUM
} ValGpsSessStateT;
typedef enum
{
   RAND_PERIOD_EXP,
   LOC_REQ,
} ValGpsAuthChallResultT;
typedef struct {
   uint8  SecDataId;
   uint8  SecDataLen;
   uint8  SecData[GPS_MAX_SECURITY_DATA]; /*Rand1*/
} GPS_Security;
typedef struct {
   uint32 EncryptAlg;  /*GPS_ENCRYPT_AES_128_LAT_LONG*/
   ValGpsSystemTimeT UTCTime;
   uint8 EncDataLen;
   uint8 EncData[GPS_MAX_SECURITY_DATA];
   uint8 AesKey[GPS_MAX_SECURITY_DATA];
} GPS_Encrypt; /*Rand2*/

typedef enum
{
   VAL_GPS_MSS_IDLE,
   VAL_GPS_MSS_STARTED,
   VAL_GPS_MSS_DONE
} ValGpsMssStateT;

typedef enum
{
   VAL_GPS_INACTIVE,
   VAL_GPS_OPENNING,
   VAL_GPS_OPEN
}ValSessBlkStateE;


typedef struct ValGpsSessCtlBlkT
{
    UINT32 InstanceID;

    ValGpsFixModeT FixMode;
    UINT32 NumFixes;
    UINT32 TimeBFixes;

    uint32 HorizontalAccuracy;
    uint32 VerticalAccuracy;
#ifdef MTK_CBP
    uint32 PseudorangeAccuracy;
    bool   OnCTNtwk;
    ValGpsSecOperE  SecOp;
#ifdef MTK_DEV_GPSONE_ON_LTE
    int32  RatMode;
#endif
#endif
    uint32 Performance;

    ValGpsSessStateT GpsSessState;

    ExeTimerT GpsRand1TimerCb;
    uint32 RandPeriod;
    ValGpsAuthChallResultT AuthResult;

    GPS_Security GpsSecurity;
    GPS_Encrypt  GpsEncrypt;

    ValGpsMssStateT MssState;
    uint8 MssDataRcvFlag;
    bool  bDataReadReady;
    ValLocRespMsgT LocRespData;
    uint8 LocEncryPhase;
    ValGpsSystemTimeT UTCTime;

    uint16 NmeaSpyFlag;
    uint32 NumberOfPositions;

    ValGpsReadOffsetInfoMsgT ReadOffsetInfoMsg;
    bool bReadOffsetInfoPending;

    SessionStatusE State;
    ValSessBlkStateE Lbsstate;

    uint8 UserId; /*0: default; 1: RPC; 2: AT; 3: VGTT: 4: 3rd party; 5: exception*/
    uint8 CancelId;

#if ((defined SYS_OPTION_RPC) && (defined SYS_OPTION_GPS_RPC))
    bool bAPInit;
#endif
    uint8 StopReason;
} ValGpsSessCtlBlkT;

extern ValGpsSessCtlBlkT* ValGpsGetFreeSessCtlBlk(uint32 InstanceID);

typedef enum {
  VAL_POWER_OFF_BY_CPOF,
  VAL_POWER_OFF_BY_EPOF,
  VAL_POWER_OFF_BY_OTHERS,
}ValPowerOffModeT;

#endif

#ifdef __CARRIER_RESTRICTION__
/* num of valid sets */
typedef struct {
    kal_uint8 num;
} ValUmlCategoryMetaT;

/* General lock structure*/
typedef struct
{
    ValUmlCategoryMetaT cat[5];  /* 5 VAL_UML_SUPPORT_CAT_SIZE */
    uint8   code_cat_n[5*3];       /* 3* 5 VAL_UML_CFG_CAT_NET_SIZE */
    uint8   code_cat_spn[5*23];     /* 23 * 5 VAL_UML_CFG_CAT_SPN_SIZE */
    uint8   code_cat_imsi_prefix[5*4]; /* 4 * 5 VAL_UML_CFG_CAT_INSI_PREFIX_SIZE */
    uint8   code_cat_gid1[5*4];   /* 4 * 5  VAL_UML_CFG_CAT_GID1_SIZE */
    uint8   code_cat_gid2[5*4];   /* 4 * 5 VAL_UML_CFG_CAT_GID2_SIZE*/
}ValUmlCategoryListT;   /* 195 bytes */

typedef PACKED_PREFIX struct
{
    uint8   slot_id;
    uint8   carrier_rest_status;  /* 0 - READY 1 - CARD RESTRICTED 2 -CARD REBOOT */
}PACKED_POSTFIX ValCarrierRestrictionStatusIndMsgT;

typedef PACKED_PREFIX struct
{
    uint8   slot_id;
    ValUmlCategoryListT   black_list;
    ValUmlCategoryListT   white_list;
    uint8   allow_all;  /* 0 - Not allow all, 1 - Allow all sim*/
    uint8   carrier_rest_state; /* 0 - Enabled 1 - Disabled */
    uint8   multi_sim_policy;
    uint8   allowed_carriers_prioritized;
}ValCarrierRestrictionSyncIndMsgT;

typedef PACKED_PREFIX struct
{
    uint8   slot_id;
} PACKED_POSTFIX  ValUimErrorIndMsgT;
#endif

 /*------------------------------------------------------------------------
 * Global function prototypes
 *------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif

void   ValGetcpStatus( void );
int8   GetValStatus( void );
void   ValGetDebugInfo( bool isAtCmd );
RegIdT ValRegister( const ValEventHandlerT* EventHandlerP,
                    ValEventFunc      CallBack );
void   ValUnRegister( const ValEventHandlerT* EventHandlerP,
                      RegIdT            RegId );
void   ValProcessEvent( const ValEventHandlerT* EventHandlerP,
                        uint32            Event,
                        void*             EventDataP );
void GetUimSPName(void * MsgP);
#ifdef MTK_CBP
BOOL ValGetRegResumeFlag(void);
#endif

/*-----------------------------------------------------------------
 *  valmisc.c interface
 *----------------------------------------------------------------*/
void   ValMiscPowerDown( ValPowerDownIdT Signal, uint32 ValMsgId );
RegIdT ValMiscRegister( ValEventFunc CallBack );
void   ValMiscUnregister( RegIdT RegId );
void   ValMiscNamRequest( void );
void   ValUserInfoControl( ValUserInfoCmdT CmdId );
RegIdT ValVrecRegister( ValEventFunc EventFuncP );
bool   ValGetEmergencyModeTestingMsg( void );
void   ValGetFirmwareVersion (void);

/*-----------------------------------------------------------------
 *  valfsimsg.c interface
 *----------------------------------------------------------------*/
RegIdT ValFsiRegister(ValEventFunc EventFuncP);

/*-----------------------------------------------------------------
 *  val interfaces to HWD (valmisc.c)
 *----------------------------------------------------------------*/
void ValPwrOffBoard( void );
void* ValMalloc( uint32 size );
void* ValMallocNoHalt( uint32 size );
void* ValMallocNoHaltProtected (uint32 size);
void ValFree(void* Ptr);
uint32 ValGetMemPoolMinFree(ValMemPoolTypeT MemPoolId);
uint32 ValGetAvailableSpace(ValMemPoolTypeT PoolType);
uint32 ValMemoryNoHaltPoolAddrGet(void);
uint32 ValMemoryPoolSizeGet(ValMemPoolTypeT PoolId);
void ValTestModeStartRefurbishAging (SysCdmaBandT Band, uint16 Channel);
void ValTestModeStopRefurbishAging (void);
void ValTestModeRefurbishAgingChangeTxPwr (bool Increment);
void ValDispGetLcdDeviceInfoTest(ValDispDeviceInfoMsgT* MsgP);
void ValCalendarSystemTimeSetInSec(uint32 Seconds, uint8 LpSec, int8 LtmOff, bool DayLt);

/*-----------------------------------------------------------------
 * val voice stream VSTRM
 *----------------------------------------------------------------*/
void ValVstrmFlushVoiceData(void);
void ValVstrmGetVoiceData(uint8 *DstBuf, uint16 Size, uint16 *NumPcktsP, uint16* NumBytesRcvdP);
void ValVstrmChkVoiceData(uint16 *NumPcktsP, uint16* NumBytesRcvdP);
void ValVstrmDiscardVoiceData(uint16 Size, uint16 *NumPcktsP, uint16* NumBytesDiscardedP);

/*-----------------------------------------------------------------
 * val interfaces for connectivity (valconnectivity.c)
 *----------------------------------------------------------------*/
ValRptCpEventT ValPktSvcState( void );


/*-----------------------------------------------------------------
 * val interfaces for Silent Logging (valtask.c)
 *----------------------------------------------------------------*/
uint16 ValSilentLogUpload (uint8 *DstP, uint16 NumBytes, uint32 Offset, uint32 *TotSizeP);


/*-----------------------------------------------------------------
 * val PGPS functions (valpgps.c)
 *----------------------------------------------------------------*/
void PgpsPdaInit(void);
void PgpsPdaStart(void );
void PgpsPdaDbmReadRspMsg(void * MsgDataP);
void PgpsPdaDbmWriteRspMsg(void *MsgDataP);
void PgpsPdaCfgSet( ValPgpsCfgT * msg_buf);
void PgpsPdaCfgGet(   ExeRspMsgT  * MsgP );
void PgpsPdaTimerHandler(void );
void PgpsPdaStartNetwork(void );
void PgpsConnected( void );
BOOL PgpsReadMoreData(void );
BOOL PgpsClosePpp(void );
void PgpsDataFeteched(void );
void   ValRecordModemFailure(uint8 Interface, bool Set);
void ValHrpdStatMeasStartSessTime(void *Msg_buffer);
void ValHrpdStatInit(bool connectionOpen);
void ValHrpdStatMeasStopSessTime(void);
BOOL ValPsdmIsPsAttached(void); // check if +cgatt initiated
#ifdef MTK_CBP
extern uint32 GPSSessStartTime;
extern void ValPswNotifyGpsOfEmergencyCall(uint8 IsActive);
extern void ValPswTriggerFakeStopGps(void);
#endif

#ifdef MTK_DEV_C2K_IRAT
#ifndef MTK_PLT_ON_PC
void Send_Irat_Debug_Info_To_Md1(char  *pMsg, uint32 a, uint32 b, uint32 c);
void Send_Irat_Debug_Info_To_Md1_Common(const char* fmt, ...);
#define PRINT_TRACE_IN_ELT(fmt, ...) Send_Irat_Debug_Info_To_Md1_Common(fmt, ##__VA_ARGS__)
#define C2K_IRAT_ELT_TRACE(pMsg, a, b, c) Send_Irat_Debug_Info_To_Md1(pMsg, a, b, c)
#else
#define PRINT_TRACE_IN_ELT(format, ...)
#define C2K_IRAT_ELT_TRACE(pMsg, a, b, c)
#endif
#endif

#ifdef MTK_DEV_C2K_IRAT
#define C2K_IRAT_STATUS      (ValIratGetInterRatOperationStatus())
#define C2K_IRAT_ON          if(C2K_IRAT_STATUS)
#define C2K_IRAT_OFF         if(!C2K_IRAT_STATUS)
BOOL ValPsdmIsMyPpp(void); // check if the PPP is initiated by C-PSDM
BOOL ValPsdmIsLTEDisabled(void);/*check if 4G is disabled*/
#ifdef MTK_DEV_C2K_SRLTE
/* C2K_SRLTE_STATUS is a constant, for non-single bin build. */
#ifdef MTK_DEV_C2K_SRLTE_L1
#define C2K_SRLTE_STATUS     (TRUE)
#else
#define C2K_SRLTE_STATUS     (FALSE)
#endif

#define C2K_SRLTE_ON         if (C2K_SRLTE_STATUS)
#define C2K_SRLTE_OFF        if (!C2K_SRLTE_STATUS)

/* #define SRLTE_RSVA_STUB 1 */
#else /* MTK_DEV_C2K_SRLTE */
#define C2K_SRLTE_STATUS     (FALSE)
#endif /* MTK_DEV_C2K_SRLTE */

#define C2K_IRAT_ON_OR_SRLTE_ON if((C2K_IRAT_STATUS)||(C2K_SRLTE_STATUS))

#else
#define C2K_IRAT_STATUS      (ValIratGetInterRatOperationStatus())
#define C2K_IRAT_ON          if(C2K_IRAT_STATUS)
#define C2K_IRAT_OFF         if(!C2K_IRAT_STATUS)
#define C2K_IRAT_ELT_TRACE(pMsg, a, b, c)
BOOL ValPsdmIsMyPpp(void); // check if the PPP is initiated by C-PSDM
BOOL ValPsdmIsAttached(void); // check if +cgatt initiated
BOOL ValPsdmIsLTEDisabled(void);/*check if 4G is disabled*/

#endif /* MTK_DEV_C2K_IRAT */


#ifdef __cplusplus
}
#endif /* __cplusplus */

#ifdef MTK_CBP /*for mode switch optimization*/
extern OperationModeT ValOpMode;
extern OperationModeT ValOpModeOld;
extern OperationModeT OpModeForSim1;


extern bool GmssModeSwitchOngoing;
extern bool ATModeSwitchOngoing;
extern OperationModeT C2KPrefMode; 
extern uint8 PowerCtrlPendByModeSwitch;
extern uint8 ValGmssHybridModeSetProgress;
extern uint8 ValATHybridModeSetProgress;


extern OperationModeT ValModeGet(void);
extern void ValSavePrefModeToNV(OperationModeT PrefModeForSave);

#define VAL_HYBRID_MODE_CHG_1X_ENABLE_MASK     0x01
#define VAL_HYBRID_MODE_CHG_1X_DISABLE_MASK    0x02
#define VAL_HYBRID_MODE_CHG_DO_ENABLE_MASK     0x04
#define VAL_HYBRID_MODE_CHG_DO_DISABLE_MASK    0x08
#define VAL_HYBRID_MODE_CHG_REQ_START_MASK     0x80
#endif

#endif /* VALAPI_H */

/*****************************************************************************
 End of file
*****************************************************************************/
/**Log information: \main\6 2012-02-20 08:44:52 GMT hbi
** HREF#0000: remove warning (Audio)**/
/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:15:47 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:20:15 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
/**Log information: \main\Trophy\Trophy_zjiang_href22162\1 2013-04-22 07:49:54 GMT zjiang
** HREF#22162.1x only版本编译错误**/
/**Log information: \main\Trophy\2 2013-04-22 08:04:35 GMT gdeng
** HREF#22162|**/
/**Log information: \main\Trophy\Trophy_wzhou_href22163\1 2013-04-25 03:10:40 GMT wzhou
** HREF#22163: add A12 auth status message in VAL**/
/**Log information: \main\Trophy\3 2013-04-25 03:06:29 GMT jzwang
** href#22163**/
/**Log information: \main\Trophy\Trophy_wzhou_href22221\1 2013-06-05 08:57:58 GMT wzhou
** HREF#22221: fix Agps**/
/**Log information: \main\Trophy\5 2013-06-05 08:47:21 GMT jzwang
** href#22221**/
/**Log information: \main\Trophy\Trophy_zjiang_href22256\1 2013-08-21 07:39:36 GMT zjiang
** HREF#22256.1.crts21316:模块短信自注册编译版本里存在的自注册短信多余上报修改;2.+CPIN命令优化.**/
/**Log information: \main\Trophy\6 2013-08-21 07:42:28 GMT cshen
** href#22256**/
/**Log information: \main\Trophy\Trophy_zjiang_href22290\1 2013-10-25 07:51:33 GMT zjiang
** HREF#22290.fix crts 21713 and crts 21496. 添加+VECIO主动上报。修改VMEMFL命令中的问题。**/
/**Log information: \main\Trophy\7 2013-10-25 07:54:10 GMT cshen
** href#22290**/
/**Log information: \main\Trophy\Trophy_zjiang_href22317\1 2013-11-27 09:21:37 GMT zjiang
** HREF#22317.fix crts21903.增加UICC卡类型。**/
/**Log information: \main\Trophy\8 2013-11-28 01:24:36 GMT cshen
** href#22317**/
/**Log information: \main\Trophy\Trophy_yzhang_href22324\1 2013-12-05 09:30:46 GMT yzhang
** HREF#22324:India MTS/TATA ESN Tracking SMS requirement**/
/**Log information: \main\Trophy\9 2013-12-06 02:32:24 GMT cshen
** href#22324**/
/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:18:07 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\10 2013-12-10 08:33:46 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
/**Log information: \main\Trophy\Trophy_zjiang_href22338\1 2013-12-23 07:32:21 GMT zjiang
** HREF#22338**/
/**Log information: \main\Trophy\12 2013-12-23 08:58:55 GMT cshen
** href#22338**/
/**Log information: \main\Trophy\Trophy_zjiang_href22357\1 2014-01-11 07:30:26 GMT zjiang
** HREF#22357**/
/**Log information: \main\Trophy\13 2014-01-11 07:36:42 GMT cshen
** HREF#22357**/
