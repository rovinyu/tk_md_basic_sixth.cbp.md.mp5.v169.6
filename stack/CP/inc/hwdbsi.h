/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * hwdbsi.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _HWDBSI_H_
#define _HWDBSI_H_
/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
/* BSI delay type */
#define BSI_DLY_TYP(bOUNDARY, tRIGnUM) (bOUNDARY | (tRIGnUM << 8))

/* BSI control configurations for SPI */
#define IMMED_CTRL_SPI(wRrD, cHIPsEL, lOCK)     (((wRrD) << 13) | ((lOCK) << 1) | ((cHIPsEL) << 0))
#define IMMED_CTRL_SPI_WR(cHIPsEL, lOCK)        IMMED_CTRL_SPI(0, cHIPsEL, lOCK)
#define IMMED_CTRL_SPI_RD(cHIPsEL, lOCK)        IMMED_CTRL_SPI(1, cHIPsEL, lOCK)
#define DLY_CTRL_SPI_WR(cHIPsEL, lOCK)          (((lOCK) << 1) | ((cHIPsEL) << 0))
#define GATE_CTRL_SPI_WR(cHIPsEL, lOCK)         DLY_CTRL_SPI_WR(cHIPsEL, lOCK)

/* BSI control configurations for MIPI */
#define IMMED_CTRL_MIPI_WR(wlEN, lOCK)          ((0 << 13) | ((((wlEN) - 1) & 0x1F) << 2) | ((lOCK) << 1))
#define IMMED_CTRL_MIPI_RD(wlEN, rlEN)                (((1 << 13) | (((rlEN) - 1) & 0x3F) << 7) | ((wlEN) << 2))/* lock===0 when read according to Leo.Zhu's comment */
#define DLY_CTRL_MIPI_WR(wlEN, lOCK)            (((((wlEN) - 1) & 0x1F) << 2) | ((lOCK) << 1))
#define GATE_CTRL_MIPI_WR(wLEN, lOCK)           DLY_CTRL_MIPI_WR(wLEN, lOCK)

#define IMMED_MIPI_WR0                    IMMED_CTRL_MIPI_WR(7+6, 0 /*LockEn*/)
#define IMMED_MIPI_WR                     IMMED_CTRL_MIPI_WR(7+6+9, 0 /*LockEn*/)
#define IMMED_MIPI_RD                     IMMED_CTRL_MIPI_RD(12, 9)
#define IMMED_MIPI_RD_EXT_ONLY_ONEBYTE    IMMED_CTRL_MIPI_RD(21, 9)
#define IMMED_MIPI_EXTWR_1ST              IMMED_CTRL_MIPI_WR(13+9+9, 1 /*LockEn*/)
#define IMMED_MIPI_EXTWR_LEFT(bYTEnUM)    IMMED_CTRL_MIPI_WR((bYTEnUM)*9, 0 /*LockEn*/)
#define IMMED_MIPI_EXTWR_ONLY_ONEBYTE     IMMED_CTRL_MIPI_WR(13+9+9, 0 /*LockEn*/)


#define DLY_MIPI_WR0                      DLY_CTRL_MIPI_WR(7+6, 0)
#define DLY_MIPI_WR                       DLY_CTRL_MIPI_WR(7+6+9, 0 /*LockEn*/)
#define DLY_MIPI_EXTWR_1ST                DLY_CTRL_MIPI_WR(13+9+9, 1 /*LockEn*/)
#define DLY_MIPI_EXTWR_LEFT(rEGnUM)       DLY_CTRL_MIPI_WR((rEGnUM)*9, 0 /*LockEn*/)
#define DLY_MIPI_EXTWR_ONLY_ONEBYTE       DLY_CTRL_MIPI_WR(13+9+9, 0 /*LockEn*/)


#define GATE_MIPI_WR0                     GATE_CTRL_MIPI_WR(7+6, 0)
#define GATE_MIPI_WR                      GATE_CTRL_MIPI_WR(7+6+9, 0 /*LockEn*/)
#define GATE_MIPI_EXTWR_1ST               GATE_CTRL_MIPI_WR(13+9+9, 1 /*LockEn*/)
#define GATE_MIPI_EXTWR_LEFT(rEGnUM)      GATE_CTRL_MIPI_WR((rEGnUM)*9, 0 /*LockEn*/)
#define GATE_MIPI_EXTWR_ONLY_ONEBYTE      GATE_CTRL_MIPI_WR(13+9+9, 0 /*LockEn*/)


/* RESET register field definition */
#define GLOBAL_RST      (1 << 0)
#define IMM_WR_BUF_RST  (1 << 1)
#define IMM_RD_BUF_RST  (1 << 2)
#define DLY_BUF1_RST    (1 << 3)
#define DLY_BUF2_RST    (1 << 4)
#define TGON_BUF_RST    (1 << 5)
#define TGOFF_BUF_RST   (1 << 6)

/* CTRL register field definition */
#define CMD_RD_INTERVAL(cYCLE)  (((cYCLE) & 0xff) << 24)
#define CMD_WR_INTERVAL(cYCLE)  (((cYCLE) & 0xff) << 16)

/* GATE mask register field definition */
#define TGON_MASK       (1 << 0)
#define TGOFF_MASK      (1 << 1)

/* INT_MASK regsiter field definition */
#define IMM_DONE_MASK           (1 << 0)
#define DLY1_DO1ST_DONE_MASK    (1 << 1)
#define DLY1_DO2ND_DONE_MASK    (1 << 2)
#define DLY2_DO1ST_DONE_MASK    (1 << 3)
#define DLY2_DO2ND_DONE_MASK    (1 << 4)
#define TG_ON_DONE_MASK         (1 << 5)
#define TG_OFF_DONE_MASK        (1 << 6)
#define IMM_ERR_MASK            (1 << 16)
#define IMM_RD_ERR_MASK         (1 << 17)
#define DLY1_DO1ST_ERR_MASK     (1 << 18)
#define DLY1_DO2ND_ERR_MASK     (1 << 19)
#define DLY2_DO1ST_ERR_MASK     (1 << 20)
#define DLY2_DO2ND_ERR_MASK     (1 << 21)
#define TG_ON_ERR_MASK          (1 << 22)
#define TG_OFF_ERR_MASK         (1 << 23)

#define DONE_MASK               (0x0000ffff)
#define ERR_MASK                (0xffff0000)

/******************************************************
    For JADE + Orion+
******************************************************/
#define IMMED_SPI_WR                      IMMED_CTRL_SPI_WR(0 /*IC*/, 0 /*LockEn*/)
#define IMMED_SPI_RD                      IMMED_CTRL_SPI_RD(0 /*IC*/, 0 /*LockEn*/)
#define DLY_SPI_WR                        DLY_CTRL_SPI_WR(0 /*IC*/, 0 /*LockEn*/)
#define GATE_SPI_WR                       GATE_CTRL_SPI_WR(0 /*IC*/, 0 /*LockEn*/)

/******************************************************
    For JADE
******************************************************/
/* CTRL register field definition */
#define BSI_RD_INTERVAL             CMD_RD_INTERVAL(2)
#define BSI_WR_INTERVAL             CMD_WR_INTERVAL(50)
#define MIPI_RD_INTERVAL            CMD_RD_INTERVAL(2)
#define MIPI_WR_INTERVAL            CMD_WR_INTERVAL(2)
#define BSI_RD_PROTECT              (1 << 1)
#define MIPI_MODE                   (0)
#define SPI_MODE                    (1)

#define BSI_SPI_CTRL                (BSI_RD_INTERVAL | BSI_WR_INTERVAL | SPI_MODE)
#define BSI_MIPI_CTRL               (MIPI_RD_INTERVAL | MIPI_WR_INTERVAL | MIPI_MODE)

#define BSI_IMMED_BUF_MAX_DEPTH     (32)
#define BSI_IMMED_RD_BUF_MAX_DEPTH  (8)
#define BSI_GATED_BUF_MAX_DEPTH     (4)
#define BSI_DLY_BUF_MAX_DEPTH       (8)

/* BSI bus timing */
#define BSI_TIME_OUT_CYCLE_PER_US   (10)
#define BSI_TIME_OUT_US    ((438 * 32 * 32 + 999) / 1000)
#define BSI_TIME_MAX_CW             BSI_IMMED_BUF_MAX_DEPTH

/* BSI/MIPI CTRL selection */
#define CTRL_SEL(bSInAME, sPIcTRL, mIPIcTRL) (bSInAME < MIPI_END ? mIPIcTRL : sPIcTRL)

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
typedef struct
{
    uint32 low;
    uint32 high;
} BsiReadResExT;


typedef enum
{
    BSI_DLY_1,
    BSI_DLY_2,
    BSI_DLY_NUM_MAX
} BsiDlyNumT;

typedef enum
{
    TX_GATE_ON,
    TX_GATE_OFF
} BsiTxGateT;

typedef enum
{
    SPI_DEV_CFG,
    SPI_DEV_CFG_DLY,
    SPI_WR_IMMED_SING,
    SPI_WR_IMMED_BLK,
    SPI_WR_DLY_SING,
    SPI_WR_DLY_BLK,
    SPI_WR_DLY_BUF_RST,
    SPI_RD_IMMED,
    SPI_RD_DLY,
    SPI_REG_WRITE,
    SPI_REG_READ,
    SPI_REG_SET_BIT,
    SPI_REG_CLR_BIT,
    SPI_REG_WRITE_SIM,
    SPI_REG_READ_SIM,
    SPI_REG_SET_BIT_SIM,
    SPI_REG_CLR_BIT_SIM,
} HwdSpiTraceCtrlT;

/*----------------------------------------------------------------------------
 Global Interfaces
----------------------------------------------------------------------------*/
/* Regsiter manipulation */
#ifndef MTK_DEV_HW_SIM_RF
#define BsiWrite(aDDR, dATA)        do {\
    HwdWrite32(aDDR, dATA);\
    if (HwdBsiTraceEn) {\
        if (HwdBsiTraceAll || (!HwdBsiTraceAll && ((aDDR) & HwdBsiTraceAddrMask) == (HwdBsiTraceAddrData & HwdBsiTraceAddrMask)))\
            MonTrace(MON_CP_HWD_SPI_DBG_TRACE_ID, 3, SPI_REG_WRITE, aDDR, dATA);\
    }\
} while(0)
#define BsiRead(aDDR)               ({\
    uint32 res = HwdRead32(aDDR);\
    if (HwdBsiTraceEn) {\
        if (HwdBsiTraceAll || (!HwdBsiTraceAll && ((aDDR) & HwdBsiTraceAddrMask) == (HwdBsiTraceAddrData & HwdBsiTraceAddrMask)))\
            MonTrace(MON_CP_HWD_SPI_DBG_TRACE_ID, 3, SPI_REG_READ, aDDR, res);\
    }\
    res;\
})
#define BsiSetBit(aDDR, bITmASK)    do {\
    HwdSetBit32(aDDR, bITmASK);\
    if (HwdBsiTraceEn) {\
        if (HwdBsiTraceAll || (!HwdBsiTraceAll && ((aDDR) & HwdBsiTraceAddrMask) == (HwdBsiTraceAddrData & HwdBsiTraceAddrMask)))\
            MonTrace(MON_CP_HWD_SPI_DBG_TRACE_ID, 3, SPI_REG_SET_BIT, aDDR, bITmASK);\
    }\
} while(0)
#define BsiClrBit(aDDR, bITmASK)    do {\
    HwdResetBit32(aDDR, bITmASK);\
    if (HwdBsiTraceEn) {\
        if (HwdBsiTraceAll || (!HwdBsiTraceAll && ((aDDR) & HwdBsiTraceAddrMask) == (HwdBsiTraceAddrData & HwdBsiTraceAddrMask)))\
            MonTrace(MON_CP_HWD_SPI_DBG_TRACE_ID, 3, SPI_REG_CLR_BIT, aDDR, bITmASK);\
    }\
} while(0)
#endif /* MTK_DEV_HW_SIM_RF */

/* SPI */
#define M_HwdBsiWriteImmed(dATApTR, dATAnUM) \
    HwdBsiWriteImmedEx(BSI1, NULL, dATApTR, dATAnUM)
#define M_HwdBsiReadImmed(rEADcMDpTR, rEADrESpTR, dATAnUM) \
    HwdBsiReadImmedEx(BSI1, NULL, rEADcMDpTR, rEADrESpTR, dATAnUM)
#define M_HwdBsiWriteDly1st(bAUNDARY, dATApTR, dATAnUM) \
    HwdBsiWriteDlyEx(BSI1, bAUNDARY, BSI_DLY_1, NULL, dATApTR, dATAnUM)
#define M_HwdBsiWriteDly2nd(bAUNDARY, dATApTR, dATAnUM) \
    HwdBsiWriteDlyEx(BSI1, bAUNDARY, BSI_DLY_2, NULL, dATApTR, dATAnUM)
#define M_HwdBsiWriteDly(bAUNDARY, dATApTR, dATAnUM) \
    M_HwdBsiWriteDly1st(bAUNDARY, dATApTR, dATAnUM)
#define M_HwdBsiWriteTxGateOn(dATApTR, dATAnUM) \
    HwdBsiWriteTgEx(BSI1, TX_GATE_ON, NULL, dATApTR, dATAnUM)
#define M_HwdBsiWriteTxGateOff(dATApTR, dATAnUM) \
    HwdBsiWriteTgEx(BSI1, TX_GATE_OFF, NULL, dATApTR, dATAnUM)
/* MIPI */
#define M_HwdMipiWriteImmed(bSInAME, dATApTR, dATAnUM) \
    HwdBsiWriteImmedEx(bSInAME, NULL, dATApTR, dATAnUM)
#define M_HwdMipiReadImmed(bSInAME, rEADcMDpTR, rEADrESpTR, dATAnUM) \
    HwdBsiReadImmedEx(bSInAME, NULL, rEADcMDpTR, rEADrESpTR, dATAnUM)
#define M_HwdMipiWriteImmedExt(bSInAME, cTRLpTR, dATApTR, dATAnUM) \
    HwdBsiWriteImmedEx(bSInAME, cTRLpTR, dATApTR, dATAnUM)
#define M_HwdMipiWriteDly(bSInAME, bAUNDARY, dATApTR, dATAnUM) \
    HwdBsiWriteDlyEx(bSInAME, bAUNDARY, BSI_DLY_1, NULL, dATApTR, dATAnUM)
#define M_HwdMipiWriteDlyExt(bSInAME, bAUNDARY, cTRLpTR, dATApTR, dATAnUM) \
    HwdBsiWriteDlyEx(bSInAME, bAUNDARY, BSI_DLY_1, cTRLpTR, dATApTR, dATAnUM)
#define M_HwdMipiWriteDlyExt2(bSInAME, bAUNDARY, cTRLpTR, dATApTR, dATAnUM) \
        HwdBsiWriteDlyEx(bSInAME, bAUNDARY, BSI_DLY_2, cTRLpTR, dATApTR, dATAnUM)
#define M_HwdMipiWriteTxGateOn(bSInAME, cTRLpTR, dATApTR, dATAnUM) \
    HwdBsiWriteTgEx(bSInAME, TX_GATE_ON, cTRLpTR, dATApTR, dATAnUM)
#define M_HwdMipiWriteTxGateOff(bSInAME, cTRLpTR, dATApTR, dATAnUM) \
    HwdBsiWriteTgEx(bSInAME, TX_GATE_OFF, cTRLpTR, dATApTR, dATAnUM)

#define M_HwdBsiTxGateOnEnable(bSInAME)    BsiClrBit(BSI_REG_TXGATE_MASK(bSInAME), TGON_MASK)
#define M_HwdBsiTxGateOffEnable(bSInAME)   BsiClrBit(BSI_REG_TXGATE_MASK(bSInAME), TGOFF_MASK)
#define M_HwdBsiTxGateDisable(bSInAME)     BsiSetBit(BSI_REG_TXGATE_MASK(bSInAME), TGON_MASK | TGOFF_MASK) 

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
extern void HwdBsiInit(void);
extern void HwdBsiWriteImmedEx(const BsiNameT bsiName, const uint32 *ctrlPtr, const uint32 *dataPtr, const uint16 num);
extern void HwdBsiReadImmedEx(const BsiNameT bsiName, const uint32 *ctrlPtr, const uint32 *readCmdPtr, BsiReadResExT *readResPtr, const uint16 num);
extern void HwdBsiWriteDlyEx(const BsiNameT bsiName, const BsiDlyBoundaryT dlyBoundary, const uint8 dlyTrigNum,
    const uint32 *ctrlPtr, const uint32 *dataPtr, const uint16 num);
extern void HwdBsiWriteTgEx(const BsiNameT bsiName, const BsiTxGateT gate, const uint32 *ctrlPtr, const uint32 *dataPtr, const uint16 num);

void HwdBsiLisr(uint32 src);

/*----------------------------------------------------------------------------
 External Interfaces
----------------------------------------------------------------------------*/
#ifndef MTK_DEV_HW_SIM_RF
#define M_HwdBsiInit()  HwdBsiInit()
#endif /* MTK_DEV_HW_SIM_RF */

extern uint32 HwdBsiTraceEn;
extern uint32 HwdBsiTraceAll;
extern uint32 HwdBsiTraceAddrMask;
extern uint32 HwdBsiTraceAddrData;

#if (defined MTK_DEV_DUMP_REG)
extern void HwdBsiRegLogRdAll(void);
#define M_HwdBsiRegLogRdAll()  HwdBsiRegLogRdAll()
#endif

#endif /* _HWDBSI_H_ */

