/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/**************************************************************************************************
* %version: 2 %  %instance: HZPT_2 %   %date_created: Fri Mar 23 15:56:42 2007 %  %created_by: jingzhang %  %derived_by: jingzhang %
**************************************************************************************************/

#ifndef FSIAPI_H
#define FSIAPI_H
/*****************************************************************************
*FILE NAME: valfsiapi.h
*
*DESCRIPTION:
*This is the File System Interface API include file.
*
* HISTORY:
*   See Log at end of file
*
*****************************************************************************/
#include "sysdefs.h"
#include "FsmApi.h"
#include "Fsmdataitem.h"

//don't modify following macro, brew has referred to it
/*max full path file name length, doesn't include end char "0" */
#define    FSI_MAX_PATH_NAME_LENGTH    MAX_PATH_LENGTH

/*max file name length, doesn't include end char "0" */
#define    FSI_MAX_FILE_NAME_LENGTH    MAX_FILE_NAME_LEN

/*max volume name length, doesn't include end char "0" */
#define    FSI_MAX_VOLUME_NAME_LENGTH  MAX_VOLUME_NAME_LEN

#define    FSI_INVALID_FIND_HANDLE     NULL
#define    FSI_INVALID_FILE_HANDLE     INVALID_FILE_HANDLE

#define    FSI_SIDB_WILDCARD_TYPE      WILDCARD_TYPE
#define    FSI_SIDB_WILDCARD_ID        WILDCARD_ID

#ifdef MTK_DEV_CCCI_FS
#define VAL_FSI_SIDB_DBM_NODE_DATA_LEN 8
#define VAL_FSI_SIDB_NODE_NUM          4
typedef struct 
{
   uint16 ItemType;
   uint16 ItemID;
   uint16 LID;
   uint16 RecIdx;
}ValFsiSidb2LidT;
typedef struct
{
   uint8 IsValid;
   uint8 Data[VAL_FSI_SIDB_DBM_NODE_DATA_LEN];
}ValFsiSidbDbmNodeT;

#endif

typedef    HFSMFILE       ValFsiHandleT;
typedef    HENUM          ValFsiFindHandle; /*file search handle*/

//don't modify following Enum, brew has referred to it
enum
{
    FSI_FILE_ATTRIB_NORMAL      = FSM_FILE_ATTRIB_NORMAL,
    FSI_FILE_ATTRIB_READONLY    = FSM_FILE_ATTRIB_READONLY,
    FSI_FILE_ATTRIB_HIDDEN      = FSM_FILE_ATTRIB_HIDDEN,
    FSI_FILE_ATTRIB_SYSTEM      = FSM_FILE_ATTRIB_SYSTEM,
    FSI_FILE_ATTRIB_ARCHIVE     = FSM_FILE_ATTRIB_ARCHIVE,
    FSI_FILE_ATTRIB_DRM     = FSM_FILE_ATTRIB_DRM,
    FSI_FILE_ATTRIB_ROM     = FSM_FILE_ATTRIB_ROM,  
    FSI_FILE_ATTRIB_DIR         = FSM_FILE_ATTRIB_DIR,
    FSI_FILE_ATTRIB_FILE        = FSM_FILE_ATTRIB_FILE
};

//don't modify following Struct, brew has referred to it
typedef struct 
{
    uint16        Attrib;
    uint32        CreateTime;  /*Include date and time of day.*/
    uint32        Size;
} ValFsiFileAttribT;

//don't modify following Struct, brew has referred to it
typedef struct 
{
    ValFsiFileAttribT  FileAttrib;
    char               Name[FSI_MAX_FILE_NAME_LENGTH + 1];
} ValFsiFileInfoT;

typedef struct 
{
    uint16    ItemId;
    uint16    ItemType;
    uint32    ItemLength;
} ValFsiDataItemInfoT;

/*Open modes for file open, All write mode permit read operation:
Mode 0:
Read only mode. If file don't exist, return error. File pointer is at start.
Mode 1:
write an existing file. File pointer is set to file start.
Mode 2:
Create a new file, if file already exist, return error.
Mode 3:
write mode, If file don't exist, create it, else truncate it to zero length.
Mode 4:
write mode, if file don't exist, create it, else don't truncate it, file
pointer is at start.
Mode 5:
share mode, this file can be opened many times to read or write.
if file don't exist, system will create it, file pointer is at start.
*/
//don't modify following Enum, brew has referred to it
typedef enum 
{
    FSI_FILE_OPEN_READ_EXIST     = 0,
    FSI_FILE_OPEN_WRITE_EXIST    = 1,
    FSI_FILE_OPEN_CREATE_NEW     = 2,
    FSI_FILE_OPEN_CREATE_ALWAYS  = 3,
    FSI_FILE_OPEN_WRITE_ALWAYS   = 4,
    FSI_FILE_OPEN_SHARE   = 5
} ValFsiFileOpenModeT;

//don't modify following Enum, brew has referred to it
typedef enum 
{ 
    FSI_FILE_SEEK_START   = FSM_FILE_BEGIN,
    FSI_FILE_SEEK_END     = FSM_FILE_END,
    FSI_FILE_SEEK_CURRENT = FSM_FILE_CURRENT
} ValFsiFileSeekTypeT;

//don't modify following Enum, brew has referred to it
typedef enum
{
   FSI_SUCCESS         = 0, /*No errors. Function completed successfully.*/
   FSI_ERR_PARAMETER   = 1, /*Incorrect parameter to the function.*/
   FSI_ERR_READ        = 2, /*file read operation is failed.*/
   FSI_ERR_WRITE       = 3, /*file write operation is failed.*/
   FSI_ERR_SYSTEM      = 4, /*Indicates that a system error has occurred.*/
   FSI_ERR_EXIST       = 5, /*The specified object has existed already.*/
   FSI_ERR_NOTEXIST    = 6, /*No matched object in specified media.*/
   FSI_ERR_EOF         = 7, /*file pointer reaches the end-of-file.*/
   FSI_ERR_FULL        = 8, /*Flash device is full*/
   FSI_ERR_NOSUPPORT   = 9, /*FSI does not support this function now .*/
   FSI_ERR_FORMAT      = 10, /*Volume is in the incorrect format.*/
   FSI_ERR_ACCESS_DENY = 11, /*Insufficient permissions to access object.*/
   
   /* reach to a limitation of the maximum number of the files that can be open
   simultaneously.*/
   FSI_ERR_MAX_OPEN    = 12, 
   FSI_ERR_TIMEOUT     = 13,
   FSI_ERR_INIT        = 14,
   FSI_ERR_MEMORY      = 15,
   FSI_ERR_ACCESS      = 16,
   FSI_ERR_MOUNTED     = 17,
   FSI_ERR_UNMOUNTED   = 18,
   FSI_ERR_UNKNOWN     = 255 /*Other unknowned error occar*/
} ValFsiResultT;

/* Define volume type */
typedef enum{

   FSI_VOLUME_TYPE_UNKNOWN = 0,
   FSI_VOLUME_TYPE_REMOVABLE, /* The drive has removable media; for example, a SD Card */
   FSI_VOLUME_TYPE_FLASH

}ValFsiVolumeTypeT;

 /* Define system type */
typedef enum{

  FSI_SYSTEM_TYPE_UNKNOWN = 0,
  FSI_SYSTEM_TYPE_DFS, 
  FSI_SYSTEM_TYPE_FAT12,
  FSI_SYSTEM_TYPE_FAT16,
  FSI_SYSTEM_TYPE_FAT32,
  FSI_SYSTEM_TYPE_NTFS

}ValFsiSystemTypeT;

//don't modify following Struct, brew has referred to it
typedef struct{

  uint32       TotalSpace;
  uint32        FreeSpace;
  uint32        hTotalSpace;    /* high 32bit value of total space for >4GB */
  uint32        hFreeSpace;     /* high 32bit value of free  space for >4GB */
  ValFsiVolumeTypeT VolumeType;
  ValFsiSystemTypeT SystemType;

}ValFsiVolumeInfoT;


/* Define VAL FSI Background Reclaim veto flags for the different units 
 * that need them. These defines are used as bit masks in calls to 
 * ValFsiReclaimSuspend and ValFsiReclaimResume.
 */
typedef enum 
{
    VAL_FSI_RECLAIM_SLC_1_FLAG = 0x0001,

    /* Continue adding defines up to and including MAX flag (which is just
     * there to make enum 16-bits.
     */
    VAL_FSI_RECLAIM_MAX_FLAG   = 0x8000 

} ValFsiReclaimVetoT;

#ifdef  __cplusplus
extern "C" {
#endif
//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiGetVolumeInfo(const char *VolumeNameP, ValFsiVolumeInfoT *VolumeInfoP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiGetSpaceInfo(const char *VolumeNameP, uint32 *TotalSpaceP,
                           uint32 *FreeSpaceP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFileOpen(ValFsiHandleT *FileP, const char *FileNameP,
                       ValFsiFileOpenModeT Mode);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFileClose(ValFsiHandleT File);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFileRead(void *BufferP, uint32 ItemSize, uint32 *ItemNumP,
                       ValFsiHandleT File);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFileWrite(void *BufferP, uint32 ItemSize, uint32 *ItemNumP,
                        ValFsiHandleT File);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFlush(ValFsiHandleT File);
ValFsiResultT ValFsiFlushAll(void);

ValFsiResultT ValFsiGetFileLength(const char *NameP, uint32 *FileLengthP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiGetFileHandleAttrib(ValFsiHandleT File,
                                  ValFsiFileAttribT *FileAttribP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiGetFileAttrib(const char *FileNameP,
                            ValFsiFileAttribT *FileAttribP);

ValFsiResultT ValFsiSetFileAttrib(const char *FileNameP, uint16 Attrib);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiSeek(ValFsiHandleT File, ValFsiFileSeekTypeT SeekFrom,
                   int32 MoveDistance);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiTell(ValFsiHandleT File, uint32 *PosP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiRename(const char *OldNameP, const char *NewNameP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFileTruncate(ValFsiHandleT File, uint32 NewSize);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiRemove(const char *NameP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFindFirst(ValFsiFindHandle *FindHandleP, const char *NameP,
                        ValFsiFileInfoT *FileInfoP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFindNext(ValFsiFindHandle FindHandle, ValFsiFileInfoT *FileInfoP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiFindClose(ValFsiFindHandle FindHandle);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiMakeDir (const char *DirNameP);

ValFsiResultT ValFsiOpenDir (ValFsiHandleT *FileP,const char *DirNameP);

ValFsiResultT ValFsiReadDir (ValFsiHandleT File, ValFsiFileInfoT *FileInfo);

ValFsiResultT ValFsiCloseDir (ValFsiHandleT File);

/****************************************************************************

  Function Name: ValFsiSysFormat

  Descriptions: Formats a device, the Host can call this after/before mounting a device,
                After formatting a device, the Host has to call the 
                FsmMount API to remount the file system.

  Arguments: VolumeName: [in] The device name. 
                         
                    sysType: [in] System format type, maybe  FSM_SYSTEM_TYPE_FAT16, FSM_SYSTEM_TYPE_FAT32

  Returns: FSI_SUCCESS: Function execution is successful.
           Other: Please refer to the Error Code Table.

  See also: FsmMount

****************************************************************************/
ValFsiResultT ValFsiSysFormat(char* VolumeName, FsmSystemTypeT sysType);

/*Data Item APIs: */

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiSidbRead(uint16 ItemType,uint16 ItemID,void*BufferP,
                       uint32 Offset, uint32 *SizeP);

//don't modify following Function, brew has referred to it
ValFsiResultT ValFsiSidbWrite(uint16 ItemType,uint16 ItemID, void*BufferP,
                        uint32 Offset, uint32 *SizeP);

ValFsiResultT ValFsiSidbDelete(uint16 ItemType,uint16 ItemID);

ValFsiResultT ValFsiSidbFindFirst(ValFsiHandleT *FindHandleP, uint16 ItemType,uint16 ItemID, ValFsiDataItemInfoT *InfoP);

ValFsiResultT ValFsiSidbFindNext (ValFsiHandleT FindHandle, ValFsiDataItemInfoT *InfoP);

ValFsiResultT ValFsiSidbFindClose (ValFsiHandleT FindHandle);

ValFsiResultT ValFsiSidbFlush(uint16 ItemType,uint16 ItemID);

ValFsiResultT ValFsiSidbFlushAll(void);


/*****************************************************************************
 
   FUNCTION:     ValFsiReclaimSuspend
   
   DESCRIPTION:  Temporarily suspends FSM Background Reclaim during critical
                 sections of code.
                 
                 Reclaim should be suspended for as short a time as possible.
   
   PARAMETERS:   BitMask - 16-bit mask where bit corresponding to suspending 
                           unit is set.
                           
   RETURNS:      None.

*****************************************************************************/
void ValFsiReclaimSuspend (ValFsiReclaimVetoT BitMask);

/*****************************************************************************
 
   FUNCTION:     ValFsiReclaimResume
   
   DESCRIPTION:  Resumes FSM Background Reclaim for a particular bit in the
                 veto mask that was previously suspended. 
                 When veto mask becomes zero, FSM background reclaim will be 
                 allowed to continue.
                 
   PARAMETERS:   BitMask - 16-bit mask indicating which unit is being resumed.
                           
   RETURNS:      None.

*****************************************************************************/
void ValFsiReclaimResume (ValFsiReclaimVetoT BitMask);

/*****************************************************************************
 
   FUNCTION:     ValFsiIsReclaimActive
   
   DESCRIPTION:  This function is used to determine whether or not FSM is in the
                 process of doing a dirty block reclaim.
                 
   PARAMETERS:   None
                           
   RETURNS:      TRUE if FSM reclaim is in progress, FALSE otherwise

*****************************************************************************/
bool ValFsiIsReclaimActive (void);

#ifdef  __cplusplus
}
#endif




/*****************************************************************************
* $Log: valfsiapi.h $
* Revision 1.1  2007/10/29 11:53:06  binye
* Initial revision
* Revision 1.1  2007/10/12 13:57:31  yjin
* Initial revision
* Revision 1.1  2006/11/26 21:43:15  yliu
* Initial revision
* Revision 1.2  2006/11/21 09:38:35  yjin
* update lsm
* Revision 1.1  2006/10/24 15:02:04  binye
* Initial revision
* Revision 1.2  2006/01/07 12:11:14  wavis
* Merging in VAL.
* Revision 1.1.1.2  2005/12/14 11:04:39  wavis
* Changes to support DOS File System.
* Revision 1.1.1.1  2005/11/07 14:59:24  wavis
* Duplicate revision
* Revision 1.1  2005/11/07 14:59:24  wavis
* Initial revision
* Revision 1.1.1.3  2005/11/04 10:06:30  agontar
* added response info to FSI messages
* Revision 1.1.1.2  2005/10/14 16:37:31  agontar
* Added new FSI api function
* Revision 1.1.1.1  2005/10/13 14:11:47  agontar
* Duplicate revision
* Revision 1.1  2005/10/13 14:11:47  dorloff
* Initial revision
*****************************************************************************/
#endif
/**Log information: \main\vtui2_5x\2 2008-06-02 08:35:45 GMT hwang
** HREF#416**/
/**Log information: \main\vtui2_5x\3 2008-09-10 06:59:07 GMT rli
** HREF#1930,add three val fsi api for dir operation**/
/**Log information: \main\vtui2_5x\4 2008-12-22 12:09:54 GMT yjin
** HREF#4142**/
/**Log information: \main\helios_dev\helios_dev_hwang_href7865\1 2009-07-27 08:53:17 GMT hwang
** HREF#7865**/
/**Log information: \main\helios_dev\3 2009-07-27 09:09:17 GMT yjin
** href7865:provide share mode to open multi-files**/
/**Log information: \main\VTUI3\VTUI3_hwang_href11124\1 2010-03-26 08:41:28 GMT hwang
** HREF#11124**/
/**Log information: \main\VTUI3\2 2010-03-29 05:46:18 GMT yjin
** href11124**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_hwang_href14358\1 2010-09-13 09:39:08 GMT hwang
** href14358**/
/**Log information: \main\CBP7FeaturePhone\5 2010-09-14 02:23:45 GMT yjin
** href14358**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\1 2011-07-04 08:23:02 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\2 2011-07-08 06:55:38 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\6 2011-07-12 09:40:41 GMT marszhang
** HREF#17384**/
