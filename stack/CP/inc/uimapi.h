/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef UIMAPI_H
#define UIMAPI_H
/*****************************************************************************
 
  FILE NAME:  uimapi.h

  DESCRIPTION:

    This file contains all constants and typedefs needed to interface
    with the UIM unit via the Exe mail service routines.


*****************************************************************************/

#include "sysdefs.h"
#include "sysapi.h"
#include "pswnam.h"
#include "pswcustom.h"
#ifdef MTK_DEV_C2K_IRAT
#include "iratapi.h"
#include "cc_irq_public.h"
#endif
#include "sbpapi.h"
/*------------------------------------------------------------------------
* Define constants used in UIM API
*------------------------------------------------------------------------*/
#define SW_OK    0x9000
#define SW_PARAM_ERR 0x6B00
#define TERM_PROFILE_LEN 20

/*------------------------------------------------------------------------
* EXE Interfaces - Definition of Signals and Mailboxes
*------------------------------------------------------------------------*/
#define UIM_STARTUP_SIGNAL   EXE_SIGNAL_1
#define UIM_RESET_SIGNAL     EXE_SIGNAL_2
#if ((defined SYS_OPTION_REMOTE_UICC)||(defined SYS_OPTION_LTEDO_UART) || (defined SYS_OPTION_UIM_REMOTE_ACCESS))
#define UIM_REMOTE_UICC_SIGNAL EXE_SIGNAL_3
#endif

/* UIM command mailbox id */
#define UIM_MAILBOX          EXE_MAILBOX_1_ID
#define UIM_MAILBOX_S        EXE_MAILBOX_2_ID

#define UICC_ICCID_DATA_LEN 10
#define UICC_CHV1_DATA_LEN 8
#define UICC_ICCID_NV_INVALID_ENTRY_ID 0xFF

#ifdef MTK_DEV_C2K_IRAT
#define UICC_DBM_CACHE_CARD_NUM 2   /* Number of card info cached in DBM NV */
#define UICC_DBM_CURR_CARD_INDEX 0  /* Current card number index */
#define UIM_MAILBOX_REMOTE   EXE_MAILBOX_3_ID
#define UIM_MAILBOX_ACCESS_CONFIG    EXE_MAILBOX_4_ID
#define UICC_COMMAND_LEN  5
#define UICC_ATR_MAX_LEN  40
#endif

#define MAXALPHALENGTH 21
#define MAXSMSMSGLENGTH 253
#define UIM_PARAM_P_LEN 64
#define UIM_PARAM_G_LEN 20
#define UIM_MAX_BS_RESULT_LEN 64
#define UIM_MAX_PARAM_SIZE 200
#define UIM_MAX_OTA_DATA_BURST_SIZE 255
#define UIM_MAX_EF_NUM  6

#define UIM_SIM_SC_ADDRESS_LENGTH 0x0a
#define UIM_SIM_STATUS_OK     0
#define UIM_SIM_STATUS_NO_SIM 1
#define UIM_SIM_STATUS_NO_PIN 2
#define MAX_NUMBER_BCD_LEN 10

#ifdef MTK_CBP
#define MAX_EFPL_SIZE  8*2 /* Each language indicator takes 2 bytes, 8 languages should be supported at maximum according to spec C.R1001 */
#define MAX_NUMBER_FDN_BCD_LEN  20
#define UIM_EXT2_REC_ADD_DATA_TYPE  0x2
#define UIM_EXT2_REC_CALL_PARTY_SUBADD_TYPE  0x1
#define UIM_EXT2_REC_FREE_TYPE  0x0

#define UIM_ECC_NUMBER_BCD_LEN  3
#define MAX_ECC_LIST  10
#endif

#define SESSIONID_LSH(id)  (id << 5)	// the LSB 5 bits are DFIndex, and the MSB 3 bits are SessionID
#define SESSIONID_RSH(id)  (id >> 5)
#define DFINDEX(idx)       (idx & 0x1f)

/* c2k vsim data length */
#define EF_META_SIZE 32
#define EF_DATA_SIZE 48


typedef enum
{
    UIM_MF = 0,
    UIM_DF_CDMA,
    UIM_DF_TELECOM,
    UIM_DF_GSM,
    UIM_DF_PHS,
    UIM_DF_CURRENTDF,
    UIM_DF_CSIM = 7,
    UIM_DF_USIM,
    UIM_DF_ISIM,
/*child DF*/
    UIM_DF_GLOBALPHB, /*,global phb,7f10/0x5F3A*/
    UIM_DF_CSIMPHB,/*app phb ,CSIM/0x5F3A*/
    UIM_DF_USIMPHB,/*app phb ,USIM/0x5F3A*/
    UIM_DF_GSM_ACCESS, /*0x5F3B*/
    UIM_DF_MAX_NUM
} UimDFIndexT;

/* UIM APP ID is used to diffirentiate each APP. Either CSIM or RUIM is active for MD3 */
typedef enum
{
    CARD_APP_ID_ISIM,
    CARD_APP_ID_USIM,
    CARD_APP_ID_CSIM,   
    CARD_APP_ID_SIM,
    CARD_APP_ID_RUIM,    
    CARD_APP_ID_MAX_NUM,
    CARD_APP_INVALID = 0xff
} UimCardAppIdT;


/*------------------------------------------------------------------------
* Define UIM message interface constants and structures
*------------------------------------------------------------------------*/
#define UIM_CMD_MSG_CLASS   0x0000
#define UIM_NAM_MSG_CLASS   0x0100
#define UIM_APP_MSG_CLASS   0x0200
#define UIM_MNG_MSG_CLASS   0x0300

#define NAM_CST_SIZE  21 /*size of CST 6f32*/
#define UIM_MAX_NAI_NUM 16 
/* Define UIM msg command Ids */
typedef enum 
{
    UIM_RAW_CMD_MSG = UIM_CMD_MSG_CLASS,
    UIM_SELECT_MSG,
    UIM_STATUS_MSG,
    UIM_READ_BINARY_MSG,
    UIM_UPDATE_BINARY_MSG,
    UIM_READ_RECORD_MSG,
    UIM_UPDATE_RECORD_MSG,
    UIM_VERIFY_CHV_MSG,
    UIM_CHANGE_CHV_MSG,
    UIM_ENABLE_CHV_MSG,
    UIM_DISABLE_CHV_MSG,
    UIM_UNBLOCK_CHV_MSG,   
    UIM_BS_CHALLENGE_MSG,
    UIM_CONFIRM_SSD_MSG,
    UIM_RUN_CAVE_MSG,
    UIM_GENERATE_KEY_VPM_MSG,
    UIM_STORE_ESN_ME_MSG,
    UIM_TERMINAL_PROFILE_MSG,
    UIM_TERMINAL_RESPONSE_MSG,
    UIM_ENVELOPE_MSG,
    UIM_HLP_ACCESS_CHAP_REQ_MSG,
    
    UIM_3GPD_OPC_GET_MSG,   /*Get EFME3GPDOPC*/
    UIM_3GPD_OPC_UPDATE_MSG,  /*Update EFME3GPDOPC*/
    UIM_3GPD_OPM_GET_MSG,   /*Get EF3GPDOPM*/
    UIM_3GPD_OPM_UPDATE_MSG,  /*Update EF3GPDOPM*/
    UIM_3GPD_SIPCAP_GET_MSG,   /*Get EFSIPCAP*/
    UIM_3GPD_MIPCAP_GET_MSG,  /* Get EFMIPCAP */
    UIM_3GPD_SIPUPP_GET_MSG,  /* Get EFSIPUPP */
    UIM_3GPD_MIPUPP_GET_MSG,  /*Get EFMIPUPP */
    UIM_3GPD_SIPSP_GET_MSG,    /* Get EFSIPSP */
    UIM_3GPD_SIPSP_UPDATE_MSG,  /* Update EFSIPSP */
    UIM_3GPD_MIPSP_GET_MSG,        /*Get EFMIPSP*/
    UIM_3GPD_MIPSP_UPDATE_MSG,  /* Update EFMIPSP */
    UIM_3GPD_SIPPAPSS_GET_MSG,   /* Get SIPPAPSS */
    UIM_3GPD_SIPPAPSS_UPDATE_MSG, /* Update SIPPAPSS*/

#ifdef MTK_CBP
    UIM_3GPD_IPV6_CAP_GET_MSG,            /* Get EFIPV6CAP   7F25/6F77 in UIM or 7FFF/6F87 in CSIM */
    UIM_3GPD_MIP_FLAGS_GET_MSG,           /* Get EFMIPFlags  7F25/6F78 in UIM or 7FFF/6F84 in CSIM */
    UIM_3GPD_TCP_CONFIG_GET_MSG,          /* Get EFTCPCONFIG 7F25/6F79 in UIM or 7FFF/6F88 in CSIM */
    UIM_3GPD_DATA_GENERIC_CONFIG_GET_MSG, /* Get EFDCG       7F25/6F7A in UIM or 7FFF/6F89 in CSIM */    
    UIM_3GPD_UPPEXT_GET_MSG,              /* Get EFSIPUPPEXT 7F25/6F7D in UIM or 7FFF/6F85 in CSIM, referred to as 3GPDUpp in latest Spec */
#endif
    
    UIM_MS_KEY_REQUEST_MSG,                      
    UIM_KEY_GENERATION_REQUEST_MSG,         
    UIM_COMMIT_MSG,                                  
    UIM_VALIDATE_MSG,                                    
    UIM_CONFIGURATION_REQUEST_MSG,    
    UIM_DOWNLOAD_REQUEST_MSG,    
    UIM_SSPR_CONFIGURATION_REQUEST_MSG, 
    UIM_SSPR_DOWNLOAD_REQUEST_MSG,  
#ifdef MTK_CBP
    UIM_PUZL_CONFIGURATION_REQUEST_MSG,
    UIM_PUZL_DOWNLOAD_REQUEST_MSG,
    UIM_3GPD_CONFIGURATION_REQUEST_MSG,
    UIM_3GPD_DOWNLOAD_REQUEST_MSG,
	UIM_MMD_CONFIGURATION_REQUEST_MSG,
	UIM_MMD_DOWNLOAD_REQUEST_MSG,
	UIM_MMS_CONFIGURATION_REQUEST_MSG,
	UIM_MMS_DOWNLOAD_REQUEST_MSG,
	UIM_SYSTEM_TAG_CONFIGURATION_REQUEST_MSG,
	UIM_SYSTEM_TAG_DOWNLOAD_REQUEST_MSG,
	UIM_MMSS_CONFIGURATION_REQUEST_MSG,
	UIM_MMSS_DOWNLOAD_REQUEST_MSG,
    UIM_SECURE_MODE_MSG,
    UIM_FRESH_MSG,
    UIM_SERVICE_KEY_GENERATION_REQUEST_MSG,
    UIM_BCMCS_MSG,
    UIM_APPLICATION_AUTHENTION_MSG,
    UIM_UMAC_GENERATION_MSG,
    UIM_CONFIRM_KEYS_MSG,
#endif
    UIM_OTAPA_REQUEST_MSG,
    UIM_OTA_RUN_CAVE_MSG,
    UIM_PROACTIVE_REGISTER_MSG,
    UIM_PROACTIVE_POLLING_MSG,  

    UIM_HRPD_MD5_AUTH_MSG,
    UIM_CARD_TYPE_GET_MSG,
    UIM_3G_SIP_CHAP_REQ_MSG,
    UIM_3G_MIP_MNHA_AUTH_REQ_MSG,
    UIM_3G_MIP_RRQ_HASH_REQ_MSG,
    UIM_3G_MIP_MNAAA_AUTH_REQ_MSG,
    UIM_3G_AKA_AUTH_REQ_MSG,
    UIM_3G_AKA_READ_EFKeysPS_MSG,
    UIM_3G_AKA_UPDATE_EFKeysPS_MSG,
    UIM_GET_USIM_AD_MSG, /*Administrative Data*/
    UIM_GET_USIM_IMSI_MSG,
    UIM_GET_ISIM_IMPU_MSG, /*NAI for ehrpd*/
    UIM_APP_INIT_TERM_MSG, /*UIM application initialization or termination*/
#ifdef SYS_OPTION_UIM_REMOTE_ACCESS
    UIM_GET_UART_INIT_DONE_MSG,    
#endif
    UIM_EXT_INFO_GET_MSG,
    UICC_LOGI_CHAN_OPEN_MSG,
    UICC_LOGI_CHAN_CLOSE_MSG,

    UIM_RESET_MSG,    
#ifdef MTK_DEV_C2K_IRAT
    UIM_UTK_REFRESH_MSG,
    UIM_FILE_CHANGE_RSP_MSG,
    UIM_SIM_ACCESS_OPTION_MSG,
#endif /* MTK_DEV_C2K_IRAT */

#ifdef MTK_CBP
    UIM_GET_PRL_MSG,
    UIM_GET_EXT_PRL_MSG,
    UIM_GET_EFPL_MSG,    /* prefer language file, 3F00/2F05 */
#endif
    UIM_USIM_UST_DATA_GET_MSG,   /* Get ust */
    UIM_USIM_EST_DATA_GET_MSG,   /* Get est */
    UIM_USIM_ACL_DATA_GET_MSG,   /* Get acl */

    UIM_GET_NAM_DATA_MSG = UIM_NAM_MSG_CLASS,
    UIM_UPDATE_NAM_DATA_MSG,
    UIM_GET_CST_MSG,
    UIM_GET_UIMID_EUIMID_MSG,
    UIM_GET_IMSI_MSG,
#ifdef MTK_CBP
    UIM_OTA_UPDATE_NAM_MSG,/* NAM data updated by OTAPA/OTASP */
#endif
#ifdef __CARRIER_RESTRICTION__
    UIM_GET_UML_DATA_MSG,
#endif


    UIM_GET_PHB_REC_PARAMS_MSG= UIM_APP_MSG_CLASS,
    UIM_GET_PHB_REC_MSG,
    UIM_UPDATE_PHB_REC_MSG,
    UIM_ERASE_PHB_REC_MSG,
    UIM_GET_SMS_REC_PARAMS_MSG,
    UIM_GET_SMS_REC_MSG,
    UIM_UPDATE_SMS_REC_MSG,
    UIM_ERASE_SMS_REC_MSG,

    UIM_GET_STATUS_MSG,
    UIM_CARD_DETECT_MSG,

    
    /* CHV APP */
    UIM_APP_VERIFY_CHV_MSG,
    UIM_APP_CHANGE_CHV_MSG,
    UIM_APP_ENABLE_CHV_MSG,
    UIM_APP_DISABLE_CHV_MSG,
    UIM_APP_UNBLOCK_CHV_MSG,

    UIM_GET_EF_PROPERTY_MSG,
    UIM_SET_CARDTYPE_MSG,
#ifdef MTK_DEV_C2K_IRAT
    UIM_FULL_RECOVERY_MSG,
    UIM_BTSAP_CONNECT_MSG,
    UIM_BTSAP_DISCONNECT_MSG,
    UIM_BTSAP_POWER_ON_MSG,
    UIM_BTSAP_POWER_OFF_MSG,
    UIM_BTSAP_RESET_MSG,
    UIM_BTSAP_TRANSFER_APDU_MSG,
#endif
#ifdef MTK_CBP
    UIM_GET_FDN_REC_MSG,
    UIM_UPDATE_FDN_REC_MSG,
    UIM_ERASE_FDN_REC_MSG,
    UIM_SET_FDN_STATUS_MSG,
    UIM_GET_FDN_STATUS_MSG,
    UIM_GET_SMS_CAP_MSG,
    UIM_OTA_COMPLETE_IND_MSG,
#endif
    
    UIM_NOTIFY_REGISTER_MSG = UIM_MNG_MSG_CLASS,
    UIM_SET_PARM_MSG,
    UIM_GET_PARM_MSG, 
    UIM_DBM_DATA_READ_RSP_MSG,
    UIM_DBM_DATA_WRITE_ACK_MSG
#ifdef MTK_DEV_C2K_IRAT
    ,UIM_DBM_CARD_DATA_READ_RSP_MSG,
    UIM_DBM_CARD_DATA_WRITE_ACK_MSG
#ifndef MTK_PLT_ON_PC
    ,UIM_SIM_APDU_ACCESS_CNF                = MSG_ID_UIM_SIM_APDU_ACCESS_CNF,
    UIM_SIM_ERROR_IND                       = MSG_ID_UIM_SIM_ERROR_IND,
    UIM_SIM_STATUS_IND                      = MSG_ID_UIM_SIM_STATUS_IND,
    UIM_SAT_TERMINAL_RSP_CNF                = MSG_ID_UIM_SAT_TERMINAL_RSP_CNF,
    UIM_SAT_ENVELOPE_CNF                    = MSG_ID_UIM_SAT_ENVELOPE_CNF,
    UIM_SAT_PROACTIVE_CMD_IND               = MSG_ID_UIM_SAT_PROACTIVE_CMD_IND,
    UIM_SIM_APDU_ACCESS_REQ                 = MSG_ID_UIM_SIM_APDU_ACCESS_REQ,
    UIM_SAT_TERMINAL_RSP_REQ                = MSG_ID_UIM_SAT_TERMINAL_RSP_REQ,
    UIM_SAT_ENVELOPE_REQ                    = MSG_ID_UIM_SAT_ENVELOPE_REQ,
    UIM_SAT_FILE_CHANGE_RES                 = MSG_ID_UIM_SAT_FILE_CHANGE_RES,
    UIM_SIM_RESET_IND                       = MSG_ID_UIM_SIM_RESET_IND,
    UIM_SIM_SECURITY_CHANGE_IND             = MSG_ID_UIM_SIM_SECURITY_CHANGE_IND,
    UIM_SIM_3G_AKA_AUTH_REQ                 = MSG_ID_UIM_SIM_AUTHENTICATE_REQ,
    UIM_SIM_3G_AKA_AUTH_CNF                 = MSG_ID_UIM_SIM_AUTHENTICATE_CNF,
    UIM_SIM_READ_REQ                        = MSG_ID_UIM_SIM_READ_REQ,
    UIM_SIM_READ_CNF                        = MSG_ID_UIM_SIM_READ_CNF,
    BT_UIM_SIM_RESET_REQ                    = MSG_ID_BT_UIM_SIM_RESET_REQ,
    BT_UIM_SIM_RESET_CNF                    = MSG_ID_BT_UIM_SIM_RESET_CNF,
    BT_UIM_SIM_POWER_ON_REQ                 = MSG_ID_BT_UIM_SIM_POWER_ON_REQ,
    BT_UIM_SIM_POWER_ON_CNF                 = MSG_ID_BT_UIM_SIM_POWER_ON_CNF,
    BT_UIM_SIM_POWER_OFF_REQ                = MSG_ID_BT_UIM_SIM_POWER_OFF_REQ,
    BT_UIM_SIM_POWER_OFF_CNF                = MSG_ID_BT_UIM_SIM_POWER_OFF_CNF,
    UIM_SIM_MDSTATUS_UPDATE_IND             = MSG_ID_UIM_SIM_MDSTATUS_UPDATE_IND,
    UIM_PRL_READ_FINISH_IND                 = MSG_ID_UIM_PRL_READ_FINISH_IND,
    UIM_MD3_MD1_PIN_SYNC_IND                = MSG_ID_UIM_SIM_PIN_SYNC_IND,
    UIM_MD1_MD3_PIN_SYNC_IND                = MSG_ID_SIM_UIM_PIN_SYNC_IND,    
    UIM_SIM_C2K_BATCH_FILE_READ_REQ         = MSG_ID_UIM_SIM_C2K_BATCH_FILE_READ_REQ,
    UIM_SIM_C2K_BATCH_FILE_READ_CNF         = MSG_ID_UIM_SIM_C2K_BATCH_FILE_READ_CNF,
    UIM_SIM_APP_READY_IND                   = MSG_ID_UIM_SIM_APP_READY_IND
#else /* !MTK_PLT_ON_PC */
    ,UIM_SIM_APDU_ACCESS_CNF,
    UIM_SIM_ERROR_IND,
    UIM_SIM_STATUS_IND,
    UIM_SAT_TERMINAL_RSP_CNF,
    UIM_SAT_ENVELOPE_CNF,
    UIM_SAT_PROACTIVE_CMD_IND,
    UIM_SIM_APDU_ACCESS_REQ,
    UIM_SAT_TERMINAL_RSP_REQ,
    UIM_SAT_ENVELOPE_REQ,
    UIM_SAT_FILE_CHANGE_RES,
    UIM_SIM_RESET_IND,
    UIM_SIM_SECURITY_CHANGE_IND,
    UIM_SIM_3G_AKA_AUTH_REQ,
    UIM_SIM_3G_AKA_AUTH_CNF,    
    UIM_SIM_READ_REQ,
    UIM_SIM_READ_CNF,    
    BT_UIM_SIM_RESET_REQ,
    BT_UIM_SIM_RESET_CNF,
    BT_UIM_SIM_POWER_ON_REQ,
    BT_UIM_SIM_POWER_ON_CNF,
    BT_UIM_SIM_POWER_OFF_REQ,
    BT_UIM_SIM_POWER_OFF_CNF,
    UIM_SIM_MDSTATUS_UPDATE_IND,
    UIM_PRL_READ_FINISH_IND,
    UIM_MD3_MD1_PIN_SYNC_IND,
    UIM_MD1_MD3_PIN_SYNC_IND,
    UIM_SIM_APP_READY_IND
#if defined(MTK_PLT_ON_PC_UT)
    ,IRAT_REMOTE_SIM_TEST,
    IRAT_LOCAL_SIM_TEST,
    IRAT_LOCAL_SIM_CONFIG_REQ
#endif /* MTK_PLT_ON_PC_UT */
#endif /* !MTK_PLT_ON_PC */
#endif /* MTK_DEV_C2K_IRAT */
} UimMsgIdT;

/* Define UIM msg header format */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;    
} PACKED_POSTFIX  UimMsgHeaderT;
/* Define UIM msg response for UIM_CMD_MSG_CLASS format */
typedef PACKED_PREFIX struct 
{
    uint16          Sw; /* status word, 0x9000 denotes ACK, 0x0000 denotes FAULT, other value see GSM11.11 */
    uint16          Len;
#if (defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT))
    uint8           Data[256];
#else
    uint8           Data[1];
#endif
} PACKED_POSTFIX  UimRspMsgT;
/* Define UIM raw cmd msg command */
typedef PACKED_PREFIX struct 
{
   uint16          Sw; /* status word, 0x9000 denotes ACK, 0x0000 denotes FAULT, other value see GSM11.11 */
   uint8            CaveSeqId;
   uint16           CaveMsgId;
   uint16          Len;
   uint8           Data[1];
} PACKED_POSTFIX  UimRspToPsMsgT;

/* Define UIM raw cmd msg command */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT  RspInfo;
    uint8       Cmd[5];
#ifdef MTK_CBP
    uint16      Len;
#else
    uint8       Len;
#endif
    uint8       Data[1];
} PACKED_POSTFIX  UimRawCmdMsgT;

/* Define UIM select msg command */
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT       RspInfo;
   uint16           EfId;
   UimDFIndexT      DfIndex; 
} PACKED_POSTFIX  UimSelectMsgT;
/* Define UIM status msg command */
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT       RspInfo;
} PACKED_POSTFIX  UimStatusMsgT;

/* Define UIM read record msg command */
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT       RspInfo;
   uint16           EfId;
   UimDFIndexT      DfIndex; 
   uint8            RecordIndex;
} PACKED_POSTFIX  UimReadRecordMsgT;

/* Define UIM update record msg command */
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT       RspInfo;
   uint16           EfId;
   UimDFIndexT      DfIndex; 
   uint8            RecordIndex;
   uint8            Len;
   uint8            Data[1];
} PACKED_POSTFIX  UimUpdateRecordMsgT;

/* Define UIM read binary msg command */
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT       RspInfo;
   uint16           EfId;
   UimDFIndexT      DfIndex; 
   uint16           Offset;
   uint8            Len;
} PACKED_POSTFIX  UimReadBinaryMsgT;

/* Define UIM update binary msg command */
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT       RspInfo;
   uint16           EfId;
   UimDFIndexT      DfIndex; 
   uint16           Offset;
   uint8            Len;
   uint8            Data[1];
} PACKED_POSTFIX  UimUpdateBinaryMsgT;

/************************************
  CHV command message definition 
  ************************************/
/* verify CHV msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint8           ChvId;        /* Specify the CHV */
    uint8           ChvLen;
    uint8           ChvVal[8];    /* CHV value */
} PACKED_POSTFIX  UimVerifyCHVMsgT;

/* change CHV  msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint8           ChvId;        /* Specify the CHV */
    uint8           OldChvLen;
    uint8           OldChvVal[8];    /* Old CHV value */
    uint8           NewChvLen;
    uint8           NewChvVal[8];    /* New CHV value */
} PACKED_POSTFIX  UimChangeCHVMsgT;

/*    Disable CHV Msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint8           ChvLen;
    uint8           Chv1Val[8];    /* CHV1 value */
} PACKED_POSTFIX  UimDisableCHVMsgT;

/*    Enable CHV Msg*/
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;        /* Requesting task's id, mailbox, msg ID */ 
    uint8           ChvLen;
    uint8           Chv1Val[8];     /* CHV1 value */
} PACKED_POSTFIX  UimEnableCHVMsgT;

/* Unblock CHV Msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;        /* Requesting task's id, mailbox, msg ID */ 
    uint8           ChvId;          /* Specify the CHV */
    uint8           UblkChvLen;
    uint8           UblkChvVal[8];  /* Unblock CHV value */
    uint8           NewChvLen;
    uint8           NewChvVal[8];   /* New CHV value */
} PACKED_POSTFIX  UimUnblockCHVMsgT;

/***********************************************
                UTK  Commands 
************************************************/
/* UIM Terminal Profile command */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT RspInfo;  /* Requesting task's id, mailbox, msg ID */ 
    uint8      Profile[TERM_PROFILE_LEN]; /* The list of UIM Application Toolkit 
    facilities that are supported by the ME */
} PACKED_POSTFIX  UimTermProfileMsgT;

/* Define UIM Terminal Response  command */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Len;  /* Length of response data */
    uint8        Data[1]; /* Response data */
} PACKED_POSTFIX  UimTermRspMsgT;

/* Define UIM Envelope command */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Len;    /* Length of envelope data */
    uint8        Data[1];/* Envelope data */
} PACKED_POSTFIX  UimEnvelopeMsgT;
/***********************************************
        based OTASP\OPAPA Commands 
  ***********************************************/
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT   RspInfo;
   uint8        RANDSeed[20];  
   uint8        A_KEY_P_REV;
   uint8        PLen;
   uint8        GLen;
   uint8        PARAM_P[UIM_PARAM_P_LEN];
   uint8        PARAM_G[UIM_PARAM_G_LEN];    
} PACKED_POSTFIX  UimMSKeyMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Len;   
    uint8        Result[UIM_MAX_BS_RESULT_LEN];
} PACKED_POSTFIX  UimKeyGenMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  UimCommitMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;   
    uint8        Len;
    uint8        Data[UIM_MAX_PARAM_SIZE];
} PACKED_POSTFIX  UimValidateMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;   
} PACKED_POSTFIX  UimConfigurationMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;   
    uint8        Len;
    uint8        Data[UIM_MAX_PARAM_SIZE];
} PACKED_POSTFIX  UimDownloadMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;   
    uint16       ReqOffset;
    uint8        ReqMax;
} PACKED_POSTFIX  UimSSPRConfigurationMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;   
    uint8        Len;
    uint8        Data[UIM_MAX_OTA_DATA_BURST_SIZE];
} PACKED_POSTFIX  UimSSPRDownloadMsgT;

#ifdef MTK_CBP
typedef PACKED_PREFIX  union
{
  struct
  {
     uint8 blockID;
  } blockID0;
  struct
  {
     uint8 blockID; 
     uint8 reqIndex[2];
     uint8 reqMaxEntry;
  } blockID1;
  struct
  {
     uint8 blockID;
     uint8 UZ_ID[2];
     uint8 UZ_SID[2];
     uint8 reqOffset[2];
     uint8 reqMaxSize;        
  } blockID2;
  struct
  {
     uint8 blockID;
     uint8 reqIndex[2];
     uint8 reqOffset[2];
     uint8 reqMaxSize;        
  } blockID3;
} PACKED_POSTFIX  UimPuzlConfigurationBlockParamT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    UimPuzlConfigurationBlockParamT UimPuzlConfigBlockParam;
} PACKED_POSTFIX  UimPuzlConfigurationRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint8        Len;
    uint8        Data[UIM_MAX_OTA_DATA_BURST_SIZE];
} PACKED_POSTFIX  UimPuzlDownloadRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
} PACKED_POSTFIX  Uim3gpdConfigurationRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint8        Len;
    uint8        Data[UIM_MAX_OTA_DATA_BURST_SIZE];
} PACKED_POSTFIX  Uim3gpdDownloadRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
} PACKED_POSTFIX  UimMmdConfigurationRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint8        Len;
    uint8        Data[UIM_MAX_OTA_DATA_BURST_SIZE];
} PACKED_POSTFIX  UimMmdDownloadRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
} PACKED_POSTFIX  UimMmsConfigurationRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint8        Len;
    uint8        Data[UIM_MAX_OTA_DATA_BURST_SIZE];
} PACKED_POSTFIX  UimMmsDownloadRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint16       ReqOffset;
    uint8        ReqMax;
} PACKED_POSTFIX  UimSystemTagConfigurationRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint8        Len;
    uint8        Data[UIM_MAX_OTA_DATA_BURST_SIZE];
} PACKED_POSTFIX  UimSystemTagDownloadRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint16       ReqOffset;
    uint8        ReqMax;
} PACKED_POSTFIX  UimMmssConfigurationRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        Block;
    uint8        Len;
    uint8        Data[UIM_MAX_OTA_DATA_BURST_SIZE];
} PACKED_POSTFIX  UimMmssDownloadRequestMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        keyInUse;
    uint8        startStop;   /* 0x00 for start, 0x01 for stop */
    uint8        randSM[8];
} PACKED_POSTFIX  UimSecureModeMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
	uint8        putGet;     /* 0x00 for put, 0x01 for get     */
	uint16       cryptoSync; /* High octet for first Byte of cryptoSync */
} PACKED_POSTFIX  UimFreshMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
	uint16       keyID;      /* High octet for first Byte of KEY_ID */
} PACKED_POSTFIX  UimServiceKeyGenerationRequestMsgT;

typedef PACKED_PREFIX  struct 
{
   struct
   {
     uint8     BcmcsFlowIDLen;
     uint8*    BcmcsFlowIDPtr;
     uint8     BakIDLen;
     uint8*    BakIDPtr;
     uint8     SKRandLen;
     uint8*    SKRandPtr;
   }RetrieveSK;
   
   struct
   { 
     uint8     BcmcsFlowIDLen;
     uint8*    BcmcsFlowIDPtr;
     uint8     BakIDLen;
     uint8*    BakIDPtr;
     uint8     BakExpireLen;
     uint8*    BakExpirePtr;
     uint8     TKRandLen;
     uint8*    TKRandPtr;
     uint8     EncryptedBAK[16]; /* must be 16 according to 4.9 of C.S0023-D */
   }UpdateBAK;
   
   struct
   {
     uint8     BcmcsFlowIDLen;
     uint8*    BcmcsFlowIDPtr;
     uint8     BakIDLen;
     uint8*    BakIDPtr;     
   }DeleteBAK;
   
   struct
   {
     uint8     BcmcsFlowIDLen;
     uint8*    BcmcsFlowIDPtr;
     uint8     BakIDLen;
     uint8*    BakIDPtr;   
     uint8     SKRandLen;
     uint8*    SKRandPtr;
     uint8     PacketIndexLen;
     uint8*    PacketIndexPtr;
   }RetrieveSRTPSK;
   
   struct
   {
     uint8     BcmcsFlowIDLen;
     uint8*    BcmcsFlowIDPtr;
     uint8     BakIDLen;
     uint8*    BakIDPtr;   
     uint8     TimeStampLen;
     uint8*    TimeStampPtr;     
   }GenAuthSig;

   struct
   {
     uint8     RandLen;
     uint8*    RandPtr;
     uint8     ChallengeLen;
     uint8*    ChallengePtr;        
   }BCMCSAuth;

} PACKED_POSTFIX  UimBcmcsUnionT;

typedef enum
{
    BCMCS_TYPE_RETRIEVE_SK = 0x00,
    BCMCS_TYPE_UPDATE_BAK,
    BCMCS_TYPE_DELETE_BAK,
    BCMCS_TYPE_RETRIEVE_SRTP_SK,
    BCMCS_TYPE_GEN_AUTH_SIGNATURE,
    BCMCS_TYPE_BCMCS_AUTH,

    BCMCS_TYPE_INVALID = 0xFF
}BcmcsTypeT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    BcmcsTypeT   BcmcsType;
    UimBcmcsUnionT UimBcmcsUnion;
} PACKED_POSTFIX  UimBcmcsMsgT;


typedef enum
{
    APP_AUTH_CRAM_MD5 = 0x00,
    APP_AUTH_HTTP_DIGEST_MD5,
    APP_AUTH_HTTP_DIGEST_MD5_SESS,
    APP_AUTH_HTTP_DIGEST_AKAV1_MD5,
    APP_AUTH_HTTP_DIGEST_AKAV1_MD5_SESS,
    APP_AUTH_SASL_DIGEST,
    APP_AUTH_SASL_OTP,
    APP_AUTH_SASL_GSSAPI,
    APP_AUTH_MAX,
    APP_AUTH_INVALID = 0xFF
}AppAuthMechanismAlgorithmT;


typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    AppAuthMechanismAlgorithmT AppAuthMechAlgo;
    uint8        AppID;
    uint16       RealmLen;
    uint8*       Realm;
    uint16       ServerNonceLen;
    uint8*       ServerNonce;
    uint16       ClientNonceLen;
    uint8*       ClientNonce;
} PACKED_POSTFIX  UimApplicationAuthentionMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        MAC_I[4];
} PACKED_POSTFIX  UimUmacGenerationMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  UimConfirmKeysMsgT;
#endif
/* TO_DO END */


typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT   RspInfo;
    uint8        StartStop;
    uint32       RANDSeed;
    uint8        Esn[7];
} PACKED_POSTFIX  UimOTAPAMsgT;
/***********************************************
        ANSI-41-based Security-Related Commands 
  ***********************************************/
/* Base station challenge  msg command */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint32          RandSeed;    /* A random number generated by ME  */
    uint8           RandSSD[7];  /* A random number generated by BS, received
                                    in Update SSD message */
    uint8           ProcessCtrl; /* Process control */
    uint32          Esn;         /* Electronic Serial Number */
} PACKED_POSTFIX  UimBSChallengeMsgT;

/* confirm SSD msg command */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint32          AuthBS;      /* Value of AUTHBS received in Base Station 
                                    Challenge Confirmation Order */
} PACKED_POSTFIX  UimConfirmSSDMsgT;

/* Run Cave msg command */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint8           RandType;    /* Type of RandVal */
    uint32          RandVal;     /* A random number generated by BS ,
                                    Global random challenge or Unique random challenge */
    uint8           DigLen;      /* Length of digit */ 
    uint8           Digit[3];    /* A subset of (coded) dialed digits*/
    uint8           ProcessCtrl; /* Process control */
    uint32          Esn;         /* Electronic Serial Number */
    uint8           CaveSeqId;
    uint16          CaveMsgId;
} PACKED_POSTFIX  UimRunCaveMsgT;

/* Generate Key/VPM msg command */
typedef PACKED_PREFIX struct 
{
    uint8           VPMFirst;    /* First octet of VPM to be output */
    uint8           VPMLast;     /* Last octet of VPM to be output */
    uint8 *         KeyDataP;    /* the pointer to key buffer */
    uint8 *         VPMDataP;    /* the pointer to vpm buffer */
} PACKED_POSTFIX  UimGenerateKeyVPMMsgT;

/* Store ESN_ME */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT      RspInfo;    
    uint8           Len;     
    uint8           Esn[7];
    bool            UsageInd;
} PACKED_POSTFIX  UimStoreEsnMeMsgT;

/*******************Related NAM data message************************/
#ifdef __CARRIER_RESTRICTION__
typedef PACKED_PREFIX struct
{
  uint8           Encoding;
  uint8           Language;
  uint16          StringLen;
  char            String[32];
} PACKED_POSTFIX  SPNameT;

/* data of UML in UIM */
typedef PACKED struct
{
    bool            IMSI_M_Valid;   /* IMSI_M    */
    IMSIType        IMSI_Mp;
    bool            IMSI_T_Valid;   /* IMSI_T    */
    IMSIType        IMSI_Tp;
    bool            Spn_Valid;
    SPNameT         Spn;
    bool            Gid1_Valid;
    uint8           Gid1[20];
    bool            Gid2_Valid;
    uint8           Gid2[20];
}UimUmlDataT;
 
/* Response data structure of UIM_GET_UML_DATA_MSG */
typedef PACKED_PREFIX struct 
{
    UimUmlDataT         UimUmlData;    /* Nam data in UIM */ 
} PACKED_PREFIX UimGetUmlDataRspMsgT;
#endif

/* Get Nam data from UIM card */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;    
} PACKED_POSTFIX  UimGetNamDataMsgT;

/* Define Cst and EST sevice list index */
typedef enum
{
#ifdef MTK_CBP
    CHV_DISABLE_INDEX = 0, /*CHV disable function*/
	ADN_INDEX,	    /*Abbreviated Dialing Numbers*/
    FDN_INDEX,      /*Fixed Dialing Numbers*/ 
#else
    FDN_INDEX  =0,  /*Fixed Dialing Numbers*/ 
#endif
    SDN_INDEX,      /*Service Dialing Numbers (SDN)*/
    SMS_INDEX,      /*Short Message Storage (SMS)*/
    SMP_INDEX,      /*Short Message Storage parameters*/
    HRPD_INDEX,     /*HRPD*/
    SMSBROADCAST_INDEX,/*Data Download via SMS Broadcast*/
    SMSPP_INDEX,         /*Data Download via SMS-PP*/
    EUIMID_INDEX,   /*SF_EUIMID-based EUIMID*/
    MEID_INDEX,     /*MEID, only used in RUIM*/
    CALL_CONTROL_INDEX,      /*Call Control*/
    SIP_INDEX,      /*3GPD-SIP*/
    MIP_INDEX,      /*3GPD-MIP*/
    AKA_INDEX,      /*AKA*/
    OCI_INDEX,      /*Outgoing Call Information (OCI)*/
    ICI_INDEX,      /*Incoming Call Information (ICI)*/
    EST_INDEX,      /*Enabled Services Table*/
    OMH_INDEX,
    IPV6_INDEX,     /* IPv6 */
    MESSAGE_3GPD_EXT_INDEX, /* message and 3GPD extensions */
    EXT2_INDEX,
#ifdef __CARRIER_RESTRICTION__
    SPN_INDEX,      /* CDMA Service Provider Name */
    GID1_INDEX,     /* Group Identifier Level 1 */
    GID2_INDEX,     /* Group Identifier Level 2 */
#endif
    ECST_MAX_NUM
}UimECstIndexT;

/* NAM structure in UIM */
typedef PACKED_PREFIX struct
{
    uint8           COUNTsp;                    /* Call Count */
    bool            IMSI_M_Valid;    /* IMSI_M    */
    IMSIType        IMSI_Mp;       
    bool            IMSI_T_Valid;    /* IMSI_T    */
    IMSIType        IMSI_Tp;      
    uint8           ASSIGNING_TMSI_ZONE_LENsp;    /* TMSI */
    uint8           ASSIGNING_TMSI_ZONEsp[ CP_MAX_TMSI_ZONE_LEN ];
    uint32          TMSI_CODEsp;    
    uint32          TMSI_EXP_TIMEsp; 
    uint16          HOME_SIDp;            /* Analog Home SID */
    uint8           EXp;                            /* Analog Operational Parameters */
    uint32          NXTREGsp;            /* Analog Location and Registration Indicators */
    uint16          SIDsp;
    uint16          LOCAIDsp;
    bool            PUREGsp;
    uint16          SIDp[MAX_POSITIVE_SIDS]; /* CDMA Home SID, NID */
    uint16          NIDp[MAX_POSITIVE_SIDS];
    uint8           NUM_POSITIVE_SID_NIDp;
    ZoneList        ZONE_LISTsp;            /* CDMA Zone-Based Registration Indicators */
    SidNidList      SID_NID_LISTsp;        /* CDMA System/Network Registration Indicators */
    int32           BASE_LAT_REGsp;     /* CDMA Distance-Based Registration Indicators */
    int32           BASE_LONG_REGsp;
    uint16          REG_DIST_REGsp; 
    uint8           ACCOLCp;                     /* access overload class */
    uint8           MOB_TERM_HOMEp;/* Call Termination Mode Preferences */  
    uint8           MOB_TERM_SIDp;   
    uint8           MOB_TERM_NIDp;     
    uint8           SLOT_CYCLE_INDEXp;/* Suggested Slot Cycle Index */
    uint16          FIRSTCHPp;                /* Analog Channel Preferences */
    uint16          FCCA; 
    uint16          FCCB;
    uint8           NUM_FCC_SCAN;
    bool            ALLOW_OTAPA;    /* OTAPA/SPC_Enable */
    bool            NAM_LOCKp;            /* NAM_LOCK */
    uint8           MDN_NUM_DIGITS;    /* EF Mobile Directory Number */
    uint8           Mdn[16];     
    uint32          UimId;    /* UIMID */
    uint8           ECST[ECST_MAX_NUM]; /*bit 0: allocated or not,bit 1: service activated or not*/
    bool            SF_EUIMID_UsageInd;
    uint8           SF_EUIMID[NAM_MEID_SIZE];
    bool            UIM_ID_UsageInd;
    uint8           PREF_SERV_BAND0p;/* Service Preferences */
    uint8           SPC_Status;
} PACKED_POSTFIX UimNamT;

/* Response data structure of UIM_GET_NAM_DATA_MSG */
typedef PACKED_PREFIX  struct 
{
    UimNamT         UimNam;    /* Nam data in UIM */ 
} PACKED_POSTFIX  UimGetNamDataRspMsgT;

/* Request the update of NAM */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT      RspInfo;    
    UimNamT         UimNam;
} PACKED_POSTFIX UimUpdateNamDataMsgT;

/* Response of the update of NAM */
typedef PACKED_PREFIX struct
{
    bool            Ack;
} PACKED_POSTFIX UimUpdateNamDataRspMsgT;
/* add for uim page slot conflict check*/
/******************APP API Related SMS phone book msg************************/
typedef enum              /* phonebook storage */
{
    PHB_STOR_LND = 0x6f44, /*EFLND (Last number dialled)*/
    PHB_STOR_ADN = 0x6f3a, /*EFADN (Abbreviated dialling numbers)*/
    PHB_STOR_FDN = 0x6f3b, /*EFFDN (Fixed dialling numbers)*/
    PHB_STOR_SDN = 0x6f49, /*EFSDN (Service Dialling Numbers)*/
    PHB_STORE_EXT2 = 0x6f7a,
    PHB_STORE_EXT2_UIM = 0x6f4b,
    PHB_STOR_FILE_COUNT  /* File Count */
}UimPhbStorFileT;

typedef enum              /* phonebook storage */
{
    PHB_GLOBAL = 0, /*global phb*/
    PHB_CSIM = 1, /*Csim phb*/
    PHB_USIM = 2, /*Usim phb*/
    PHB_ISIM = 3, /*Isim phb*/
    PHB_MAX_LEVEL
}UimPhbLevelT;

/* Request phone book records Params*/
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo; 
    UimPhbStorFileT FileType;
    UimPhbLevelT    PhbLevel; 
} PACKED_POSTFIX  UimGetPhbRecParamsMsgT;


/* Response data structure of UIM_GET_PHB_REC_PARAMS_MSG */
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    UimPhbStorFileT FileType;
    uint8           PhbRecCount;		
    uint8           PhbPerRecSize;
#ifdef MTK_CBP
    bool            FdnEnabled;
    uint8           FdnRecCount;
    uint8           FdnRecSize;
#endif
} PACKED_POSTFIX  UimGetPhbRecParamsRspMsgT;


/* Request a record of phone book */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;  
    UimPhbStorFileT FileType;
    uint8           RecordIndex;        
    UimPhbLevelT    PhbLevel; 
} PACKED_POSTFIX  UimGetPhoneRecMsgT;

/*Response data structure of UIM_GET_PHB_REC_MSG */
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    UimPhbStorFileT FileType;
    bool            IsFree; 
    uint8           RecordIndex;
    uint8           AlphaIdentifier[MAXALPHALENGTH];
    uint8           PhoneNumber[MAX_NUMBER_BCD_LEN];
    uint8           TON;
    uint8           NPI;
} PACKED_POSTFIX  UimGetPhoneRecRspMsgT;


  /* Request the update of a record of phone book */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    UimPhbStorFileT FileType;
    uint8           RecordIndex;
    uint8           AlphaIdentifier[MAXALPHALENGTH];
    uint8           PhoneNumber[MAX_NUMBER_BCD_LEN];
    uint8           TON;
    uint8           NPI;
    UimPhbLevelT    PhbLevel; 
} PACKED_POSTFIX  UimUpdatePhoneRecMsgT;


/* Response data of UIM_UPDATE_PHB_REC_MSG */
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    UimPhbStorFileT FileType;
    uint8           RecordIndex;
} PACKED_POSTFIX  UimUpdatePhoneRecRspMsgT;

/*Request to erase  a record of phone book */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;  
    UimPhbStorFileT FileType;
    uint8           RecordIndex;        
    UimPhbLevelT    PhbLevel;
} PACKED_POSTFIX  UimErasePhoneRecMsgT;

/* Response data of UIM_ERASE_PHB_REC_MSG */
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    UimPhbStorFileT FileType;
    uint8           RecordIndex;
} PACKED_POSTFIX  UimErasePhoneRecRspMsgT;


/* Request SMS(EFSMS) records sum*/
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
} PACKED_POSTFIX  UimGetSmsRecParamsMsgT;

/* Response data structure of UIM_GET_SMS_REC_ PARAMS _MSG */
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    uint8           SmsRecCount;
    uint8           SmsPerRecSize;
    uint8           SmsNullRecLen;
#ifdef MTK_PLT_ON_PC_UT
    uint8           SmsNullRec[256]; /*pointer to record status*/
#else
    uint8           SmsNullRec[1]; /*pointer to record status*/
#endif
} PACKED_POSTFIX  UimGetSmsRecParamsRspMsgT;


/* Request a record of SMS */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    uint8           RecordIndex;
} PACKED_POSTFIX  UimGetSmsRecMsgT;

/*Response data structure of UIM_GET_SMS_REC_MSG*/
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    uint8           RecordIndex;
    uint8           Status;
    uint8           MsgLen;
    uint8           MsgData[1];     
} PACKED_POSTFIX  UimGetSmsRecRspMsgT;

 /* Request the update of SMS record */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    uint8           RecordIndex;
    uint8           Status;
    uint8           MsgLen;
    uint8           MsgData[1];    
} PACKED_POSTFIX  UimUpdateSmsRecMsgT;

 /* Response data of UIM_UPDATE_SMS_REC_MSG*/
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    uint8           RecordIndex;
} PACKED_POSTFIX  UimUpdateSmsRecRspMsgT;

 /* Request Erase SMS record */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    uint8           RecordIndex;
} PACKED_POSTFIX  UimEraseSmsRecMsgT;

 /* Response data of UIM_ERASE_SMS_REC_MSG*/
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    uint8           RecordIndex;
} PACKED_POSTFIX  UimEraseSmsRecRspMsgT;

#ifdef MTK_CBP
/* Request the capability of SMS */
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;    
} PACKED_POSTFIX  UimGetSmsCapMsgT;

/*Response data structure of UIM_GET_SMS_CAP_MSG */
typedef PACKED_PREFIX  struct 
{
    uint8           RetryPeriod;
    uint8           RetryInterval;
    uint8           Flags;
    uint8           PrefSO;
} PACKED_POSTFIX  UimGetSmsCapRspMsgT;
#endif

/***************End of APP API about PhoneBook and SMS***************/

#ifdef MTK_DEV_C2K_IRAT
/*****************************************
    define the types for UIM Bluetooth SAP
******************************************/
typedef enum
{
    BTSAP_PRO_TYPE_0 = 0x00, /*Transport protocol type T=0*/
    BTSAP_PRO_TYPE_1 = 0x01, /*Transport protocol type T=1*/
    BTSAP_PRO_TYPE_0_1 = 0x02, /*Transport protocol type T=0 and T=1*/
    BTSAP_PRO_TYPE_INVALID = 0xFF, /* Invalid transport protocol type*/
}BtsapProTypeT;

/******************Bluetooth SAP related msg************************/
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
} PACKED_POSTFIX  UimBtsapConnectMsgT,
  PACKED_POSTFIX  UimBtsapDisconnectMsgT,
  PACKED_POSTFIX  UimBtsapPowerOffMsgT;

typedef PACKED_PREFIX  struct 
{
    uint16          result;
} PACKED_POSTFIX  UimBtsapDisconnectRspMsgT,
  PACKED_POSTFIX  UimBtsapPowerOffRspMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    BtsapProTypeT   transport_protocol_type;
} PACKED_POSTFIX  UimBtsapPowerOnMsgT,
  PACKED_POSTFIX  UimBtsapResetMsgT;

typedef PACKED_PREFIX  struct 
{
    uint16          result;
    BtsapProTypeT   current_transport_protocol_type;
    uint16          atr_len;
    uint8           atr[40];
} PACKED_POSTFIX  UimBtsapPowerOnRspMsgT,
  PACKED_POSTFIX  UimBtsapResetRspMsgT;

typedef PACKED_PREFIX  struct 
{
    uint16          result;
    BtsapProTypeT   current_transport_protocol_type;
    BtsapProTypeT   supported_transport_protocol_type_capability;
    uint16          atr_len;
    uint8           atr[40];
} PACKED_POSTFIX  UimBtsapConnectRspMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    BtsapProTypeT   transport_protocol_type;
    uint16          apdu_req_len;
    uint8           apdu_req[APDU_REQ_MAX_LEN];
} PACKED_POSTFIX  UimBtsapTransferApduMsgT;

typedef PACKED_PREFIX  struct 
{
    uint16          result;
    uint16          apdu_rsp_len;
    uint8           apdu_rsp[APDU_RSP_MAX_LEN];
} PACKED_POSTFIX  UimBtsapTransferApduRspMsgT;
/***************End of bluetooth SAP related msg***************/
#endif


/*********************************
Get Uim Card status
*********************************/
#define  NO_CARD      0x00
#define  UIM_CARD     0x01
#define  SIM_CARD     0x02
#define  UIM_SIM_CARD (UIM_CARD|SIM_CARD)
#define  UICC_CARD    0x04
#define  CSIM_CARD    0x10
#define  USIM_CARD    0x20
#define  ISIM_CARD    0x40
#define  UNKNOWN_CARD 0x80

#ifdef MTK_CBP
#define  CSIM_USIM_CARD      (UICC_CARD|CSIM_CARD|USIM_CARD)
#define  USIM_ISIM_CARD      (UICC_CARD|USIM_CARD|ISIM_CARD)
#define  CSIM_USIM_ISIM_CARD (UICC_CARD|CSIM_CARD|USIM_CARD|ISIM_CARD)
#endif
#define  CARD_NOT_READY 0x100
#define  BOOTUP         0x200
#define  LINK_FAILURE   0x300
#define  CT_CARD        0x400
#define  CT_UIM_CARD    (CT_CARD|UIM_CARD)
#define  CT_UIM_SIM_CARD (CT_CARD|UIM_SIM_CARD)
#define  CT_UICC_CARD    (CT_CARD|UICC_CARD)
#ifdef MTK_CBP
#define  CT_CSIM_CARD           (CT_UICC_CARD|CSIM_CARD)
#define  CT_CSIM_USIM_CARD      (CT_UICC_CARD|CSIM_USIM_CARD)
#define  CT_CSIM_USIM_ISIM_CARD (CT_UICC_CARD|CSIM_USIM_ISIM_CARD)
#endif

/*Can not get card type because of PIN status(CHV1_ENABLED_NOT_VERIFIED or CHV1_BLOCKED or CHV1_DEADLOCKED). 
 Application needs check CHV status to decide next step(input PIN/input PUK/deadcard) . */
#define  NEED_PIN_CARD   0x1000
#ifdef MTK_DEV_C2K_IRAT
#define MD1_PIN_NEEDED_CARD  0x2000
#define MD1_SECURITY_STATUS_BUSY   0x4000
#define MD1_CARD_LOCKED      0x5000
#endif

#ifdef __CARRIER_RESTRICTION__
#define UML_LOCKED_CARD      0x8000
#define UML_CARD_REBOOT      0x8080
#endif

typedef uint16 UimCardTypeT;
/*CardStatus is compatible with the current design,but need different process to support new card type*/
typedef uint16 CardStatusT;  

/*typedef enum{
   CARD_NOT_READY = 0,
   UIM_CARD,//CDMA card
   SIM_CARD, // GSM card
   UIM_SIM_CARD,  //CDMA and GSM dual mode card
   CSIM_CARD, 
   USIM_CARD,   
   CSIM_SIM_CARD,
   USIM_SIM_CARD,   
   UNKNOWN_CARD, // unknown card 
   BOOTUP,
   LINK_FAILURE
} CardStatusT;
*/

typedef enum
{
   UICC_NOT_INITIALIZED= 0, /* Initial state after powerup */
   UICC_BOOTUP,      /* UICC driver is trying to establish initial link */
   UICC_READY,       /* UICC link established and nominal */
   UICC_NOT_READY,   /* UICC link never established. Could be no UICC card */
#if (!defined MTK_DEV_C2K_IRAT) || (!defined MTK_DEV_CARD_HOTPLUG)
   UICC_LINK_FAILURE, /* Initialization completed but lost UICC link afterwards */
#endif
#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_CARD_HOTPLUG)
   UICC_INVALID_CARD_STATUS,
#endif
} UiccCardStatusType;

typedef enum{
   CHV1_DISABLED,
   CHV1_ENABLED_NOT_VERIFIED,/*Waiting for correct chv1 entry*/
   CHV1_ENABLED_VERIFIED, /* chv is enabled and verified */
   CHV1_BLOCKED,    /*Waiting for UNBLOCK1 entry and new PIN1 code*/
   CHV1_DEADLOCKED, /* card is deadlocked */ 
   CHV1_STATUS_ENUM_MAX,
#ifdef __CARRIER_RESTRICTION__
   CHV1_DISABLED_EXT_UML, /*chv disabled and locked by uml*/
   CHV1_ENABLED_VERIFIED_EXT_UML /*chv verified and locked by uml*/
#endif
} ChvStatusT;

#ifdef MTK_CBP
/* Indicates whether UIC is OMH card type */
typedef enum{
   UIM_IS_NOT_OMH_CARD  = 0,
   UIM_IS_OMH_CARD      = 1,
   UIM_OMH_CARD_UNKNOWN 
} UimOmhCardTypeT;

/* Indicates whether IMSI_M is programmed in UIM*/
typedef enum{
   IMSI_M_NOT_PROGRAMMED = 0,
   IMSI_M_PROGRAMMED     = 1,
   IMSI_M_PROGRAM_STATE_UNKNOWN, 
} UimImsiMProgramStateT;

/* Define IMSI_M state ind message Struct */
typedef struct 
{
    UimImsiMProgramStateT  UimImsiMProgramState;
} UimImsimProgramStateIndMsgT;

extern UimOmhCardTypeT UimCheckOMHCard(void);
#endif

/* Request uim card status */
typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  UimCardStatusRecMsgT;

 /* Response to uim card status*/
typedef PACKED_PREFIX  struct 
{
   CardStatusT CardStatus;
   ChvStatusT ChvStatus;
#ifdef MTK_DEV_C2K_IRAT
   bool       UsimAppExisted;
   bool       RemoteSimSwtichStart;/* dynamic remote sim switch happen or not */
#endif
   uint8 Pin1Count;
   uint8 Pin2Count;
   uint8 Puk1Count;
   uint8 Puk2Count;
#ifdef __CARRIER_RESTRICTION__
   uint8 slotId;
   bool  isCmdaOnly;
#endif
} PACKED_POSTFIX  UimCardStatusRspMsgT;

/************************************
  APP CHV  message definition 
  ************************************/
typedef enum
{
    UIM_CHV_1 = 1,
    UIM_CHV_2
}UimChvIdT;

/* verify CHV msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    UimChvIdT       ChvId;        /* Specify the CHV */
    uint8           ChvLen;
    uint8           ChvVal[8];    /* CHV value */
#ifdef MTK_DEV_C2K_IRAT
    bool            LocalPin;     /* Whether use local PIN from NV */
#endif
} PACKED_POSTFIX  UimAppVerifyCHVMsgT;

/* change CHV  msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    UimChvIdT       ChvId;        /* Specify the CHV */
    uint8           OldChvLen;
    uint8           OldChvVal[8];    /* Old CHV value */
    uint8           NewChvLen;
    uint8           NewChvVal[8];    /* New CHV value */
} PACKED_POSTFIX  UimAppChangeCHVMsgT;

/*    Disable CHV Msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint8           Chv1Len;
    uint8           Chv1Val[8];  /* CHV1 value */
} PACKED_POSTFIX  UimAppDisableCHVMsgT;

/*    Enable CHV Msg*/
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;     /* Requesting task's id, mailbox, msg ID */ 
    uint8           Chv1Len;
    uint8           Chv1Val[8];  /* CHV1 value */
} PACKED_POSTFIX  UimAppEnableCHVMsgT;

/* Unblock CHV Msg */
typedef PACKED_PREFIX struct 
{
    ExeRspMsgT      RspInfo;        /* Requesting task's id, mailbox, msg ID */ 
    UimChvIdT       ChvId;          /* Specify the CHV */
    uint8           UblkChvLen;
    uint8           UblkChvVal[8];  /* Unblock CHV value */
    uint8           NewChvLen;
    uint8           NewChvVal[8];   /* New CHV value */
} PACKED_POSTFIX  UimAppUnblockCHVMsgT;

/* Define CHV operation response result */
typedef enum
{
    UIM_CHV_SUCCESS  =0, 
    UIM_CHV_NOT_INITIALIZE,   /* no CHV initialized */
    UIM_CHV_FAILURE_AND_PERMIT_ATTEMPT, /*  unsuccessful CHV/UNBLOCK CHV verification, at least one attempt left */
    UIM_CHV_CONTRADICTION_WITH_CHV_STATUS, /* in contradiction with CHV status */
    UIM_CHV_CONTRADICTION_WITH_INVALIDATION_STATE, /* in contradiction with invalidation status */
    UIM_CHV_FAILURE_AND_NO_ATTEMPT, /*unsuccessful CHV/UNBLOCK CHV verification, no attempt left;CHV/UNBLOCK CHV blocked*/
    UIM_CHV_FAILURE
}UimChvResultT;

/* APP CHV operation response message */
typedef PACKED_PREFIX struct
{
    UimChvResultT Result;
} PACKED_POSTFIX  UimAppChvRspMsgT;

/************************************
  UIM_MNG_MSG_CLASS  message definition 
 ************************************/
  
/* Register/Deregister  the notify message of UIM status changed */
typedef PACKED_PREFIX struct
{
    ExeRspMsgT  RspInfo;
    bool        Register;
} PACKED_POSTFIX  UimNotifyRegisterMsgT;

/* Uim notify message */
typedef PACKED_PREFIX struct
{
   UiccCardStatusType Status;
#ifdef MTK_DEV_C2K_IRAT
   bool               RemoteSimSwtichStart;/* dynamic remote sim switch happen or not */
   CardStatusT        CardStatus; /* notify card status directly only when card is not ready */
#endif
} PACKED_POSTFIX  UimNotifyMsgT;

#ifdef MTK_DEV_C2K_IRAT
typedef struct {
   ChvStatusT ChvStatus;
   uint8      Chv1LeftCount;
   uint8      Chv2LeftCount;
   uint8      Puk1LeftCount;
   uint8      Puk2LeftCount;
} UimChvStatusChangeIndMsgT;
#endif

typedef struct {
   ExeRspMsgT    RspInfo;
   uint8               ChapId;
   uint8               NAIEntryId;
   uint8               ChallengeLen;
   uint8               *pChallengeData;
} Uim3GSipChapReqMsgT;
typedef PACKED_PREFIX struct {
   ExeRspMsgT    RspInfo;   
   uint8               ChapId;
   uint8               NAIEntryId;
   uint8               ChallengeLen;
   uint8               pChallengeData[1];
} PACKED_POSTFIX  Uim3GSipChapReqEtsMsgT;

typedef struct {
   ExeRspMsgT RspInfo;
   uint8            NAIEntryId; 
   uint16            RRQLen;
   uint8            *pRRQData;
} Uim3GMipMNHAAuthReqMsgT;
typedef PACKED_PREFIX struct {
   ExeRspMsgT RspInfo;
   uint8            NAIEntryId; 
   uint16            RRQLen;
   uint8            pRRQData[1];
} PACKED_POSTFIX  Uim3GMipMNHAAuthReqEtsMsgT;

typedef struct {
   ExeRspMsgT RspInfo;
   uint16            PreRRQLen;
   /*including Preceding Mobile IP data ||MN AAA Extension Header(Type, Subtype, Length, SPI), can be programmed into Lc already*/
   uint8            *pPreRRQData;
} Uim3GMipRRQHashReqMsgT;
typedef PACKED_PREFIX struct {
   ExeRspMsgT RspInfo;
   uint16            PreRRQLen;
   uint8            pPreRRQData[1];
} PACKED_POSTFIX  Uim3GMipRRQHashReqEtsMsgT;

typedef struct {
   ExeRspMsgT       RspInfo;
   uint8            NAIEntryId;
   uint16               ChallengeLen;
   uint8               *pChallengeData;
} Uim3GMipMNAAAAuthReqMsgT;
typedef PACKED_PREFIX struct {
   ExeRspMsgT       RspInfo;
   uint8            NAIEntryId;
   uint16            ChallengeLen;
   uint8            pChallengeData[1];
} PACKED_POSTFIX  Uim3GMipMNAAAAuthReqEtsMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8           RandLen;
   uint8*          Rand;    /*RANDA*/
   uint8           AutnLen;      /* Length of AUTN (L1) */ 
   uint8 *         Autn;         /* AUTN*/   
} Uim3GAkaAuthReqMsgT;
typedef PACKED_PREFIX struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8           RandLen;
   uint8           AutnLen;      /* Length of AUTN (L1) */ 
   uint8           Rand[16];    /*RANDA*/
   uint8           Autn[16];         /* AUTN*/   
} PACKED_POSTFIX  Uim3GAkaAuthReqEtsMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
}Uim3GAkaReadEfKeysPSMsgT;
typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8           KSIPS;
   uint8           CKPS[16];
   uint8           IKPS[16];
}Uim3GAkaUpdateEfKeysPSMsgT;
typedef PACKED_PREFIX struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8           KSIPS;
   uint8           CKPS[16];
   uint8           IKPS[16];
} PACKED_POSTFIX Uim3GAkaUpdateEfKeysPSEtsMsgT;

typedef PACKED_PREFIX struct {
   ExeRspMsgT RspInfo;
   uint8 ChapId;
   uint8 ChallengeLen;
   uint8 *pChallengeData;
   bool  bFallbackCave;
   /*Indicate the type of Username is a complete NAI or MDN, if MDN, HLP will generate NAI based on it*/
   bool  *bNAI;  
   uint8 *UserNameLen;
   uint8 *UserName;
} PACKED_POSTFIX  UimHlpAccessChapReqMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDOPCGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8 opMode;   
} Uim3GPDOPCUpdateMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDOPMGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8 opMode;   
} Uim3GPDOPMUpdateMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDSIPCapGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDMIPCapGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDSIPUPPGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDMIPUPPGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDSIPSPGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8 ActNAIEntryIndex;   
} Uim3GPDSIPSPUpdateMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDMIPSPGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8 ActNAIEntryIndex;
   uint8 NumAddActNAI;
   uint8 AddActNaiEntryIndex[UIM_MAX_NAI_NUM];
} Uim3GPDMIPSPUpdateMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDSIPPAPSSGetMsgT;

#ifdef MTK_CBP
typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDIpV6CapGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDMobileIPFlagsGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDTCPConfigsGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDDataGenericConfigsGetMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} Uim3GPDUppExtGetMsgT;
#endif

typedef struct {
   uint8 NAIEntryIndex;
   uint8 SSLen;
   uint8 SS[72];
} SipPapSST;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
   uint8 NumNAI;
   SipPapSST SipPapSSRec[UIM_MAX_NAI_NUM];
} Uim3GPDSIPPAPSSUpdateMsgT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */
   uint16          FileSize;
} UimUsimFileDataGetMsgT;

#ifdef MTK_CBP
typedef struct {
   bool  isEfPlRead;                 /* indicates if MF/2F05 already read */
   uint8 UimEfPlLen;
   uint8 UimEfPlData[MAX_EFPL_SIZE]; /*MF/2F05, prefer language file content */
} UimEfPlInfoT;

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */ 
} UimGetEfPlMsgT;

#endif

typedef struct {
   ExeRspMsgT      RspInfo;      /* Requesting task's id, mailbox, msg ID */
   uint8 Index;    /*record number to be read*/
} UimIsimImpuReadMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT  RspInfo;
    uint16      EfId;
    UimDFIndexT DfIndex; 
} PACKED_POSTFIX UimGetEFPropertyMsgT;

typedef enum
{
    UIM_FILE_TRANSPARENT = 0,
    UIM_FILE_FIXED = 1,
    UIM_FILE_CYCLIC = 3
}UimEfStructureT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{   // Refer to TS 102.221 v11.0.0, Annex C for detail
    UIM_COMMAND_TYPE_CASE_1  = 1, // no data input, no data output
    UIM_COMMAND_TYPE_CASE_2  = 2, // no data input, has data output
    UIM_COMMAND_TYPE_CASE_3  = 3, // has data input, no data output
	UIM_COMMAND_TYPE_CASE_4  = 4, // has data input, has data output
	UIM_COMMAND_TYPE_UNKNOWN = 0xFF
}UimCommandTypeT;
#endif

typedef PACKED_PREFIX struct
{
    bool                Ack;
    uint16              FileId;
    uint16              FileSize;
    UimEfStructureT     EFStructure;
    uint8               RecordLen;
} PACKED_POSTFIX UimGetEFPropertyRspMsgT;

extern void UimSetAuthCmdRspAllow(bool value);

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
} PACKED_POSTFIX  UimProactiveRegiserMsgT;

typedef enum
{
    VAL_UTK = 0,
    VAL_UIM = 1,
    POLL_ID_MAX
}UimPollingIdT;

typedef PACKED_PREFIX struct
{
    uint16      TimeInterval; /* uint is "s", if =0: polling off */
    UimPollingIdT AppId; 
} PACKED_POSTFIX UimProactivePollingMsgT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT RspInfo;
    uint8 ChapId;
    uint8 ChallengeLen;
    uint8 pChallengeData[1];
} PACKED_POSTFIX UimHrpdMD5AuthMsgT;

/************************************
  Remote UICC IPC  message definition 
 ************************************/
typedef PACKED_PREFIX struct
{
    uint32   IPCMsgId;
    uint8    CTPdu_Hdr[5];
    uint8    ChanNum;
    uint16   DataLen;
    uint8    Data[1];
} PACKED_POSTFIX UiccCTpdu_t;

#ifdef Remote_UICC_EXTENSIONS
typedef PACKED_PREFIX struct
{
    uint32   IPCMsgId;
    uint16   DataLen;
    uint8    Data[1];
} PACKED_POSTFIX UiccTerRsp_t;
#endif

typedef PACKED_PREFIX struct
{
    uint32   IPCMsgId;
    uint16    Status;
    uint16   DataLen;
    uint8    Data[1];
} PACKED_POSTFIX UiccRTpdu_t;

typedef PACKED_PREFIX struct
{
    uint32   IPCMsgId;
#ifdef Remote_UICC_EXTENSIONS
    uint16   CardStatus;
#else
    uint8    CardStatus;
#endif
    uint16   CardType;
} PACKED_POSTFIX UiccStatusInd_t;

typedef PACKED_PREFIX struct
{
    uint32   IPCMsgId;
#ifdef Remote_UICC_EXTENSIONS
    uint16   CardStatus;
#else
    uint8    CardStatus;
#endif
    uint16   CardType;
    uint8    CSIMChan;
    uint8    USIMChan;
    uint8    ISIMChan;
    uint8    CSIMAid[16];// CSIM AID
    uint8    USIMAid[16];// USIM AID
    uint8    ISIMAid[16]; // ISIM AID	
    uint32   WWT;
    uint8    ATRLen;
    uint8    ATR[1];
} PACKED_POSTFIX UiccReadyInd_t;

typedef PACKED_PREFIX struct
{
    uint32  IPCMsgId;
    uint16  Len;
    uint8   Data[1];
} PACKED_POSTFIX UiccProactiveCmd_t;

/*For USIM/ISIM access*/
typedef PACKED_PREFIX struct
{
    uint32   IPCMsgId;
    uint8    ChanNum;
    uint8    Command;
    uint16   FileId;
    uint8    P1;
    uint8    P2;
    uint8    P3;
    uint8    Len;
    uint16   PathIdLen;
    uint8    Data[1];
} PACKED_POSTFIX UiccResaccReq_t;

typedef PACKED_PREFIX struct
{
    uint32   IPCMsgId;
    uint16   Status;
    uint16   Len;
    uint8    Data[1];
} PACKED_POSTFIX UiccResaccRsp_t;

typedef enum 
{
  UICC_PARM_SET_FACTORY_MODE,
  UICC_PARM_GET_FACTORY_MODE,
#ifdef MTK_DEV_C2K_IRAT
  UICC_PARM_SET_CHV_ICCID_DATA,
  UICC_PARM_GET_CHV_ICCID_DATA,
#endif  
  
  UICC_PARM_OPERATION_ID_END_LIST
} UiccParmOperationId;

typedef enum 
{
   UICC_PARM_MIN_VALUE,
   UICC_PARM_MAX_VALUE,
   UICC_PARM_DEFAULT_VALUE,
   UICC_PARM_CUSTOM_VALUE,
   UICC_PARM_OP_TYPE_LIST_END
} UiccParmOperationType;

typedef enum
{
   UICC_PARM_OPERATION_SUCCESS,
   UICC_PARM_OPERATION_FAIL_READ_NOT_ALLOWED,
   UICC_PARM_OPERATION_FAIL_WRITE_NOT_ALLOWED,
   UICC_PARM_OPERATION_PARAMETER_NOT_SUPPORTED,
   UICC_PARM_OPERATION_INTERFACE_NOT_SUPPORTED,
   UICC_PARM_OPERATION_FAIL_VALUE_OUT_OF_RANGE
} UiccParmAccessResultCode;


typedef PACKED_PREFIX struct
{
   BOOL Mode;
} PACKED_POSTFIX  UiccFactoryMode_APIStruct;

#ifdef MTK_DEV_C2K_IRAT
typedef NV_PACKED_PREFIX struct
{
#ifdef MTK_DEV_CCCI_FS
   BOOL IsICCIDValid;
   BOOL IsCHVValid;
#else
   BOOL IsICCIDValid:1;
   BOOL IsCHVValid:1;
#endif
   uint8 CHVData[8];
   uint8 ICCIDData[10];
   uint8 cardSlotID;
} NV_PACKED_POSTFIX  UiccDbmChvIccidStruct;
#endif

typedef enum
{
   DBM_SAVE_CHV_ICCID_FROM_UIM_STATE = 0x00,    /* save CHV ICCID info from UimState to first Element of DbmChvIccid[UICC_DBM_CACHE_CARD_NUM]*/
   DBM_SAVE_CHV_ICCID_FROM_PARM_LIST,           /* save CHV ICCID info from parameter list of UiccWriteCHVICCIDToNV() */
   DBM_CLEAR_ALL_CHV_ICCID_LIST,                /* clear all Elements of DbmChvIccid[UICC_DBM_CACHE_CARD_NUM] */
   DBM_CLEAR_SPECIFIED_SLOT_CHV_ICCID,          /* clear the first Entry with slotId as specified in parameter list of UiccWriteCHVICCIDToNV()*/
   DBM_INVALID_OPTION
}DbmChvIccidWriteOptionT;

typedef NV_PACKED_PREFIX struct
{
#ifdef MTK_DEV_C2K_IRAT
   UiccDbmChvIccidStruct DbmChvIccid[UICC_DBM_CACHE_CARD_NUM]; /* 0 for current Card */
#endif
} NV_PACKED_POSTFIX  UiccDbmCardDataT;


typedef NV_PACKED_PREFIX struct
{
   BOOL FactoryMode;
#ifdef MTK_DEV_C2K_IRAT
   uint8 padding[20];
#endif
} NV_PACKED_POSTFIX  UiccDbmDataT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    uint8           OperMode; /*1: init; 0:terminate*/
    uint8          Aid[16];
} PACKED_POSTFIX  UimAppInitTermMsgT;

typedef enum
{
	Info_ATRGet =0,
	Info_Max
}UimExtInfoId;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT      RspInfo; 
   UimExtInfoId infoid;
} PACKED_POSTFIX  UimExtSimInfoGetT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT      RspInfo; 
#ifdef MTK_CBP
   uint8 Aid_len;
#endif        
   uint8 Aid[16];
} PACKED_POSTFIX  UiccLogicChanOpenT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT      RspInfo; 
   uint8 Sessionid;
} PACKED_POSTFIX  UiccLogicChanCloseT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint8       RecordIndex;
} PACKED_POSTFIX UimGetSimSmsScAddrMsgT;

typedef PACKED_PREFIX  struct 
{
    bool      Ack;  /* success for TRUE; failure for FALSE */
    uint8     Length; /* SC address length */
    uint8     Tosca;  /* SC address type, 0x91:international; 0x81:Unkonw; 0xA1:National */
    char      Address[UIM_SIM_SC_ADDRESS_LENGTH];  /* SC address,in BCD code */
} PACKED_POSTFIX UimGetSimSmsScAddrRspMsgT; 

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint8       RecordIndex;
} PACKED_POSTFIX UimGetSimMdnRecMsgT;

typedef PACKED_PREFIX struct
{
   bool   Ack;
   uint8  RecordIndex;
   uint8  MdnLen;
   uint8  Mdn[MAX_NUMBER_BCD_LEN+1]; /* First byte is TON|NPI. */
} PACKED_POSTFIX UimGetSimMdnRecRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint8       RecordIndex;
   uint8       MdnLen;
   uint8       Mdn[MAX_NUMBER_BCD_LEN+1]; /* First byte is TON|NPI. */  
} PACKED_POSTFIX UimUpdateSimMdnRecMsgT;

typedef PACKED_PREFIX struct
{
   bool   Ack;
   uint8  RecordIndex;
} PACKED_POSTFIX UimUpdateSimMdnRecRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
} PACKED_POSTFIX UimSimSessReqMsgT;

typedef PACKED_PREFIX struct
{
   uint8 Status;
   uint8 SstGprs;
   uint8 SstPlmnwact;
   uint8 SstOplmnwact;
   uint8 SstHplmnact;
   uint8 SstSMSPPDataDown;
   uint8 SstSMSCBDataDown;
} PACKED_POSTFIX UimSimSessReqRspMsgT;

typedef PACKED_PREFIX  struct 
{
   ExeRspMsgT       RspInfo;
} PACKED_POSTFIX  UimResetMsgT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
  UIM_REFRESH_CSIM_INIT,
  UIM_REFRESH_CARD_RESET,
  UIM_REFRESH_FILE_CHANGE,
  UIM_REFRESH_SESSION_RESET
}UimRefreshTypeT;

typedef PACKED_PREFIX struct
{
    ExeRspMsgT  RspInfo;
    UimRefreshTypeT RefreshType;    
} PACKED_POSTFIX  UimUtkRefreshMsgT;

typedef PACKED_PREFIX struct
{
    bool Success;
} PACKED_POSTFIX  UimUtkRefreshRspMsgT;
#endif

typedef PACKED_PREFIX struct
{
   bool Ack;
} PACKED_POSTFIX  UimResetRspMsgT;

#ifdef SYS_OPTION_GSM
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint8 Rand[16];
} PACKED_POSTFIX UimRunGsmAlgoMsgT;

typedef PACKED_PREFIX struct
{
   uint16      DfId; 
   uint16      EfId;
} PACKED_POSTFIX UimEFIdT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint8    FileNum;
   UimEFIdT File[UIM_MAX_EF_NUM];
} PACKED_POSTFIX UimGetMultiEFPropMsgT;

typedef PACKED_PREFIX struct
{
   uint16      DfId; 
   uint16      EfId;
   uint16      FileSize;
   uint8       EFStructure;
   uint8       RecordLen;
} PACKED_POSTFIX UimEFInfoT;

typedef PACKED_PREFIX struct
{
   uint8  FileNum;
   UimEFInfoT FileInfo[UIM_MAX_EF_NUM];
} PACKED_POSTFIX UimGetMultiEFPropRspMsgT;
#endif

#ifdef MTK_CBP
typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;  
    bool            ActiveFdn;
    UimChvIdT       ChvId;        /* Specify the CHV */
    uint8           ChvLen;
    uint8           ChvVal[8];    /* CHV value */    
} PACKED_POSTFIX  UimSetFdnStatusMsgT;
typedef enum
{
  UIM_FDN_SET_OK,
  UIM_FDN_SET_PIN2_REQUIRED,
  UIM_FDN_SET_PIN2_PUK,
  UIM_FDN_SET_CARD_NOT_SUPPORTED,
  UIM_FDN_VERIFY_PIN2_FAIL,
  UIM_FDN_OPERATION_NOT_ALLOWED
}UimFdnSetCauseT;

typedef PACKED_PREFIX  struct 
{
    UimFdnSetCauseT SetCause;   
    bool            FdnEnabled;
    bool            Pin2Updated;
    uint8           Pin2Count;
    uint8           Puk2Count;
} PACKED_POSTFIX  UimSetFdnStatusRspMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;  
} PACKED_POSTFIX  UimGetFdnStatusMsgT;

typedef PACKED_PREFIX  struct 
{
    bool            FdnEnabled;
} PACKED_POSTFIX  UimGetFdnStatusRspMsgT;



/*Response data structure of UIM_GET_FDN_REC_MSG */
typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    UimPhbStorFileT FileType;
    bool            IsFree; 
    uint8           RecordIndex;
    uint8           AlphaIdentifier[MAXALPHALENGTH];
    uint8           PhoneNumber[MAX_NUMBER_FDN_BCD_LEN];
    uint8           TON;
    uint8           NPI;
} PACKED_POSTFIX  UimGetFdnRecRspMsgT;

typedef PACKED_PREFIX  struct 
{
    ExeRspMsgT      RspInfo;
    UimPhbStorFileT FileType;
    uint8           RecordIndex;
    uint8           AlphaIdentifier[MAXALPHALENGTH];
    uint8           PhoneNumber[MAX_NUMBER_FDN_BCD_LEN];
    uint8           TON;
    uint8           NPI;
    UimPhbLevelT    PhbLevel; 
} PACKED_POSTFIX  UimUpdateFdnRecMsgT;


typedef PACKED_PREFIX  struct 
{
    bool            Ack;
    UimPhbStorFileT FileType;
    uint8           RecordIndex;
} PACKED_POSTFIX  UimUpdateFdnRecRspMsgT;

typedef struct
{
  uint8 EccNum[UIM_ECC_NUMBER_BCD_LEN];/* ecc number value, encode with BCD format according to C.S0065 */
}UimEccContactT;

typedef PACKED_PREFIX struct
{
  uint8           NumOfEccList;
  UimEccContactT  EccList[MAX_ECC_LIST];
} PACKED_POSTFIX UimEccListIndMsgT;
#endif
#ifdef MTK_DEV_C2K_IRAT

typedef struct
{
  ExeRspMsgT   RspInfo;
  bool  ChangeSuccess;
}UimFileChangeRspMsgT;

typedef struct
{
  uim_access_option_enum SimAccessOption;
  bool                   UnlockTestSim;
  bool                   ClearLocalPin;
}UimSimAccessOptionMsgT;

#if defined(MTK_PLT_ON_PC) && defined(MTK_PLT_ON_PC_UT)
typedef struct
{
  uint16  Sw;
  uint16  Len;
  uint8   Buf[256];
}IratLocalSimTestT;

typedef struct
{
  ChvStatusT ChvStatus;
}IratTestConfigT;

typedef struct
{
  bool ATR;
}IratLocalSimConfigTestT;

typedef struct
{
  uim_access_option_enum UimAccess;
  ChvStatusT  ChvStatus;
}IratRemoteSimTestT;

#endif
#endif


typedef enum
{
   UIM_CARD_MTS = 1,
   UIM_CARD_TATA,
   UIM_CARD_RELIANCE
}UimCarrierT;


#define CARD_IS_UICC()   ((sbp_query_data_feature(SBP_DATA_RUIM) != SBP_RUIM_DISABLED) && ((UimGetCardType() & (UICC_CARD)) == UICC_CARD))
#define CARD4CDMA_IS_UICC()    ((sbp_query_data_feature(SBP_DATA_RUIM) != SBP_RUIM_DISABLED) &&((UimGetCardType() & (UICC_CARD)) == UICC_CARD))
#ifdef MTK_CBP
extern UimOmhCardTypeT          OmhCardType;
#define CARD_IS_OMH()          (OmhCardType == UIM_IS_OMH_CARD)
#endif

extern uint8           Csim_AID[16], Usim_AID[16],Isim_AID[16];
extern UimCardTypeT UimGetCardType(void);
extern void UimSetCardType(UimCardTypeT type);
extern uint8 UimGetCSTValueByIndex(UimECstIndexT index);
extern UimCarrierT UimGetCardCaiier( void );
#ifdef MTK_CBP
extern uint16 UimReadEfPl(void);
extern bool UimIsTestCard(void);
#endif



#ifdef MTK_DEV_C2K_IRAT
extern bool UimGetCardIvsrStatus(void);
extern void UimStopAllTimer(void);
#endif

#if defined(SYS_OPTION_REMOTE_UICC) && !defined(MTK_DEV_C2K_IRAT)
extern IopDataChRetStatus (*pfunc_IopReadRemoteUicc)(void ** Buff, uint16 * datalen, uint32 flags);
extern IopDataChRetStatus (*pfunc_IopWriteRemoteUicc)(void * DataP, uint16 DataLen, uint32 flags);
#endif

#ifdef __cplusplus
extern "C" {
#endif

#define UiccDbmIsUimModeDisabled() \
        _UiccDbmIsUimModeDisabled(__MODULE__, __func__, __LINE__)

extern bool _UiccDbmIsUimModeDisabled(const char *Filename, const char *FunctionName, unsigned Linenumber);


#ifdef __cplusplus
}
#endif
#endif /* UIMAPI_H */
/**Log information: \main\4 2012-04-06 05:34:47 GMT pzhang
** modify contact uim interface**/
/**Log information: \main\SMART\1 2013-04-22 02:37:48 GMT shirleyli
** HREF#22159:add function UimGetCSTValueByIndex into INC file for CSS access**/
/**Log information: \main\Trophy\Trophy_mwang_href22200\1 2013-05-09 02:54:33 GMT mwang
** HREF#22200:Add the support for CT UICC card.**/
/**Log information: \main\Trophy\2 2013-05-09 03:14:40 GMT cshen
** href#22200**/
/**Log information: \main\Trophy\Trophy_xjyang_href22322\1 2013-12-03 07:25:53 GMT xjyang
** href#22322 增加UICC类型**/
/**Log information: \main\Trophy\3 2013-12-03 07:33:28 GMT cshen
** href#22322**/
/**Log information: \main\Trophy\Trophy_yzhang_href22324\1 2013-12-05 09:30:45 GMT yzhang
** HREF#22324:India MTS/TATA ESN Tracking SMS requirement**/
/**Log information: \main\Trophy\4 2013-12-06 02:32:21 GMT cshen
** href#22324**/
/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:18:06 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\5 2013-12-10 08:33:44 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
/**Log information: \main\Trophy\Trophy_xjyang_href22336\1 2013-12-23 02:32:17 GMT xjyang
** HREF#22336**/
/**Log information: \main\Trophy\6 2013-12-23 02:48:23 GMT cshen
** href#22336**/
/**Log information: \main\Trophy\9 2014-01-17 10:10:39 GMT xxing
** HREF#22414**/
