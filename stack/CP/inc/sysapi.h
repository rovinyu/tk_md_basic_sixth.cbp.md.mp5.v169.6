/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef SYSAPI_H
#define SYSAPI_H
/*****************************************************************************

  FILE NAME:  sysapi.h

  DESCRIPTION:

    This file contains all the constants, mail message definition and
    function prototypes exported by the SYS unit.

*****************************************************************************/

#include "sysdefs.h"
#include "cssdefs.h"
#include "exeapi.h"

#if (!(defined MTK_PLT_ON_PC) && !(defined GEN_FOR_PC))  /* Disable for MoDIS CodenGen error */
/* Define functions normally accessed from the C library */
#include <string.h>
#include <stdlib.h>
#endif
#define SysMemset   memset
#define SysMemcpy   memcpy
#define SysMemcmp   memcmp
#define	SysAbs( x ) abs( (x) )

#define ALL_ST_INTS_MASK  0xFFFF  /* Mask used for all ST interrupt Mask */

/* Define variables used for System Timer interrupts */
#define ST_CPINT_125   0x0001
#define ST_CPINT_250   0x0002
#define ST_CPINT_375   0x0004
#define ST_CPINT_500   0x0008
#define ST_CPINT_625   0x0010
#define ST_CPINT_750   0x0020
#define ST_CPINT_875   0x0040
#define ST_CPINT_1000  0x0080
#define ST_CPINT_1125  0x0100
#define ST_CPINT_1250  0x0200
#define ST_CPINT_1375  0x0400
#define ST_CPINT_1500  0x0800
#define ST_CPINT_1625  0x1000
#define ST_CPINT_1750  0x2000
#define ST_CPINT_1875  0x4000
#define ST_CPINT_0     0x8000

#define ST_CPINT_ALL   0xffff

/* Define variables used for System Timer interrupts  for DO*/
#define ST_DO_CPINT_166   0x0001
#define ST_DO_CPINT_333   0x0002
#define ST_DO_CPINT_500   0x0004
#define ST_DO_CPINT_667   0x0008
#define ST_DO_CPINT_834   0x0010
#define ST_DO_CPINT_1001  0x0020
#define ST_DO_CPINT_1168  0x0040
#define ST_DO_CPINT_1335  0x0080
#define ST_DO_CPINT_1502  0x0100
#define ST_DO_CPINT_1669  0x0200
#define ST_DO_CPINT_1836  0x0400
#define ST_DO_CPINT_2003  0x0800
#define ST_DO_CPINT_2170  0x1000
#define ST_DO_CPINT_2337  0x2000
#define ST_DO_CPINT_2504  0x4000
#define ST_DO_CPINT_0     0x8000

#define ST_DO_CPINT_ALL   0xFFFF



#define INVALID_SYSTIME_SECS 0xFFFFFFFF
#define MN(name)             #name
#define M2S(name)            MN(name)
/*---------------------------------------------------------------
*  Carrier definitions and check for carrier compile options
*----------------------------------------------------------------*/
/* !!! When adding new carrier extension:                    !!! */
/* !!!  -update TOTAL_CARRIER_EXTENSIONS check below         !!! */
/* !!!  -add carrier to SysCarrierId enum  below             !!! */
/* !!!  -update function sysGetCarrierdId() in sysutils.c    !!! */
/* !!!  -update CarrierFeatureMatrix[][] in function         !!! */
/* !!!   sysIsFeatureSupported()in sysutils.c                !!! */
#define TOTAL_CARRIER_EXTENSIONS (  defined(VERIZON_EXTENSIONS) + \
                                    defined(SPRINT_EXTENSIONS) + \
                                    defined(LGT_EXTENSIONS) + \
                                    defined(KDDI_EXTENSIONS) )

#if TOTAL_CARRIER_EXTENSIONS > 1
 #error "Multiple carrier extensions(Verizon/Sprint/...) enabled in this build"
#endif

typedef enum
{
  SYS_CARRIER_GENERIC = 0,      /* no carrier extension defined */
  SYS_CARRIER_VERIZON,          /* VERIZON_EXTENSIONS           */
  SYS_CARRIER_SPRINT,           /* SPRINT_EXTENSIONS            */
  SYS_CARRIER_CHINATELECOM,     /* CHINATELECOM_EXTENSIONS      */
  SYS_CARRIER_LGT,              /* LGT_EXTENSIONS               */
  SYS_CARRIER_KDDI,             /* KDDI_EXTENSIONS              */
  SYS_NUM_CARRIERS
} SysCarrierId;

/*---------------------------------------------------------------
*  Declare constants and typedefs used in montask
*----------------------------------------------------------------*/

/* Define trace info */
typedef uint8 MonTraceInfoT;

/* Define spy info */
typedef uint8 MonSpyInfoT;

/*---------------------------------------------------------------
*  Declare constants and typedefs used for CTS in syscts.c
*----------------------------------------------------------------*/

/* Enum used to define in which interrupt context to execute CTS timer callback
** (i.e., LISR for fast/precise execution or HISR when RTOS interaction is required)
** - valid only for microsecond-based callback API */
typedef enum
{
   CTS_LISR_CALLBACK,
   CTS_HISR_CALLBACK
} SysCtsCallbackContextT;

/*---------------------------------------------------------------
*  Declare constants and typedefs used for 1x in SysTime.c
*----------------------------------------------------------------*/

/* Define interrupt types used in SysIntEnable call */
typedef enum
{
   SYS_FIQ_INT = 0x40,
   SYS_IRQ_INT = 0x80,
   SYS_ALL_INT = SYS_IRQ_INT | SYS_FIQ_INT
} SysIntT;

/* structure used for finer system time resolution (36 bits) */
typedef PACKED_PREFIX struct
{
   uint32 MostSignificant32Bits;
   uint8 LeastSignificant4Bits;
} PACKED_POSTFIX  SysSystemTimeFineT;

typedef PACKED_PREFIX struct
{
   bool Immediate;      /* do immdediate or at action time */
   SysSystemTimeT Time; /* action time in 20 ms */
} PACKED_POSTFIX  SysActionTimeT;

typedef enum
{
   SYS_FRAME_SIZE_20MS = 0,
   SYS_FRAME_SIZE_26MS = 1
} SysFrameSizeT;

typedef enum
{
   SYS_TX_SIGNAL_PERIOD_5MS,
   SYS_TX_SIGNAL_PERIOD_10MS,
   SYS_TX_SIGNAL_PERIOD_20MS
} SysTxSignalPeriodT;

typedef enum
{
   SYS_TX_SIGNAL_START_FRAME_SUBFRAME_0, /* Tx signal starts at 0 ms into 20 ms frame  */
   SYS_TX_SIGNAL_START_FRAME_SUBFRAME_1, /* Tx signal starts at 5 ms into 20 ms frame  */
   SYS_TX_SIGNAL_START_FRAME_SUBFRAME_2, /* Tx signal starts at 10 ms into 20 ms frame */
   SYS_TX_SIGNAL_START_FRAME_SUBFRAME_3  /* Tx signal starts at 15 ms into 20 ms frame */
} SysTxSignalStartFrameT;

typedef enum
{
   SYS_TIME_MODE_CDMA,
   SYS_TIME_MODE_AMPS
} SysTimeModeT;

typedef enum
{
   SLOT00_NUM = 0x00,   /* PCG or Slot 0 */
   SLOT01_NUM,  
   SLOT02_NUM, 
   SLOT03_NUM,
   SLOT04_NUM,
   SLOT05_NUM,
   SLOT06_NUM,
   SLOT07_NUM,
   SLOT08_NUM,
   SLOT09_NUM,
   SLOT10_NUM,
   SLOT11_NUM,
   SLOT12_NUM,
   SLOT13_NUM,
   SLOT14_NUM,
   SLOT15_NUM 
} SlotMaskNumberT;


typedef enum
{
   SLOT00_MASK = 0x8000, /* PCG or SLOT 0 bit mask */ 
   SLOT01_MASK = 0x0001, 
   SLOT02_MASK = 0x0002, 
   SLOT03_MASK = 0x0004, 
   SLOT04_MASK = 0x0008, 
   SLOT05_MASK = 0x0010, 
   SLOT06_MASK = 0x0020, 
   SLOT07_MASK = 0x0040, 
   SLOT08_MASK = 0x0080, 
   SLOT09_MASK = 0x0100, 
   SLOT10_MASK = 0x0200, 
   SLOT11_MASK = 0x0400, 
   SLOT12_MASK = 0x0800, 
   SLOT13_MASK = 0x1000, 
   SLOT14_MASK = 0x2000, 
   SLOT15_MASK = 0x4000 
} SlotMaskBitPositionT;

typedef PACKED_PREFIX struct
{
   bool   Enabled; 
   uint8  FrameSize;
   uint8  Cnt;
   uint8  System;
} PACKED_POSTFIX   FreeRunT;

typedef PACKED_PREFIX struct
{
   uint32 Lower32;
   uint8  Upper6;
   uint8  LpSec;
   int8   LtmOff;
   bool   DayLt;
   FreeRunT FreeRun;
} PACKED_POSTFIX  SysTimeDataT;

typedef struct
{
   uint32 Lower32;
   uint8  Upper6;
}FullSystemTimeT;


typedef PACKED_PREFIX struct
{
   uint32 Seconds;
   uint32 Milliseconds;
} PACKED_POSTFIX  SysCalendarTimeDataT;


/*---------------------------------------------------------------
*  Declare constants and typedefs used for DO in SysTime.c
*----------------------------------------------------------------*/
/* Select the mode when called SysCallbackregister or cancel */
#define SYS_ACTION_EVENT_MASK        0x1
#define SYS_ACTION_SLOT_EVENT_MASK   0x2
#define SYS_PERIODIC_SLOT_EVENT_MASK 0x4
typedef enum
{  /* bit 0 = 0: slot event 
      bit 0 = 1: action event
      bit 1 = 1: action slot event
      bit 2 = 0: single event
      bit 2 = 1: periodic event
    */
   ACTION_EVENT               = SYS_ACTION_EVENT_MASK,                         
   ACTION_SLOT_EVENT          = (SYS_ACTION_EVENT_MASK | SYS_ACTION_SLOT_EVENT_MASK), 
   SLOT_EVENT                 = 0,   
   PERIODIC_SLOT_EVENT        = SYS_PERIODIC_SLOT_EVENT_MASK, 
   ACTION_PERIODIC_SLOT_EVENT = (ACTION_SLOT_EVENT | SYS_PERIODIC_SLOT_EVENT_MASK) 
} SysEventTypeT;

typedef enum
{
   SINGLE_PCG_EVENT    = SLOT_EVENT,
   PERIODIC_PCG_EVENT  = PERIODIC_SLOT_EVENT 
} PcgEventTypeT;
#define SINGLE_SLOT_EVENT            SLOT_EVENT

/* For Tag Type */
#define SYS_TAG_FRMOFT_ADJ_MASK 0x8  /* FrameOffset adjust flag, bit 3: 0 = Offset not yet adjusted, 1 = FrameOffset adjusted */
#define SYS_TAG_FRMOFT_MASK 0x4  /* FrameOffset,   bit 2: 0 = No Offset, 1 = FrameOffset */
#define SYS_TAG_KEEP_MASK   0x2  /* Flush or Keep, bit 1: 0 = Flush Q,   1 = Keep Q      */
#define SYS_TAG_MODE_MASK   0x1  /* AirInterface,  bit 0: 0 = 1xRTT,     1 = EVDO        */
typedef enum
{
   FLUSH_QUEUE_1X = 0,  /* b000 */
   FLUSH_QUEUE_DO = 1,  /* b001 */
   KEEP_QUEUE_1X  = 2,  /* b010 */  
   KEEP_QUEUE_DO  = 3,  /* b011 */
   OFT_FLUSH_QUEUE_DO = 5, /* b101 */ 
   OFT_KEEP_QUEUE_DO  = 7  /* b111 */ 
} SysTagTypeT;

/* For SubFrame number for 1x and DO */
typedef enum
{
   SUBFRAME_NUMBER_FOR_20MS = 0x30,       /* 1X frame has 48 subframe */
   SUBFRAME_NUMBER_FOR_26MS = 0x40        /* DO frame has 64 subframe */
#ifdef MTK_CBP
  ,SUBFRAME_NUMBER_FOR_80MS = 0xC0        /* Super frame has 192 subframe */
#endif
} SubFrameCounterT;


/* Define Tracking Frame Counter based, according 1X and DO Frame counter */
typedef PACKED_PREFIX struct StTrackingStruct
{
   uint32 Lower32;      /* Lower 32 bit */
   uint8  Upper6;       /* Upper 6 bit  */
} PACKED_POSTFIX  FrameRecT;


typedef PACKED_PREFIX struct 
{
  uint32  Lower32;
  uint8   Upper6;
  int8    Subframe;
  int8    Slot;
  int16   SymbNum; 
} PACKED_POSTFIX  SysTimeFullT;


/* For Transition from 1x to do or vise versa */
#define SYS_TRANS_HW_SWITCH          0x1
#define SYS_TRANS_DUAL_UPDATE        0x2
#define SYS_TRANS_TIME_RESET         0x4
#define SYS_TRANS_TIME_RESYNC        0x8
typedef enum
{
/* (Time << 2) | (Update << 1) | HW
   Time  : 0 = Curr, 1 = Reset, 2 = Resync
   Update: 0 = SINGLE, 1 = DUAL
   HW    : 0 = NO, 1 = HW
*/
   SYS_TRANS_NO_ACTION        = 0,
   SYS_TRANS_CURR_SINGLE_HW   = 1,   /* b0001 */
   SYS_TRANS_RESET_SINGLE_HW  = 5,   /* b0101 */ 
   SYS_TRANS_RESET_DUAL_HW    = 7,   /* b0111 */ 
   SYS_TRANS_RESYNC_SINGLE_NO = 8,   /* b1000 */
   SYS_TRANS_RESYNC_SINGLE_HW = 9,   /* b1001 */
   SYS_TRANS_RESYNC_DUAL_HW   = 11   /* b1011 */
} SysTransitionTypeT;


/*----------------------------------------------------------------------------------
*  Declare constants and typedefs used for Bondout Option functions in SysBondout.c
*-----------------------------------------------------------------------------------*/
typedef enum
{
    SYS_BONDOUT_EVDOREV0_VOICE,
    SYS_BONDOUT_EVDOREV0_NO_VOICE,
    SYS_BONDOUT_EVDOREV0A_VOICE,
    SYS_BONDOUT_EVDOREV0A_NO_VOICE,
    SYS_BONDOUT_VOICE_ONLY
} SysBondoutOptionsT;

typedef enum
{
    SYS_ENH_SYSTEM_SELECT_FEATURE, /* Enhanced System Select with AutoA and AutoB options */
    SYS_ERI_FEATURE, /* Enhanced Roaming Indicator */
    SYS_EXT_SMS_INIT_FEATURE, /* External SMS Initialization */
    SYS_RSVD_ASSIGN_MODE_111_FEATURE,/* Enables IS-95A to specify Rate set 1 or 2 for data calls
                                        using Reserved 111 ASSIGN Mode. */
    SYS_GPS_FEATURE,                /* Enhanced GPS HW select with features */
    SYS_ALT_AKEY_CHKSUM_FEATURE, /* Alternate Akey Checksum algorithm */

    SYS_GPS_SUPL_FEATURE,

    SYS_PRL_ENHANCE_FOR_INT_ROAM_FEATURE,   /* PRL Enhancements for International Roaming per CDG 86 */

    SYS_REGISTRATION_THROTTLING_FEATURE, /* Registration throttling (to avoid draining battery in area 
                                            where MS continously fails to register due to Max Access 
                                            Probe failures)    */

    SYS_SAFETY_NET_REGISTRATION_FEATURE,  /* Feature to perform safety net/fallback registration in
                                           * network conditions that may result in the network not
                                           * knowing the whereabouts of the MS for extended periods.
                                           * Feature needed to compensation for poor network configurations
                                           * seen on the TATA network in India.
                                           */

    SYS_HSC_CLK_CAL_FAST_SETTLE_FEATURE,  /* Feature to be turned on on devices with fast drifting 32k clocks */

    SYS_HWD_KEYBOARD_FEATURE,    /* Hwd Keyboard. */

    SYS_CSS_1X_MAPE_HOME_SYS_AVOID_FEATURE,  /* Home system avoidance upon MAPE registration failure. 
                                             Acquire less preferred system if available */

    SYS_CSS_1X_CDG143_MAPE_SYS_AVOID_FEATURE, /* CDG143 Req. The channel over which access failed is placed last 
                                              in the channels list */ 

    SYS_CSS_1X_CDG143_REDIR_SYS_AVOID_FEATURE, /* CDG143 Req. If Redirection Scan List is exhausted and return_if_fail is FALSE, 
                                               MS shall avoid the original channel over which the redirection was received for 30 sec. */

    SYS_CSS_1X_CDG143_REDIR_MPSS_FEATURE, /* CDG143 Req 4.5.5. Perform MPSS after T_bsr_redir when acquiring a less pref. sys. following a 
                                             redirection. If no better service found, reacquire the original system from which it got redirected */
                                               
    SYS_CSS_1X_CDG143_CALL_RELEASE_MPSS_FEATURE, /* CDG143 Req 4.2.5b. MS shall perform better service reselection T_bsr_call sec after end of call */

    SYS_CSS_1X_FINISH_1ST_BSR_B4_DATACALL_FEATURE, /* MS will continue the initial BSR after acquiring a less preferred system 
                                               if the user attempts to access the network for a data call */ 

    SYS_CSS_1X_FINISH_1ST_BSR_AFTER_POWERUP_FEATURE, /* MS will continue the initial BSR after acquiring a less preferred system 
                                                        during power up if user attempts calls (except for 911) */ 

    SYS_CSS_1X_USE_NAM_FOR_VALIDATION_FEATURE, /* A 1x System not found in the PRL, not negative in NAM but found in the NAM 
                                                  positive SID NID list will be declared Home and accepted (if PRL pref_only=FALSE) */ 

    SYS_CSS_1X_VOICE_ROAM_BARRING_FEATURE, /* Ability to reject International and or Domestic roaming based on ERI */ 

    SYS_CSS_1X_LOST_CHANNEL_DWELL_FEATURE, /* The lost channel will be scanned repeatedly for the duratioon specified */
                                           /* in Phase 0 of OOSA System Lost Stage => Phase 0 scan method MUST be set to timer based. */ 

    SYS_CSS_1X_RESET_GEO_UPON_SYSLOST_FEATURE, /* Upon system lost, the MS will scan from the top of the GEO instead of continuing  */ 
                                               /* from current index */

    SYS_CSS_1X_RESTRICT_SILENTRETRY_TO_SAME_GEO_FEATURE, /* Only same Geo Systems will be accepted in Silent Retry state */

    SYS_CSS_1X_ONLY_REJECT_REDIR_IF_NEG_IN_PRL_FEATURE, /* Reject the redirected system only if found negative in PRL or NAM.  */
                                                        /* Non-system table systems will be accepted no matter what            */

    SYS_CSS_1X_ACCEPT_SIDNID_CHANGE_IN_IDLE_FEATURE, /* Do not inititate immediate better service scan in SID NID changes  */
                                                     /* as a result of idle channel hash, idle handoff                     */

    SYS_CSS_1X_USE_RESTRICTIVE_SIDNID_MATCH_FEATURE, /* When matching an SID/NID in the PRL, use more restrictive SID/NID */
                                                     /* matching using the band class and channel                         */

    SYS_CSS_DO_USE_RESTRICTIVE_SUBNET_MATCH_FEATURE, /* Restrictive subnet match feature */

    SYS_CSS_1X_MPSS_PILOT_STRENGTH_FEATURE, /* During MPSS, higher priority system can only be selected if they meet certain pilot strength criteria*/

    SYS_CSS_1X_CDG143_NEW_SYS_MPSS_FEATURE, /* CDG143 Req 4.2.6. MS shall perform better service reselection T_bsr_newsys sec after idle handoff or hash to less pref system */

    SYS_CSS_1X_ALLOW_SILENTRETRY_ON_LOWER_PRI_FEATURE, /* Allow Silent Retry on lower priority systems in same geo. */

    SYS_MMC_MBIM_API_FEATURE,  /* Microsoft MBIM API feature */

    SYS_MCC_1X_LTE_ASSOCIATION,  /* SPRINT carrier requirement */

    SYS_NUM_FEATURES
} SysFeatureId;

/* bitmap for Home System avoidance customization */
typedef enum
{
  ALLOW_AVOIDANCE_WHEN_SYS_LOST = 0,
  ALLOW_LESS_PREF_SYS_AVOIDANCE,
  ALLOW_MPSS_DURING_AVOIDANCE
} SysCssHomeSysAvoidanceOptions;


/*****************************************************************************

  FUNCTION NAME: SysGetCpsrValue

  DESCRIPTION:

  PARAMETERS:

    None

  RETURNED VALUES:

    CPSR Value.

*****************************************************************************/
extern uint32 SysGetCpsrValue(void);


/*****************************************************************************

  FUNCTION NAME: SysGetSpValue

  DESCRIPTION:

  PARAMETERS:

    None

  RETURNED VALUES:

    SP Value.

*****************************************************************************/
extern uint32 SysGetSpValue(void);

/*****************************************************************************
;
;  FUNCTION NAME: SysSaveArmRegs
;
;  DESCRIPTION:
;
;    Stores the Arm current mode regs (r0~r15 to ArmRegs)
;   
;    This function should only be used during crash dump.
;
;  C FUNCTION PROTOTYPE:
;
;    void SysSaveArmRegs(void);
;
;  PARAMETERS:
;
;    None
;
;  RETURNED VALUES:
;
;    None.
;
;*****************************************************************************/
void SysSaveArmRegs(void);

/*****************************************************************************

  FUNCTION NAME: SysSaveContextHistory

  DESCRIPTION:

    This routine save NU_Thread_Id and 32k time to ContextHistory array.
    It is called by high frequency, so use ASM routine to save time. 

  C FUNCTION PROTOTYPE:

    void SysSaveContextHistory(void);

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
void SysSaveContextHistory(void);

/****************************************************************************

 
  FUNCTION NAME: SysIntIsDisabled

  DESCRIPTION:

    This routine detect whether IRQ and FIQ is disabled.

  PARAMETERS:

    
  RETURNED VALUES:

    Return TRUE if SYS_ALL_INT is dissabled.


*****************************************************************************/
bool SysIntIsDisabled( void );


/*****************************************************************************

  FUNCTION NAME: SysJumpToCodeAddr

  DESCRIPTION:

    This routine jump to the one code section, boot, cp or UA boot etc.

  PARAMETERS:

    CpAddr: The start address of CP code section.
    R6Flag: The flag need to save to R6

  RETURNED VALUES:

    None

*****************************************************************************/
extern void SysJumpToCodeAddr(uint32 CpAddr, uint32 R6Flag);

#ifdef SYS_OPTION_NOISR_MEASUREMENTS
extern uint32 PostIntDisable(SysIntT IntType, uint32 ret);
extern uint32 PreIntEnable(SysIntT IntType, uint32 ret, uint32 param);

#ifdef BOOT_BUILDING
extern void SysIntDisable(uint32 IntType); 
extern void SysIntEnable(uint32 IntType);
#else

/* This code force to disable and enable of interrupt from the same scope */
#define  SysIntDisable(IntType)  {    uint32 param, ret;           \
                                      ret = ExeInterruptDisable(IntType);   \
                                      param = PostIntDisable(IntType,ret);

#define  SysIntEnable(IntType)        PreIntEnable(IntType,ret, param); \
                                      ExeInterruptEnable(); } 
#endif

#else

#ifdef BOOT_BUILDING
extern void SysIntDisable(uint32 IntType); 
extern void SysIntEnable(uint32 IntType);
#else

#if defined(MTK_DEV_SHARED_DBG)
#if (SYS_ASIC < SA_MT6755)
extern void ExeSharedDbgDisIF(SysIntT type);
extern void ExeSharedDbgEnIF(SysIntT type);
extern void ExeSharedDbgSaveSchedule(uint8 thrdId);
#define EXE_SHARED_DBG_SAVE_DISIF(Type)             ExeSharedDbgDisIF(Type)
#define EXE_SHARED_DBG_SAVE_ENIF(Type)              ExeSharedDbgEnIF(Type)
#define EXE_SHARED_DBG_SAVE_SCHEDULE(TId)           ExeSharedDbgSaveSchedule(TId)
#else
extern void ExeSharedDbgContext(uint8 thrdId);
extern void ExeSharedDbgInt(bool Enable, uint8 Type);
#define EXE_SHARED_DBG_SAVE_DISIF(Type)             ExeSharedDbgInt(0, Type)
#define EXE_SHARED_DBG_SAVE_ENIF(Type)              ExeSharedDbgInt(1, Type)
#define EXE_SHARED_DBG_SAVE_SCHEDULE(TId)           ExeSharedDbgContext(TId)
#endif
#endif

#ifndef MTK_PLT_ON_PC
#ifdef MTK_DEV_SSDVT
#define check_intStack(a,b) if(a!=(~b)){;}
#else
#define check_intStack(a,b) if(a!=(~b)){MonFault(MON_EXE_FAULT_UNIT, EXE_INT_MANAGE_STACK_PTR_ERR, 0, MON_HALT);}
#endif
#else
extern void check_intStack(uint32, uint32);
#endif


uint32 SysIntDisableAsm(uint32);
void SysIntEnableAsm(uint32);
void SysInfiniteLoop(void);
#ifdef SYS_OPTION_PSO_ENABLED
void SysIntDisableDirect(uint32);
void SysIntEnableDirect(uint32);
#endif

#ifdef MTK_DEV_SHARED_DBG
#if (SYS_ASIC < SA_MT6755)
/* This code force to disable and enable of interrupt from the same scope */
#define  SysIntDisable(IntType)  {    uint32 M_IntStat, M_IntStat_prot;M_IntStat = SysIntDisableAsm(IntType);M_IntStat_prot = (~M_IntStat);EXE_SHARED_DBG_SAVE_DISIF(IntType);
#define  SysIntEnable(IntType)        SysIntEnableAsm(M_IntStat);EXE_SHARED_DBG_SAVE_ENIF(IntType);check_intStack(M_IntStat,M_IntStat_prot);}
#define  SysIntDisableEnd(IntType)    if(M_IntStat!=(~M_IntStat_prot)){;}}
#else
#define  SysIntDisable(IntType)  {    volatile uint32 M_IntStat; M_IntStat = SysIntDisableAsm(IntType);EXE_SHARED_DBG_SAVE_DISIF(IntType);
#define  SysIntEnable(IntType)        EXE_SHARED_DBG_SAVE_ENIF(IntType); SysIntEnableAsm(M_IntStat);}
#define  SysIntDisableEnd(IntType)    M_IntStat;}
#endif
#else
/* This code force to disable and enable of interrupt from the same scope */
#define  SysIntDisable(IntType)  {    uint32 M_IntStat, M_IntStat_prot;M_IntStat = SysIntDisableAsm(IntType);M_IntStat_prot = (~M_IntStat);
#define  SysIntEnable(IntType)        SysIntEnableAsm(M_IntStat);check_intStack(M_IntStat,M_IntStat_prot);  }
#define  SysIntDisableEnd(IntType)    if(M_IntStat!=(~M_IntStat_prot)){;}}
#endif
#endif
#endif

#define  SysIntDisableInternal(IntType)  {    uint32 M_IntStat;M_IntStat = SysIntDisableAsm(IntType);
#define  SysIntEnableInternal(IntType)        SysIntEnableAsm(M_IntStat);}

/*****************************************************************************

  FUNCTION NAME: SysIRQIsDisabled

  DESCRIPTION:

    This routine detect whether IRQ is disabled.

  PARAMETERS:

    
  RETURNED VALUES:

    Return TRUE if SysIntDisable() is called before and SysIntEnable() 
    hasn't been called.

*****************************************************************************/
extern bool SysIRQIsDisabled(void);


/*****************************************************************************

  FUNCTION NAME: SysTimeRegister

  DESCRIPTION:

    This routine registers either a signal OR a call back function associated
    with an action time in the system time queue. The system time queue contains an order
    list of all action times.

    NOTE: The Routine function pointer MUST be set to NULL to cause a signal to be sent.

  PARAMETERS:

    ActionTime - Action time to insert into the time queue
    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.
    Routine    - Call back routine when action time is reached

  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysTimeRegister(ActionTime, TaskId, Signal, Routine) \
        __SysTimeRegister(ActionTime, TaskId, Signal, Routine, __MODULE__, __LINE__) 
extern void __SysTimeRegister(uint32 ActionTime, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32), 
                              const char *ModuleName, unsigned line);
#else
extern void SysTimeRegister(uint32 ActionTime, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32));
#endif



/****************************************************************************

  FUNCTION NAME: SysDelayUs

  DESCRIPTION:

    This routine executes a software loop for the input number of
    microseconds.  The maximum delay accepted by this routine is 30
    microseconds.  Note that interrupts are NOT disabled internally by
    this routine so the actual delay time may be extended if this routine
    is interrupted.

  C FUNCTION PROTOTYPE:

    void SysDelayUs(uint32 DelayTimeInUs);

  PARAMETERS:

    DelayTimeInUs: Number of microseconds this routine should wait before exiting.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysDelayUs(uint32 DelayTimeInUs);



/****************************************************************************

  FUNCTION NAME: SysInLisr

  DESCRIPTION:

    This routine checks the currently executing thread and determines if
    it is in the LISR range of Thread Ids.
    is interrupted.

  PARAMETERS:

    None.

  RETURNED VALUES:

    TRUE if executing in LISR, FALSE if not.

*****************************************************************************/
extern bool SysInLisr(void);

/****************************************************************************

  FUNCTION NAME: SysInHisr

  DESCRIPTION:

    This routine checks the currently executing thread and determines if
    it is in the LISR range of Thread Ids.

  PARAMETERS:

    None.

  RETURNED VALUES:

    TRUE if executing in HISR, FALSE if not.

*****************************************************************************/
extern bool SysInHisr(void);


/*****************************************************************************

  FUNCTION NAME: SysTimeRegisterCancel

  DESCRIPTION:

    This routine cancels the signal OR callback that is registered at a specified
    action time.

  PARAMETERS:

    ActionTime - Action time of registered signal to cancel.
    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.
    Routine    - Call back routine when action time is reached

  RETURNED VALUES:

    Boolean flag indicating operation status.
    Returns TRUE if action cancelled successfully.
    Returns FALSE if specified action not found in registration queue.

  Note: Input paramters must match those used when SysTimeRegister was called
  originally.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysTimeRegisterCancel(ActionTime, TaskId, Signal, Routine) \
        __SysTimeRegisterCancel(ActionTime, TaskId, Signal, Routine, __MODULE__, __LINE__)
extern bool __SysTimeRegisterCancel(uint32 ActionTime, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32),
                                    const char *ModuleName, unsigned line);
#else
extern bool SysTimeRegisterCancel(uint32 ActionTime, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32));
#endif


/*****************************************************************************

  FUNCTION NAME: SysCallbackGroupCancel

  DESCRIPTION:

    This routine cancels all the SLOT_EVENT, ACTION_EVENT, and SLOT_ACTION_EVENT
    (basically ALL) callbacks associated with the specified TaskId and Signal.

  PARAMETERS:

    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.

  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysCallbackGroupCancel(AirInterface, TaskId, Signal) \
        __SysCallbackGroupCancel(AirInterface, TaskId, Signal, __MODULE__, __LINE__)
extern void  __SysCallbackGroupCancel(SysAirInterfaceT AirInterface, ExeTaskIdT TaskId, ExeSignalT Signal,
                                   const char *ModuleName, unsigned line);
#else
extern void  SysCallbackGroupCancel(SysAirInterfaceT AirInterface, ExeTaskIdT TaskId, ExeSignalT Signal);
#endif


/*****************************************************************************

  FUNCTION NAME: SysPcgEventRegister

  DESCRIPTION:

    This routine registers either a signal OR a call back function associated
    with a PCG trigger with the system timer interrupts. The PCG event queue contains
    the number of events based on which PCG is checked.

  PARAMETERS:

    PcgMask    - System Timer event mask to identify which PCG(s) to trigger event
    EventType  - Continuous periodic or single PCG event types.
    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.
    Routine    - Call back routine when action time is reached

  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysPcgEventRegister(PcgMask, EventType, TaskId, Signal, Routine) \
        __SysPcgEventRegister(PcgMask, EventType, TaskId, Signal, Routine, __MODULE__, __LINE__)
extern void __SysPcgEventRegister(uint16 PcgMask, PcgEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32),
                                  const char *ModuleName, unsigned line);
#else
extern void SysPcgEventRegister(uint16 PcgMask, PcgEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32));
#endif


/*****************************************************************************

  FUNCTION NAME: SysTimePcgEventRegister

  DESCRIPTION:

    This routine registers either a signal OR a callback function associated
    with a PCG trigger at an defined action time. This is done with a special
    callback at the set action time which then logs the PCG event in the
    appropriate PCG event queue.

  PARAMETERS:

    ActionTime - Action time to insert PCG event callback routine into the time queue
    PcgMask    - System Timer event mask to identify which PCG(s) to trigger event
    EventType  - Continuous periodic or single PCG event types.
    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.
    Routine    - Call back routine when action time is reached

  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysTimePcgEventRegister(ActionTime, PcgMask, EventType, TaskId, Signal, Routine)  \
        __SysTimePcgEventRegister(ActionTime, PcgMask, EventType, TaskId, Signal, Routine, __MODULE__, __LINE__)
extern void __SysTimePcgEventRegister(uint32 ActionTime, uint16 PcgMask, PcgEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32),
                                      const char *ModuleName, unsigned line);
#else
extern void SysTimePcgEventRegister(uint32 ActionTime, uint16 PcgMask, PcgEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32));
#endif


/*****************************************************************************

  FUNCTION NAME: SysPcgEventRegisterCancel

  DESCRIPTION:

    This routine cancels the signal OR callback that is registered at a specified
    PCG event.

    NOTE: Input parameters must match those used when SysEventRegister was called
    originally. This function would normally be used to clear a PCG event which
    is generated continuously/periodically - single PCG events clear themselves.

  PARAMETERS:

    PcgMask    - System Timer event mask to identify which PCG(s) to clear event
    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.
    Routine    - Call back routine when action time is reached

  RETURNED VALUES:

    Boolean flag indicating operation status.
    Returns TRUE if action cancelled successfully.
    Returns FALSE if specified action not found in PCG event queue.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysPcgEventRegisterCancel(PcgMask, TaskId, Signal, Routine)  \
        __SysPcgEventRegisterCancel(PcgMask, TaskId, Signal, Routine, __MODULE__, __LINE__)
extern bool __SysPcgEventRegisterCancel(uint16 PcgMask, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32),
                                        const char *ModuleName, unsigned line);
#else
extern bool SysPcgEventRegisterCancel(uint16 PcgMask, ExeTaskIdT TaskId, ExeSignalT Signal, void (*Routine) (uint32));
#endif

/*****************************************************************************

  FUNCTION NAME: SysTimeGet

  DESCRIPTION:

    This routine gets a copy of the system time variable.

  PARAMETERS:

    None.

  RETURNED VALUES:

    uint32 - System time variable is returned.

*****************************************************************************/
extern uint32 SysTimeGet(void);



/*****************************************************************************

  FUNCTION NAME: SysTimeGetFine

  DESCRIPTION:

    This routine returns the current system time with finer resolution,
    nominally 1.25 ms resolution.

  PARAMETERS:

    None.

  RETURNED VALUES:

   Structure containing 36 bit resolution system time.

*****************************************************************************/
extern SysSystemTimeFineT SysTimeGetFine(void);

/*****************************************************************************

  FUNCTION NAME: SysTimeLisr1X

  DESCRIPTION:

    This routine is the Lisr for system time. It increments
    the system time variable and then determines if an action
    is equal to the new system time. If so the HISR routine
    is activated.

  PARAMETERS:

    StIntSrc - System Time Block Interrupt Source register contents at time of
                   interrupt.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeLisr1X(uint32 StIntSrc);


/*****************************************************************************

  FUNCTION NAME: SysTimeLisrDO

  DESCRIPTION:

    This routine is the Lisr for system time. It increments
    the system time variable and then determines if an action
    is equal to the new system time. If so the HISR routine
    is activated.

  PARAMETERS:

    StIntSrc - System Time Block Interrupt Source register contents at time of
                   interrupt.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeLisrDO(uint32 StIntSrc);


/*****************************************************************************

  FUNCTION NAME: SysTimeLisr

  DESCRIPTION:

    This routine is the Lisr for system time. It increments
    the system time variable and then determines if an action
    is equal to the new system time. If so the HISR routine
    is activated.

  PARAMETERS:

    StIntSrc - System Time Block Interrupt Source register contents at time of
                   interrupt.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeLisr(uint32 StIntSrc, uint8 AirInterface);


/*****************************************************************************

  FUNCTION NAME: SysTimeHisr

  DESCRIPTION:

    This routine is the Hisr for system time. It scans the entire system
    time queue for action time entries that match the current system time.
    If there is a match the call back routine is envoked and then the entry
    is removed from the queue.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeHisr(void);

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)

/*****************************************************************************

  FUNCTION NAME: SysFTimerLisr

  DESCRIPTION:

    This routine is the Lisr for system time. It scans the current
    FTimer  that match the current system time.
    If there is a match the call back routine is envoked and then the entry
    is removed from the queue.

  PARAMETERS:

    uint32 SysFtimerStatus

  RETURNED VALUES:

    None.

*****************************************************************************/

extern void SysFTimerLisr(uint32 SysFtimerStatus);
#endif
/*****************************************************************************

  FUNCTION NAME: SysPcgEventHisr

  DESCRIPTION:

    This routine is the Hisr for the PCG event queue.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysPcgEventHisr(void);




/*****************************************************************************

  FUNCTION NAME: SysInit

  DESCRIPTION:

    This routine initializes system time variables.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysInit(void);

/*****************************************************************************

  FUNCTION NAME: SysEnableTxSignal

  DESCRIPTION:

    This routine controls the sending of the Tx signal to any tasks registered
        using the SysRegisterTxSignal routine.


  PARAMETERS:

    TxSigPeriod     - Sets rate at which Tx signal occurs.
    TxSigStartFrame - Sets which sub-20ms frame the first Tx signal occurs in.
                      Parameter is relative to the 20 ms frame.  This is ignored
                      for a 20 ms frame.  Note this is valid for the first Tx
                      Signal; after the first one interrupts occur at each (sub)frame.

    Note: Tx signal interrupt is configured for a constant number of ms before
        the frame.


  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysEnableTxSignal(SysTxSignalPeriodT TxSigPeriod,
                              SysTxSignalStartFrameT TxSigStartFrame,
                              uint8 PcgIdx);



/*****************************************************************************

  FUNCTION NAME: SysDisableTxSignal

  DESCRIPTION:

    This routine disables the sending of the Tx signal to any tasks registered
        using the SysRegisterTxSignal routine.


  PARAMETERS:

    None.


  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysDisableTxSignal(void);



/*****************************************************************************

  FUNCTION NAME: SysEnableTimerSync

  DESCRIPTION:

    This routine enables/disables the synchronization of the CTS/DTS timers
    with the System Timer.  When enabled the System Timer block controls the
    rollover of the CTS Timer.  On disable, the CTS timer is set to rollover
    at 20 ms, but not synced to the System Timer.

  PARAMETERS:

    Enable - TRUE for enable, FALSE for disable.

  RETURNED VALUES:

    None.

*****************************************************************************/
void SysEnableTimerSync(bool Enable);


/*****************************************************************************

  FUNCTION NAME: SysSetFrameSize

  DESCRIPTION:

    This routine sets the System Timer block hardware to produce interrupts
    corresponding to either a 20 ms or 26 ms period.

  PARAMETERS:

    FrameSize - Either 20 or 26 ms frames.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysSetFrameSize(SysFrameSizeT FrameSize);



/*****************************************************************************

  FUNCTION NAME:   SysCtsCallbackClear

  DESCRIPTION:     Clears out all instances of the specified function from the
                   CTS callback routine .

  PARAMETERS:      CallbackPtr - routine to clear out of callback queue

  RETURNED VALUES: None

*****************************************************************************/
extern void SysCtsCallbackClear(void (*CallbackPtr) (void));



/*****************************************************************************

  FUNCTION NAME:   SysCtsRegisterCallback

  DESCRIPTION:     Provides a callback after the specified delay. The delay
                   is generated by using the hardware frame counter to
                   determine the current time, and then programming a single
                   shot strobe to occur after the specified delay.

                   Notes:
                      1) Assumes frame time is 20 ms.
                      2) Contains overrun checking.

  PARAMETERS:      DelayMs     - Delay in ms.
                   CallbackPtr - Function to call after the delay.

  RETURNED VALUES: Flag indicating if a callback was scheduled successfully.
                   TRUE: Callback scheduled.
                   FALSE: NO CTS Timers available, callback NOT scheduled.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysCtsRegisterCallback(DelayMs, CallbackPtr)  \
        __SysCtsRegisterCallbackUsec(((DelayMs) * 1000), CTS_HISR_CALLBACK,  CallbackPtr, __MODULE__, __LINE__)
#else
#define SysCtsRegisterCallback(DelayMs, CallbackPtr) \
         SysCtsRegisterCallbackUsec(((DelayMs) * 1000), CTS_HISR_CALLBACK,  CallbackPtr)
#endif



/*****************************************************************************

  FUNCTION NAME:   SysCtsRegisterCallbackUsec

  DESCRIPTION:     Provides a callback after the specified delay. The delay
                   is generated by using the hardware frame counter to
                   determine the current time, and then programming a single
                   shot strobe to occur after the specified delay.

                   Notes:
                      1) Contains overrun checking.
                      2) Minimum delay of 100usec is required due to resolution
                         of 80kHz counter value.
                      3) Resolution of 80kHz timer effectively "truncates" usec callbacks
                         to units of 12.5usec ticks.
                      4) Specifying the LISR context will cause the system to crash if
                         the registered callback attempts to use any EXE/RTOS functionality
                         (e.g. - Intertask messagings/signals or MonFaults) This should
                         ONLY be used for time-critical operations that does not involve
                         the RTOS.

  PARAMETERS:      DelayUsec   - Delay in usec.
                   Context     - Context in which to execute callback (i.e., LISR or HISR)
                   CallbackPtr - Function to call after the delay.

  RETURNED VALUES: Flag indicating if a callback was scheduled successfully.
                   TRUE:  Callback scheduled.
                   FALSE: NO CTS Timers available, callback NOT scheduled.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#ifndef MTK_PLT_ON_PC_UT
#define SysCtsRegisterCallbackUsec(DelayUsec, Context, CallbackPtr)  \
        __SysCtsRegisterCallbackUsec(DelayUsec, Context, CallbackPtr, __MODULE__, __LINE__)
extern bool __SysCtsRegisterCallbackUsec(uint32 DelayUsec, SysCtsCallbackContextT Context, void (*CallbackPtr) (void),
                                         const char *ModuleName, unsigned line);
#else
#define SysCtsRegisterCallbackUsec(DelayUsec, Context, CallbackPtr)
#endif
         
#else
extern bool SysCtsRegisterCallbackUsec(uint32 DelayUsec, SysCtsCallbackContextT Context, void (*CallbackPtr) (void));
#endif



/*****************************************************************************

  FUNCTION NAME:   SysCtsRegisterCallbackAbs

  DESCRIPTION:     Provides a callback when the hardware frame counter reaches
                   the specified value. This is implemented by programming a
                   single shot strobe to occur at the specified count.

                   Notes:
                      1) Assumes frame time is 20 ms.
                      2) Contains overrun checking.
                      3) Uses same CTS as SysCtsRegisterCallbackDly


  PARAMETERS:      AbsCount    - Frame counter value to generate callback
                   CallbackPtr - Function to call after the delay.

  RETURNED VALUES: None

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysCtsRegisterCallbackAbs(AbsCount, CallbackPtr)  \
        __SysCtsRegisterCallbackAbs(AbsCount, CallbackPtr, __MODULE__, __LINE__)
extern bool __SysCtsRegisterCallbackAbs(uint32 AbsCount, void (*CallbackPtr) (void),
                                        const char *ModuleName, unsigned line);
#else
extern bool SysCtsRegisterCallbackAbs(uint32 AbsCount, void (*CallbackPtr) (void));
#endif

/*****************************************************************************
 
  FUNCTION NAME: SysCtsPeriodicTimerEnable
  
  DESCRIPTION:

    This routine enables/disables the CTS periodic timer.  This timer is intended
    to be used to count system time during AMPS mode.  It generates an interrupt
    at approximately 20 ms intervals.
    
  PARAMETERS:

    Enable - TRUE for enable, FALSE for disable.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern bool SysCtsPeriodicTimerEnable( bool Enable, void (*CallbackPtr)(void));


/*****************************************************************************

  FUNCTION NAME: SysTimeReset

  DESCRIPTION:

    This routine re-initializes system time variables and clears all callback
    queues.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeReset(void);

/*****************************************************************************

  FUNCTION NAME: SysTimeAdcMeasEnable

  DESCRIPTION:

    This function enables or disables the system time interrupts used for
    Aux ADC measurements.

  PARAMETERS:

    Enable - Enable/Disable flag.
    AdcMeasCallbackPtr - Pointer to ADC Measurement function to call on ST interrupt.
                         Only required if enabled.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeAdcMeasEnable(bool Enable, void (*AdcMeasCallbackPtr) (uint32));

/*****************************************************************************

  FUNCTION NAME: SysTimeDataGet

  DESCRIPTION:

    This routine retrieves the current system time data including the 38 bit
    system time in 20 ms units and the other information most recently received
    in the sync channel message.

  PARAMETERS:

    TimeDataP - Pointer to Time data structure to fill in with current data.

  RETURNED VALUES:

    bool - TRUE if valid data was filled in.  FALSE if valid data does NOT exist.

*****************************************************************************/
extern bool SysTimeDataGet(SysTimeDataT *TimeDataP);



/*****************************************************************************

  FUNCTION NAME: SysTimeSync

  DESCRIPTION:

    This routine is used to synchronize the system time variables received on
    the sync channel with the data stored by SYS.

  PARAMETERS:

    LpSec             : Leap seconds as received in sync channel message.
    LtmOff            : Local time offset as received in sync channel message
                        (units of signed 30 min).
    DayLt             : Daylight savings time boolean flag.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeSync(uint8 *FullSystemTimeP,
                        uint8 LpSec,
                         int8 LtmOff,
                         bool DayLt);

/*****************************************************************************
 
  FUNCTION NAME: SysTimeDataSync

  DESCRIPTION:

    This routine is used to synchronize the system time data passed as a 
    parameter with the calendar system time data stored by SYS.

  PARAMETERS:
      
    FullSystemTimeP   : Pointer to 38 bits of system time, in 20 msec units.
    LpSec             : Leap seconds as received in sync channel message.
    LtmOff            : Local time offset as received in sync channel message
                        (units of signed 30 min).
    DayLt             : Daylight savings time boolean flag.
    
    None.

  RETURNED VALUES:

    None.
 
*****************************************************************************/
void SysTimeDataSync(uint8 *FullSystemTimeP, uint8 LpSec, int8 LtmOff, bool DayLt);

/*****************************************************************************

  FUNCTION NAME: SysTimeUpper6BitsGet

  DESCRIPTION:

    This routine retrieves the upper 6 bits of the current system time kept in
    20 ms units, making the full system time 38 bits.

  PARAMETERS:

    None.

  RETURNED VALUES:

    Upper 6 bits of system time (right justified in the 8 bits returned).

*****************************************************************************/
extern uint8 SysTimeUpper6BitsGet(void);




/*****************************************************************************

  FUNCTION NAME: SysTimeFullGet

  DESCRIPTION:

    This routine retrieves the current system time in 80 ms units.
    It is returned in the same format as that received on the sync channel,
    36 bits contained in a five byte array with the data left justified.



  PARAMETERS:

    FullSystemTimeP - Pointer to 5 byte array for return data.

  RETURNED VALUES:

    Format:
    FullSystemTimeP[0] => Current System Time, bits 35-28
    FullSystemTimeP[1] => Current System Time, bits 27-20
    FullSystemTimeP[2] => Current System Time, bits 19-12
    FullSystemTimeP[3] => Current System Time, bits 11-4
    FullSystemTimeP[4] => Current System Time, bits 3-0
                          (contained in bits 7-4 of this byte)


*****************************************************************************/
extern void SysTimeFullGet(uint8 *FullSystemTimeP);

/*****************************************************************************

  FUNCTION NAME: SysTimeDataCopy

  DESCRIPTION:

  PARAMETERS:

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeDataCopy(SysAirInterfaceT AirInterface);

/*****************************************************************************

  FUNCTION NAME: SysTimeDataFlush

  DESCRIPTION:

  PARAMETERS:

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeDataFlush(SysAirInterfaceT AirInterface);

/*****************************************************************************

  FUNCTION NAME: SysTime32BitTimeCalc

  DESCRIPTION:

    This routine calculates the system time in 20 ms, given the system
    time in sync channel message format (36 bits in 80 ms units, left
    justified) and returns the lower 32 bits of the calculated value.



  PARAMETERS:

    FullSystemTimeP - Pointer to 5 byte system time array.
    Format:
    FullSystemTimeP[0] => Current System Time, bits 35-28
    FullSystemTimeP[1] => Current System Time, bits 27-20
    FullSystemTimeP[2] => Current System Time, bits 19-12
    FullSystemTimeP[3] => Current System Time, bits 11-4
    FullSystemTimeP[4] => Current System Time, bits 3-0
                          (contained in bits 7-4 of this byte)

  RETURNED VALUES:

    Lower 32 bits of system time in 20 ms units.

*****************************************************************************/
extern uint32 SysTime32BitTimeCalc(uint8 *FullSystemTimeP);

/*****************************************************************************

  FUNCTION NAME: SysTimeAdjust

  DESCRIPTION:

    This routine adjusts the system time variables to new values given a delta.

  PARAMETERS:

    AdjustTicks - System timer ticks to adjust (always positive).

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeAdjust(uint32 AdjustTicks);

/*****************************************************************************

        NAME
                SysRndLC
                
        SYNOPSIS
                uint32
                SysRndLC( uint32 Seed )
        
        DESCRIPTION
                A pseudorandom number generator as described by Donald Knuth, The Art
                of Computer Programming, Vol.2, Seminumerical Algorithms, pp. 15-24,
                156-157.
                
                A Mersenne Twister implementation would result in a betetr generator. This
                version is good enough for testing.

        PARAMETERS
                Seed - != 0, will initialize the generator; == 0, will return a pseudo-
                random number

        RETURNS
                A pseudorandom number between 0 and 0x7FFFFFFF

*****************************************************************************/
extern uint32 SysRndLC(uint32 Seed);

/*****************************************************************************

        FUNCTION  : SysTime2Secs

        DESCRIPTION:Converts system time data into number of seconds since
                    the beginning of System Time.  System time data includes
                    local time offset, leap seconds, and daylight savings time
                    indicator.

        PARAMETERS: TimeDataP

                    !!!!IMPORTANT!!!!
                    TimeDataP must be initialized by SysTimeDataGet(TimeDataP).
                    In other words, SysTimeDataGet(TimeDataP) must be called 
                    right before this function.

        RETURNS:    uint32 - Number of seconds since start of System Time.
                    Returns 0xFFFFFFFF if not valid.

  ***********************************************************************
  * NOTE: THIS FUNCTION WILL NOT RETURN CORRECT VALUES FOR SYSTEM TIMES *
  *       CORRESPONDING TO A DATE LATER THAN FEB 12, 2067 (1X) or       *
  *       31 DEC, 2115 (DO).                                            *
  *       SEE EMBEDDED COMMENTS IN THIS FUNCTION FOR MORE DETAILS.      *
  ***********************************************************************

*****************************************************************************/
extern SysCalendarTimeDataT SysTime2Secs(SysTimeDataT* SysTimeData);



/*****************************************************************************

  FUNCTION NAME: SysCallbackRegister

  DESCRIPTION:

    This routine registers either a signal OR a call back function associated
    with an action time or Slot/PCG in the system time queue. The system time
    queue contains an order list of all action times and contains Slot masks
    without order.

    NOTE: The Routine function pointer MUST be set to NULL to cause a signal to be sent.

  PARAMETERS:

    ActionTime - Action time 
    SlotMask   - System Timer event mask to identify which Slot(s) to trigger event
    TagType    - Identify the Air interface with Quese operation for Transition	such as Flush or Keep.
    Type       - Signal type such as single or periodic shot.
    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.
    Routine    - Call back routine when action time is reached

  RETURNED VALUES:

    None.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysCallbackRegister(ActionTime, SlotNum, TagType, EventType, TaskId, Signal, Routine)  \
        __SysCallbackRegister(ActionTime, SlotNum, TagType, EventType, TaskId, Signal, Routine, __MODULE__, __LINE__)
extern bool __SysCallbackRegister(uint32 ActionTime, uint16 SlotNum, SysTagTypeT TagType,
                                  SysEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal,
                                  void (*Routine) (uint32),
                                  const char *ModuleName, unsigned line);
#else
extern bool SysCallbackRegister(uint32 ActionTime, uint16 SlotNum, SysTagTypeT TagType,
                                SysEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal,
                                void (*Routine) (uint32));
#endif


/*****************************************************************************

  FUNCTION NAME: SysCallbackRegisterCancel

  DESCRIPTION:

    This routine registers either a signal OR a call back function associated
    with an action time or Slot in the system time queue. The system time queue contains
    an order list of all action times and contains Slot mask without order.

    NOTE: The Routine function pointer MUST be set to NULL to cause a signal to be sent.

  PARAMETERS:

    ActionTime - Action time
    SlotMask   - System Timer event mask to identify which Slot(s) to trigger event
    TagType    - Identify the Air interface with Quese operation for Transition	such as Flush or Keep.
    TaskId     - Task ID of task to send signal.
    Signal     - Signal to send.
    Routine    - Call back routine when action time is reached

  RETURNED VALUES:

    Boolean flag indicating operation status.
    Returns TRUE if action cancelled successfully.
    Returns FALSE if specified action not found in registration queue.

  Note: Input paramters must match those used when SysCallbackRegister was called
  originally.

*****************************************************************************/
#ifdef SYS_DEBUG_FAULT_FILE_INFO
#define SysCallbackRegisterCancel(ActionTime, SlotNum, TagType, EventType, TaskId, Signal, Routine)  \
        __SysCallbackRegisterCancel(ActionTime, SlotNum, TagType, EventType, TaskId, Signal, Routine, __MODULE__, __LINE__)
extern bool __SysCallbackRegisterCancel(uint32 ActionTime, uint16 SlotNum, SysTagTypeT TagType,
                                        SysEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal, 
                                        void (*Routine) (uint32),
                                        const char *ModuleName, unsigned line);
#else
extern bool SysCallbackRegisterCancel(uint32 ActionTime, uint16 SlotNum, SysTagTypeT TagType,
                                      SysEventTypeT EventType, ExeTaskIdT TaskId, ExeSignalT Signal, 
                                      void (*Routine) (uint32));
#endif



/*****************************************************************************

  FUNCTION NAME: SysDoTimeGet

  DESCRIPTION:

    This routine gets a copy of the system time variable as 26msec unit.

  PARAMETERS:

    None.

  RETURNED VALUES:

    uint32 - System time variable is returned.

*****************************************************************************/
extern uint32 SysDoTimeGet( bool useFrameOffset );


/*****************************************************************************

  FUNCTION NAME: SysDoTimeSet

  DESCRIPTION:

    This routine set the system time variable to a new value in 26msec unit.

  PARAMETERS:

    NewSysTime - A new value for system time.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysDoTimeSet(uint32 NewSysTime);



/*****************************************************************************

  FUNCTION NAME: SysDoTimeGetSubframe

  DESCRIPTION:

    Returns Subframe: a 0-11 subframe count for an 80ms interval. 

  PARAMETERS:

    UseFrameOffset: Set to TRUE to subtrace FrameOffset from the SYS unit hardware subframe.

  RETURNED VALUES:
    int8 

*****************************************************************************/
extern int8 SysDoTimeGetSubframe( bool UseFrameOffset );


/*****************************************************************************

  FUNCTION NAME: SysDoTimeInSlotGet

  DESCRIPTION:

    This routine returns the current system time in units of slots. 
    Slots are 1.66ms duration.

  PARAMETERS:

    None.

  RETURNED VALUES:

   Structure containing 36 bit resolution system time.

*****************************************************************************/
extern uint64 SysDoTimeInSlotGet( bool UseFrameOffset );


/*****************************************************************************

  FUNCTION NAME: SysDoTimeSync

  DESCRIPTION:

    This routine is used to synchronize the system time variables received on
    the sync channel with the data stored by SYS.

  PARAMETERS:

    FullSystemTimeP   : System Time in Sync Channel in 26.67 ms units.
    LpSec             : Leap seconds as received in sync channel message.
    LtmOff            : Local time offset as received in sync channel message
                        (units of signed 30 min).
    DayLt             : Daylight savings time boolean flag.

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysDoTimeSync(FrameRecT FullSystemTime, uint8 LpSec, int8 LtmOff, bool DayLt);

/*****************************************************************************

  FUNCTION NAME: SysDoTimeUpdateLtfLps

  DESCRIPTION:

    This routine writes the LeapSecs and LocalTimeOffset into global structure 
    SysTimeData.

  PARAMETERS:

    FrameOffset - in units of slots

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void  SysDoTimeUpdateLtfLps(uint8 LpSec, uint16 LtmOff);

/*****************************************************************************

  FUNCTION NAME: SysTimeStatusUpdate

  DESCRIPTION:

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeStatusUpdate(SysAirInterfaceT AirInterface, uint8 UpdateReason);


/*****************************************************************************

  FUNCTION NAME: SysTimeResetEVDO

  DESCRIPTION: Resets the system time for DO

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeResetEVDO(void);

/*****************************************************************************

  FUNCTION NAME: SysDoTimeFullSet

  DESCRIPTION:

    This routine sets the current system time.

  PARAMETERS:

    FrameRecT FullSystemTime - Structure to 37 bits input system time array.

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysDoTimeFullSet(FrameRecT FullSystemTime);


/*****************************************************************************

  FUNCTION NAME: SysDoTimeFullGet

  DESCRIPTION:

    Returns:
    - frame count in units of 26.66ms frame 
    - subframe count within 26.66ms frame [0-3]
    - slot count within 26.66ms frame [0-15]
    - symbol number within 26.66ms frame [0-511]

  PARAMETERS:

    none

  RETURNED VALUES:
    See SysTimeFullT in sysapi.h

*****************************************************************************/
extern SysTimeFullT SysDoTimeFullGet(bool UseFrameOffset);


/*****************************************************************************

  FUNCTION NAME: SysSetDoFrameOffset

  DESCRIPTION:

    This routine writes the FrameOffset to ST_EV_SYNC_FR_OFFSET.
    Moreover, it realigns entries in the SLOT_QUEUE and ACTION_SLOT_QUEUE 
    to the new frame boundary.

  PARAMETERS:

    FrameOffset - in units of slots

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysSetDoFrameOffset(uint8 FrameOffset);


/*****************************************************************************

  FUNCTION NAME: SysTimeSymbNum

  DESCRIPTION:
    Returns the Symbol count [0-0x5FF] if AirInterface==SYS_MODE_EVDO. (wraps 80ms)
    Returns 0 if AirInterface== SYS_MODE_1xRTT. (wraps 80ms)

  PARAMETERS:

    StIntSrc - Current setted Interrupt Mask.

  RETURNED VALUES:

*****************************************************************************/
extern uint16 SysTimeSymbNum(SysAirInterfaceT SysAirInterface);


/*****************************************************************************

  FUNCTION NAME: SysTime9MHzCnt

  DESCRIPTION:
    Returns the 9MHz SystemTime count of the corresponding AirInterface

  PARAMETERS:

    StIntSrc - Current setted Interrupt Mask.

  RETURNED VALUES:

*****************************************************************************/
extern uint32 SysTime9MHzCnt(SysAirInterfaceT SysAirInterface);


/*****************************************************************************

  FUNCTION NAME: SysSpySysTimeDo

  DESCRIPTION:
  Spies the following DO Time values in MON_CP_HWD_SYS_ST_DO_SPY_ID :
  uint32  Sys9MHzCnt; 
  uint16  SysSymbNum;
  SysTimeFullT SysTimeFull; 
  SingleSystemTimeT SingleSystemTime;

  PARAMETERS:

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysSpySysTimeDo(void);


/*****************************************************************************

  FUNCTION NAME: SysFrameCntGet

  DESCRIPTION:

  PARAMETERS:
  Return Upper8 and Lower 32 Frame Cnt  (NO Frame Offset for EVDO!)

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysFrameCntGet(uint8 AirInterfaceBM, FrameRecT* FrameCntP);

/*****************************************************************************

  FUNCTION NAME: SysTimeRemove

  DESCRIPTION:

    This routine requests removes system time from active list.

  PARAMETERS:

    SysAirInterfaceT AirInterface

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeRemove(SysAirInterfaceT AirInterface);

/*****************************************************************************

  FUNCTION NAME: SysTimeSwitchControl

  DESCRIPTION:

    This routine requests adds/removes systime for the system.

  PARAMETERS:

    SysAirInterfaceT NewAirInterface
    bool             Enable

  RETURNED VALUES:

    None.

*****************************************************************************/
uint8 SysTimeSwitchControl(SysAirInterfaceT NewAirInterface, bool Enable);

/*****************************************************************************

  FUNCTION NAME: SysFrameCountGet

  DESCRIPTION:  This function feels the 1X and DO time.

  PARAMETERS:
  Return  

  RETURNED VALUES:

    None.

*****************************************************************************/

extern void SysFrameCountGet(void* pSysTime);
#ifdef MTK_CBP
extern void SysFrameCountGetForDspmTrace(void* psysTime,uint32 dspmSymbolCnt);
extern void  SysStIntRequire(SysAirInterfaceT AirInterface, uint16 SlotNum, bool Require);
#endif
/*****************************************************************************

  FUNCTION NAME: SysFrameSetZeroTime

  DESCRIPTION:  This function feels  zero time for the 1X and DO time.

  PARAMETERS:
  Return  

  RETURNED VALUES:

    None.

*****************************************************************************/

void SysFrameSetZeroTime(void* psysTime);


/*****************************************************************************

  FUNCTION NAME: SysAirInterfaceGet

  DESCRIPTION:

  PARAMETERS:

  RETURNED VALUES:
  return SYS_MODE_1xRTT or SYS_MODE_EVDO

    None.

*****************************************************************************/
extern uint8  SysAirInterfaceGet(void);

/*****************************************************************************

  FUNCTION NAME: SysTimeGetValidInterface

  DESCRIPTION:

    Returns all valid system interfaces.

  PARAMETERS:

    None.

  RETURNED VALUES:

    AirInterface bitmap 

*****************************************************************************/
extern uint8 SysTimeGetValidInterface(void);

/*****************************************************************************

  FUNCTION NAME: Sys9MHzCntInitReq

  DESCRIPTION:   Uses the hardware 1X System time count to initialize the
                 hardware DO System time count
                 Note: Can do   : 1X time -> DO time.  
                       CanNOT do: DO time -> 1X time

  PARAMETERS:    AckRequired:
                 TRUE : INSP_1XASSIST_DOACQ_MSG or some form of acknowledgement
                        will be sent upon successful completion of this request.
                 FALSE: No acknowledgement will be sent to indicate completion of
                        request.

  RETURNED VALUE:
                 TRUE : 1X system time can be used to initialize DO system time.
                 FALSE: 1X system time cannot be used to initialize DO system time.

*****************************************************************************/
extern bool   Sys9MHzCntInitReq(bool AckRequired);


/*****************************************************************************

  FUNCTION NAME: SysTimeValid

  DESCRIPTION:

    Inquire whether the AirInterface has valid or invalid system time.

  PARAMETERS:

    None.

  RETURNED VALUES:

    TRUE: valid system time.  FALSE: invalid system time. 

*****************************************************************************/
extern uint8 SysTimeValid(uint8 AirInterfaceBM);


/*****************************************************************************

  FUNCTION NAME: SysTimeCurrent

  DESCRIPTION:

    Inquire whether the AirInterface has current system time.

  PARAMETERS:

    None.

  RETURNED VALUES:

    TRUE: valid system time.  FALSE: invalid system time. 

*****************************************************************************/
extern bool SysTimeCurrent(uint8 AirInterfaceBM);


#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
/*****************************************************************************

  FUNCTION NAME: SysFrameSet

  DESCRIPTION:

    Set sys frame by getting SIB8 info from LTE

  PARAMETERS:

    AirInterface - 1x or DO

  RETURNED VALUES:

*****************************************************************************/
extern void SysFrameSet(SysAirInterfaceT AirInterface, FrameRecT *Frame);


/*****************************************************************************

  FUNCTION NAME: SysFrameRecover

  DESCRIPTION:

    Recover frame number for resync denied scenario

  PARAMETERS:

    AirInterface - 1x or DO

  RETURNED VALUES:

*****************************************************************************/
extern void SysFrameRecover(SysAirInterfaceT AirInterface);


/*****************************************************************************

  FUNCTION NAME: SysFrameAlign

  DESCRIPTION:   Align super frame boundary between SW frame and HW system timer

  PARAMETERS:        AirInterface: 1X or DO

  RETURNED VALUES:

    None.
*****************************************************************************/
extern void SysFrameAlign(SysAirInterfaceT AirInterface);


/*****************************************************************************

  FUNCTION NAME: SysSFrameGet

  DESCRIPTION:

  PARAMETERS:        AirInterface: 1X or DO
                     Curr:         Return curr super frame num or not
                     CheckFrameLo32: If Curr=FALSE, Return super frame num of CheckFrameLo32
  
  Return Lower 32 Super Frame Cnt

  RETURNED VALUES:

    None.

*****************************************************************************/
extern uint32 SysSFrameGet(SysAirInterfaceT AirInterface, bool Curr, uint32 CheckFrameLo32);



/*****************************************************************************

  FUNCTION NAME: SysSFrameGetWith9M

  DESCRIPTION:   Get correct super frame numer with giving 9M Cnt

  PARAMETERS:        AirInterface: 1X or DO
                     Check9M:      given 8X clock cnt of system time
  
  Return Lower 32 of Super Frame Cnt

  RETURNED VALUES:

    None.

*****************************************************************************/
extern uint32 SysSFrameGetWith9M(SysAirInterfaceT AirInterface, uint32 Check9M);


/*****************************************************************************

  FUNCTION NAME: SysFrameGet

  DESCRIPTION:   Get correct frame numer with giving 9M Cnt

  PARAMETERS:        AirInterface: 1X or DO
  
  Return Full Frame number

  RETURNED VALUES:

    None.

*****************************************************************************/
extern FrameRecT SysFrameGetWith9M(SysAirInterfaceT AirInterface, uint32 Check9M);


/*****************************************************************************

  FUNCTION NAME: SysFrameSizeIs26ms

  DESCRIPTION:

  PARAMETERS:

  RETURNED VALUES:
  return Frame size type
            TRUE:  is 80/3 ms
            FALSE: is 20 ms

    None.

*****************************************************************************/
extern bool SysFrameSizeIs26ms(SysAirInterfaceT AirInterface);


/*****************************************************************************

  FUNCTION NAME: SysTimeRecoverByFrc

  DESCRIPTION:

  PARAMETERS: AirInterface: 1X or DO

  RETURNED VALUES:

    None.

*****************************************************************************/
extern void SysTimeRecoverByFrc(SysAirInterfaceT AirInterface);


/*****************************************************************************

  FUNCTION NAME: SysUpdateFrameCnt

  DESCRIPTION:
    This routine updates SysFrame[SYS_MODE_EVDO] and SysFrame[SYS_MODE_1xRTT].

  PARAMETERS:

    StIntSrc - Current setted Interrupt Mask.

  RETURNED VALUES:

*****************************************************************************/
extern void  SysUpdateFrameCnt(SysAirInterfaceT AirInterface);


/*****************************************************************************

  FUNCTION NAME: SysReadHwSystemTime

  DESCRIPTION:
    This routine updates single system time at every ST interrupt.

  PARAMETERS:

    StIntSrc - Current setted Interrupt Mask.

  RETURNED VALUES:

*****************************************************************************/
extern uint32 SysReadHwSystemTime(SysAirInterfaceT AirInterface);

#endif


#ifdef MTK_PLT_ON_PC
/*****************************************************************************

  FUNCTION NAME: SysUpdateFrameCnt

  DESCRIPTION:
    This routine updates SysFrame[SYS_MODE_EVDO] and SysFrame[SYS_MODE_1xRTT].

  PARAMETERS:

    StIntSrc - Current setted Interrupt Mask.

  RETURNED VALUES:

*****************************************************************************/
extern void  SysUpdateFrameCnt(SysAirInterfaceT AirInterface);


/*****************************************************************************

  FUNCTION NAME: SysReadHwSystemTime

  DESCRIPTION:
    This routine updates single system time at every ST interrupt.

  PARAMETERS:

    StIntSrc - Current setted Interrupt Mask.

  RETURNED VALUES:

*****************************************************************************/
extern uint32 SysReadHwSystemTime(SysAirInterfaceT AirInterface);
#endif

/****************************************************************************

  FUNCTION NAME: EndianSwap

  DESCRIPTION:

   This function is used to convert
   unsigned integers from Big endian format to Little endian format and vice-versa.

  PARAMETERS:

   uint8 *input           Input that needs to be endian-swapped, in-place.
   uint8 numOfBytes       Number of bytes

  RETURNED VALUES:

    TRUE if numOfBytes is 2,4,8 FALSE otherwise. This is because we have 
    16,32 and 64 bit integers only in our S/W.

*****************************************************************************/
bool EndianSwap( uint8* input , uint8 numOfBytes);

/****************************************************************************

  FUNCTION NAME: CP_Strnlen

  DESCRIPTION:

   This function is used to return the length of a "C" Style string (NULL
   Terminated). The max length returned is capped at MAX_LENGTH parameter
   which means that if the string is of size greater than MAX_LENGTH, further
   calculation of the string's length is stopped and MAX_LENGTH is returned, 
   hence the name CP_Str **n** len.

  PARAMETERS:

   char *str              Input string that needs to be used
   uint32 MAX_LENGTH      Max length to be returned

  RETURNED VALUES: the length of the string if its less than or equal to MAX_LENGTH
  else MAX_LENGTH is returned.

*****************************************************************************/
uint32 CP_Strnlen( char *str, uint32 MAX_LENGTH);

/****************************************************************************
;
;  FUNCTION NAME: SysArmSleep
;
;  DESCRIPTION:
;
;    Puts Arm into light sleep using coprocessor.  Any INT should wake
;    (SN bit does not work for CBP70 A0 so we use Arm968 light sleep)
;
;
;  PARAMETERS:
;
;    None
;
;  RETURNED VALUES:
;
;    None.
;
*****************************************************************************/
void SysArmSleep(void);

/*****************************************************************************
;
;  FUNCTION NAME: SysArmModeRegs
;
;  DESCRIPTION:
;
;    Stores the Arm mode regs (CPSR, SP, LR, SPSR) for 5 Arm modes
;    (supervisor, FIQ, IRQ, Undef,  Abort, Sytem). User mode does not have
;    its own mode specific registers.
;   
;    This function should only be used during crash dump.
;
;  C FUNCTION PROTOTYPE:
;
;    void SysArmModeRegs(MonExceptArmModeRegs* pRegs);
;    Use uint32* so we don't need definition of MonExceptArmModeRegs
;
;  PARAMETERS:
;
;    pRegs (r0), pointer to array of 6 MonExceptArmModeRegs
;
;  RETURNED VALUES:
;
;    None.
;
;*****************************************************************************/
void SysArmModeRegs(uint32* pRegs);

/****************************************************************************
 *
 * Name:        sysIsBandSupported()
 *
 * Description: This routine validates the band by examining the allowed
 *              operation modes as set by teh compile time switches in
 *              custom.h.
 *
 * Parameters:  SysCdmaBandT band - band to be validated
 *
 * Returns:     BOOL - TRUE if the band is supported
 *
 * Notes:
 *
 ****************************************************************************/
bool sysIsBandSupported(SysCdmaBandT band);


/****************************************************************************
 *
 * Name:        sysFrequencyToBlock()
 *
 * Description: This routine finds the block for the given channel.
 *
 * Parameters:  SysCdmaBandT band - band channel is in
 *              UINT16 channel - channel for which block is to be found
 *
 * Returns:     UINT8 - PCS block
 *
 * Notes:       A liberal range is used for channel numbers since this is
 *              not validating the channels, only selecting a system.
 *
 ****************************************************************************/
uint8 sysFrequencyToBlock(SysCdmaBandT band, UINT16 channel);

/****************************************************************************
 *
 * Name:        sysFrequencyToSubClass()
 *
 * Description: This routine finds the subclass for the given channel.
 *
 * Parameters:  SysCdmaBandT band - band channel is in
 *              UINT16 channel - channel for which block is to be found
 *
 * Returns:     UINT8 - Subclass
 *
 ****************************************************************************/
UINT8 sysFrequencyToSubClass(SysCdmaBandT band, UINT16 channel);

/****************************************************************************
 *
 * Name:        sysIsBandChannelValid()
 *
 * Description: This routine validates a given channel against the given
 *              band, based on Standard band definion only.
 *              It DOES NOT consider hardware, including possible hwd 
 *              band SubClass limitation.
 *
 * Parameters:  BandChannel *bandChannel - band and channel to validate
 *
 * Returns:     BOOL - TRUE if channel is valid within the band
 *
 * Notes:
 *
 ****************************************************************************/
bool sysIsBandChannelValid(SysBandChannelT *bandChannel);

/****************************************************************************
 *
 * Name:        sysSupportCDMAChannel()
 *
 * Description: This routine determines if the provided band and channel
 *              are supported and valid.
 *
 * Parameters:  BandChannel *bandChannel - band and channel
 *
 * Returns:     BOOL - TRUE if the band is supported and the channel is valid
 *
 * Notes:
 *
 ****************************************************************************/
bool sysIsBandChannelSupported(SysBandChannelT *bandChannel);

/*****************************************************************************

  FUNCTION NAME: sysSOServiceType

  DESCRIPTION:

    This routine returns the service type for the passed Service option.

  PARAMETERS:

    SysServiceOptionT serviceOption 

  RETURNED VALUES:

    SysServiceTypeT  service type (e.g. VOICE, PACKET_DATA, etc...)

*****************************************************************************/
SysServiceTypeT sysSOServiceType(SysServiceOptionT serviceOption);

/*****************************************************************************

  FUNCTION NAME: sysLoopbackSvcType

  DESCRIPTION:

    This routine returns the service subtype for a loopback Service option.

  PARAMETERS:

    SysServiceOptionT serviceOption 

  RETURNED VALUES:

    SysLoopbackSvcTypeT  service sub type (IS126, Markov, TDSO)

*****************************************************************************/
SysLoopbackSvcTypeT sysLoopbackSvcType(SysServiceOptionT serviceOption);

/*****************************************************************************

  FUNCTION NAME: sysCircuitDataSvcType

  DESCRIPTION:

    This routine returns the service subtype for a cicuit data  Service option.

  PARAMETERS:

    SysServiceOptionT serviceOption 

  RETURNED VALUES:

    SysCircuitDataSvcTypeT  service sub type (Async, Fax)

*****************************************************************************/
SysCircuitDataSvcTypeT sysCircuitDataSvcType(SysServiceOptionT serviceOption);

/*****************************************************************************

  FUNCTION NAME: sysPacketDataSvcType

  DESCRIPTION:

    This routine returns the service subtype for a packet data Service option.

  PARAMETERS:

    SysPacketDataSvcTypeT serviceOption 

  RETURNED VALUES:

    SysPacketDataSvcTypeT  service sub type (LSPD, MSPD, HSPD)

*****************************************************************************/

SysPacketDataSvcTypeT sysPacketDataSvcType(SysServiceOptionT serviceOption);



SysSoGrpT sysSOSvcGrp(WORD serviceOption);

/*****************************************************************************
 
  FUNCTION NAME: calcChecksum

  DESCRIPTION:
   
   Calculates the checksum for the data pointed to by ptrtodata whose size
   is given by sizeofdata. The function also inverts the checksum before
   returning it.
  
  PARAMETERS:

   uint8 *ptrtodata    - pointer to the data whose checksum has to be calc
   uint16 sizeofdata   - size of the data whose checksum has to be calc
                         (this does not incl the checksum size)
  
  RETURNED VALUES: Inverted checksum.

*****************************************************************************/
uint16 calcChecksum(uint8 *ptrtodata, uint16 sizeofdata);

/****************************************************************************
 *
 * Name:        SysBondoutOptionGet ()
 *
 * Description: This routine returns the chip's bondout option configuration.
 *
 * Parameters:  None
 *
 * Returns:     SysBondoutOptionsT - Bondout option
 *
 ****************************************************************************/
SysBondoutOptionsT SysBondoutOptionGet (void);

/****************************************************************************
 *
 * Name:        SysIsVoiceSupported()
 *
 * Description: This routine returns a boolean indicating whether voice is 
 *              supported based on the chip's bondout option configuration.
 *
 * Parameters:  None
 *
 * Returns:     TRUE if voice is supported, FALSE otherwise
 *
 ****************************************************************************/
bool SysIsVoiceSupported (void);

/****************************************************************************
 *
 * Name:        SysIsDORev0Supported()
 *
 * Description: This routine returns a boolean indicating whether or not EV-DO
 *              Rev0 is supported based on the chip's bondout option configuration.
 *
 * Parameters:  None
 *
 * Returns:     TRUE if EV-DO Rev0 is supported, FALSE otherwise
 *
 ****************************************************************************/
bool SysIsDORev0Supported (void);

/****************************************************************************
 *
 * Name:        SysIsDORevASupported()
 *
 * Description: This routine returns a boolean indicating whether or not EV-DO
 *              RevA is supported based on the chip's bondout option configuration.
 *
 * Parameters:  None
 *
 * Returns:     TRUE if EV-DO RevA is supported, FALSE otherwise
 *
 ****************************************************************************/
bool SysIsDORevASupported (void);

/****************************************************************************
 *
 * Name:        sysIsFeatureSupported()
 *
 * Description: This routine returns whether a system wide feature is
 *              supported.
 *
 * Parameters:  SysFeatures Feature
 *
 * Returns:     BOOL - FALSE: Feature not supported
 *                     TRUE:  Feature supported
 *
 ****************************************************************************/
BOOL sysIsFeatureSupported(SysFeatureId Feature);

/****************************************************************************
 *
 * Name:        sysHWFeatureValue()
 *
 * Description: Returns an indication as to which variation of a hardware 
 *              feature is supported.
 *
 * Parameters:  SysFeatures Feature
 *
 * Returns:     Value
 *
 ****************************************************************************/
uint32 sysHWFeatureValue(SysFeatureId Feature);

/****************************************************************************
 *
 * Name:        sysSWFeatureValue()
 *
 * Description: Returns an indication as to which variation of a software 
 *              feature is supported.
 *
 * Parameters:  SysFeatures Feature
 *
 * Returns:     
 *
 ****************************************************************************/
uint32 sysSWFeatureValue(SysFeatureId Feature);

/****************************************************************************
 *
 * Name:        sysGetCarrierdId()
 *
 * Description: Returns the carrier Id for which the SW was compiled for
 *
 * Parameters:  none
 *
 * Returns:     SysCarrierId enum
 *
 ****************************************************************************/
SysCarrierId sysGetCarrierId(void);

/*****************************************************************************

  FUNCTION NAME: SysStSaveRegisters

  DESCRIPTION:

    This routine saves ST registers before going to deep sleep with PSO enabled

  PARAMETERS:

    None

  RETURNED VALUES:

    None

*****************************************************************************/
void SysStSaveRegisters(void);

/*****************************************************************************

  FUNCTION NAME: SysStRestoreRegisters

  DESCRIPTION:

    This routine saves ST registers before going to deep sleep with PSO enabled

  PARAMETERS:

    None

  RETURNED VALUES:

    None

*****************************************************************************/
void SysStRestoreRegisters(void);

/*****************************************************************************

  FUNCTION NAME: SysTransitionReq

  DESCRIPTION:

    This routine requests transition from 1x to Do and vice versa.

  PARAMETERS:

    SysAirInterfaceT NewAirInterface
    bool             HybridMode

  RETURNED VALUES:

    None.

*****************************************************************************/
uint8 SysTransitionReq(SysAirInterfaceT NewAirInterface, bool HybridMode);

/****************************************************************************
 ****************************************************************************
 *
 *SysQ library.
 *
 * Description: Routines to create and manage a queue of elements.            
 ****************************************************************************/
#ifdef __cplusplus
extern "C" {
#endif

typedef struct QElement
{
     struct QElement *Next;
    struct QElement *Prev; 
    void*                 Pool;     /* Initial free pool list */
    void*                 CList;     /* Points to current list.   */
    uint16               Size;      /* Size in bytes of user portion */
    uint32                 Data[1];     /* User data portion. We use uint32 as "place holder" to ensure 32 bit align.
                                                  * Cannot use flexible element because c++ will have error. 
                                                  */
} SysQElement, * SysQElementID ;

typedef struct
{
    uint32                   ID;               /* Set to unique number to identify valid List */
    SysQElementID   Head;
    SysQElementID   Tail;
    uint16                    Count;
} SysQListDesc, *SysQListID;

typedef struct
{
    SysQListDesc   List;
    uint32                 ElementDataSize;    /* Size of user data.  Be sure to end struct aligned on 32bits  */    
}SysQPoolDesc, *SysQPoolID;

/* Return codes */
typedef enum
{
    SYS_Q_OK,
    SYS_Q_ERROR    
}SysQStatus;

/* Macro to caculate the size in bytes required for a pool of _nElements of _Size (in bytes).  _Size refers to user data area in bytes.  
 * Macro should  be used to calculate buffer size for Q Pool of elements.  SysQElement includes a uint32 as a place holder for Data so we subtract that 
 * when calculating size
 */
#define SYS_Q_POOL_SIZE(_nElements, _Size) ( PAD32(sizeof(SysQPoolDesc)) + ((sizeof(SysQElement) - sizeof(uint32) + PAD32((_Size))) * (_nElements)) )

/****************************************************************************
 *
 * Name:        SysQPoolInit()
 *
 * Description: This routine initializes a pool of Q elements. A QPool is list of
 *                      free elements.
 *
 * Parameters:  ElementSize: size in bytes of each element in Q
 *                       NumElements: number of elements in pool.
 *
 * Returns:     SysQPoolId - Handle to pool - NULL if unable to create
 *
 ****************************************************************************/
SysQPoolID SysQPoolInit(void* PoolBuf, uint16 PoolBufSize, uint16 ElementSize);

/****************************************************************************
 *
 * Name:        SysQListInit()
 *
 * Description: This routine initializes a QList.            
 *
 * Parameters:  QList - Pointer to QList.
 *
 * Returns:     SysQPoolId - Handle to pool - 
 *
 ****************************************************************************/
SysQListID SysQListInit (SysQListDesc* QList );

/****************************************************************************
 *
 * Name:        SysQElementAlloc()
 *
 * Description: Allocs an element from the PoolId.             
 *
 * Parameters:  PoolId: Handle to pool returned by SysQPoolInit.
 *
 * Returns:     SysQElement - Pointer to Element. NULL if unable to alloc
 *
 ****************************************************************************/
SysQElementID SysQElementAlloc (SysQPoolID PoolId);

/****************************************************************************
 *
 * Name:        SysQElementFree()
 *
 * Description: Frees an element and returns to proper PoolId.             
 *
 * Parameters:  SysQElement: Element to be freed Must not belong to any Q
 *
 * Returns:     SysQStatus
 *
 ****************************************************************************/
SysQStatus SysQElementFree (SysQElementID Element);

/****************************************************************************
 *
 * Name:        SysQElementFreeAll()
 *
 * Description: Frees all elements of a QList and returns elements to proper PoolId.             
 *
 * Parameters:  SysQListID: Qlist
 *
 * Returns:     SysQStatus
 *
 ****************************************************************************/
SysQStatus SysQElementFreeAll (SysQListID QId);

/****************************************************************************
 *
 * Name:        SysQElementAdd()
 *
 * Description: Add an element to top or bottom of a QList.           
 *
 * Parameters:Qid : Q that element to be added.  
 *                     Element: Element to be added to Q
 *                     Bottom: Set to TRUE if want to add to bottom of Q.
 *
 * Returns:     SysQStatus
 *
 ****************************************************************************/
SysQStatus SysQElementAdd (SysQListID QId, SysQElementID Element,  bool Bottom);

/****************************************************************************
 *
 * Name:        SysQElementRemove()
 *
 * Description: Remove an element from a QList with option to free.          
 *
 * Parameters:Qid : Q that element to be removed.  
 *                     Element: Element to be removed.
 *                     Free: Set to TRUE if element should be freed.
 *
 * Returns:     SysQStatus
 *
 ****************************************************************************/
SysQStatus SysQElementRemove (SysQListID QId, SysQElementID Element,  bool Free);

/****************************************************************************
 *
 * Name:        SysQElementInsert()
 *
 * Description: Insert an element to a Q..             
 *
 * Parameters:Qid : Q that element to be added.  
 *                     Element: Element to be inserted to Q
 *                     Location: Location element that defines where insert element will go
 *                     After: Set to TRUE if element insert after Location else will be inserted before.
 *
 * Returns:     SysQStatus
 *
 ****************************************************************************/
SysQStatus SysQElementInsert (SysQListID QId, SysQElementID Element, SysQElementID Location, bool After);

/****************************************************************************
 *
 * Name:        SysQElementFindFirst()
 *
 * Description: Get first element in a Q..             
 *
 * Parameters:Qid : Q.  
 *
 * Returns:     SysQElementID: First element.  NULL if no elements in Q.
 *
 ****************************************************************************/
/* SysQElementID SysQElementFindFirst(SysQListID QId); */
#define SysQElementFindFirst(_Qid_)    ((_Qid_)->Head)

/****************************************************************************
 *
 * Name:        SysQElementFindLast()
 *
 * Description: Get last element in a Q..             
 *
 * Parameters:Qid : Q.  
 *
 * Returns:     SysQElementID: Last element.  NULL if no elements in Q.
 *
 ****************************************************************************/
/* SysQElementID SysQElementFindLast(SysQListID QId); */
#define SysQElementFindLast(_Qid_)    ((_Qid_)->Tail)

/****************************************************************************
 *
 * Name:        SysQElementFindNext()
 *
 * Description: Get next element.             
 *
 * Parameters:Element : Root element for next.  
 *
 * Returns:     SysQElementID: Next element.  NULL if no Next.
 *
 ****************************************************************************/
/* SysQElementID SysQElementFindNext (SysQElementID Element); */
#define SysQElementFindNext(_Element)  ((_Element)->Next)
/****************************************************************************
 *
 * Name:        SysQElementFindPrev()
 *
 * Description: Get previous element.             
 *
 * Parameters:Element : Root element for previous.  
 *
 * Returns:     SysQElementID: Previous element.  NULL if no Next.
 *
 ****************************************************************************/
/* SysQElementID SysQElementFindPrev (SysQElementID Element); */
#define SysQElementFindPrev(_Element)  ((_Element)->Prev)

/****************************************************************************
 *
 * Name:        SysQGetCount()
 *
 * Description: Find number of elements in a Q..             
 *
 * Parameters:Qid : Q.  
 *
 * Returns:     uint16 number of elements
 *
 ****************************************************************************/
/* uint16 SysQGetCount(SysQListID QId); */
#define SysQGetCount(_Qid_)    ((_Qid_)->Count)

/****************************************************************************
 *
 * Name:        SysQElementGetSize()
 *
 * Description: Get size in bytes of an element.             
 *
 * Parameters:Element : Element of interest.  
 *
 * Returns:     uint16 size of element in bytes.
 *
 ****************************************************************************/
/* uint16 SysQElementGetSize (SysQElementID Element); */
#define SysQElementGetSize(_Element)  ((_Element)->Size)

#ifndef MTK_PLT_ON_PC /* Disable for compile error on PC */
#ifndef GEN_FOR_PC
extern uint32 Image$$SRAM$$RO$$Base, Image$$SRAM$$RO$$Limit, Image$$FLASH$$RO$$Base, Image$$FLASH$$RO$$Limit, Image$$IRAM$$RO$$Base, Image$$IRAM$$RO$$Limit;
#endif

#define  SRAM_CODE_BASE          ((uint32) &Image$$SRAM$$RO$$Base)
#define  SRAM_CODE_LIMIT          ((uint32) &Image$$SRAM$$RO$$Limit)
#define  FLASH_CODE_BASE          ((uint32) &Image$$FLASH$$RO$$Base)
#define  FLASH_CODE_LIMIT          ((uint32) &Image$$FLASH$$RO$$Limit)
#define  IRAM_CODE_BASE          ((uint32) &Image$$IRAM$$RO$$Base)
#define  IRAM_CODE_LIMIT          ((uint32) &Image$$IRAM$$RO$$Limit)
#endif
/****************************************************************************
 *
 * Name:        SysCallbackPtrValid()
 *
 * Description: This routine checks if a callback function is valid or not
 *
 * Parameters:  pointer for the callback function
 *
 * Returns:     BOOL - FALSE: callback function is invalid
 *                     TRUE:  callback function is valid
 *
 ****************************************************************************/

bool SysCallbackPtrValid(void* CallbackPtr);


#ifdef __cplusplus
}
#endif

/****************************************************************************
 *
 * Name:        max8
 *
 * Scope:       public
 *
 * Description: Returns the greater of two UINT8 values.
 *              
 * Parameters:
 *   UINT8 a - First value.
 *   UINT8 b - Second value.
 *
 * Returns:
 *   UINT8
 *
 * Notes:
 *
 ****************************************************************************/
UINT8 max8 (UINT8 a, UINT8 b);

/****************************************************************************
 *
 * Name:        min8
 *
 * Scope:       public
 *
 * Description: Returns the lesser of two UINT8 values.
 *              
 * Parameters:
 *   UINT8 a - First value.
 *   UINT8 b - Second value.
 *
 * Returns:
 *   UINT8
 *
 * Notes:
 *
 ****************************************************************************/
UINT8 min8 (UINT8 a, UINT8 b);

/****************************************************************************
 *
 * Name:        max16
 *
 * Scope:       public
 *
 * Description: Returns the greater of two UINT16 values.
 *              
 * Parameters:
 *   UINT16 a - First value.
 *   UINT16 b - Second value.
 *
 * Returns:
 *   UINT16
 *
 * Notes:
 *
 ****************************************************************************/
UINT16 max16 (UINT16 a, UINT16 b);

/****************************************************************************
 *
 * Name:        min16
 *
 * Scope:       public
 *
 * Description: Returns the lesser of two UINT16 values.
 *              
 * Parameters:
 *   UINT16 a - First value.
 *   UINT16 b - Second value.
 *
 * Returns:
 *   UINT16
 *
 * Notes:
 *
 ****************************************************************************/
UINT16 min16 (UINT16 a, UINT16 b);

#endif

/*****************************************************************************
* $Log: sysapi.h $
*****************************************************************************/

/**Log information: \main\CBP80\cbp80_cshen_scbp10098\1 2012-07-26 06:27:42 GMT cshen
** cbp80_cshen_scbp10098**/
/**Log information: \main\Trophy\Trophy_czhang_href21785\1 2013-11-25 02:42:46 GMT czhang
** HREF#21785**/
/**Log information: \main\Trophy\1 2013-11-25 02:55:25 GMT czhang
** HREF#21785**/
/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:18:04 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\2 2013-12-10 08:33:42 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
