/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE. 
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *   hwddefs_mtk.h
 *
 * Project:
 * --------
 *   C2K
 *
 * Description:
 * ------------
 *   Hardware definitions for MTK serial chips.
 *
 * Author:
 * -------
 *   mtk08613
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:$
 * $Modtime:$
 * $Log:$
 * 
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/

#ifndef _HWDEFS_MTK_H_
#define _HWDEFS_MTK_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/

                             /*-----------------**
                             ** CP Register Map **
                             **-----------------*/

/* Note: All registers are named the same as in the Technical Manual
 *       but with an additional "HWD_" prefix.
 */

#define HWD_CP_BASE_ADDR      0x52000000
#define HWD_NO_BASE_ADDR      0x00000000

#define HWD_REG32_BOOTST      (HWD_CP_BASE_ADDR+0x0000B018)
#define HWD_REG32_WDSTAT      (HWD_CP_BASE_ADDR+0x0000B0F0)

#define RAM_SIZE_8MB     8
#define RAM_SIZE_10MB    10
#define RAM_SIZE_16MB    16
#define RAM_SIZE_32MB    32
#define RAM_SIZE_64MB    64
#define RAM_SIZE_128MB   128
#define RAM_SIZE_256MB   256
#define RAM_SIZE_512MB   512

#if (SYS_ASIC == SA_MT6735)
   #define RAM_SIZE_MB   RAM_SIZE_8MB
#elif (SYS_ASIC >= SA_MT6755)
   #define RAM_SIZE_MB   RAM_SIZE_10MB
#else  // other SYS_ASIC's
   #error "define or review the RAM\SRAM size on the new platform"
#endif


/*-------------------------------------------------------------------------------------------------*/
/*      Name                   Addr                           R/W  Description                               Reset  */
/*-------------------------------------------------------------------------------------------------*/

/*------------------**
** PMIC Interface    **
**------------------*/
#define HWD_PMIC_BASE               (HWD_CP_BASE_ADDR+0x00004000)   /* PMIC Ctrl Base Address */


/* PMIC Intf */
#define PMIC_INT_STS                (HWD_PMIC_BASE+0x0000)     /* R/W  C2K SYS PMIC Control Interrupt STATUS regisiter */
#define PMIC_INT_MSK                (HWD_PMIC_BASE+0x0004)     /* R/W  C2K SYS PMIC Control Interrupt MASK regisiter */
#define PMIC_IMMD_STATUS            (HWD_PMIC_BASE+0x0008)     /* R/W  C2K SYS Immediately mode status register  */
#define PMIC_DATA                   (HWD_PMIC_BASE+0x000C)     /* R/W  CP write c2k_Pmic_ctrl data address   */
#define PMIC_CTRL                   (HWD_PMIC_BASE+0x0010)     /* R/W  PMIC Control register  */
#define PMIC_INTERVAL_PERIOD        (HWD_PMIC_BASE+0x0014)     /* R/W  Guard Period for the interval 2 consecutive pmic transaction */
#define CMD_DO_1ST                  (HWD_PMIC_BASE+0x0018)     /* R/W  CP/DelayLoader DO 1st half slot VPA command word  */
#define CMD_DO_2ND                  (HWD_PMIC_BASE+0x001C)     /* R/W  CP/DelayLoader DO 1st half slot VPA command word */
#define DO_1ST_STATUS               (HWD_PMIC_BASE+0x0020)     /* R/W  VPA 1st half slot VPA control status  */
#define DO_2ND_STATUS               (HWD_PMIC_BASE+0x0024)     /* R/W  VPA 2nd half slot VPA control status */
#define CMD_1X_ADDR                 (HWD_PMIC_BASE+0x0028)     /* R/W  Clock configuration                     0x0 */
#define CMD_1X_DATA                 (HWD_PMIC_BASE+0x002C)     /* R/W  Clock configuration                     0x0 */
#define VPA_1X_STATUS               (HWD_PMIC_BASE+0x0030)     /* R/W  Clock configuration                     0x0 */
              

/*----------**
** PLLCfg      **
**----------*/
#define HWD_PLL_MIXSYS_ADDR    (HWD_CP_BASE_ADDR+0x00013000)  /* PLLCfg Base Address */   

/* PLL Mixsys Part registers */
#define HWD_PLL_CON1           (HWD_PLL_MIXSYS_ADDR+0x00000000)  /* R/W  PLL MixSys control register 1         0x0000 */
#define HWD_PLL_CON2           (HWD_PLL_MIXSYS_ADDR+0x00000004)  /* R/W  PLL MixSys control register 2         0x0004 */
#define HWD_PLL_CON3           (HWD_PLL_MIXSYS_ADDR+0x00000008)  /* R/W  PLL MixSys control register 3         0x0008 */

#define HWD_DFS_CON7           (HWD_PLL_MIXSYS_ADDR+0x0000000C)  /* R/W  PLL MixSys  DFS control register    0x000C */
#define HWD_DFS_CON8           (HWD_PLL_MIXSYS_ADDR+0x00000010)  /* R/W  PLL MixSys  DFS control register    0x0010 */
#define HWD_DFS_CON9           (HWD_PLL_MIXSYS_ADDR+0x00000014)  /* R/W  PLL MixSys  DFS control register    0x0014 */

#define HWD_C2KPLL1_CON0       (HWD_PLL_MIXSYS_ADDR+0x00000018)  /* R/W  PLL MixSys  MDPLL1 control register0    0x0018  */
#define HWD_C2KPLL1_CON1       (HWD_PLL_MIXSYS_ADDR+0x0000001C)  /* R/W  PLL MixSys  MDPLL1 control register1    0x001C */
#define HWD_C2KPLL1_CON2       (HWD_PLL_MIXSYS_ADDR+0x00000020)  /* R/W  PLL MixSys  MDPLL1 control register2    0x0020 */
#define HWD_C2KPLL1_CON3       (HWD_PLL_MIXSYS_ADDR+0x00000024)  /* R/W  PLL MixSys  MDPLL1 control register3    0x0024 */
#define HWD_C2KPLL1_CON4       (HWD_PLL_MIXSYS_ADDR+0x00000028)  /* R/W  PLL MixSys  MDPLL1 control register4    0x0028 */
                               
#define HWD_C2KPLL2_CON0       (HWD_PLL_MIXSYS_ADDR+0x0000002C)  /* R/W  PLL MixSys  MDPLL2 control register0    0x002C */
#define HWD_C2KPLL2_CON1       (HWD_PLL_MIXSYS_ADDR+0x00000030)  /* R/W  PLL MixSys  MDPLL2 control register1    0x0030 */
#define HWD_C2KPLL2_CON2       (HWD_PLL_MIXSYS_ADDR+0x00000034)  /* R/W  PLL MixSys  MDPLL2 control register2    0x0034 */
#define HWD_C2KPLL2_CON3       (HWD_PLL_MIXSYS_ADDR+0x00000038)  /* R/W  PLL MixSys  MDPLL2 control register3    0x0038 */
#define HWD_C2KPLL2_CON4       (HWD_PLL_MIXSYS_ADDR+0x0000003C)  /* R/W  PLL MixSys  MDPLL2 control register4    0x003C */

#define HWD_CPPLL_CON0         (HWD_PLL_MIXSYS_ADDR+0x00000040)  /* R/W  PLL MixSys  CPPLL control register0    0x0040  */
#define HWD_CPPLL_CON1         (HWD_PLL_MIXSYS_ADDR+0x00000044)  /* R/W  PLL MixSys  CPPLL control register1    0x0044 */
#define HWD_CPPLL_CON2         (HWD_PLL_MIXSYS_ADDR+0x00000048)  /* R/W  PLL MixSys  CPPLL control register2    0x0048 */
#define HWD_CPPLL_CON3         (HWD_PLL_MIXSYS_ADDR+0x0000004C)  /* R/W  PLL MixSys  CPPLL control register3    0x004C */

#define HWD_DSPPLL_CON0        (HWD_PLL_MIXSYS_ADDR+0x00000050)  /* R/W  PLL MixSys  CPPLL control register0    0x0050  */
#define HWD_DSPPLL_CON1        (HWD_PLL_MIXSYS_ADDR+0x00000054)  /* R/W  PLL MixSys  CPPLL control register1    0x0054 */
#define HWD_DSPPLL_CON2        (HWD_PLL_MIXSYS_ADDR+0x00000058)  /* R/W  PLL MixSys  CPPLL control register2    0x0058 */
#define HWD_DSPPLL_CON3        (HWD_PLL_MIXSYS_ADDR+0x0000005C)  /* R/W  PLL MixSys  CPPLL control register3    0x005C */

#define HWD_PLLTD_CON0         (HWD_PLL_MIXSYS_ADDR+0x00000074)  /* R/W  PLL MixSys  PLLTD Control register     0x0074  */
#define HWD_PLLTD_CON1         (HWD_PLL_MIXSYS_ADDR+0x00000078)  /* R/W  PLL MixSys  PLLTD Control register     0x0078  */
#define HWD_PLLTD_CON2         (HWD_PLL_MIXSYS_ADDR+0x0000007C)  /* R/W  PLL MixSys  PLLTD Control register     0x007C  */
#define HWD_PLLTD_CON3         (HWD_PLL_MIXSYS_ADDR+0x00000080)  /* R/W  PLL MixSys  PLLTD Control register     0x0080  */
#define HWD_PLLTD_CON4         (HWD_PLL_MIXSYS_ADDR+0x00000084)  /* R/W  PLL MixSys  PLLTD Control register     0x0084  */
#define HWD_PLLTD_CON5         (HWD_PLL_MIXSYS_ADDR+0x00000088)  /* R/W  PLL MixSys  PLLTD Control register     0x0088  */
#define HWD_PLLTD_CON6         (HWD_PLL_MIXSYS_ADDR+0x0000008C)  /* R/W  PLL MixSys  PLLTD Control register     0x008C  */
#define HWD_PLLTD_CON7         (HWD_PLL_MIXSYS_ADDR+0x00000090)  /* R/W  PLL MixSys  PLLTD Control register     0x0090  */

#define HWD_FQMTR_CON0         (HWD_PLL_MIXSYS_ADDR+0x000000F0)  /* R/W  PLL MixSys  Freqency Meter register     0x00F0  */
#define HWD_FQMTR_CON1         (HWD_PLL_MIXSYS_ADDR+0x000000F4)  /* R/W  PLL MixSys  Freqency Meter register     0x00F4  */
#define HWD_FQMTR_CON2         (HWD_PLL_MIXSYS_ADDR+0x000000F8)  /* R/W  PLL MixSys  Freqency Meter register     0x00F8  */
#define HWD_FQMTR_DATA         (HWD_PLL_MIXSYS_ADDR+0x000000FC)  /* R/W  PLL MixSys  Freqency Meter register     0x00FC  */

#define HWD_FH_CON0            (HWD_PLL_MIXSYS_ADDR+0x00000100)  /* R/W  PLL MixSys  Freq Hopping register      0x0100  */
#define HWD_FH_CON1            (HWD_PLL_MIXSYS_ADDR+0x00000104)  /* R/W  PLL MixSys  Freq Hopping  register     0x0104  */
#define HWD_FH_CON2            (HWD_PLL_MIXSYS_ADDR+0x00000108)  /* R/W  PLL MixSys  Freq Hopping  register     0x0108  */
#define HWD_FH_CON3            (HWD_PLL_MIXSYS_ADDR+0x0000010C)  /* R/W  PLL MixSys  Freq Hopping  register     0x010C  */
#define HWD_FH_CON4            (HWD_PLL_MIXSYS_ADDR+0x00000110)  /* R/W  PLL MixSys  Freq Hopping register      0x0110  */
#define HWD_FH_CON5            (HWD_PLL_MIXSYS_ADDR+0x00000114)  /* R/W  PLL MixSys  Freq Hopping  register     0x0114  */
#define HWD_FH_CON6            (HWD_PLL_MIXSYS_ADDR+0x00000118)  /* R/W  PLL MixSys  Freq Hopping  register     0x0118  */
#define HWD_FH_CON7            (HWD_PLL_MIXSYS_ADDR+0x0000011C)  /* R/W  PLL MixSys  Freq Hopping  register     0x011C  */
#define HWD_FH_CON8            (HWD_PLL_MIXSYS_ADDR+0x00000120)  /* R/W  PLL MixSys  Freq Hopping register      0x0120  */
#define HWD_FH_CON9            (HWD_PLL_MIXSYS_ADDR+0x00000124)  /* R/W  PLL MixSys  Freq Hopping  register     0x0124  */
#define HWD_FH_CON10           (HWD_PLL_MIXSYS_ADDR+0x00000128)  /* R/W  PLL MixSys  Freq Hopping  register     0x0128  */
#define HWD_PLL_RESERVE_REG    (HWD_PLL_MIXSYS_ADDR+0x0000012C)  /* R/W  PLL MixSys  Resever  register     0x012C  */
#define HWD_C2KPLL_TEST        (HWD_PLL_MIXSYS_ADDR+0x00000130)  /* R/W  PLL MixSys  C2KPLL test mode  register     0x0130  */
#define HWD_C2KPLL1_MODESEL    (HWD_PLL_MIXSYS_ADDR+0x00000134)  /* R/W  PLL MixSys  */
#define HWD_C2KPLL2_MODESEL    (HWD_PLL_MIXSYS_ADDR+0x00000138)  /* R/W  PLL MixSys  */
#define HWD_C2KPLL_MMD_DIV     (HWD_PLL_MIXSYS_ADDR+0x0000013C)  /* R/W  PLL MixSys  */

/*---------------------------------------------------**          
**    MIBST definition                                **
**---------------------------------------------------*/
#define HWD_MIBST_CONFG_BASE        (HWD_CP_BASE_ADDR+0x00018000)  /* MBISTCfg Base Address */

/*---------------------**
** New SDIO IP registers    **
**----------------------*/
#define HWD_SDIO_BASE      (0x90020000)                 /* SDIO base register addr  */
#define HWD_SDIO_HGFCR     (HWD_SDIO_BASE + 0x0)        /* HIF global firmware configuration register */
#define HWD_SDIO_HGFISR    (HWD_SDIO_BASE + 0x4)        /* HIF global firmware interrupt status register */
#define HWD_SDIO_HGFIER    (HWD_SDIO_BASE + 0x8)        /* HIF global firmware interrupt enable register */
#define HWD_SDIO_HSDBDLSR  (HWD_SDIO_BASE + 0x10)       /* HIF SDIO bus delay selection register */
#define HWD_SDIO_HSDLSR    (HWD_SDIO_BASE + 0x14)       /* HIF SRAM delay selection register */
#define HWD_SDIO_HCSDCR    (HWD_SDIO_BASE + 0x18)       /* HIF Clock Stop Detection register */
#define HWD_SDIO_HGH2DR    (HWD_SDIO_BASE + 0x1c)       /* HIF Global Host to  Device register */
#define HWD_SDIO_HDBGCR    (HWD_SDIO_BASE + 0x20)       /* HIF Global Debug control register */
#define HWD_SDIO_HGTMTCR   (HWD_SDIO_BASE + 0x30)       /* Test mode trigger control register */
#define HWD_SDIO_HGTMCR    (HWD_SDIO_BASE + 0x34)       /* Test mode  control register */
#define HWD_SDIO_HGTMDPCR0 (HWD_SDIO_BASE + 0x38)       /* Test mode  Data pattern control register 0 */
#define HWD_SDIO_HGTMDPCR1 (HWD_SDIO_BASE + 0x3c)       /* Test mode  Data pattern control register 1 */



#define HWD_SDIO_HCFISR    (HWD_SDIO_BASE + 0x300)       /* HIF Common firmware interrupt status register */
#define HWD_SDIO_HCFIER    (HWD_SDIO_BASE + 0x304)       /* HIF Common firmware interrupt enable register */
#define HWD_SDIO_HCFSCR    (HWD_SDIO_BASE + 0x308)       /* HIF Common firmware status control register */
#define HWD_SDIO_HCFCR     (HWD_SDIO_BASE + 0x30c)       /* HIF Common firmware control register */
#define HWD_SDIO_HCFTDR    (HWD_SDIO_BASE + 0x310)       /* HIF Common firmware TX data  register, RO  */
#define HWD_SDIO_HCFRDR    (HWD_SDIO_BASE + 0x314)       /* HIF Common firmware RX data  register, WO  */
#define HWD_SDIO_HCFHBR    (HWD_SDIO_BASE + 0x318)       /* HIF Common firmware Host buffer control  register, RW */
#define HWD_SDIO_HCFTCR    (HWD_SDIO_BASE + 0x31c)       /* HIF Common firmware Tx count  register, RO */
#define HWD_SDIO_HCFRCR    (HWD_SDIO_BASE + 0x320)       /* HIF Common firmware Rx count  register , RO*/
#define HWD_SDIO_HCFSDBGR  (HWD_SDIO_BASE + 0x324)       /* HIF Common firmware SRAM debug register, RO */
#define HWD_SDIO_HCFSTPR   (HWD_SDIO_BASE + 0x328)       /* HIF Common firmware SRAM TX pointer register, RO */
#define HWD_SDIO_HCFSRPR   (HWD_SDIO_BASE + 0x32C)       /* HIF Common firmware SRAM RX pointer register, RO  */

/* HIF global firmware configuration reg bits, HWD_SDIO_HGFCR */
#define HWD_SDIO_HGRCR_SW_CLK                                (0x1 << 30)       /* bit30, RW,the balance/non-balance clok is controlled by SW */
#define HWD_SDIO_HGRCR_HW_CLK                                (~(0x1 << 30))    /* bit30, RW, the balance/non-balance clok is controlled by HW, default value */
#define HWD_SDIO_HGRCR_NONBLC_CLK                            (0x1 << 29)       /* bit29, RW, Use non-balance sd clk for pad macro */
#define HWD_SDIO_HGRCR_BLC_CLK                               (~(0x1 << 29))    /* bit29, RW, Use balance sd clk for pad macro, default */
#define HWD_SDIO_HGRCR_PAD_BY_FIRMWARE                       (0x1 << 28)       /* bit28, RW, pad macro control register set by firmware(test mode) */
#define HWD_SDIO_HGRCR_PAD_BY_HOST                           (~(0x1 << 28))    /* bit28, RW, pad macro control register set by host driver(normal mode), defalut */
#define HWD_SDIO_HGRCR_PB_HCLK_DIS                           (0x1 << 27)       /* bit27, RW, Disable AHB clock for PIO-Based funtion */
#define HWD_SDIO_HGRCR_PB_HCLK_EN                            (~(0x1 << 27))    /* bit27, RW, Not Disable AHB clock for PIO-Based funtion, default */
#define HWD_SDIO_HGRCR_EHPI_HCLK_DIS                         (0x1 << 26)       /* bit26, RW, Disable AHB clock for EHPI funtion */
#define HWD_SDIO_HGRCR_EHPI_HCLK_EN                          (~(0x1 << 26))    /* bit26, RW, Not Disable AHB clock for EHPI funtion, default */
#define HWD_SDIO_HGRCR_SPI_HCLK_DIS                          (0x1 << 25)       /* bit25, RW, Disable AHB clock for SPI funtion */
#define HWD_SDIO_HGRCR_SPI_HCLK_EN                           (~(0x1 << 25))    /* bit25, RW, Not Disable AHB clock for SPI funtion, default */
#define HWD_SDIO_HGRCR_SDIO_HCLK_DIS                         (0x1 << 24)       /* bit24, RW, Disable AHB clock for SDIO1 funtion */
#define HWD_SDIO_HGRCR_SDIO_HCLK_EN                          (~(0x1 << 24))    /* bit24, RW, Not Disable AHB clock for SDIO1 funtion, default */
#define HWD_SDIO_HGRCR_FORCE_SD_HS_EN                        (0x1 << 18)       /* bit18, RW, Force SDIO to operate in high speed despite the value of EHS of CCCR */
#define HWD_SDIO_HGRCR_FORCE_SD_HS_DIS                       (~(0x1 << 18))    /* bit18, SDIO is in the operation mode specified in EHS OF CCCR, default */
#define HWD_SDIO_HGRCR_HCLK_NO_GATED                         (0x1 << 17)       /* bit17, RW, 1: AHB clock inside SDIO controller would always turn-on, 1 is default */
#define HWD_SDIO_HGRCR_HCLK_GATED                            (~(0x1 << 17))    /* bit17, RW, 0: SDIO controller would gate some part of AHB clock inside automatically for the unused period */
#define HWD_SDIO_HGRCR_INT_TER_CYC_MASK_EN                   (0x1 << 16)       /* bit16, RW, driver high during the termination cycle only in interrupt period */
#define HWD_SDIO_HGRCR_INT_TER_CYC_MASK_DIS                  (~(0x1 << 16))    /* bit16, RW, always driver high during the termination cycle, default */
#define HWD_SDIO_HGRCR_SDCTL_BUSY                            (0x1 << 10)       /* bit10, SDIO controller is still busy, 0 is not busy */
#define HWD_SDIO_HGRCR_HINT_AS_FW_OB                         (0x1 << 8)        /* bit8, Use interrupt to host as a firmware own back control, 0 is FW own, 1 is host own. */
#define HWD_SDIO_HGRCR_SDIO_PIO_SEL_MASK                     (0x1 << 6)        /* bit6, RO, indicate that if SDIO PIO-mode in used, 1 is used */
#define HWD_SDIO_HGRCR_EHPI_MODE_MASK                        (0x1 << 5)        /* bit5, RO, indicate that if EHPI8-MODE or EHPI8-MODE  in used */
#define HWD_SDIO_HGRCR_SPI_MODE_MASK                         (0x1 << 4)        /* bit4, RO, indicate that if TI-mode or Motor-mode  in used */
#define HWD_SDIO_HGRCR_DB_HIF_SEL_MASK                       (0x7)             /* bit[2 : 0], RO, Host interface for DMA-BASED function in used */

/* HIF Global firmware interrupt status register bits, HWD_SDIO_HGFISR */
#define HWD_SDIO_HGFISR_SD1_SET_DS_INT                       (0x1 << 11)       /* bit11, R/W1C,  If host set the CCCR deep sleep reg,this bit will be set. */
#define HWD_SDIO_HGFISR_SD1_SET_XTAL_UPD_INT                 (0x1 << 10)       /* bit10, R/W1C,  If host set the CCCR XTAL frequency update reg,this bit will be set. */
#define HWD_SDIO_HGFISR_CHG_TO_18V_REQ_INT                   (0x1 << 9)        /* bit9,  R/W1C,  If host send CMD11 to request the device to change its voltage to 1.8v,this bit will be set. */
#define HWD_SDIO_HGFISR_CRC_ERROR_INT                        (0x1 << 8)        /* bit8,  R/W1C,  the status bit of TX data port CRC error interrupt */
#define HWD_SDIO_HGFISR_PB_INT                               (0x1 << 7)        /* bit7, RO,  the status bit of PIO-Based function firmware interrupt  */
#define HWD_SDIO_HGFISR_DB_INT                               (0x1 << 6)        /* bit6, RO,  the status bit of DMA-Based function firmware interrupt  */
#define HWD_SDIO_HGFISR_SDIO_SET_ABT                         (0x1 << 5)        /* bit5, R/W1C, sdio write 1 to SDIO CCCR.ABORT to abort transaction. fireware should discard the data in TX/Rx buffer */
#define HWD_SDIO_HGFISR_SDIO_SET_RES                         (0x1 << 4)        /* bit4, R/W1C, sdio write 1 to SDIO CCCR.RES to assert software reset. fireware should disable the sub-sys */
#define HWD_SDIO_HGFISR_DRV_SET_PB_IOE                       (0x1 << 3)        /* bit3, R/W1C, If host set CCCR.IOE bit of PIO-Based function block, the bit will be set. */
#define HWD_SDIO_HGFISR_DRV_SET_DB_IOE                       (0x1 << 2)        /* bit2, R/W1C, If host set CCCR.IOE bit of DMA-Based function block, the bit will be set. */
#define HWD_SDIO_HGFISR_DRV_CLR_PB_IOE                       (0x1 << 1)        /* bit1, R/W1C, If host clear CCCR.IOE bit of PIO-Based function block, the bit will be set. */
#define HWD_SDIO_HGFISR_DRV_CLR_DB_IOE                       (0x1 << 0)        /* bit0, R/W1C, If host clear CCCR.IOE bit of DMA-Based function block, the bit will be set. */

/* HIF Global firmware interrupt enable register bits, HWD_SDIO_HGFIER */
#define HWD_SDIO_HGFIER_SD1_SET_DS_INT_EN                    (0x1 << 11)       /* bit11, RW,  Enable the related interrupt output. default is disable(0). */
#define HWD_SDIO_HGFIER_SD1_SET_XTAL_UPD_INT_EN              (0x1 << 10)       /* bit10, RW,  Enable the related interrupt output. default is disable(0). */
#define HWD_SDIO_HGFIER_CHG_TO_18V_REQ_INT_EN                (0x1 << 9)        /* bit9,  RW,  Enable the related interrupt output. default is disable(0).*/
#define HWD_SDIO_HGFIER_CRC_ERROR_INT_EN                     (0x1 << 8)        /* bit8,  RW,  Enable the related interrupt output. default is disable(0).*/
#define HWD_SDIO_HGFIER_PB_INT_EN                            (0x1 << 7)        /* bit7, RW,  Enable the related interrupt output. default is disable(0). */
#define HWD_SDIO_HGFIER_DB_INT_EN                            (0x1 << 6)        /* bit6, RW,  Enable the related interrupt output. default is disable(0).  */
#define HWD_SDIO_HGFIER_SDIO_SET_ABT_EN                      (0x1 << 5)        /* bit5, RW, Enable the related interrupt output. default is disable(0). */
#define HWD_SDIO_HGFIER_SDIO_SET_RES_EN                      (0x1 << 4)        /* bit4, RW, Enable the related interrupt output. default is disable(0). */
#define HWD_SDIO_HGFIER_DRV_SET_PB_IOE_EN                    (0x1 << 3)        /* bit3, RW, Enable the related interrupt output. default is disable(0).*/
#define HWD_SDIO_HGFIER_DRV_SET_DB_IOE_EN                    (0x1 << 2)        /* bit2, RW, Enable the related interrupt output. default is disable(0).*/
#define HWD_SDIO_HGFIER_DRV_CLR_PB_IOE_EN                    (0x1 << 1)        /* bit1, RW, Enable the related interrupt output. default is disable(0). */
#define HWD_SDIO_HGFIER_DRV_CLR_DB_IOE_EN                    (0x1 << 0)        /* bit0, RW, Enable the related interrupt output. default is disable(0).*/

/* HIF SDIO Bus delay selection reg bits,HWD_SDIO_HSDBDLSR */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_1_4NS                    (0x7 << 12)       /* bit[14: 12], RW, about 1.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_1_2NS                    (0x6 << 12)       /* bit[14: 12], RW, about 1.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_1_0NS                    (0x5 << 12)       /* bit[14: 12], RW, about 1.0ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_0_8NS                    (0x4 << 12)       /* bit[14: 12], RW, about 0.8ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_0_6NS                    (0x3 << 12)       /* bit[14: 12], RW, about 0.6ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_0_4NS                    (0x2 << 12)       /* bit[14: 12], RW, about 0.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_0_2NS                    (0x1 << 12)       /* bit[14: 12], RW, about 0.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA3_0NS                      (0x0 << 12)       /* bit[14: 12], RW, no delay  */

#define HWD_SDIO_HSDBDLSR_SD1_DATA2_1_4NS                    (0x7 << 8)        /* bit[10: 8], RW, about 1.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA2_1_2NS                    (0x6 << 8)        /* bit[10: 8], RW, about 1.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA2_1_0NS                    (0x5 << 8)        /* bit[10: 8], RW, about 1.0ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA2_0_8NS                    (0x4 << 8)        /* bit[10: 8], RW, about 0.8ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA2_0_6NS                    (0x3 << 8)        /* bit[10: 8], RW, about 0.6ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA2_0_4NS                    (0x2 << 8)        /* bit[10: 8], RW, about 0.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA2_0_2NS                    (0x1 << 8)        /* bit[10: 8], RW, about 0.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA2_0NS                      (0x0 << 8)        /* bit[10: 8], RW, no delay  */

#define HWD_SDIO_HSDBDLSR_SD1_DATA1_1_4NS                    (0x7 << 4)        /* bit[6 : 4], RW, about 1.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA1_1_2NS                    (0x6 << 4)        /* bit[6 : 4], RW, about 1.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA1_1_0NS                    (0x5 << 4)        /* bit[6 : 4], RW, about 1.0ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA1_0_8NS                    (0x4 << 4)        /* bit[6 : 4], RW, about 0.8ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA1_0_6NS                    (0x3 << 4)        /* bit[6 : 4], RW, about 0.6ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA1_0_4NS                    (0x2 << 4)        /* bit[6 : 4], RW, about 0.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA1_0_2NS                    (0x1 << 4)        /* bit[6 : 4], RW, about 0.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA1_0NS                      (0x0 << 4)        /* bit[6 : 4], RW, no delay  */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_1_4NS                    (0x7 << 0)        /* bit[2 : 0], RW, about 1.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_1_2NS                    (0x6 << 0)        /* bit[2 : 0], RW, about 1.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_1_0NS                    (0x5 << 0)        /* bit[2 : 0], RW, about 1.0ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_0_8NS                    (0x4 << 0)        /* bit[2 : 0], RW, about 0.8ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_0_6NS                    (0x3 << 0)        /* bit[2 : 0], RW, about 0.6ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_0_4NS                    (0x2 << 0)        /* bit[2 : 0], RW, about 0.4ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_0_2NS                    (0x1 << 0)        /* bit[2 : 0], RW, about 0.2ns */
#define HWD_SDIO_HSDBDLSR_SD1_DATA0_0NS                      (0x0 << 0)        /* bit[2 : 0], RW, no delay  */


/* Test mode trigger control register reg bits,HWD_SDIO_HGTMTCR */
#define HWD_SDIO_HGTMTCR_TM_BUS_SD_1BIT                      (0x0 << 11)      /* bit[12 : 11], RW, TM_BUS_WIDTH, 0x1 reserved  */
#define HWD_SDIO_HGTMTCR_TM_BUS_SD_4BIT                      (0x2 << 11)      /* bit[12 : 11], RW, TM_BUS_WIDTH  */
#define HWD_SDIO_HGTMTCR_TM_BUS_SD_8BIT                      (0x3 << 11)      /* bit[12 : 11], RW, TM_BUS_WIDTH  */
#define HWD_SDIO_HGTMTCR_TM_BSS_SDR12                        (0x0 << 8)       /* bit[10 : 8], RW, TM_BSS  */
#define HWD_SDIO_HGTMTCR_TM_BSS_SDR25                        (0x1 << 8)       /* bit[10 : 8], RW, TM_BSS  */
#define HWD_SDIO_HGTMTCR_TM_BSS_SDR50                        (0x2 << 8)       /* bit[10 : 8], RW, TM_BSS  */
#define HWD_SDIO_HGTMTCR_TM_BSS_SDR104                       (0x3 << 8)       /* bit[10 : 8], RW, TM_BSS  */
#define HWD_SDIO_HGTMTCR_FW_TRIGGER_CRC_EN                   (0x1 << 1)       /* bit1, RW, FW_TRIGGER_CRC_STS, 1 Enable device send CRS status  */
#define HWD_SDIO_HGTMTCR_FW_TRIGGER_CRC_DIS                  (~(0x1 << 1))    /* bit1, RW, FW_TRIGGER_CRC_STS,0 disable  */
#define HWD_SDIO_HGTMTCR_FW_TRIGGER_TM_DATA_EN               (0x1 << 0)       /* bit0, RW, FW_TRIGGER_TM_DATA, 1 Enable device send response and block data  */
#define HWD_SDIO_HGTMTCR_FW_TRIGGER_TM_DATA_DIS             (~(0x1 << 0))    /* bit0, RW, FW_TRIGGER_TM_DATA,0 disable  */



/* Test mode  control register reg bits,HWD_SDIO_HGTMCR */
#define HWD_SDIO_HGTMCR_TEST_MODE_FW_OWN_HOST                 (0x0 << 24)      /* bit24, RW, 0 HOST has the ownship  */
#define HWD_SDIO_HGTMCR_TEST_MODE_FW_OWN_FW                   (0x1 << 24)      /* bit24, RW, 1 FW has the ownship  */
#define HWD_SDIO_HGTMCR_TEST_MODE_STATUS_MASK                 (0x1 << 8)       /* bit8, RO, TO record the compare result of 
                                                                                                                                            *   latest test mode write. 0 pass, 1 fail  */
#define HWD_SDIO_HGTMCR_TEST_MODE_32BIT                       (0x0 << 0)       /* bit[1 : 0], RW, TEST mode select --32BIT */
#define HWD_SDIO_HGTMCR_TEST_MODE_64BIT                       (0x1 << 0)       /* bit[1 : 0], RW, TEST mode select --64BIT */
#define HWD_SDIO_HGTMCR_TEST_MODE_PRBS                        (0x2 << 0)       /* bit[1 : 0], RW, TEST mode select --PRBS pattern  */



/* HIF Common fireware interrupt status reg bits,HWD_SDIO_HCFISR */
#define HWD_SDIO_HCFISR_RX_DATA_INT                          (0x1 << 12)      /* bit12, W1C, RX_DATA_INT */ 
#define HWD_SDIO_HCFISR_TX_DATA_INT                          (0x1 << 11)      /* bit11, W1C,  TX_DATA_INT */
#define HWD_SDIO_HCFISR_DRV_CLR_FW_OWN                       (0x1 << 9)       /* bit9, W1C,  */
#define HWD_SDIO_HCFISR_DRV_SET_FW_OWN                       (0x1 << 8)       /* bit8, W1C*/

/* HIF Common firmware interrupt enable reg bits,HWD_SDIO_HCFIER */
#define HWD_SDIO_HCFIER_RX_DATA_INT_ENABLE                   (0x1 << 12)      /* bit12, R/W, Enable bit */
#define HWD_SDIO_HCFIER_TX_DATA_INT_ENABLE                   (0x1 << 11)      /* bit11, R/W, Enable bit  */
#define HWD_SDIO_HCFIER_DRV_CLR_FW_OWN_INT_ENABLE            (0x1 << 9)       /* bit9, R/W, Enable bit  */
#define HWD_SDIO_HCFIER_DRV_SET_FW_OWN_INT_ENABLE            (0x1 << 8)       /* bit8, R/W, Enable bit  */

/* HIF Common firmware status control reg bits,HWD_SDIO_HCFSCR */
#define HWD_SDIO_HCFSCR_FIRMWARE_INT                         (0x1 << 9)       /* bit[15 : 9], W1S, W1 will set CHISR.FIRMWARE_INT */
#define HWD_SDIO_HCFSCR_TX_COMPLETE_COUNT_BIT_SHIFT          (4)              /* bit[ 6 :  4], R/W */
#define HWD_SDIO_HCFSCR_TX_COMPLETE_COUNT_BIT_MASK           (0x70)           /* bit[ 6 :  4], R/W */
#define HWD_SDIO_HCFSCR_TX_UNDER_THOLD                       (0x1 << 3)       /* bit3, W1S, W1 will set CHISR.TX_UNDER_THOLD */
#define HWD_SDIO_HCFSCR_TX_EMPTY                             (0x1 << 2)       /* bit2, W1S, W1 will set CHISR.TX_EMPTY */
#define HWD_SDIO_HCFSCR_TX_FW_OWN_BACK_INT                   (0x1 << 0)       /* bit0, W1S */

/* HIF Common firmware control reg bits,HWD_SDIO_HCFCR */
#define HWD_SDIO_HCFCR_RX_PKT_LEN_BIT_SHIFT                  (16)             /* bit[31 : 16], R/W, total length of rx packet, unit is byte*/ 
#define HWD_SDIO_HCFCR_RX_PKT_LEN_BIT_MASK                   (0xffff0000)     /* bit[31 : 16], R/W*/ 
#define HWD_SDIO_HCFCR_HOST_ACCESS_IND                       (0x1 << 10)      /* bit10,  W1C*/ 
#define HWD_SDIO_HCFCR_SDBG_SADDR_SEL                        (0x1 << 8)       /* bit8,  R/W*/ 
#define HWD_SDIO_HCFCR_RX_PKT_RDY                            (0x1 << 4)       /* bit4,  W1S, write 1 to indicate the rx packet and length are ready*/ 
#define HWD_SDIO_HCFCR_G_FUNC_RDY                            (0x1 << 2)       /* bit2,  R/W*/
#define HWD_SDIO_HCFCR_F_FUNC_RDY                            (0x1 << 1)       /* bit1,  R/W*/
#define HWD_SDIO_HCFCR_B_FUNC_RDY                            (0x1 << 0)       /* bit0,  R/W*/

/* HIF Common firmware host buffer control reg bits,HWD_SDIO_HCFHBR */
#define HWD_SDIO_HCFHBR_TX_BUFF_EN                           (0x1 << 29)      /* bit29,  R/W*/
#define HWD_SDIO_HCFHBR_RX_BUFF_EN                           (0x1 << 28)      /* bit28,  R/W*/
#define HWD_SDIO_HCFHBR_TX_BUFF_IDLE_EDGE_INT                (0x1 << 27)      /* bit27,  R/W*/
#define HWD_SDIO_HCFHBR_TX_BUFF_IDLE_LEVEL_INT               (~(0x1 << 27))   /* bit27,  R/W*/
#define HWD_SDIO_HCFHBR_TX_IDLE_CNT_EN                       (0x1 << 26)      /* bit26,  R/W*/
#define HWD_SDIO_HCFHBR_MAX_TX_IDLE_TIME_BIT_SHIFT           (16)             /* bit[25 : 16],  R/W, the unit is AHB clock count */
#define HWD_SDIO_HCFHBR_MAX_TX_IDLE_TIME_BIT_MASK            (0x3ff0000)      /* bit[25 : 16],  R/W*/
#define HWD_SDIO_HCFHBR_MAX_TX_DATA_THOLD_BIT_SHIFT          (0)              /* bit[7 : 0],  R/W, the uint is 16bytes */
#define HWD_SDIO_HCFHBR_MAX_TX_DATA_THOLD_BIT_MASK           (0xff)           /* bit[7 : 0],  R/W*/

/* HIF Common firmware Tx count reg bits,HWD_SDIO_HCFTCR */
#define HWD_SDIO_HCFTCR_TX_BUFF_EMPTY                        (0x1 << 16)      /* bit16,  RO, indicates if TX FIFO is empty */
#define HWD_SDIO_HCFTCR_TX_DATA_CNT_BIT_MASK                 (0xffff)         /* bit[ 15 : 0],  RO, indicates the numbers of data stored in TX FIFO, unit is 1byte */

/* HIF Common firmware Rx count reg bits,HWD_SDIO_HCFRCR */
#define HWD_SDIO_HCFRCR_RX_BUFF_EMPTY                        (0x1 << 16)      /* bit16,  RO, indicates if RX FIFO is empty*/
#define HWD_SDIO_HCFRCR_RX_DATA_CNT_BIT_MASK                 (0xffff)         /* bit[ 15 : 0],  RO, indicates the numbers of data stored in RX FIFO, unit is 1byte */

/*-------------------**
** Log3G    **
**-------------------*/

#define HWD_LOG3G_BASE              (HWD_NO_BASE_ADDR+0x0b410000)     /* R/W LOG3G Base Address */

/*-----------------------**
**  Speech Subsystem (FD216)   **
**------------------------*/

#define HWD_SPH_SUBSYS_BASE          0x91000000

#define HWD_SPH_IDMA_BASE           (HWD_SPH_SUBSYS_BASE+0x00000000)
#define HWD_SPH_SHARE_BASE          (HWD_SPH_SUBSYS_BASE+0x00CA0000)
#define HWD_SPH_DPRAM_BASE          (HWD_SPH_SUBSYS_BASE+0x00207000)
#define HWD_SPH_PATCH_BASE          (HWD_SPH_SUBSYS_BASE+0x00CC0000)
#define HWD_SPH_CONFG_BASE          (HWD_SPH_SUBSYS_BASE+0x00C00000)

/*------------**
** CCIF           **
**------------*/
#define HWD_CCIF_BASE                0x40000000  /* CCIF Base Address */
#if (SYS_ASIC == SA_MT6735)
#define HWD_MDCCIF_REG_BASE          0xA0219000
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_MDCCIF_REG_BASE          0xA020C000
#endif
#define HWD_MDCCIF_CON               (HWD_MDCCIF_REG_BASE+0x00000000)        
#define HWD_MDCCIF_BUSY              (HWD_MDCCIF_REG_BASE+0x00000004)
#define HWD_MDCCIF_START             (HWD_MDCCIF_REG_BASE+0x00000008)
#define HWD_MDCCIF_TCHNUM            (HWD_MDCCIF_REG_BASE+0x0000000c)
#define HWD_MDCCIF_RCHNUM            (HWD_MDCCIF_REG_BASE+0x00000010)
#define HWD_MDCCIF_ACK               (HWD_MDCCIF_REG_BASE+0x00000014)
#define HWD_MDCCIF_CHDATA            (HWD_MDCCIF_REG_BASE+0x00000100)

#if (SYS_ASIC == SA_MT6735)
#define HWD_APCCIF_REG_BASE          0xA0218000
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_APCCIF_REG_BASE          0xA020B000
#endif
#define HWD_APCCIF_CON               (HWD_APCCIF_REG_BASE+0x00000000)        
#define HWD_APCCIF_BUSY              (HWD_APCCIF_REG_BASE+0x00000004)
#define HWD_APCCIF_START             (HWD_APCCIF_REG_BASE+0x00000008)
#define HWD_APCCIF_TCHNUM            (HWD_APCCIF_REG_BASE+0x0000000c)
#define HWD_APCCIF_RCHNUM            (HWD_APCCIF_REG_BASE+0x00000010)
#define HWD_APCCIF_ACK               (HWD_APCCIF_REG_BASE+0x00000014)
#define HWD_APCCIF_CHDATA            (HWD_APCCIF_REG_BASE+0x00000100)


#define HWD_CCIF_SRAM_SIZE           0x200
/* NOTES: we use the ccif last 64 bytes to save the boot fail log  */

/*------------**
** AP INFRA         **
**------------*/
#define HWD_AP_INFRA_BASE            0xA0000000  /* AP INFRA Base Address */

#if (SYS_ASIC == SA_MT6735)
#define PMIC_WRAP_BASE              (HWD_AP_INFRA_BASE+0x1000)
#elif (SYS_ASIC >= SA_MT6755)
#define PMIC_WRAP_BASE              (HWD_AP_INFRA_BASE+0xD000)    
#endif
#define PMIC_WRAP_WACS2_EN          (PMIC_WRAP_BASE+0x98)
#define PMIC_WRAP_INIT_DONE2        (PMIC_WRAP_BASE+0x9C)  
#define PMIC_WRAP_WACS2_CMD         (PMIC_WRAP_BASE+0xA0)    
#define PMIC_WRAP_WACS2_RDATA       (PMIC_WRAP_BASE+0xA4)    
#define PMIC_WRAP_WACS2_VLDCLR      (PMIC_WRAP_BASE+0xA8)   
       
#define PMIC_WRAP_WACS3_EN          (PMIC_WRAP_BASE+0xAC)
#define PMIC_WRAP_INIT_DONE3        (PMIC_WRAP_BASE+0xB0)  
#define PMIC_WRAP_WACS3_CMD         (PMIC_WRAP_BASE+0xB4)    
#define PMIC_WRAP_WACS3_RDATA       (PMIC_WRAP_BASE+0xB8)    
#define PMIC_WRAP_WACS3_VLDCLR      (PMIC_WRAP_BASE+0xBC)

/* Debug Mux registers */
#if (SYS_ASIC == SA_MT6735)
#define HWD_C2K_CONFIG              (HWD_AP_INFRA_BASE + 0x330)
#define HWD_C2K_SATUS               (HWD_AP_INFRA_BASE + 0x334)
#define HWD_C2K_DEBUGMUX_SEL        (HWD_AP_INFRA_BASE + 0x2116D0)
#elif (SYS_ASIC == SA_MT6755) || (SYS_ASIC == SA_MT6750)||(SYS_ASIC == SA_MT6757)
#define HWD_C2K_CONFIG              (HWD_AP_INFRA_BASE + 0x1360)
#define HWD_C2K_SATUS               (HWD_AP_INFRA_BASE + 0x1364)
#define HWD_C2K_DEBUGMUX_SEL        (HWD_AP_INFRA_BASE + 0x56D0)
#elif (SYS_ASIC == SA_MT6797)
#define HWD_C2K_CONFIG              (HWD_AP_INFRA_BASE + 0x1B360)
#define HWD_C2K_SATUS               (HWD_AP_INFRA_BASE + 0x1B364)
#define HWD_C2K_DEBUGMUX_SEL        (HWD_AP_INFRA_BASE + 0x11000)
#elif (SYS_ASIC == SA_ELBRUS)
#define HWD_C2K_CONFIG              (HWD_AP_INFRA_BASE + 0x360)
#define HWD_C2K_SATUS               (HWD_AP_INFRA_BASE + 0x364)
#define HWD_C2K_DEBUGMUX_SEL        (HWD_AP_INFRA_BASE + 0x370500)
#else
#error "Unknown CHIP ID!!!"
#endif


#if (SYS_ASIC == SA_MT6735)
#define LEGACY_REG_ADDR  (HWD_AP_INFRA_BASE + 0x0700)
#define LEGACY_SEL_MASK  (1 << 2)
#elif ((SYS_ASIC == SA_MT6755) || (SYS_ASIC == SA_MT6797) ||(SYS_ASIC == SA_MT6750) || (SYS_ASIC == SA_MT6757))
#define LEGACY_REG_ADDR  (HWD_AP_INFRA_BASE + 0x1F00)
#define LEGACY_SEL_MASK  (1 << 2)
#elif (SYS_ASIC == SA_ELBRUS)
#define LEGACY_REG_ADDR  (HWD_AP_INFRA_BASE + 0x0F00)
#define LEGACY_SEL_MASK  (1 << 3)
#else
#error "Unknown CHIP ID!!!"
#endif

/* AP pinmux related registers */
#if (SYS_ASIC == SA_MT6735)
#define AP_GPIO_MODE_BASE           (HWD_AP_INFRA_BASE+0x211300)
#elif ((SYS_ASIC == SA_MT6755)||(SYS_ASIC == SA_MT6797)||(SYS_ASIC == SA_MT6750)||(SYS_ASIC == SA_MT6757))
#define AP_GPIO_MODE_BASE           (HWD_AP_INFRA_BASE+0x5300)    
#elif (SYS_ASIC == SA_ELBRUS) 
#define AP_GPIO_MODE_BASE           (HWD_AP_INFRA_BASE+0x00370300)
#else
#error "Unknown CHIP ID!!!"
#endif
#define AP_GPIO_MODE1               (AP_GPIO_MODE_BASE+0x0000)
#define AP_GPIO_MODE2               (AP_GPIO_MODE_BASE+0x0010)
#define AP_GPIO_MODE3               (AP_GPIO_MODE_BASE+0x0020)
#define AP_GPIO_MODE4               (AP_GPIO_MODE_BASE+0x0030)
#define AP_GPIO_MODE5               (AP_GPIO_MODE_BASE+0x0040)
#define AP_GPIO_MODE6               (AP_GPIO_MODE_BASE+0x0050)
#define AP_GPIO_MODE7               (AP_GPIO_MODE_BASE+0x0060)
#define AP_GPIO_MODE8               (AP_GPIO_MODE_BASE+0x0070)
#define AP_GPIO_MODE9               (AP_GPIO_MODE_BASE+0x0080)
#define AP_GPIO_MODE10              (AP_GPIO_MODE_BASE+0x0090)
#define AP_GPIO_MODE11              (AP_GPIO_MODE_BASE+0x00A0)
#define AP_GPIO_MODE12              (AP_GPIO_MODE_BASE+0x00B0)
#define AP_GPIO_MODE13              (AP_GPIO_MODE_BASE+0x00C0)
#define AP_GPIO_MODE14              (AP_GPIO_MODE_BASE+0x00D0)
#define AP_GPIO_MODE15              (AP_GPIO_MODE_BASE+0x00E0)
#define AP_GPIO_MODE16              (AP_GPIO_MODE_BASE+0x00F0)
#define AP_GPIO_MODE17              (AP_GPIO_MODE_BASE+0x0100)
#define AP_GPIO_MODE18              (AP_GPIO_MODE_BASE+0x0110)
#define AP_GPIO_MODE19              (AP_GPIO_MODE_BASE+0x0120)
#if ((SYS_ASIC == SA_MT6735)||(SYS_ASIC == SA_MT6797)||(SYS_ASIC == SA_ELBRUS)||(SYS_ASIC == SA_MT6757))
#define AP_GPIO_MODE20              (AP_GPIO_MODE_BASE+0x0130)
#define AP_GPIO_MODE21              (AP_GPIO_MODE_BASE+0x0140)
#endif

#if ((SYS_ASIC == SA_MT6797)||(SYS_ASIC == SA_ELBRUS)||(SYS_ASIC == SA_MT6757))
#define AP_GPIO_MODE22              (AP_GPIO_MODE_BASE+0x0150)
#define AP_GPIO_MODE23              (AP_GPIO_MODE_BASE+0x0160)
#define AP_GPIO_MODE24              (AP_GPIO_MODE_BASE+0x0170)
#endif
#if ((SYS_ASIC == SA_MT6797)||(SYS_ASIC == SA_MT6757))
#define AP_GPIO_MODE25              (AP_GPIO_MODE_BASE+0x0180)
#endif
#if (SYS_ASIC == SA_MT6797)
#define AP_GPIO_MODE26              (AP_GPIO_MODE_BASE+0x0190)
#define AP_GPIO_MODE27              (AP_GPIO_MODE_BASE+0x01A0)
#define AP_GPIO_MODE28              (AP_GPIO_MODE_BASE+0x01B0)
#define AP_GPIO_MODE29              (AP_GPIO_MODE_BASE+0x01C0)
#define AP_GPIO_MODE30              (AP_GPIO_MODE_BASE+0x01D0)
#define AP_GPIO_MODE31              (AP_GPIO_MODE_BASE+0x01E0)
#define AP_GPIO_MODE32              (AP_GPIO_MODE_BASE+0x01F0)
#define AP_GPIO_MODE33              (AP_GPIO_MODE_BASE+0x0200)
#endif

#if (SYS_ASIC == SA_ELBRUS)
#define AP_GPIO_MODE1_MWR           (AP_GPIO_MODE_BASE+0x000C)
#define AP_GPIO_MODE2_MWR           (AP_GPIO_MODE_BASE+0x001C)
#define AP_GPIO_MODE3_MWR           (AP_GPIO_MODE_BASE+0x002C)
#define AP_GPIO_MODE4_MWR           (AP_GPIO_MODE_BASE+0x003C)
#define AP_GPIO_MODE5_MWR           (AP_GPIO_MODE_BASE+0x004C)
#define AP_GPIO_MODE6_MWR           (AP_GPIO_MODE_BASE+0x005C)
#define AP_GPIO_MODE7_MWR           (AP_GPIO_MODE_BASE+0x006C)
#define AP_GPIO_MODE8_MWR           (AP_GPIO_MODE_BASE+0x007C)
#define AP_GPIO_MODE9_MWR           (AP_GPIO_MODE_BASE+0x008C)
#define AP_GPIO_MODE10_MWR          (AP_GPIO_MODE_BASE+0x009C)
#define AP_GPIO_MODE11_MWR          (AP_GPIO_MODE_BASE+0x00AC)
#define AP_GPIO_MODE12_MWR          (AP_GPIO_MODE_BASE+0x00BC)
#define AP_GPIO_MODE13_MWR          (AP_GPIO_MODE_BASE+0x00CC)
#define AP_GPIO_MODE14_MWR          (AP_GPIO_MODE_BASE+0x00DC)
#define AP_GPIO_MODE15_MWR          (AP_GPIO_MODE_BASE+0x00EC)
#define AP_GPIO_MODE16_MWR          (AP_GPIO_MODE_BASE+0x00FC)
#define AP_GPIO_MODE17_MWR          (AP_GPIO_MODE_BASE+0x010C)
#define AP_GPIO_MODE18_MWR          (AP_GPIO_MODE_BASE+0x011C)
#define AP_GPIO_MODE19_MWR          (AP_GPIO_MODE_BASE+0x012C)
#define AP_GPIO_MODE20_MWR          (AP_GPIO_MODE_BASE+0x013C)
#define AP_GPIO_MODE21_MWR          (AP_GPIO_MODE_BASE+0x014C)
#define AP_GPIO_MODE22_MWR          (AP_GPIO_MODE_BASE+0x015C)
#define AP_GPIO_MODE23_MWR          (AP_GPIO_MODE_BASE+0x016C)
#define AP_GPIO_MODE24_MWR          (AP_GPIO_MODE_BASE+0x017C)
#endif

#if (SYS_ASIC == SA_MT6735)
#define AP_GPIO_SMT_G0              (HWD_AP_INFRA_BASE+0x211810)
#define AP_GPIO_SMT_G2              (HWD_AP_INFRA_BASE+0x211A10)
#define AP_GPIO_SMT_G4              (HWD_AP_INFRA_BASE+0x211C10)
#define AP_GPIO_PULLEN0_G0          (HWD_AP_INFRA_BASE+0x211830)
#define AP_GPIO_PULLSEL0_G0         (HWD_AP_INFRA_BASE+0x211850)
#define AP_GPIO_PULLEN0_G1          (HWD_AP_INFRA_BASE+0x211930)
#define AP_GPIO_PULLSEL0_G1         (HWD_AP_INFRA_BASE+0x211950)
#define AP_GPIO_PULLEN0_G2          (HWD_AP_INFRA_BASE+0x211A30)
#define AP_GPIO_PULLSEL0_G2         (HWD_AP_INFRA_BASE+0x211A50)
#define AP_GPIO_PULLEN1_G2          (HWD_AP_INFRA_BASE+0x211A40)
#define AP_GPIO_PULLSEL1_G2         (HWD_AP_INFRA_BASE+0x211A60)
#define AP_GPIO_PUPD0_G0            (HWD_AP_INFRA_BASE+0x211880)
#define AP_GPIO_PUPD0_G2            (HWD_AP_INFRA_BASE+0x211A80)
#define AP_GPIO_PUPD0_G4            (HWD_AP_INFRA_BASE+0x211C80)
#define AP_GPIO_PUPD1_G4            (HWD_AP_INFRA_BASE+0x211C90)
#define AP_GPIO_DRV0_G0             (HWD_AP_INFRA_BASE+0x211870)
#define AP_GPIO_DRV0_G1             (HWD_AP_INFRA_BASE+0x211970)
#define AP_GPIO_DRV0_G2             (HWD_AP_INFRA_BASE+0x211A70)
#define AP_GPIO_DRV0_G3             (HWD_AP_INFRA_BASE+0x211B70)
#define AP_GPIO_DRV0_G4             (HWD_AP_INFRA_BASE+0x211C70)
#define AP_GPIO_DRV1_G2             (HWD_AP_INFRA_BASE+0x211A74)
#elif (SYS_ASIC == SA_MT6755) || (SYS_ASIC == SA_MT6750) || (SYS_ASIC == SA_MT6757) 
#define AP_GPIO_SMT_G0              (HWD_AP_INFRA_BASE+0x2010)
#define AP_GPIO_SMT_G2              (HWD_AP_INFRA_BASE+0x2410)
#define AP_GPIO_SMT_G3              (HWD_AP_INFRA_BASE+0x2610)
#define AP_GPIO_SMT_G4              (HWD_AP_INFRA_BASE+0x2810)
#define AP_GPIO_PULLEN0_G0          (HWD_AP_INFRA_BASE+0x2060)
#define AP_GPIO_PULLSEL0_G0         (HWD_AP_INFRA_BASE+0x2080)
#define AP_GPIO_PULLEN0_G1          (HWD_AP_INFRA_BASE+0x2260)
#define AP_GPIO_PULLSEL0_G1         (HWD_AP_INFRA_BASE+0x2280)
#define AP_GPIO_PULLEN0_G2          (HWD_AP_INFRA_BASE+0x2460)
#define AP_GPIO_PULLSEL0_G2         (HWD_AP_INFRA_BASE+0x2480)
#define AP_GPIO_PULLEN0_G3          (HWD_AP_INFRA_BASE+0x2660)
#define AP_GPIO_PULLSEL0_G3         (HWD_AP_INFRA_BASE+0x2680)
#define AP_GPIO_PULLEN0_G4          (HWD_AP_INFRA_BASE+0x2860)
#define AP_GPIO_PULLSEL0_G4         (HWD_AP_INFRA_BASE+0x2880)
#define AP_GPIO_PUPD0_G0            (HWD_AP_INFRA_BASE+0x20C0)
#define AP_GPIO_PUPD1_G0            (HWD_AP_INFRA_BASE+0x20D0)
#define AP_GPIO_PUPD0_G3            (HWD_AP_INFRA_BASE+0x26C0)
#define AP_GPIO_PUPD0_G4            (HWD_AP_INFRA_BASE+0x28C0)
#define AP_GPIO_PUPD1_G4            (HWD_AP_INFRA_BASE+0x28D0)
#define AP_GPIO_DRV0_G0             (HWD_AP_INFRA_BASE+0x20A0)
#define AP_GPIO_DRV0_G1             (HWD_AP_INFRA_BASE+0x22A0)
#define AP_GPIO_DRV0_G2             (HWD_AP_INFRA_BASE+0x24A0)
#define AP_GPIO_DRV0_G3             (HWD_AP_INFRA_BASE+0x26A0)
#define AP_GPIO_DRV0_G4             (HWD_AP_INFRA_BASE+0x28A0)
#define AP_GPIO_DRV1_G0             (HWD_AP_INFRA_BASE+0x20B0)
#define AP_GPIO_DRV1_G1             (HWD_AP_INFRA_BASE+0x22B0)
#define AP_GPIO_DRV1_G3             (HWD_AP_INFRA_BASE+0x26B0)
#elif (SYS_ASIC == SA_MT6797)
#define AP_GPIO_SMT_CFG0_R          (HWD_AP_INFRA_BASE+0x2830)
#define AP_GPIO_SMT_CFG0_L          (HWD_AP_INFRA_BASE+0x2040)
#define AP_GPIO_DRV_CFG1_B          (HWD_AP_INFRA_BASE+0x25A0)
#define AP_GPIO_DRV_CFG2_B          (HWD_AP_INFRA_BASE+0x25B0)
#define AP_GPIO_DRV_CFG0_L          (HWD_AP_INFRA_BASE+0x20F0)
#define AP_GPIO_PD_CFG2_B           (HWD_AP_INFRA_BASE+0x2550)
#define AP_GPIO_PU_CFG2_B           (HWD_AP_INFRA_BASE+0x2560)
#define AP_GPIO_PD_CFG0_L           (HWD_AP_INFRA_BASE+0x20D0)
#define AP_GPIO_PU_CFG0_L           (HWD_AP_INFRA_BASE+0x20B0)
#define AP_GPIO_PUPD_CFG0_B         (HWD_AP_INFRA_BASE+0x2500)
#define AP_GPIO_R0_CFG0_B           (HWD_AP_INFRA_BASE+0x2510)
#define AP_GPIO_R1_CFG0_B           (HWD_AP_INFRA_BASE+0x2520)
#elif (SYS_ASIC == SA_ELBRUS)
#define AP_GPIO_SMT0_RB             (HWD_AP_INFRA_BASE+0x01C10100)
#define AP_GPIO_SMT0_BR             (HWD_AP_INFRA_BASE+0x01D30100)
#define AP_GPIO_SMT0_LB             (HWD_AP_INFRA_BASE+0x01E00100)
#define AP_GPIO_SMT0_LT             (HWD_AP_INFRA_BASE+0x01E10100)
#define AP_GPIO_SMT0_LT_SET         (HWD_AP_INFRA_BASE+0x01E10104)
#define AP_GPIO_SMT0_LT_CLR         (HWD_AP_INFRA_BASE+0x01E10108)
#define AP_GPIO_SMT0_TR             (HWD_AP_INFRA_BASE+0x01F20100)
#define AP_GPIO_SMT0_TR_SET         (HWD_AP_INFRA_BASE+0x01F20104)
#define AP_GPIO_SMT0_TR_CLR         (HWD_AP_INFRA_BASE+0x01F20108)

#define AP_GPIO_PULLEN0_RB          (HWD_AP_INFRA_BASE+0x01C10400)
#define AP_GPIO_PULLEN0_LB          (HWD_AP_INFRA_BASE+0x01E00400)
#define AP_GPIO_PULLEN0_LT          (HWD_AP_INFRA_BASE+0x01E10400)
#define AP_GPIO_PULLEN1_LT          (HWD_AP_INFRA_BASE+0x01E10410)
#define AP_GPIO_PULLEN0_TR          (HWD_AP_INFRA_BASE+0x01F20400)
#define AP_GPIO_PULLEN1_TR          (HWD_AP_INFRA_BASE+0x01F20410)

#define AP_GPIO_PULLSEL0_RB          (HWD_AP_INFRA_BASE+0x01C10500)
#define AP_GPIO_PULLSEL0_LB          (HWD_AP_INFRA_BASE+0x01E00500)
#define AP_GPIO_PULLSEL0_LT          (HWD_AP_INFRA_BASE+0x01E10500)
#define AP_GPIO_PULLSEL1_LT          (HWD_AP_INFRA_BASE+0x01E10510)
#define AP_GPIO_PULLSEL0_TR          (HWD_AP_INFRA_BASE+0x01F20500)
#define AP_GPIO_PULLSEL1_TR          (HWD_AP_INFRA_BASE+0x01F20510)

#define AP_GPIO_PUPD0_RB             (HWD_AP_INFRA_BASE+0x01C10700)
#define AP_GPIO_PUPD0_BR             (HWD_AP_INFRA_BASE+0x01D30700)
#define AP_GPIO_PUPD1_BR             (HWD_AP_INFRA_BASE+0x01D30710)
#define AP_GPIO_PUPD0_LB             (HWD_AP_INFRA_BASE+0x01E00700)
#define AP_GPIO_PUPD1_LB             (HWD_AP_INFRA_BASE+0x01E00710)
#define AP_GPIO_PUPD0_LT             (HWD_AP_INFRA_BASE+0x01E10700)
#define AP_GPIO_PUPD1_LT             (HWD_AP_INFRA_BASE+0x01E10710)
#define AP_GPIO_PUPD0_TR             (HWD_AP_INFRA_BASE+0x01F20700)
#define AP_GPIO_PUPD1_TR             (HWD_AP_INFRA_BASE+0x01F20710)

#define AP_GPIO_DRV0_RB              (HWD_AP_INFRA_BASE+0x01C10600)
#define AP_GPIO_DRV1_RB              (HWD_AP_INFRA_BASE+0x01C10610)
#define AP_GPIO_DRV0_BR              (HWD_AP_INFRA_BASE+0x01D30600)
#define AP_GPIO_DRV0_LB              (HWD_AP_INFRA_BASE+0x01E00600)
#define AP_GPIO_DRV0_LT              (HWD_AP_INFRA_BASE+0x01E10600)
#define AP_GPIO_DRV1_LT              (HWD_AP_INFRA_BASE+0x01E10610)
#define AP_GPIO_DRV2_LT              (HWD_AP_INFRA_BASE+0x01E10620)
#define AP_GPIO_DRV0_TR              (HWD_AP_INFRA_BASE+0x01F20600)
#define AP_GPIO_DRV1_TR              (HWD_AP_INFRA_BASE+0x01F20610)
#define AP_GPIO_DRV2_TR              (HWD_AP_INFRA_BASE+0x01F20620)
#endif

#if (SYS_ASIC >= SA_MT6755)
/*** BSI/MIPI/BPI ***/
#if (SYS_ASIC == SA_MT6797)
#define DSIM_BSI_TOP_BASE                           (0xA0229000)
#else
#define DSIM_BSI_TOP_BASE                           (0xA021F000)
#endif
#define DSIM_BSI_TOP_MM_BASE                        (DSIM_BSI_TOP_BASE + 0x0000)
#define DSIM_BSI_TOP_RFIC_BASE                      (DSIM_BSI_TOP_BASE + 0x2000)
#define DSIM_BSI_TOP_PMIC_BASE                      (DSIM_BSI_TOP_BASE + 0x3000)
#define DSIM_BSI_TOP_MIPI0_BASE                     (DSIM_BSI_TOP_BASE + 0x3800)
#define DSIM_BSI_TOP_MIPI1_BASE                     (DSIM_BSI_TOP_BASE + 0x4000)
#define DSIM_BSI_TOP_MIPI2_BASE                     (DSIM_BSI_TOP_BASE + 0x4800)
#define DSIM_BSI_TOP_MIPI3_BASE                     (DSIM_BSI_TOP_BASE + 0x5000)

#define DSIM_BSI_MM_IMM_MD1_RFIC1_CONF_ADDR         (DSIM_BSI_TOP_MM_BASE + 0x0004)
#define DSIM_BSI_MM_IMM_MD1_RFIC1_WDATA_1_ADDR      (DSIM_BSI_TOP_MM_BASE + 0x0008)
#define DSIM_BSI_MM_IMM_MD1_RFIC1_STATUS_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x000C)
#define DSIM_BSI_MM_IMM_MD1_RFIC1_RDAT_L_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0010)
#define DSIM_BSI_MM_IMM_MD1_RFIC1_RDAT_H_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0014)

#define DSIM_BSI_MM_IMM_MD1_MIPI0_CONF_ADDR         (DSIM_BSI_TOP_MM_BASE + 0x0060)
#define DSIM_BSI_MM_IMM_MD1_MIPI0_WDATA_1_ADDR      (DSIM_BSI_TOP_MM_BASE + 0x0064)
#define DSIM_BSI_MM_IMM_MD1_MIPI0_STATUS_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0068)
#define DSIM_BSI_MM_IMM_MD1_MIPI0_RDAT_L_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0074)
#define DSIM_BSI_MM_IMM_MD1_MIPI0_RDAT_H_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0078)

#define DSIM_BSI_MM_IMM_MD1_MIPI1_CONF_ADDR         (DSIM_BSI_TOP_MM_BASE + 0x0080)
#define DSIM_BSI_MM_IMM_MD1_MIPI1_WDATA_1_ADDR      (DSIM_BSI_TOP_MM_BASE + 0x0084)
#define DSIM_BSI_MM_IMM_MD1_MIPI1_STATUS_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0088)
#define DSIM_BSI_MM_IMM_MD1_MIPI1_RDAT_L_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0094)
#define DSIM_BSI_MM_IMM_MD1_MIPI1_RDAT_H_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x0098)

#define DSIM_BSI_MM_IMM_MD1_MIPI2_CONF_ADDR         (DSIM_BSI_TOP_MM_BASE + 0x00A0)
#define DSIM_BSI_MM_IMM_MD1_MIPI2_WDATA_1_ADDR      (DSIM_BSI_TOP_MM_BASE + 0x00A4)
#define DSIM_BSI_MM_IMM_MD1_MIPI2_STATUS_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x00A8)
#define DSIM_BSI_MM_IMM_MD1_MIPI2_RDAT_L_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x00B4)
#define DSIM_BSI_MM_IMM_MD1_MIPI2_RDAT_H_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x00B8)

#define DSIM_BSI_MM_IMM_MD1_MIPI3_CONF_ADDR         (DSIM_BSI_TOP_MM_BASE + 0x00C0)
#define DSIM_BSI_MM_IMM_MD1_MIPI3_WDATA_1_ADDR      (DSIM_BSI_TOP_MM_BASE + 0x00C4)
#define DSIM_BSI_MM_IMM_MD1_MIPI3_STATUS_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x00C8)
#define DSIM_BSI_MM_IMM_MD1_MIPI3_RDAT_L_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x00D4)
#define DSIM_BSI_MM_IMM_MD1_MIPI3_RDAT_H_ADDR       (DSIM_BSI_TOP_MM_BASE + 0x00D8)

#define DSIM_RFIC_BSISPI_PORT_PARAM_ADDR            (DSIM_BSI_TOP_RFIC_BASE + 0x0004)
#define DSIM_RFIC_BSISPI_SW_MODE_ADDR               (DSIM_BSI_TOP_RFIC_BASE + 0x0008)
#define DSIM_RFIC_BSISPI_IC0_PARAM1_ADDR            (DSIM_BSI_TOP_RFIC_BASE + 0x0010)
#define DSIM_RFIC_BSISPI_IC0_PARAM2_ADDR            (DSIM_BSI_TOP_RFIC_BASE + 0x0014)
#define DSIM_RFIC_BSISPI_IC1_PARAM1_ADDR            (DSIM_BSI_TOP_RFIC_BASE + 0x0020)
#define DSIM_RFIC_BSISPI_IC1_PARAM2_ADDR            (DSIM_BSI_TOP_RFIC_BASE + 0x0024)
#define DSIM_RFIC_BSISPI_LOGGER_PARAM_ADDR          (DSIM_BSI_TOP_RFIC_BASE + 0x0030)

#define DSIM_MIPI0_BSISPI_PORT_PARAM_ADDR           (DSIM_BSI_TOP_MIPI0_BASE + 0x0004)
#define DSIM_MIPI0_BSISPI_IC0_PARAM1_ADDR           (DSIM_BSI_TOP_MIPI0_BASE + 0x0010)
#define DSIM_MIPI0_BSISPI_IC0_PARAM2_ADDR           (DSIM_BSI_TOP_MIPI0_BASE + 0x0014)
#define DSIM_MIPI0_BSISPI_LOGGER_PARAM_ADDR         (DSIM_BSI_TOP_MIPI0_BASE + 0x0030)

#define DSIM_MIPI1_BSISPI_PORT_PARAM_ADDR           (DSIM_BSI_TOP_MIPI1_BASE + 0x0004)
#define DSIM_MIPI1_BSISPI_IC0_PARAM1_ADDR           (DSIM_BSI_TOP_MIPI1_BASE + 0x0010)
#define DSIM_MIPI1_BSISPI_IC0_PARAM2_ADDR           (DSIM_BSI_TOP_MIPI1_BASE + 0x0014)
#define DSIM_MIPI1_BSISPI_LOGGER_PARAM_ADDR         (DSIM_BSI_TOP_MIPI1_BASE + 0x0030)

#define DSIM_MIPI2_BSISPI_PORT_PARAM_ADDR           (DSIM_BSI_TOP_MIPI2_BASE + 0x0004)
#define DSIM_MIPI2_BSISPI_IC0_PARAM1_ADDR           (DSIM_BSI_TOP_MIPI2_BASE + 0x0010)
#define DSIM_MIPI2_BSISPI_IC0_PARAM2_ADDR           (DSIM_BSI_TOP_MIPI2_BASE + 0x0014)
#define DSIM_MIPI2_BSISPI_LOGGER_PARAM_ADDR         (DSIM_BSI_TOP_MIPI2_BASE + 0x0030)

#define DSIM_MIPI3_BSISPI_PORT_PARAM_ADDR           (DSIM_BSI_TOP_MIPI3_BASE + 0x0004)
#define DSIM_MIPI3_BSISPI_IC0_PARAM1_ADDR           (DSIM_BSI_TOP_MIPI3_BASE + 0x0010)
#define DSIM_MIPI3_BSISPI_IC0_PARAM2_ADDR           (DSIM_BSI_TOP_MIPI3_BASE + 0x0014)
#define DSIM_MIPI3_BSISPI_LOGGER_PARAM_ADDR         (DSIM_BSI_TOP_MIPI3_BASE + 0x0030)


#define DSIM_BPI_TOP_BASE                           (DSIM_BSI_TOP_BASE - 0x1000)

#define DSIM_BPI_TOP_OUT_OR_3100_ADDR               (DSIM_BPI_TOP_BASE + 0x0004)
#define DSIM_BPI_TOP_OUT_OR_4732_ADDR               (DSIM_BPI_TOP_BASE + 0x0008)
#define DSIM_BPI_TOP_OUT_AND_3100_ADDR              (DSIM_BPI_TOP_BASE + 0x000C)
#define DSIM_BPI_TOP_OUT_AND_4732_ADDR              (DSIM_BPI_TOP_BASE + 0x0010)
#define DSIM_BPI_TOP_MASK_MD1_G_3100_ADDR           (DSIM_BPI_TOP_BASE + 0x0014)
#define DSIM_BPI_TOP_MASK_MD1_G_4732_ADDR           (DSIM_BPI_TOP_BASE + 0x0018)
#define DSIM_BPI_TOP_MASK_MD1_W_3100_ADDR           (DSIM_BPI_TOP_BASE + 0x001C)
#define DSIM_BPI_TOP_MASK_MD1_W_4732_ADDR           (DSIM_BPI_TOP_BASE + 0x0020)
#define DSIM_BPI_TOP_MASK_MD1_T_3100_ADDR           (DSIM_BPI_TOP_BASE + 0x0024)
#define DSIM_BPI_TOP_MASK_MD1_T_4732_ADDR           (DSIM_BPI_TOP_BASE + 0x0028)
#define DSIM_BPI_TOP_MASK_MD1_L_3100_ADDR           (DSIM_BPI_TOP_BASE + 0x002C)
#define DSIM_BPI_TOP_MASK_MD1_L_4732_ADDR           (DSIM_BPI_TOP_BASE + 0x0030)
#define DSIM_BPI_TOP_MASK_MD3_C_3100_ADDR           (DSIM_BPI_TOP_BASE + 0x0044)
#define DSIM_BPI_TOP_MASK_MD3_C_4732_ADDR           (DSIM_BPI_TOP_BASE + 0x0048)

/* Temp Sharing Module */
#define TEMP_SHARE_MODULE_BASE               HWD_AP_INFRA_BASE+0x18000
#define TEMP_SHARE_MODULE_CTRL_REG          (TEMP_SHARE_MODULE_BASE+0x00)
#define TEMP_SHARE_MODULE_RATIO_REG         (TEMP_SHARE_MODULE_BASE+0x04)  
#define TEMP_SHARE_MODULE_BANKPTR_REG       (TEMP_SHARE_MODULE_BASE+0x08)    
#define TEMP_SHARE_MODULE_LATESTSTATUS_REG  (TEMP_SHARE_MODULE_BASE+0x0C)    
#define TEMP_SHARE_MODULE_BANKSTATUS_REG    (TEMP_SHARE_MODULE_BASE+0x10)
#define TEMP_SHARE_MODULE_LATEST_REG        (TEMP_SHARE_MODULE_BASE+0x14)
#define TEMP_SHARE_MODULE_BANK0_REG         (TEMP_SHARE_MODULE_BASE+0x18)
#define TEMP_SHARE_MODULE_BANK1_REG         (TEMP_SHARE_MODULE_BASE+0x1C)
#define TEMP_SHARE_MODULE_BANK2_REG         (TEMP_SHARE_MODULE_BASE+0x20)
#define TEMP_SHARE_MODULE_BANK3_REG         (TEMP_SHARE_MODULE_BASE+0x24)
#define TEMP_SHARE_MODULE_BANK4_REG         (TEMP_SHARE_MODULE_BASE+0x28)
#define TEMP_SHARE_MODULE_BANK5_REG         (TEMP_SHARE_MODULE_BASE+0x2C)
#define TEMP_SHARE_MODULE_BANK6_REG         (TEMP_SHARE_MODULE_BASE+0x30)
#define TEMP_SHARE_MODULE_BANK7_REG         (TEMP_SHARE_MODULE_BASE+0x34)
#define TEMP_SHARE_MODULE_BANK8_REG         (TEMP_SHARE_MODULE_BASE+0x38)
#define TEMP_SHARE_MODULE_BANK9_REG         (TEMP_SHARE_MODULE_BASE+0x3C)
#define TEMP_SHARE_MODULE_BANK10_REG        (TEMP_SHARE_MODULE_BASE+0x40)
#define TEMP_SHARE_MODULE_BANK11_REG        (TEMP_SHARE_MODULE_BASE+0x44)
#define TEMP_SHARE_MODULE_BANK12_REG        (TEMP_SHARE_MODULE_BASE+0x48)
#define TEMP_SHARE_MODULE_BANK13_REG        (TEMP_SHARE_MODULE_BASE+0x4C)
#define TEMP_SHARE_MODULE_BANK14_REG        (TEMP_SHARE_MODULE_BASE+0x50)
#define TEMP_SHARE_MODULE_BANK15_REG        (TEMP_SHARE_MODULE_BASE+0x54)
#define TEMP_SHARE_MODULE_BANK16_REG        (TEMP_SHARE_MODULE_BASE+0x58)
#define TEMP_SHARE_MODULE_BANK17_REG        (TEMP_SHARE_MODULE_BASE+0x5C)
#define TEMP_SHARE_MODULE_BANK18_REG        (TEMP_SHARE_MODULE_BASE+0x60)
#define TEMP_SHARE_MODULE_BANK19_REG        (TEMP_SHARE_MODULE_BASE+0x64)
#define TEMP_SHARE_MODULE_BANK20_REG        (TEMP_SHARE_MODULE_BASE+0x68)
#define TEMP_SHARE_MODULE_BANK21_REG        (TEMP_SHARE_MODULE_BASE+0x6C)
#define TEMP_SHARE_MODULE_BANK22_REG        (TEMP_SHARE_MODULE_BASE+0x70)
#define TEMP_SHARE_MODULE_BANK23_REG        (TEMP_SHARE_MODULE_BASE+0x74)
#define TEMP_SHARE_MODULE_BANK24_REG        (TEMP_SHARE_MODULE_BASE+0x78)
#define TEMP_SHARE_MODULE_BANK25_REG        (TEMP_SHARE_MODULE_BASE+0x7C)
#define TEMP_SHARE_MODULE_BANK26_REG        (TEMP_SHARE_MODULE_BASE+0x80)
#define TEMP_SHARE_MODULE_BANK27_REG        (TEMP_SHARE_MODULE_BASE+0x84)
#define TEMP_SHARE_MODULE_BANK28_REG        (TEMP_SHARE_MODULE_BASE+0x88)
#define TEMP_SHARE_MODULE_BANK29_REG        (TEMP_SHARE_MODULE_BASE+0x8C)
#define TEMP_SHARE_MODULE_BANK30_REG        (TEMP_SHARE_MODULE_BASE+0x90)
#define TEMP_SHARE_MODULE_BANK31_REG        (TEMP_SHARE_MODULE_BASE+0x94)

/* TEMP_SHARE_MODULE_LATEST_REG  */

#define TEMP_READY                          0x01       
#endif


/*------------**
** Interrupts **
**------------*/
#define HWD_CPI_BASE_ADDR      (HWD_CP_BASE_ADDR+0x0000a000)

#define HWD_CP_ISR0_L          (HWD_CPI_BASE_ADDR+0x00000000)  /* R/W  CP Low Priority Interrupt Status 0        0x0000 */
#define HWD_CP_IMR0_L          (HWD_CPI_BASE_ADDR+0x00000004)  /* R/W  CP Low Priority Interrupt Mask 0          0xffff */
#define HWD_CP_SRC0_L          (HWD_CPI_BASE_ADDR+0x00000008)  /* R    CP Low Priority Source 0                  0x0000 */

#define HWD_CP_ISR1_L          (HWD_CPI_BASE_ADDR+0x00000010)  /* R/W  CP Low Priority Interrupt Status 1        0x0000 */
#define HWD_CP_IMR1_L          (HWD_CPI_BASE_ADDR+0x00000014)  /* R/W  CP Low Priority Interrupt Mask 1          0xffff */
#define HWD_CP_SRC1_L          (HWD_CPI_BASE_ADDR+0x00000018)  /* R    CP Low Priority Source 1                  0x0000 */

#define HWD_CP_ISR2_L          (HWD_CPI_BASE_ADDR+0x00000020)  /* R/W  CP Low Priority Interrupt Status 2        0x0000 */
#define HWD_CP_IMR2_L          (HWD_CPI_BASE_ADDR+0x00000024)  /* R/W  CP Low Priority Interrupt Mask 2          0xffff */
#define HWD_CP_SRC2_L          (HWD_CPI_BASE_ADDR+0x00000028)  /* R    CP Low Priority Source 2                  0x0000 */

#define HWD_CP_ISR3_L          (HWD_CPI_BASE_ADDR+0x00000030)  /* R/W  CP Low Priority Interrupt Status 3        0x0000 */
#define HWD_CP_IMR3_L          (HWD_CPI_BASE_ADDR+0x00000034)  /* R/W  CP Low Priority Interrupt Mask 3          0xffff */
#define HWD_CP_SRC3_L          (HWD_CPI_BASE_ADDR+0x00000038)  /* R    CP Low Priority Source 3                  0x0000 */

#define HWD_CP_ISR4_L          (HWD_CPI_BASE_ADDR+0x00000040)  /* R/W  CP Low Priority Interrupt Status 4        0x0000 */
#define HWD_CP_IMR4_L          (HWD_CPI_BASE_ADDR+0x00000044)  /* R/W  CP Low Priority Interrupt Mask 4          0xffff */
#define HWD_CP_SRC4_L          (HWD_CPI_BASE_ADDR+0x00000048)  /* R    CP Low Priority Source 4                  0x0000 */

#define HWD_CP_ISR5_L          (HWD_CPI_BASE_ADDR+0x00000050)  /* R/W  CP Low Priority Interrupt Status 4        0x0000 */
#define HWD_CP_IMR5_L          (HWD_CPI_BASE_ADDR+0x00000054)  /* R/W  CP Low Priority Interrupt Mask 4          0xffff */
#define HWD_CP_SRC5_L          (HWD_CPI_BASE_ADDR+0x00000058)  /* R    CP Low Priority Source 4                  0x0000 */

#define HWD_CP_ISR6_L          (HWD_CPI_BASE_ADDR+0x0060)  /* R/W  CP Low Priority Interrupt Status 6        0x0000 */
#define HWD_CP_IMR6_L          (HWD_CPI_BASE_ADDR+0x0064)  /* R/W  CP Low Priority Interrupt Mask 6          0xffff */
#define HWD_CP_SRC6_L          (HWD_CPI_BASE_ADDR+0x0068)  /* R    CP Low Priority Source 6                  0x0000 */

#define HWD_CP_ISR7_L          (HWD_CPI_BASE_ADDR+0x0070)  /* R/W  CP Low Priority Interrupt Status 7        0x0000 */
#define HWD_CP_IMR7_L          (HWD_CPI_BASE_ADDR+0x0074)  /* R/W  CP Low Priority Interrupt Mask 7          0xffff */
#define HWD_CP_SRC7_L          (HWD_CPI_BASE_ADDR+0x0078)  /* R    CP Low Priority Source 7                  0x0000 */

#define HWD_CP_ISR8_L          (HWD_CPI_BASE_ADDR+0x0080)  /* R/W  CP Low Priority Interrupt Status 8        0x0000 */
#define HWD_CP_IMR8_L          (HWD_CPI_BASE_ADDR+0x0084)  /* R/W  CP Low Priority Interrupt Mask 8          0xffff */
#define HWD_CP_SRC8_L          (HWD_CPI_BASE_ADDR+0x0088)  /* R    CP Low Priority Source 8                  0x0000 */

#define HWD_CP_ISR0_H          (HWD_CPI_BASE_ADDR+0x00000200)  /* R/W  CP High Priority Interrupt Status 0       0x0000 */
#define HWD_CP_IMR0_H          (HWD_CPI_BASE_ADDR+0x00000204)  /* R/W  CP High Priority Interrupt Mask 0         0xffff */
#define HWD_CP_SRC0_H          (HWD_CPI_BASE_ADDR+0x00000208)  /* R    CP High Priority Source 0                 0x0000 */

#define HWD_CP_ISR1_H          (HWD_CPI_BASE_ADDR+0x00000210)  /* R/W  CP High Priority Interrupt Status 1       0x0000 */
#define HWD_CP_IMR1_H          (HWD_CPI_BASE_ADDR+0x00000214)  /* R/W  CP High Priority Interrupt Mask 1         0xffff */
#define HWD_CP_SRC1_H          (HWD_CPI_BASE_ADDR+0x00000218)  /* R    CP High Priority Source 1                 0x0000 */

#define HWD_CP_ISR2_H          (HWD_CPI_BASE_ADDR+0x00000220)  /* R/W  CP High Priority Interrupt Status 2       0x0000 */
#define HWD_CP_IMR2_H          (HWD_CPI_BASE_ADDR+0x00000224)  /* R/W  CP High Priority Interrupt Mask 2         0xffff */
#define HWD_CP_SRC2_H          (HWD_CPI_BASE_ADDR+0x00000228)  /* R    CP High Priority Source 2                 0x0000 */

#define HWD_CP_ISR3_H          (HWD_CPI_BASE_ADDR+0x00000230)  /* R/W  CP High Priority Interrupt Status 3       0x0000 */
#define HWD_CP_IMR3_H          (HWD_CPI_BASE_ADDR+0x00000234)  /* R/W  CP High Priority Interrupt Mask 3         0xffff */
#define HWD_CP_SRC3_H          (HWD_CPI_BASE_ADDR+0x00000238)  /* R    CP High Priority Source 3                 0x0000 */

#define HWD_CP_ISR4_H          (HWD_CPI_BASE_ADDR+0x00000240)  /* R/W  CP High Priority Interrupt Status 4       0x0000 */
#define HWD_CP_IMR4_H          (HWD_CPI_BASE_ADDR+0x00000244)  /* R/W  CP High Priority Interrupt Mask 4         0xffff */
#define HWD_CP_SRC4_H          (HWD_CPI_BASE_ADDR+0x00000248)  /* R    CP High Priority Source 4                 0x0000 */

#define HWD_CP_ISR5_H          (HWD_CPI_BASE_ADDR+0x00000250)  /* R/W  CP High Priority Interrupt Status 5       0x0000 */
#define HWD_CP_IMR5_H          (HWD_CPI_BASE_ADDR+0x00000254)  /* R/W  CP High Priority Interrupt Mask 5         0xffff */
#define HWD_CP_SRC5_H          (HWD_CPI_BASE_ADDR+0x00000258)  /* R    CP High Priority Source 5                 0x0000 */

#define HWD_CP_ISR6_H          (HWD_CPI_BASE_ADDR+0x00000260)  /* R/W  CP High Priority Interrupt Status 6       0x0000 */
#define HWD_CP_IMR6_H          (HWD_CPI_BASE_ADDR+0x00000264)  /* R/W  CP High Priority Interrupt Mask 6         0xffff */
#define HWD_CP_SRC6_H          (HWD_CPI_BASE_ADDR+0x00000268)  /* R    CP High Priority Source 6                 0x0000 */

#define HWD_CP_ISR7_H          (HWD_CPI_BASE_ADDR+0x00000270)  /* R/W  CP High Priority Interrupt Status 7       0x0000 */
#define HWD_CP_IMR7_H          (HWD_CPI_BASE_ADDR+0x00000274)  /* R/W  CP High Priority Interrupt Mask 7         0xffff */
#define HWD_CP_SRC7_H          (HWD_CPI_BASE_ADDR+0x00000278)  /* R    CP High Priority Source 7                 0x0000 */

#define HWD_CP_ISR8_H          (HWD_CPI_BASE_ADDR+0x00000280)  /* R/W  CP High Priority Interrupt Status 8       0x0000 */
#define HWD_CP_IMR8_H          (HWD_CPI_BASE_ADDR+0x00000284)  /* R/W  CP High Priority Interrupt Mask 8         0xffff */
#define HWD_CP_SRC8_H          (HWD_CPI_BASE_ADDR+0x00000288)  /* R    CP High Priority Source 8                 0x0000 */

/*-------------------------**
** CP Interrupt Generation **
**-------------------------*/

#define HWD_C2C_SW0            (HWD_CPI_BASE_ADDR+0x00000420)  /*   W  CP to CP Interrupt Generation                    */
#define HWD_C2C_SW1            (HWD_CPI_BASE_ADDR+0x00000424)  /*   W  CP to CP Interrupt Generation                    */

/*-------------------------------**
** CP Interrupt Priority Sorting **
**-------------------------------*/

/* IRQ/FIQ Interrupt Priority Control registers */
#define HWD_INTPRY_CTRL_IRQ    (HWD_CPI_BASE_ADDR+0x00000460)  /* R/W  IRQ Int priority sorting control register 0x0000 */
#define HWD_IVR_IDX_IRQ        (HWD_CPI_BASE_ADDR+0x00000464)  /*   W  IRQ Int vector index                             */
#define HWD_INTPRY_CTRL_FIQ    (HWD_CPI_BASE_ADDR+0x00000480)  /* R/W  FIQ Int priority sorting control register 0x0000 */
#define HWD_IVR_IDX_FIQ        (HWD_CPI_BASE_ADDR+0x00000484)  /*   W  FIQ Int vector index                             */

/* There are 72 priority registers for IRQ
 * The first one is HWD_STAT0_BIT01_GPRY_IRQ at address   HWD_CPI_BASE_ADDR+0x00000800
 * and the last one HWD_STAT8_BIT1415_GPRY_IRQ at address HWD_CPI_BASE_ADDR+0x0000091C
 */
#define HWD_STAT0_BIT01_GPRY_IRQ    (HWD_CPI_BASE_ADDR+0x00000800)  /* R/W  STAT0 BIT0 & 1 group priority IRQ           */

/* There are 72 priority registers for FIQ
 * The first one is HWD_STAT0_BIT01_GPRY_FIQ at address   HWD_CPI_BASE_ADDR+0x00000A00
 * and the last one HWD_STAT9_BIT1415_GPRY_FIQ at address HWD_CPI_BASE_ADDR+0x00000B1C
 */
#define HWD_STAT0_BIT01_GPRY_FIQ    (HWD_CPI_BASE_ADDR+0x00000A00)  /* R/W  STAT0 BIT0 & 1 group priority FIQ           */


/*---------------**
** Modem Mailbox **
**---------------*/

#define HWD_MBM_C2M_D_RRDY     (HWD_CPI_BASE_ADDR+0x00000408) /*   W  CP->Modem Mailbox Data Read Ready */
#define HWD_MBM_C2M_C_RRDY     (HWD_CPI_BASE_ADDR+0x0000040c) /*   W  CP->Modem Mailbox Control Read Ready */
#define HWD_MBM_C2M_D_WRDY     (HWD_CPI_BASE_ADDR+0x00000410) /*   W  CP->Modem Mailbox Data Write Ready */
#define HWD_MBM_C2M_C_WRDY     (HWD_CPI_BASE_ADDR+0x00000414) /*   W  CP->Modem Mailbox Control Write Ready */
#define HWD_MBV_C2M_FMB_INT    (HWD_CPI_BASE_ADDR+0x0000041c) /*   W  CP->DSPM, CP has finished reading Fast Mailbox   */

#define HWD_MBM_DAT_TX_BASE    (HWD_NO_BASE_ADDR+0x0ba00000)  /*   W  CP -> DSPM Mailbox Buffer (768 * 16) */
#define HWD_MBM_DAT_RX_BASE    (HWD_NO_BASE_ADDR+0x0ba01000)  /*   R  DSPM -> CP Mailbox Buffer (500 * 16) */

/*--------**
** Timers **
**--------*/
#define HWD_CP_TIMER_ADDR      (HWD_CP_BASE_ADDR+0x00001000)

/* CTS Timers */
#define HWD_CTS_CMP0           (HWD_CP_TIMER_ADDR+0x00000000)  /* R/W  CP Timer Strobe #0 Comparator Register    0x0000 */
#define HWD_CTS_CMP1           (HWD_CP_TIMER_ADDR+0x00000004)  /* R/W  CP Timer Strobe #1 Comparator Register    0x0000 */
#define HWD_CTS_CMP2           (HWD_CP_TIMER_ADDR+0x00000008)  /* R/W  CP Timer Strobe #2 Comparator Register    0x0000 */
#define HWD_CTS_CMP3           (HWD_CP_TIMER_ADDR+0x0000000c)  /* R/W  CP Timer Strobe #3 Comparator Register    0x0000 */
#define HWD_CTS_CT_CNT_DOUT    (HWD_CP_TIMER_ADDR+0x00000010)  /* R    CP Timer Current Counter Value Register   0x0000 */
#define HWD_CTS_CT_CNTLD_TC    (HWD_CP_TIMER_ADDR+0x00000014)  /* R/W  CP Timer Rollover Count Register          0x00a0 */
#define HWD_CTS_CT_CNT_CTRL    (HWD_CP_TIMER_ADDR+0x00000018)  /* R/W  CP Timer Counter Control Register         0x0000 */
#define HWD_CTS_CT_RDY_FLG_N   (HWD_CP_TIMER_ADDR+0x0000001c)  /* R    Ready flag for CTS logic                  0x0000 */
#define HWD_CTS2_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000024)  /* R/W  CTS2 timer reference select               0x0000 */
#define HWD_CTS2_EN            (HWD_CP_TIMER_ADDR+0x00000028)  /* R/W  CTS2 compare enable                       0x0000 */
#define HWD_CTS3_REF_SEL       (HWD_CP_TIMER_ADDR+0x0000002c)  /* R/W  CTS3 timer reference select               0x0000 */
#define HWD_CTS3_EN            (HWD_CP_TIMER_ADDR+0x00000030)  /* R/W  CTS3 compare enable                       0x0000 */
#define HWD_CTS_CT_CNT80K      (HWD_CP_TIMER_ADDR+0x00000034)  /* R    CP Timer Current 80KHz Counter Value Register 0x0000 */
#define HWD_CTS4_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000118)  /* R/W  CTS4 timer reference select               0x0000 */
#define HWD_CTS4_EN            (HWD_CP_TIMER_ADDR+0x0000011C)  /* R/W  CTS4 compare enable                       0x0000 */
#define HWD_CTS_CMP4           (HWD_CP_TIMER_ADDR+0x00000120)  /* R/W  CP Timer Strobe #0 Comparator Register    0x0000 */  
#define HWD_CTS5_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000124)  /* R/W  CTS5 timer reference select               0x0000 */
#define HWD_CTS5_EN            (HWD_CP_TIMER_ADDR+0x00000128)  /* R/W  CTS5 compare enable                       0x0000 */
#define HWD_CTS_CMP5           (HWD_CP_TIMER_ADDR+0x0000012C)  /* R/W  CP Timer Strobe #1 Comparator Register    0x0000 */
#define HWD_CTS6_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000130)  /* R/W  CTS6 timer reference select               0x0000 */
#define HWD_CTS6_EN            (HWD_CP_TIMER_ADDR+0x00000134)  /* R/W  CTS6 compare enable                       0x0000 */
#define HWD_CTS_CMP6           (HWD_CP_TIMER_ADDR+0x00000138)  /* R/W  CP Timer Strobe #2 Comparator Register    0x0000 */
#define HWD_CTS7_REF_SEL       (HWD_CP_TIMER_ADDR+0x0000013c)  /* R/W  CTS7 timer reference select               0x0000 */
#define HWD_CTS7_EN            (HWD_CP_TIMER_ADDR+0x00000140)  /* R/W  CTS7 compare enable                       0x0000 */
#define HWD_CTS_CMP7           (HWD_CP_TIMER_ADDR+0x00000144)  /* R/W  CP Timer Strobe #3 Comparator Register    0x0000 */
#define HWD_CTS8_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000148)  /* R/W  CTS8 timer reference select               0x0000 */
#define HWD_CTS8_EN            (HWD_CP_TIMER_ADDR+0x0000014C)  /* R/W  CTS8 compare enable                       0x0000 */
#define HWD_CTS_CMP8           (HWD_CP_TIMER_ADDR+0x00000150)  /* R/W  CP Timer Strobe #0 Comparator Register    0x0000 */
#define HWD_CTS9_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000154)  /* R/W  CTS9 timer reference select               0x0000 */
#define HWD_CTS9_EN            (HWD_CP_TIMER_ADDR+0x00000158)  /* R/W  CTS9 compare enable                       0x0000 */
#define HWD_CTS_CMP9           (HWD_CP_TIMER_ADDR+0x0000015c)  /* R/W  CP Timer Strobe #1 Comparator Register    0x0000 */
#define HWD_CTS10_REF_SEL      (HWD_CP_TIMER_ADDR+0x00000160)  /* R/W  CTS10 timer reference select               0x0000 */
#define HWD_CTS10_EN           (HWD_CP_TIMER_ADDR+0x00000164)  /* R/W  CTS10 compare enable                       0x0000 */
#define HWD_CTS_CMP10          (HWD_CP_TIMER_ADDR+0x00000168)  /* R/W  CP Timer Strobe #2 Comparator Register    0x0000 */
#define HWD_CTS11_REF_SEL      (HWD_CP_TIMER_ADDR+0x0000016c)  /* R/W  CTS11 timer reference select               0x0000 */
#define HWD_CTS11_EN           (HWD_CP_TIMER_ADDR+0x00000170)  /* R/W  CTS11 compare enable                       0x0000 */
#define HWD_CTS_CMP11          (HWD_CP_TIMER_ADDR+0x00000174)  /* R/W  CP Timer Strobe #3 Comparator Register    0x0000 */

/* Additional CTS hardware registers valid only for CBP7x */
#define HWD_CTS0_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000100)  /* R/W  CTS0 timer reference select               0x0000 */
#define HWD_CTS0_EN            (HWD_CP_TIMER_ADDR+0x00000104)  /* R/W  CTS0 compare enable                       0x0000 */
#define HWD_CTS1_REF_SEL       (HWD_CP_TIMER_ADDR+0x00000108)  /* R/W  CTS1 timer reference select               0x0000 */
#define HWD_CTS1_EN            (HWD_CP_TIMER_ADDR+0x0000010c)  /* R/W  CTS1 compare enable                       0x0000 */
#define HWD_CNT80K_EN          (HWD_CP_TIMER_ADDR+0x00000110)  /* R/W  CP 80KHz Timer Counter Control Register   0x0000 */
#define HWD_CNT80K_TC          (HWD_CP_TIMER_ADDR+0x00000114)  /* R/W  CP 80KHz Timer Rollover Count Register    0x063f */

/* RTOS Timer */
#define HWD_RTOS_LOAD_VALUE    (HWD_CP_TIMER_ADDR+0x00000040)  /* R/W  CP RTOS timer load value reg              0x0    */
#define HWD_RTOS_EN            (HWD_CP_TIMER_ADDR+0x00000044)  /* R/W  CP RTOS timer enable reg                  0x0    */
#define HWD_RTOS_CNT_OUT       (HWD_CP_TIMER_ADDR+0x00000048)  /* R    CP RTOS timer counter reg                 0x0    */

/* Watchdog Timer */
#define HWD_WD_ABCD            (HWD_CP_TIMER_ADDR+0x00000080)  /* R/W  CP Watchdog timer update reg              0x0    */
#define HWD_WD_MAX_TIME        (HWD_CP_TIMER_ADDR+0x00000084)  /* R/W  CP Watchdog timer load value reg          0x0    */
#define HWD_WD_DISABLE         (HWD_CP_TIMER_ADDR+0x0000008c)  /* R/W  CP Watchdog timer disable reg             0x0    */

/*------------------------------**
** Clock Register Configuration **
**------------------------------*/
/* In BB7, clock control registers have a different base offset since they are replicated
** in all 4 BB7 FPGAs - only the mixed-signal FPGA base address will be utilized for
** read-modify write operations */
#define HWD_CP_CLOCK_ADDR      (HWD_CP_BASE_ADDR+0x00000000)

#define HWD_CLK_CTRL0          (HWD_CP_CLOCK_ADDR+0x00000000)  /* R/W  Clock control register 0                  0x7F9a */
#define HWD_CLK_CTRL1          (HWD_CP_CLOCK_ADDR+0x00000004)  /* R/W  Clock control register 1                  0x7e3f */
#define HWD_CLK_CTRL2          (HWD_CP_CLOCK_ADDR+0x00000008)  /* R/W  Clock control register 2                  0x0633 */
#define HWD_CLK_CTRL3          (HWD_CP_CLOCK_ADDR+0x0000000C)  /* R/W  Clock control register 3                  0x001e */
#define HWD_CLK_CTRL4          (HWD_CP_CLOCK_ADDR+0x00000010)  /* R/W  Clock control register 4                  0xfc00 */
#define HWD_CHIP_ID            (HWD_CP_CLOCK_ADDR+0x0000B01C)  /* R    Chip ID                                          */

#define HWD_JIT0_OFFSET        (HWD_CP_CLOCK_ADDR+0x0000001C)  /* R/W  UART0 Jitter clock offset                 0x0000 */
#define HWD_JIT0_CONST         (HWD_CP_CLOCK_ADDR+0x00000020)  /* R/W  UART0 Jitter clock constant               0x0000 */
#define HWD_JIT0_DIV           (HWD_CP_CLOCK_ADDR+0x00000024)  /* R/W  UART0 Jitter clock divider                0x0000 */
#define HWD_JIT1_OFFSET        (HWD_CP_CLOCK_ADDR+0x00000028)  /* R/W  UART1 Jitter clock offset                 0x0000 */
#define HWD_JIT1_CONST         (HWD_CP_CLOCK_ADDR+0x0000002C)  /* R/W  UART1 Jitter clock constant               0x0000 */
#define HWD_JIT1_DIV           (HWD_CP_CLOCK_ADDR+0x00000030)  /* R/W  UART1 Jitter clock divider                0x0000 */


#define HWD_CLK_CTRL5          (HWD_CP_CLOCK_ADDR+0x00000064)  /* R/W  Clock control register 5                  0x0342 */


#define HWD_CG_ARM_AMBA_CLKSEL (HWD_CP_CLOCK_ADDR+0x00000234)  /* R/W  AMBA clock frequency control              0xC120 */


#define HWD_CLK_CTRL6          (HWD_CP_CLOCK_ADDR+0x0000025C)  /* R/W  Clock control register 6                  0xFDFF */
#define HWD_CLK_CTRL7          (HWD_CP_CLOCK_ADDR+0x00000268)  /* R/W  Clock control register 7                  0xFFFF */
#define HWD_CLK_CTRL8          (HWD_CP_CLOCK_ADDR+0x0000026C)  /* R/W  Clock control register 7                  0x0001 */
#define HWD_SLP_BUS_STOP_CTL   (HWD_CP_CLOCK_ADDR+0x00000270)  /* R/W  Bus Stop Control for sleep mode           0x0000 */
#define HWD_CLK32_TC           (HWD_CP_CLOCK_ADDR+0x00000274)  /* R/W  Terminal count for 19.2 MHz clock 
                                                                       divider for internal 32 kHz clock         0x0249 */
#define HWD_CLK_CTRL9          (HWD_CP_CLOCK_ADDR+0x0000029C)  /* R/W  Clock control register 6                  0xFDFF */
#define HWD_CLK_CTRL10         (HWD_CP_CLOCK_ADDR+0x00000300)  /* R/W  Clock control register 7                  0xFFFF */
#if (SYS_ASIC >= SA_MT6755)
#define HWD_CLK_CTRL11         (HWD_CP_CLOCK_ADDR+0x00000308)  /* R/W  Clock control register 11                 0xXXXX */
#endif

/*--------------------------------**
** DSPM patch and match addresses **
**--------------------------------*/

#define HWD_PTCH_ADDR          (HWD_CP_BASE_ADDR+0x00000078)  /* R/W  DSPM patch enable  (15-0)   */

#define HWD_DM_PTCH_EN         (HWD_PTCH_ADDR+0x00000000)  /* R/W  DSPM patch enable  (15-0)   */
#define HWD_DM_PTCH_AD0        (HWD_PTCH_ADDR+0x00000080)  /* R/W  DSPM patch address 0        */
#define HWD_DM_PTCH_AD1        (HWD_PTCH_ADDR+0x00000084)  /* R/W  DSPM patch address 1        */
#define HWD_DM_PTCH_AD2        (HWD_PTCH_ADDR+0x00000088)  /* R/W  DSPM patch address 2        */
#define HWD_DM_PTCH_AD3        (HWD_PTCH_ADDR+0x0000008c)  /* R/W  DSPM patch address 3        */
#define HWD_DM_PTCH_AD4        (HWD_PTCH_ADDR+0x00000090)  /* R/W  DSPM patch address 4        */
#define HWD_DM_PTCH_AD5        (HWD_PTCH_ADDR+0x00000094)  /* R/W  DSPM patch address 5        */
#define HWD_DM_PTCH_AD6        (HWD_PTCH_ADDR+0x00000098)  /* R/W  DSPM patch address 6        */
#define HWD_DM_PTCH_AD7        (HWD_PTCH_ADDR+0x0000009c)  /* R/W  DSPM patch address 7        */
#define HWD_DM_PTCH_AD8        (HWD_PTCH_ADDR+0x000000a0)  /* R/W  DSPM patch address 8        */
#define HWD_DM_PTCH_AD9        (HWD_PTCH_ADDR+0x000000a4)  /* R/W  DSPM patch address 9        */
#define HWD_DM_PTCH_AD10       (HWD_PTCH_ADDR+0x000000a8)  /* R/W  DSPM patch address 10       */
#define HWD_DM_PTCH_AD11       (HWD_PTCH_ADDR+0x000000ac)  /* R/W  DSPM patch address 11       */
#define HWD_DM_PTCH_AD12       (HWD_PTCH_ADDR+0x000000b0)  /* R/W  DSPM patch address 12       */
#define HWD_DM_PTCH_AD13       (HWD_PTCH_ADDR+0x000000b4)  /* R/W  DSPM patch address 13       */
#define HWD_DM_PTCH_AD14       (HWD_PTCH_ADDR+0x000000b8)  /* R/W  DSPM patch address 14       */
#define HWD_DM_PTCH_AD15       (HWD_PTCH_ADDR+0x000000bc)  /* R/W  DSPM patch address 15       */

#define HWD_DM_PTCH_MTCH0      (HWD_PTCH_ADDR+0x000000e0)  /* R/W  DSPM patch match address 0  */
#define HWD_DM_PTCH_MTCH1      (HWD_PTCH_ADDR+0x000000e4)  /* R/W  DSPM patch match address 1  */
#define HWD_DM_PTCH_MTCH2      (HWD_PTCH_ADDR+0x000000e8)  /* R/W  DSPM patch match address 2  */
#define HWD_DM_PTCH_MTCH3      (HWD_PTCH_ADDR+0x000000ec)  /* R/W  DSPM patch match address 3  */
#define HWD_DM_PTCH_MTCH4      (HWD_PTCH_ADDR+0x000000f0)  /* R/W  DSPM patch match address 4  */
#define HWD_DM_PTCH_MTCH5      (HWD_PTCH_ADDR+0x000000f4)  /* R/W  DSPM patch match address 5  */
#define HWD_DM_PTCH_MTCH6      (HWD_PTCH_ADDR+0x000000f8)  /* R/W  DSPM patch match address 6  */
#define HWD_DM_PTCH_MTCH7      (HWD_PTCH_ADDR+0x000000fc)  /* R/W  DSPM patch match address 7  */
#define HWD_DM_PTCH_MTCH8      (HWD_PTCH_ADDR+0x00000100)  /* R/W  DSPM patch match address 8  */
#define HWD_DM_PTCH_MTCH9      (HWD_PTCH_ADDR+0x00000104)  /* R/W  DSPM patch match address 9  */
#define HWD_DM_PTCH_MTCH10     (HWD_PTCH_ADDR+0x00000108)  /* R/W  DSPM patch match address 10 */
#define HWD_DM_PTCH_MTCH11     (HWD_PTCH_ADDR+0x0000010c)  /* R/W  DSPM patch match address 11 */
#define HWD_DM_PTCH_MTCH12     (HWD_PTCH_ADDR+0x00000110)  /* R/W  DSPM patch match address 12 */
#define HWD_DM_PTCH_MTCH13     (HWD_PTCH_ADDR+0x00000114)  /* R/W  DSPM patch match address 13 */
#define HWD_DM_PTCH_MTCH14     (HWD_PTCH_ADDR+0x00000118)  /* R/W  DSPM patch match address 14 */
#define HWD_DM_PTCH_MTCH15     (HWD_PTCH_ADDR+0x0000011c)  /* R/W  DSPM patch match address 15 */

/*----------------**
** Tx/Rx Control  **
**----------------*/

/* CP RF ON/Tx ON Control Registers */
#define HWD_RXTX_BASE          (HWD_NO_BASE_ADDR)  /* R/W  Processor Control Bits for DSPM ctrl      0x0    */

#define HWD_TX_CP_DSPM_SEL     (HWD_RXTX_BASE+0x0b850000)  /* R/W  Processor Control Bits for DSPM ctrl      0x0    */
#define HWD_TX_CP_DSPV_SEL     (HWD_RXTX_BASE+0x0b8500bc)  /* R/W  Processor Control Bits for DSPV ctrl      0x0    */
#define HWD_TX_DAC_SETUP_HOLD  (HWD_RXTX_BASE+0x0b850004)  /* R/W  Internal TX Dac setup and hold            0x0    */
#define HWD_TX_DAC_CFG         (HWD_RXTX_BASE+0x0b850008)  /* R/W  Internal TX Dac configuration             0x0    */
#define HWD_TX_DAC_DIN         (HWD_RXTX_BASE+0x0b85000c)  /* R/W  Internal TX Dac override data             0x0    */
#define HWD_TX_ON0_SETUP_HOLD  (HWD_RXTX_BASE+0x0b850010)  /* R/W  TX ON1 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON0_CFG         (HWD_RXTX_BASE+0x0b850014)  /* R/W  TX ON1 configuration                      0x0    */
#define HWD_TX_ON0_DIN         (HWD_RXTX_BASE+0x0b850018)  /* R/W  TX ON1 override data                      0x0    */
#define HWD_TX_ON1_SETUP_HOLD  (HWD_RXTX_BASE+0x0b85001c)  /* R/W  TX ON2 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON1_CFG         (HWD_RXTX_BASE+0x0b850020)  /* R/W  TX ON2 configuration                      0x0    */
#define HWD_TX_ON1_DIN         (HWD_RXTX_BASE+0x0b850024)  /* R/W  TX ON2 override data                      0x0    */
#define HWD_TX_ON2_SETUP_HOLD  (HWD_RXTX_BASE+0x0b850028)  /* R/W  TX ON3 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON2_CFG         (HWD_RXTX_BASE+0x0b85002c)  /* R/W  TX ON3 configuration                      0x0    */
#define HWD_TX_ON2_DIN         (HWD_RXTX_BASE+0x0b850030)  /* R/W  TX ON3 override data                      0x0    */
#define HWD_TX_ON3_SETUP_HOLD  (HWD_RXTX_BASE+0x0b850034)  /* R/W  TX ON4 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON3_CFG         (HWD_RXTX_BASE+0x0b850038)  /* R/W  TX ON4 configuration                      0x0    */
#define HWD_TX_ON3_DIN         (HWD_RXTX_BASE+0x0b85003c)  /* R/W  TX ON4 override data                      0x0    */
#define HWD_TX_RFON0_DIN       (HWD_RXTX_BASE+0x0b850040)  /* R/W  Tx_RF_ON0 pending data                    0x0    */
#define HWD_TX_RFON0_CFG       (HWD_RXTX_BASE+0x0b850044)  /* R/W  Tx_RF_ON0 configuration                   0x0    */
#define HWD_TX_RFON1_DIN       (HWD_RXTX_BASE+0x0b850048)  /* R/W  Tx_RF_ON1 pending data                    0x0    */
#define HWD_TX_RFON1_CFG       (HWD_RXTX_BASE+0x0b85004c)  /* R/W  Tx_RF_ON1 configuration                   0x0    */
#define HWD_TX_RFON2_DIN       (HWD_RXTX_BASE+0x0b850050)  /* R/W  Tx_RF_ON2 pending data                    0x0    */
#define HWD_TX_RFON2_CFG       (HWD_RXTX_BASE+0x0b850054)  /* R/W  Tx_RF_ON2 configuration                   0x0    */
#define HWD_TX_RFON3_DIN       (HWD_RXTX_BASE+0x0b850058)  /* R/W  Tx_RF_ON3 pending data                    0x0    */
#define HWD_TX_RFON3_CFG       (HWD_RXTX_BASE+0x0b85005c)  /* R/W  Tx_RF_ON3 configuration                   0x0    */
#define HWD_TX_RFON4_DIN       (HWD_RXTX_BASE+0x0b850060)  /* R/W  Tx_RF_ON4 pending data                    0x0    */
#define HWD_TX_RFON4_CFG       (HWD_RXTX_BASE+0x0b850064)  /* R/W  Tx_RF_ON4 configuration                   0x0    */
#define HWD_TX_RFON5_DIN       (HWD_RXTX_BASE+0x0b850068)  /* R/W  Tx_RF_ON5 pending data                    0x0    */
#define HWD_TX_RFON5_CFG       (HWD_RXTX_BASE+0x0b85006C)  /* R/W  Tx_RF_ON5 configuration                   0x0    */
#define HWD_TX_RFON6_DIN       (HWD_RXTX_BASE+0x0b850070)  /* R/W  Tx_RF_ON6 pending data                    0x0    */
#define HWD_TX_RFON6_CFG       (HWD_RXTX_BASE+0x0b850074)  /* R/W  Tx_RF_ON6 configuration                   0x0    */
#define HWD_TX_RFON7_DIN       (HWD_RXTX_BASE+0x0b850078)  /* R/W  Tx_RF_ON7 pending data                    0x0    */
#define HWD_TX_RFON7_CFG       (HWD_RXTX_BASE+0x0b85007C)  /* R/W  Tx_RF_ON7 configuration                   0x0    */
#define HWD_TX_RFON8_DIN       (HWD_RXTX_BASE+0x0b850148)  /* R/W  Tx_RF_ON8 pending data                    0x0    */
#define HWD_TX_RFON8_CFG       (HWD_RXTX_BASE+0x0b85014C)  /* R/W  Tx_RF_ON8 configuration                   0x0    */
#define HWD_TX_RFON9_DIN       (HWD_RXTX_BASE+0x0b850150)  /* R/W  Tx_RF_ON9pending data                    0x0    */
#define HWD_TX_RFON9_CFG       (HWD_RXTX_BASE+0x0b850154)  /* R/W  Tx_RF_ON9configuration                   0x0    */
#define HWD_TX_ON_GPO          (HWD_RXTX_BASE+0x0b850080)  /* R/W  Tx_On GPO control and data                0x0    */
#define HWD_TX_ON4_SETUP_HOLD  (HWD_RXTX_BASE+0x0b8500A0)  /* R/W  TX ON4 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON4_CFG         (HWD_RXTX_BASE+0x0b8500A4)  /* R/W  TX ON4 configuration                      0x0    */
#define HWD_TX_ON4_DIN         (HWD_RXTX_BASE+0x0b8500A8)  /* R/W  TX ON4 override data                      0x0    */
#define HWD_TX_ON_DLY_DIN1_CFG (HWD_RXTX_BASE+0x0b8500C4)  /* R/W  TX ON first half slot delay load data in do delay load mode                0x0    */
#define HWD_TX_ON_DLY_DIN2_CFG (HWD_RXTX_BASE+0x0b8500C8)  /* R/W  TX ON second half slot delay load data in do delay load mode            0x0    */
#define HWD_TX_ON5_SETUP_HOLD  (HWD_RXTX_BASE+0x0b8500D4)  /* R/W  TX ON5 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON5_CFG         (HWD_RXTX_BASE+0x0b8500D8)  /* R/W  TX ON5 configuration                      0x0    */
#define HWD_TX_ON5_DIN         (HWD_RXTX_BASE+0x0b8500DC)  /* R/W  TX ON5 override data                      0x0    */
#define HWD_TX_ON6_SETUP_HOLD  (HWD_RXTX_BASE+0x0b8500E0)  /* W  TX ON6 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON6_CFG         (HWD_RXTX_BASE+0x0b8500E4)  /* W  TX ON6 configuration                      0x0    */
#define HWD_TX_ON6_DIN         (HWD_RXTX_BASE+0x0b8500E8)  /* W  TX ON6 override data                      0x0    */
#define HWD_TX_ON7_SETUP_HOLD  (HWD_RXTX_BASE+0x0b8500FC)  /* W  TX ON7 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_ON7_CFG         (HWD_RXTX_BASE+0x0b850100)  /* W  TX ON7 configuration                      0x0    */
#define HWD_TX_ON7_DIN         (HWD_RXTX_BASE+0x0b850104)  /* W  TX ON7 override data                      0x0    */
#define HWD_TX_DAC2_SETUP_HOLD (HWD_RXTX_BASE+0x0b850108)  /* W  TX ON8 setup and hold relative to Tx PCG  0x0    */
#define HWD_TX_DAC2_CFG        (HWD_RXTX_BASE+0x0b85010C)  /* W  TX ON8 configuration                      0x0    */
#define HWD_TX_DAC2_DIN        (HWD_RXTX_BASE+0x0b850110)  /* W  TX ON8 override data                      0x0    */
#define HWD_TX_CP_DSPM_78_SEL  (HWD_RXTX_BASE+0x0b850130)  /* R/W  Processor Control Bits for DSPM ctrl      0x0    */
#define HWD_TX_ON_DLY_1X_DO    (HWD_RXTX_BASE+0x0b850114) /* R/W TX_ON [0:9] 1x mode or DO mode */

#if (SYS_ASIC == SA_MT6735)
/* BPI check register */
#define HWD_AP_LTE_OCCUPY_ANT  (HWD_AP_INFRA_BASE+0x340)
#define HWD_AP_C2K_OCCUPY_ANT  (HWD_AP_INFRA_BASE+0x344)

/* CP Tx Modem Registers */
#define HWD_TX_TEST_VAL        (HWD_RXTX_BASE+0x0b850084)  /* R/W  Level of square wave inserted to Tx
                                                                 filter during test mode                   0x0    */
#define HWD_TX_TEST_CTRL       (HWD_RXTX_BASE+0x0b850088)  /* R/W  Test Control, synchronized to the PCG     0x0    */
#define HWD_TX_I_OFFSET_INT    (HWD_RXTX_BASE+0x0b85008C)  /* R/W  Tx DC I offset prior to int MS DAC (cal)  0x0    */
#define HWD_TX_Q_OFFSET_INT    (HWD_RXTX_BASE+0x0b850090)  /* R/W  Tx DC Q offset prior to int MS DAC (cal)  0x0    */

#define HWD_TX_GAIN_COMP1_1X   (HWD_RXTX_BASE+0x0b8500AC)  /* R/W  1X TX gain compensation value                0x0    */
#define HWD_TX_GAIN_COMP2_1X   (HWD_RXTX_BASE+0x0b8500B0)  /* R/W  1X TX gain compensation value                0x0    */

#define HWD_TX_AP_IIR2_A1_CP_1X (HWD_RXTX_BASE+0x0b850118)  /* R/W  1X TX equalizer A1 coefficient value                0x0    */
#define HWD_TX_AP_IIR2_A2_CP_1X (HWD_RXTX_BASE+0x0b85011C)  /* R/W  1X TX equalizer A2 coefficient value                0x0    */


#define HWD_TX_EQUAL_EN_1X     (HWD_RXTX_BASE+0x0b850128)  /* R/W  1X TX equalizer enable                0x0    */

#define HWD_TX_DSM_SEL_1X      (HWD_RXTX_BASE+0x0B85012C)  /* R/W DSM enable bit selection bit TX                0x0    */
#define HWD_TX_MULT_GAIN_1X    (HWD_RXTX_BASE+0x0B850134)  /* R/W Mult Gain value for TX Gain block 1X TX        0x0    */
#define HWD_TX_SHIFT_GAIN_1X   (HWD_RXTX_BASE+0x0B850138)  /* R/W Shift gain value for TX Gain bock  1X TX       0x0    */
#define HWD_TX_GAIN_COMP1_1X   (HWD_RXTX_BASE+0x0b8500AC)  /* R/W  1X TX gain compensation value                0x0    */
#define HWD_TX_GAIN_COMP2_1X   (HWD_RXTX_BASE+0x0b8500B0)  /* R/W  1X TX gain compensation value                0x0    */
#define HWD_TX_EQUAL_EN_1X     (HWD_RXTX_BASE+0x0b850128)  /* R/W  1X TX equalizer enable                0x0    */
#define HWD_TX_FIR_H_1X        (HWD_RXTX_BASE+0x0B85013C)  /* R/W FIR H and enable value for DSM Droop 1X TX     0x0    */
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_TX_TEST_VAL        (HWD_RXTX_BASE+0x0b850168)  /* R/W  Level of square wave inserted to Tx
                                                                 filter during test mode                   0x0    */
#define HWD_TX_TEST_CTRL       (HWD_RXTX_BASE+0x0b85016C)  /* R/W  Test Control, synchronized to the PCG     0x0    */
#define HWD_TX_I_OFFSET_INT    (HWD_RXTX_BASE+0x0b850170)  /* R/W  Tx DC I offset prior to int MS DAC (cal)  0x0    */
#define HWD_TX_Q_OFFSET_INT    (HWD_RXTX_BASE+0x0b850174)  /* R/W  Tx DC Q offset prior to int MS DAC (cal)  0x0    */


#define HWD_TX_AP_IIR2_A1_CP_1X (HWD_RXTX_BASE+0x0b850180)  /* R/W  1X TX equalizer A1 coefficient value                0x0    */
#define HWD_TX_AP_IIR2_A2_CP_1X (HWD_RXTX_BASE+0x0b850184)  /* R/W  1X TX equalizer A2 coefficient value                0x0    */



#define HWD_TX_DSM_SEL_1X      (HWD_RXTX_BASE+0x0B85018C)  /* R/W DSM enable bit selection bit TX                0x0    */
#define HWD_TX_MULT_GAIN_1X    (HWD_RXTX_BASE+0x0B850190)  /* R/W Mult Gain value for TX Gain block 1X TX        0x0    */
#define HWD_TX_SHIFT_GAIN_1X   (HWD_RXTX_BASE+0x0B850194)  /* R/W Shift gain value for TX Gain bock  1X TX       0x0    */

#define HWD_JADE_TX_GAIN_COMP1_1X   (HWD_RXTX_BASE+0x0b850178)  /* R/W  1X TX gain compensation value                0x0    */
#define HWD_JADE_TX_GAIN_COMP2_1X   (HWD_RXTX_BASE+0x0b85017C)  /* R/W  1X TX gain compensation value                0x0    */
#define HWD_JADE_TX_EQUAL_EN_1X     (HWD_RXTX_BASE+0x0b850188)  /* R/W  1X TX equalizer enable                0x0    */
#define HWD_JADE_TX_FIR_H_1X        (HWD_RXTX_BASE+0x0B850198)  /* R/W FIR H and enable value for DSM Droop 1X TX     0x0    */
#endif

/*---------------**                                        
** CP Interface  **                                        
**---------------*/                                        
#define HWD_IRAM_MASK          (HWD_CP_BASE_ADDR+0x0000B010)  /* R/W  CP Mask to generate IRAM write interrupt  0x00 */


/*---------------**
** System Timing **
**---------------*/

#define HWD_ST_BASE_ADDR        0x0

#define HWD_ST_CPINT_FR        (HWD_ST_BASE_ADDR+0x0b860000)  /* R/W  Frame source type (20 or 26 ms)           0x?    */
#define HWD_ST_CPINT_CLR       (HWD_ST_BASE_ADDR+0x0b860004)  /* R/W  Clear status register                     0x?    */
#define HWD_ST_CPINT_MASK      (HWD_ST_BASE_ADDR+0x0b860008)  /* R/W  Interrupt Mask Register                   0x?    */
#define HWD_ST_CPINT_SRC       (HWD_ST_BASE_ADDR+0x0b86000C)  /* R    Interrupt Source Register                 0x?    */
#define HWD_ST_CPINT_CNT       (HWD_ST_BASE_ADDR+0x0b860010)  /* R    Subframe count */
#define HWD_ST_CNT_LOCK        (HWD_ST_BASE_ADDR+0x0b860014)  /* W    Lock System Time Counter Value */
#define HWD_ST_CNT             (HWD_ST_BASE_ADDR+0x0b860018)  /* R    System Time Counter 20 bits */
#define HWD_ST_LC_MODE         (HWD_ST_BASE_ADDR+0x0b860020)  /* R    Long Code Update Mode: 0: 20ms, 1: 5ms */
#define HWD_ST_CNT_INIT_0      (HWD_ST_BASE_ADDR+0x0b860024)  /* R/W  Low order 16 bit value for system counter */
#define HWD_ST_CNT_INIT_1      (HWD_ST_BASE_ADDR+0x0b860028)  /* R/W  High order 4 bit value for system counter */
#if (SYS_ASIC >= SA_MT6755)
#define HWD_TIMING_SYNC_MODE_1X    (HWD_ST_BASE_ADDR+0x0B86002C) /* 1x Timing sync trigger mode select */
#define HWD_TIMING_SYNC_CMP_0_1X   (HWD_ST_BASE_ADDR+0x0B860030) /* The compare value of 1x timer controlled trigger mode (LSB 16 bits) */
#define HWD_TIMING_SYNC_CMP_1_1X   (HWD_ST_BASE_ADDR+0x0B860034) /* The compare value of 1x timer controlled trigger mode (MSB 4 bits) */
#define HWD_TIMING_SYNC_TRIG_1X    (HWD_ST_BASE_ADDR+0x0B860038) /* 1x Timing sync start */
#define HWD_TIMER_CNT_1X           (HWD_ST_BASE_ADDR+0x0B86003C) /* Latched 1x system timer counter in timer controlled trigger mode */
#define HWD_TIMER_INT0_CMP_0_1X    (HWD_ST_BASE_ADDR+0x0B860040) /* The LSB 16 bits compare value of 1x timer int0 */
#define HWD_TIMER_INT0_CMP_1_1X    (HWD_ST_BASE_ADDR+0x0B860044) /* The MSB 4 bits compare value of 1x timer int0 */
#define HWD_TIMER_INT1_CMP_0_1X    (HWD_ST_BASE_ADDR+0x0B860048) /* The LSB 16 bits compare value of 1x timer int1 */
#define HWD_TIMER_INT1_CMP_1_1X    (HWD_ST_BASE_ADDR+0x0B86004C) /* The MSB 4 bits compare value of 1x timer int1 */
#define HWD_TIMER_INT2_CMP_0_1X    (HWD_ST_BASE_ADDR+0x0B860050) /* The LSB 16 bits compare value of 1x timer int2 */
#define HWD_TIMER_INT2_CMP_1_1X    (HWD_ST_BASE_ADDR+0x0B860054) /* The MSB 4 bits compare value of 1x timer int2 */
#define HWD_TIMER_INT0_TRIG_1X     (HWD_ST_BASE_ADDR+0x0B860058) /* 1x int0 trigger */
#define HWD_TIMER_INT1_TRIG_1X     (HWD_ST_BASE_ADDR+0x0B86005C) /* 1x int1 trigger */
#define HWD_TIMER_INT2_TRIG_1X     (HWD_ST_BASE_ADDR+0x0B860060) /* 1x int2 trigger */
#define HWD_TIMER_INT_RST_1X       (HWD_ST_BASE_ADDR+0x0B860064) /* SW reset */
#endif

/*---------------**
** EVDO System Time **
**---------------*/

#define HWD_EV_SLOT_CNT_INT_MASK        0x0001 /* EVDO Slot counter mask */
#define HWD_EV_FRAME_CNT_INT_MASK       0x8000 /* EVDO Frame counter mask */
#define HWD_ST_SYMB_NUM_MASK            0x000007FF  
#define HWD_ST_SYMB_NUM_MASK_26MS       0x000001FF  
#define HWD_ST_EV_CPINT_ALL             0xFFFF  


#define HWD_STDO_BASE_ADDR       (HWD_NO_BASE_ADDR+0x0b750000)

#define HWD_STDO_PNI_INIT        (HWD_STDO_BASE_ADDR+0x0000)   /* [14:0] Evdo I PN Generator Initialization */
#define HWD_STDO_PNQ_INIT        (HWD_STDO_BASE_ADDR+0x0004)   /* [14:0] Evdo Q PN Generator Initialization */
#define HWD_STDO_CNT_INIT_0      (HWD_STDO_BASE_ADDR+0x0008)   /* [15:0] Evdo LSB System Counter Initialization */
#define HWD_STDO_CNT_INIT_1      (HWD_STDO_BASE_ADDR+0x000C)   /* [4:0]  Evdo MSB System Counter Initialization */
#define HWD_STDO_ADV_RET         (HWD_STDO_BASE_ADDR+0x0010)   /* [10:0] Evdo System Counter Time Track Advance/Retard */
#define HWD_STDO_SYNC_FR_OFFSET  (HWD_STDO_BASE_ADDR+0x0014)   /* [3:0]  Evdo System Counter Frame Offset */

#define HWD_STDO_SYNC_TIME_0     (HWD_STDO_BASE_ADDR+0x0018)   /* [15:0] Evdo System Time Counter Terminal Count LSBs */
#define HWD_STDO_SYNC_TIME_1     (HWD_STDO_BASE_ADDR+0x001C)   /* [1:0]  Evdo System Time Counter Terminal Count MSBs */
#define HWD_STDO_SYNC_TIME_STB   (HWD_STDO_BASE_ADDR+0x0020)   /* Evdo System Time Counter Active Strobe */
#define HWD_STDO_FN_ALLOC_CTL    (HWD_STDO_BASE_ADDR+0x0024)   /* [9:0]  Evdo Finger & Searcher Allocation control */

#define HWD_STDO_LC_INIT_0       (HWD_STDO_BASE_ADDR+0x0028)   /* [15:0] Evdo Long Code Initialization value (bits 15:0) */
#define HWD_STDO_LC_INIT_1       (HWD_STDO_BASE_ADDR+0x002C)   /* [15:0] Evdo Long Code Initialization value (bits 31:16) */
#define HWD_STDO_LC_INIT_2       (HWD_STDO_BASE_ADDR+0x0030)   /* [9:0]  Evdo Long Code Initialization value (bits 41:32) */
#define HWD_STDO_LC_INIT_STB     (HWD_STDO_BASE_ADDR+0x0034)   /* Evdo Long Code Initialization load strobe */

#define HWD_STDO_TX_MIL_0        (HWD_STDO_BASE_ADDR+0x0038)   /* [15:0] Transmit MIL [15:0] */
#define HWD_STDO_TX_MIL_1        (HWD_STDO_BASE_ADDR+0x003C)   /* [15:0] Transmit MIL [31:16] */

#define HWD_STDO_TX2RX_OFFSET    (HWD_STDO_BASE_ADDR+0x0040)   /* [9:0] Transmit MIH Bits [9:0] */
#define HWD_STDO_TX_MIH_0        (HWD_STDO_BASE_ADDR+0x0044)   /* [9:0] Transmit MIH Bits [9:0] */
#define HWD_STDO_ACK_START_TIME  (HWD_STDO_BASE_ADDR+0x0048)   /* [9:0] Offset from system time slot boundary to ACKer start */
#define HWD_STDO_RXSLOT_OFFSET   (HWD_STDO_BASE_ADDR+0x004C)   /* [9:0] Offset of system time input to RX controller */

#define HWD_STDO_INTS            (HWD_STDO_BASE_ADDR+0x0050)   /* [10:0] Programmable offset value for Outer-Loop Slot ISR generation */
#define HWD_STDO_INTH            (HWD_STDO_BASE_ADDR+0x0054)   /* [10:0] Programmable offset value for Outer-Loop Half-Slot ISR generation */

/* Interrupts */
#define HWD_STDO_CPINT_FR        (HWD_STDO_BASE_ADDR+0x0060)   /* EV SubFrame Interrupt Type  (20 or 26 ms) 0:20ms frame, 1:26.6ms */
#define HWD_STDO_CPINT_MASK      (HWD_STDO_BASE_ADDR+0x0064)   /* EV Interrupt Mask Register */
#define HWD_STDO_CPINT_CLR       (HWD_STDO_BASE_ADDR+0x0068)   /* EV Interrupt Clear */
#define HWD_STDO_CPINT_SRC       (HWD_STDO_BASE_ADDR+0x006C)   /* EV Interrupt Source Readback */
#define HWD_STDO_CP_ISR          (HWD_STDO_BASE_ADDR+0x0070)   /* EV Interrupt ??? */

#define HWD_STDO_SUBFR_STATUS    (HWD_STDO_BASE_ADDR+0x0074)   /* [15:0] Evdo Subframe Status */
#define HWD_STDO_SUB_CNT         (HWD_STDO_BASE_ADDR+0x0078)   /* [15:0] Evdo Subframe counter */

#define HWD_STDO_PN_GEN_START    (HWD_STDO_BASE_ADDR+0x007C)   /* Evdo System Time PN Generation start strobe */

#define HWD_STDO_TS0             (HWD_STDO_BASE_ADDR+0x0080)   /* Evdo Timing Strobe 0 */
#define HWD_STDO_TS1             (HWD_STDO_BASE_ADDR+0x0084)   /* Evdo Timing Strobe 1 */
#define HWD_STDO_TS2             (HWD_STDO_BASE_ADDR+0x0088)   /* Evdo Timing Strobe 2 */
#define HWD_STDO_TS3             (HWD_STDO_BASE_ADDR+0x008C)   /* Evdo Timing Strobe 3 */

#define HWD_STDO_PRG_SYM_TMR0    (HWD_STDO_BASE_ADDR+0x0090)   /* [9:0] Evdo Periodic/Single Shot Symbol 
                                                                * Status for Programmable Symbol Count 0
                                                                */
#define HWD_STDO_PRG_SYM_TMR1    (HWD_STDO_BASE_ADDR+0x0094)   /* [9:0] Evdo Periodic/Single Shot Symbol 
                                                                * Status for Programmable Symbol Count 1
                                                                */
#define HWD_STDO_PRG_SYM_TMR2    (HWD_STDO_BASE_ADDR+0x0098)   /* [9:0] Evdo Periodic/Single Shot Symbol 
                                                                * Status for Programmable Symbol Count 2
                                                                */
#define HWD_STDO_PRG_SYM_TMR3    (HWD_STDO_BASE_ADDR+0x009C)   /* [9:0] Evdo Periodic/Single Shot Symbol 
                                                                * Status for Programmable Symbol Count 3
                                                                */

#define HWD_STDO_MDM_STATUS      (HWD_STDO_BASE_ADDR+0x00A0)   /* [15:0] Evdo System Time Modem Status */
#define HWD_STDO_TMG_STATUS      (HWD_STDO_BASE_ADDR+0x00A4)   /* [15:0] Evdo System Time Timing Status */
#define HWD_STDO_TDM_SYM_STATUS  (HWD_STDO_BASE_ADDR+0x00A8)   /* [15:0] Evdo System Time Timed Symbol Status */

#define HWD_STDO_PNI_STATE       (HWD_STDO_BASE_ADDR+0x00AC)   /* [15:0] Evdo Searcher PNI State */
#define HWD_STDO_PNQ_STATE       (HWD_STDO_BASE_ADDR+0x00B0)   /* [15:0] Evdo Searcher PNQ State */

#define HWD_STDO_SYS_LOCK        (HWD_STDO_BASE_ADDR+0x00B4)   /* Evdo System Time Count Lock */
#define HWD_STDO_SYS_CNT         (HWD_STDO_BASE_ADDR+0x00B8)   /* [19:0] Evdo System Time Count Value */

#define HWD_STDO_LD_OFFSET_0     (HWD_STDO_BASE_ADDR+0x00C0)   /* [14:0] Finger Sector0 Load Offset value */
#define HWD_STDO_LD_OFFSET_1     (HWD_STDO_BASE_ADDR+0x00C4)   /* [14:0] Finger Sector1 Load Offset value */
#define HWD_STDO_LD_OFFSET_2     (HWD_STDO_BASE_ADDR+0x00C8)   /* [14:0] Finger Sector2 Load Offset value */
#define HWD_STDO_MAC_LD_OFFSET_0 (HWD_STDO_BASE_ADDR+0x00CC)   /* [14:0] MAC Sector0 Load Offset value */
#define HWD_STDO_MAC_LD_OFFSET_1 (HWD_STDO_BASE_ADDR+0x00D0)   /* [14:0] MAC Sector1 Load Offset value */
#define HWD_STDO_MAC_LD_OFFSET_2 (HWD_STDO_BASE_ADDR+0x00D4)   /* [14:0] MAC Sector2 Load Offset value */

#define HWD_STDO_FN0_ALLOC       (HWD_STDO_BASE_ADDR+0x00D8)   /* [5:0] Finger Sector0 Allocation */
#define HWD_STDO_FN1_ALLOC       (HWD_STDO_BASE_ADDR+0x00DC)   /* [5:0] Finger Sector1 Allocation */
#define HWD_STDO_FN2_ALLOC       (HWD_STDO_BASE_ADDR+0x00E0)   /* [5:0] Finger Sector2 Allocation */
#define HWD_STDO_MAC0_ALLOC      (HWD_STDO_BASE_ADDR+0x00E4)   /* [5:0] MAC Sector 0 Allocation */
#define HWD_STDO_MAC1_ALLOC      (HWD_STDO_BASE_ADDR+0x00E8)   /* [5:0] MAC Sector 1 Allocation */
#define HWD_STDO_MAC2_ALLOC      (HWD_STDO_BASE_ADDR+0x00EC)   /* [5:0] MAC Sector 1 Allocation */

#define HWD_STDO_MAC_LD_OFFSET_3 (HWD_STDO_BASE_ADDR+0x0110)   /* [14:0] MAC Sector0 Load Offset value */
#define HWD_STDO_MAC_LD_OFFSET_4 (HWD_STDO_BASE_ADDR+0x0114)   /* [14:0] MAC Sector1 Load Offset value */
#define HWD_STDO_MAC_LD_OFFSET_5 (HWD_STDO_BASE_ADDR+0x0118)   /* [14:0] MAC Sector2 Load Offset value */

#define HWD_STDO_MAC3_ALLOC      (HWD_STDO_BASE_ADDR+0x011C)   /* [5:0] MAC Sector 0 Allocation */
#define HWD_STDO_MAC4_ALLOC      (HWD_STDO_BASE_ADDR+0x0120)   /* [5:0] MAC Sector 1 Allocation */
#define HWD_STDO_MAC5_ALLOC      (HWD_STDO_BASE_ADDR+0x0124)   /* [5:0] MAC Sector 1 Allocation */


#define HWD_STDO_PDM_SU          (HWD_STDO_BASE_ADDR+0x0104) /* R/W  delay loading window from slot boundary */
#define HWD_STDO_TX_EARLY_STB    (HWD_STDO_BASE_ADDR+0x0108) /* R/W tx early stb time - default 512 chips */
#define HWD_STDO_TX_KS_CALC_STB  (HWD_STDO_BASE_ADDR+0x010C) /* R/W  ks calc stb time - default 400 chips */

#if (SYS_ASIC >= SA_MT6755)
#define HWD_TIMING_SYNC_MODE_DO  (HWD_STDO_BASE_ADDR+0x0128)  /*DO Timing sync trigger mode select */
#define HWD_TIMING_SYNC_CMP_0_DO (HWD_STDO_BASE_ADDR+0x012C)  /*The compare value of DO timer controlled trigger mode (LSB 16 bits) */
#define HWD_TIMING_SYNC_CMP_1_DO (HWD_STDO_BASE_ADDR+0x0130)  /*The compare value of DO timer controlled trigger mode (MSB 4 bits) */
#define HWD_TIMING_SYNC_TRIG_DO  (HWD_STDO_BASE_ADDR+0x0134)  /*DO Timing sync start */
#define HWD_TIMER_CNT_DO         (HWD_STDO_BASE_ADDR+0x0138)  /*Latched DO system timer counter in timer controlled trigger mode */
#define HWD_TIMER_INT0_CMP_0_DO  (HWD_STDO_BASE_ADDR+0x013C)  /*The LSB 16 bits compare value of DO timer int0 */
#define HWD_TIMER_INT0_CMP_1_DO  (HWD_STDO_BASE_ADDR+0x0140)  /*The MSB 4 bits compare value of DO timer int0 */
#define HWD_TIMER_INT1_CMP_0_DO  (HWD_STDO_BASE_ADDR+0x0144)  /*The LSB 16 bits compare value of DO timer int1 */
#define HWD_TIMER_INT1_CMP_1_DO  (HWD_STDO_BASE_ADDR+0x0148)  /*The MSB 4 bits compare value of DO timer int1 */
#define HWD_TIMER_INT2_CMP_0_DO  (HWD_STDO_BASE_ADDR+0x014C)  /*The LSB 16 bits compare value of DO timer int2 */
#define HWD_TIMER_INT2_CMP_1_DO  (HWD_STDO_BASE_ADDR+0x0150)  /*The MSB 4 bits compare value of DO timer int2 */
#define HWD_TIMER_INT0_TRIG_DO   (HWD_STDO_BASE_ADDR+0x0154)  /*DO int0 trigger */
#define HWD_TIMER_INT1_TRIG_DO   (HWD_STDO_BASE_ADDR+0x0158)  /*DO int1 trigger */
#define HWD_TIMER_INT2_TRIG_DO   (HWD_STDO_BASE_ADDR+0x015C)  /*DO int2 trigger */
#define HWD_TIMER_INT_RST_DO     (HWD_STDO_BASE_ADDR+0x0160)  /*SW reset */
#endif

/*--------------------------**
** CP Sleep and Calibration **
**--------------------------*/
#define HWD_CS_BASE_ADDR                   (HWD_CP_BASE_ADDR+0x00002000)
                                           
#define HWD_CS_SLP_CTRL                    (HWD_CS_BASE_ADDR+0x0000)  /* R/W  Sleep control register                      0x0000 */
#define HWD_CS_RESYNC_TIME_1X_LO           (HWD_CS_BASE_ADDR+0x0004)  /* R/W  LSB of slp counter compare value for resync 0x0000 */
#define HWD_CS_RESYNC_TIME_1X_HI           (HWD_CS_BASE_ADDR+0x0008)  /* R/W  MSB of slp counter compare value for resync 0x0000 */
#define HWD_CS_SLP_TIME_1X_LO              (HWD_CS_BASE_ADDR+0x000C)  /* R/W  LSB of slp counter compare value for wakeup 0xffff */
#define HWD_CS_SLP_TIME_1X_HI              (HWD_CS_BASE_ADDR+0x0010)  /* R/W  MSB of slp counter compare value for wakeup 0xffff */
#define HWD_CS_GPINT_CTRL                  (HWD_CS_BASE_ADDR+0x0014)  /* R/W  Ctrl GPINT polarity                         0x0000 */
#define HWD_CS_EXT_WAKE_EN_1X              (HWD_CS_BASE_ADDR+0x0018)  /* R/W  Ctrl for wakeup from deep-sleep enable      0x0000 */
#define HWD_CS_PWR_UP_WAIT                 (HWD_CS_BASE_ADDR+0x001C)  /* R/W  Count for OSC/PLL settling time on power up 0x0000 */
#define HWD_CS_CAL_SYSTIME_1X_LO           (HWD_CS_BASE_ADDR+0x0020)  /* R/W  16bit LSB of systime on 32K edge            0x0000 */
#define HWD_CS_CAL_SYSTIME_1X_HI           (HWD_CS_BASE_ADDR+0x0024)  /* R/W  4bit MSB of systime on 32K edge             0x0000 */
#define HWD_CS_CAL_SLPCNT_1X_LO            (HWD_CS_BASE_ADDR+0x0028)  /* R/W  16bit LSB of 32K count on 32K edge          0x0000 */
#define HWD_CS_CAL_SLPCNT_1X_HI            (HWD_CS_BASE_ADDR+0x002C)  /* R/W  4bit MSB of 32K count on 32K edge           0x0000 */
#define HWD_CS_RESYNC_CTL                  (HWD_CS_BASE_ADDR+0x0038)  /* R/W  */
#define HWD_CS_EXT_WAKE_EN2_1X             (HWD_CS_BASE_ADDR+0x003C)  /* R/W  */
#if (SYS_ASIC == SA_MT6735)
#define HWD_CS_PSO_LTCH_EN                 (HWD_CS_BASE_ADDR+0x0040)  /* R/W  */
#endif
#define HWD_CS_RESYNC_TIME_DO_LO           (HWD_CS_BASE_ADDR+0x0044)  /* R/W  */
#define HWD_CS_RESYNC_TIME_DO_HI           (HWD_CS_BASE_ADDR+0x0048)  /* R/W  */
#define HWD_CS_SLP_TIME_DO_LO              (HWD_CS_BASE_ADDR+0x004C)  /* R/W  */
#define HWD_CS_SLP_TIME_DO_HI              (HWD_CS_BASE_ADDR+0x0050)  /* R/W  */
#define HWD_CS_EXT_WAKE_EN_DO              (HWD_CS_BASE_ADDR+0x0058)  /* R/W  */
#define HWD_CS_EXT_WAKE_EN2_DO             (HWD_CS_BASE_ADDR+0x005C)  /* R/W  */
#define HWD_CS_CAL_SYSTIME_DO_LO           (HWD_CS_BASE_ADDR+0x0060)  /* R/W  */
#define HWD_CS_CAL_SYSTIME_DO_HI           (HWD_CS_BASE_ADDR+0x0064)  /* R/W  */
#define HWD_CS_CAL_SLPCNT_DO_LO            (HWD_CS_BASE_ADDR+0x0068)  /* R/W  */
#define HWD_CS_CAL_SLPCNT_DO_HI            (HWD_CS_BASE_ADDR+0x006C)  /* R/W  */
#define HWD_CS_SLP_STATUS                  (HWD_CS_BASE_ADDR+0x0078)  /* R/W  */
#define HWD_CS_GPINT_CTRL2                 (HWD_CS_BASE_ADDR+0x007C)  /* R/W  */

#define HWD_CS_RTOS_SLP_TIME_1X_LO         (HWD_CS_BASE_ADDR+0x0080)  /* R/W */
#define HWD_CS_RTOS_SLP_TIME_1X_HI         (HWD_CS_BASE_ADDR+0x0084)  /* R/W */
#define HWD_CS_RTOS_SLP_TIME_DO_LO         (HWD_CS_BASE_ADDR+0x0088)  /* R/W */
#define HWD_CS_RTOS_SLP_TIME_DO_HI         (HWD_CS_BASE_ADDR+0x008C)  /* R/W */
#define HWD_UIM_HOT_PLUG_EINT_STA          (HWD_CS_BASE_ADDR+0x0090)  /* RO */
#define HWD_UIM_HOT_PLUG_EINT_INTACK0      (HWD_CS_BASE_ADDR+0x0094)  /* WO */
#define HWD_UIM_HOT_PLUG_EINT_EEVT         (HWD_CS_BASE_ADDR+0x009C)  /* RO */
#define HWD_UIM_HOT_PLUG_EINT_MASK         (HWD_CS_BASE_ADDR+0x00A0)  /* RO */
#define HWD_UIM_HOT_PLUG_EINT_MASK_SET     (HWD_CS_BASE_ADDR+0x00A4)  /* W1S */
#define HWD_UIM_HOT_PLUG_EINT_MASK_CLR     (HWD_CS_BASE_ADDR+0x00A8)  /* W1C */
#define HWD_UIM_HOT_PLUG_EINT_SENS         (HWD_CS_BASE_ADDR+0x00AC)  /* RO */
#define HWD_UIM_HOT_PLUG_EINT_SENS_SET     (HWD_CS_BASE_ADDR+0x00B0)  /* W1S */
#define HWD_UIM_HOT_PLUG_EINT_SENS_CLR     (HWD_CS_BASE_ADDR+0x00B4)  /* W1C */
#define HWD_UIM_HOT_PLUG_EINT_SOFT         (HWD_CS_BASE_ADDR+0x00B8)  /* RO */
#define HWD_UIM_HOT_PLUG_EINT_SOFT_SET     (HWD_CS_BASE_ADDR+0x00BC)  /* W1S */
#define HWD_UIM_HOT_PLUG_EINT_SOFT_CLR     (HWD_CS_BASE_ADDR+0x00C0)  /* W1C */
#define HWD_UIM_HOT_PLUG_EINT_EN           (HWD_CS_BASE_ADDR+0x00C4)  /* R/W */
#define HWD_UIM_HOT_PLUG_EINT0_CON         (HWD_CS_BASE_ADDR+0x00C8)  /* R/W */
#define HWD_UIM_HOT_PLUG_EINT1_CON         (HWD_CS_BASE_ADDR+0x00CC)  /* R/W */
#define HWD_UIM_HOT_PLUG_EINT_RSTDBC       (HWD_CS_BASE_ADDR+0x00D0)  /* WO */
#define HWD_FPM_LPM_CNT_CFG                (HWD_CS_BASE_ADDR+0x00D4)  /* R/W */
#if (SYS_ASIC == SA_MT6735)
#define HWD_RTC_CNT_LO                     (HWD_CS_BASE_ADDR+0x00D8)  /* RO */
#define HWD_RTC_CNT_HI                     (HWD_CS_BASE_ADDR+0x00DC)  /* RO */
#define HWD_FPM_CNT_LO                     (HWD_CS_BASE_ADDR+0x00E0)  /* RO */
#define HWD_FPM_CNT_HI                     (HWD_CS_BASE_ADDR+0x00E4)  /* RO */
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_LPM_CNT_LO                     (HWD_CS_BASE_ADDR+0x00E0)  /* RO */
#define HWD_LPM_CNT_HI                     (HWD_CS_BASE_ADDR+0x00E4)  /* RO */
#endif
#define HWD_C2K_APSRC_REQ_TIME_LO          (HWD_CS_BASE_ADDR+0x00E8)  /* RO */
#define HWD_C2K_APSRC_REQ_TIME_HI          (HWD_CS_BASE_ADDR+0x00EC)  /* RO */
#define HWD_C2K_APSRC_ACK_TIME_LO          (HWD_CS_BASE_ADDR+0x00F0)  /* RO */
#define HWD_C2K_APSRC_ACK_TIME_HI          (HWD_CS_BASE_ADDR+0x00F4)  /* RO */
#if (SYS_ASIC >= SA_MT6755)
/*  for Wakeup */
#define HWD_C2K_FRC_SHADOW_LO              (HWD_CS_BASE_ADDR+0x00F8)  /* RO */
#define HWD_C2K_FRC_SHADOW_MI              (HWD_CS_BASE_ADDR+0x00FC)  /* RO */
#define HWD_C2K_FRC_SHADOW_HI              (HWD_CS_BASE_ADDR+0x0100)  /* RO */
#define HWD_C2K_TIMESTAMP_SHADOW_LO        (HWD_CS_BASE_ADDR+0x0104)  /* RO */
#define HWD_C2K_TIMESTAMP_SHADOW_HI        (HWD_CS_BASE_ADDR+0x0108)  /* RO */
#define HWD_C2K_FRC_SHADOW_RDY             (HWD_CS_BASE_ADDR+0x010C)  /* RO */

/*  for Sync */
#define HWD_C2K_FRC_1X_SYNC_LO             (HWD_CS_BASE_ADDR+0x0110)  /* RO */
#define HWD_C2K_FRC_1X_SYNC_MI             (HWD_CS_BASE_ADDR+0x0114)  /* RO */
#define HWD_C2K_FRC_1X_SYNC_HI             (HWD_CS_BASE_ADDR+0x0118)  /* RO */
#define HWD_C2K_FRC_DO_SYNC_LO             (HWD_CS_BASE_ADDR+0x011C)  /* RO */
#define HWD_C2K_FRC_DO_SYNC_MI             (HWD_CS_BASE_ADDR+0x0120)  /* RO */
#define HWD_C2K_FRC_DO_SYNC_HI             (HWD_CS_BASE_ADDR+0x0124)  /* RO */
#define HWD_CLK_26M_SETTLETIME             (HWD_CS_BASE_ADDR+0x0134)  /* RO */
#define HWD_RFPOR_VRF18_EN                 (HWD_CS_BASE_ADDR+0x0138)  /* RO */
#endif

#define HWD_REG_SRESET                     (HWD_CP_BASE_ADDR+0x00008000)  /* R/W  Software Reset Register */
/*---------------------------------------------------**          
** CP Reset Generator for Media Peripheral           **          
**---------------------------------------------------*/   
#define HWD_SRESET                         HWD_REG_SRESET

#define HWD_CS_CLK32K_CNT                  (HWD_CP_BASE_ADDR+0x00008004) /* R */

#define HWD_CLK32_READ_CNT()               (HwdRead32(HWD_CS_CLK32K_CNT))  


/*******************
** UIM REGISTER   **
********************/
#define HWD_UIM_BASE_ADDR        (HWD_CP_BASE_ADDR+0x00010000)

#define HWD_UIM_CTL              (HWD_UIM_BASE_ADDR+0x0000)  /* UIM control register */
#define HWD_UIM_TX_BUF           (HWD_UIM_BASE_ADDR+0x0004)  /* UIM Tx Data buffer */
#define HWD_UIM_RX_BUF           (HWD_UIM_BASE_ADDR+0x0008)  /* UIM Rx data buffer */
#define HWD_UIM_RX_STAT          (HWD_UIM_BASE_ADDR+0x000c)  /* UIM Rx status */
#define HWD_UIM_RX_WD_CNT        (HWD_UIM_BASE_ADDR+0x0010)  /* UIM Rx Buffer word count */
#define HWD_UIM_WWT_CTL          (HWD_UIM_BASE_ADDR+0x0014)  /* UIM work waiting time control register */
#define HWD_UIM_WWT_END_HI       (HWD_UIM_BASE_ADDR+0x0018)  /* UIM work waiting time end count, high */
#define HWD_UIM_WWT_END_LO       (HWD_UIM_BASE_ADDR+0x001c)  /* UIM work waiting time end count, low */
#define HWD_UIM_TX_INT_MSK       (HWD_UIM_BASE_ADDR+0x0020)  /* UIM Tx interrupt mask */
#define HWD_UIM_TX_STAT          (HWD_UIM_BASE_ADDR+0x0024)  /* UIM Tx interrupt status */
#define HWD_UIM_BUF_TRIG         (HWD_UIM_BASE_ADDR+0x0028)  /* UIM Buffer trigger level */
#define HWD_UIM_TX_WD_CNT        (HWD_UIM_BASE_ADDR+0x002c)  /* UIM Buffer word count during Tx mode*/
#define HWD_UIM_MAXBYTES         (HWD_UIM_BASE_ADDR+0x0030)  /* Maxmum UIM bytes to transmit */
#if (SYS_ASIC >= SA_MT6755)
#define HWD_UIM_BAUD_CTRL        (HWD_UIM_BASE_ADDR+0x0034)  /* UIM baud rate control */
#define HWD_UIM_IO_CTRL          (HWD_UIM_BASE_ADDR+0x0038)  /* UIM IO control */
#endif

#define HWD_UIM2_BASE_ADDR        (HWD_CP_BASE_ADDR+0x00011000)

#define HWD_UIM2_CTL              (HWD_UIM2_BASE_ADDR+0x0000)  /* UIM control register */
#define HWD_UIM2_TX_BUF           (HWD_UIM2_BASE_ADDR+0x0004)  /* UIM Tx Data buffer */
#define HWD_UIM2_RX_BUF           (HWD_UIM2_BASE_ADDR+0x0008)  /* UIM Rx data buffer */
#define HWD_UIM2_RX_STAT          (HWD_UIM2_BASE_ADDR+0x000c)  /* UIM Rx status */
#define HWD_UIM2_RX_WD_CNT        (HWD_UIM2_BASE_ADDR+0x0010)  /* UIM Rx Buffer word count */
#define HWD_UIM2_WWT_CTL          (HWD_UIM2_BASE_ADDR+0x0014)  /* UIM work waiting time control register */
#define HWD_UIM2_WWT_END_HI       (HWD_UIM2_BASE_ADDR+0x0018)  /* UIM work waiting time end count, high */
#define HWD_UIM2_WWT_END_LO       (HWD_UIM2_BASE_ADDR+0x001c)  /* UIM work waiting time end count, low */
#define HWD_UIM2_TX_INT_MSK       (HWD_UIM2_BASE_ADDR+0x0020)  /* UIM Tx interrupt mask */
#define HWD_UIM2_TX_STAT          (HWD_UIM2_BASE_ADDR+0x0024)  /* UIM Tx interrupt status */
#define HWD_UIM2_BUF_TRIG         (HWD_UIM2_BASE_ADDR+0x0028)  /* UIM Buffer trigger level */
#define HWD_UIM2_TX_WD_CNT        (HWD_UIM2_BASE_ADDR+0x002c)  /* UIM Buffer word count during Tx mode*/
#define HWD_UIM2_MAXBYTES         (HWD_UIM2_BASE_ADDR+0x0030)  /* Maxmum UIM bytes to transmit */
#if (SYS_ASIC >= SA_MT6755)
#define HWD_UIM2_BAUD_CTRL        (HWD_UIM2_BASE_ADDR+0x0034)  /* UIM baud rate control */
#define HWD_UIM2_IO_CTRL          (HWD_UIM2_BASE_ADDR+0x0038)  /* UIM IO control */
#endif

/*------------------------**
** Mixed Signal Interface **
**------------------------*/
#define HWD_MXS_BASE_ADDR                 (HWD_NO_BASE_ADDR+0x0b840000)

#define MTK_HWD_M_RXSD_TESTMODE           (HWD_MXS_BASE_ADDR+0x000C)  /* R/W  [0:0], 0=>normal, 1=>uses test in to RXF  0x00   */
#define MTK_HWD_M_RXSD_FIRTAP             (HWD_MXS_BASE_ADDR+0x0018)  /* R/W  [7:0], RxSd FIR I-ch coefficient          0x54   */

/** New added Macro definitions for DFE compensation filter related to MTK OrionC-Everest Platform */
/** Compensation filter coefficient 0 */
#define MTK_HWD_M_RXSD_COMP_F_C0          (HWD_MXS_BASE_ADDR+0x0020)
/** Compensation filter coefficient 1*/
#define MTK_HWD_M_RXSD_COMP_F_C1          (HWD_MXS_BASE_ADDR+0x0024)
/** Compensation filter coefficient 2*/
#define MTK_HWD_M_RXSD_COMP_F_C2          (HWD_MXS_BASE_ADDR+0x0028)
/** Compensation filter coefficient 3 */
#define MTK_HWD_M_RXSD_COMP_F_C3          (HWD_MXS_BASE_ADDR+0x002C)
/** Compensation filter coefficient 4 */
#define MTK_HWD_M_RXSD_COMP_F_C4          (HWD_MXS_BASE_ADDR+0x0030)
/** Compensation filter coefficient 5 */
#define MTK_HWD_M_RXSD_COMP_F_C5          (HWD_MXS_BASE_ADDR+0x0034)
/** Compensation filter coefficient 6 */
#define MTK_HWD_M_RXSD_COMP_F_C6          (HWD_MXS_BASE_ADDR+0x0038)
/** Compensation filter coefficient 7 */
#define MTK_HWD_M_RXSD_COMP_F_C7          (HWD_MXS_BASE_ADDR+0x003C)
/** Compensation filter coefficient 8 */
#define MTK_HWD_M_RXSD_COMP_F_C8          (HWD_MXS_BASE_ADDR+0x0040)
/** Compensation filter coefficient 9 */
#define MTK_HWD_M_RXSD_COMP_F_C9          (HWD_MXS_BASE_ADDR+0x0044)
/** Compensation filter coefficient 10 */
#define MTK_HWD_M_RXSD_COMP_F_C10         (HWD_MXS_BASE_ADDR+0x0048)

#define MTK_HWD_M_RXSD_REGSEL             (HWD_MXS_BASE_ADDR+0x0054)  /* R/W  [3:0], RxSd Control processor select      0x00   */
#define MTK_HWD_M_RXSD_ENABLES            (HWD_MXS_BASE_ADDR+0x0078)  /* R/W       , Rx Control register                       */
#define MTK_HWD_M_RXSD_EN_PHASE_EQ        (HWD_MXS_BASE_ADDR+0x007C)  /* R/W  [1:0], RxSd En Phase Equalizer for DRC    0x00   */

/** New added Macro definitions for DFE RX DC module related to MTK OrionC-Everest Platform */
/** Main Path DC calibration control Register 
*** bit 0 is dc calibration mode selection bit;
*** 0 : enable normal dc estimation loop
*** 1 : enable pre-burst dc calibration estimation loop
*** bit 1 is only effective when operated in dc calibration mode, it will generated a start pulse when asserted '1'
*** 0 : no effect
*** 1 : issue dc calibration start pulse */
#define MTK_HWD_M_RXSD_DC_CAL_CTRL        (HWD_MXS_BASE_ADDR+0x0130)

/** Main Path DC calibration Start Timing Register 
*** 0x000-0x3ff. Only effective when operated in dc calibration mode.
*** The time unit is Tc/8, the maximum support timing value is 1023(~104us)  */
#define MTK_HWD_M_RXSD_DC_CAL_TIME        (HWD_MXS_BASE_ADDR+0x0134)

/** Main Path DC calibration Length Index Register-- Only effective when operated in dc calibration mode. 0x00-0x03
*** This registers indicates the dc calibration length.
*** 00: calibration length is 128*8 samples (at 8x chip rate)
*** 01: calibration length is 64*8 samples
*** 10: calibration length is 32*8 samples
*** 11: reserved */
#define MTK_HWD_M_RXSD_DC_CAL_LEN         (HWD_MXS_BASE_ADDR+0x0138)

/** Main Path DC calibration Interrupt Control Register
*** Bit 0 it the pre-burst dc calibration interrupt control mask bit
*** 0: interrupt is not visible to CP
*** 1 : interrupt is visible to CP.
*** Bit 1 is the clear bit for dc calibration interrupt. It will generate a pulse to clear the interrupt when asserted '1'.
*** 0 : no effect
*** 1 : clear the interrupt.*/
#define MTK_HWD_M_RXSD_DC_INT_CTRL        (HWD_MXS_BASE_ADDR+0x013C)

/** New added Macro definitions for DFE RX Frac and Integer Scaling related to MTK OrionC-Everest Platform*/

/** Main path integer and fractional scaling
*** 0:3--This field is the fraction scale factor. The value range is 0-15.
*** 0: bypass fractional scaling
*** 1 : multiply the input with factor 1/16
*** ����
*** 15 : multiply the input with factor 15/16
*** 4:6--This field is the integer scale factor. It controls how to extract 9-bit from 14-bit input.
*** 000 : extract bit[13:5] as output (c0r5)
*** 001 : extract bit[12:4] as output (c1r4)
*** 010 : extract bit[11:3] as output (c2r3)
*** 011 : extract bit[10:2] as output (c3r2)
*** 100 : extract bit[9:1] as output (c4r1)
*** 101 : extract bit[8:0] as output (c5r0)
*** others: reserved
*/
#define MTK_HWD_M_RXSD_SCALE              (HWD_MXS_BASE_ADDR+0x0140)

/** New added Macro definitions for DFE RX Phase Jump Compensation related to MTK OrionC-Everest Platform*/
/** Main path LNA phase jump mode control 
*** Bit 0 controls whether to enable phase jump compensation or not.
*** 0: Bypass phase jump compensation.
*** 1: Enable phase jump compensation.
*** Bit 1 controls the phase jump compensation mode selection.
*** 0 : immediate mode
*** 1 : delay mode. */
#define MTK_HWD_M_RXSD_PHASE_JUMP_CTRL    (HWD_MXS_BASE_ADDR+0x0150)

/** Main path timing when  LNA phase jump takes effect. */
#define MTK_HWD_M_RXSD_PHASE_JUMP_TIME    (HWD_MXS_BASE_ADDR+0x0154)

/** New added Macro definitions for DFE RX Phase I/Q imbalance Compensation related to MTK OrionC-Everest Platform*/
/** Main path I/Q phase imbalance correction factor */
#define MTK_HWD_M_RXSD_PHASE_IMB          (HWD_MXS_BASE_ADDR+0x0160)

/** Main path I External DC Offset */
#define MTK_HWD_M_RXSD_OFFSET_I           (HWD_MXS_BASE_ADDR+0x0170)
/** Main path Q External DC Offset */
#define MTK_HWD_M_RXSD_OFFSET_Q           (HWD_MXS_BASE_ADDR+0x0174)

#define MTK_HWD_M_RXSD1_BITSEL_DIGIGAIN   (HWD_MXS_BASE_ADDR+0x0180)  /* R/W  [9:0], RxSd Bitsel [11:8], digi gain [6:0] 0x00  */


/** Main Rx filters reset enable to allow global reset to reset the RX filter*/
#define MTK_HWD_M_RXSD_RST_SW             (HWD_MXS_BASE_ADDR+0x0184)  /* R/W  0x00  */
/** Main receiving path initial value for i-branch dc estimation loop. 
*** For calibration mode, its value is zero;
*** for normal dc estimation loop, the value is the dc offset between two analog AGC gains.*/
#define MTK_HWD_M_RXSD_DCO_I_INIT         (HWD_MXS_BASE_ADDR+0x0188)
/** Main receiving path initial value for Q-branch dc estimation loop. 
*** For calibration mode, its value is zero;
*** for normal dc estimation loop, the value is the dc offset between two analog AGC gains.*/
#define MTK_HWD_M_RXSD_DCO_Q_INIT         (HWD_MXS_BASE_ADDR+0x018C)

/** Main Rx Filter coefficient for internal DC offset correction and CIC order 
*** b0:b4--Rx filter coefficient for internal DC correction: Log2(x) = COEF + 3 */
#define MTK_HWD_M_RXSD_DC_IIR_COEF        (HWD_MXS_BASE_ADDR+0x0190)

/** Main Rx I-channel DC offset level output  */
#define MTK_HWD_M_RXSD_DCO_TP_I           (HWD_MXS_BASE_ADDR+0x0194)
/** Main Rx Q-channel DC offset level output */
#define MTK_HWD_M_RXSD_DCO_TP_Q           (HWD_MXS_BASE_ADDR+0x0198)

/** Main path LNA phase jump value */
#define MTK_HWD_M_RXSD_PHASE_JUMP         (HWD_MXS_BASE_ADDR+0x019C)

/** Main path wideband RSSI */
#define MTK_HWD_M_RXSD_WB_RSSI            (HWD_MXS_BASE_ADDR+0x01A0)

/** Main Rx ADC mode 
*** b0: RXSD_EN_CDMA. 0:   Disable Rx filter CDMA mode. 1: Enable Rx filter CDMA mode
*** b1: RXSD_EN_GPS. 0: Disable Rx filter GPS. 1: Enable Rx filter GPS mode
*** b2: RXSD_EN_DO. 0: Disable Rx filter DO mode. 1: Enable Rx filter DO mode
*/
#define MTK_HWD_M_RXSD_ADC_MODE           (HWD_MXS_BASE_ADDR+0x01A8)

/** Main Rx phase equalizer A2 */
#define MTK_HWD_M_RXSD_AP_IIR_A2          (HWD_MXS_BASE_ADDR+0x01AC)

/** Main Rx phase equalizer A1 */
#define MTK_HWD_M_RXSD_AP_IIR_A1          (HWD_MXS_BASE_ADDR+0x01B0)

/** Main Rx filter internal sub-block reset */
#define MTK_HWD_M_RXSD_GP_RSTN            (HWD_MXS_BASE_ADDR+0x01B4)
/** Main Rx gain control */

#if (SYS_ASIC >= SA_MT6797)
#define MTK_HWD_M_RXSD_GAIN_UP            (HWD_MXS_BASE_ADDR+0x01AC)
#else
#define MTK_HWD_M_RXSD_GAIN_UP            (HWD_MXS_BASE_ADDR+0x01B8)
#endif

/** Main AGC control */
#define MTK_HWD_M_RXSD_CTRL               (HWD_MXS_BASE_ADDR+0x01BC)

#define MTK_HWD_M_RXSD_LOG2TARGET         (HWD_MXS_BASE_ADDR+0x01C0)
#define MTK_HWD_M_RXSD_LOOPGAIN           (HWD_MXS_BASE_ADDR+0x01C4)
#define MTK_HWD_M_RXSD_STEPGAINADJ        (HWD_MXS_BASE_ADDR+0x01C8)
#define MTK_HWD_M_RXSD_GAINOFFSET_I       (HWD_MXS_BASE_ADDR+0x01CC)
#define MTK_HWD_M_RXSD_GAINOFFSET_Q       (HWD_MXS_BASE_ADDR+0x01D0)
#define MTK_HWD_M_RXSD_PERIODMASK         (HWD_MXS_BASE_ADDR+0x01D4)
#define MTK_HWD_M_RXSD_WINDOW_LSB         (HWD_MXS_BASE_ADDR+0x01D8)
#define MTK_HWD_M_RXSD_DIGIGAIN_TIMING    (HWD_MXS_BASE_ADDR+0x01DC)
#define MTK_HWD_M_RXSD_STEPGAIN_TIMING    (HWD_MXS_BASE_ADDR+0x01E0)
#define MTK_HWD_M_RXSD_WINDOW_MSB         (HWD_MXS_BASE_ADDR+0x01E4)
#define MTK_HWD_M_RXSD_RXAGC_BITSELDIG_I  (HWD_MXS_BASE_ADDR+0x01E8)
#define MTK_HWD_M_RXSD_RXAGC_BITSELDIG_Q  (HWD_MXS_BASE_ADDR+0x01EC)
#define MTK_HWD_M_RXSD_RXAGC_DIGIGAIN     (HWD_MXS_BASE_ADDR+0x01F0)
#define MTK_HWD_M_RXSD_DO_1X_SEL          (HWD_MXS_BASE_ADDR+0x01F4)

/** Start for Diversity Path */
#define MTK_HWD_D_RXSD_TESTMODE           (HWD_MXS_BASE_ADDR+0x026C)  /* R/W  [0:0], 0=>normal, 1=>uses test in to RXF  0x00   */
#define MTK_HWD_D_RXSD_FIRTAP             (HWD_MXS_BASE_ADDR+0x0274)  /* R/W  [7:0], RxSd FIR I-ch coefficient          0x54   */

#define MTK_HWD_D_RXSD_REGSEL             (HWD_MXS_BASE_ADDR+0x0280)  /* R/W  [3:0], RxSd Control processor select      0x00   */
#define MTK_HWD_D_RXSD_ENABLES            (HWD_MXS_BASE_ADDR+0x0284)  /* R/W       , Rx Control register                       */
#define MTK_HWD_D_RXSD_EN_PHASE_EQ        (HWD_MXS_BASE_ADDR+0x0288)  /* R/W  [1:0], RxSd En Phase Equalizer for DRC    0x00   */

/** Main Rx ADC mode 
*** b0: RXSD_EN_CDMA. 0:   Disable Rx filter CDMA mode. 1: Enable Rx filter CDMA mode
*** b1: RXSD_EN_GPS. 0: Disable Rx filter GPS. 1: Enable Rx filter GPS mode
*** b2: RXSD_EN_DO. 0: Disable Rx filter DO mode. 1: Enable Rx filter DO mode
*/
#define MTK_HWD_D_RXSD_ADC_MODE           (HWD_MXS_BASE_ADDR+0x028C)

/** Main Rx phase equalizer A2 */
#define MTK_HWD_D_RXSD_AP_IIR_A2          (HWD_MXS_BASE_ADDR+0x0290)

/** Main Rx phase equalizer A1 */
#define MTK_HWD_D_RXSD_AP_IIR_A1          (HWD_MXS_BASE_ADDR+0x0294)

/** Main Rx filter internal sub-block reset */
#define MTK_HWD_D_RXSD_GP_RSTN            (HWD_MXS_BASE_ADDR+0x0298)

/** Main Rx gain control */
#if (SYS_ASIC >= SA_MT6797)
#define MTK_HWD_D_RXSD_GAIN_UP            (HWD_MXS_BASE_ADDR+0x02EC)
#else
#define MTK_HWD_D_RXSD_GAIN_UP            (HWD_MXS_BASE_ADDR+0x029C)
#endif

/** Main AGC control */
#define MTK_HWD_D_RXSD_CTRL               (HWD_MXS_BASE_ADDR+0x02A0)

/** Main path I External DC Offset */
#define MTK_HWD_D_RXSD_OFFSET_I           (HWD_MXS_BASE_ADDR+0x02B0)

/** Main path Q External DC Offset */
#define MTK_HWD_D_RXSD_OFFSET_Q           (HWD_MXS_BASE_ADDR+0x02B4)

#define MTK_HWD_D_RXSD1_BITSEL_DIGIGAIN   (HWD_MXS_BASE_ADDR+0x02C0)  /* R/W  [9:0], RxSd Bitsel [11:8], digi gain [6:0] 0x00  */

/** Main Rx filters reset enable to allow global reset to reset the RX filter*/
#define MTK_HWD_D_RXSD_RST_SW             (HWD_MXS_BASE_ADDR+0x02C4)  /* R/W  0x00  */

/** Main receiving path initial value for i-branch dc estimation loop. 
*** For calibration mode, its value is zero;
*** for normal dc estimation loop, the value is the dc offset between two analog AGC gains.*/
#define MTK_HWD_D_RXSD_DCO_I_INIT         (HWD_MXS_BASE_ADDR+0x02C8)

/** Main receiving path initial value for Q-branch dc estimation loop. 
*** For calibration mode, its value is zero;
*** for normal dc estimation loop, the value is the dc offset between two analog AGC gains.*/
#define MTK_HWD_D_RXSD_DCO_Q_INIT         (HWD_MXS_BASE_ADDR+0x02CC)

/** Main Rx Filter coefficient for internal DC offset correction and CIC order 
*** b0:b4--Rx filter coefficient for internal DC correction: Log2(x) = COEF + 3 */
#define MTK_HWD_D_RXSD_DC_IIR_COEF        (HWD_MXS_BASE_ADDR+0x02D0)

/** Main Rx I-channel DC offset level output  */
#define MTK_HWD_D_RXSD_DCO_TP_I           (HWD_MXS_BASE_ADDR+0x02D4)

/** Main Rx Q-channel DC offset level output */
#define MTK_HWD_D_RXSD_DCO_TP_Q           (HWD_MXS_BASE_ADDR+0x02D8)

/** Main path LNA phase jump value */
#define MTK_HWD_D_RXSD_PHASE_JUMP         (HWD_MXS_BASE_ADDR+0x02DC)

/** Main path wideband RSSI */
#define MTK_HWD_D_RXSD_WB_RSSI            (HWD_MXS_BASE_ADDR+0x02E0)

#define MTK_HWD_D_RXSD_LOG2TARGET         (HWD_MXS_BASE_ADDR+0x0300)
#define MTK_HWD_D_RXSD_LOOPGAIN           (HWD_MXS_BASE_ADDR+0x0304)
#define MTK_HWD_D_RXSD_STEPGAINADJ        (HWD_MXS_BASE_ADDR+0x0308)
#define MTK_HWD_D_RXSD_GAINOFFSET_I       (HWD_MXS_BASE_ADDR+0x030C)
#define MTK_HWD_D_RXSD_GAINOFFSET_Q       (HWD_MXS_BASE_ADDR+0x0310)
#define MTK_HWD_D_RXSD_PERIODMASK         (HWD_MXS_BASE_ADDR+0x0314)
#define MTK_HWD_D_RXSD_WINDOW_LSB         (HWD_MXS_BASE_ADDR+0x0318)
#define MTK_HWD_D_RXSD_DIGIGAIN_TIMING    (HWD_MXS_BASE_ADDR+0x031C)
#define MTK_HWD_D_RXSD_STEPGAIN_TIMING    (HWD_MXS_BASE_ADDR+0x0320)
#define MTK_HWD_D_RXSD_WINDOW_MSB         (HWD_MXS_BASE_ADDR+0x0324)
#define MTK_HWD_D_RXSD_RXAGC_BITSELDIG_I  (HWD_MXS_BASE_ADDR+0x0328)
#define MTK_HWD_D_RXSD_RXAGC_BITSELDIG_Q  (HWD_MXS_BASE_ADDR+0x032C)
#define MTK_HWD_D_RXSD_RXAGC_DIGIGAIN     (HWD_MXS_BASE_ADDR+0x0330)
#define MTK_HWD_D_RXSD_DO_1X_SEL          (HWD_MXS_BASE_ADDR+0x0334)

#define MTK_HWD_RXSD_RXAGC_TSTMODE        (HWD_MXS_BASE_ADDR+0x0340)
#define MTK_HWD_M_RXSD_RXAGC_TSTBUS       (HWD_MXS_BASE_ADDR+0x0344)
#define MTK_HWD_D_RXSD_RXAGC_TSTBUS       (HWD_MXS_BASE_ADDR+0x0348)
#define MTK_HWD_RXSD_SRC_SEL              (HWD_MXS_BASE_ADDR+0x0704)


/** New added Macro definitions for DFE compensation filter related to MTK OrionC-Everest Platform */
/** Compensation filter coefficient 0 */
#define MTK_HWD_D_RXSD_COMP_F_C0          (HWD_MXS_BASE_ADDR+0x0710)
/** Compensation filter coefficient 1*/
#define MTK_HWD_D_RXSD_COMP_F_C1          (HWD_MXS_BASE_ADDR+0x0714)
/** Compensation filter coefficient 2*/
#define MTK_HWD_D_RXSD_COMP_F_C2          (HWD_MXS_BASE_ADDR+0x0718)
/** Compensation filter coefficient 3 */
#define MTK_HWD_D_RXSD_COMP_F_C3          (HWD_MXS_BASE_ADDR+0x071C)
/** Compensation filter coefficient 4 */
#define MTK_HWD_D_RXSD_COMP_F_C4          (HWD_MXS_BASE_ADDR+0x0720)
/** Compensation filter coefficient 5 */
#define MTK_HWD_D_RXSD_COMP_F_C5          (HWD_MXS_BASE_ADDR+0x0724)
/** Compensation filter coefficient 6 */
#define MTK_HWD_D_RXSD_COMP_F_C6          (HWD_MXS_BASE_ADDR+0x0728)
/** Compensation filter coefficient 7 */
#define MTK_HWD_D_RXSD_COMP_F_C7          (HWD_MXS_BASE_ADDR+0x072C)
/** Compensation filter coefficient 8 */
#define MTK_HWD_D_RXSD_COMP_F_C8          (HWD_MXS_BASE_ADDR+0x0730)
/** Compensation filter coefficient 9 */
#define MTK_HWD_D_RXSD_COMP_F_C9          (HWD_MXS_BASE_ADDR+0x0734)
/** Compensation filter coefficient 10 */
#define MTK_HWD_D_RXSD_COMP_F_C10         (HWD_MXS_BASE_ADDR+0x0738)


/** New added Macro definitions for DFE RX DC module related to MTK OrionC-Everest Platform */
/** Main Path DC calibration control Register 
*** bit 0 is dc calibration mode selection bit;
*** 0 : enable normal dc estimation loop
*** 1 : enable pre-burst dc calibration estimation loop
*** bit 1 is only effective when operated in dc calibration mode, it will generated a start pulse when asserted '1'
*** 0 : no effect
*** 1 : issue dc calibration start pulse */
#define MTK_HWD_D_RXSD_DC_CAL_CTRL        (HWD_MXS_BASE_ADDR+0x0740)

/** Main Path DC calibration Start Timing Register 
*** 0x000-0x3ff. Only effective when operated in dc calibration mode.
*** The time unit is Tc/8, the maximum support timing value is 1023(~104us)  */
#define MTK_HWD_D_RXSD_DC_CAL_TIME        (HWD_MXS_BASE_ADDR+0x0744)

/** Main Path DC calibration Length Index Register-- Only effective when operated in dc calibration mode. 0x00-0x03
*** This registers indicates the dc calibration length.
*** 00: calibration length is 128*8 samples (at 8x chip rate)
*** 01: calibration length is 64*8 samples
*** 10: calibration length is 32*8 samples
*** 11: reserved */
#define MTK_HWD_D_RXSD_DC_CAL_LEN         (HWD_MXS_BASE_ADDR+0x0748)

/** Main Path DC calibration Interrupt Control Register
*** Bit 0 it the pre-burst dc calibration interrupt control mask bit
*** 0: interrupt is not visible to CP
*** 1 : interrupt is visible to CP.
*** Bit 1 is the clear bit for dc calibration interrupt. It will generate a pulse to clear the interrupt when asserted '1'.
*** 0 : no effect
*** 1 : clear the interrupt.*/
#define MTK_HWD_D_RXSD_DC_INT_CTRL        (HWD_MXS_BASE_ADDR+0x074C)

/** New added Macro definitions for DFE RX Frac and Integer Scaling related to MTK OrionC-Everest Platform*/

/** Main path integer and fractional scaling
*** 0:3--This field is the fraction scale factor. The value range is 0-15.
*** 0: bypass fractional scaling
*** 1 : multiply the input with factor 1/16
*** ����
*** 15 : multiply the input with factor 15/16
*** 4:6--This field is the integer scale factor. It controls how to extract 9-bit from 14-bit input.
*** 000 : extract bit[13:5] as output (c0r5)
*** 001 : extract bit[12:4] as output (c1r4)
*** 010 : extract bit[11:3] as output (c2r3)
*** 011 : extract bit[10:2] as output (c3r2)
*** 100 : extract bit[9:1] as output (c4r1)
*** 101 : extract bit[8:0] as output (c5r0)
*** others: reserved
*/
#define MTK_HWD_D_RXSD_SCALE              (HWD_MXS_BASE_ADDR+0x0750)

/** New added Macro definitions for DFE RX Phase Jump Compensation related to MTK OrionC-Everest Platform*/
/** Main path LNA phase jump mode control 
*** Bit 0 controls whether to enable phase jump compensation or not.
*** 0: Bypass phase jump compensation.
*** 1: Enable phase jump compensation.
*** Bit 1 controls the phase jump compensation mode selection.
*** 0 : immediate mode
*** 1 : delay mode. */
#define MTK_HWD_D_RXSD_PHASE_JUMP_CTRL    (HWD_MXS_BASE_ADDR+0x0760)

/** Main path timing when  LNA phase jump takes effect. */
#define MTK_HWD_D_RXSD_PHASE_JUMP_TIME    (HWD_MXS_BASE_ADDR+0x0764)

/** New added Macro definitions for DFE RX Phase I/Q imbalance Compensation related to MTK OrionC-Everest Platform*/
/** Main path I/Q phase imbalance correction factor */
#define MTK_HWD_D_RXSD_PHASE_IMB          (HWD_MXS_BASE_ADDR+0x0770)

#if (SYS_ASIC >= SA_MT6755)
#define HWD_JADE_RXSD_DFE_TEST_DATA_IN_IQ_EXTRA_MSB (HWD_MXS_BASE_ADDR+0x0114)
#define HWD_JADE_RXSD_DFE_TEST_DATA_IN_VLD_RDY      (HWD_MXS_BASE_ADDR+0x0118)
#define HWD_JADE_RXSD_AFC_DELTA_1X                  (HWD_MXS_BASE_ADDR+0x01A8)
#define HWD_JADE_RXSD_AFC_DELTA_DO                  (HWD_MXS_BASE_ADDR+0x02E8)
#define HWD_JADE_RXSD_SRC_SEL                       (HWD_MXS_BASE_ADDR+0x0704)

#define HWD_JADE_RXSD_TESTMODE_M                    (HWD_MXS_BASE_ADDR+0x000C)
#define HWD_JADE_RXSD_FIRTAP_M                      (HWD_MXS_BASE_ADDR+0x0018)/*Not used*/

#define HWD_JADE_RXSD_COMP_FILT_COEFF0_M            (HWD_MXS_BASE_ADDR+0x0020)
#define HWD_JADE_RXSD_COMP_FILT_COEFF1_M            (HWD_MXS_BASE_ADDR+0x0024)
#define HWD_JADE_RXSD_COMP_FILT_COEFF2_M            (HWD_MXS_BASE_ADDR+0x0028)
#define HWD_JADE_RXSD_COMP_FILT_COEFF3_M            (HWD_MXS_BASE_ADDR+0x002C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF4_M            (HWD_MXS_BASE_ADDR+0x0030)
#define HWD_JADE_RXSD_COMP_FILT_COEFF5_M            (HWD_MXS_BASE_ADDR+0x0034)
#define HWD_JADE_RXSD_COMP_FILT_COEFF6_M            (HWD_MXS_BASE_ADDR+0x0038)
#define HWD_JADE_RXSD_COMP_FILT_COEFF7_M            (HWD_MXS_BASE_ADDR+0x003C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF8_M            (HWD_MXS_BASE_ADDR+0x0040)
#define HWD_JADE_RXSD_COMP_FILT_COEFF9_M            (HWD_MXS_BASE_ADDR+0x0044)
#define HWD_JADE_RXSD_COMP_FILT_COEFF10_M           (HWD_MXS_BASE_ADDR+0x0048)

#define HWD_JADE_RXSD_REGSEL_M                      (HWD_MXS_BASE_ADDR+0x0054)
#define HWD_JADE_RXSD_ENABLES_M                     (HWD_MXS_BASE_ADDR+0x0078)

#define HWD_JADE_RXSD_DFE_TESTPORT_MODE             (HWD_MXS_BASE_ADDR+0x0100)
#define HWD_JADE_RXSD_DFE_TEST_DATA_IN_I_LSB_M      (HWD_MXS_BASE_ADDR+0x0104)
#define HWD_JADE_RXSD_DFE_TEST_DATA_IN_Q_LSB_M      (HWD_MXS_BASE_ADDR+0x0108)

#define HWD_JADE_RXSD_DC_CAL_CTRL_M                 (HWD_MXS_BASE_ADDR+0x0130)
#define HWD_JADE_RXSD_DC_CAL_TIMING_M               (HWD_MXS_BASE_ADDR+0x0134)
#define HWD_JADE_RXSD_DC_CAL_LEN_IDX_M              (HWD_MXS_BASE_ADDR+0x0138)
#define HWD_JADE_RXSD_DC_INT_CTRL_M                 (HWD_MXS_BASE_ADDR+0x013C)
#define HWD_JADE_RXSD_SCALE_M                       (HWD_MXS_BASE_ADDR+0x0140)

#define HWD_JADE_RXSD_PHASE_JUMP_CTRL_M             (HWD_MXS_BASE_ADDR+0x0150)
#define HWD_JADE_RXSD_PHASE_JUMP_TIMING_M           (HWD_MXS_BASE_ADDR+0x0154)
#if (SYS_ASIC >= SA_MT6797)
#define HWD_RXSD_DCO_INI_VALUE_MODE_M               (HWD_MXS_BASE_ADDR+0x0158)
#define HWD_RXSD_DCO_INI_VALUE_TIMING_M             (HWD_MXS_BASE_ADDR+0x015C)
#endif
#define HWD_JADE_RXSD_IQ_PHASE_IMB_M                (HWD_MXS_BASE_ADDR+0x0160)
#define HWD_JADE_RXSD_OFFSETI_M                     (HWD_MXS_BASE_ADDR+0x0170)
#define HWD_JADE_RXSD_OFFSETQ_M                     (HWD_MXS_BASE_ADDR+0x0174)
#if (SYS_ASIC >= SA_MT6797)
#define HWD_JADE_RXSD_PRE_ITP_DCOFFSET_I_M          (HWD_MXS_BASE_ADDR+0x0178)
#define HWD_JADE_RXSD_PRE_ITP_DCOFFSET_Q_M          (HWD_MXS_BASE_ADDR+0x017C)
#endif
#define HWD_JADE_RXSD_BITSEL_DIGIGAIN_M             (HWD_MXS_BASE_ADDR+0x0180)
#define HWD_JADE_RXSD_MISC_CTRL_M                   (HWD_MXS_BASE_ADDR+0x0184)
#define HWD_JADE_RXSD_DCO_I_INI_VALUE_M             (HWD_MXS_BASE_ADDR+0x0188)
#define HWD_JADE_RXSD_DCO_Q_INI_VALUE_M             (HWD_MXS_BASE_ADDR+0x018C)
#define HWD_JADE_RXSD_DC_IIR_COEF_M                 (HWD_MXS_BASE_ADDR+0x0190)
#define HWD_JADE_RXSD_DCO_TP_I_M                    (HWD_MXS_BASE_ADDR+0x0194)
#define HWD_JADE_RXSD_DCO_TP_Q_M                    (HWD_MXS_BASE_ADDR+0x0198)
#define HWD_JADE_RXSD_PHASE_JUMP_VALUE_LSB_M        (HWD_MXS_BASE_ADDR+0x019C)
#define HWD_JADE_RXSD_PHASE_JUMP_VALUE_MSB_M        (HWD_MXS_BASE_ADDR+0x01A0)
#define HWD_JADE_RXSD_WB_RSSI_M                     (HWD_MXS_BASE_ADDR+0x01A4)

#define HWD_JADE_RXSD_ADC_MODE_M                    (HWD_MXS_BASE_ADDR+0x01B0)
#define HWD_JADE_RXSD_GP_RSTN_M                     (HWD_MXS_BASE_ADDR+0x01B4)
#if (SYS_ASIC >= SA_MT6797)
#define HWD_JADE_RXSD_GAIN_UP_M                     (HWD_MXS_BASE_ADDR+0x01AC)
#else
#define HWD_JADE_RXSD_GAIN_UP_M                     (HWD_MXS_BASE_ADDR+0x01B8)
#endif
#define HWD_JADE_RXSD_CTRL_M                        (HWD_MXS_BASE_ADDR+0x01BC)

#define HWD_JADE_RXSD_LOG2TARGET_M                  (HWD_MXS_BASE_ADDR+0x01C0)
#define HWD_JADE_RXSD_LOOPGAIN_M                    (HWD_MXS_BASE_ADDR+0x01C4)
#define HWD_JADE_RXSD_STEPGAINADJ_M                 (HWD_MXS_BASE_ADDR+0x01C8)
#define HWD_JADE_RXSD_GAINOFFSET_I_M                (HWD_MXS_BASE_ADDR+0x01CC)
#define HWD_JADE_RXSD_GAINOFFSET_Q_M                (HWD_MXS_BASE_ADDR+0x01D0)
#define HWD_JADE_RXSD_PERIODMASK_M                  (HWD_MXS_BASE_ADDR+0x01D4)
#define HWD_JADE_RXSD_WINDOW_LSB_M                  (HWD_MXS_BASE_ADDR+0x01D8)
#define HWD_JADE_RXSD_DIGIGAIN_TIMING_M             (HWD_MXS_BASE_ADDR+0x01DC)
#define HWD_JADE_RXSD_STEPGAIN_TIMING_M             (HWD_MXS_BASE_ADDR+0x01E0)
#define HWD_JADE_RXSD_WINDOW_MSB_M                  (HWD_MXS_BASE_ADDR+0x01E4)
#define HWD_JADE_RXSD_RXAGC_BITSELDIG_I_M           (HWD_MXS_BASE_ADDR+0x01E8)
#define HWD_JADE_RXSD_RXAGC_BITSELDIG_Q_M           (HWD_MXS_BASE_ADDR+0x01EC)
#define HWD_JADE_RXSD_RXAGC_DIGIGAIN_M              (HWD_MXS_BASE_ADDR+0x01F0)
#define HWD_JADE_RXSD_DO_1X_SEL_M                   (HWD_MXS_BASE_ADDR+0x01F4)
#define HWD_JADE_RXSD_RXAGC_TSTBUS_M                (HWD_MXS_BASE_ADDR+0x0344)

/** Start for Diversity Path */

#define HWD_JADE_RXSD_DFE_TEST_DATA_IN_I_LSB_D      (HWD_MXS_BASE_ADDR+0x010C)
#define HWD_JADE_RXSD_DFE_TEST_DATA_IN_Q_LSB_D      (HWD_MXS_BASE_ADDR+0x0110)

#define HWD_JADE_RXSD_TESTMODE_D                    (HWD_MXS_BASE_ADDR+0x026C)
#define HWD_JADE_RXSD_FIRTAP_D                      (HWD_MXS_BASE_ADDR+0x0274)/* Not Used*/

#define HWD_JADE_RXSD_REGSEL_D                      (HWD_MXS_BASE_ADDR+0x0280)
#define HWD_JADE_RXSD_ENABLES_D                     (HWD_MXS_BASE_ADDR+0x0284)

#define HWD_JADE_RXSD_ADC_MODE_D                    (HWD_MXS_BASE_ADDR+0x028C)


#define HWD_JADE_RXSD_GP_RSTN_D                     (HWD_MXS_BASE_ADDR+0x0298)
#if (SYS_ASIC >= SA_MT6797)
#define HWD_JADE_RXSD_GAIN_UP_D                     (HWD_MXS_BASE_ADDR+0x02EC)
#else
#define HWD_JADE_RXSD_GAIN_UP_D                     (HWD_MXS_BASE_ADDR+0x029C)
#endif
#define HWD_JADE_RXSD_CTRL_D                        (HWD_MXS_BASE_ADDR+0x02A0)

#define HWD_JADE_RXSD_OFFSETI_D                     (HWD_MXS_BASE_ADDR+0x02B0)
#define HWD_JADE_RXSD_OFFSETQ_D                     (HWD_MXS_BASE_ADDR+0x02B4)
#if (SYS_ASIC >= SA_MT6797)
#define HWD_JADE_RXSD_PRE_ITP_DCOFFSET_I_D          (HWD_MXS_BASE_ADDR+0x02B8)
#define HWD_JADE_RXSD_PRE_ITP_DCOFFSET_Q_D          (HWD_MXS_BASE_ADDR+0x02BC)
#endif

#define HWD_JADE_RXSD_BITSEL_DIGIGAIN_D             (HWD_MXS_BASE_ADDR+0x02C0)
#define HWD_JADE_RXSD_MISC_CTRL_D                   (HWD_MXS_BASE_ADDR+0x02C4)
#define HWD_JADE_RXSD_DCO_I_INI_VALUE_D             (HWD_MXS_BASE_ADDR+0x02C8)
#define HWD_JADE_RXSD_DCO_Q_INI_VALUE_D             (HWD_MXS_BASE_ADDR+0x02CC)
#define HWD_JADE_RXSD_DC_IIR_COEF_D                 (HWD_MXS_BASE_ADDR+0x02D0)
#define HWD_JADE_RXSD_DCO_TP_I_D                    (HWD_MXS_BASE_ADDR+0x02D4)
#define HWD_JADE_RXSD_DCO_TP_Q_D                    (HWD_MXS_BASE_ADDR+0x02D8)
#define HWD_JADE_RXSD_PHASE_JUMP_VALUE_LSB_D        (HWD_MXS_BASE_ADDR+0x02DC)
#define HWD_JADE_RXSD_PHASE_JUMP_VALUE_MSB_D        (HWD_MXS_BASE_ADDR+0x02E0)
#define HWD_JADE_RXSD_WB_RSSI_D                     (HWD_MXS_BASE_ADDR+0x02E4)

#define HWD_JADE_RXSD_LOG2TARGET_D                  (HWD_MXS_BASE_ADDR+0x0300)
#define HWD_JADE_RXSD_LOOPGAIN_D                    (HWD_MXS_BASE_ADDR+0x0304)
#define HWD_JADE_RXSD_STEPGAINADJ_D                 (HWD_MXS_BASE_ADDR+0x0308)
#define HWD_JADE_RXSD_GAINOFFSET_I_D                (HWD_MXS_BASE_ADDR+0x030C)
#define HWD_JADE_RXSD_GAINOFFSET_Q_D                (HWD_MXS_BASE_ADDR+0x0310)
#define HWD_JADE_RXSD_PERIODMASK_D                  (HWD_MXS_BASE_ADDR+0x0314)
#define HWD_JADE_RXSD_WINDOW_LSB_D                  (HWD_MXS_BASE_ADDR+0x0318)
#define HWD_JADE_RXSD_DIGIGAIN_TIMING_D             (HWD_MXS_BASE_ADDR+0x031C)
#define HWD_JADE_RXSD_STEPGAIN_TIMING_D             (HWD_MXS_BASE_ADDR+0x0320)
#define HWD_JADE_RXSD_WINDOW_MSB_D                  (HWD_MXS_BASE_ADDR+0x0324)
#define HWD_JADE_RXSD_RXAGC_BITSELDIG_I_D           (HWD_MXS_BASE_ADDR+0x0328)
#define HWD_JADE_RXSD_RXAGC_BITSELDIG_Q_D           (HWD_MXS_BASE_ADDR+0x032C)
#define HWD_JADE_RXSD_RXAGC_DIGIGAIN_D              (HWD_MXS_BASE_ADDR+0x0330)
#define HWD_JADE_RXSD_DO_1X_SEL_D                   (HWD_MXS_BASE_ADDR+0x0334)
#define HWD_JADE_RXSD_RXAGC_TSTMODE                 (HWD_MXS_BASE_ADDR+0x0340)
#define HWD_JADE_RXSD_RXAGC_TSTBUS_D                (HWD_MXS_BASE_ADDR+0x0348)

#define HWD_JADE_RXSD_COMP_FILT_COEFF0_D            (HWD_MXS_BASE_ADDR+0x0710)
#define HWD_JADE_RXSD_COMP_FILT_COEFF1_D            (HWD_MXS_BASE_ADDR+0x0714)
#define HWD_JADE_RXSD_COMP_FILT_COEFF2_D            (HWD_MXS_BASE_ADDR+0x0718)
#define HWD_JADE_RXSD_COMP_FILT_COEFF3_D            (HWD_MXS_BASE_ADDR+0x071C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF4_D            (HWD_MXS_BASE_ADDR+0x0720)
#define HWD_JADE_RXSD_COMP_FILT_COEFF5_D            (HWD_MXS_BASE_ADDR+0x0724)
#define HWD_JADE_RXSD_COMP_FILT_COEFF6_D            (HWD_MXS_BASE_ADDR+0x0728)
#define HWD_JADE_RXSD_COMP_FILT_COEFF7_D            (HWD_MXS_BASE_ADDR+0x072C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF8_D            (HWD_MXS_BASE_ADDR+0x0730)
#define HWD_JADE_RXSD_COMP_FILT_COEFF9_D            (HWD_MXS_BASE_ADDR+0x0734)
#define HWD_JADE_RXSD_COMP_FILT_COEFF10_D           (HWD_MXS_BASE_ADDR+0x0738)


#define HWD_JADE_RXSD_DC_CAL_CTRL_D                 (HWD_MXS_BASE_ADDR+0x0740)
#define HWD_JADE_RXSD_DC_CAL_TIME_D                 (HWD_MXS_BASE_ADDR+0x0744)
#define HWD_JADE_RXSD_DC_CAL_LEN_IDX_D              (HWD_MXS_BASE_ADDR+0x0748)
#define HWD_JADE_RXSD_DC_INT_CTRL_D                 (HWD_MXS_BASE_ADDR+0x074C)
#define HWD_JADE_RXSD_SCALE_D                       (HWD_MXS_BASE_ADDR+0x0750)
#define HWD_JADE_RXSD_PHASE_JUMP_CTRL_D             (HWD_MXS_BASE_ADDR+0x0760)
#define HWD_JADE_RXSD_PHASE_JUMP_TIME_D             (HWD_MXS_BASE_ADDR+0x0764)
#if (SYS_ASIC >= SA_MT6797)
#define HWD_RXSD_DCO_INI_VALUE_MODE_D               (HWD_MXS_BASE_ADDR+0x0768)
#define HWD_RXSD_DCO_INI_VALUE_TIMING_D             (HWD_MXS_BASE_ADDR+0x076C)
#endif
#define HWD_JADE_RXSD_IQ_PHASE_IMB_D                (HWD_MXS_BASE_ADDR+0x0770)

#define HWD_MXS_BASE_ADDR2                               (0x0b830000)
#define HWD_JADE_RXSD_COMP_FILT_COEFF11_M                (HWD_MXS_BASE_ADDR2 + 0)
#define HWD_JADE_RXSD_COMP_FILT_COEFF12_M                (HWD_MXS_BASE_ADDR2 + 0x4)
#define HWD_JADE_RXSD_COMP_FILT_COEFF13_M                (HWD_MXS_BASE_ADDR2 + 0x8)
#define HWD_JADE_RXSD_COMP_FILT_COEFF14_M                (HWD_MXS_BASE_ADDR2 + 0xC)
#define HWD_JADE_RXSD_COMP_FILT_COEFF15_M                (HWD_MXS_BASE_ADDR2 + 0x10)
#define HWD_JADE_RXSD_COMP_FILT_COEFF16_M                (HWD_MXS_BASE_ADDR2 + 0x14)
#define HWD_JADE_RXSD_COMP_FILT_COEFF17_M                (HWD_MXS_BASE_ADDR2 + 0x18)
#define HWD_JADE_RXSD_COMP_FILT_COEFF18_M                (HWD_MXS_BASE_ADDR2 + 0x1C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF19_M                (HWD_MXS_BASE_ADDR2 + 0x20)
#define HWD_JADE_RXSD_COMP_FILT_COEFF20_M                (HWD_MXS_BASE_ADDR2 + 0x24)
#define HWD_JADE_RXSD_COMP_FILT_COEFF21_M                (HWD_MXS_BASE_ADDR2 + 0x28)
#define HWD_JADE_RXSD_COMP_FILT_COEFF22_M                (HWD_MXS_BASE_ADDR2 + 0x2C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF23_M                (HWD_MXS_BASE_ADDR2 + 0x30)
#define HWD_JADE_RXSD_COMP_FILT_COEFF24_M                (HWD_MXS_BASE_ADDR2 + 0x34)
#define HWD_JADE_RXSD_COMP_FILT_COEFF25_M                (HWD_MXS_BASE_ADDR2 + 0x38)
#define HWD_JADE_RXSD_COMP_FILT_COEFF26_M                (HWD_MXS_BASE_ADDR2 + 0x3C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF27_M                (HWD_MXS_BASE_ADDR2 + 0x40)
#define HWD_JADE_RXSD_COMP_FILT_COEFF28_M                (HWD_MXS_BASE_ADDR2 + 0x44)
#define HWD_JADE_RXSD_COMP_FILT_COEFF29_M                (HWD_MXS_BASE_ADDR2 + 0x48)
#define HWD_JADE_RXSD_COMP_FILT_COEFF30_M                (HWD_MXS_BASE_ADDR2 + 0x4C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF31_M                (HWD_MXS_BASE_ADDR2 + 0x50)
#define HWD_JADE_RXSD_COMP_FILT_COEFF32_M                (HWD_MXS_BASE_ADDR2 + 0x54)
#define HWD_JADE_RXSD_COMP_FILT_LEN_SEL_M                (HWD_MXS_BASE_ADDR2 + 0x58)

#define HWD_JADE_RXSD_IIR_H1_COEFF_A1_M                  (HWD_MXS_BASE_ADDR2 + 0x80)
#define HWD_JADE_RXSD_IIR_H2_COEFF_A1_M                  (HWD_MXS_BASE_ADDR2 + 0x84)
#define HWD_JADE_RXSD_IIR_H2_COEFF_A2_M                  (HWD_MXS_BASE_ADDR2 + 0x88)
#define HWD_JADE_RXSD_IIR_H2_COEFF_B1_M                  (HWD_MXS_BASE_ADDR2 + 0x8C)
#define HWD_JADE_RXSD_IIR_H3_COEFF_A1_M                  (HWD_MXS_BASE_ADDR2 + 0x90)
#define HWD_JADE_RXSD_IIR_H3_COEFF_A2_M                  (HWD_MXS_BASE_ADDR2 + 0x94)
#define HWD_JADE_RXSD_IIR_H3_COEFF_B1_M                  (HWD_MXS_BASE_ADDR2 + 0x98)
#define HWD_JADE_RXSD_IIR_H4_COEFF_A1_M                  (HWD_MXS_BASE_ADDR2 + 0x9C)
#define HWD_JADE_RXSD_IIR_H4_COEFF_A2_M                  (HWD_MXS_BASE_ADDR2 + 0xA0)

#define HWD_JADE_RXSD_NCO_FREQ_OFFSET_LSB_M              (HWD_MXS_BASE_ADDR2 + 0xB0)
#define HWD_JADE_RXSD_NCO_FREQ_OFFSET_MSB_M              (HWD_MXS_BASE_ADDR2 + 0xB4)

#define HWD_JADE_RXSD_NBIF1_COEFF_A_REAL_LSB_M           (HWD_MXS_BASE_ADDR2 + 0xC0)
#define HWD_JADE_RXSD_NBIF1_COEFF_A_REAL_MSB_M           (HWD_MXS_BASE_ADDR2 + 0xC4)
#define HWD_JADE_RXSD_NBIF1_COEFF_A_IMAG_LSB_M           (HWD_MXS_BASE_ADDR2 + 0xC8)
#define HWD_JADE_RXSD_NBIF1_COEFF_A_IMAG_MSB_M           (HWD_MXS_BASE_ADDR2 + 0xCC)
#define HWD_JADE_RXSD_NBIF1_COEFF_B_LSB_M                (HWD_MXS_BASE_ADDR2 + 0xD0)
#define HWD_JADE_RXSD_NBIF1_COEFF_B_MSB_M                (HWD_MXS_BASE_ADDR2 + 0xD4)
#define HWD_JADE_RXSD_NBIF1_RSSI_MEAS_CTRL_M             (HWD_MXS_BASE_ADDR2 + 0xD8)
#define HWD_JADE_RXSD_NBIF1_IN_RSSI_LSB_M                (HWD_MXS_BASE_ADDR2 + 0xDC)
#define HWD_JADE_RXSD_NBIF1_IN_RSSI_MSB_M                (HWD_MXS_BASE_ADDR2 + 0xE0)
#define HWD_JADE_RXSD_NBIF1_BANDPASS_RSSI_LSB_M          (HWD_MXS_BASE_ADDR2 + 0xE4)
#define HWD_JADE_RXSD_NBIF1_BANDPASS_RSSI_MSB_M          (HWD_MXS_BASE_ADDR2 + 0xE8)
#define HWD_JADE_RXSD_NBIF1_OUT_RSSI_LSB_M               (HWD_MXS_BASE_ADDR2 + 0xEC)
#define HWD_JADE_RXSD_NBIF1_OUT_RSSI_MSB_M               (HWD_MXS_BASE_ADDR2 + 0xF0)

#define HWD_JADE_RXSD_NBIF2_COEFF_A_REAL_LSB_M           (HWD_MXS_BASE_ADDR2 + 0x100)
#define HWD_JADE_RXSD_NBIF2_COEFF_A_REAL_MSB_M           (HWD_MXS_BASE_ADDR2 + 0x104)
#define HWD_JADE_RXSD_NBIF2_COEFF_A_IMAG_LSB_M           (HWD_MXS_BASE_ADDR2 + 0x108)
#define HWD_JADE_RXSD_NBIF2_COEFF_A_IMAG_MSB_M           (HWD_MXS_BASE_ADDR2 + 0x10C)
#define HWD_JADE_RXSD_NBIF2_COEFF_B_LSB_M                (HWD_MXS_BASE_ADDR2 + 0x110)
#define HWD_JADE_RXSD_NBIF2_COEFF_B_MSB_M                (HWD_MXS_BASE_ADDR2 + 0x114)
#define HWD_JADE_RXSD_NBIF2_RSSI_MEAS_CTRL_M             (HWD_MXS_BASE_ADDR2 + 0x118)
#define HWD_JADE_RXSD_NBIF2_IN_RSSI_LSB_M                (HWD_MXS_BASE_ADDR2 + 0x11C)
#define HWD_JADE_RXSD_NBIF2_IN_RSSI_MSB_M                (HWD_MXS_BASE_ADDR2 + 0x120)
#define HWD_JADE_RXSD_NBIF2_BANDPASS_RSSI_LSB_M          (HWD_MXS_BASE_ADDR2 + 0x124)
#define HWD_JADE_RXSD_NBIF2_BANDPASS_RSSI_MSB_M          (HWD_MXS_BASE_ADDR2 + 0x128)
#define HWD_JADE_RXSD_NBIF2_OUT_RSSI_LSB_M               (HWD_MXS_BASE_ADDR2 + 0x12C)
#define HWD_JADE_RXSD_NBIF2_OUT_RSSI_MSB_M               (HWD_MXS_BASE_ADDR2 + 0x130)

#define HWD_JADE_RXSD_NBIF3_COEFF_A_REAL_LSB_M           (HWD_MXS_BASE_ADDR2 + 0x140)
#define HWD_JADE_RXSD_NBIF3_COEFF_A_REAL_MSB_M           (HWD_MXS_BASE_ADDR2 + 0x144)
#define HWD_JADE_RXSD_NBIF3_COEFF_A_IMAG_LSB_M           (HWD_MXS_BASE_ADDR2 + 0x148)
#define HWD_JADE_RXSD_NBIF3_COEFF_A_IMAG_MSB_M           (HWD_MXS_BASE_ADDR2 + 0x14C)
#define HWD_JADE_RXSD_NBIF3_COEFF_B_LSB_M                (HWD_MXS_BASE_ADDR2 + 0x150)
#define HWD_JADE_RXSD_NBIF3_COEFF_B_MSB_M                (HWD_MXS_BASE_ADDR2 + 0x154)
#define HWD_JADE_RXSD_NBIF3_RSSI_MEAS_CTRL_M             (HWD_MXS_BASE_ADDR2 + 0x158)
#define HWD_JADE_RXSD_NBIF3_IN_RSSI_LSB_M                (HWD_MXS_BASE_ADDR2 + 0x15C)
#define HWD_JADE_RXSD_NBIF3_IN_RSSI_MSB_M                (HWD_MXS_BASE_ADDR2 + 0x160)
#define HWD_JADE_RXSD_NBIF3_BANDPASS_RSSI_LSB_M          (HWD_MXS_BASE_ADDR2 + 0x164)
#define HWD_JADE_RXSD_NBIF3_BANDPASS_RSSI_MSB_M          (HWD_MXS_BASE_ADDR2 + 0x168)
#define HWD_JADE_RXSD_NBIF3_OUT_RSSI_LSB_M               (HWD_MXS_BASE_ADDR2 + 0x16C)
#define HWD_JADE_RXSD_NBIF3_OUT_RSSI_MSB_M               (HWD_MXS_BASE_ADDR2 + 0x170)


#define HWD_JADE_RXSD_COMP_FILT_COEFF11_D                (HWD_MXS_BASE_ADDR2 + 0x200)
#define HWD_JADE_RXSD_COMP_FILT_COEFF12_D                (HWD_MXS_BASE_ADDR2 + 0x204)
#define HWD_JADE_RXSD_COMP_FILT_COEFF13_D                (HWD_MXS_BASE_ADDR2 + 0x208)
#define HWD_JADE_RXSD_COMP_FILT_COEFF14_D                (HWD_MXS_BASE_ADDR2 + 0x20C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF15_D                (HWD_MXS_BASE_ADDR2 + 0x210)
#define HWD_JADE_RXSD_COMP_FILT_COEFF16_D                (HWD_MXS_BASE_ADDR2 + 0x214)
#define HWD_JADE_RXSD_COMP_FILT_COEFF17_D                (HWD_MXS_BASE_ADDR2 + 0x218)
#define HWD_JADE_RXSD_COMP_FILT_COEFF18_D                (HWD_MXS_BASE_ADDR2 + 0x21C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF19_D                (HWD_MXS_BASE_ADDR2 + 0x220)
#define HWD_JADE_RXSD_COMP_FILT_COEFF20_D                (HWD_MXS_BASE_ADDR2 + 0x224)
#define HWD_JADE_RXSD_COMP_FILT_COEFF21_D                (HWD_MXS_BASE_ADDR2 + 0x228)
#define HWD_JADE_RXSD_COMP_FILT_COEFF22_D                (HWD_MXS_BASE_ADDR2 + 0x22C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF23_D                (HWD_MXS_BASE_ADDR2 + 0x230)
#define HWD_JADE_RXSD_COMP_FILT_COEFF24_D                (HWD_MXS_BASE_ADDR2 + 0x234)
#define HWD_JADE_RXSD_COMP_FILT_COEFF25_D                (HWD_MXS_BASE_ADDR2 + 0x238)
#define HWD_JADE_RXSD_COMP_FILT_COEFF26_D                (HWD_MXS_BASE_ADDR2 + 0x23C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF27_D                (HWD_MXS_BASE_ADDR2 + 0x240)
#define HWD_JADE_RXSD_COMP_FILT_COEFF28_D                (HWD_MXS_BASE_ADDR2 + 0x244)
#define HWD_JADE_RXSD_COMP_FILT_COEFF29_D                (HWD_MXS_BASE_ADDR2 + 0x248)
#define HWD_JADE_RXSD_COMP_FILT_COEFF30_D                (HWD_MXS_BASE_ADDR2 + 0x24C)
#define HWD_JADE_RXSD_COMP_FILT_COEFF31_D                (HWD_MXS_BASE_ADDR2 + 0x250)
#define HWD_JADE_RXSD_COMP_FILT_COEFF32_D                (HWD_MXS_BASE_ADDR2 + 0x254)
#define HWD_JADE_RXSD_COMP_FILT_LEN_SEL_D                (HWD_MXS_BASE_ADDR2 + 0x258)

#define HWD_JADE_RXSD_IIR_H1_COEFF_A1_D                  (HWD_MXS_BASE_ADDR2 + 0x280)
#define HWD_JADE_RXSD_IIR_H2_COEFF_A1_D                  (HWD_MXS_BASE_ADDR2 + 0x284)
#define HWD_JADE_RXSD_IIR_H2_COEFF_A2_D                  (HWD_MXS_BASE_ADDR2 + 0x288)
#define HWD_JADE_RXSD_IIR_H2_COEFF_B1_D                  (HWD_MXS_BASE_ADDR2 + 0x28C)
#define HWD_JADE_RXSD_IIR_H3_COEFF_A1_D                  (HWD_MXS_BASE_ADDR2 + 0x290)
#define HWD_JADE_RXSD_IIR_H3_COEFF_A2_D                  (HWD_MXS_BASE_ADDR2 + 0x294)
#define HWD_JADE_RXSD_IIR_H3_COEFF_B1_D                  (HWD_MXS_BASE_ADDR2 + 0x298)
#define HWD_JADE_RXSD_IIR_H4_COEFF_A1_D                  (HWD_MXS_BASE_ADDR2 + 0x29C)
#define HWD_JADE_RXSD_IIR_H4_COEFF_A2_D                  (HWD_MXS_BASE_ADDR2 + 0x2A0)

#define HWD_JADE_RXSD_NCO_FREQ_OFFSET_LSB_D              (HWD_MXS_BASE_ADDR2 + 0x2B0)
#define HWD_JADE_RXSD_NCO_FREQ_OFFSET_MSB_D              (HWD_MXS_BASE_ADDR2 + 0x2B4)

#define HWD_JADE_RXSD_NBIF1_COEFF_A_REAL_LSB_D           (HWD_MXS_BASE_ADDR2 + 0x2C0)
#define HWD_JADE_RXSD_NBIF1_COEFF_A_REAL_MSB_D           (HWD_MXS_BASE_ADDR2 + 0x2C4)
#define HWD_JADE_RXSD_NBIF1_COEFF_A_IMAG_LSB_D           (HWD_MXS_BASE_ADDR2 + 0x2C8)
#define HWD_JADE_RXSD_NBIF1_COEFF_A_IMAG_MSB_D           (HWD_MXS_BASE_ADDR2 + 0x2CC)
#define HWD_JADE_RXSD_NBIF1_COEFF_B_LSB_D                (HWD_MXS_BASE_ADDR2 + 0x2D0)
#define HWD_JADE_RXSD_NBIF1_COEFF_B_MSB_D                (HWD_MXS_BASE_ADDR2 + 0x2D4)
#define HWD_JADE_RXSD_NBIF1_RSSI_MEAS_CTRL_D             (HWD_MXS_BASE_ADDR2 + 0x2D8)
#define HWD_JADE_RXSD_NBIF1_IN_RSSI_LSB_D                (HWD_MXS_BASE_ADDR2 + 0x2DC)
#define HWD_JADE_RXSD_NBIF1_IN_RSSI_MSB_D                (HWD_MXS_BASE_ADDR2 + 0x2E0)
#define HWD_JADE_RXSD_NBIF1_BANDPASS_RSSI_LSB_D          (HWD_MXS_BASE_ADDR2 + 0x2E4)
#define HWD_JADE_RXSD_NBIF1_BANDPASS_RSSI_MSB_D          (HWD_MXS_BASE_ADDR2 + 0x2E8)
#define HWD_JADE_RXSD_NBIF1_OUT_RSSI_LSB_D               (HWD_MXS_BASE_ADDR2 + 0x2EC)
#define HWD_JADE_RXSD_NBIF1_OUT_RSSI_MSB_D               (HWD_MXS_BASE_ADDR2 + 0x2F0)

#define HWD_JADE_RXSD_NBIF2_COEFF_A_REAL_LSB_D           (HWD_MXS_BASE_ADDR2 + 0x300)
#define HWD_JADE_RXSD_NBIF2_COEFF_A_REAL_MSB_D           (HWD_MXS_BASE_ADDR2 + 0x304)
#define HWD_JADE_RXSD_NBIF2_COEFF_A_IMAG_LSB_D           (HWD_MXS_BASE_ADDR2 + 0x308)
#define HWD_JADE_RXSD_NBIF2_COEFF_A_IMAG_MSB_D           (HWD_MXS_BASE_ADDR2 + 0x30C)
#define HWD_JADE_RXSD_NBIF2_COEFF_B_LSB_D                (HWD_MXS_BASE_ADDR2 + 0x310)
#define HWD_JADE_RXSD_NBIF2_COEFF_B_MSB_D                (HWD_MXS_BASE_ADDR2 + 0x314)
#define HWD_JADE_RXSD_NBIF2_RSSI_MEAS_CTRL_D             (HWD_MXS_BASE_ADDR2 + 0x318)
#define HWD_JADE_RXSD_NBIF2_IN_RSSI_LSB_D                (HWD_MXS_BASE_ADDR2 + 0x31C)
#define HWD_JADE_RXSD_NBIF2_IN_RSSI_MSB_D                (HWD_MXS_BASE_ADDR2 + 0x320)
#define HWD_JADE_RXSD_NBIF2_BANDPASS_RSSI_LSB_D          (HWD_MXS_BASE_ADDR2 + 0x324)
#define HWD_JADE_RXSD_NBIF2_BANDPASS_RSSI_MSB_D          (HWD_MXS_BASE_ADDR2 + 0x328)
#define HWD_JADE_RXSD_NBIF2_OUT_RSSI_LSB_D               (HWD_MXS_BASE_ADDR2 + 0x32C)
#define HWD_JADE_RXSD_NBIF2_OUT_RSSI_MSB_D               (HWD_MXS_BASE_ADDR2 + 0x330)

#define HWD_JADE_RXSD_NBIF3_COEFF_A_REAL_LSB_D           (HWD_MXS_BASE_ADDR2 + 0x340)
#define HWD_JADE_RXSD_NBIF3_COEFF_A_REAL_MSB_D           (HWD_MXS_BASE_ADDR2 + 0x344)
#define HWD_JADE_RXSD_NBIF3_COEFF_A_IMAG_LSB_D           (HWD_MXS_BASE_ADDR2 + 0x348)
#define HWD_JADE_RXSD_NBIF3_COEFF_A_IMAG_MSB_D           (HWD_MXS_BASE_ADDR2 + 0x34C)
#define HWD_JADE_RXSD_NBIF3_COEFF_B_LSB_D                (HWD_MXS_BASE_ADDR2 + 0x350)
#define HWD_JADE_RXSD_NBIF3_COEFF_B_MSB_D                (HWD_MXS_BASE_ADDR2 + 0x354)
#define HWD_JADE_RXSD_NBIF3_RSSI_MEAS_CTRL_D             (HWD_MXS_BASE_ADDR2 + 0x358)
#define HWD_JADE_RXSD_NBIF3_IN_RSSI_LSB_D                (HWD_MXS_BASE_ADDR2 + 0x35C)
#define HWD_JADE_RXSD_NBIF3_IN_RSSI_MSB_D                (HWD_MXS_BASE_ADDR2 + 0x360)
#define HWD_JADE_RXSD_NBIF3_BANDPASS_RSSI_LSB_D          (HWD_MXS_BASE_ADDR2 + 0x364)
#define HWD_JADE_RXSD_NBIF3_BANDPASS_RSSI_MSB_D          (HWD_MXS_BASE_ADDR2 + 0x368)
#define HWD_JADE_RXSD_NBIF3_OUT_RSSI_LSB_D               (HWD_MXS_BASE_ADDR2 + 0x36C)
#define HWD_JADE_RXSD_NBIF3_OUT_RSSI_MSB_D               (HWD_MXS_BASE_ADDR2 + 0x370)
#endif

/* Tx SD */
#define HWD_TXSD_OPER          (HWD_MXS_BASE_ADDR+0x010C)  /* R/W  [1:0], Tx offset cal. comparator PD       0x01   */
#define HWD_TXSD_CMVSEL        (HWD_MXS_BASE_ADDR+0x0118)  /* R/W  1 bit, Tx DAC common voltage src sel      0x00   */
#define HWD_TXSD_REGSEL        (HWD_MXS_BASE_ADDR+0x0128)  /* R/W  [1:0], Tx Shared RegSel                   0x00   */
#define HWD_TXSD_CNTEN         (HWD_MXS_BASE_ADDR+0x0140)  /* W    1 bit, Tx start txsd tune counter         N/A    */
#define HWD_TXSD_PWRONI        (HWD_MXS_BASE_ADDR+0x0144)  /* R/W  1 bit, Tx I-ch power on bit               0x00   */
#define HWD_TXSD_PWRONQ        (HWD_MXS_BASE_ADDR+0x0148)  /* R/W  1 bit, Tx Q-ch power on bit               0x00   */
#define HWD_TXSD_PWRSEL        (HWD_MXS_BASE_ADDR+0x014C)  /* R/W  1 bit, Tx SD PCG gating power select      0x00   */
#define HWD_TXSD_GDIQ          (HWD_MXS_BASE_ADDR+0x0150)  /* R/W  [7:0], Tx SD Trim control of I,Q chnls    0x00   */
#define HWD_TXSD_SCALE         (HWD_MXS_BASE_ADDR+0x0154)  /* R/W  [3:0], TX Sd gain scale                   0x00   */
#define HWD_TXSD_TUNE          (HWD_MXS_BASE_ADDR+0x0158)  /* R/W  [5:0], control tuning of RC cutoff freq.  0x00   */
#define HWD_TXSD_TUNECNT       (HWD_MXS_BASE_ADDR+0x015C)  /* R/W  [15:0], read back the TxSD tune counter   0x00   */
#define HWD_TXSD_OSC_CTRL      (HWD_MXS_BASE_ADDR+0x0160)  /* R/W  [2:0], Controls the OSC                   0x00   */
#define HWD_TXSD_GAIN_SEL      (HWD_MXS_BASE_ADDR+0x0164)  /* R/W  [1:0],  Tx gain select                    0x00   */

/* PDM */
#define HWD_PDM_BASE_ADDR      (HWD_MXS_BASE_ADDR+0x0000)  /* HWD_NO_BASE_ADDR = 0x00000000 */

#define HWD_PDM0_REGSEL        (HWD_PDM_BASE_ADDR+0x0240)  /* R/W  PDM0 shared regsel: 00=APB, 01=DM, 10=DV  0x00   */
#define HWD_PDM3_REGSEL        (HWD_PDM_BASE_ADDR+0x024C)  /* R/W  PDM3 shared regsel: 00=APB, 01=DM, 10=DV  0x00   */
#define HWD_PDM0_DOUT          (HWD_PDM_BASE_ADDR+0x020C)  /* R/W  [11:0] PDM0 data out register             0x00   */
#define HWD_PDM3_DOUT          (HWD_PDM_BASE_ADDR+0x0218)  /* R/W  [11:0] PDM3 data out register             0x00   */
#define HWD_PDM3_DIN           (HWD_PDM_BASE_ADDR+0x022c)  /* R/W  [11:0] PDM3 data in register              0x00   */
#define HWD_PDM3_DOUT_SECOND   (HWD_PDM_BASE_ADDR+0x03a4)  /* R/W  [11:0] PDM3 data for DO halfslot boundary 0x00   */

/* Reserved register */                                    
#define HWD_CF_RESERVED        (HWD_MXS_BASE_ADDR+0x0444)  /* R/W  Reserved register                         0x00   */ 
#define HWD_CF_BB_CTL          (HWD_MXS_BASE_ADDR+0x038c)  /* R/W  Reserved register                         0x00   */   
#define HWD_CF_TX_CTL          (HWD_MXS_BASE_ADDR+0x0390)  /* R/W  Reserved register                         0x00   */  
#define HWD_CF_GPS_STB_CFG     (HWD_MXS_BASE_ADDR+0x0394)  /* R/W  Reserved register                         0x00   */  
#define HWD_CF_GPS_STB_CTL     (HWD_MXS_BASE_ADDR+0x0398)  /* R/W  Reserved register                         0x00   */  
#define HWD_DO1X_TST_SEL       (HWD_MXS_BASE_ADDR+0x03b4)  /* R/W  Reserved register                         0x00   */ 


/* MixedSys register definition in Everest and after */
#if (SYS_ASIC == SA_MT6735)
#define HWD_MS_BASE_ADDR       (0x0B870000)
#define HWD_MS_C2KDIG_CON5     (HWD_MS_BASE_ADDR+0x0014)   /* R/W  APC DAC 1&2 enable selection */
#define HWD_MS_C2KDIG_CON20    (HWD_MS_BASE_ADDR+0x0080)   /* Rx ADC software enable */
#define HWD_MS_C2KDIG_CON24    (HWD_MS_BASE_ADDR+0x0090)   /* Tx DAC software enable */
#define HWD_MS_C2KDIG_CON61    (HWD_MS_BASE_ADDR+0x0184)   /* Sine wave clock control */
#define HWD_MS_C2KDIG_CON62    (HWD_MS_BASE_ADDR+0x0188)   /* Sine wave enable and fix value control */
#define HWD_MS_C2KDIG_CON63    (HWD_MS_BASE_ADDR+0x018C)   /* Sine wave output fix value of I path */
#define HWD_MS_C2KDIG_CON64    (HWD_MS_BASE_ADDR+0x0190)   /* Sine wave output fix value of Q path */
#define HWD_MS_C2KDIG_CON65    (HWD_MS_BASE_ADDR+0x0194)   /* Sine wave clock div and mode */
#define HWD_MS_C2KDIG_CON66    (HWD_MS_BASE_ADDR+0x0198)   /* Sine wave clock division parameter on I path*/
#define HWD_MS_C2KDIG_CON67    (HWD_MS_BASE_ADDR+0x019C)   /* Sine wave clock division parameter on Q path */
#define HWD_MS_C2KDIG_CON68    (HWD_MS_BASE_ADDR+0x01A0)   /* Sine wave gain. i.e. full scale voltage */
#define HWD_MS_C2KDIG_CON69    (HWD_MS_BASE_ADDR+0x01A4)   /* Sine wave gain mode */
#define HWD_MS_C2KDIG_CON6A    (HWD_MS_BASE_ADDR+0x01A8)   /* Sine wave frequency step on I path */
#define HWD_MS_C2KDIG_CON6B    (HWD_MS_BASE_ADDR+0x01AC)   /* Sine wave frequency step on Q path */
#define HWD_MS_C2KDIG_CON6C    (HWD_MS_BASE_ADDR+0x01B0)   /* Sine wave phase on I path */
#define HWD_MS_C2KDIG_CON6D    (HWD_MS_BASE_ADDR+0x01B4)   /* Sine wave phase on Q path */
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_MS_BASE_ADDR       (0x0B41A000)
#define HWD_MS_C2KDIG_CON5     (HWD_MS_BASE_ADDR+0x00C)   /* R/W  APC DAC 1&2 enable selection */
#endif

/* APC DAC special registers definition */
#define HWD_MS_APC_EN          HWD_PDM0_DOUT               /* Use PDM0_DATA_FIRST as APC DAC enable in EVEREST */
#define HWD_MS_APC_PRCSEL      HWD_PDM0_REGSEL             /* Use PDM0_PRCSEL as control processor seletion in EVEREST */
#define HWD_MS_APC_DATA_FIRST  HWD_PDM3_DOUT               /* APC data in first half slot */
#define HWD_MS_APC_DATA_SECOND HWD_PDM3_DOUT_SECOND        /* APC data in second half slot */
#define HWD_MS_APC_DATA_PRCSEL HWD_PDM3_REGSEL             /* APC data control processor selection */

#if (SYS_ASIC == SA_MT6735)
/* MixedSys TXDC CAL module relative registers */
#define HWD_MS_C2KDIG_CON40     (HWD_MS_BASE_ADDR+0x0100)  /* TXDC control in SW mode */
#define HWD_MS_C2KDIG_CON41     (HWD_MS_BASE_ADDR+0x0104)  /* TXDC I path data cfg in SW mode */
#define HWD_MS_C2KDIG_CON42     (HWD_MS_BASE_ADDR+0x0108)  /* TXDC Q path data cfg in SW mode */
#define HWD_MS_C2KDIG_CON7E     (HWD_MS_BASE_ADDR+0x01F8)  /* TXDET CMP FSM Timing control */
#define HWD_MS_C2KDIG_CON7F     (HWD_MS_BASE_ADDR+0x01FC)  /* TXDET ADJ/WARIT FSM Timing control */
#define HWD_MS_C2KDIG_CON8A     (HWD_MS_BASE_ADDR+0x0228)  /* TXDC Q path value for SW read out*/
#define HWD_MS_C2KDIG_CON8B     (HWD_MS_BASE_ADDR+0x022C)  /* TXDC I path value for SW read out*/
#define HWD_MS_C2KDIG_CON90     (HWD_MS_BASE_ADDR+0x0240)
#define HWD_MS_C2KDIG_CON91     (HWD_MS_BASE_ADDR+0x0244)
#define HWD_MS_C2KDIG_CON93     (HWD_MS_BASE_ADDR+0x024C)
#define HWD_MS_C2KDIG_CON94     (HWD_MS_BASE_ADDR+0x0250)
#define HWD_MS_C2KDIG_CON95     (HWD_MS_BASE_ADDR+0x0254)
#define HWD_MS_C2KDIG_CON96     (HWD_MS_BASE_ADDR+0x0258)
#endif

/* BBTX registers */
#define HWD_MS_C2KBBTX_CON3     (HWD_MS_BASE_ADDR+0x070c)
#define HWD_MS_C2KBBTX_CONA     (HWD_MS_BASE_ADDR+0x0728)

/* BBRX registers */
#define HWD_MS_C2KBBRX_CON19    (HWD_MS_BASE_ADDR+0x0A64)
#define HWD_MS_C2KBBRX_CON20    (HWD_MS_BASE_ADDR+0x0A80)
#define HWD_MS_C2KBBRX_CON21    (HWD_MS_BASE_ADDR+0x0A84)
#define HWD_MS_C2KBBRX_CON39    (HWD_MS_BASE_ADDR+0x0AE4)
#define HWD_MS_C2KBBRX_CON40    (HWD_MS_BASE_ADDR+0x0B00)
#define HWD_MS_C2KBBRX_CON41    (HWD_MS_BASE_ADDR+0x0B04)
#define HWD_MS_C2KBBRX_CON61    (HWD_MS_BASE_ADDR+0x0B84)
#define HWD_MS_C2KBBRX_CON62    (HWD_MS_BASE_ADDR+0x0B88)
#define HWD_MS_C2KBBRX_CON63    (HWD_MS_BASE_ADDR+0x0B8C)
#define HWD_MS_C2KBBRX_CON71    (HWD_MS_BASE_ADDR+0x0BC4)
#define HWD_MS_C2KBBRX_CON72    (HWD_MS_BASE_ADDR+0x0BC8)
#define HWD_MS_C2KBBRX_CON73    (HWD_MS_BASE_ADDR+0x0BCC)

#define HWD_MXS_TX_TRIG_1XDO_MODE       (HWD_MXS_BASE_ADDR + 0x0400)
#define HWD_MXS_TX_IMMED_TRIG           (HWD_MXS_BASE_ADDR + 0x0404)
#define HWD_MXS_TXCAL_CNT_INIT          (HWD_MXS_BASE_ADDR + 0x0408)

/** HWD_MXS_TX_TRIG_1XDO_MODE register*/
#define HWD_MXS_TX_TRIG_1XDO_MODE_1X   (0)
#define HWD_MXS_TX_TRIG_1XDO_MODE_DO   (1)

/** HWD_MXS_TX_IMMED_TRIG register*/
#define HWD_MXS_TX_IMMED_TRIG_GBB0_OL   (1 << 0)
#define HWD_MXS_TX_IMMED_TRIG_GBB1      (1 << 1)
#define HWD_MXS_TX_IMMED_TRIG_TXCAL     (1 << 2)

#if (SYS_ASIC >= SA_MT6755)
#define HWD_MXS_TOPSM_ADDR                (HWD_NO_BASE_ADDR+0x0B41B000)
#define MTK_HWD_SM_PWR_CON0               (HWD_MXS_TOPSM_ADDR+0x0000) 
#define MTK_HWD_SM_PWR_ON_SW_CTRL0        (HWD_MXS_TOPSM_ADDR+0x0080) 
#define MTK_HWD_SM_PWR_OFF_SW_CTRL0       (HWD_MXS_TOPSM_ADDR+0x00A0) 
#define MTK_HWD_SM_PWR_PER0               (HWD_MXS_TOPSM_ADDR+0x00C0) 
#define MTK_HWD_SM_PWR_PER1               (HWD_MXS_TOPSM_ADDR+0x00C4)
#define MTK_HWD_SM_PWR_SWCTRLSEL          (HWD_MXS_TOPSM_ADDR+0x00CC)
#define MTK_HWD_SM_PWR_RDY_REG            (HWD_MXS_TOPSM_ADDR+0x00D0)
#define MTK_HWD_SM_PWR_RDY                (HWD_MXS_TOPSM_ADDR+0x00D4)
#define MTK_HWD_SM_TMR_REQ_MASK           (HWD_MXS_TOPSM_ADDR+0x0100)
#define MTK_HWD_SM_TMR_SYSCLK_MASK        (HWD_MXS_TOPSM_ADDR+0x0108)
#define MTK_HWD_SM_TMR_PLL_MASK0          (HWD_MXS_TOPSM_ADDR+0x0120)
#define MTK_HWD_SM_TMR_PWR_MASK0          (HWD_MXS_TOPSM_ADDR+0x0140)
#define MTK_HWD_SM_TMR_MAS_TRIG_MASK      (HWD_MXS_TOPSM_ADDR+0x0160)
#define MTK_HWD_SM_TMR_TIMER_TRIG_MASK    (HWD_MXS_TOPSM_ADDR+0x0170)
#define MTK_HWD_SM_TMR_CLIENT_ACT_MASK    (HWD_MXS_TOPSM_ADDR+0x0180)
#define MTK_HWD_SM_TMR_SSTA0          	  (HWD_MXS_TOPSM_ADDR+0x01A0)
#define MTK_HWD_SM_SLV_REQ_MASK           (HWD_MXS_TOPSM_ADDR+0x0200)
#define MTK_HWD_SM_SLV_SYSCLK_MASK        (HWD_MXS_TOPSM_ADDR+0x0208)
#define MTK_HWD_SM_SLV_PLL_MASK0          (HWD_MXS_TOPSM_ADDR+0x0220)
#define MTK_HWD_SM_SLV_PWR_MASK0          (HWD_MXS_TOPSM_ADDR+0x0240)
#define MTK_HWD_SM_SLV_MAS_TRIG_MASK      (HWD_MXS_TOPSM_ADDR+0x0260)
#define MTK_HWD_SM_SLV_TIMER_TRIG_MASK    (HWD_MXS_TOPSM_ADDR+0x0270)
#define MTK_HWD_SM_SLV_CLIENT_ACT_MASK    (HWD_MXS_TOPSM_ADDR+0x0280)
#define MTK_HWD_SM_DBG_REQ_MASK           (HWD_MXS_TOPSM_ADDR+0x0300)
#define MTK_HWD_SM_DBG_SYSCLK_MASK        (HWD_MXS_TOPSM_ADDR+0x0308)
#define MTK_HWD_SM_DBG_PLL_MASK0          (HWD_MXS_TOPSM_ADDR+0x0320)
#define MTK_HWD_SM_DBG_PWR_MASK0          (HWD_MXS_TOPSM_ADDR+0x0340)
#define MTK_HWD_SM_DBG_MAS_TRIG_MASK0     (HWD_MXS_TOPSM_ADDR+0x0360)
#define MTK_HWD_SM_DBG_TIMER_TRIG_MASK    (HWD_MXS_TOPSM_ADDR+0x0370)
#define MTK_HWD_SM_DBG_CLIENT_ACT_MASK    (HWD_MXS_TOPSM_ADDR+0x0380)

#define MTK_HWD_SM_CLK_SETTLE             (HWD_MXS_TOPSM_ADDR+0x0400)
#define MTK_HWD_SM_TIMER_TRIG_SETTLE      (HWD_MXS_TOPSM_ADDR+0x0410)
#define MTK_HWD_SM_MAS_TRIG_MAX_SETTLE    (HWD_MXS_TOPSM_ADDR+0x0418)
#define MTK_HWD_SM_MAS_TRIG_GRP_SETTLE    (HWD_MXS_TOPSM_ADDR+0x0420)
#define MTK_HWD_SM_MAS_TRIG_GRP_SAL       (HWD_MXS_TOPSM_ADDR+0x0430)
#define MTK_HWD_SM_MAS_TRIG_SEL           (HWD_MXS_TOPSM_ADDR+0x0440)
#define MTK_HWD_SM_SLV_SW_TRIG            (HWD_MXS_TOPSM_ADDR+0x0450)
#define MTK_HWD_SM_DBG_SW_TRIG            (HWD_MXS_TOPSM_ADDR+0x0458)
#endif

/* HWD_CF_TX_CTL register */
#define HWD_AUX_TX_CLK_INV     (0x01<<3)
#define HWD_MAIN_TX_CLK_INV    (0x01<<2)
#define HWD_AUX_TX_DO1X_SEL    (0x01<<1)
#define HWD_MAIN_TX_DO1X_SEL   (0x01<<0)

/* === Generic DMA Register Map Begin === */
#define HWD_GENDMA_BASE           (HWD_NO_BASE_ADDR+0x0b440000)  /* base address for Generic DMA */

#define HWD_GENDMA_SRCADDR_CH0    (HWD_GENDMA_BASE + 0x0000) /* Source Address for DMA channel 0 */
#define HWD_GENDMA_DSTADDR_CH0    (HWD_GENDMA_BASE + 0x0004) /* Destination address for DMA channel 0 */
#define HWD_GENDMA_TRANSFER_CH0   (HWD_GENDMA_BASE + 0x0008) /* Transfer Counter register for DMA channel 0 */
#define HWD_GENDMA_CONFIG_CH0     (HWD_GENDMA_BASE + 0x000c) /* Configuration register for DMA Channel 0 */
#define HWD_GENDMA_CONTROL_CH0    (HWD_GENDMA_BASE + 0x0080) /* 8b Control Register for DMA channel 0 */
#define HWD_GENDMA_STATUS_CH0     (HWD_GENDMA_BASE + 0x0088) /* 8b Status Register for DMA channel 0 */

#define HWD_GENDMA_SRCADDR_CH1    (HWD_GENDMA_BASE + 0x0010) /* Source Address for DMA channel 1 */
#define HWD_GENDMA_DSTADDR_CH1    (HWD_GENDMA_BASE + 0x0014) /* Destination address for DMA channel 1 */
#define HWD_GENDMA_TRANSFER_CH1   (HWD_GENDMA_BASE + 0x0018) /* 3Transfer Counter register for DMA channel 1 */
#define HWD_GENDMA_CONFIG_CH1     (HWD_GENDMA_BASE + 0x001c) /* Configuration register for DMA Channel 1 */
#define HWD_GENDMA_CONTROL_CH1    (HWD_GENDMA_BASE + 0x0081) /* 8b Control Register for DMA channel 1 */
#define HWD_GENDMA_STATUS_CH1     (HWD_GENDMA_BASE + 0x0089) /* 8b Status Register for DMA channel 1 */

#define HWD_GENDMA_SRCADDR_CH2    (HWD_GENDMA_BASE + 0x0020) /* Source Address for DMA channel 2 */
#define HWD_GENDMA_DSTADDR_CH2    (HWD_GENDMA_BASE + 0x0024) /* Destination address for DMA channel 2 */
#define HWD_GENDMA_TRANSFER_CH2   (HWD_GENDMA_BASE + 0x0028) /* Transfer Counter register for DMA channel 2 */
#define HWD_GENDMA_CONFIG_CH2     (HWD_GENDMA_BASE + 0x002c) /* Configuration register for DMA Channel 2 */
#define HWD_GENDMA_CONTROL_CH2    (HWD_GENDMA_BASE + 0x0082) /* 8b Control Register for DMA channel 2 */
#define HWD_GENDMA_STATUS_CH2     (HWD_GENDMA_BASE + 0x008a) /* 8b Status Register for DMA channel 2 */

#define HWD_GENDMA_SRCADDR_CH3    (HWD_GENDMA_BASE + 0x0030) /* Source Address for DMA channel 3 */
#define HWD_GENDMA_DSTADDR_CH3    (HWD_GENDMA_BASE + 0x0034) /* Destination address for DMA channel 3 */
#define HWD_GENDMA_TRANSFER_CH3   (HWD_GENDMA_BASE + 0x0038) /* Transfer Counter register for DMA channel 3 */
#define HWD_GENDMA_CONFIG_CH3     (HWD_GENDMA_BASE + 0x003c) /* Configuration register for DMA Channel 3 */
#define HWD_GENDMA_CONTROL_CH3    (HWD_GENDMA_BASE + 0x0083) /* 8b Control Register for DMA channel 3 */
#define HWD_GENDMA_STATUS_CH3     (HWD_GENDMA_BASE + 0x008b) /* 8b Status Register for DMA channel 3 */

#define HWD_GENDMA_SRCADDR_CH4    (HWD_GENDMA_BASE + 0x0040) /* Source Address for DMA channel 4 */
#define HWD_GENDMA_DSTADDR_CH4    (HWD_GENDMA_BASE + 0x0044) /* Destination address for DMA channel 4 */
#define HWD_GENDMA_TRANSFER_CH4   (HWD_GENDMA_BASE + 0x0048) /* Transfer Counter register for DMA channel 4 */
#define HWD_GENDMA_CONFIG_CH4     (HWD_GENDMA_BASE + 0x004c) /* Configuration register for DMA Channel 4 */
#define HWD_GENDMA_CONTROL_CH4    (HWD_GENDMA_BASE + 0x0084) /* 8b Control Register for DMA channel 4 */
#define HWD_GENDMA_STATUS_CH4     (HWD_GENDMA_BASE + 0x008c) /* 8b Status Register for DMA channel 4 */

#define HWD_GENDMA_SRCADDR_CH5    (HWD_GENDMA_BASE + 0x0050) /* Source Address for DMA channel 5 */
#define HWD_GENDMA_DSTADDR_CH5    (HWD_GENDMA_BASE + 0x0054) /* Destination address for DMA channel 5 */
#define HWD_GENDMA_TRANSFER_CH5   (HWD_GENDMA_BASE + 0x0058) /* Transfer Counter register for DMA channel 5 */
#define HWD_GENDMA_CONFIG_CH5     (HWD_GENDMA_BASE + 0x005c) /* Configuration register for DMA Channel 5 */
#define HWD_GENDMA_CONTROL_CH5    (HWD_GENDMA_BASE + 0x0085) /* 8b Control Register for DMA channel 5 */
#define HWD_GENDMA_STATUS_CH5     (HWD_GENDMA_BASE + 0x008d) /* 8b Status Register for DMA channel 5 */

#define HWD_GENDMA_SRCADDR_CH6    (HWD_GENDMA_BASE + 0x0060) /* Source Address for DMA channel 6 */
#define HWD_GENDMA_DSTADDR_CH6    (HWD_GENDMA_BASE + 0x0064) /* Destination address for DMA channel 6 */
#define HWD_GENDMA_TRANSFER_CH6   (HWD_GENDMA_BASE + 0x0068) /* Transfer Counter register for DMA channel 6 */
#define HWD_GENDMA_CONFIG_CH6     (HWD_GENDMA_BASE + 0x006c) /* Configuration register for DMA Channel 6 */
#define HWD_GENDMA_CONTROL_CH6    (HWD_GENDMA_BASE + 0x0086) /* 8b Control Register for DMA channel 6 */ 
#define HWD_GENDMA_STATUS_CH6     (HWD_GENDMA_BASE + 0x008e) /* 8b Status Register for DMA channel 6 */

#define HWD_GENDMA_SRCADDR_CH7    (HWD_GENDMA_BASE + 0x0070) /* Source Address for DMA channel 7 */
#define HWD_GENDMA_DSTADDR_CH7    (HWD_GENDMA_BASE + 0x0074) /* Destination address for DMA channel 7 */
#define HWD_GENDMA_TRANSFER_CH7   (HWD_GENDMA_BASE + 0x0078) /* Transfer Counter register for DMA channel 7 */
#define HWD_GENDMA_CONFIG_CH7     (HWD_GENDMA_BASE + 0x007c) /* Configuration register for DMA Channel 7 */
#define HWD_GENDMA_CONTROL_CH7    (HWD_GENDMA_BASE + 0x0087) /* 8b Control Register for DMA channel 7 */  
#define HWD_GENDMA_STATUS_CH7     (HWD_GENDMA_BASE + 0x008f) /* 8b Status Register for DMA channel 7 */

  
#define HWD_GENDMA_GLOB_CTRL_REG  (HWD_GENDMA_BASE + 0x90) /* DMA Global Control Register */
#define HWD_GENDMA_GLOB_STAT_REG  (HWD_GENDMA_BASE + 0x94) /* DMA Global Status Register */
#define HWD_GENDMA_GLOB_TEST_REG  (HWD_GENDMA_BASE + 0x98)   /* 8b Status Register for DMA channel 7 */
#define HWD_GENDMA_TX_REQ30       (HWD_GENDMA_BASE + 0x10230)/* DMA HW REQ Control channels 0 -3*/
#define HWD_GENDMA_TX_REQ74       (HWD_GENDMA_BASE + 0x10234)/* DMA HW REQ Control channels 4 -7*/

/* generic DMA Channel selected BitFields defined */
#define HWD_GENDMA_CONFIG_CHAN_TC_INTR          0x00100000  /* channel enable intr on TC */
#define HWD_GENDMA_CONFIG_CHAN_ABORT_INTR       0x00200000  /* channel enable intr on abort */
#define HWD_GENDMA_CONFIG_CHAN_ERROR_INTR       0x00400000  /* channel enable intr on error */
#define HWD_GENDMA_CONTROL_CHAN_ENABLE          0x01        /* channel enable */
#define HWD_GENDMA_ERRORS_AND_ABORTS            0x1e        /* ERROR-READ/WRITE ABORT-READ/WRITE mask */

/* HWD_GENDMA_GLOB_CTRL_REG - Global Control Register, bit fields */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA       0x0100      /* flag to enable device to assert interrupt signal */
#define HWD_GENDMA_GLOB_CTRL_REG_ENABLE         0x0001      /* DMA device enable: 0-disable  1-enable */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA0      0x010000    /* flag to enable device to assert interrupt signal ch0 */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA1      0x020000    /* flag to enable device to assert interrupt signal ch1 */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA2      0x040000    /* flag to enable device to assert interrupt signal ch2 */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA3      0x080000    /* flag to enable device to assert interrupt signal ch3 */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA4      0x100000    /* flag to enable device to assert interrupt signal ch4 */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA5      0x200000    /* flag to enable device to assert interrupt signal ch5 */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA6      0x400000    /* flag to enable device to assert interrupt signal ch6 */
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA7      0x800000    /* flag to enable device to assert interrupt signal ch7 */

#ifdef SYS_OPTION_EVDO
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA_ALL    (HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA0 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA1 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA2 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA3 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA4 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA5 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA6 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA7 )
#else
#define HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA_ALL    (HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA0 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA1 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA2 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA3 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA4 | \
                                                  HWD_GENDMA_GLOB_CTRL_REG_INTR_ENA5 )
#endif

/* HWD_GENDMA_GLOB_TEST_REG - Global Control Register, bit fields */
#define HWD_GENDMA_GTR_CH_SEL_SHIFT     0x10  /* Amount to shift channel mask. */
#define HWD_GENDMA_GTR_CH_BLK(_mask_) ((_mask_) << HWD_GENDMA_GTR_CH_SEL_SHIFT)
#define HWD_GENDMA_GTR_AHB_SEL_SHIFT   0x8  /* Amount to shift AHB selection mask. */
#define HWD_GENDMA_GTR_AHB_BLK(_mask_) ((_mask_) << HWD_GENDMA_GTR_AHB_SEL_SHIFT)
#define HWD_GENDMA_GTR_TXFIFO_LVL_25   0x00
#define HWD_GENDMA_GTR_TXFIFO_LVL_50   0x40
#define HWD_GENDMA_GTR_TXFIFO_LVL_75   0x80
#define HWD_GENDMA_GTR_TXFIFO_LVL_100   0xc0
#define HWD_GENDMA_GTR_DMA_BLK_MODE_AUTO  0x00
#define HWD_GENDMA_GTR_DMA_BLK_MODE_MAN 0x20
#define HWD_GENDMA_GTR_AHB_BLK_MODE_AUTO  0x10
#define HWD_GENDMA_GTR_AHB_BLK_MODE_MAN  0x00
#define HWD_GENDMA_GTR_TSTBUS_OUT_SEL  0x08
#define HWD_GENDMA_GTR_DMA_SMODS_NORMAL 0x7


/* === Generic DMA Register Map End === */

/*************************
* DO MDM control    *
**************************/
/* For CD no memory translation necessary between DMA and CP */
#define HWD_DMA_MDM_BUFF               0x00006000      /* BB7 dma mdm test buffer address 4KB 0xc0006000~0xc0006fff */
#define HWD_DMA_MDM_BUFF_MDM_OFFSET    0x00000000      /* BB7 dma mdm test buffer address for refrering by mdm */
#define HWD_DMA_MDM_BUFF_MDM_MASK_H    0x00000000      /* Mask to get 32 bit addr for CP access.  */
#define HWD_DMA_MDM_BUFF_MDM_MASK      0xffffffff      /* Mask to get lower 24 bits */
#define HWD_DMA_MDM_BASE               0x0b450000      /*  dma mdm base address */

#define HWD_DMA_TX_OUT_FIFO            0x0b470000   /* TXDMA OUTPUT FIFO base */

#define HWD_DMA_TCH_TEST_BUFF_ADDRESS  (HWD_DMA_MDM_BUFF+0x800)      /* BB7 tch test input buffer address 2KB 0xc0008800~0xc0008fff */

#define HWD_DMA_GENERIC_BASE        HWD_GENDMA_BASE      /* dma base address */

#define HWD_DMA_GLOBAL_TEST (HWD_DMA_GENERIC_BASE+0x0098)

#define HWD_DMA_TX_ADDR0    (HWD_DMA_MDM_BASE + 0x0200)   
#define HWD_DMA_TX_ADDR1    (HWD_DMA_MDM_BASE + 0x0204)   
#define HWD_DMA_TX_CTRL     (HWD_DMA_MDM_BASE + 0x0210)   
#define HWD_DMA_TX_STAT     (HWD_DMA_MDM_BASE + 0x0214)   
#define HWD_DMA_TX_LLD0     (HWD_DMA_MDM_BASE + 0x0220)   
#define HWD_DMA_TX_LLD1     (HWD_DMA_MDM_BASE + 0x0224)   
#define HWD_DMA_TX_LLD2     (HWD_DMA_MDM_BASE + 0x0228)   
#define HWD_DMA_TX_LLD3     (HWD_DMA_MDM_BASE + 0x022C)   

/* HWD_DMA_TX_CTRL */
#define TXHA_DMA_TX_ACK_SHIFT           0     /* [1:0]   */
#define TXHA_DMA_TX_ACK                 ((uint32)1 << TXHA_DMA_TX_ACK_SHIFT)        
#define TXHA_DMA_TX_NAK                 ((uint32)2 << TXHA_DMA_TX_ACK_SHIFT)        
#define TXHA_DMA_TX_ACK_MASK            ((uint32)3 << TXHA_DMA_TX_ACK_SHIFT)       
#define TXHA_DMA_TX_HW_ACK_SHIFT        4     /* [4]   */
#define TXHA_DMA_TX_HW_ACK_MASK         ((uint32)1 << TXHA_DMA_TX_HW_ACK_SHIFT)            
#define TXHA_DMA_TX_CHANNEL_SHIFT       8     /* [10:8]   */
#define TXHA_DMA_TX_CHANNEL_MASK        ((uint32)7 << TXHA_DMA_TX_CHANNEL_SHIFT)        
#define TXHA_DMA_TX_CHANNEL_0           ((uint32)0 << TXHA_DMA_TX_CHANNEL_SHIFT)        
#define TXHA_DMA_TX_CHANNEL_1           ((uint32)1 << TXHA_DMA_TX_CHANNEL_SHIFT)        
#define TXHA_DMA_TX_CHANNEL_7           ((uint32)7 << TXHA_DMA_TX_CHANNEL_SHIFT)        
#define TXHA_DMA_TX_NAK_ENABLE_SHIFT    12     /* [12]   */
#define TXHA_DMA_TX_NAK_ENABLE          ((uint32)1 << TXHA_DMA_TX_NAK_ENABLE_SHIFT)        
#define TXHA_DMA_TX_ENABLE_SHIFT        16     /* [16]   */
#define TXHA_DMA_TX_ENABLE              ((uint32)1 << TXHA_DMA_TX_ENABLE_SHIFT)        
#define TXHA_DMA_TX_RESET_SHIFT         24     /* [24]   */
#define TXHA_DMA_TX_RESET_MASK          ((uint32)1 << TXHA_DMA_TX_RESET_SHIFT)        
#define TXHA_DMA_TX_RESET               ((uint32)0 << TXHA_DMA_TX_RESET_SHIFT)        
#define TXHA_DMA_TX_FIFO_RESET_SHIFT    25     /* [25]   */
#define TXHA_DMA_TX_FIFO_RESET_MASK     ((uint32)1 << TXHA_DMA_TX_FIFO_RESET_SHIFT)        
#define TXHA_DMA_TX_FIFO_RESET          ((uint32)0 << TXHA_DMA_TX_FIFO_RESET_SHIFT)        

/*************************
* DO DRC control    *
**************************/

#define HWD_DRC_BASE                 (HWD_NO_BASE_ADDR+0x0b7c0000)  /* HWD_NO_BASE_ADDR = 0x00000000 */   

#define HWD_DRC_LEN                  (HWD_DRC_BASE+0x0500) /*[1:0] */
#define HWD_DRC_IIRPOLE              (HWD_DRC_BASE+0x0504) /* [ 2:0] */
#define HWD_DRC_ERRIIRPOLE           (HWD_DRC_BASE+0x0508) /* [2:0] */
#define HWD_DRC_SLMSMU               (HWD_DRC_BASE+0x050C) /* [3:0] */
#define HWD_DRC_FIXEDRXVAL           (HWD_DRC_BASE+0x0510) /* [3:0] */
#define HWD_DRC_FIXEDTXVAL           (HWD_DRC_BASE+0x0514) /* [3:0] */
#define HWD_DRC_LVCROSSLEN           (HWD_DRC_BASE+0x0518) /* [3:0] */
#define HWD_DRC_CONTRL               (HWD_DRC_BASE+0x051C) /* [6:0] */
#define HWD_DRC_THRBYPASS1           (HWD_DRC_BASE+0x0520) /* [7:0] */
#define HWD_DRC_THRBYPASS2           (HWD_DRC_BASE+0x0524) /* [7:0] */
#define HWD_DRC_LVCROSS              (HWD_DRC_BASE+0x0528) /* [7:0] */
#define HWD_DRC_GLOBALADJ            (HWD_DRC_BASE+0x052C) /* [11:0] */
#define HWD_DRC_SLMSERR              (HWD_DRC_BASE+0x0530) /* [15:0] */
#define HWD_DRC_ENDTIME              (HWD_DRC_BASE+0x0534) /* [15:0] */
#define HWD_DRC_SLMSSTEP             (HWD_DRC_BASE+0x0538) /* [15:0] */
#define HWD_DRC_C2ISHORT             (HWD_DRC_BASE+0x053C) /* [15:0] */
#define HWD_DRC_C2ILONG              (HWD_DRC_BASE+0x0540) /* [15:0] */
#define HWD_DRC_SLMSCOEFF_M_START    (HWD_DRC_BASE+0x0544) /* [15:0] , 0x0B8A0544 ~0x0B8A0554   */
#define HWD_DRC_OFFSET_M_START       (HWD_DRC_BASE+0x0558) /* [3:0] , 0x0B8A0558~0x0B8A0590*/
#define HWD_DRC_C2ITHR_M_START       (HWD_DRC_BASE+0x0594) /* [11:0] , 0x0B8A0594~0x0B8A05C8*/
#define HWD_DRC_THRPUT_M_START       (HWD_DRC_BASE+0x05CC) /* [11:0] , 0x0B8A05CC~0x0B8A0600*/
#define HWD_DRC_THRAWGN_M_START      (HWD_DRC_BASE+0x0604) /* [11:0] , 0x0B8A0604~0x0B8A0638*/
#define HWD_DRC_C2IVAL_M_START       (HWD_DRC_BASE+0x063C) /* [15:0] , 0x0B8A063C~0x0B8A0650*/ 
#define HWD_DRC_C2IMAX               (HWD_DRC_BASE+0x0654)
#define HWD_DRC_C2IMIN               (HWD_DRC_BASE+0x0658)
#define HWD_Load_slmscoeff           (HWD_DRC_BASE+0x065C)
#define HWD_DRC_VALUE                (HWD_DRC_BASE+0x0660)
#define HWD_DRC_HALF_SLOT_CNT_INIT   (HWD_DRC_BASE+0x0664)
#define HWD_DRC_DRC4MCD              (HWD_DRC_BASE+0x0668)
#define HWD_DRC_FN_DELAY             (HWD_DRC_BASE+0x066c)  /* R/W, [3:0] */
#define HWD_DRC_PREDICTED_C2IDB      (HWD_DRC_BASE+0x0670)
#define HWD_DRC_HALF_SLOT_CNT        (HWD_DRC_BASE+0x0674)/* [16:0]*/ 
#define HWD_DRC_CELLSW_STATUS        (HWD_DRC_BASE+0x0678)  /* R, [0]: FN_DELAY_DONE; [1]:C2I_SEL; [2]:FN_DELAY_START; */
#define HWD_DRC_FN_DELAY_CNT         (HWD_DRC_BASE+0x067c)  /* R, [3:0] */
#define HWD_DRC_C2I                  (HWD_DRC_BASE+0x0680)  /* R, [15:0] */
#define HWD_DRC_DEBUG                (HWD_DRC_BASE+0x0684)  /* Drc_debug=0, only fn_c2i can feed to DRC. */
#define HWD_DRC_TABLE_ASK            (HWD_DRC_BASE+0x0688)
#define HWD_DRC_TABLE_RES            (HWD_DRC_BASE+0x068C)
#define HWD_DRC_IIR_RST              (HWD_DRC_BASE+0x0690)
#define HWD_DRC_SUPER_EN             (HWD_DRC_BASE+0x0694)
#define HWD_DRC_ZERO_CNT_RST         (HWD_DRC_BASE+0x0698)
#define HWD_DRC_NONZERO_CNT_RST      (HWD_DRC_BASE+0x069C)
#define HWD_DRC_ZERO_CNT             (HWD_DRC_BASE+0x06A0)
#define HWD_DRC_NONZERO_CNT          (HWD_DRC_BASE+0x06A4)
#define HWD_DRC_TENTATIVE            (HWD_DRC_BASE+0x06A8)
#define HWD_DRC_ZERO_THR             (HWD_DRC_BASE+0x06AC)
#define HWD_DRC_NONZERO_THR          (HWD_DRC_BASE+0x06B0)
#define HWD_DRC_LONGTERM_INI         (HWD_DRC_BASE+0x06B4)
#define HWD_DRC_SHORTTERM_INI        (HWD_DRC_BASE+0x06B8)
#define HWD_DRC_LEVELCROSS_INI       (HWD_DRC_BASE+0x06BC)
#define HWD_DRC_SFT_RSTN             (HWD_DRC_BASE+0x06C0)


/*************************
* END OF DRC control    *
**************************/

/*************************
* DO preamble control    *
**************************/
#define HWD_PMB_BASE            (HWD_NO_BASE_ADDR+0x0b7f0000)  /* HWD_NO_BASE_ADDR = 0x00000000 */   

#define HWD_PMB_IIR_PARAM       (HWD_PMB_BASE + 0x0000) 
#define HWD_PMB_TH_LIM_ACQ      (HWD_PMB_BASE + 0x0004) 
#define HWD_PMB_ADJ_LEN_SLP     (HWD_PMB_BASE + 0x0008) 
#define HWD_PMB_ADJ_LEN_OFFS    (HWD_PMB_BASE + 0x000c) 
#define HWD_PMB_ADJ_LEN6_SLP    (HWD_PMB_BASE + 0x0010) 
#define HWD_PMB_ADJ_LEN6_OFFS   (HWD_PMB_BASE + 0x0014) 
#define HWD_PMB_SNRBKOFF        (HWD_PMB_BASE + 0x0018) 
#define HWD_PMB_ADJ_MU          (HWD_PMB_BASE + 0x001c) 
#define HWD_PMB_DIFF_L          (HWD_PMB_BASE + 0x0020) 
#define HWD_PMB_DIFF_H          (HWD_PMB_BASE + 0x0024) 
#define HWD_PMB_CTL             (HWD_PMB_BASE + 0x0028) 
#define HWD_PMB_ALPHA           (HWD_PMB_BASE + 0x002c) 
#define HWD_PMB_TSTCTL          (HWD_PMB_BASE + 0x0030) 
#define HWD_PMB_AVG_SLOT01_INIT (HWD_PMB_BASE + 0x0034)
#define HWD_PMB_AVG_SLOT23_INIT (HWD_PMB_BASE + 0x0038)
#define HWD_PMB_AVG_SLOT4_INIT  (HWD_PMB_BASE + 0x003c)
#define HWD_PMB_STATUS_NoUsed   (HWD_PMB_BASE + 0x0040) 
#define HWD_PMB_ADJ_LEN_MIN     (HWD_PMB_BASE + 0x0044) 
#define HWD_PMB_REF_LLR_L_DBG   (HWD_PMB_BASE + 0x0048) 
#define HWD_PMB_REF_LLR_H_DBG   (HWD_PMB_BASE + 0x004c) 
#define HWD_PMB_LLR_L_DBG       (HWD_PMB_BASE + 0x0050) 
#define HWD_PMB_LLR_H_DBG       (HWD_PMB_BASE + 0x0054) 
#define HWD_PMB_SNR_IIR_DBG     (HWD_PMB_BASE + 0x0058) 
#define HWD_PMB_AVG_SLOT01      (HWD_PMB_BASE + 0x005c) 
#define HWD_PMB_AVG_SLOT23      (HWD_PMB_BASE + 0x0060) 
#define HWD_PMB_AVG_SLOT4       (HWD_PMB_BASE + 0x0064) 
#define HWD_PMB_DRC_DBG         (HWD_PMB_BASE + 0x0068) 
#define HWD_PMB_MAX_SYM_DBG_L   (HWD_PMB_BASE + 0x006c)
#define HWD_PMB_LEN_DBG         (HWD_PMB_BASE + 0x0070)
#define HWD_PMB_MACINDX_DBG     (HWD_PMB_BASE + 0x0074)
#define HWD_PMB_THR_DBG_L       (HWD_PMB_BASE + 0x0078)
#define HWD_PMB_INTERLACE_DBG   (HWD_PMB_BASE + 0x007c)
#define HWD_PMB_MACINDX_DET_DBG (HWD_PMB_BASE + 0x0080)
#define HWD_PMB_DISABLE_LLRCHK  (HWD_PMB_BASE + 0x0084)
#define HWD_PMB_MAX_SYM_DBG_H   (HWD_PMB_BASE + 0x0088) 
#define HWD_PMB_THR_DBG_H       (HWD_PMB_BASE + 0x008c)
#define HWD_PMB_SYMBOL_ADJ      (HWD_PMB_BASE + 0x0090)    
#define HWD_PMB_TH_I_TABLE      (HWD_PMB_BASE + 0x0094) /*0x0B7F0094~0x0B7F00C0 */
#define HWD_PMB_FR_ADJ_TABLE    (HWD_PMB_BASE + 0x00C4) /*0x0B7F00C4~0x0B7F00D0, PMB_ADJ_TABLE, Adjustment table for front-end threshold detection logic */
#define HWD_PMB_C2I_0           (HWD_PMB_BASE + 0x00D4)
#define HWD_PMB_C2I_1           (HWD_PMB_BASE + 0x00D8)
#define HWD_PMB_ENB_PAMB_M      (HWD_PMB_BASE + 0x00DC)
#define HWD_PMB_DB_VALUE_NEW    (HWD_PMB_BASE + 0x00E0)
#define HWD_PMB_FN_PAMB_DAT     (HWD_PMB_BASE + 0x00E4)
#define HWD_PMB_FN_PAMB_ABS     (HWD_PMB_BASE + 0x00E8)
#define HWD_PMB_TH_I_PASS       (HWD_PMB_BASE + 0x00EC)
#define HWD_PMB_SNRBKOFF_NEW    (HWD_PMB_BASE + 0x00F0)
#define HWD_PMB_DIFF_L_NEW      (HWD_PMB_BASE + 0x00F4)
#define HWD_PMB_DIFF_H_NEW      (HWD_PMB_BASE + 0x00F8)
#define HWD_PMB_THR_TABLE       (HWD_PMB_BASE + 0x0100) /*0x0b763100-0x0b76315c   */
#define HWD_PMB_MAIN_TABLE      (HWD_PMB_BASE + 0x0200) /*0x0b763200-0x0b7632f8(0x0b7632Xc is unused)*/
#define HWD_PMB_LLR_ADJ_TABLE   (HWD_PMB_BASE + 0x0300) /*0x0b763300-0x0b763358(0x0b7633Xc is unused), PMB_ADJ_TABLE_L, PMB_ADJ_TABLE_M, PMB_ADJ_TABLE_H for LLR*/

#define HWD_PMB_DOF_SHIFT        5
#define HWD_PMB_DOF_MASK         0x1F

/*************************
* DO Harq control    *
**************************/

#define HWD_HARQ_BASE           (HWD_NO_BASE_ADDR+0x0b7b0000)  /* HWD_NO_BASE_ADDR = 0x00000000 */   

#define HWD_HARQ_CTL0           (HWD_HARQ_BASE + 0x0000)
#define HWD_HARQ_CTL1           (HWD_HARQ_BASE + 0x0004)
#define HWD_HARQ_CTL2           (HWD_HARQ_BASE + 0x0008)
#define HWD_HARQ_CTL3           (HWD_HARQ_BASE + 0x000c)
#define HWD_HARQ_CTL4           (HWD_HARQ_BASE + 0x0010)
#define HWD_HARQ_CTL5           (HWD_HARQ_BASE + 0x0014)
#define HWD_HARQ_CTL6           (HWD_HARQ_BASE + 0x0018)
#define HWD_HARQ_CTL7           (HWD_HARQ_BASE + 0x001c)
#define HWD_HARQ_ACCSNR0        (HWD_HARQ_BASE + 0x0020)   /* one per interlace*/ 
#define HWD_HARQ_ACCSNR1        (HWD_HARQ_BASE + 0x0024)
#define HWD_HARQ_ACCSNR2        (HWD_HARQ_BASE + 0x0028)
#define HWD_HARQ_ACCSNR3        (HWD_HARQ_BASE + 0x002c)

/*************************
* DO Turbo decoder control*
**************************/
#define HWD_CP_TD_BASE          (HWD_NO_BASE_ADDR+0x0b7a0000)  /* HWD_NO_BASE_ADDR = 0x00000000 */   

#define HWD_CP_TD_CTL           (HWD_CP_TD_BASE + 0x0000)
#define HWD_CP_TD_DO_LCESNT0    (HWD_CP_TD_BASE + 0x0004)
#define HWD_CP_TD_DO_LCESNT1    (HWD_CP_TD_BASE + 0x0008)
#define HWD_CP_TD_DO_LCESNT2    (HWD_CP_TD_BASE + 0x000c)

/*************************
* DO RX control*
**************************/
#define HWD_RXC_BASE            (HWD_NO_BASE_ADDR+0x0b780000)  /* HWD_NO_BASE_ADDR = 0x00000000 */   

#define HWD_RXC_RXC_MODE        (HWD_RXC_BASE + 0x0000)
#define HWD_RXC_DRC_MODE        (HWD_RXC_BASE + 0x0004)
#define HWD_RXC_ACK_MODE        (HWD_RXC_BASE + 0x0008)
#define HWD_RXC_DRC_ENDTIME     (HWD_RXC_BASE + 0x000c)
#define HWD_RXC_USR_MACINDX     (HWD_RXC_BASE + 0x0010)
#define HWD_RXC_LEN_MAC5        (HWD_RXC_BASE + 0x0014)
#define HWD_RXC_BCMCS_MODE      (HWD_RXC_BASE + 0x0018)
#define HWD_RXC_BCMCS_RATE      (HWD_RXC_BASE + 0x001c)

/*************************
* DMA RX MDM control*
**************************/
#define HWD_DMA_MDM_RX_PTR0     (HWD_DMA_MDM_BASE + 0x0000)
#define HWD_DMA_MDM_RX_PRM0     (HWD_DMA_MDM_BASE + 0x0004)
#define HWD_DMA_MDM_RX_PTR1     (HWD_DMA_MDM_BASE + 0x0010)
#define HWD_DMA_MDM_RX_PRM1     (HWD_DMA_MDM_BASE + 0x0014)
#define HWD_DMA_MDM_RX_PTR2     (HWD_DMA_MDM_BASE + 0x0020)
#define HWD_DMA_MDM_RX_PRM2     (HWD_DMA_MDM_BASE + 0x0024)
#define HWD_DMA_MDM_RX_PTR3     (HWD_DMA_MDM_BASE + 0x0030)
#define HWD_DMA_MDM_RX_PRM3     (HWD_DMA_MDM_BASE + 0x0034)
#define HWD_DMA_MDM_RX_PTR4     (HWD_DMA_MDM_BASE + 0x0040)
#define HWD_DMA_MDM_RX_PRM4     (HWD_DMA_MDM_BASE + 0x0044)
#define HWD_DMA_MDM_RX_PTR5     (HWD_DMA_MDM_BASE + 0x0050)
#define HWD_DMA_MDM_RX_PRM5     (HWD_DMA_MDM_BASE + 0x0054)
#define HWD_DMA_MDM_RX_PTR6     (HWD_DMA_MDM_BASE + 0x0060)
#define HWD_DMA_MDM_RX_PRM6     (HWD_DMA_MDM_BASE + 0x0064)
#define HWD_DMA_MDM_RX_PTR7     (HWD_DMA_MDM_BASE + 0x0070)
#define HWD_DMA_MDM_RX_PRM7     (HWD_DMA_MDM_BASE + 0x0074)
#define HWD_DMA_MDM_RX_CTL      (HWD_DMA_MDM_BASE + 0x0080)
#define HWD_DMA_MDM_RX_STAT     (HWD_DMA_MDM_BASE + 0x0084)
#define HWD_DMA_MDM_RX_DST      (HWD_DMA_MDM_BASE + 0x0088)
#define HWD_DMA_MDM_RX_TST      (HWD_DMA_MDM_BASE + 0x008C)
                                                           
/*------------------------------**
** Serial Programmer Interfaces **
**------------------------------*/
#if (SYS_ASIC == SA_MT6735)
/* RF SPI */
#define HWD_RFSP_BASE          (HWD_NO_BASE_ADDR+0x0b890000)

#define HWD_RFSP_IMCTL         HWD_RFSP_BASE + 0x00  /* R/W  RF SPI Immediate mode control         */
#define HWD_RFSP_IMWR_ADR      HWD_RFSP_BASE + 0x04  /* R/W  RF SPI Immediate mode write buffer address */
#define HWD_RFSP_IMWR_MSB      HWD_RFSP_BASE + 0x08  /* R/W  RF SPI Immediate mode write buffer MSB 16 bits of 48-bit word */
#define HWD_RFSP_IMWR_MID      HWD_RFSP_BASE + 0x0c  /* R/W  RF SPI Immediate mode write buffer middle 16 bits of 48-bit word */
#define HWD_RFSP_IMWR_LSB      HWD_RFSP_BASE + 0x10  /* R/W  RF SPI Immediate mode write buffer LSB 16 bits of 48-bit word */

#define HWD_RFSP_IMRD_ADR      HWD_RFSP_BASE + 0x14  /* R/W  RF SPI Immediate mode read buffer address */
#define HWD_RFSP_IMRD_MSB      HWD_RFSP_BASE + 0x18  /* R/W  RF SPI Immediate mode read buffer MSB 16 bits of 32-bit word */
#define HWD_RFSP_IMRD_LSB      HWD_RFSP_BASE + 0x1c  /* R/W  RF SPI Immediate mode read buffer LSB 16 bits of 32-bit word */

#define HWD_RFSP_DEV0_CTL2     HWD_RFSP_BASE + 0x20  /* R/W  RF SPI device #0 control #2 */
#define HWD_RFSP_DEV0_CTL1     HWD_RFSP_BASE + 0x24  /* R/W  RF SPI device #0 control #1 */
#define HWD_RFSP_DEV0_CTL0     HWD_RFSP_BASE + 0x28  /* R/W  RF SPI device #0 control #0 */
#define HWD_RFSP_DEV1_CTL2     HWD_RFSP_BASE + 0x2c  /* R/W  RF SPI device #1 control #2 */
#define HWD_RFSP_DEV1_CTL1     HWD_RFSP_BASE + 0x30  /* R/W  RF SPI device #1 control #1 */
#define HWD_RFSP_DEV1_CTL0     HWD_RFSP_BASE + 0x34  /* R/W  RF SPI device #1 control #0 */
#define HWD_RFSP_DEV2_CTL2     HWD_RFSP_BASE + 0x38  /* R/W  RF SPI device #2 control #2 */
#define HWD_RFSP_DEV2_CTL1     HWD_RFSP_BASE + 0x3c  /* R/W  RF SPI device #2 control #1 */
#define HWD_RFSP_DEV2_CTL0     HWD_RFSP_BASE + 0x40  /* R/W  RF SPI device #2 control #0 */
#define HWD_RFSP_DEV3_CTL2     HWD_RFSP_BASE + 0x44  /* R/W  RF SPI device #3 control #2 */
#define HWD_RFSP_DEV3_CTL1     HWD_RFSP_BASE + 0x48  /* R/W  RF SPI device #3 control #1 */
#define HWD_RFSP_DEV3_CTL0     HWD_RFSP_BASE + 0x4c  /* R/W  RF SPI device #3 control #0 */

#define HWD_RFSP_DEV0_CTL3     HWD_RFSP_BASE + 0xac  /* R/W  RF SPI device #5 control #2 */
#define HWD_RFSP_DEV1_CTL3     HWD_RFSP_BASE + 0xb0  /* R/W  RF SPI device #5 control #2 */
#define HWD_RFSP_DEV2_CTL3     HWD_RFSP_BASE + 0xb4  /* R/W  RF SPI device #5 control #2 */
#define HWD_RFSP_DEV3_CTL3     HWD_RFSP_BASE + 0xb8  /* R/W  RF SPI device #5 control #2 */
#define HWD_RFSP_VIRT_CTL1     HWD_RFSP_BASE + 0x68  /* R/W  RF SPI virtual mode control #1 */
#define HWD_RFSP_VIRT_CTL0     HWD_RFSP_BASE + 0x6c  /* R/W  RF SPI virtual mode control #0 */
#define HWD_RFSP_VIRT_CTL2     HWD_RFSP_BASE + 0xc4  /* R/W  RF SPI virtual mode control #1 */

#define HWD_RFSP0_DLYCTL       HWD_RFSP_BASE + 0x70  /* R/W  RF SPI delay mode control for 1st half slot */
#define HWD_RFSP1_DLYCTL       HWD_RFSP_BASE + 0x74  /* R/W  RF SPI delay mode control for 2nd half slot */
#define HWD_RFSP0_DLYDAT_MSB   HWD_RFSP_BASE + 0x78  /* R/W  RF SPI MSB data written to external device for 1st half slot */
#define HWD_RFSP0_DLYDAT_LSB   HWD_RFSP_BASE + 0x7c  /* R/W  RF SPI LSB data written to external device for 1st half slot */
#define HWD_RFSP1_DLYDAT_MSB   HWD_RFSP_BASE + 0x80  /* R/W  RF SPI MSB data written to external device for 2nd half slot */
#define HWD_RFSP1_DLYDAT_LSB   HWD_RFSP_BASE + 0x84  /* R/W  RF SPI LSB data written to external device for 2nd half slot */

#define HWD_RFSP_STATUS        HWD_RFSP_BASE + 0x88  /* R/W  RF SPI controller status */
#define HWD_RFSP_IMDIS         HWD_RFSP_BASE + 0x8c  /* R/W  RF SPI Time to disable immediate mode programming */
#define HWD_RFSP_IMEN          HWD_RFSP_BASE + 0x90  /* R/W  RF SPI Time to enable immediate mode programming */
#define HWD_RFSP_IMRD_CNT      HWD_RFSP_BASE + 0x94  /* R/W  RF SPI Number of words in immediate mode read buffer */

#define HWD_RFSP_DLY_WRCNT_RST HWD_RFSP_BASE + 0x98  /* R/W  RF SPI Delay buffer write pointer reset */
#define HWD_RFSP0_DLY_TST      HWD_RFSP_BASE + 0x9c  /* R/W  RF SPI Delay buffer 0 test control */
#define HWD_RFSP1_DLY_TST      HWD_RFSP_BASE + 0xa0  /* R/W  RF SPI Delay buffer 1 test control */
#define HWD_RFSP_DLY_RDCNT_RST HWD_RFSP_BASE + 0xa4  /* R/W  RF SPI Delay buffer read pointer reset */
#define HWD_RFSP_CLK_CTL       HWD_RFSP_BASE + 0xa8  /* R/W  RF SPI RFSP clock control */
#define HWD_RFSP_DEBUG1        HWD_RFSP_BASE + 0xc8  /* R/W  RF SPI RFSP debug1 */
#define HWD_RFSP_DEBUG2        HWD_RFSP_BASE + 0xcc  /* R/W  RF SPI RFSP debug2 */
#define HWD_RFSP_SW_RST        HWD_RFSP_BASE + 0xd0  /* R/W  RF SPI RFSP software reset */


/* TX SPI */
#define HWD_TXSP_BASE          (HWD_NO_BASE_ADDR+0x0b740000)

#define HWD_TXSP_IMCTL         HWD_TXSP_BASE + 0x00  /* R/W  TX SPI Immediate mode control         */
#define HWD_TXSP_IMWR_ADR      HWD_TXSP_BASE + 0x04  /* R/W  TX SPI Immediate mode write buffer address */
#define HWD_TXSP_IMWR_MSB      HWD_TXSP_BASE + 0x08  /* R/W  TX SPI Immediate mode write buffer MSB 16 bits of 48-bit word */
#define HWD_TXSP_IMWR_MID      HWD_TXSP_BASE + 0x0c  /* R/W  TX SPI Immediate mode write buffer middle 16 bits of 48-bit word */
#define HWD_TXSP_IMWR_LSB      HWD_TXSP_BASE + 0x10  /* R/W  TX SPI Immediate mode write buffer LSB 16 bits of 48-bit word */

#define HWD_TXSP_IMRD_ADR      HWD_TXSP_BASE + 0x14  /* R/W  TX SPI Immediate mode read buffer address */
#define HWD_TXSP_IMRD_MSB      HWD_TXSP_BASE + 0x18  /* R/W  TX SPI Immediate mode read buffer MSB 16 bits of 32-bit word */
#define HWD_TXSP_IMRD_LSB      HWD_TXSP_BASE + 0x1c  /* R/W  TX SPI Immediate mode read buffer LSB 16 bits of 32-bit word */

#define HWD_TXSP_DEV0_CTL2     HWD_TXSP_BASE + 0x20  /* R/W  TX SPI device #0 control #2 */
#define HWD_TXSP_DEV0_CTL1     HWD_TXSP_BASE + 0x24  /* R/W  TX SPI device #0 control #1 */
#define HWD_TXSP_DEV0_CTL0     HWD_TXSP_BASE + 0x28  /* R/W  TX SPI device #0 control #0 */
#define HWD_TXSP_DEV1_CTL2     HWD_TXSP_BASE + 0x2c  /* R/W  TX SPI device #1 control #2 */
#define HWD_TXSP_DEV1_CTL1     HWD_TXSP_BASE + 0x30  /* R/W  TX SPI device #1 control #1 */
#define HWD_TXSP_DEV1_CTL0     HWD_TXSP_BASE + 0x34  /* R/W  TX SPI device #1 control #0 */
#define HWD_TXSP_DEV2_CTL2     HWD_TXSP_BASE + 0x38  /* R/W  TX SPI device #2 control #2 */
#define HWD_TXSP_DEV2_CTL1     HWD_TXSP_BASE + 0x3c  /* R/W  TX SPI device #2 control #1 */
#define HWD_TXSP_DEV2_CTL0     HWD_TXSP_BASE + 0x40  /* R/W  TX SPI device #2 control #0 */
#define HWD_TXSP_DEV3_CTL2     HWD_TXSP_BASE + 0x44  /* R/W  TX SPI device #3 control #2 */
#define HWD_TXSP_DEV3_CTL1     HWD_TXSP_BASE + 0x48  /* R/W  TX SPI device #3 control #1 */
#define HWD_TXSP_DEV3_CTL0     HWD_TXSP_BASE + 0x4c  /* R/W  TX SPI device #3 control #0 */

#define HWD_TXSP_DEV0_CTL3     HWD_TXSP_BASE + 0xac  /* R/W  TX SPI device #5 control #2 */
#define HWD_TXSP_DEV1_CTL3     HWD_TXSP_BASE + 0xb0  /* R/W  TX SPI device #5 control #2 */
#define HWD_TXSP_DEV2_CTL3     HWD_TXSP_BASE + 0xb4  /* R/W  TX SPI device #5 control #2 */
#define HWD_TXSP_DEV3_CTL3     HWD_TXSP_BASE + 0xb8  /* R/W  TX SPI device #5 control #2 */
#define HWD_TXSP_VIRT_CTL1     HWD_TXSP_BASE + 0x68  /* R/W  TX SPI virtual mode control #1 */
#define HWD_TXSP_VIRT_CTL0     HWD_TXSP_BASE + 0x6c  /* R/W  TX SPI virtual mode control #0 */
#define HWD_TXSP_VIRT_CTL2     HWD_TXSP_BASE + 0xc4  /* R/W  TX SPI virtual mode control #1 */

#define HWD_TXSP_VIRT_CTL1     HWD_TXSP_BASE + 0x68  /* R/W  TX SPI virtual mode control #1 */
#define HWD_TXSP_VIRT_CTL0     HWD_TXSP_BASE + 0x6c  /* R/W  TX SPI virtual mode control #0 */
#define HWD_TXSP_VIRT_CTL2     HWD_TXSP_BASE + 0xc4  /* R/W  TX SPI virtual mode control #1 */

#define HWD_TXSP0_DLY_PTR       HWD_TXSP_BASE + 0x70  /* R/W  TX SPI delay mode control for 1st half slot */
#define HWD_TXSP1_DLY_PTR       HWD_TXSP_BASE + 0x74  /* R/W  TX SPI delay mode control for 2nd half slot */
#define HWD_TXSP0_DLYDAT_MSB   HWD_TXSP_BASE + 0x78  /* R/W  TX SPI MSB data written to external device for 1st half slot */
#define HWD_TXSP0_DLYDAT_LSB   HWD_TXSP_BASE + 0x7c  /* R/W  TX SPI LSB data written to external device for 1st half slot */
#define HWD_TXSP1_DLYDAT_MSB   HWD_TXSP_BASE + 0x80  /* R/W  TX SPI MSB data written to external device for 2nd half slot */
#define HWD_TXSP1_DLYDAT_LSB   HWD_TXSP_BASE + 0x84  /* R/W  TX SPI LSB data written to external device for 2nd half slot */

#define HWD_TXSP_STATUS        HWD_TXSP_BASE + 0x88  /* R/W  TX SPI controller status */
#define HWD_TXSP_IMDIS         HWD_TXSP_BASE + 0x8c  /* R/W  TX SPI Time to disable immediate mode programming */
#define HWD_TXSP_IMEN          HWD_TXSP_BASE + 0x90  /* R/W  TX SPI Time to enable immediate mode programming */
#define HWD_TXSP_IMRD_CNT      HWD_TXSP_BASE + 0x94  /* R/W  TX SPI Number of words in immediate mode read buffer */

#define HWD_TXSP0_DLY_CTL      HWD_TXSP_BASE + 0x9c  /* R/W  TX SPI Delay buffer 0 test control */
#define HWD_TXSP1_DLY_CTL      HWD_TXSP_BASE + 0xa0  /* R/W  TX SPI Delay buffer 1 test control */
#define HWD_TXSP_CLK_CTL       HWD_TXSP_BASE + 0xa8  /* R/W  TX SPI TXSP clock control */
#define HWD_TXSP_DEBUG1        HWD_TXSP_BASE + 0xc8  /* R/W  TX SPI TXSP debug1 */
#define HWD_TXSP_DEBUG2        HWD_TXSP_BASE + 0xcc  /* R/W  TX SPI TXSP debug2 */
#define HWD_TXSP_SW_RST        HWD_TXSP_BASE + 0xd0  /* R/W  TX SPI TXSP software reset */
#define HWD_TXSP_CFG           HWD_TXSP_BASE + 0xd4  /* R/W  TX SPI */
#define HWD_TXSP_GAINRD_MSB    HWD_TXSP_BASE + 0xf0  /* R/W  TX SPI */
#define HWD_TXSP_GAINRD_MID    HWD_TXSP_BASE + 0xf4  /* R/W  TX SPI */
#define HWD_TXSP_GAINRD_LSB    HWD_TXSP_BASE + 0xf8  /* R/W  TX SPI */
#define HWD_TXSP_GAINVAL0_MSB  HWD_TXSP_BASE + 0xfc  /* R/W  TX SPI */
#define HWD_TXSP_GAINVAL0_LSB  HWD_TXSP_BASE + 0x100 /* R/W  TX SPI */
#define HWD_TXSP_GAINVAL1_MSB  HWD_TXSP_BASE + 0x104 /* R/W  TX SPI */
#define HWD_TXSP_GAINVAL1_LSB  HWD_TXSP_BASE + 0x108 /* R/W  TX SPI */
#define HWD_TXSP_TXON_MSB      HWD_TXSP_BASE + 0x10c /* R/W  TX SPI */
#define HWD_TXSP_TXON_MID      HWD_TXSP_BASE + 0x110 /* R/W  TX SPI */
#define HWD_TXSP_TXON_LSB      HWD_TXSP_BASE + 0x114 /* R/W  TX SPI */
#define HWD_TXSP_TXOFF_MSB     HWD_TXSP_BASE + 0x118 /* R/W  TX SPI */
#define HWD_TXSP_TXOFF_MID     HWD_TXSP_BASE + 0x11c /* R/W  TX SPI */
#define HWD_TXSP_TXOFF_LSB     HWD_TXSP_BASE + 0x120 /* R/W  TX SPI */
#define HWD_TXSP_TXGATE_MASK   HWD_TXSP_BASE + 0x124 /* R/W  TX SPI */
#define HWD_TXSP_INT_MASK      HWD_TXSP_BASE + 0x128 /* R/W  TX SPI */
#endif

/* Recheck mem map for EVB7 */
#define HWD_RFSP_DLY_BASE               (HWD_NO_BASE_ADDR+0x0b840800)

#define HWD_RFSPI_DISCMPFROMCP          (HWD_RFSP_DLY_BASE + 0x44)
#define HWD_RFSPI_DISABLE_DLYCNT_LD     (HWD_RFSP_DLY_BASE + 0x54)
#define HWD_RFSPI_ON_DLY_MASK           (HWD_RFSP_DLY_BASE + 0x5C)
#define HWD_RFSPI_ON_CMPFIRST           (HWD_RFSP_DLY_BASE + 0x28)
#define HWD_RFSPI_ON_CMPSECOND          (HWD_RFSP_DLY_BASE + 0x2C)


/* Bit definitions for HWD_RFSPI_DISCMPFROMCP */
#define DLY_COMP_DIS                    1<<0        /* Disable delay load comparator */
#define DLY_COMP_EN                     0<<0        /* Enable delay load comparator */

/* Bit definitions for HWD_RFSPI_DISABLE_DLYCNT_LD */
#define DLY_LOADING_DATA_REG_DIS        1<<0        /* Disable the loading to PDM data register */
#define DLY_LOADING_DATA_REG_EN         0<<0        /* Enable the loading to PDM data register */


/* Bit definitions for HWD_RFSP_DLY_WRCNT_RST */
#define RESET_1ST_SLOT_WRCNT            1<<0        /* reset delay buffer #0 write pointer */
#define RESET_2ND_SLOT_WRCNT            1<<1        /* reset delay buffer #1 write pointer */

/* Bit definitions for HWD_RFSPI_ON_DLY_MASK */
#define DLY_MASK_1ST_DLY_TRIG_EN        1<<0        /* enable delay load triggering for the 1st half slot */
#define DLY_MASK_2ND_DLY_TRIG_EN        1<<1        /* enable delay load triggering for the 2nd half slot */

/* Bit definitions for HWD_RFSP/TXSP_IMCTL */
#define IMCTL_BLOCK_MODE_EN   (0<<3)
#define IMCTL_BLOCK_MODE_DIS  (1<<3)

#define IMCTL_TRIG            1<<2        /* Trigger immediate mode serialization */
#define IMCTL_RDBUFADR_RST    1<<1        /* Reset read buffer address */
#define IMCTL_WRBUFADR_RST    1<<0        /* Reset write buffer address */

/* Bit definitions for HWD_RFSP_STATUS */
#define STATUS_IMMED_DONE     1<<2

/* Bit definitions for HWD_RFSP_IMWR_MSB */
#define IMWR_READ_ENABLE      1<<3        /* Enable SPI read mode */

/* Bit definitions for HWD_TXSP_CFG */
#define CFG_SEP_PADS          (0)
#define CFG_CLK_LD_MUXED      (1)
#define CFG_ALL_MUXED         (3)

/* Bit definitions for HWD_TXSP#_DLY_CTL */
#define DLY_CTL_CLK_EN        (1 << 3)    /* Enable delay mode logic clock */
#define DLY_CTL_WRCNT_RST     (1 << 2)    /* Reset delay buffer */


/*--------**
** UARTs  **
**--------*/
#define HWD_UART_BASE    (HWD_CP_BASE_ADDR+0x00012000) /* Internal UART Base address */
#define HWD_UART_RHR(x)  (HWD_UART_BASE + (x)*0x100 + 0x00)  /* Tx holding data */
#define HWD_UART_THR(x)  (HWD_UART_BASE + (x)*0x100 + 0x00)  /* Tx holding data */
#define HWD_UART_IER(x)  (HWD_UART_BASE + (x)*0x100 + 0x04)  /* Interrupt enable */
#define HWD_UART_ISR(x)  (HWD_UART_BASE + (x)*0x100 + 0x08)  /* Interrupt iden. register */
#define HWD_UART_FCR(x)  (HWD_UART_BASE + (x)*0x100 + 0x08)  /* Fifo control */
#define HWD_UART_LCR(x)  (HWD_UART_BASE + (x)*0x100 + 0x0C)  /* Line control */
#define HWD_UART_MCR(x)  (HWD_UART_BASE + (x)*0x100 + 0x10)  /* Modem control */
#define HWD_UART_LSR(x)  (HWD_UART_BASE + (x)*0x100 + 0x14)  /* Line status */
#define HWD_UART_MSR(x)  (HWD_UART_BASE + (x)*0x100 + 0x18)  /* Modem status */
#define HWD_UART_SPR(x)  (HWD_UART_BASE + (x)*0x100 + 0x1C)  /* Scratchpad */
#define HWD_UART_DLL(x)  (HWD_UART_BASE + (x)*0x100 + 0x00)  /* LSB divisor latch */
#define HWD_UART_DLM(x)  (HWD_UART_BASE + (x)*0x100 + 0x04)  /* MSB divisor latch */
#define HWD_UART_RXCNT(x)  (HWD_UART_BASE + (x)*0x100 + 0x34)  /* Rx buf count */
#define HWD_UART_TXCNT(x)  (HWD_UART_BASE + (x)*0x100 + 0x38)  /* Tx buf count */
/*--------------------------------------------------------------------
* UART Port Definitions For The 16550 serial interface
*--------------------------------------------------------------------*/
#define HWD_UART_PORT_NUM_MAX   2
#define HWD_UART0_FIFO_DEPTH 64             /* Uart0 fifo depth */
#define HWD_UART1_FIFO_DEPTH 1024           /* Uart1 fifo depth */                                                                                      

/*-------------------------------------**
** CP Global Control Registers         **
**--------------------------------------*/
#define HWD_GCR_BASE_ADDR      (HWD_CP_BASE_ADDR+0x0000b000)

#define HWD_GCR_CGBR0          (HWD_GCR_BASE_ADDR+0x0000)
#define HWD_GCR_CGBR1          (HWD_GCR_BASE_ADDR+0x0004)
#define HWD_GCR_CGBR2          (HWD_GCR_BASE_ADDR+0x0008)
#define HWD_GCR_CGBR3          (HWD_GCR_BASE_ADDR+0x000c)
#define HWD_GCR_IRAMMR         (HWD_GCR_BASE_ADDR+0x0010)
#define HWD_GCR_DDRCSC         (HWD_GCR_BASE_ADDR+0x0014)
#define HWD_GCR_BOOTST         (HWD_GCR_BASE_ADDR+0x0018)
#define HWD_GCR_CHIPID         (HWD_GCR_BASE_ADDR+0x001c)
#define HWD_GCR_FCTREN         (HWD_GCR_BASE_ADDR+0x00F4)     /* R/W  free running counter enable          0x0000 */

/* HWD_GCR_FCTREN register */
#define HWD_GCR_FCTREN_ENABLE  (1<<0)      /* bit[0] 1=enable, 0=disable                         */

#if (SYS_ASIC >= SA_MT6755)
/* HWD_GCR_CGBR0 register */
#define HWD_CGBRO_U1_PINOUT    (0x01<<15)/*CGBR0[15]==1,  UART1 pin out; CGBR0[15]==0, attach to AP*/
#endif

/* Debug Mux */
#define HWD_DEBUG_SEL0         (HWD_GCR_BASE_ADDR+0x010C)
#define HWD_DEBUG_SEL1         (HWD_GCR_BASE_ADDR+0x0110)
#define HWD_DEBUG_SEL2         (HWD_GCR_BASE_ADDR+0x0114)

#if (SYS_ASIC == SA_MT6755) || (SYS_ASIC == SA_MT6750)
#define DBG_SEL_MATRIX_S3      0x0
#define DBG_SEL_MATRIX_S2      0x3b
#define DBG_SEL_MATRIX_S1      0x3c
#define DBG_SEL_MATRIX_S0      0x3d

#define DBG_SEL_MATRIX_M2      0x3e
#define DBG_SEL_MATRIX_M1      0x3f
#define DBG_SEL_MATRIX_M0      0x40
#elif (SYS_ASIC == SA_MT6797) || (SYS_ASIC == SA_MT6757)
#define DBG_SEL_MATRIX_S3      0x3b
#define DBG_SEL_MATRIX_S2      0x3c
#define DBG_SEL_MATRIX_S1      0x3d
#define DBG_SEL_MATRIX_S0      0x3e

#define DBG_SEL_MATRIX_M2      0x3f
#define DBG_SEL_MATRIX_M1      0x40
#define DBG_SEL_MATRIX_M0      0x41
#else
#define DBG_SEL_MATRIX_S3      0x0
#define DBG_SEL_MATRIX_S2      0x0
#define DBG_SEL_MATRIX_S1      0x0
#define DBG_SEL_MATRIX_S0      0x0

#define DBG_SEL_MATRIX_M2      0x0
#define DBG_SEL_MATRIX_M1      0x0
#define DBG_SEL_MATRIX_M0      0x0
#endif

/*-----------------**
** CP GPIO         **
**-----------------*/
#define HWD_GPIO_BASE          (HWD_CP_BASE_ADDR+0x00019000)

#define HWD_GPIO_DAT0          (HWD_GPIO_BASE+0x000)  /* R/W  CP GPIO data control for GPIO[15:0]    0x0 */
#define HWD_GPIO_DAT1          (HWD_GPIO_BASE+0x008)  /* R/W  CP GPIO data control for GPIO[31:16]   0x0 */
#define HWD_GPIO_DAT2          (HWD_GPIO_BASE+0x010)  /* R/W  CP GPIO data control for GPIO[47:32]   0x0 */
#define HWD_GPIO_DAT3          (HWD_GPIO_BASE+0x018)  /* R/W  CP GPIO data control for GPIO[63:48]   0x0 */
#define HWD_GPIO_DAT4          (HWD_GPIO_BASE+0x020)  /* R/W  CP GPIO data control for GPIO[79:64]   0x0 */
#define HWD_GPIO_DAT5          (HWD_GPIO_BASE+0x028)  /* R/W  CP GPIO data control for GPIO[95:80]   0x0 */
#define HWD_GPIO_DAT6          (HWD_GPIO_BASE+0x030)  /* R/W  CP GPIO data control for GPIO[111:96]  0x0 */
#define HWD_GPIO_DAT7          (HWD_GPIO_BASE+0x038)  /* R/W  CP GPIO data control for GPIO[127:112] 0x0 */
#define HWD_GPIO_DAT8          (HWD_GPIO_BASE+0x040)  /* R/W  CP GPIO data control for GPIO[143:128] 0x0 */
#define HWD_GPIO_DAT9          (HWD_GPIO_BASE+0x048)  /* R/W  CP GPIO data control for GPIO[159:144] 0x0 */
#define HWD_GPIO_DAT10         (HWD_GPIO_BASE+0x050)  /* R/W  CP GPIO data control for GPIO[171:160] 0x0 */

#define HWD_DIR0               (HWD_GPIO_BASE+0x004)  /* R/W  CP GPIO direction control for GPIO[15:0]    0xffff */
#define HWD_DIR1               (HWD_GPIO_BASE+0x00c)  /* R/W  CP GPIO direction control for GPIO[31:16]   0xffff */
#define HWD_DIR2               (HWD_GPIO_BASE+0x014)  /* R/W  CP GPIO direction control for GPIO[47:32]   0xffff */
#define HWD_DIR3               (HWD_GPIO_BASE+0x01c)  /* R/W  CP GPIO direction control for GPIO[63:48]   0xffff */
#define HWD_DIR4               (HWD_GPIO_BASE+0x024)  /* R/W  CP GPIO direction control for GPIO[79:64]   0xffff */
#define HWD_DIR5               (HWD_GPIO_BASE+0x02c)  /* R/W  CP GPIO direction control for GPIO[95:80]   0xffff */
#define HWD_DIR6               (HWD_GPIO_BASE+0x034)  /* R/W  CP GPIO direction control for GPIO[111:96]  0xffff */
#define HWD_DIR7               (HWD_GPIO_BASE+0x03c)  /* R/W  CP GPIO direction control for GPIO[127:112] 0xffff */
#define HWD_DIR8               (HWD_GPIO_BASE+0x044)  /* R/W  CP GPIO direction control for GPIO[143:128] 0xffff */
#define HWD_DIR9               (HWD_GPIO_BASE+0x04c)  /* R/W  CP GPIO direction control for GPIO[159:144] 0xffff */
#define HWD_DIR10              (HWD_GPIO_BASE+0x054)  /* R/W  CP GPIO direction control for GPIO[171:160] 0x07ff */


/*---------------**
** DO Searcher   **
**---------------*/
#define HWD_SCHR_BASE            0x0b760000      /* CBP8 searcher base address */

#define HWD_SRDO_START           (HWD_SCHR_BASE + 0x0000)   /* W      DO Searcher start                          */
#define HWD_SRDO_PAUSE           (HWD_SCHR_BASE + 0x0004)   /* R/W    DO Searcher pause                          */
#define HWD_SRDO_RST             (HWD_SCHR_BASE + 0x0008)   /* W      DO Searcher reset                          */
#define HWD_SRDO_INBUF_CTL       (HWD_SCHR_BASE + 0x000C)   /* R/W    DO Searcher input buffer control           */
#define HWD_SRDO_CTL             (HWD_SCHR_BASE + 0x0010)   /* R/W    DO Searcher control0                       */
#define HWD_SRDO_GENSTAT0        (HWD_SCHR_BASE + 0x0018)   /* R/W    DO Searcher statistics generation control0 */
#define HWD_SRDO_GENSTAT1        (HWD_SCHR_BASE + 0x001C)   /* R/W    DO Searcher statistics generation control1 */
#define HWD_SRDO_PATHMAINT       (HWD_SCHR_BASE + 0x0020)   /* R/W    DO Searcher path maintenance control       */
#define HWD_SRDO_INBUF_ADR       (HWD_SCHR_BASE + 0x0024)   /* R/W    DO Searcher input buffer test address      */
#define HWD_SRDO_INBUF_DAT       (HWD_SCHR_BASE + 0x0028)   /* R/W    DO Searcher input buffer test data         */
#define HWD_SRDO_STATUS0         (HWD_SCHR_BASE + 0x002C)   /* R      DO Searcher status                         */
#define HWD_SRDO_PLTINFO_CLR0    (HWD_SCHR_BASE + 0x0030)   /* R/W    DO Searcher pilot info clear               */
#define HWD_SRDO_PLTINFO_CLR1    (HWD_SCHR_BASE + 0x0034)   /* R/W    DO Searcher pilot info clear               */
#define HWD_SRDO_PATHINFO_CLR    (HWD_SCHR_BASE + 0x0038)   /* R/W    DO Searcher path info clear                */
#define HWD_SRDO_ACQ_CTL         (HWD_SCHR_BASE + 0x003C)   /* R/W    DO Searcher acquisition offset             */
#define HWD_SRDO_TSTCTL          (HWD_SCHR_BASE + 0x0040)   /* R/W    DO Searcher test control             */
#define HWD_SRDO_CLKCTL          (HWD_SCHR_BASE + 0x0048)   /* R/W    DO Searcher clock control             */
#define HWD_SRDO_STATUS1         (HWD_SCHR_BASE + 0x0050)   /* R      DO Searcher status                         */
#define HWD_SRDO_INBUF_DAT1      (HWD_SCHR_BASE + 0x0054)   /* R/W    read/write to input buffer in test mode    */
#define HWD_FNDO_MEMCTL          (HWD_SCHR_BASE + 0x0058)   /* R/W    select the order to stores data in finger memories */
#define HWD_SRDO_PATHINFO        (HWD_SCHR_BASE + 0x0080)   /* R      DO Searcher PathInfo buffer                */
#define HWD_SRDO_PLTLIST         (HWD_SCHR_BASE + 0x0100)   /* R/W    DO Searcher Pilot List                     */
#define HWD_SRDO_PLTINFO         (HWD_SCHR_BASE + 0x0200)   /* R      DO Searcher PilotInfo buffer               */
#define HWD_SRDO_PATHBUF         (HWD_SCHR_BASE + 0x0800)   /* R      DO Searcher Path buffer                    */

/*HWD_SRDO_CLKCTL*/
#define CLKCTL_CLK_MODE_SHIFT                 1     /* [1] */
#define CLKCTL_CLKENB_SHIFT                   0     /* [0] */
#define CLKCTL_CLK_MODE_MASK                  (0x1 << CLKCTL_CLK_MODE_SHIFT)
#define CLKCTL_CLKENB_MASK                    (0x1 << CLKCTL_CLKENB_SHIFT)

/*-------------------------*        
** DSPM GPIO Mode Control **
**------------------------*/
#define HWD_DM_GPIO_FUNC       (HWD_GPIO_BASE+0x01a8)
#define HWD_DV_GPIO_FUNC       (HWD_GPIO_BASE+0x01ac)

/*-----------------**          
** IRAM            **          
**-----------------*/
#define HWD_IRAM_BASE_ADDR     0x51000000  /* IRAM base address */
#define HWD_IRAM_SIZE          0x00018000  /* MTK Everest IRAM size in bytes (96K) */

#define HWD_IRAM_FULL_SIZE     HWD_IRAM_SIZE

#define HWD_SRAM_BASE_ADDR     0x00000000
#ifdef SYS_OPTION_IOP_CCIF
#define HWD_SRAM_FULL_SIZE     0x00C00000
#else
#define HWD_SRAM_FULL_SIZE     0x01000000
#endif
/* Flags to be shared between CP and ASIC boot */
/* For CBP8.2 and above the 32-bit scratch pad resister at 0x5200B0F0
 * is used instead of the variable at IRAM offset 0x20 */
#define IRAM_RESET_HANDLER_FLAG_ADDR       (HWD_REG32_WDSTAT)

#define IRAM_RESET_HANDLER_FLAG_VALID       0xCB80900D
#define IRAM_RESET_HANDLER_FLAG_WATCHDOG    0xD09D09D0
                   
/*-----------------**
** Flash Memory    **
**-----------------*/

#ifdef SYS_FLASH_LESS_SUPPORT
#define HWD_FLASH_BASE_ADDRESS 0x00000000   /* SRAM base address  */
#endif

#define HWD_OFFLINE_VALUE       0x59595959   /* Pattern indicating CP to Offline mode */
#define HWD_ONLINE_VALUE        0x00000000   /* Pattern indicating CP to Online mode */
#define HWD_CONTROL_VALUE       0xc4c4c4c4   /* Pattern indicating CP to Boot loader mode transition */


/*---------------**
** MuxPDU Memory **
**---------------*/

#define HWD_MPDU_BASE_ADDR     (HWD_NO_BASE_ADDR+0x0b420000)

#define HWD_MPDU_CTL           (HWD_MPDU_BASE_ADDR+0x0800)  /* R/W  MuxPDU Control Register                   0x0000 */
#define HWD_MPDU_TYPE5_CTL     (HWD_MPDU_BASE_ADDR+0x0804)  /* R/W  MuxPDU Type5 Control Register             0x0000 */
#define HWD_MPDU_TIMER_INIT    (HWD_MPDU_BASE_ADDR+0x0808)  /* R/W  MuxPDU Programmable Timer Initial Value   0x0000 */
#define HWD_MPDU_DAT_TST       (HWD_MPDU_BASE_ADDR+0x080C)  /* R/W  MuxPDU 16-bit Data in Test Mode           0x0000 */
#define HWD_MPDU_SPARE         (HWD_MPDU_BASE_ADDR+0x0810)  /* R/W  MuxPDU 16-bit Spare Register              0x0000 */
#define HWD_MPDU_MEMORY        (HWD_MPDU_BASE_ADDR+0x0000)  /* R/W  MuxPDU Memory Data Out (Size=300x32)             */
                                                           

/*---------------------------------------------**
** MPU Populated Memory Base Address Registers **
**---------------------------------------------*/
#define HWD_MPU_BASE           (HWD_CP_BASE_ADDR+0x00005000)   /* base register addr for USB */

#define HWD_MPU_NUM_POP_BLOCKS (16)
#define HWD_MPU_BLOCK0_BADDR   (HWD_MPU_BASE+0x00000000)  /* R/W  Memory Block 0 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK1_BADDR   (HWD_MPU_BASE+0x00000004)  /* R/W  Memory Block 1 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK2_BADDR   (HWD_MPU_BASE+0x00000008)  /* R/W  Memory Block 2 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK3_BADDR   (HWD_MPU_BASE+0x0000000C)  /* R/W  Memory Block 3 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK4_BADDR   (HWD_MPU_BASE+0x00000010)  /* R/W  Memory Block 4 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK5_BADDR   (HWD_MPU_BASE+0x00000014)  /* R/W  Memory Block 5 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK6_BADDR   (HWD_MPU_BASE+0x00000018)  /* R/W  Memory Block 6 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK7_BADDR   (HWD_MPU_BASE+0x0000001C)  /* R/W  Memory Block 7 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK8_BADDR   (HWD_MPU_BASE+0x00000020)  /* R/W  Memory Block 8 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK9_BADDR   (HWD_MPU_BASE+0x00000024)  /* R/W  Memory Block 9 Base Address (32bits)  0xFFFFFFFF */
#define HWD_MPU_BLOCK10_BADDR  (HWD_MPU_BASE+0x00000028)  /* R/W  Memory Block 10 Base Address (32bits) 0xFFFFFFFF */
#define HWD_MPU_BLOCK11_BADDR  (HWD_MPU_BASE+0x0000002C)  /* R/W  Memory Block 11 Base Address (32bits) 0xFFFFFFFF */
#define HWD_MPU_BLOCK12_BADDR  (HWD_MPU_BASE+0x00000030)  /* R/W  Memory Block 12 Base Address (32bits) 0xFFFFFFFF */
#define HWD_MPU_BLOCK13_BADDR  (HWD_MPU_BASE+0x00000034)  /* R/W  Memory Block 13 Base Address (32bits) 0xFFFFFFFF */
#define HWD_MPU_BLOCK14_BADDR  (HWD_MPU_BASE+0x00000038)  /* R/W  Memory Block 14 Base Address (32bits) 0xFFFFFFFF */
#define HWD_MPU_BLOCK15_BADDR  (HWD_MPU_BASE+0x0000003C)  /* R/W  Memory Block 15 Base Address (32bits) 0xFFFFFFFF */

/*----------------------------------------**
** MPU Populated Memory Control Registers **
**----------------------------------------*/

#define HWD_MPU_BLOCK0_CTL     (HWD_MPU_BASE+0x00000040)  /* R/W  Memory Block 0 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK1_CTL     (HWD_MPU_BASE+0x00000044)  /* R/W  Memory Block 1 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK2_CTL     (HWD_MPU_BASE+0x00000048)  /* R/W  Memory Block 2 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK3_CTL     (HWD_MPU_BASE+0x0000004C)  /* R/W  Memory Block 3 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK4_CTL     (HWD_MPU_BASE+0x00000050)  /* R/W  Memory Block 4 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK5_CTL     (HWD_MPU_BASE+0x00000054)  /* R/W  Memory Block 5 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK6_CTL     (HWD_MPU_BASE+0x00000058)  /* R/W  Memory Block 6 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK7_CTL     (HWD_MPU_BASE+0x0000005C)  /* R/W  Memory Block 7 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK8_CTL     (HWD_MPU_BASE+0x00000060)  /* R/W  Memory Block 8 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK9_CTL     (HWD_MPU_BASE+0x00000064)  /* R/W  Memory Block 9 Control Register (6bits)  b000000 */
#define HWD_MPU_BLOCK10_CTL    (HWD_MPU_BASE+0x00000068)  /* R/W  Memory Block 10 Control Register (6bits) b000000 */
#define HWD_MPU_BLOCK11_CTL    (HWD_MPU_BASE+0x0000006C)  /* R/W  Memory Block 11 Control Register (6bits) b000000 */
#define HWD_MPU_BLOCK12_CTL    (HWD_MPU_BASE+0x00000070)  /* R/W  Memory Block 12 Control Register (6bits) b000000 */
#define HWD_MPU_BLOCK13_CTL    (HWD_MPU_BASE+0x00000074)  /* R/W  Memory Block 13 Control Register (6bits) b000000 */
#define HWD_MPU_BLOCK14_CTL    (HWD_MPU_BASE+0x00000078)  /* R/W  Memory Block 14 Control Register (6bits) b000000 */
#define HWD_MPU_BLOCK15_CTL    (HWD_MPU_BASE+0x0000007C)  /* R/W  Memory Block 15 Control Register (6bits) b000000 */

/*------------------------------------------**
** MPU Unpopulated Memory Segment Registers **
**------------------------------------------*/

#define HWD_MPU_NUM_UNPOP_SEGS (16)
#define HWD_MPU_SEG0_REG       (HWD_MPU_BASE+0x00000080)  /* R/W  Segment 0 Register (5bits)                b00000 */
#define HWD_MPU_SEG1_REG       (HWD_MPU_BASE+0x00000084)  /* R/W  Segment 1 Register (5bits)                b00000 */
#define HWD_MPU_SEG2_REG       (HWD_MPU_BASE+0x00000088)  /* R/W  Segment 2 Register (5bits)                b00000 */
#define HWD_MPU_SEG3_REG       (HWD_MPU_BASE+0x0000008C)  /* R/W  Segment 3 Register (5bits)                b00000 */
#define HWD_MPU_SEG4_REG       (HWD_MPU_BASE+0x00000090)  /* R/W  Segment 4 Register (5bits)                b00000 */
#define HWD_MPU_SEG5_REG       (HWD_MPU_BASE+0x00000094)  /* R/W  Segment 5 Register (5bits)                b00000 */
#define HWD_MPU_SEG6_REG       (HWD_MPU_BASE+0x00000098)  /* R/W  Segment 6 Register (5bits)                b00000 */
#define HWD_MPU_SEG7_REG       (HWD_MPU_BASE+0x0000009C)  /* R/W  Segment 7 Register (5bits)                b00000 */
#define HWD_MPU_SEG8_REG       (HWD_MPU_BASE+0x000000A0)  /* R/W  Segment 8 Register (5bits)                b00000 */
#define HWD_MPU_SEG9_REG       (HWD_MPU_BASE+0x000000A4)  /* R/W  Segment 9 Register (5bits)                b00000 */
#define HWD_MPU_SEG10_REG      (HWD_MPU_BASE+0x000000A8)  /* R/W  Segment 10 Register (5bits)               b00000 */
#define HWD_MPU_SEG11_REG      (HWD_MPU_BASE+0x000000AC)  /* R/W  Segment 11 Register (5bits)               b00000 */
#define HWD_MPU_SEG12_REG      (HWD_MPU_BASE+0x000000B0)  /* R/W  Segment 12 Register (5bits)               b00000 */
#define HWD_MPU_SEG13_REG      (HWD_MPU_BASE+0x000000B4)  /* R/W  Segment 13 Register (5bits)               b00000 */
#define HWD_MPU_SEG14_REG      (HWD_MPU_BASE+0x000000B8)  /* R/W  Segment 14 Register (5bits)               b00000 */
#define HWD_MPU_SEG15_REG      (HWD_MPU_BASE+0x000000BC)  /* R/W  Segment 15 Register (5bits)               b00000 */

/*----------------------------**
** MPU Fault Status Registers **
**----------------------------*/

#define HWD_MPU_FAULT_STATUS   (HWD_MPU_BASE+0x000000C0)  /* R/W  Fault Status Register (3bits)               b000 */
#define HWD_MPU_FAULT_ADDR     (HWD_MPU_BASE+0x000000C4)  /* R    Fault Address Register (32bits)                  */

/*----------------------------**
** MPU instruction enable Registers **
**----------------------------*/
#define HWD_MPU_ITRACE_EN_REG  (HWD_MPU_BASE+0x000000CC)  /*R/W  enable instruction tracing Register(1bit)   b0*/

/*----------------------------------------*/
/*  1xEVDO AFC hardware registers         */
/*----------------------------------------*/
/* Bit definitions/masks for HWD_AFC_CTRL */
#define HWD_AFC_RST_BIT             0x0001  /* HWD_AFC_CTRL Bit[0] - AFC Soft reset bit used to clear AFC accumulators and counter */
#define HWD_AFC_LOCK_BIT            0x0002  /* HWD_AFC_CTRL Bit[1] - AFC Lock/Latch bit used to update AFC accumulators and counter */
#define HWD_AFC_BUSY_BIT            0x0004  /* HWD_AFC_CTRL Bit[2] - AFC Read-only bit: 0x0 == AFC registers safe to read, 0x1 == AFC registers are busy */

/*----------------------------------------*/
/*  1xEVDO TXHA registers                 */
/*----------------------------------------*/

#define HWD_DO_TXHA_ACK_DATA        (HWD_NO_BASE_ADDR+0x0b758000) /* R/W  ACK Channel register - use only for BB7 HW Test */
#define HWD_DO_TEST_BUF_CTRL        (HWD_NO_BASE_ADDR+0x0b758008)   /* R/W big buffer capture trigger signal - use only for BB7 HW Test */

#define HWD_DO_TXHA_BASE_ADDR       (HWD_NO_BASE_ADDR+0x0b7d0000)

#define HWD_DO_TXHA_DRC_COVER_IMMEDIATE_MASK      0x00000008      /*R/W 1 indicates that DRC_Cover should be applied immediately*/
#define HWD_DO_TXHA_DRC_LEN_MASK                  0x03 
#define HWD_DO_TXHA_CELL_SWITCH_DISABLE_MASK      0x00000010      /*R/W 1 indicates that Cell swicth enabled(0) or disabled(1) */
 
#define HWD_DO_TXHA_RRI_DATA_ACK0   (HWD_DO_TXHA_BASE_ADDR+0x0000) /* R/W  ACK Channel Access Rate register */
#define HWD_DO_TXHA_RRI_DATA_ACK1   (HWD_DO_TXHA_BASE_ADDR+0x0004) /* R/W  ACK Channel RRI Symbol Rate register */
#define HWD_DO_TXHA_RRI_DATA_ACK2   (HWD_DO_TXHA_BASE_ADDR+0x0008) /* R/W  ACK Channel Payload/Subpacket Index register */
#define HWD_DO_TXHA_RRI_NAK         (HWD_DO_TXHA_BASE_ADDR+0x000c) /* R/W  NAK Channel Payload/Subpacket Index register */

#define HWD_DO_TXHA_DRC_COVER       (HWD_DO_TXHA_BASE_ADDR+0x0010) /* R/W  DRC Cover register */
#define HWD_DO_TXHA_DRC_VALUE       (HWD_DO_TXHA_BASE_ADDR+0x0014) /* R/W  DRC Value register */
#define HWD_DO_TXHA_DSC_DATA        (HWD_DO_TXHA_BASE_ADDR+0x0018) /* R/W  DSC Value register */
                                                           
#define HWD_DO_TXHA_TX_CTRL         (HWD_DO_TXHA_BASE_ADDR+0x001c) /* R    Transmitter Enable status register */ 
#define HWD_DO_TXHA_KSD_INDEX_FIRST (HWD_DO_TXHA_BASE_ADDR+0x0020) /* R/W  slot Ksd Index */
#define HWD_DO_TXHA_KSD_INDEX_SECOND (HWD_DO_TXHA_BASE_ADDR+0x0024) /* R/W  halfslot Ksd Index */
                                                           
#define HWD_DO_TXHA_CHNL_TYPE       (HWD_DO_TXHA_BASE_ADDR+0x0028) /* R/W  Channel Type register */
#define HWD_DO_TXHA_PROTOCOL_SUBTYP (HWD_DO_TXHA_BASE_ADDR+0x002c) /* R/W  Protocol Subtype register */
#define HWD_DO_TXHA_TX_RESET        (HWD_DO_TXHA_BASE_ADDR+0x0030) /* R/W  TXHA Reset register */
#define HWD_DO_TXHA_TX_ENABLE       (HWD_DO_TXHA_BASE_ADDR+0x0034) /* R/W  TXHA Enable register */

#define HWD_DO_TXHA_ACCESS_START    (HWD_DO_TXHA_BASE_ADDR+0x0038) /* R/W  Access Start register, in units of slots */

#define HWD_DO_TXHA_TX_ABORT        (HWD_DO_TXHA_BASE_ADDR+0x003c) /* R/W  TxAbort */
#define HWD_DO_TXHA_IQ_INV          (HWD_DO_TXHA_BASE_ADDR+0x0040) /* R/W  IQ bit swap */

#define HWD_DO_TXHA_DRC_GATING      (HWD_DO_TXHA_BASE_ADDR+0x0048) /* R/W  DRC Gating register */
#define HWD_DO_TXHA_DRC_LENGTH      (HWD_DO_TXHA_BASE_ADDR+0x004c) /* R/W  DRC Length register */
#define HWD_DO_TXHA_DSC_LENGTH      (HWD_DO_TXHA_BASE_ADDR+0x0050) /* R/W  DSC Length register, in units of 8*slots */
#define HWD_DO_TXHA_DRC_BOOST_LEN   (HWD_DO_TXHA_BASE_ADDR+0x0054) /* R/W  DRC Boost Length register, in units of 2*slots */
#define HWD_DO_TXHA_DSC_BOOST_LEN   (HWD_DO_TXHA_BASE_ADDR+0x0058) /* R/W  DSC Boost Length register, in units of 8*slots */

#define HWD_DO_TXHA_AUXPLTMIN_PYLD  (HWD_DO_TXHA_BASE_ADDR+0x005c) /* R/W  Auxiliary Pilot Minimum Payload register */
#define HWD_DO_TXHA_PILOT_SCALE     (HWD_DO_TXHA_BASE_ADDR+0x0060) /* R/W  Pilot Scale register, in <9,3,u> format */
#define HWD_DO_TXHA_AUXPILOT_ACK_SCALE  (HWD_DO_TXHA_BASE_ADDR+0x0064) /* R/W  Auxiliary Pilot Ack Scale register, in <13,6,u> format */
#define HWD_DO_TXHA_AUXPILOT_NAK_SCALE  (HWD_DO_TXHA_BASE_ADDR+0x00fc) /* R/W  Auxiliary Pilot Nak Scale register, in <13,6,u> format */
#define HWD_DO_TXHA_RRI_SCALE_ACK   (HWD_DO_TXHA_BASE_ADDR+0x0068) /* R/W  RRI Scale for ACK register, in <9,3,u> format */
#define HWD_DO_TXHA_RRI_SCALE_NAK   (HWD_DO_TXHA_BASE_ADDR+0x006c) /* R/W  RRI Scale for NAK register, in <9,3,u> format */
#define HWD_DO_TXHA_DSC_SCALE       (HWD_DO_TXHA_BASE_ADDR+0x0070) /* R/W  DSC Scale register, in <9,2,u> format */
#define HWD_DO_TXHA_DSC_SCALE_BOOST (HWD_DO_TXHA_BASE_ADDR+0x0074) /* R/W  DSC Scale Boost register, in <9,2,u> format */
#define HWD_DO_TXHA_DRC_SCALE       (HWD_DO_TXHA_BASE_ADDR+0x0078) /* R/W  DRC Scale register, in <9,2,u> format */
#define HWD_DO_TXHA_DRC_SCALE_BOOST (HWD_DO_TXHA_BASE_ADDR+0x007c) /* R/W  DRC Scale Boost register, in <9,2,u> format */
#define HWD_DO_TXHA_ACK_SCALE_SUP   (HWD_DO_TXHA_BASE_ADDR+0x0080) /* R/W  ACK Single-User Scale register, in <9,3,u> format */
#define HWD_DO_TXHA_ACK_SCALE_MUP   (HWD_DO_TXHA_BASE_ADDR+0x0084) /* R/W  ACK Multi-User Scale register, in <9,3,u> format */
#define HWD_DO_TXHA_DATA_SCALE_ACK  (HWD_DO_TXHA_BASE_ADDR+0x00c0) /* R/W  Data Scale ACK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE0_ACK (HWD_DO_TXHA_BASE_ADDR+0x0088) /* R/W  Data Scale 0 ACK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE1_ACK (HWD_DO_TXHA_BASE_ADDR+0x008c) /* R/W  Data Scale 1 ACK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE2_ACK (HWD_DO_TXHA_BASE_ADDR+0x0090) /* R/W  Data Scale 2 ACK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE3_ACK (HWD_DO_TXHA_BASE_ADDR+0x0094) /* R/W  Data Scale 3 ACK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE_NAK  (HWD_DO_TXHA_BASE_ADDR+0x00c4) /* R/W  Data Scale ACK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE0_NAK (HWD_DO_TXHA_BASE_ADDR+0x0098) /* R/W  Data Scale 0 NAK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE1_NAK (HWD_DO_TXHA_BASE_ADDR+0x009c) /* R/W  Data Scale 1 NAK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE2_NAK (HWD_DO_TXHA_BASE_ADDR+0x00a0) /* R/W  Data Scale 2 NAK register (see register decription document), in <11,6,u> format */ 
#define HWD_DO_TXHA_DATA_SCALE3_NAK (HWD_DO_TXHA_BASE_ADDR+0x00a4) /* R/W  Data Scale 3 NAK register (see register decription document), in <11,6,u> format */ 

#define HWD_DO_TXHA_2BIT_ACK        (HWD_DO_TXHA_BASE_ADDR+0x00a8) /* R/W  2-bit ACK for Access Channel */
#define HWD_DO_TXHA_2BIT_NAK        (HWD_DO_TXHA_BASE_ADDR+0x00ac) /* R/W  2-bit NAK for Access Channel */

#define HWD_DO_TXHA_BYTE_SWAP       (HWD_DO_TXHA_BASE_ADDR+0x00b0) /* R/W  ByteSwap */

#define HWD_DO_TXHA_PREPLT_SCALE    (HWD_DO_TXHA_BASE_ADDR+0x00b4) /* R/W - Preamble Pilot Scale */

#define HWD_DO_TXHA_KSD_FIRST_HALF  (HWD_DO_TXHA_BASE_ADDR+0x00b8) /* R/W - Ks linear 1st half */
#define HWD_DO_TXHA_KSD_SECOND_HALF (HWD_DO_TXHA_BASE_ADDR+0x00bc) /* R/W - Ks linear 2nd half */
#define HWD_DO_TXHA_KSDLOG2_FIRST_HALF  (HWD_DO_TXHA_BASE_ADDR+0x00c8) /* R/W - Ks Alog2 1st half */
#define HWD_DO_TXHA_KSDLOG2_SECOND_HALF (HWD_DO_TXHA_BASE_ADDR+0x00cc) /* R/W - Ks Alog2 2nd half */

#define HWD_DO_TXHA_FREEZE (HWD_DO_TXHA_BASE_ADDR+0x00d0) /* R/W - Do TxHA freeze */

#define HWD_DO_TXHA_CL_TEST_MODE (HWD_DO_TXHA_BASE_ADDR+0x00d4) /* R/W - Do TxHA close loop test mode */
#define HWD_DO_TXHA_CL_TEST_DATA (HWD_DO_TXHA_BASE_ADDR+0x00d8) /* R/W - Do TxHA close loop test data */

#define HWD_DO_TXHA_TEST0  (HWD_DO_TXHA_BASE_ADDR+0x00d8)   /* R/W, [4:0], TX TEST REGISTER */
#define HWD_DO_TXHA_TEST1  (HWD_DO_TXHA_BASE_ADDR+0x00dc)   /* R/C, [1:0], DSC_STATUS */
#define HWD_DO_TXHA_TEST2  (HWD_DO_TXHA_BASE_ADDR+0x00e0)   /* R, [7]:TX_RESUME, [6]:TX_FREEZE_0, [5:3]:DSC_DATA_LAT, [2:0]:DRC_COVER_LAT */
#define HWD_DO_TXHA_TEST3  (HWD_DO_TXHA_BASE_ADDR+0x00f0)   /* R(/W), FCS.. */
#define HWD_DO_TXHA_STATUS (HWD_DO_TXHA_BASE_ADDR+0x00f0) /* R/W - Do TxHA Status indication */

#define HWD_DO_TXHA_WAIT_CNT            (HWD_DO_TXHA_BASE_ADDR+0x00f4) /* R/W - CRC HW wait cycle */
#define HWD_DO_TXHA_1CRC_MODE           (HWD_DO_TXHA_BASE_ADDR+0x00f8) /* R/W - CRC HW mode */

#define HWD_DO_TX_IQ_INV       (HWD_NO_BASE_ADDR+0x0b870040)  /* R/W  [0], 1: Invert IQ from TXDO */
#define HWD_DO_TX_IQ_SWAP      0x0001
#define HWD_DO_TX_TEST_CTRL       (HWD_DO_TXHA_BASE_ADDR+0x0100)
#define HWD_DO_TX_I_OFFSET_INT    (HWD_DO_TXHA_BASE_ADDR+0x0108)  /* R/W  Tx DC I offset prior to int MS DAC (cal)  0x0    */
#define HWD_DO_TX_Q_OFFSET_INT    (HWD_DO_TXHA_BASE_ADDR+0x010c)  /* R/W  Tx DC Q offset prior to int MS DAC (cal)  0x0    */

#define HWD_DO_TXHA_CLIP_THR1       (HWD_DO_TXHA_BASE_ADDR+0x0110) /* R/W DO Tx Slot Clipper Threshold, in <7,0,u> format */
#define HWD_DO_TXHA_CLIP_THR2       (HWD_DO_TXHA_BASE_ADDR+0x0114) /* R/W DO Tx Slot Clipper Threshold, in <7.0,u> format */

#define HWD_TX_GAIN_COMP1_DO   (HWD_DO_TXHA_BASE_ADDR+0x0118)  /* R/W  DO TX gain compensation value                0x0    */
#define HWD_TX_GAIN_COMP2_DO   (HWD_DO_TXHA_BASE_ADDR+0x011c)  /* R/W  DO TX gain compensation value                0x0    */

#define HWD_TX_AP_IIR2_A1_CP_DO   (HWD_DO_TXHA_BASE_ADDR+0x0120)  /* R/W  DO TX equalizer A1 coefficient value                0x0    */
#define HWD_TX_AP_IIR2_A2_CP_DO   (HWD_DO_TXHA_BASE_ADDR+0x0124)  /* R/W  DO TX equalizer A2 coefficient value                0x0    */

#define HWD_TX_EQUAL_EN_DO   (HWD_DO_TXHA_BASE_ADDR+0x0128)  /* R/W  DO TX equalizer enable                0x0    */

#define HWD_DO_TX_DSM_EN   (HWD_DO_TXHA_BASE_ADDR+0x012c)  /* R/W  DO TX 3-bit DSM enable                0x0    */ 

#define HWD_TX_FIR_H_DO         (HWD_DO_TXHA_BASE_ADDR+0x0144)  /* R/W FIR H and enable value for DSM Droop DO TX 0x0     */

#define HWD_DO_TX_RF_GAIN_TARGET_1ST      (HWD_DO_TXHA_BASE_ADDR+0x0150) /** R/W Tx RF target value on the first half slot */ 
#define HWD_DO_TX_RF_GAIN_TARGET_2ND      (HWD_DO_TXHA_BASE_ADDR+0x0154) /** R/W Tx RF target value on the second half slot */
#define HWD_DO_TX_TXUPC_CLIP_1ST          (HWD_DO_TXHA_BASE_ADDR+0x0158) /** R/W Tx TxUPC clip value for first half slot */
#define HWD_DO_TX_FINE_GAIN_GBB0_OL_1ST   (HWD_DO_TXHA_BASE_ADDR+0x015C) /** R/W Tx open loop GBB0 fine gain value on the first half slot */
#define HWD_DO_TX_FINE_GAIN_GBB0_OL_2ND   (HWD_DO_TXHA_BASE_ADDR+0x0160) /** R/W Tx open loop GBB0 fine gain value on the second half slot */
#define HWD_DO_TX_FINE_GAIN_GBB0_CL_1ST   (HWD_DO_TXHA_BASE_ADDR+0x0164) /** R/W Tx close loop GBB0 fine gain value on the first half slot */
#define HWD_DO_TX_FINE_GAIN_GBB0_CL_2ND   (HWD_DO_TXHA_BASE_ADDR+0x0168) /** R/W Tx close loop GBB0 fine gain value on the second half slot */
#define HWD_DO_TX_CLOSE_LOOP_MODE         (HWD_DO_TXHA_BASE_ADDR+0x016C) /** R/W Tx operation mode */
#define HWD_DO_TX_NCO_THETA0              (HWD_DO_TXHA_BASE_ADDR+0x0170) /** R/W Tx NCO theta value on the first half slot */
#define HWD_DO_TX_NCO_THETA1              (HWD_DO_TXHA_BASE_ADDR+0x0174) /** R/W Tx NCO theta value on the second half slot */
#define HWD_DO_TX_FINE_GAIN_GBB1          (HWD_DO_TXHA_BASE_ADDR+0x0178) /** R/W Tx GBB1 fine gain value on the first/second half slot */
#define HWD_DO_TX_FINE_GAIN_DELTA         (HWD_DO_TXHA_BASE_ADDR+0x017C) /** R/O Tx delta gain value */
#define HWD_DO_TX_RF_GAIN_DET             (HWD_DO_TXHA_BASE_ADDR+0x0180) /** R/0 Tx DDPC loop detected RF gain */
#define HWD_DO_TX_TXUPC_CLIP_2ND          (HWD_DO_TXHA_BASE_ADDR+0x0184) /** R/W Tx TxUPC clip value for second half slot */
#if (SYS_ASIC <= SA_MT6750)
#define HWD_DO_TX_ORIONPLUS_EN            (HWD_DO_TXHA_BASE_ADDR+0x0200) /** R/W TX Orion+ mode enable*/
#endif
#define HWD_DO_TX_DETADC_CLK_INV          (HWD_DO_TXHA_BASE_ADDR+0x0204) /** R/W TX SRC mode enable*/
#define HWD_DO_TX_PHASE_ACC_S2_EN         (HWD_DO_TXHA_BASE_ADDR+0x0208) /** R/W TX Phase Accumulator solution 2 enable*/
#define HWD_DO_TX_AFC_DELTA               (HWD_DO_TXHA_BASE_ADDR+0x020C) /** R/W TX frequency error*/
#define HWD_DO_TX_IQ_PHASE_EST            (HWD_DO_TXHA_BASE_ADDR+0x0210) /** R/W Estimated phase error for IQ imbalance compensation*/
#define HWD_DO_TX_IQ_GAIN_EST             (HWD_DO_TXHA_BASE_ADDR+0x0214) /** R/W Estimated gain error for IQ imbalance compensation*/
#define HWD_DO_TX_ANTIDROOP_COEFF0        (HWD_DO_TXHA_BASE_ADDR+0x0218) /** R/W Cofficient0 of TXDFE anti-droop filter*/
#define HWD_DO_TX_ANTIDROOP_COEFF1        (HWD_DO_TXHA_BASE_ADDR+0x021C) /** R/W Cofficient1 of TXDFE anti-droop filter*/
#define HWD_DO_TX_ANTIDROOP_COEFF2        (HWD_DO_TXHA_BASE_ADDR+0x0220) /** R/W Cofficient2 of TXDFE anti-droop filter*/
#define HWD_DO_TX_ANTIDROOP_COEFF3        (HWD_DO_TXHA_BASE_ADDR+0x0224) /** R/W Cofficient3 of TXDFE anti-droop filter*/
#define HWD_DO_TX_ANTIDROOP_GAINII        (HWD_DO_TXHA_BASE_ADDR+0x0228) /** R/W Gain shift number ii of TXDFE anti-droop filter*/
#define HWD_DO_TX_ANTIDROOP_GAINJJ        (HWD_DO_TXHA_BASE_ADDR+0x022C) /** R/W Gain shift number jj of TXDFE anti-droop filter*/
#define HWD_DO_TX_ANTIDROOP_GAINKK        (HWD_DO_TXHA_BASE_ADDR+0x0230) /** R/W Gain shift number kk of TXDFE anti-droop filter*/
#define HWD_DO_TX_NCO_EN                  (HWD_DO_TXHA_BASE_ADDR+0x0234) /** R/W TX NCO enable*/
#define HWD_DO_TX_NCO_PHASE_JUMP_EN       (HWD_DO_TXHA_BASE_ADDR+0x0238) /** R/W TX phase jump enable*/
#define HWD_DO_TX_NCO_PHI_LO              (HWD_DO_TXHA_BASE_ADDR+0x023C) /** R/W Normalized shifting frequency of NCO*/
#define HWD_DO_TX_NCO_PHI_HI              (HWD_DO_TXHA_BASE_ADDR+0x0240) /** R/W Normalized shifting frequency of NCO*/
#define HWD_DO_TX_TTG_PHI_LO              (HWD_DO_TXHA_BASE_ADDR+0x0244) /** R/W Normalized shifting frequency of TTG*/
#define HWD_DO_TX_TTG_PHI_HI              (HWD_DO_TXHA_BASE_ADDR+0x0248) /** R/W Normalized shifting frequency of TTG*/
#define HWD_DO_TX_NCO_JUMP_THETA_LO       (HWD_DO_TXHA_BASE_ADDR+0x024C) /** R/W Jumping phase value*/
#define HWD_DO_TX_NCO_JUMP_THETA_HI       (HWD_DO_TXHA_BASE_ADDR+0x0250) /** R/W Jumping phase value*/
#define HWD_DO_TX_DC_OFFSETI              (HWD_DO_TXHA_BASE_ADDR+0x0254) /** R/W DC offset I of TX*/  
#define HWD_DO_TX_DC_OFFSETQ              (HWD_DO_TXHA_BASE_ADDR+0x0258) /** R/W DC offset Q of TX*/
#define HWD_DO_DET_FIR_BYPASS             (HWD_DO_TXHA_BASE_ADDR+0x0300) /** R/W Bypass DETDFE FIR */
#define HWD_DO_DET_IQ_PHASE_EST           (HWD_DO_TXHA_BASE_ADDR+0x0304) /** R/W Estimated phase error for DETDFE IQ imbalace compensation*/
#define HWD_DO_DET_IQ_GAIN_EST            (HWD_DO_TXHA_BASE_ADDR+0x0308) /** R/W Estimated gain error for DETDFE IQ imbalacne compensation*/
#define HWD_DO_DET_ANTIDROOP_COEFF0       (HWD_DO_TXHA_BASE_ADDR+0x030C) /** R/W Coefficient0 of DETDFE anti-droop filter*/
#define HWD_DO_DET_ANTIDROOP_COEFF1       (HWD_DO_TXHA_BASE_ADDR+0x0310) /** R/W Coefficient1 of DETDFE anti-droop filter*/
#define HWD_DO_DET_ANTIDROOP_COEFF2       (HWD_DO_TXHA_BASE_ADDR+0x0314) /** R/W Coefficient2 of DETDFE anti-droop filter*/
#define HWD_DO_DET_ANTIDROOP_COEFF3       (HWD_DO_TXHA_BASE_ADDR+0x0318) /** R/W Coefficient3 of DETDFE anti-droop filter*/
#define HWD_DO_DET_DC_OFFSETI             (HWD_DO_TXHA_BASE_ADDR+0x031C) /** R/W DC offset I of DETDFE*/
#define HWD_DO_DET_DC_OFFSETQ             (HWD_DO_TXHA_BASE_ADDR+0x0320) /** R/W DC offset Q of DETDFE*/
#define HWD_DO_DET_COMPENSATION_SCALE     (HWD_DO_TXHA_BASE_ADDR+0x0324) /** R/W Compensation scale for DETDFE anti-droop*/
#define HWD_DO_DET_ANTIDROOP_TRUNC_SEL    (HWD_DO_TXHA_BASE_ADDR+0x0328) /** R/W Truncation bits for DETDFE antidroop*/
#define HWD_DO_DET_DDPC_LENGTH            (HWD_DO_TXHA_BASE_ADDR+0x032C) /** R/W DDPC length*/
#define HWD_DO_DET_MEAS_TERM              (HWD_DO_TXHA_BASE_ADDR+0x0330) /** R/W Meas term for DDPC*/
#define HWD_DO_DET_TXDET_GAIN             (HWD_DO_TXHA_BASE_ADDR+0x0334) /** R/W TXDET gain*/
#define HWD_DO_DET_GBB0_OTHER_GAIN        (HWD_DO_TXHA_BASE_ADDR+0x0338) /** R/W GBB0 and others gain*/
#define HWD_DO_DET_DELAY_DIFF             (HWD_DO_TXHA_BASE_ADDR+0x033C) /** R/W Time difference between detect path and reference path*/
#define HWD_DO_DET_RF_GAIN_DET            (HWD_DO_TXHA_BASE_ADDR+0x0340) /** R/O DDPC result*/
#define HWD_DO_DET_DDPC_SUMI_LO_REF       (HWD_DO_TXHA_BASE_ADDR+0x0344) /** R/O Sum of reference I path in DDPC for debug*/
#define HWD_DO_DET_DDPC_SUMI_HI_REF       (HWD_DO_TXHA_BASE_ADDR+0x0348) /** R/O Sum of reference I path in DDPC for debug*/
#define HWD_DO_DET_DDPC_SUMQ_LO_REF       (HWD_DO_TXHA_BASE_ADDR+0x034C) /** R/O Sum of reference Q path in DDPC for debug*/
#define HWD_DO_DET_DDPC_SUMQ_HI_REF       (HWD_DO_TXHA_BASE_ADDR+0x0350) /** R/O Sum of reference Q path in DDPC for debug*/
#define HWD_DO_DET_DDPC_SUM_PROD_LO_REF   (HWD_DO_TXHA_BASE_ADDR+0x0354) /** R/O Sum of square if refernce I path and Q path in DDPC, for debug*/
#define HWD_DO_DET_DDPC_SUM_PROD_MI_REF   (HWD_DO_TXHA_BASE_ADDR+0x0358) /** R/O Sum of square if refernce I path and Q path in DDPC, for debug*/
#define HWD_DO_DET_DDPC_SUM_PROD_HI_REF   (HWD_DO_TXHA_BASE_ADDR+0x035C) /** R/O Sum of square if refernce I path and Q path in DDPC, for debug*/
#define HWD_DO_DET_SUMI_LO_DET            (HWD_DO_TXHA_BASE_ADDR+0x0360) /** R/O Sum of detect I path in DDPC, for debug*/
#define HWD_DO_DET_SUMI_HI_DET            (HWD_DO_TXHA_BASE_ADDR+0x0364) /** R/O Sum of detect I path in DDPC, for debug*/
#define HWD_DO_DET_SUMQ_LO_DET            (HWD_DO_TXHA_BASE_ADDR+0x0368) /** R/O Sum of detect Q path in DDPC, for debug*/
#define HWD_DO_DET_SUMQ_HI_DET            (HWD_DO_TXHA_BASE_ADDR+0x036C) /** R/O Sum of detect Q path in DDPC, for debug*/
#define HWD_DO_DET_DDPC_SUM_PROD_LO_DET   (HWD_DO_TXHA_BASE_ADDR+0x0370) /** R/O Sum of square of detect I path and Q path in DDPC for debug*/
#define HWD_DO_DET_DDPC_SUM_PROD_MI_DET   (HWD_DO_TXHA_BASE_ADDR+0x0374) /** R/O Sum of square of detect I path and Q path in DDPC for debug*/
#define HWD_DO_DET_DDPC_SUM_PROD_HI_DET   (HWD_DO_TXHA_BASE_ADDR+0x0378) /** R/O Sum of square of detect I path and Q path in DDPC for debug*/

/*----------------------------------------*/
/*  1xEVDO Cell-Sw H/W Registers          */
/*----------------------------------------*/
#define HWD_CELLSW_TOGGLE_SLOTS_MASK      0x000000FF

#define HWD_CELLSW_STATUS_CS_ON_MASK      0x00000001
#define HWD_CELLSW_STATUS_TST_MODE_MASK   0x00000002
#define HWD_CELLSW_STATUS_FIXED_MODE_MASK 0x00000004
#define HWD_CELLSW_STATUS_REAL_MODE_MASK  0x00000008
#define HWD_CELLSW_STATUS_TOGGLE_MASK     0x00000010

#define HWD_DO_CELLSW_CELL_MODE_MASK      0x00000003
#define HWD_DO_CELLSW_CELL_REAL_MODE      0x00000000
#define HWD_DO_CELLSW_CELL_FIXED_MODE     0x00000001
#define HWD_DO_CELLSW_CELL_TST_MODE       0x00000002

#define HWD_DO_CELLSW_BASE_ADDR   0x0b7c0700

#define HWD_DO_CELLSW_TOGGLE_SLOTS        (HWD_DO_CELLSW_BASE_ADDR+0x10)
#define HWD_DO_CELLSW_CELL_MODE           (HWD_DO_CELLSW_BASE_ADDR+0x14)
#define HWD_DO_CELLSW_CELL_STATUS         (HWD_DO_CELLSW_BASE_ADDR+0x18)
#define HWD_DO_CELLSW_STARTING_SLOTS      (HWD_DO_CELLSW_BASE_ADDR+0x1c)
#define HWD_DO_CELLSW_TEST_SLOTS          (HWD_DO_CELLSW_BASE_ADDR+0x20)
#define HWD_DO_CELLSW_CELL_STATE          (HWD_DO_CELLSW_BASE_ADDR+0x24)
#define HWD_DO_CELLSW_FIXED_SLOTS         (HWD_DO_CELLSW_BASE_ADDR+0x28)
#define HWD_DO_CELLSW_ERROR_STATUS        (HWD_DO_CELLSW_BASE_ADDR+0x2c)

/*----------------------------------------*/
/*  1xEVDO Delayed-Load Registers         */
/*----------------------------------------*/
#define HWD_DELAY_LD_BASE_ADDR (HWD_NO_BASE_ADDR+0x0b840800)

#define HWD_TXON_CMP0_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x00) /* R/W Compare register for TXON0 for the first half slot */
#define HWD_TXON_CMP0_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x04) /* R/W Compare register for TXON0 for the second half slot */
#define HWD_TXON_CMP1_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x08) /* R/W Compare register for TXON1 for the first half slot */
#define HWD_TXON_CMP1_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x0c) /* R/W Compare register for TXON1 for the second half slot */
#define HWD_TXON_CMP2_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x10) /* R/W Compare register for TXON2 for the first half slot */
#define HWD_TXON_CMP2_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x14) /* R/W Compare register for TXON2 for the second half slot */
#define HWD_TXON_CMP3_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x18) /* R/W Compare register for TXON3 for the first half slot */
#define HWD_TXON_CMP3_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x1c) /* R/W Compare register for TXON3 for the second half slot */
#define HWD_TXON_CMP4_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x20) /* R/W Compare register for TXON4 for the first half slot */
#define HWD_TXON_CMP4_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x24) /* R/W Compare register for TXON4 for the second half slot */

#define HWD_TXON_CMP5_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x64) /* R/W Compare register for TXON5 for the first half slot */
#define HWD_TXON_CMP5_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x68) /* R/W Compare register for TXON5 for the second half slot */
#define HWD_TXON_CMP6_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x6c) /* R/W Compare register for TXON6 for the first half slot */
#define HWD_TXON_CMP6_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x70) /* R/W Compare register for TXON6 for the second half slot */

#define HWD_TXON_CMP7_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x74) /* R/W Compare register for TXON7 for the first half slot */
#define HWD_TXON_CMP7_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x78) /* R/W Compare register for TXON7 for the second half slot */
#define HWD_TXON_CMP8_FIRST         (HWD_DELAY_LD_BASE_ADDR+0x7c) /* R/W Compare register for TXON8 for the first half slot */
#define HWD_TXON_CMP8_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x80) /* R/W Compare register for TXON8 for the second half slot */

#define HWD_TXON_INT_FIRST          (HWD_DELAY_LD_BASE_ADDR+0x48) /* R/W Compare register for TX_INT for the first half slot */
#define HWD_TXON_INT_SECOND         (HWD_DELAY_LD_BASE_ADDR+0x4c) /* R/W Compare register for TX_INT for the second half slot */

#define HWD_SPI_ON_CMP_FIRST        (HWD_DELAY_LD_BASE_ADDR+0x28) /* R/W Compare register for SPI for the first half slot */
#define HWD_SPI_ON_CMP_SECOND       (HWD_DELAY_LD_BASE_ADDR+0x2c) /* R/W Compare register for SPI for the second half slot */

#define HWD_PDM_CMP0_FIRST          (HWD_DELAY_LD_BASE_ADDR+0x84) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP0_SECOND         (HWD_DELAY_LD_BASE_ADDR+0x88) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP1_FIRST          (HWD_DELAY_LD_BASE_ADDR+0x8C) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP1_SECOND         (HWD_DELAY_LD_BASE_ADDR+0x90) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP2_FIRST          (HWD_DELAY_LD_BASE_ADDR+0x30) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP2_SECOND         (HWD_DELAY_LD_BASE_ADDR+0x34) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP3_FIRST          (HWD_DELAY_LD_BASE_ADDR+0x94) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP3_SECOND         (HWD_DELAY_LD_BASE_ADDR+0x98) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP4_FIRST          (HWD_DELAY_LD_BASE_ADDR+0x9C) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP4_SECOND         (HWD_DELAY_LD_BASE_ADDR+0xA0) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP5_FIRST          (HWD_DELAY_LD_BASE_ADDR+0xA4) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP5_SECOND         (HWD_DELAY_LD_BASE_ADDR+0xA8) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP6_FIRST          (HWD_DELAY_LD_BASE_ADDR+0xAC) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP6_SECOND         (HWD_DELAY_LD_BASE_ADDR+0xB0) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_TX_FINEGAIN0_OL_CMP_FIRST   (HWD_DELAY_LD_BASE_ADDR+0x104)
#define HWD_TX_FINEGAIN0_OL_CMP_SECOND  (HWD_DELAY_LD_BASE_ADDR+0x108)
#define HWD_TX_FINEGAIN0_CL_CMP_FIRST   (HWD_DELAY_LD_BASE_ADDR+0x114)
#define HWD_TX_FINEGAIN0_CL_CMP_SECOND  (HWD_DELAY_LD_BASE_ADDR+0x118)
#define HWD_TX_FINEGAIN1_CL_CMP_FIRST   (HWD_DELAY_LD_BASE_ADDR+0x11C)
#define HWD_TX_FINEGAIN1_CL_CMP_SECOND  (HWD_DELAY_LD_BASE_ADDR+0x120)
#define HWD_TX_RFPHASE_CMP_FIRST        (HWD_DELAY_LD_BASE_ADDR+0x124)
#define HWD_TX_RFPHASE_CMP_SECOND       (HWD_DELAY_LD_BASE_ADDR+0x128)
#define HWD_TXGAIN_WR_CMP_FIRST         (HWD_DELAY_LD_BASE_ADDR+0xEC)
#define HWD_TXGAIN_WR_CMP_SECOND        (HWD_DELAY_LD_BASE_ADDR+0xF0)
#define HWD_TXGAIN_RD_CMP_FIRST         (HWD_DELAY_LD_BASE_ADDR+0xFC)
#define HWD_TXGAIN_RD_CMP_SECOND        (HWD_DELAY_LD_BASE_ADDR+0x100)
#define HWD_TXSPION_CMP_FIRST           (HWD_DELAY_LD_BASE_ADDR+0xDC)
#define HWD_TXSPION_CMP_SECOND          (HWD_DELAY_LD_BASE_ADDR+0xE0)
#define HWD_APCDAC_CMP_FIRST            (HWD_PDM_CMP3_FIRST)
#define HWD_APCDAC_CMP_SECOND           (HWD_PDM_CMP3_SECOND)
#define HWD_PMIC_CMP_FIRST              (HWD_DELAY_LD_BASE_ADDR+0x12C)
#define HWD_PMIC_CMP_SECOND             (HWD_DELAY_LD_BASE_ADDR+0x130)
#define HWD_TXCAL_CMP_FIRST             (HWD_DELAY_LD_BASE_ADDR+0x16C)
#define HWD_TXCAL_CMP_SECOND            (HWD_DELAY_LD_BASE_ADDR+0x170)


#define HWD_TXON_DLYMODE            (HWD_DELAY_LD_BASE_ADDR+0x38) /* R/W TXON Delayed Load Mode register for the first and second half slot */
#define HWD_PDM_DLYMODE             (HWD_DELAY_LD_BASE_ADDR+0x3C) /* R/W PDM Delayeojid Load Mode register for the first and second half slot */
#define HWD_APC_DLYMODE             HWD_PDM_DLYMODE /* APC DAC (PDM) Delay Load Mode */
#define HWD_SPI_DLYMODE             (HWD_DELAY_LD_BASE_ADDR+0x50) /* R/W SPI Delayed Load Mode register for the first and second half slot */
#define HWD_TXON_DELAY_LOAD_DIS_CP       (HWD_DELAY_LD_BASE_ADDR+0xD4) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_PDM_DELAY_LOAD_DIS_CP       (HWD_DELAY_LD_BASE_ADDR+0x44) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_SPI_DELAY_LOAD_DIS_CP       (HWD_DELAY_LD_BASE_ADDR+0xD8) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_DIS_DELAY_LOAD_CNT      (HWD_DELAY_LD_BASE_ADDR+0x54) /* comparator disable for CP */ /* use for Delay Loading counter enable - 1 : disable, 0 : enable */
#define HWD_TXON_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0x60) /* R/W TXON Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_APC_DELAY_LOAD_DIS_CP   HWD_PDM_DELAY_LOAD_DIS_CP
#define HWD_TX_DELAY_LOAD_DIS_CP        (HWD_DELAY_LD_BASE_ADDR+0x110) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_TXGAIN_DELAY_LOAD_DIS_CP    (HWD_DELAY_LD_BASE_ADDR+0xF8) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_TXSPION_DELAY_LOAD_DIS_CP   (HWD_DELAY_LD_BASE_ADDR+0xE8) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_PMIC_DELAY_LOAD_DIS_CP      (HWD_DELAY_LD_BASE_ADDR+0x138) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_TXCAL_DELAY_LOAD_DIS_CP     (HWD_DELAY_LD_BASE_ADDR+0x178) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */

#define HWD_PDM0_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0xB4) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM1_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0xB8) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM2_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0x58) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM3_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0xBC) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_APC_DLY_MASK            HWD_PDM3_DLY_MASK /* R/W APC Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_TX_DLY_MASK             (HWD_DELAY_LD_BASE_ADDR+0x10C)
#define HWD_TXGAIN_DLY_MASK         (HWD_DELAY_LD_BASE_ADDR+0xF4)
#define HWD_TXSPION_DLY_MASK        (HWD_DELAY_LD_BASE_ADDR+0xE4)
#define HWD_PMIC_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0x134)
#define HWD_TXCAL_DLY_MASK          (HWD_DELAY_LD_BASE_ADDR+0x174)
#define HWD_PDM4_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0xC0) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM5_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0xC4) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM6_DLY_MASK           (HWD_DELAY_LD_BASE_ADDR+0xC8) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */

#define HWD_SPI_DLY_MASK            (HWD_DELAY_LD_BASE_ADDR+0x5c) /* R/W SPI Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */

#define HWD_TXON_DLY_MASK2           (HWD_DELAY_LD_BASE_ADDR+0xCC) /* R/W TXON Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM_DLY_1X_DO             (HWD_DELAY_LD_BASE_ADDR+0xD0) /* R/W PDM [0:6] 1x mode or DO mode */

#define HWD_DELAY_LD_RD_BASE_ADDR (HWD_NO_BASE_ADDR+0x0b840800)

#define HWD_TXON_CMP0_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x00) /* R/W Compare register for TXON0 for the first half slot */
#define HWD_TXON_CMP0_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x04) /* R/W Compare register for TXON0 for the second half slot */
#define HWD_TXON_CMP1_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x08) /* R/W Compare register for TXON1 for the first half slot */
#define HWD_TXON_CMP1_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x0c) /* R/W Compare register for TXON1 for the second half slot */
#define HWD_TXON_CMP2_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x10) /* R/W Compare register for TXON2 for the first half slot */
#define HWD_TXON_CMP2_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x14) /* R/W Compare register for TXON2 for the second half slot */
#define HWD_TXON_CMP3_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x18) /* R/W Compare register for TXON3 for the first half slot */
#define HWD_TXON_CMP3_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x1c) /* R/W Compare register for TXON3 for the second half slot */
#define HWD_TXON_CMP4_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x20) /* R/W Compare register for TXON4 for the first half slot */
#define HWD_TXON_CMP4_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x24) /* R/W Compare register for TXON4 for the second half slot */

#define HWD_TXON_CMP5_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x64) /* R/W Compare register for TXON5 for the first half slot */
#define HWD_TXON_CMP5_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x68) /* R/W Compare register for TXON5 for the second half slot */
#define HWD_TXON_CMP6_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x6c) /* R/W Compare register for TXON6 for the first half slot */
#define HWD_TXON_CMP6_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x70) /* R/W Compare register for TXON6 for the second half slot */

#define HWD_TXON_CMP7_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x74) /* R/W Compare register for TXON7 for the first half slot */
#define HWD_TXON_CMP7_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x78) /* R/W Compare register for TXON7 for the second half slot */
#define HWD_TXON_CMP8_FIRST_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x7c) /* R/W Compare register for TXON8 for the first half slot */
#define HWD_TXON_CMP8_SECOND_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x80) /* R/W Compare register for TXON8 for the second half slot */

#define HWD_TXON_INT_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0x48) /* R/W Compare register for TX_INT for the first half slot */
#define HWD_TXON_INT_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x4c) /* R/W Compare register for TX_INT for the second half slot */

#define HWD_SPI_ON_CMP_FIRST_READBACK        (HWD_DELAY_LD_RD_BASE_ADDR+0x28) /* R/W Compare register for SPI for the first half slot */
#define HWD_SPI_ON_CMP_SECOND_READBACK       (HWD_DELAY_LD_RD_BASE_ADDR+0x2c) /* R/W Compare register for SPI for the second half slot */

#define HWD_PDM_CMP0_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0x84) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP0_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x88) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP1_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0x8C) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP1_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x90) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP2_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0x30) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP2_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x34) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP3_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0x94) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP3_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0x98) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP4_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0x9C) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP4_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0xA0) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP5_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0xA4) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP5_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0xA8) /* R/W Compare register for TxAGC PDM2 for the second half slot */
#define HWD_PDM_CMP6_FIRST_READBACK          (HWD_DELAY_LD_RD_BASE_ADDR+0xAC) /* R/W Compare register for TxAGC PDM2 for the first half slot */
#define HWD_PDM_CMP6_SECOND_READBACK         (HWD_DELAY_LD_RD_BASE_ADDR+0xB0) /* R/W Compare register for TxAGC PDM2 for the second half slot */

#define HWD_TXON_DLYMODE_READBACK            (HWD_DELAY_LD_RD_BASE_ADDR+0x38) /* R/W TXON Delayed Load Mode register for the first and second half slot */
#define HWD_PDM_DLYMODE_READBACK             (HWD_DELAY_LD_RD_BASE_ADDR+0x3C) /* R/W PDM Delayeojid Load Mode register for the first and second half slot */
#define HWD_SPI_DLYMODE_READBACK             (HWD_DELAY_LD_RD_BASE_ADDR+0x50) /* R/W SPI Delayed Load Mode register for the first and second half slot */

#define HWD_TXON_DELAY_LOAD_DIS_CP_READBACK       (HWD_DELAY_LD_RD_BASE_ADDR+0xD4) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_PDM_DELAY_LOAD_DIS_CP_READBACK       (HWD_DELAY_LD_RD_BASE_ADDR+0x44) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_SPI_DELAY_LOAD_DIS_CP_READBACK       (HWD_DELAY_LD_RD_BASE_ADDR+0xD8) /* comparator disable for CP */ /* use for Delay Loading Register Access - 1 : can access delay load registers, 0 : delay loading function working */
#define HWD_DIS_DELAY_LOAD_CNT_READBACK      (HWD_DELAY_LD_RD_BASE_ADDR+0x54) /* comparator disable for CP */ /* use for Delay Loading counter enable - 1 : disable, 0 : enable */
#define HWD_TXON_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0x60) /* R/W TXON Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */

#define HWD_PDM0_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0xB4) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM1_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0xB8) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM2_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0x58) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM3_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0xBC) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM4_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0xC0) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM5_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0xC4) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM6_DLY_MASK_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0xC8) /* R/W PDM2 Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */

#define HWD_SPI_DLY_MASK_READBACK            (HWD_DELAY_LD_RD_BASE_ADDR+0x5c) /* R/W SPI Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_TXON_DLY_MASK2_READBACK           (HWD_DELAY_LD_RD_BASE_ADDR+0xCC) /* R/W TXON Delayed Load Mask register for the first and second half slot for one shot function - 0:mask delay load, 1:un-mask delay load */
#define HWD_PDM_DLY_1X_DO_READBACK             (HWD_DELAY_LD_RD_BASE_ADDR+0xD0) /* R/W PDM [0:6] 1x mode or DO mode */


#define HWD_VIC_I_CACHE_MASK                  0x00001000 /*; I Cache control mask  */
#define HWD_VIC_D_CACHE_MASK                  0x00000004 /*; D Cache control mask  */
#define HWD_VIC_M_CACHE_MASK                  0x00000001 /*; M Cache control mask  */


      /*--------------------------------------------**
            ** Register Bitfield And Constant Definitions **
            **--------------------------------------------*/

/* HWD_CLK_CTRL0 Clock Control */
#define HWD_UIM_FREQ_SEL      0x8000
#define HWD_MXS_RXF_DIV_ENB   0x4000
#define HWD_CLK2_EN           0x2000
#define HWD_DMA_MDM_TOP_EN    0x1000
#define HWD_GFN_CLK9_EN       0x0800
#define HWD_GTX_CLK9_EN       0x0400
#define HWD_GRX_CLK9_EN       0x0200
#define HWD_MXS_RXF_MAIN_ENB  0x0080
#define HWD_DSPM_BOOT         0x0020
#define HWD_AUDSYS_RST        0x0010
#define HWD_DSPM_RST          0x0008
#define HWD_CLK32SEL          0x0004
#define HWD_GUIM_CLK9_EN      0x0002
#define HWD_MXSG_RST          0x0001

/* HWD_CLK_CTRL1 Clock Control */
#define HWD_AHBDO_CLK_ENB     0x4000
#define HWD_GPCTL_CLK9_ENB    0x2000
#define HWD_DO_CLK_ENB_BYP    0x1000
#define HWD_CPSDIO_CLK_ENB    0x0200
#define HWD_M_OCEM_KILL       0x0080
#define HWD_PPP_CLK_ENB       0x0020
#define HWD_JIT0_CLK_ENB      0x0010
#define HWD_JIT1_CLK_ENB      0x0008
#if (SYS_ASIC == SA_MT6735)
#define HWD_GSPI_CLK9_ENB     0x0002
#endif
#define HWD_TST_CLK_SEL3      0x0001

/* HWD_CLK_CTRL2 Clock Control */
#define HWD_SRCH_CLK_DIV      0x3000
#define HWD_TXDO_CLK_ENB      0x0800
#define HWD_RHA_CLK_EN        0x0400
#define HWD_THA_CLK_EN        0x0200
#define HWD_DSPM_CLK_EN       0x0100
#if (SYS_ASIC > SA_MT6797)
#define HWD_DM_MEM_CLK_DLY_ENB       0x0040
#endif
#define HWD_SRCH_CLK_SEL      0x0030
#define HWD_DM_DIV_CLK_SEL_CP 0x0008
#define HWD_EQ_CLK_ENB        0x0004
#define HWD_STDO_CLK_ENB      0x0002
#define HWD_FNDO_CLK_ENB      0x0001

/* HWD_CLK_CTRL3 Clock Control */
#define HWD_DM_CLK_DIV        0x0700
#define HWD_DM_CLK_DIV_1      0x0100
#define HWD_DM_CLK_DIV_2      0x0200
#define HWD_DM_CLK_DIV_3      0x0300
#define HWD_DM_CLK_DIV_4      0x0400
#define HWD_DM_CLK_DIV_5      0x0500
#define HWD_DM_CLK_DIV_6      0x0600
#define HWD_PROD_CNT_EN       0x0080
#define HWD_CLK_MUX_1_SEL_DEF        0x0000  /* bits [6:0] default = 00 = silent */
#define HWD_CLK_MUX_1_SEL_MASK       0x007F  /* mask for bits [6:0] */
#define HWD_CLK_MUX_1_CG_DSPM_CLK    0x0004
#define HWD_CLK_MUX_1_CG_CLK32       0x0011
#define HWD_CLK_MUX_1_CG_GUIM2_CLK   0x001D
#define HWD_CLK_MUX_1_CG_G_CLK9      0x0035  /* f26m_ck_scan_tstbuf */
#define HWD_CLK_MUX_1_CG_PDM_DCK     0x0038
#define HWD_CLK_MUX_1_GCLK_ARM_PROC  0x004C

/* HWD_CLK_CTRL4 Clock Control */
#define HWD_CLK_60M_SEL       0x0080
#define HWD_CLK_60M_DIV       0x0070
#define HWD_CLK_60M_DIV_1     0x0010
#define HWD_CLK_60M_DIV_2     0x0020
#define HWD_CLK_60M_DIV_3     0x0030
#define HWD_CLK_60M_DIV_4     0x0040
#define HWD_CLK_60M_DIV_5     0x0050
#define HWD_CLK_60M_DIV_6     0x0060
#define HWD_CLK_60M_DISABLE   0x0070
#define HWD_CLK_TDDO_SEL      0x0008
#define HWD_CLK_TDDO_DIV      0x0007
#define HWD_CLK_TDDO_DIV_1    0x0001
#define HWD_CLK_TDDO_DIV_2    0x0002
#define HWD_CLK_TDDO_DIV_3    0x0003
#define HWD_CLK_TDDO_DIV_4    0x0004
#define HWD_CLK_TDDO_DIV_5    0x0005
#define HWD_CLK_TDDO_DIV_6    0x0006
#define HWD_CLK_TDDO_DISABLE  0x0007
#define HWD_AHB_SLEEP_BYP     0x1000
#define HWD_MXS_PDM_CLK_EN    0x0800
#define HWD_ARM_CLK_RATIO      0x0400

/* HWD_CLK_CTRL5 Clock Control */
#define HWD_GPCTL_DO_CLK9_ENB  0x0400
#define HWD_JIT1_CLK_SEL1      0x0200
#define HWD_TST_BUS_CLK_SEL    0x7000
#define HWD_JIT1_CLK_SEL       0x0010
#define HWD_JIT0_CLK_SEL       0x0008
#define HWD_JIT0_CLK_SEL1      0x0004


/* HWD_CLK_CTRL6 Clock Control */
#define HWD_MPU_CLK_ENB       0x0400
#define HWD_UIM2_FREQ_SEL     0x0200
#define HWD_UIM2_ENB          0x0100
#define HWD_GPS_9M8_ENB       0x0010
#define HWD_CI_ROM_ARM_ENB    0x0001

/* HWD_CLK_CTRL7 Clock Control */
#define HWD_GPIO_ENB          0x0080


/* HWD_CLK_CTRL8 Clock Control */
#define HWD_SCH_CLK_DIV_MASK  (3<<12)
#define HWD_SCH_CLK_DIV1      (0<<12)
#define HWD_SCH_CLK_DIV2      (1<<12)
#define HWD_SCH_CLK_DIV3      (2<<12)
#define HWD_VIT_CLK_SEL       (1<<11)
#define HWD_JIT1_CLK_SEL2     0x0400
#define HWD_CLK32_ENB         0x0020


/* HWD_CG_ARM_AMBA_CLKSEL bit definitions */
#define HWD_ARM_PROC_DIV_SEL  0x0003   /* ARM Proc clock divider selection */
#define HWD_ARM_PROC_DIV_1    0x0000   /* CLK_ARM_SRC divided by 1 */
#define HWD_ARM_PROC_DIV_2    0x0001   /* CLK_ARM_SRC divided by 2 */
#define HWD_ARM_PROC_DIV_4    0x0002   /* CLK_ARM_SRC divided by 4 */

#define HWD_AMBA_DIV_SEL      (7<<8)   /* AMBA clock divider factor from CLK_ARM_PROC */
#define HWD_AMBA_DIV_1        (0<<8)   /* AMBA clock divider factor by 1 */
#define HWD_AMBA_DIV_2        (1<<8)   /* AMBA clock divider factor by 2 */
#define HWD_AMBA_DIV_3        (2<<8)   /* AMBA clock divider factor by 3 */
#define HWD_AMBA_DIV_4        (3<<8)   /* AMBA clock divider factor by 4 */
#define HWD_AMBA_DIV_6        (4<<8)   /* AMBA clock divider factor by 6 */
#define HWD_AMBA_DIV_8        (5<<8)   /* AMBA clock divider factor by 8 */
#define HWD_BYP_STANDBYWFI    (1<<14)  /* Bypass STANDBYWFI indicator from ARM core */
#define HWD_BYP_BST_GRANT     (1<<15)  /* Ignore Bus Stop Grant scheme and change clock frequency right away */

/* HWD_CLK_CTRL9 Clock Control */
#if (SYS_ASIC == SA_MT6735)
#define HWD_RFTXSPICLK_GATE_EN   (0x1 << 15)
#define HWD_RFTXSPICLK_SEL_SHFT  (12) 
#define HWD_RFTXSPICLK_SEL       (0x3 << HWD_RFTXSPICLK_SEL_SHFT)
#define HWD_RFRXSPICLK_SEL_SHFT  (8)
#define HWD_RFRXSPICLK_SEL       (0x3 << HWD_RFRXSPICLK_SEL_SHFT)
#endif
#define HWD_UIMSRCCLK_SEL_SHFT   (6)   /* UIM1 Clock Selection */
#if (SYS_ASIC == SA_MT6735)
#define HWD_UIMSRCCLK_SEL        (0x3 << HWD_UIMSRCCLK_SEL_SHFT)
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_UIMSRCCLK_SEL        (0x1 << HWD_UIMSRCCLK_SEL_SHFT)
#endif
#define HWD_UIM2SRCCLK_SEL_SHFT  (4)   /* UIM2 Clock Selection */
#if (SYS_ASIC == SA_MT6735)
#define HWD_UIM2SRCCLK_SEL       (0x3 << HWD_UIM2SRCCLK_SEL_SHFT)   
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_UIM2SRCCLK_SEL       (0x1 << HWD_UIM2SRCCLK_SEL_SHFT)
#endif
#if (SYS_ASIC <= SA_MT6797)
#define HWD_ARM_HIGH_SPEED_SEL   (0x1 << 3)
#endif
#define HWD_CPPLL_SEL            (0x1 << 2)
#define HWD_DSPPLL_SEL           (0x1 << 1)
#define HWD_MDMPLL_SEL           (0x1 << 0)

/* HWD_CLK_CTRL10 Clock Control */
#define HWD_SPH_GHBUS_CK_ENB     (0x1 << 15)
#define HWD_SPH_GDSP_CK_ENB      (0x1 << 14)
#if (SYS_ASIC == SA_MT6735)
#define HWD_RX0_CLK49_MX_ENB     (0x1 << 13)
#define HWD_RX1_CLK49_MX_ENB     (0x1 << 12)
#define HWD_C2KLOG_CLK49_ENB     (0x1 << 11)
#endif
#define HWD_C2KLOG_CLK9_ENB      (0x1 << 10)
#define HWD_PMIC_BCLK_ENB        (0x1 << 9)
#define HWD_F13M_CK_ENB          (0x1 << 8)
#if (SYS_ASIC >= SA_MT6755)
#if (SYS_ASIC <= SA_MT6797)
#define HWD_SRC_MODE             (0x1 << 4)
#endif
#define HWD_CPBSI1_CLK_ENB       (0x1 << 3)
#define HWD_RX_CLK104_D_CLK_ENB  (0x1 << 2)
#define HWD_RX_CLK104_M_CLK_ENB  (0x1 << 1)
#define HWD_C2KLOG_CLK39_CLK_ENB (0x1 << 0)

/* HWD_CLK_CTRL11 Clock Control */
#define HWD_CPMIPI3_CLK_ENB            (0x1 << 15)
#define HWD_CPMIPI2_CLK_ENB            (0x1 << 14)
#define HWD_CPMIPI1_CLK_ENB            (0x1 << 13)
#define HWD_CPMIPI0_CLK_ENB            (0x1 << 12)
#define HWD_TXDET_CLK19_26_CLK_ENB     (0x1 << 11)
#define HWD_TXDET_CLK9_13_CLK_ENB      (0x1 << 10)
#define HWD_TXDET_CLK98_104_CLK_ENB    (0x1 << 9)
#define HWD_TX_CLK104_1X_CLK_ENB       (0x1 << 8)
#define HWD_TX_CLK104_DO_CLK_ENB       (0x1 << 7)
#define HWD_TX_CLK13_1X_CLK_ENB        (0x1 << 6)
#define HWD_TX_CLK13_DO_CLK_ENB        (0x1 << 5)
#define HWD_RX_CLK13_D_CLK_ENB         (0x1 << 4)
#define HWD_RX_CLK13_M_CLK_ENB         (0x1 << 3)
#define HWD_PHACC_CLK13_1X_CLK_ENB     (0x1 << 2)
#define HWD_PHACC_CLK13_DO_CLK_ENB     (0x1 << 1)
#define HWD_CLK104_FREE_ENB            (0x1 << 0)
#endif

/* 123 MHz TBD: SysDelay must be recalibrated for the new processor clock speed */
#define HWD_CLK_CP_PROC_PLL_M         410 
#define HWD_CLK_CP_PROC_PLL_N         64


/* TX_TEST_CTRL Bit definitions */
#define HWD_TX_TC_SWAP_IQ_CDMA    0x0100
/* "01" - 307.2  kHz test tone is muxed into the Tx filter
   "10" - 614.4 kHz test tone is muxed into Tx filter
   "11" - 1.2288 MHz test tone is muxed into the Tx 2x interpolator. 
   The test tone has amplitude TX_TEST_VAL. */
#define HWD_TX_TC_307K_TONE       0x0040
#define HWD_TX_TC_614K_TONE       0x0080
#define HWD_TX_TC_1228K_TONE      0x00c0
#define HWD_TX_TC_TONE_SEL        0x00c0
#define HWD_TX_TC_CALIB_EN_INT    0x0010
#define HWD_TX_TC_SWAP_IQ_AMPS    0x0002
#define HWD_TX_TC_TEST_MODE       0x0001

/* TX_ON_GPO */
#define HWD_TX_GPO_SEL_TXON0    0x0020
#define HWD_TX_GPO_SEL_TXON1    0x0040
#define HWD_TX_GPO_SEL_TXON2    0x0080
#define HWD_TX_GPO_SEL_TXON3    0x0100
#define HWD_TX_GPO_SEL_TXON4    0x0200
#define HWD_TX_GPO_SEL_TXON_ALL (HWD_TX_GPO_SEL_TXON0 | \
            HWD_TX_GPO_SEL_TXON1 | HWD_TX_GPO_SEL_TXON2 | \
            HWD_TX_GPO_SEL_TXON3 | HWD_TX_GPO_SEL_TXON4 )

/*------------------------------------------------------------------------
* CP Timers
*------------------------------------------------------------------------*/
/* HWD_CTS_CT_CNT_CTRL - Counter Control Reg Bit positions */
#define HWD_CTS_UNMSK_ST_1X_RST 0x04
#define HWD_CTS_UNMSK_ST_DO_RST 0x04
#define HWD_CTS_ENABLE_CTR      0x02      /* ENABLE BIT for CTS0 and CTS1 */
#define HWD_CTS_RESET_CNT       0x01

/* HWD_CTSx_EN - CTS Enable Reg Bit positions */
#define HWD_CTS0_ENABLE_CTR  0x01      /* ENABLE BIT for CTS0 */
#define HWD_CTS1_ENABLE_CTR  0x01      /* ENABLE BIT for CTS1 */
#define HWD_CTS2_ENABLE_CTR  0x01      /* ENABLE BIT for CTS2 */
#define HWD_CTS3_ENABLE_CTR  0x01      /* ENABLE BIT for CTS3 */

/* HWD_CTSx_REF_SEL - CTS Reference Frequence Select Reg values */
#define HWD_CTS_REF_FREQ_8KHz    0x00
#define HWD_CTS_REF_FREQ_80KHz   0x01

/* HWD_CTS_CT_RDY_FLG_N - CTS Ready Flag */
#define HWD_CTS_READY_BIT    0x01

/* HWD_CNT80K_TC  - 80kHz Counter Control Reg Bit positions */
#define HWD_CTS_80K_ENABLE_CTR 0x01

/*------------------------------------------------------------------------
* CP Interrupts
*------------------------------------------------------------------------*/
/* HWD_CP_ISR0_L & HWD_CP_ISR0_H - Interrupt bit positions */
#define HWD_INT_RESYNC       0x8000  /* System resync event */
#if (SYS_ASIC == SA_MT6735)
#define HWD_INT_RF_SER       0x4000  /* Serial Programmer 0 interrupt flag */
#endif
#define HWD_INT_REGWAKE      0x2000  /* Register wakeup interrupt */
#define HWD_INT_UIM1TX       0x1000  /* UIM1 Tx interrupt */
#define HWD_INT_UIM1RX       0x0800  /* UIM1 Rx interrupt */
#define HWD_INT_SLP1X        0x0400  /* Sleep Timer Wakeup */
#define HWD_INT_SLPDO        0x0200  /* Sleep Timer end flag */
#define HWD_INT_AUDIO        0x0100  /* Pure SW interrupt from audio to CPI,  R*/
#if (SYS_ASIC == SA_MT6735)
#define HWD_INT_TX_SPI       0x0080  /* TX SPI interrupt */
#endif
#define HWD_INT_PMIC_CTRL    0x0040  /* C2K interrnal pmic controller interrupt, R */
#define HWD_INT_PMIC_WRAP_ERR 0x0020  /* pmic_wrap error interrupt, including cmd miss, channel under/pverrun, etc, R */
#define HWD_INT_M2CDR        0x0008  /* Modem to CP Data Read Ready */
#define HWD_INT_M2CCR        0x0004  /* Modem to CP Control Read Ready */
#define HWD_INT_M2CDW        0x0002  /* Modem to CP Data Write Ready */
#define HWD_INT_M2CCW        0x0001  /* Modem to CP Control Write Ready */

/* For both RF SPI and TX SPI */
#define HWD_INT_SER_ALL      (HWD_INT_RF_SER | HWD_INT_TX_SPI)

/* HWD_CP_ISR1_L & HWD_CP_ISR1_H - Interrupt bit positions */
#define HWD_INT_TIM11        0x8000  /* Timer 11 Interrupt */
#define HWD_INT_TIM10        0x4000  /* Timer 10 Interrupt */
#define HWD_INT_TIM9         0x2000  /* Timer 9 Interrupt */
#define HWD_INT_TIM8         0x1000  /* Timer 8 Interrupt */
#define HWD_INT_CS_RTOS_WAKE 0x0200  /* RTOS sleep over Interrupt */
#define HWD_INT_RX_DCOC_DIV  0x0100  /* Interrupt from RX DFE, not to dm_int */
#define HWD_INT_RX_DCOC_MAIN 0x0080  /* Interrupt from RX DFE, not to dm_int */
#define HWD_INT_M2CFM        0x0040  /* Fast MailBox to CP from DSPM Interrupt */
#define HWD_INT_SDIO_S       0x0020  /* SDIO Slave interrupt */
#define HWD_INT_WIRAM        0x0010  /* Write IRAM  */
#define HWD_INT_TIM3         0x0008  /* Timer 3 Interrupt */
#define HWD_INT_TIM2         0x0004  /* Timer 2 Interrupt */
#define HWD_INT_TIM1         0x0002  /* Timer 1 Interrupt */
#define HWD_INT_TIM0         0x0001  /* Timer 0 Interrupt */
#define HWD_INT_CTS_ALL      (HWD_INT_TIM0 | HWD_INT_TIM1 | HWD_INT_TIM2  | HWD_INT_TIM3 | HWD_INT_TIM8 | HWD_INT_TIM9 | HWD_INT_TIM10 | HWD_INT_TIM11)
                             
/* HWD_CP_ISR2_L & HWD_CP_ISR2_H - Interrupt bit positions */
#if (SYS_ASIC == SA_MT6735)
#define HWD_INT_MD_CCIF5     0x8000  /* CCIF interrupt 5 for MD->C2k communication */
#define HWD_INT_MD_CCIF4     0x4000  /* CCIF interrupt 4 for MD->C2k communication */
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_INT_FRC_DO_SYNC  0x8000  /* DO FRC time sync intterrupt */
#define HWD_INT_FRC_1X_SYNC  0x4000  /* 1X FRC time sync intterrupt */
#endif 
#define HWD_INT_UAWAK0       0x2000  /* UART0 Wakeup Interrupt */
#if (SYS_ASIC == SA_MT6735)
#define HWD_INT_MD_CCIF3     0x1000  /* CCIF interrupt 3 for MD->C2k communication */
#define HWD_INT_RTOS         0x0800  /* RTOS Interrupt */
#define HWD_INT_MD_CCIF2     0x0400  /* CCIF interrupt 2 for MD->C2k communication */
#define HWD_INT_MD_CCIF1     0x0200  /* CCIF interrupt 1 for MD->C2k communication */
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_INT_PS_C2K_PCCIF3     0x1000  /* PCCIF interrupt 3 for MD1 PScore->C2k communication */
#define HWD_INT_RTOS         0x0800  /* RTOS Interrupt */
#define HWD_INT_PS_C2K_PCCIF2     0x0400  /* PCCIF interrupt 2 for MD1 PScore->C2k communication */
#define HWD_INT_PS_C2K_PCCIF1     0x0200  /* PCCIF interrupt 1 for MD1 Pscore->C2k communication */
#endif
#define HWD_INT_UIM0TX       0x0080  /* User Interface Transmit Interrupt */
#define HWD_INT_UIM0RX       0x0040  /* User Interface Receive Interrupt */
#define HWD_INT_UAWAK1       0x0020  /* UART1 Wakeup Interrupt */
#define HWD_INT_UAWAK2       0x0010  /* UART2 Wakeup Interrupt */
#if (SYS_ASIC == SA_MT6735)
#define HWD_INT_MD_CCIF0     0x0008  /* CCIF interrupt 0 for MD->C2k communication */
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_INT_PS_C2K_PCCIF0     0x0008  /* PCCIF interrupt 0 for MD1 Pscore->C2k communication */
#endif
#define HWD_INT_ARMCLK_CHG_DONE     0x0004  /* ARM and AHB/EBIF clock ratio change */
#define HWD_INT_USBWAL       0x0002  /* USB Wakeup Interrupt */
#define HWD_INT_AP_CCIF      0x0001  /* CCIF interrupt 0 for AP->C2k communication */

/* HWD_CP_ISR3_L & HWD_CP_ISR3_H - Interrupt bit positions */
#define HWD_INT_PDU          0x4000  /* PDU Interrupt */
#define HWD_INT_MPU_INT      0x2000  /* Memory protection unit fault interrupt */
#define HWD_INT_SWINT1       0x1000  /* Software Interrupt 1 */
#define HWD_INT_SWINT0       0x0800  /* Software Interrupt 0 */
#define HWD_INT_SDIO_WAKE    0x0100  /* Indication the wake up from deep sleep source was SDIO host */
#define HWD_INT_GPIO7        0x0080  /* GPINT 7 Interrupt */
#define HWD_INT_GPIO6        0x0040  /* GPINT 6 Interrupt */
#define HWD_INT_GPIO5        0x0020  /* GPINT 5 Interrupt */
#define HWD_INT_GPIO4        0x0010  /* GPINT 4 Interrupt */
#define HWD_INT_GPIO3        0x0008  /* GPINT 3 Interrupt */
#define HWD_INT_GPIO2        0x0004  /* GPINT 2 Interrupt */
#define HWD_INT_GPIO1        0x0002  /* GPINT 1 Interrupt */
#define HWD_INT_GPIO0        0x0001  /* GPINT 0 Interrupt */
#define HWD_INT_GPINT3       (HWD_INT_GPIO0 | HWD_INT_GPIO1 | HWD_INT_GPIO2 | HWD_INT_GPIO3 | \
                              HWD_INT_GPIO4 | HWD_INT_GPIO5 | HWD_INT_GPIO6 | HWD_INT_GPIO7)

/* HWD_CP_ISR4_L & HWD_CP_ISR4_H - Interrupt bit positions */
#define HWD_INT_DO_CELLSWRDY 0x8000  /* DO CSM Cell Switch Ready Interrupt */
#define HWD_INT_DO_C2I       0x4000  /* DO C2I interrupt  */
#define HWD_INT_DMA_TX_MDM   0x2000  /* DMA Tx modem interrupt */
#define HWD_INT_DMA_MDM_RX   0x1000  /* DMA Rx modem interrupt  */
#define HWD_INT_DMA_GENERIC  0x0800  /* Generic DMA interrupt */
#define HWD_INT_D0_PPP_ENC1  0x0400  /* D0 PPP Hardware Accelerator Encoder No. 1 */
#define HWD_INT_D0_PPP_ENC0  0x0200  /* D0 PPP Hardware Accelerator Encoder No. 0 */
#define HWD_INT_D0_PPP_DEC1  0x0100  /* D0 PPP Hardware Accelerator Decoder No. 1 */
#define HWD_INT_D0_PPP_DEC0  0x0080  /* D0 PPP Hardware Accelerator Decoder No. 0 */
#define HWD_INT_DO_RXC_INT   0x0040  /* EVDO Rx controller interrupt */
#define HWD_INT_DO_FN_TST    0x0020  /* EVDO finger tst interrupt  */
#define HWD_INT_DO_FN_SLOTFOUND 0x0010  /* DO finger slot found, FN_PDET_INT*/
#define HWD_INT_DO_HALFSLOT  0x0008  /* DO Outer-Loop Half-Slot interrupt */
#define HWD_INT_DO_SLOT      0x0004  /* DO Outer-Loop Slot interrupt */
#define HWD_INT_DO_SYSTIME   0x0002  /* DO System Time Slot interrupt (confirm w/ASIC) */
#define HWD_INT_DO_SRCH      0x0001  /* DO Searcher interrupt */

/* HWD_CP_ISR5_L & HWD_CP_ISR5_H - Interrupt bit positions */
#define HWD_INT_UART1         0x4000  /* UART1 Interrupt */
#define HWD_INT_UART0         0x2000  /* UART0 Interrupt */
#define HWD_INT_1X_SYSTIME    0x1000  /* CDMA system timer interrupt */
#define HWD_INT_TIM7          0x0800  /* Timer 7 Interrupt */
#define HWD_INT_TIM6          0x0400  /* Timer 6 Interrupt */
#define HWD_INT_TIM5          0x0200  /* Timer 5 Interrupt */
#define HWD_INT_TIM4          0x0100  /* Timer 4 Interrupt */
#define HWD_INT_DMA7_GENERIC  0x0080  /* Generic DMA channel 7 interrupt */
#define HWD_INT_DMA6_GENERIC  0x0040  /* Generic DMA channel 6 interrupt */
#define HWD_INT_DMA5_GENERIC  0x0020  /* Generic DMA channel 5 interrupt */
#define HWD_INT_DMA4_GENERIC  0x0010  /* Generic DMA channel 4 interrupt */
#define HWD_INT_DMA3_GENERIC  0x0008  /* Generic DMA channel 3 interrupt */
#define HWD_INT_DMA2_GENERIC  0x0004  /* Generic DMA channel 2 interrupt */
#define HWD_INT_DMA1_GENERIC  0x0002  /* Generic DMA channel 1 interrupt */
#define HWD_INT_DMA0_GENERIC  0x0001  /* Generic DMA channel 0 interrupt */
#define HWD_INT_DMA_GENERIC_SHIFT 0x0
#define HWD_INT_CTS2_ALL      (HWD_INT_TIM4 | HWD_INT_TIM5 | HWD_INT_TIM6  | HWD_INT_TIM7)

/* HWD_CP_ISR6_L & HWD_CP_ISR6_H - Interrupt bit positions */
#define HWD_INT_GPIO15        0x8000  /* GPINT 15 Interrupt */
#define HWD_INT_GPIO14        0x4000  /* GPINT 14 Interrupt */
#define HWD_INT_GPIO13        0x2000  /* GPINT 13 Interrupt */
#define HWD_INT_GPIO12        0x1000  /* GPINT 12 Interrupt */
#define HWD_INT_GPIO11        0x0800  /* GPINT 11 Interrupt */
#define HWD_INT_GPIO10        0x0400  /* GPINT 10 Interrupt */
#define HWD_INT_GPIO9         0x0200  /* GPINT 9 Interrupt */
#define HWD_INT_GPIO8         0x0100  /* GPINT 8 Interrupt */
#if (SYS_ASIC >= SA_MT6755)
#define HWD_INT_CTI           HWD_INT_GPIO11  /* C2K_CTI_IRQ Interrupt */
#endif
#define HWD_INT_ABBMIX2       HWD_INT_GPIO10 /* Abb mixedsys rx2 overload Interrupt */
#define HWD_INT_ABBMIX1       HWD_INT_GPIO9  /* Abb mixedsys rx1 overloaInterrupt */
#define HWD_INT_BLUETOOTH     HWD_INT_GPIO8  /* BT phone call Interrupt */
#define HWD_INT_UU67          0x0080  /* Unused bit 7 Interrupt */
#if (SYS_ASIC ==SA_MT6735)
#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
#define HWD_INT_TIMER_INT2_1X 0x0040  /* 1x sys timer INT2 */
#define HWD_INT_TIMER_INT1_1X 0x0020  /* 1x sys timer INT1 */
#define HWD_INT_TIMER_INT0_1X 0x0010  /* 1x sys timer INT0 */
#define HWD_INT_TIMER_INT2_DO 0x0008  /* DO sys timer INT2 */
#define HWD_INT_TIMER_INT1_DO 0x0004  /* DO sys timer INT1 */
#define HWD_INT_TIMER_INT0_DO 0x0002  /* DO sys timer INT0 */
#define HWD_INT_TIMER_INT    (HWD_INT_TIMER_INT0_DO | HWD_INT_TIMER_INT1_DO | HWD_INT_TIMER_INT2_DO|HWD_INT_TIMER_INT0_1X|HWD_INT_TIMER_INT1_1X|HWD_INT_TIMER_INT2_1X)
#else
#define HWD_INT_UU66          0x0040  /* Unused bit 6 Interrupt */
#define HWD_INT_UU65          0x0020  /* Unused bit 5 Interrupt */
#define HWD_INT_UU64          0x0010  /* Unused bit 4 Interrupt */
#define HWD_INT_UU63          0x0008  /* Unused bit 3 Interrupt */
#define HWD_INT_UU62          0x0004  /* Unused bit 2 Interrupt */
#define HWD_INT_RESYNCGPS     0x0002  /* System resynchronization event for GPS */
#endif
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_INT_TIMER_INT2_1X 0x0040  /* 1x sys timer INT2 */
#define HWD_INT_TIMER_INT1_1X 0x0020  /* 1x sys timer INT1 */
#define HWD_INT_TIMER_INT0_1X 0x0010  /* 1x sys timer INT0 */
#define HWD_INT_TIMER_INT2_DO 0x0008  /* DO sys timer INT2 */
#define HWD_INT_TIMER_INT1_DO 0x0004  /* DO sys timer INT1 */
#define HWD_INT_TIMER_INT0_DO 0x0002  /* DO sys timer INT0 */
#define HWD_INT_TIMER_INT    (HWD_INT_TIMER_INT0_DO | HWD_INT_TIMER_INT1_DO | HWD_INT_TIMER_INT2_DO|HWD_INT_TIMER_INT0_1X|HWD_INT_TIMER_INT1_1X|HWD_INT_TIMER_INT2_1X)
#endif
#define HWD_INT_RESYNCDO      0x0001  /* System resynchronization event for DO */
#define HWD_INT_GPINT6        (HWD_INT_GPIO11 | HWD_INT_GPIO12 | HWD_INT_GPIO13 | HWD_INT_GPIO14 | HWD_INT_GPIO15)

/* HWD_CP_ISR7_L & HWD_CP_ISR7_H - Interrupt bit positions */
#define HWD_INT_SW_BUS        0x0100  /* AHB Matrix Interrupt */

#if (SYS_ASIC >= SA_MT6755)
/* HWD_CP_ISR8_L & HWD_CP_ISR8_H - Interrupt bit positions */
#define HWD_INT_PS_C2K_CCIRQ3        0x0800  /* MD3<->MD1 PScore  rccif interrupt3*/
#define HWD_INT_PS_C2K_CCIRQ2        0x0400  /* MD3<->MD1 PScore  rccif interrupt2*/
#define HWD_INT_PS_C2K_CCIRQ1        0x0200  /* MD3<->MD1 PScore  rccif interrupt1*/
#define HWD_INT_PS_C2K_CCIRQ0        0x0100  /* MD3<->MD1 PScore  rccif interrupt0*/
#define HWD_INT_L1_C2K_CCIRQ7        0x0080  /* MD3<->MD1 L1core  rccif interrupt7*/
#define HWD_INT_L1_C2K_CCIRQ6        0x0040  /* MD3<->MD1 L1core  rccif interrupt6*/
#define HWD_INT_L1_C2K_CCIRQ5        0x0020  /* MD3<->MD1 L1core  rccif interrupt5*/
#define HWD_INT_L1_C2K_CCIRQ4        0x0010  /* MD3<->MD1 L1core  rccif interrupt4*/
#define HWD_INT_L1_C2K_CCIRQ3        0x0008  /* MD3<->MD1 L1core  rccif interrupt3*/
#define HWD_INT_L1_C2K_CCIRQ2        0x0004  /* MD3<->MD1 L1core  rccif interrupt2*/
#define HWD_INT_L1_C2K_CCIRQ1        0x0002  /* MD3<->MD1 L1core  rccif interrupt1*/
#define HWD_INT_L1_C2K_CCIRQ0        0x0001  /* MD3<->MD1 L1core  rccif interrupt0*/

#define HWD_INT_MD1_CCIRQ           (HWD_INT_L1_C2K_CCIRQ0 | HWD_INT_L1_C2K_CCIRQ1 | \
                                     HWD_INT_L1_C2K_CCIRQ2 | HWD_INT_L1_C2K_CCIRQ3 | \
                                     HWD_INT_L1_C2K_CCIRQ4 | HWD_INT_L1_C2K_CCIRQ5 | \
                                     HWD_INT_L1_C2K_CCIRQ6 | HWD_INT_L1_C2K_CCIRQ7 | \
                                     HWD_INT_PS_C2K_CCIRQ0 | HWD_INT_PS_C2K_CCIRQ1 | \
                                     HWD_INT_PS_C2K_CCIRQ2 | HWD_INT_PS_C2K_CCIRQ3)
#endif

/* HWD_C2C_SW0 & HWD_C2C_SW1 bit positions */
#define HWD_SW_INT_GEN       0x0001

#ifdef SYS_OPTION_EVDO
#define HWD_INT_DMA_ALL_GENERIC   (HWD_INT_DMA0_GENERIC | HWD_INT_DMA1_GENERIC | \
                                   HWD_INT_DMA2_GENERIC | HWD_INT_DMA3_GENERIC|  \
                                   HWD_INT_DMA4_GENERIC | HWD_INT_DMA5_GENERIC |  \
                                   HWD_INT_DMA6_GENERIC | HWD_INT_DMA7_GENERIC)  /* Generic DMA interrupts */
#else
#define HWD_INT_DMA_ALL_GENERIC   (HWD_INT_DMA0_GENERIC | HWD_INT_DMA1_GENERIC | \
                                   HWD_INT_DMA2_GENERIC | HWD_INT_DMA3_GENERIC|  \
                                   HWD_INT_DMA4_GENERIC | HWD_INT_DMA5_GENERIC )  /* Generic DMA interrupts */
#endif


/* System time interrupt mask definitions */
#define HWD_PCG_CNT_INT_MASK        0x0001 /* PCG counter mask */
#define HWD_FRAME_CNT_INT_MASK      0x8000 /* Frame counter mask */
#define HWD_AUX_IMMED_WRITE_READY   0x0100 /* bit mask for aux immediate write interrupt */
#define HWD_AUX_TIMED_WRITE_READY   0x0200 /* bit mask for aux timed write interrupt     */
#define HWD_PDM_IMMED_WRITE_READY   0x0400 /* bit mask for pdm immediate write interrupt */
#define HWD_PDM_TIMED_WRITE_READY   0x0800 /* bit mask for pdm timed write interrupt     */

/* CP_MBM_CHS Modem Mailbox Handshake Register bit position */
#define HWD_MMB_WHS         0x01
#define HWD_MMB_RHS         0x01

/* Vocoder Fast Mailbox Handshake Register bit position */
#define HWD_VMB_F_RHS       0x01

/* CP_MBV_CHS Vocoder Mailbox Handshake Register bit position */
#define HWD_VMB_WHS         0x01    /* lsb of HWD_MBV_C2V_WRDY */
#define HWD_VMB_RHS         0x01    /* lsb of HWD_MBV_C2V_RRDY */

/*-------------------------------**
** CP Interrupt Priority Sorting **
**-------------------------------*/

/* HWD_INTPRY_CTRL_IRQ and HWD_INTPRY_CTRL_FIQ bit definitions */
#define HWD_INT_SRT_SEL     0x02    /* 1=ints come from sorting block, 0=ints come from orig. CPI */
#define HWD_IVR_EN          0x01    /* 1=sorting is enabled, 0=sorting is disabled */

/* HWD_IVR_IDX_IRQ and HWD_IVR_IDX_FIQ bit definitions */
#define HWD_IVR_IDX_VALID   0x8000  /* [15] indicates if current IVR index is valid */
#define HWD_IVR_IDX_MASK    0x00FF  /* [7:4] = STAT number, [3:0] = interrupt bit number */


/*-------------------------------------------------*/

/* TST_RAMMD register mode values */
#define HWD_TST_NORMAL      0x00
#define HWD_TST_RX_SAMP     0x08
#define HWD_TST_CP_WR       0x09
#define HWD_TST_TX_SAMP     0x0A
#define HWD_TST_RX_RD       0x0C
#define HWD_TST_CP_RD       0x0D
#define HWD_TST_RX_SIGDEL   0x0E

/* RX_CHAN_SEL - Rx Channel Select */
#define HWD_RX_CHAN_SEL_SYNC      0
#define HWD_RX_CHAN_SEL_TRAFFIC   1
#define HWD_RX_CHAN_SEL_PAGING    3

/* RF and TX ON control */
#define HWD_ON_CFG_ACTIVE_HI 4
#define HWD_ON_CFG_CMOS    2
#define HWD_ON_CFG_DRIVE   1
#define HWD_TX_MODEM_ON    0x0001   /* enable Tx modem, Tx modem state machines
                                     * are proceed as normal */
#define HWD_TX_MODEM_OFF   0        /* disable Tx modem, Tx modem state machines
                                     * are forced to initial states */

#define HWD_ON_MODE_IMD    0

#define HWD_ON_OFF         0
#define HWD_ON_ON          1
#define HWD_ON_AUTO        2

/*--------------------------------------------------------------------
* Sleep Control (HWD_CS_SLP_CTRL) Register Bit definitions
*--------------------------------------------------------------------*/
#define HWD_SLPCTRL_1X_SLP_EN_MASK           (1<<0)
#define HWD_SLPCTRL_DO_SLP_EN_MASK           (1<<1)
#define HWD_SLPCTRL_WAKEUP_1X_FSM_MASK       (1<<2)
#define HWD_SLPCTRL_WAKEUP_DO_FSM_MASK       (1<<3)
#define HWD_SLPCTRL_FLIGHT_MODE_N_MASK       (1<<4)
#if (SYS_ASIC > SA_MT6797)
#define HWD_SLPCTRL_DSPM_MEM_SLEEP           (1<<5)
#endif
#define HWD_SLPCTRL_PSO_MODE_MASK            (1<<14)

#define HWD_WAKEUP_FSM_MASK_ALL (HWD_SLPCTRL_1X_SLP_EN_MASK     | HWD_SLPCTRL_DO_SLP_EN_MASK | \
                                 HWD_SLPCTRL_WAKEUP_1X_FSM_MASK | HWD_SLPCTRL_WAKEUP_DO_FSM_MASK)

/*--------------------------------------------------------------------
* Sleep Status (HWD_CS_SLP_STATUS) Register Bit definitions
*--------------------------------------------------------------------*/
#define HWD_SLP_STATUS_1X_IN_DS   (1<<0)
#define HWD_SLP_STATUS_DO_IN_DS   (1<<1)
#define HWD_SLP_STATUS_DSPM_IN_LS (1<<3)

/*--------------------------------------------------------------------
* OSC Control (HWD_CS_OSC_CTRL) Register Bit definitions
*--------------------------------------------------------------------*/


/*--------------------------------------------------------------------
* Resync Control (HWD_CS_RESYNC_CTL) Register Bit definitions
*--------------------------------------------------------------------*/
#define HWD_RESYNC_CTL_1X_CAL_EN_MASK        (1<<0)
#define HWD_RESYNC_CTL_1X_RESYNC_EN_MASK     (1<<1)
#define HWD_RESYNC_CTL_DO_CAL_EN_MASK        (1<<2)
#define HWD_RESYNC_CTL_DO_RESYNC_EN_MASK     (1<<3)

#define HWD_RESYNC_CTL_1X_RESYNC_FRC_MASK    (1<<5)
#define HWD_RESYNC_CTL_DO_RESYNC_FRC_MASK    (1<<6)
#define HWD_RESYNC_CTL_INI_LOAD_EN_1X        (1<<7)
#define HWD_RESYNC_CTL_INI_LOAD_EN_EV        (1<<8)

#if (SYS_ASIC == SA_MT6735)
/*--------------------------------------------------------------------
* Resync Control (HWD_CS_PSO_LTCH_EN) Register Bit definitions
*--------------------------------------------------------------------*/
#define HWD_CS_PSO_LTCH_EN_1X                (1<<0)
#define HWD_CS_PSO_LTCH_EN_DO                (1<<1)
#endif

/*--------------------------------------------------------------------------------
* External wake enable Sleep Control (HWD_CS_EXT_WAKE_EN) Register Bit definitions
*--------------------------------------------------------------------------------*/
#define HWD_EXT_WAKE_GPINT(num)              ((num < 12)? (1<<num) : (1<<(21+num-12)))
#define HWD_EXT_WAKE_UIM0_HOT_PLUG_IN        HWD_EXT_WAKE_GPINT(4)
#define HWD_EXT_WAKE_UIM1_HOT_PLUG_IN        HWD_EXT_WAKE_GPINT(5)
#define HWD_EXT_WAKE_AP2C2K_RDY              HWD_EXT_WAKE_GPINT(6)
#define HWD_EXT_WAKE_AP2C2K_WAKE             HWD_EXT_WAKE_GPINT(7) /* Default unmask */
#if (SYS_ASIC >= SA_MT6755)
#define HWD_EXT_WAKE_C2K_ECT_IRQ             HWD_EXT_WAKE_GPINT(11)
#endif
#define HWD_EXT_WAKE_RTOS_SLP_OVER           (1 << 12)
#define HWD_EXT_WAKE_UART0                   (1 << 13)
#define HWD_EXT_WAKE_UART1                   (1 << 14)
#define HWD_EXT_WAKE_SDIO                    ((uint32) 1 << 16)
#if (SYS_ASIC >= SA_MT6755)
#define HWD_EXT_WAKE_PSTOC2K_RCCIF           ((uint32) 1 << 18)
#define HWD_EXT_WAKE_L1TOC2K_RCCIF           ((uint32) 1 << 20)
#endif
#define HWD_EXT_WAKE_APCFGC2K_13             (uint32)HWD_EXT_WAKE_GPINT(13)
#define HWD_EXT_WAKE_APCFGC2K_14             (uint32)HWD_EXT_WAKE_GPINT(14)
#define HWD_EXT_WAKE_COUNTER                 ((uint32) 1 << 29)
#define HWD_EXT_WAKE_CCIF_AP2C2K_EVT         ((uint32) 1 << 30)
#if (SYS_ASIC == SA_MT6735)
#define HWD_EXT_WAKE_CCIF_MD2C2K_EVT         ((uint32) 1 << 31)
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_EXT_WAKE_PSTOC2K_PCCIF           ((uint32) 1 << 31)
#endif

/*--------------------------------------------------------------------------------
* GPINT CTL (HWD_CS_GPINT_CTRL) Register Bit definitions
*--------------------------------------------------------------------------------*/
#define HWD_CS_GPINT_CTL_GPINT0  0x0001
#define HWD_CS_GPINT_CTL_GPINT1  0x0002
#define HWD_CS_GPINT_CTL_GPINT2  0x0004
#define HWD_CS_GPINT_CTL_GPINT3  0x0008
#define HWD_CS_GPINT_CTL_GPINT4  0x0010
#define HWD_CS_GPINT_CTL_GPINT5  0x0020
#define HWD_CS_GPINT_CTL_GPINT6  0x0040
#define HWD_CS_GPINT_CTL_GPINT7  0x0080
#define HWD_CS_GPINT_CTL_GPINT8  0x0100
#define HWD_CS_GPINT_CTL_GPINT9  0x0200
#define HWD_CS_GPINT_CTL_GPINT10 0x0400
#define HWD_CS_GPINT_CTL_GPINT11 0x0800
#define HWD_CS_GPINT_CTL_GPINT12 0x1000
#define HWD_CS_GPINT_CTL_GPINT13 0x2000
#define HWD_CS_GPINT_CTL_GPINT14 0x4000
#define HWD_CS_GPINT_CTL_GPINT15 0x8000

/*--------------------------------------------------------------------------------
*  (HWD_FPM_LPM_CNT_CFG) Register Bit definitions
*--------------------------------------------------------------------------------*/
#define HWD_SW_RSTB              0x0001
#define HWD_RTC_CNT_EN           0x0002


#if (SYS_ASIC >= SA_MT6755) 
/*--------------------------------------------------------------------------------
*  (HWD_C2K_FRC_SHADOW_RDY) Register Bit definitions
*--------------------------------------------------------------------------------*/
#define HWD_CS_FRC_SHADOW_READY  0x0001

/*--------------------------------------------------------------------------------
*  (HWD_RFPOR_VRF18_EN) Register Bit definitions
*--------------------------------------------------------------------------------*/
#define HWD_RFPOR_EN             0x0001
#define HWD_VRF18_EN             0x0002
#define HWD_CCIF_MD_WAKEUP_EN    0x0004

/*--------------------------------------------------------------------------------
*  (HWD_C2K_CONFIG) Register Bit definitions
*--------------------------------------------------------------------------------*/

#define HWD_C2K_MD1SRC_REQ_EN    (1<<3)
#define HWD_C2K_MD1SRC_ACK_EN    (1<<0)
#endif

/*--------------------------------------------------------------------
* Define RTOS timer control register bit definitions
*--------------------------------------------------------------------*/

#define HWD_RTOS_ENABLE       0x01

/*--------------------------------------------------------------------
* UART Interrupt Enable Register (IER) Bit definitions
*--------------------------------------------------------------------*/

#define HWD_IER_RX_HOLDING_INT 0x01 /* b0 - Receive Holding Register interrupt  - Enabled When Set   */
#define HWD_IER_TX_HOLDING_INT 0x02 /* b1 - Transmit Holding Register interrupt - Enabled When Set   */
#define HWD_IER_LINE_STAT_INT  0x04 /* b2 - Receiver Line Status interrupt      - Enabled When Set   */
#define HWD_IER_MODEM_STAT_INT 0x08 /* b3 - Modem Status Register interrupt     - Enabled When Set   */
#define HWD_IER_INTS_OFF       0x00 /*      Turn off all interrupts                                  */
#define HWD_IER_INTS_ON        0x07 /*      Turn on all interrupts except modem status               */

/*--------------------------------------------------------------------
* UART FIFO Control Register (FCR) Bit definitions
*--------------------------------------------------------------------*/

#define HWD_FCR_FIFO_ENABLE     0x01 /* b0 - Tx and Rx FIFO Enable               - Enabled When Set   */
#define HWD_FCR_RX_FIFO_RESET   0x02 /* b1 - Clear Rx FIFO and reset its counter - Clears When Set    */
#define HWD_FCR_TX_FIFO_RESET   0x04 /* b2 - Clear Tx FIFO and reset its counter - Clears When Set    */
#define HWD_FCR_FIFO_TRIG_4     0x40
#define HWD_FCR_FIFO_TRIG_8     0x80
#define HWD_FCR_FIFO_TRIG_14    0xC0

/* FCR b7 - b6 FIFO Trigger Level  */

#define HWD_FCR_RX_TRIG_LVL_01  0x00 /* 0 0 - FIFO Rx Trigger Level 01 */
#define HWD_FCR_RX_TRIG_LVL_04  0x40 /* 0 1 - FIFO Rx Trigger Level 04 */
#define HWD_FCR_RX_TRIG_LVL_08  0x80 /* 1 0 - FIFO Rx Trigger Level 08 */
#define HWD_FCR_RX_TRIG_LVL_16  0xC0 /* 1 1 - FIFO Rx Trigger Level 16 */

/*Used by UART1 && UART2 */
#define HWD_FCR_FIFO1024_TRIG_1   0x00
#define HWD_FCR_FIFO1024_TRIG_8   0x20
#define HWD_FCR_FIFO1024_TRIG_16  0x40
#define HWD_FCR_FIFO1024_TRIG_32  0x60
#define HWD_FCR_FIFO1024_TRIG_64  0x80
#define HWD_FCR_FIFO1024_TRIG_128 0xA0
#define HWD_FCR_FIFO1024_TRIG_512 0xC0
#define HWD_FCR_FIFO1024_TRIG_1022  0xE0

/*--------------------------------------------------------------------
* UART Latch Control Register (LCR) Bit definitions
*--------------------------------------------------------------------*/
/* LCR b0 defines number of bits 0=7, 1=8 */
/* LCR b2 defines number of stop bits.  0=1, 1=2 */
#define HWD_LCR_7_BIT_WORD_1    0x00 /* 0 0 0  - 7 Bit Word - 1 Stop Bit   */
#define HWD_LCR_8_BIT_WORD_1    0x01 /* 0 0 1  - 8 Bit Word - 1 Stop Bit   */
#define HWD_LCR_7_BIT_WORD_2    0x04 /* 1 0 0  - 7 Bit Word - 2 Stop Bit   */
#define HWD_LCR_8_BIT_WORD_2    0x05 /* 1 0 1  - 8 Bit Word - 2 Stop Bit   */

#define HWD_LCR_PARITY_ENABLE   0x08 /* b3 - Enable Parity Bit Generation and Check - Enabled When Set */
#define HWD_LCR_PARITY_EVEN     0x10 /* b4 - Odd/Even Parity Generation and Check   - Even When Set    */
#define HWD_LCR_PARITY_SET      0x20 /* b5 - Toggle Generated Parity Bit 0/1        - 0 When Set       */
#define HWD_LCR_BREAK_SET       0x40 /* b6 - Force Break Control ( Tx o/p low)      - Forced When Set  */
#define HWD_LCR_DIVISOR_LATCH   0x80 /* b7 - Enable internal Baud Rate Latch        - Enabled When Set */

/*--------------------------------------------------------------------
* UART Modem Control Register (MCR) Bit definitions
*--------------------------------------------------------------------*/

#define HWD_MCR_DTR_BIT         0x01 /* b0 - Set DTR Signal Low/High - DTR Low when Set */
#define HWD_MCR_DTR_ASSERT      0x01 /* Assert DTR */
#define HWD_MCR_DTR_DEASSERT    0x00 /* Deassert DTR */

                                                                     /* MCR b1-b3 is not used  */
#define HWD_MCR_LOOPBACK_MODE   0x10 /* b4 - Loopback(Test) Mode Enable            - Enabled When Set */
#define HWD_MCR_AUTO_CTS              0x20 /* b5 - Enable Auto-CTS (TX flow control */

/* The Following Registers are Status Registers which Report conditions within the UART/PPP during  *
 * operation. The defined values are masks to ensure that the register flags are correctly accessed */

/*--------------------------------------------------------------------
* UART Interrupt Status Register (ISR) Bit definitions
*--------------------------------------------------------------------*/

/* ISR b0 indicates that an interrupt is pending when clear. b3 - b1 signal which interrupt as per:- */

#define HWD_ISR_INT_PEND        0x01
#define HWD_ISR_MODEM_SOURCE    0x00 /* 0 0 0 - Modem Status Register         Priority 4 */
#define HWD_ISR_TX_RDY_SOURCE   0x02 /* 0 0 1 - Transmitter Holding Reg Empty Priority 3 */
#define HWD_ISR_RX_RDY_SOURCE   0x04 /* 0 1 0 - Received Data Ready           Priority 2 */
#define HWD_ISR_LSR_SOURCE      0x06 /* 0 1 1 - Receiver Line Status Register Priority 1 */
#define HWD_ISR_RX_RDY_TO_SRC   0x0C /* 1 1 0 - Received Data Ready Time Out  Priority 2 */

/* ISR b7 - b4 are not used - in st16c552 b7 - b6 are Set b5 - b4 are Clear */

/*--------------------------------------------------------------------
* UART Line Status Register (LSR) Bit definitions
*--------------------------------------------------------------------*/

#define HWD_LSR_RX_DATA_READY   0x01 /* b0 - Data Received and Saved in Holding Reg - Set when Valid */
#define HWD_LSR_OVERRUN_ERROR   0x02 /* b1 - Overrun Error Occurred                 - Set When Valid */
#define HWD_LSR_PARITY_ERROR    0x04 /* b2 - Received Data has Incorrect Parity     - Set When Valid */
#define HWD_LSR_FRAMING_ERROR   0x08 /* b3 - Framing Error (No Stop Bit)            - Set When Valid */
#define HWD_LSR_BREAK_INTERRUPT 0x10 /* b4 - Break Signal Received                  - Set When Valid */
#define HWD_LSR_TX_HOLD_EMPTY   0x20 /* b5 - Tx Holding Register is empty and ready - Set When Valid */
#define HWD_LSR_TX_FIFO_EMPTY   0x40 /* b6 - Tx Shift Registers and FIFO are Empty  - Set When Valid */
#define HWD_LSR_FIFO_ERROR      0x80 /* b7 - At Least one of b4 - b2 has occurred   - Set When Valid */

/*--------------------------------------------------------------------
* UART Modem Status Register (MSR) Bit definitions
*--------------------------------------------------------------------*/

#define HWD_MSR_CTS_CHANGE      0x01 /* b0 - Set When CTS Input has Changed State */
#define HWD_MSR_DSR_CHANGE      0x02 /* b1 - Set When DSR Input has Changed State */
#define HWD_MSR_RI_CHANGE       0x04 /* b2 - Set When RI  Input has Changed State */
#define HWD_MSR_CD_CHANGE       0x08 /* b3 - Set When CD  Input has Changed State */

#define HWD_MSR_CTS_BIT         0x10 /* b4 - RTS Equivalent during loopback - inverse of CTS */
#define HWD_MSR_CTS_ASSERT      0x10 /*      CTS asserted */
#define HWD_MSR_CTS_DEASSERT    0x00 /*      CTS deasserted */

                                                                     /* MSR b5-b7 is not used  */


/*--------------------------------------------------------------------
* UART ScratchPad Register (SPR) Bit definitions
*--------------------------------------------------------------------*/

/* This is a user register for any required bit storage required */

#define HWD_SPR_USER0           0x01
#define HWD_SPR_USER1           0x02
#define HWD_SPR_USER2           0x04
#define HWD_SPR_USER3           0x08
#define HWD_SPR_USER4           0x10
#define HWD_SPR_USER5           0x20
#define HWD_SPR_USER6           0x40
#define HWD_SPR_USER7           0x80

/*------------------------------------------------------------------------
* UART Divisor Latch Lower and Upper Byte Values (DLL DLM) Bit definitions
*------------------------------------------------------------------------*/

/* Baud Rate Time Constant t = (Baud Rate Clock Frequency)/(Baud Rate * 16)
   These are the required 16 bit divisor values for the internal baud rate
   based on the define baud clock frequency */
#define BAUD_CLOCK_FREQ         3686400

/* This macro adds 0.5 to the divisor constant which the compiler truncates */
#define BAUD_DIVISOR(baud)      ((BAUD_CLOCK_FREQ/(baud*8)+1)>>1)

#define BAUD_DIV_MSB(baud)      (((BAUD_DIVISOR(baud)) >> 8) & 0xff)
#define BAUD_DIV_LSB(baud)      ((BAUD_DIVISOR(baud)) & 0xff)

#define HWD_DLM_57600_BAUD      BAUD_DIV_MSB(57600)
#define HWD_DLL_57600_BAUD      BAUD_DIV_LSB(57600)

#define HWD_DLM_115200_BAUD     BAUD_DIV_MSB(115200)
#define HWD_DLL_115200_BAUD     BAUD_DIV_LSB(115200)

#define HWD_DLM_230400_BAUD     BAUD_DIV_MSB(230400)
#define HWD_DLL_230400_BAUD     BAUD_DIV_LSB(230400)

/* Baud Rate Time Constant t = (Baud Rate Clock Frequency)/(Baud Rate * 16)
   These are the required 16 bit divisor values for the internal baud rate
   based on the define baud clock frequency */
#define BAUD_CLOCK_FREQ_DATA    2457600

/* This macro adds 0.5 to the divisor constant which the compiler truncates */
#define BAUD_DIVISOR_DATA(baud) ((BAUD_CLOCK_FREQ_DATA/(baud*8)+1)>>1)

#define BAUD_DIV_MSB_DATA(baud) (((BAUD_DIVISOR_DATA(baud)) >> 8) & 0xff)
#define BAUD_DIV_LSB_DATA(baud) ((BAUD_DIVISOR_DATA(baud)) & 0xff)

#define HWD_DLM_300_BAUD       BAUD_DIV_MSB_DATA(300)
#define HWD_DLL_300_BAUD       BAUD_DIV_LSB_DATA(300)

#define HWD_DLM_600_BAUD       BAUD_DIV_MSB_DATA(600)
#define HWD_DLL_600_BAUD       BAUD_DIV_LSB_DATA(600)

#define HWD_DLM_1200_BAUD       BAUD_DIV_MSB_DATA(1200)
#define HWD_DLL_1200_BAUD       BAUD_DIV_LSB_DATA(1200)

#define HWD_DLM_2400_BAUD       BAUD_DIV_MSB_DATA(2400)
#define HWD_DLL_2400_BAUD       BAUD_DIV_LSB_DATA(2400)

#define HWD_DLM_4800_BAUD       BAUD_DIV_MSB_DATA(4800)
#define HWD_DLL_4800_BAUD       BAUD_DIV_LSB_DATA(4800)

#define HWD_DLM_9600_BAUD       BAUD_DIV_MSB_DATA(9600)
#define HWD_DLL_9600_BAUD       BAUD_DIV_LSB_DATA(9600)

#define HWD_DLM_19200_BAUD      BAUD_DIV_MSB_DATA(19200)
#define HWD_DLL_19200_BAUD      BAUD_DIV_LSB_DATA(19200)

#define HWD_DLM_38400_BAUD      BAUD_DIV_MSB_DATA(38400)
#define HWD_DLL_38400_BAUD      BAUD_DIV_LSB_DATA(38400)

#define HWD_DLM_153600_BAUD     BAUD_DIV_MSB_DATA(153600)
#define HWD_DLL_153600_BAUD     BAUD_DIV_LSB_DATA(153600)


/*** The following "_DS_" definitions are used by the RAM D2 chips ***/
/* Baud Rate Time Constant t = (Baud Rate Clock Frequency)/(Baud Rate * 16) 
   These are the required 16 bit divisor values for the internal baud rate 
   based on the define baud clock frequency*/
#define BAUD_DS_CLOCK_FREQ_DATA    9600

/* This macro adds 0.5 to the divisor constant, which the compiler truncates */
#define BAUD_DS_DIVISOR_DATA(baud) ((BAUD_DS_CLOCK_FREQ_DATA/(baud*8)+1)>>1)

#define BAUD_DS_DIV_MSB_DATA(baud) (((BAUD_DS_DIVISOR_DATA(baud)) >> 8) & 0xff)
#define BAUD_DS_DIV_LSB_DATA(baud) ((BAUD_DS_DIVISOR_DATA(baud)) & 0xff)

#define HWD_DS_DLM_600_BAUD        BAUD_DS_DIV_MSB_DATA(600)
#define HWD_DS_DLL_600_BAUD        BAUD_DS_DIV_LSB_DATA(600)

#define HWD_DS_DLM_300_BAUD        BAUD_DS_DIV_MSB_DATA(300)
#define HWD_DS_DLL_300_BAUD        BAUD_DS_DIV_LSB_DATA(300)

/* Baud Rate Time Constant t = (Baud Rate Clock Frequency)/(Baud Rate * 16)
   These are the required 16 bit divisor values for the internal baud rate
   based on the define baud clock frequency */
#define BAUD_CLOCK_HIGH_FREQ    14745600

/* This macro adds 0.5 to the divisor constant which the compiler truncates */
#define BAUD_DIVISOR_HIGH_FREQ(baud) ((BAUD_CLOCK_HIGH_FREQ/(baud*8)+1)>>1)

#define BAUD_DIV_MSB_HIGH_FREQ(baud) (((BAUD_DIVISOR_HIGH_FREQ(baud)) >> 8) & 0xff)
#define BAUD_DIV_LSB_HIGH_FREQ(baud) ((BAUD_DIVISOR_HIGH_FREQ(baud)) & 0xff)

#define HWD_DLM_921_BAUD       BAUD_DIV_MSB_HIGH_FREQ(921600)
#define HWD_DLL_921_BAUD       BAUD_DIV_LSB_HIGH_FREQ(921600)


/*------------------------------------------------------------------------
 * PWR_OFF_CTRL bit definitions
 *------------------------------------------------------------------------*/
#define HWD_PWR_OFF_DAT        0x00000001  /* power off level, active low                           */
#define HWD_PWR_OFF_OE         0x00000002  /* output enable, active low                             */

/*------------------------------------------------------------------------
* Ringer Memory definitions
*------------------------------------------------------------------------*/

#define HWD_RINGER_MEMORY_SIZE         2048  /* words */
#define HWD_RINGER_MEMORY_CHANNELS     1     /* words */

/*------------------------------------------------------------------------
* HWD_BOND_OPTION bit definitions
*------------------------------------------------------------------------*/
#define HWD_BOND_DSPV           (1 << 0)   /* CP can assert/deassert DSPv reset */
#define HWD_BOND_REVA           (1 << 1)   /* CP can enable/disable Rev A feature */
#define HWD_BOND_1X             (1 << 2)   /* CP can enable/disable the 1x transmitter clk */
#define HWD_BOND_DO             (1 << 3)   /* CP can enable/disable the DO transmitter clk */
#define HWD_BOND_GSM            (1 << 4)   /* Bond option for GSM baseband */
#define HWD_BOND_GPS            (1 << 5)   /* Bond option for GPS baseband */
#define HWD_BOND_H264           (1 << 6)   /* Bond option for MPEG4 and AVC function */
#define HWD_BOND_DRAM           (1 << 7)   /* Bond option for DDR feature */
#define HWD_BOND_2TCXO          (1 << 8)   /* Bond option for 2 TCXO's in system */
#define HWD_BOND_CAMERA         (1 << 9)   /* Bond option for camera subsystem function */

/*------------------------------------------------------------------------
* HWD_CG_JIT2_CLKSEL bit definitions
*------------------------------------------------------------------------*/
#define HWD_CG_JIT2_CLKSEL_CP    0
#define HWD_CG_JIT2_CLKSEL_AMBA  1
#define HWD_CG_JIT2_CLKSEL_MDM   2
#define HWD_CG_JIT2_CLKSEL_DSP   3

/*------------------------------------------------------------------------
* HWD_ST_CPINT_FR bit definitions
*------------------------------------------------------------------------*/

#define HWD_ST_FR_TYPE_BIT   0x0001
#define HWD_ST_FR_TYPE_20MS  0x0000
#define HWD_ST_FR_TYPE_26MS  0x0001

/*------------------------------------------------------------------------
* HWD_ST_CPINT_SRC bit definitions
*------------------------------------------------------------------------*/

#define HWD_ST_CPINT_ALL     0xFFFF

/*------------------------------------------------------------------------
* Configuration register definitions
*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
* HWD_DM_GPIO_FUNC Register Bit definitions
*------------------------------------------------------------------------*/
#define HWD_DM_MMC_EN         0x0007

/*------------------------------------------------------------------------
* HWD_DAI_CTRL Bit definitions
*------------------------------------------------------------------------*/
#define HWD_DAI_MIC_EN        0x0001
#define HWD_DAI_SPKR_EN       0x0002
#define HWD_EXT_CODEC_EN      0x0004
#define HWD_FSYNC_MODE0       0x0008
#define HWD_FSYNC_MODE1       0x0010
#define HWD_FSYNC_MODE2       0x0020
#define HWD_INV_CLK_BIT       0x0040
#define HWD_INV_VDIFS_BIT     0x0080
#define HWD_INV_VDOFS_BIT     0x0100
#define HWD_INV_DI            0x0200
#define HWD_INV_DO            0x0400
#define HWD_DO_EDGE_SEL       0x0800
#define HWD_DI_EDGE_SEL       0x1000

/*------------------------------------------------------------------------
* Watchdog timeout value in msecs, Max value = 31,999 msecs
* Soft Watchdog is 7.5 econds.
*------------------------------------------------------------------------*/
#if (SYS_ASIC == SA_MT6735)
#define HWD_WD_TIMEOUT_VALUE        11000
#elif (SYS_ASIC >= SA_MT6755)
#define HWD_WD_TIMEOUT_VALUE        16000
#endif

/*------------------------------------------------------------------------
* CP spare register bit definitions
*------------------------------------------------------------------------*/
#define HWD_DSPM_NMI_ENABLE           0x4  /* Enable NMI */

/*------------------------------------------------------------------------
* HWD_TXSD_OSC_CTRL Bit definitions (for external 32K)
*------------------------------------------------------------------------*/
#define HWD_TXSD_OSC_BYPASS        0x0001  /* Bypass bit for 32K Osc */
#define HWD_TXSD_OSC_CURRENT_MODE  0x0002  /* Current mode bit for 32K Osc */
#define HWD_TXSD_OSC_POWER_DOWN    0x0004  /* Power bit for 32K Osc */


/*--------------------------**
** CP Sleep and Calibration **
**--------------------------*/
/*------------------------------------------------------------------------
* UIM hot plug eint0 con register bit definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_DEBOUNCE_POL 0x0800 /* bit 11: 0-Positive,1-Negative */
#define HWD_UIM_DEBOUNCE_EN   0x8000 /* bit 15 */

/*******************
** UIM REGISTER   **
********************/
/*------------------------------------------------------------------------
* UIM control register bit definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_TX_TO_RX         0x4000   /* bit 14 UIM tx to rx */
#define HWD_UIM_PWR              0x1000   /* bit 12 UIM Power */
#define HWD_UIM_RSTN             0x0800   /* bit 11 UIM card reset */
#define HWD_UIM_SFT_RSTN         0x0100   /* bit 8 UIM soft reset */
#define HWD_UIM_TX_EN            0x0080   /* bit 7 Tx mode enable */
#define HWD_UIM_RX_EN            0x0040   /* bit 6 Rx mode enable */
#define HWD_UIM_SPEED_BITS       0x0030   /* bit 5:4 00 for default speed(UIM_CLK/372) */
#define HWD_UIM_SPEED_ENHANCE_64 0x0010   /* 01 for UIM_CLK/64 */
#define HWD_UIM_SPEED_ENHANCE_32 0x0020   /* 1x for UIM_CLK/32 */
#define HWD_UIM_CK_OFF           0x0008   /* Inactive level of UIM_CLK */
#define HWD_UIM_CKE              0x0004   /* clock enable   */
#define HWD_UIM_CONV             0x0002   /* direct convention */
#define HWD_UIM_AUTO             0x0001   /* automatic direct convention */

/*------------------------------------------------------------------------
* UIM Rx status register bit definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_RX_GT_TRIG       0x0020   /* buffer level is equal to or below UIM_RX_BUF_TRIG */
#define HWD_UIM_RX_FRERR         0x0010   /* Rx framing error */
#define HWD_UIM_RX_OVRERR        0x0008   /* Rx overrun */
#define HWD_UIM_RX_PERR          0x0004   /* Rx parity error   */
#define HWD_UIM_INV              0x0002   /* inverse convention */
#define HWD_UIM_WWT_EXP          0x0001   /* WWT timer expired */

/*------------------------------------------------------------------------
* UIM WWT control register bit definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_WWT_EN           0x0001   /* WWT timer enable */

/*------------------------------------------------------------------------
* UIM Tx interrupt mask register bit definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_BUF_TRIG_MSK     0x0002   /* disable trigger level interrupt */
#define HWD_UIM_BUF_EMP_MSK      0x0001   /* disable empty interrupt */

/*------------------------------------------------------------------------
* UIM Tx status register bit definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_TX_PERR          0x0004   /* Tx parity error */
#define HWD_UIM_BUF_LT_TRIG      0x0002   /* buffer level has fallen below UIM_TX_BUF_TRIG  */
#define HWD_UIM_BUF_EMP          0x0001   /* Tx buffer is empty */

/*------------------------------------------------------------------------
* UIM Tx/Rx FIFO buffer size definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_TX_BUF_SIZE      64
#define HWD_UIM_RX_BUF_SIZE      64

#if (SYS_ASIC >= SA_MT6755)
/*------------------------------------------------------------------------
* UIM baud ctrl definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_BAUD_EN_BIT      0x8000 

/*------------------------------------------------------------------------
* UIM IO ctrl definitions
*------------------------------------------------------------------------*/
#define HWD_UIM_IO_EN_BIT      0x10
#define HWD_UIM_IO_DATA_BIT    0x01
#endif

/*------------------------------------------------------------------------
* MuxPDU Memory Register bit definitions
*------------------------------------------------------------------------*/

/* HWD_MPDU_CTL **********************/
/* Bits [1:0] MuxPDU Type */
#define HWD_MPCTL_MUXTYPE_MASK   0x0003
#define HWD_MPCTL_MUXTYPE1       0
#define HWD_MPCTL_MUXTYPE2       1
#define HWD_MPCTL_MUXTYPE3       2
#define HWD_MPCTL_MUXTYPE5       3

/* Bit [2] Decoder Type */
#define HWD_MPCTL_DECODER_MASK   0x0004
#define HWD_MPCTL_TURBO          (0<<2)
#define HWD_MPCTL_VITERBI        (1<<2)

/* Bit [3] Single or Double LTUs */
#define HWD_MPCTL_LTUTYPE_MASK   0x0008
#define HWD_MPCTL_SINGLE         (0<<3)
#define HWD_MPCTL_DOUBLE         (1<<3)

/* Bit [4] Rate Set */
#define HWD_MPCTL_RS_MASK        0x0010
#define HWD_MPCTL_RS1            (0<<4)
#define HWD_MPCTL_RS2            (1<<4)

/* Bits [6:5] Number of PDUs */
#define HWD_MPCTL_NUMPDUS_MASK   0x0060
#define HWD_MPCTL_1_PDUS         (0<<5)
#define HWD_MPCTL_2_PDUS         (1<<5)
#define HWD_MPCTL_4_PDUS         (2<<5)
#define HWD_MPCTL_8_PDUS         (3<<5)

/* Bit [7] RLP Bits */
#define HWD_MPCTL_RLPBITS_MASK   0x0080
#define HWD_MPCTL_RLPBITS0       (0<<7)
#define HWD_MPCTL_RLPBITS2       (1<<7)

/* Bit [8] Channel Type */
#define HWD_MPCTL_CHNL_MASK      0x0100
#define HWD_MPCTL_SCH            (0<<8)
#define HWD_MPCTL_PDCH           (1<<8)

/* Bits [10:9] Operation Mode */
#define HWD_MPCTL_MODE_MASK      0x0600
#define HWD_MPCTL_MODE_NORMAL    (0<<9)
#define HWD_MPCTL_MODE_BYPASS    (1<<9)
#define HWD_MPCTL_MODE_RESERVE   (2<<9)
#define HWD_MPCTL_MODE_TEST      (3<<9)

/* Bit [11] Check LTU Flag */
#define HWD_MPCTL_CHECKLTU_MASK  0x0800
#define HWD_MPCTL_CHECK_LTU      (0<<11)
#define HWD_MPCTL_DONTCHECK_LTU  (1<<11)

/* Bit [12] Reset Trigger */
#define HWD_MPCTL_SOFT_RST       (1<<12)

/* Bit [13] Initiate Test Mode */
#define HWD_MPCTL_INIT_TST       (1<<13)

/* Bit [14] CP->Hwd Test Data Ready Indicator */
#define HWD_MPCTL_CP_VLD_WD      (1<<14)

/* Bit [15] Clock Control */
#define HWD_MPCTL_CLOCKED_NORMAL (0<<15)
#define HWD_MPCTL_CLOCKED_BY_CP  (1<<15)

/* Bit [16] Turbo Decoder Delay */
#define HWD_MPCTL_TD_DELAY_NORMAL (0<<16)
#define HWD_MPCTL_TD_DELAY_FORCED (1<<16)


/* HWD_MPDU_TYPE5_CTL *****************/
/* Bit [0] SRID1 Type */
#define HWD_MP5CTL_SRID1_PRI     0
#define HWD_MP5CTL_SRID1_SEC     1

/* Bit [1] SRID2 Type */
#define HWD_MP5CTL_SRID2_PRI     (0<<1)
#define HWD_MP5CTL_SRID2_SEC     (1<<1)

/* Bit [2] SRID3 Type */
#define HWD_MP5CTL_SRID3_PRI     (0<<2)
#define HWD_MP5CTL_SRID3_SEC     (1<<2)

/* Bit [3] SRID4 Type */
#define HWD_MP5CTL_SRID4_PRI     (0<<3)
#define HWD_MP5CTL_SRID4_SEC     (1<<3)

/* Bit [4] SRID5 Type */
#define HWD_MP5CTL_SRID5_PRI     (0<<4)
#define HWD_MP5CTL_SRID5_SEC     (1<<4)

/* Bit [5] SRID6 Type */
#define HWD_MP5CTL_SRID6_PRI     (0<<5)
#define HWD_MP5CTL_SRID6_SEC     (1<<5)

/* Bits [7:6] Number of LTUs */
#define HWD_MP5CTL_NUMLTUS_MASK  0x00C0
#define HWD_MP5CTL_1_LTUS        (0<<6)
#define HWD_MP5CTL_2_LTUS        (1<<6)
#define HWD_MP5CTL_4_LTUS        (2<<6)
#define HWD_MP5CTL_8_LTUS        (3<<6)

/* Bits [11:8] Bits per PDU */
#define HWD_MP5CTL_BITS_MASK     0x0F00
#define HWD_MP5CTL_BITS_SHIFT    8
#define HWD_MP5CTL_BITS_172      (0<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_267      (1<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_360      (2<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_552      (3<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_744      (4<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_1128     (5<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_1512     (6<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_2280     (7<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_3048     (8<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_4584     (9<<HWD_MP5CTL_BITS_SHIFT)
#define HWD_MP5CTL_BITS_6120     (10<<HWD_MP5CTL_BITS_SHIFT)

/* Bit [12] Last Word Test */
#define HWD_MP5CTL_LAST_WD_TST   (1<<12)

/* Bits [17:13] SCH Frame Size */
#define HWD_MP5CTL_SIZE_BITS_SHIFT     13
#define HWD_MP5CTL_CP_TD_SCH_SIZ_MASK  0x0003E000

/* Bits [15:13] PDCH Frame Size */
#define HWD_MP5CTL_CP_TD_PDCH_SIZ_MASK 0x0000E000

/* HWD_MPDU_TIMER_INIT ***************/
#define HWD_MPTMR_VALUE_MAX      0x3F

/*------------------------------------------------------------------------
* RF SPI bit definitions
*------------------------------------------------------------------------*/
/* HWD_SER_GENL register defintiion */
#define HWD_RF_SPI_CLKTRI  (1<<11)  /* 1 => SER_RF_CLK is CMOS.0 => SER_RF_CLK is open-drain */
#define HWD_RF_SPI_DATTRI  (1<<10)  /* 1 => SER_RF_DAT is CMOS.0 => SER_RF_DAT is open-drain */
#define HWD_RF_SPI_LDTRI1  (1<<6)   /* 1 => SER_RF_LDTX pin is CMOS.0 => SER_RF_LDTX pin is open-drain.
                                       RFSP_GENL[LEV1] bit must  be set to '0'*/
#define HWD_RF_SPI_LDTRI0  (1<<5)   /* 1 => SER_RF_LDRX is CMOS.0 => SER_RF_LDRX is open-drain. 
                                       RFSP_GENL[LEV0] bit must be set to '0' */
#define HWD_RF_SPI_LEV1    (1<<1)   /* 0 => Active low enable level for RF Tx device.
                                       1 => Active high enable level for RF Tx device */
#define HWD_RF_SPI_LEV0    (1<<0)   /* 0 => Active low enable level for RF Rx device
                                       1 => Active high enable level for RF Rx device */
#define HWD_RF_SPI_REG     (HWD_RF_SPI_LDTRI0 | HWD_RF_SPI_LDTRI1 | HWD_RF_SPI_DATTRI | HWD_RF_SPI_CLKTRI)


/*--------------------------------------------------------------------
* IRDA Bypass Register Bit definitions
*--------------------------------------------------------------------*/
#define HWD_UART_IRDA_AUTO           0x01 /* Automatically block RX */
#define HWD_UART_IRDA_BLOCK_RX       0x02 /* Block RX while transmitting */
#define HWD_UART_IRDA_IRDA           0x04 /* Uart is in IrDA mode */
#define HWD_UART_IRDA_TX             0x08 /* Uart is in TX state */

/*------------------------------------------------------------------------
* HWD_M_RXSD_DO_1X_SEL bit definitions
*------------------------------------------------------------------------*/
#define HWD_RXSD_1X_SEL  (0<<0)           /* 0 => Connect RX filter 1X 4-bit output to Rx Derotator */
#define HWD_RXSD_DO_SEL  (1<<0)           /* 1 => Connect RX filter DO 6-bit output to Rx Derotator */


/*------------------------------------------------------------------------
* HWD_TX_CP_DSPM_SEL bit definitions
*------------------------------------------------------------------------*/
#define HWD_TX_ON_INT_SEL  (1<<0)           /* 1 => DSPM control TX_ON_INT */
#define HWD_TX_ON4_SEL     (1<<1)           /* 1 => DSPM control TX_ON4 */
#define HWD_TX_ON0_SEL     (1<<2)           /* 1 => DSPM control TX_ON0 */
#define HWD_TX_ON1_SEL     (1<<3)           /* 1 => DSPM control TX_ON1 */
#define HWD_TX_ON2_SEL     (1<<4)           /* 1 => DSPM control TX_ON2 */
#define HWD_TX_ON3_SEL     (1<<5)           /* 1 => DSPM control TX_ON3 */
#define HWD_RF_ON0_SEL     (1<<6)           /* 1 => DSPM control RF_ON0 */
#define HWD_RF_ON1_SEL     (1<<7)           /* 1 => DSPM control RF_ON1 */
#define HWD_RF_ON2_SEL     (1<<8)           /* 1 => DSPM control RF_ON2 */
#define HWD_RF_ON3_SEL     (1<<9)           /* 1 => DSPM control RF_ON3 */
#define HWD_TX_ON5_SEL     (1<<10)          /* 1 => DSPM control TX_ON5 */
#define HWD_TX_ON6_SEL     (1<<11)          /* 1 => DSPM control TX_ON6 */
#define HWD_RF_ON4_SEL     (1<<12)          /* 1 => DSPM control RF_ON4 */
#define HWD_RF_ON5_SEL     (1<<13)          /* 1 => DSPM control RF_ON5 */
#define HWD_RF_ON6_SEL     (1<<14)          /* 1 => DSPM control RF_ON6 */
#define HWD_RF_ON7_SEL     (1<<15)          /* 1 => DSPM control RF_ON7 */
#define HWD_TX_ON7_SEL     (1<<16)          /* 1 => DSPM control TX_ON7 */
#define HWD_TX_ON_INT2_SEL (1<<17)          /* 1 => DSPM control TX_ON_INT2 */

/*---------------------------------------------------------*
* HWD_MPU_BLOCK0_CTL - HWD_MPU_BLOCK15_CTL Bit definitions *
*---------------------------------------------------------*/

#define HWD_MPU_BLOCK_SIZE_MASK  0x3      /* [1:0] - Block size MASK                      */
#define HWD_MPU_BLOCK_SIZE_1B    0x0      /* [1:0] - Block size is 1 byte (default)       */
#define HWD_MPU_BLOCK_SIZE_2B    0x1      /* [1:0] - Block size is 2 byte                 */
#define HWD_MPU_BLOCK_SIZE_4B    0x2      /* [1:0] - Block size is 4 byte                 */
#define HWD_MPU_BLOCK_SIZE_8B    0x3      /* [1:0] - Block size is 8 byte                 */

#define HWD_MPU_BLOCK_REN        (1<<2)   /* [2] - read-protection,  0=dis,1=en           */
#define HWD_MPU_BLOCK_WEN        (1<<3)   /* [3] - write-protection, 0=dis,1=en           */

#define HWD_MPU_BLOCK_MS_SHIFT   (4)      /* Amount of shift for Master Mask (bits[10:4]) */
#define HWD_MPU_BLOCK_MS_MASK    (0x7F)   /* Mask of             Master Mask (bits[10:4]) */

/*-----------------------------------------------------*
* HWD_MPU_SEG0_REG - HWD_MPU_SEG15_REG Bit definitions *
*-----------------------------------------------------*/

#define HWD_MPU_SEG_SIZE_MASK    0x7      /* [2:0] - Segment size MASK, 0-7 = 2,4,...,256MBytes,0MBytes */

#define HWD_MPU_SEG_REN          (1<<3)   /* [3] - read-protection,  0=dis,1=en                         */
#define HWD_MPU_SEG_WEN          (1<<4)   /* [4] - write-protection, 0=dis,1=en                         */

#define HWD_MPU_SEG_MS_SHIFT     (5)      /* Amount of shift for Master Mask (bits[11:5])               */
#define HWD_MPU_SEG_MS_MASK      (0x7F)   /* Mask of             Master Mask (bits[11:5])               */

/*-------------------------------------*
* HWD_MPU_FAULT_STATUS Bit definitions *
*-------------------------------------*/

#define HWD_MPU_FVALID_BLK       1        /* [0] - 1=there is a populated-memory fault, 0=not           */
#define HWD_MPU_FTYPE_BLK_WRITE  (1<<1)   /* [1] - 1=Block Fault type, 1=Write, 0=Read                  */

#define HWD_MPU_FVALID_SEG       (1<<2)   /* [2] - 1=there is an unpopulated-memory fault, 0=not        */
#define HWD_MPU_FTYPE_SEG_WRITE  (1<<3)   /* [3] - 1=Segment Fault type, 1=Write, 0=Read                */

#define HWD_MPU_FLTSRC_SHIFT     4        /* Amount of shift for Fault master number                    */
#define HWD_MPU_FLTSRC_MASK      (7<<4)   /* [6:4] - Fault master number mask                           */

 
/*-------------------------------------*
* CHIP ID definitions                  *
*-------------------------------------*/

/* CHIP ID Masks product (7.0, 7.1, ...) and revision (A0, A1, B0, ...) */
#define HWD_CHIPID_PRODUCT_MASK    0xFF00
#define HWD_CHIPID_REVISION_MASK   0x00FF

/* Chip product ID's (CBP7.0, CBP7.1, ...) */
#define HWD_CHIPID_PRODUCT_CBP70   0x7000
#define HWD_CHIPID_PRODUCT_CBP71   0x7100

/* Chip revisions (A0, A1, B0, ...) */         
#define HWD_CHIPID_REVISION_A0     0x00A0
#define HWD_CHIPID_REVISION_A1     0x00A1


/*---------------------------------------------------------------**          
** CP Reset Generator for  Peripheral - HWD_SRESET          **          
**----------------------------------------------------------------*/   
#define HWD_SRESET_CGPIO_RST_BIT   (((uint32)0x01)<<13)
#define HWD_SRESET_MPU_RST_BIT     (((uint32)0x01)<<15)


/*-------------------------------------------*/
/*  HWD PROFILE Register and bitfield defs */
/*-------------------------------------------*/
#define HWD_PRFL_MEM          (HWD_CP_BASE_ADDR+0x7000)     /* R/W  thread cycle counts (size=256x32)           */
#define HWD_PRFL_CTL          (HWD_CP_BASE_ADDR+0x7400)     /* R/W  control                              0x0000 */
#define HWD_PRFL_INDX         (HWD_CP_BASE_ADDR+0x7404)     /* R/W  thread index                                */

/* HWD_PRFL_CTL register */
#define HWD_PRFL_EN              (1<<0)      /* bit[0] 1=enable, 0=disable                         */
#define HWD_PRFL_MEM_CLR         (1<<1)      /* bit[0] 1=clear thread cycle counts, 0=do not clear */


/********************************************************
*Following AHB Matrix and AHB2AXI Definition 
*******************************************************/
#define HWD_MAIM_BASE            0x50000000
/*  slave port priority arbiter algorithm setting */
#define HWD_MAIM_SP0_AAR         (HWD_MAIM_BASE+0x00) 
#define HWD_MAIM_SP0_MPR         (HWD_MAIM_BASE+0x04)
#define HWD_MAIM_SP1_AAR         (HWD_MAIM_BASE+0x08)
#define HWD_MAIM_SP1_MPR         (HWD_MAIM_BASE+0x0C)
#define HWD_MAIM_SP2_AAR         (HWD_MAIM_BASE+0x10)
#define HWD_MAIM_SP2_MPR         (HWD_MAIM_BASE+0x14)
#define HWD_MAIM_SP3_AAR         (HWD_MAIM_BASE+0x18)
#define HWD_MAIM_SP3_MPR         (HWD_MAIM_BASE+0x1C)
/*  matrix interrupt mask */
#define HWD_MAIM_M_IMR           (HWD_MAIM_BASE+0x20)
/*  matrix interrupt status */
#define HWD_MAIM_M_ISR           (HWD_MAIM_BASE+0x24)
#define HWD_MAIM_M0_PROT         (HWD_MAIM_BASE+0x28) /* default:0x0000_0000  */
#define HWD_MAIM_M1_PROT         (HWD_MAIM_BASE+0x2C) /* default: 0x0000_001F  */
#define HWD_MAIM_M2_PROT         (HWD_MAIM_BASE+0x30) /* default: 0x0000_001F  */
#define HWD_MAIM_M0_ECTL         (HWD_MAIM_BASE+0x34)

#define HWD_MAIM_H2XI_CONFIG     (HWD_MAIM_BASE+0x3C)
#define HWD_MAIM_H2XD_CONFIG     (HWD_MAIM_BASE+0x40)    
#define HWD_MAIM_PF_CONFIG       (HWD_MAIM_BASE+0x44) /* prefetch buffer config          */
#define HWD_MAIM_BRIDGE_CONFIG   (HWD_MAIM_BASE+0x48) /* AXI 3*2 INTC bridge config */
#define HWD_MAIM_HP_CTRL         (HWD_MAIM_BASE+0x4C) /* high priority control config   */
#define HWD_MAIM_H2XI_STATUS     (HWD_MAIM_BASE+0x50)
#define HWD_MAIM_H2XD_STATUS     (HWD_MAIM_BASE+0x54)
#define HWD_MAIM_BRIDGE_STATUS   (HWD_MAIM_BASE+0x58)
#define HWD_MAIM_PF_CMD_CNT      (HWD_MAIM_BASE+0x60) /* prefetch buffer received command cnt           */
#define HWD_MAIM_PF_HIT_CNT      (HWD_MAIM_BASE+0x64) /* prefetch buffer hit command cnt                      */
#define HWD_MAIM_PF_TOTAL_CNT    (HWD_MAIM_BASE+0x68) /* prefetch buffer issued command cnt               */
#define HWD_MAIM_PF_CON_HIT_CNT  (HWD_MAIM_BASE+0x6C) /* prefetch buffer continuous hit command cnt  */

#define HWD_MAIM_H2X_ARB_INC     (HWD_MAIM_BASE+0x80) /* H2x bridge arb priority increases     */
#define HWD_MAIM_H2X_ARB_DEC     (HWD_MAIM_BASE+0x84) /* H2x bridge arb priority decreases    */
#define HWD_MAIM_H2X_ARB_STS     (HWD_MAIM_BASE+0x88) /* H2x bridge arb priority status           */


#define HWD_MAIM_H2XI_MERGE_EN                       (1<<0)
#define HWD_MAIM_H2XI_DIS_CG                         (1 << 8)
#define HWD_MAIM_H2XI_DIS_PROTECT                    (1 << 9)

#define HWD_MAIM_H2XD_MERGE_EN                       (1<<0)
#define HWD_MAIM_H2XD_BUFFER_EN                      (1<<1)
#define HWD_MAIM_H2XD_READ_AHEAD_BW                  (1<<2)
#define HWD_MAIM_H2XD_FIFO_THRESHOLD_MASK            (~(0x7<<3))
#define HWD_MAIM_H2XD_FIFO_THRESHOLD(Level)          ((Level)<<3)
#define HWD_MAIM_H2XD_DIS_CG                         (1<<8)

#define HWD_MAIM_H2XI_PF_ENABLE                      (1)
#define HWD_MAIM_H2XI_PF_COUNT_EN                    (1<<1)
#define HWD_MAIM_H2XI_PF_MAS_ARCACHE(Arcache_Info)   ((Arcache_Info)<<2)
#define HWD_MAIM_H2XI_PF_DIS_CG                      (1<<8)

#define HWD_MAIM_AXI3X2_CG_DISABLE                   (1<<0)
#define HWD_MAIM_AXI3X2_MEMDVFS_DISABLE              (1<<8)
#define HWD_MAIM_AXI3X2_MEMDVFS_SWMODE               (1<<9)
#define HWD_MAIM_AXI3X2_MEMDVFS_SWVALUE              (1<<10)
#define HWD_MAIM_AXI3X2_MEMDVFS_MASK                 (1<<11)
#define HWD_MAIM_AXI3X2_IGNORE_AHBS_PROTECT          (1<<16)
#define HWD_MAIM_AXI3X2_SLICE_DIS_CG                 (1<<17)

#define HWD_MAIM_AXI3X2_HP_OFFSET                    (1<<1)
#define HWD_MAIM_AXI3X2_HP_CTRL_BYPASS               (1<<1)
#define HWD_MAIM_AXI3X2_CPD_AWHP                     (1<<1)
#define HWD_MAIM_AXI3X2_DMA_AWHP                     (1<<2)
#define HWD_MAIM_AXI3X2_CPD_ARHP                     (1<<3)
#define HWD_MAIM_AXI3X2_DMA_ARHP                     (1<<4)
#define HWD_MAIM_AXI3X2_I_ARHP                       (1<<5)
#define HWD_MAIM_AXI3X2_I_ALWAYS_ARHP                (1<<6)
#define HWD_MAIM_AXI3X2_AR_HP                        (1<<16)
#define HWD_MAIM_AXI3X2_AW_HP                        (1<<17)
#define HWD_MAIM_AXI3X2_EMI_ARB_HP                   (1<<18)


#define HWD_MAIM_H2XI_BUSY_ST                        (1)

#define HWD_MAIM_H2XD_BUSY_ST                        (1)

#define HWD_MAIM_AXI3X2_MI0_R_BUSY                   (1)
#define HWD_MAIM_AXI3X2_MI0_W_BUSY                   (1<<1)
#define HWD_MAIM_AXI3X2_SI0_RD_OT_BUSY               (1<<18)
#define HWD_MAIM_AXI3X2_SI0_CTRL_UPDATED             (1<<20)
#define HWD_MAIM_AXI3X2_C2K2EMI_BUSY                 (1<<21)
#define HWD_MAIM_AXI3X2_AP2C2K_ASYNC_IDLE            (1<<22)
#define HWD_MAIM_AXI3X2_AP_PROTECT_C2K_READY         (1<<23)
#define HWD_MAIM_AXI3X2_HSEL2HREQ_IDLE               (1<<24)
#define HWD_MAIM_AXI3X2_C2K_DDR_OFF                  (1<<25)
#define HWD_MAIM_AXI3X2_C2K2EMI_STOP_GNT             (1<<26)
#define HWD_MAIM_AXI3X2_C2K2EMI_STOP_REQ             (1<<27)
#define HWD_MAIM_AXI3X2_SLICE_IDLE                   (1<<28)
#define HWD_MAIM_AXI3X2_PF_BUSY                      (1<<29)

#define HWD_MAIM_H2X_ARB_CTRL_OFFSET                 (0)
#define HWD_MAIM_H2X_ARB_AWHP_INC                    (1)
#define HWD_MAIM_H2X_ARB_ARHP_INC                    (1<<1)
#define HWD_MAIM_H2X_ARB_ARBHP_INC                   (1<<2)

#define HWD_MAIM_H2X_ARB_AWHP_DEC                    (1)
#define HWD_MAIM_H2X_ARB_ARHP_DEC                    (1<<1)
#define HWD_MAIM_H2X_ARB_ARBHP_DEC                   (1<<2)

#define EFUSE_base                                   (0xA0206000)
/* EFUSE for different foundary manufactor process  */
#define PLL_EFUSE_M_HW_RESC_5                        (EFUSE_base+0x01B0)


/********************************************************
* Debug Mux Register bit Definition 
*******************************************************/
#if (SYS_ASIC >= SA_MT6735) /* Denali and Jade and later project */
#if (SYS_ASIC == SA_MT6797) /* special for Everest */
#define DEBUG_SEL_MASK                      (0x3F)
#define DEBUG_SEL_B_C2K_DBG_OUT             (0x16<<22)
#define DEBUG_SEL_A_C2K_DBG_OUT             (0x16<<16)
#else
#define DEBUG_SEL_MASK                       (0x1F)
#define DEBUG_SEL_B_C2K_DBG_OUT              (0x15<<8)
#define DEBUG_SEL_A_C2K_DBG_OUT              (0x15<<0)
#endif
#endif

#if (SYS_ASIC == SA_MT6735)
#define DEBUG_SEL1_GPIO_OUT                  (0x9A)
#define DEBUG_SEL1_CG_MUX_CLK                (0x07) /* bit0 */
#elif (SYS_ASIC == SA_MT6755) || (SYS_ASIC == SA_MT6750)||(SYS_ASIC == SA_MT6757)
#define DEBUG_SEL1_GPIO_OUT                  (0xB2)
#define DEBUG_SEL1_CG_MUX_CLK                (0x5B) /* bit7 */
#elif (SYS_ASIC == SA_MT6797)
#define DEBUG_SEL1_GPIO_OUT                  (0xA8)
#define DEBUG_SEL1_CG_MUX_CLK                (0x51) /* bit7 */
#elif (SYS_ASIC == SA_ELBRUS)
#define DEBUG_SEL1_GPIO_OUT                  (0xA4)
#define DEBUG_SEL1_CG_MUX_CLK                (0x4E) /* bit7 */
#else
#error "Unknown CHIP ID!!!"
#endif

#if (SYS_ASIC >= SA_MT6755)
#include "hwddefs_jade.h"
#endif

#if (SYS_ASIC >= SA_MT6755)
#if (SYS_ASIC == SA_ELBRUS)
#define HWD_MIS_GPS_SYNC       (0xA00A0008)
#else /* MT6755,MT6750,MT6797*/
#define HWD_MIS_GPS_SYNC       (0xA0011008)
#endif
#define HWD_MIS_GPS_SYNC_MASK  (0x0003)
#define HWD_MIS_GPS_SYNC_C2K   (0x0002)
#else /* MT6735*/
#define HWD_MIS_GPS_SYNC       (0xA02116E0)
#define HWD_MIS_GPS_SYNC_MASK  (0x0006)
#define HWD_MIS_GPS_SYNC_C2K   (0x0004)
#endif

#endif


/*****************************************************************************
* End of File
*****************************************************************************/


