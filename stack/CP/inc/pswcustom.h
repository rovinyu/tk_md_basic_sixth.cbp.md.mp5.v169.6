/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 1997-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _PSWCUSTOM_H_
#define _PSWCUSTOM_H_

/****************************************************************************
 *
 * Module:    pswcustom.h
 *
 * Purpose:   Customer specified compile switches
 *
* HISTORY     :
*     See Log at end of file
*
***************************************************************************/

/*****************************************************************************
 * ------- Code-Build Options ... comment/uncomment as required -------      *
 ****************************************************************************/


#define IS2000_REV_0 1 /* Activate all IS2000 Rev.0 SW */
#define IS2000_REV_A 1 /* Activate all IS2000 Rev.0 SW */
#define DSCH_NEW     1

/* Release A and beyond functionality remains supported in the CBP6
 * baseline SW as indicated by IS2000_REV_A and DSCH_NEW both being defined.
 * This decision stems from a mixing Rel0 and Rel > A logic that has not been
 * clearly separated using the above #defines.  However, CBP6 solution has
 * not been verified on RelA networks and thus cannot be truly RelA compliant.
 * Some of the RelA SW must be cleaned up in order for the SW to be optimized
 * for Rel0 solutions.  The REMOVE_IS2000_REL_A #define was created for this
 * purpose.  In the case where the CBP6 solution is targeted for a RelA system,
 * REMOVE_IS2000_REL_A must be deleted or set to 0.
 */
#define REMOVE_IS2000_REL_A 1

#ifdef IS95A
#define T53_OR_IS95A 1
#else /* !IS95A */
#define T53_PLUS_OR_95B 1
#endif

/*****************************************************************************
 * ------- Tune parameters - adjust as required                              *
 ****************************************************************************/

#ifdef MTK_PLT_ON_PC_UT
/* Set default CCI to enabled in UT. */
#define CP_QPCH_CCI_ENABLED_DEFAULT   TRUE
#else
#define CP_QPCH_CCI_ENABLED_DEFAULT   FALSE
#endif

/* Sim QPCH feature: when enabled, QPCH-like wake may occur before PCH wake
   in networks where QPCH is disabled. QPCH PI is ignore, the QPCH wake is
   only used for best PN selection before PCH wake. This is the default init
   value. It can be changed at run-time and it is stored in PSW Misc DBM */
#if defined(KDDI_EXTENSIONS)
#define CP_SIM_QPCH_ENABLED_DEFAULT   TRUE
#else
#define CP_SIM_QPCH_ENABLED_DEFAULT   FALSE
#endif
/* dynamic QPCH enable/disable based on Pilot Ec/Io thresholds */
#if defined(KDDI_EXTENSIONS)
#define CP_QPCH_DYNAMIC_CTRL_ENABLED_DEFAULT   TRUE
#else
#define CP_QPCH_DYNAMIC_CTRL_ENABLED_DEFAULT   FALSE
#endif
#define CP_QPCH_DYNAMIC_CTRL_ECIO_THRESH_LOW    2609 /* -14 dB */
#define CP_QPCH_DYNAMIC_CTRL_ECIO_THRESH_HIGH   5205 /* -11 dB */

/* DFS EcIo Threshhold. DFS not started until active EcIo is weaker than threshhold */
#if defined(KDDI_EXTENSIONS)
#define CP_DEFAULT_DFS_ECIO_THRESH         24  /* -12.0 dB, Units -0.5dB  */
#else /* always do DFS */
#define CP_DEFAULT_DFS_ECIO_THRESH          0
#endif

#define CP_IC_MODE_DEFAULT  IC_NORMAL

/* Min # of frames chipset must rx the action in advance of the action time */
#if defined (MTK_CBP)
#define CC_MIN_FRAMES_BEFORE_ACTION_TIME    1
#else
#define CC_MIN_FRAMES_BEFORE_ACTION_TIME    3
#endif
#define TC_MIN_FRAMES_BEFORE_ACTION_TIME    2

/* Better service rescan after redirection in 5 minutes */
#define REDIRECTION_END_TIME    300000L   /* 300 seconds */

/* default 1x Adv Enabled */
#define CP_1XADV_ENABLED_DEFAULT      TRUE
/* default SO73 Control  */
#define CP_SO73_ENABLED_DEFAULT       TRUE
#define CP_SO73_WBENABLED_DEFAULT     TRUE

/**********************************************************************/
/***************** END CUSTOM SYSTEM SELECTION ************************/
/**********************************************************************/

/* PCH Layer 2 */
/* Increased to 6.2 seconds  */
#define ENG_MAX_BAD_PAGE_FRAME  310 /* 310 * 20 msec = 6.2 sec, for the access followed by idle supervsion 3 sec + 3 sec */

/* this sets number of base stations for which
 *     overhead info is stored
 */
#define MAX_HISTORY_RECS        7

/* new Idle Handoff Parameters */
#define THR_SLOPE_RES       2  /* Q-precision resolution of specified line slopes.
                                  (2^-THR_SLOPE_RES)*(SPEC integer slope in dB(thresh)
                                  /dB(Active pwr)). */
#define THR_SLOPE_MID       2  /* (2^THR_SLOPE_RES)/2 used for slope rounding. */

#define MIN_ACT_IMMED      34  /* -17.0 dB active power below the "instant" test
                                  threshold is THR_MIN_IMMED. */

#define THR_SLOPE_IMMED     1  /* Slope of instant threshold above active power
                                  MIN_ACT_IMMED in units of dB(thresh)/dB(Active pwr)/
                                  2^THR_SLOPE_RES. */
#define THR_MIN_IMMED       4  /* Minimum value of the "instant" test threshold.
                                  Units -0.5dB. */
#define MIN_ACT_DELAYED    34  /* Minimum value of the "persistance" test threshold.
                                  Units -0.5dB. */
#define THR_SLOPE_DELAYED   1  /* Active power below which the "persistance" test
                                  threshold is THR_MIN_DELAYED. */
#define THR_MIN_DELAYED     1  /* Slope of the instant threshold above active power of
                                  MIN_ACT_DELAYED, in units of dB(thresh)/dB(active pwr)
                                  /2^THR_SLOPE_RES. */
#define HO_DELAYED_IHO_COUNT 2 /* Persistence test for delayed HO
                                  ( default: 2 consecutive search results) */
#define NOISE_THRESHOLD    50  /* Threshold below which no neighbor pilot will be
                                  considered a valid target for handoff, units -0.5dB */
#define MIN_NGHBR_THR      36  /* Idle Handoff Candidates must be atleast -18.0 dB . */

#if defined(MTK_CBP)
/* Access Entry HO Parameters */
#define CP_ACCESS_ENTRY_HO_WEAK_PILOT_STRENGTH  24  /* -12 dB */
#else
/* Access Entry HO Parameters */
#define CP_ACCESS_ENTRY_HO_WEAK_PILOT_STRENGTH  14  /* -7 dB */
#endif

/* Max # of Access Slots to delay due to persistence before transmit msg */
#define ENG_MAX_PSIST_ACCESS_SLOTS          6

/* Broadcast Address parameters */
#define CP_BCAST_ADDRESS_MAX_SIZE   15


/* Default Service Option */

#define CP_CUSTOM_DEFAULT_SERVICE_OPTION   3   /* Rate set 1, EVRC  */
#define CP_CUSTOM_DEFAULT_HOME_MO_SO       3
#define CP_CUSTOM_DEFAULT_ROAM_MO_SO       3
#define CP_CUSTOM_DEFAULT_HOME_MT_SO       3

/* Service Configuration - Maximum Number of Service Option Connections */
#define CP_MAX_SERV_OPT_CONN_REC           2

/* Maximum Service Option Control Message Record Length */
#define MAX_SERV_OPT_CNTL_RECORD_LEN       128

/* Maximum Mobile Station Supported Supplemental Code Channels */
#define CP_MAX_SUP_CODE_CHANNELS  7

/* Maximum Sizes for Messaging Elements */
#define CP_MAX_CALLED_PARTY_NUMBER_SIZE    32  /* Called Party # record   */
#define CP_MAX_CALLING_PARTY_NUMBER_SIZE   32  /* Calling Party # record  */
#define CP_MAX_REDIRECTING_NUMBER_SIZE     32  /* Redirecting # record    */
#define CP_MAX_EXTENDED_DISPLAY_SIZE       82  /* Extended Display record */
#define CP_MAX_DISPLAY_SIZE                82  /* Display record          */
#define CP_MAX_KEYPAD_FACILITY_SIZE        32  /* Keypad Facility record  */
#define CP_MAX_SUBADDRESS_SIZE             32  /* Subaddress record       */
#define CP_MAX_BURST_DTMF_DIGITS           255 /* Burst DTMF msg          */
#define CP_MAX_DATA_BURST_CHAR             249 /* Data Burst msg          */
                                                 /* 255 - Data Burst header */
#define CP_MAX_CADENCE_GROUPS_REC          16
#define CP_MAX_EXTENDED_DISPLAY_REC        3

/* Traffic Channel Message Transmitter (tc_mtx.c) */

/* TC Transmitter Queue Sizes */
#define ACKQ_SIZE        8           /* TC Ack Queue Size          */
#define MSGQ_SIZE        8           /* TC Msg Queue Size          */

/* Traffic Channel Transmit Window Size */
#define TC_TX_WINDOW_SIZE                  4
#if ((TC_TX_WINDOW_SIZE == 0) || (TC_TX_WINDOW_SIZE > 4))
#error "Bad TC Transmit Window Size!"
#endif

/* Minimum backoff time in frames from T2m to transmit an ACK */
/*                                                                 **
**       Have to take into account potential queuing delay of      **
**       1 frame, message passing, and one frame for insurance.    **
**       Note: An Acknowledgement Message should fit in one frame: **
**       MSG_LENGTH + MSG_TYPE + ACK_FIELDS + ORDER +              **
**       ADD_REC_LEN + RESERVED + CRC = 56 bits = 7 bytes          **
**                                                                 **
**       Mux 1 9600  bps 1/2 rate  - 88  signalling bits           **
**       Mux 2 14400 bps 1/2 rate  - 138 signalling bits           **
**                                                                 */
#define TC_TX_PIGGYBACK_BACKOFF    5
#if ((TC_TX_PIGGYBACK_BACKOFF < 3) || (TC_TX_PIGGYBACK_BACKOFF > 10))
#error "Bad TC Transmit Piggyback Backoff!"
#endif

/* Minimum Signaling Bits Per Frame */
/* MUX OPTION 1 (RATE SET 1) 9600 bps, 1/2 rate, Dim & Burst */
#define TC_TX_MIN_MUX1_SIG_BITS_PER_FRAME  88
/* MUX OPTION 2 (RATE SET 2)   14400 bps, 1/2 rate, Dim & Burst */
#define TC_TX_MIN_MUX2_SIG_BITS_PER_FRAME  138

/* Minimum Closed Loop Power Control Step Size */
#define CP_MIN_PWR_CNTL_STEP               2

/* Traffic Channel Pilot Set Maintenance */
#define CP_T_DROP_COUNT   1

/* Amount of time to inhibit Registration after a failure */
#define CP_REG_DELAY_AFTER_FAIL 5000 /* msec */

#define ACCESS_FAIL_COUNT 5
#define DELAYED_ORIG_WAITING_TIME 70000/* time wait before abort orig attempt */
                                       /* 70sec (similar to silent retry)     */
/* SNR SR - Silent Retry defines. */
#define MAX_SILENT_RETRIES     15          /* Max no of sub-attempt permitted */
#define SILENT_RETRY_TIMEOUT   20L         /* 20ms -> 1 frame delay before Reorig */
#define SILENT_RETRY_RESET_TIMEOUT 30000L  /* 45 Sec Failsafe Timeout to reset SR
                                            * upon expiration. 
                                            */
/* In LG VZW lab test. If NW always send reorder order for E911 call, MS should
 * retry until reach the max. */
#define MAX_SILENT_RETRIES_ECC_REORDER 1000000 /* Max no of sub-attempt permitted */

#ifndef OTA_MAX_MDN_DIGITS
#define OTA_MAX_MDN_DIGITS 15
#endif

/* MAX_BCASTS is the number of broadcast messages that can be
 *   checked for duplicates.
 * If more than MAX_BCASTS distinct messages are received within
 *   four broadcast cycles, messages will not be lost but duplicates
 *   may be reported.
 */
#define MAX_BCASTS      10

/* MAX_SINGLE_AWI_SMS_WORDS is the maximum size of a
 * SMS message that can be received in a single
 * ALERT_WITH_INFO_SMS message. It is in units of FVC words
 * (each of which has 3 bytes of user data)
 * NOTE: This must not be greater than 127 !
 */
#define MAX_SINGLE_AWI_SMS_WORDS 64

/* MAX_ALERT_WITH_INFO_SMS_SIZE is the maximum size of an
 * SMS message that can be received via a series of
 * ALERT_WITH_INFO_SMS messages.
 */
#define MAX_ALERT_WITH_INFO_SMS_SIZE 256

/* R.Sattari, 9/6/00, added defines for capability fields of the capability */
/* information record.  Set to 1 for enabled, and to 0 for disabled */
/* CR 6254
   These macros are no longer needed. They are replaced by enums in
   Ms_cap_db.h

#define CP_ACCESS_ENTRY_HO_ENABLED  1
#define CP_ACCESS_PROBE_HO_ENABLED  1
#define CP_ANALOG_SEARCH_ENABLED    0
#define CP_HOPPING_BEACON_ENABLED   0
#define CP_MAHHO_ENABLED            0
#define CP_PUF_ENABLED              0
#define CP_ANALOG_553A_ENABLED      0
*/

/* PDCH params */
#define CP_MAX_PDCCH_CH             8

/* Default Customer Preferred Service Option */
#define PSW_DEFAULT_LSD_SO_PREF    SERVICE_OPTION_15
#define PSW_DEFAULT_MSD_SO_PREF    SERVICE_OPTION_33
#define PSW_DEFAULT_ASYNC_SO       SERVICE_OPTION_12
#define PSW_DEFAULT_FAX_SO         SERVICE_OPTION_13
#define PSW_DEFAULT_IS126_SO       SERVICE_OPTION_2
#define PSW_DEFAULT_MARKOV_SO      SERVICE_OPTION_32798
#define PSW_DEFAULT_SUPL_LPBK_SO   SERVICE_OPTION_30
#if defined(MTK_CBP) || defined(SMARTFREN_EXTENSIONS)
#define PSW_DEFAULT_SMS_SO         SERVICE_OPTION_6
#else
#define PSW_DEFAULT_SMS_SO         SERVICE_OPTION_14
#endif
#define PSW_DEFAULT_OTASP_SO       SERVICE_OPTION_19
#define PSW_DEFAULT_CSC_SO         SERVICE_OPTION_35
#define PSW_DEFAULT_VOICE_SO       CP_CUSTOM_DEFAULT_SERVICE_OPTION

/*- - - - - - - */
/* NAM Defaults */
/*- - - - - - - */
#define PSW_DEFAULT_ESN              0xFF04FFFF

#define PSW_DEFAULT_PRL_ENABLE       TRUE
/* Station Class Mark */
#define PSW_DEFAULT_SCM_BC0          0x28
#define PSW_DEFAULT_SCM_BC1          0xA8
#define PSW_DEFAULT_SCM_BC2          0x28
#define PSW_DEFAULT_SCM_BC3          0x28
#define PSW_DEFAULT_SCM_BC4          0xA8
#define PSW_DEFAULT_SCM_BC5          0x28
#define PSW_DEFAULT_SCM_BC6          0x28
#define PSW_DEFAULT_SCM_BC7          0x28
#define PSW_DEFAULT_SCM_BC8          0x28
#define PSW_DEFAULT_SCM_BC9          0x28
#define PSW_DEFAULT_SCM_BC10         0x28
#define PSW_DEFAULT_SCM_BC11         0x28
#define PSW_DEFAULT_SCM_BC12         0x28
#define PSW_DEFAULT_SCM_BC13         0x28
#define PSW_DEFAULT_SCM_BC14         0x28
#define PSW_DEFAULT_SCM_BC15         0x28
#define PSW_DEFAULT_SCM_BC16         0x28
#define PSW_DEFAULT_SCM_BC17         0x28
#define PSW_DEFAULT_SCM_BC18         0x28
#define PSW_DEFAULT_SCM_BC19         0x28
#define PSW_DEFAULT_SCM_BC20         0x28

#ifdef KDDI_EXTENSIONS
#define PSW_DEFAULT_PREV             9
#else
#define PSW_DEFAULT_PREV             6
#endif

#define PSW_DEFAULT_SLOT_CYCLE_IDX   2
#define PSW_DEFAULT_MOB_FIRM_REV     0
#define PSW_DEFAULT_MOB_MODEL        0

#define PSW_DEFAULT_SSPR_PREV            PRL_EXT /* CBP7 should be PRL_EXT...*/

#define PSW_DEFAULT_AKEY_0               0
#define PSW_DEFAULT_AKEY_1               1

#define PSW_DEFAULT_VALID_IMSI_MAP       (IMSI_M_VALID | IMSI_T_VALID)

#define PSW_DEFAULT_IMSI_M_MCC           359   /*Encoded form of 460*/
#define PSW_DEFAULT_IMSI_11_12           92    /*Encoded form of 03*/

#define PSW_DEFAULT_NUM_MDN_DIGITS       10

#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_LEN 0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_0   0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_1   0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_2   0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_3   0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_4   0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_5   0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_6   0
#define PSW_DEFAULT_ASSIGN_TMSI_ZONE_7   0
#define PSW_DEFAULT_TMSI_CODE            0xFFFFFFFFL
#define PSW_DEFAULT_TMSI_EXP_TIME        0

/* Up to 20 SIDs; if SID unused, set to 0 */
#define PSW_DEFAULT_MAX_SID_NID          MAX_POSITIVE_SIDS
#if defined(KDDI_EXTENSIONS)
#define PSW_DEFAULT_STORED_POS_SID_NID   0
#else
#define PSW_DEFAULT_STORED_POS_SID_NID   1
#endif
#if defined(VERIZON_EXTENSIONS)
#define PSW_DEFAULT_SID_0                2004
#define PSW_DEFAULT_SYSTEM_SELECT        AUTOMATIC_B
#else
#define PSW_DEFAULT_SID_0                7
#define PSW_DEFAULT_SYSTEM_SELECT        AUTOMATIC
#endif
#define PSW_DEFAULT_SID_1                0
#define PSW_DEFAULT_SID_2                0
#define PSW_DEFAULT_SID_3                0
#define PSW_DEFAULT_SID_4                0
#define PSW_DEFAULT_SID_5                0
#define PSW_DEFAULT_SID_6                0
#define PSW_DEFAULT_SID_7                0
#define PSW_DEFAULT_SID_8                0
#define PSW_DEFAULT_SID_9                0
#define PSW_DEFAULT_SID_10               0
#define PSW_DEFAULT_SID_11               0
#define PSW_DEFAULT_SID_12               0
#define PSW_DEFAULT_SID_13               0
#define PSW_DEFAULT_SID_14               0
#define PSW_DEFAULT_SID_15               0
#define PSW_DEFAULT_SID_16               0
#define PSW_DEFAULT_SID_17               0
#define PSW_DEFAULT_SID_18               0
#define PSW_DEFAULT_SID_19               0

#define PSW_DEFAULT_MOB_TERM_HOME                MOB_TERM_HOME_ENABLED
#define PSW_DEFAULT_MOB_TERM_FOREIGN_SID         MOB_TERM_SID_ENABLED
#define PSW_DEFAULT_MOB_TERM_FOREIGN_NID         MOB_TERM_NID_ENABLED

#define PSW_DEFAULT_MOB_TERM_MAP         (PSW_DEFAULT_MOB_TERM_HOME | PSW_DEFAULT_MOB_TERM_FOREIGN_SID | PSW_DEFAULT_MOB_TERM_FOREIGN_NID)



#define PSW_DEFAULT_SPC                  0x00000000
#define PSW_DEFAULT_OTA_CAPABILITY_MAP   (OTA_ALLOW_OTAPA | OTA_ENABLE_OTA | OTA_ENABLE_SPASM | OTA_ENABLE_SPC_CHANGE)

#define PSW_DEFAULT_HOME_SID             231
#define PSW_DEFAULT_EX                   0
#define PSW_DEFAULT_FIRSTCHP             333
#define PSW_DEFAULT_DTX                  FALSE
#define PSW_DEFAULT_FCCA                 333
#define PSW_DEFAULT_LCCA                 313
#define PSW_DEFAULT_FCCB                 334
#define PSW_DEFAULT_LCCB                 354

#define PSW_DEFAULT_NXTREG               0
#define PSW_DEFAULT_SID                  0
#define PSW_DEFAULT_LOCAID               0
#define PSW_DEFAULT_PUREG                TRUE

#define PSW_DEFAULT_CPCA                 283
#define PSW_DEFAULT_CSCA                 691
#define PSW_DEFAULT_CPCB                 384
#define PSW_DEFAULT_CSCB                 777
#define PSW_DEFAULT_AUTH_CAPABILITY_MAP  0
#define PSW_DEFAULT_UIM_ID               0


#define PSW_DEFAULT_ROAM_SETTING         0x0F
#define PSW_DEFAULT_ROAM_SETTING_SPRINT  0x0F /* change from 0x0D to 0x0F on request from LG/sprint */

/*- - - - - - End of NAM Defaults - - - - - - - - - - - - - - - - - */

/*- - - - - - - - - - */
/* MS CAP DB Defaults */
/*- - - - - - - - - - */
#define PSW_DEFAULT_SR1_SUPPORTED           TRUE
#define PSW_DEFAULT_SR3_SUPPORTED           FALSE

#define PSW_DEFAULT_RC_CLASS_1_SUPPORTED    TRUE
#define PSW_DEFAULT_RC_CLASS_2_SUPPORTED    TRUE
#define PSW_DEFAULT_RC_CLASS_3_SUPPORTED    TRUE

#define PSW_DEFAULT_UI_ENCRYPT_SUPPORTED    UI_Encrypt_Disabled

#define PSW_DEFAULT_ENHANCED_RC             TRUE

/* no mobile assist */
#define PSW_DEFAULT_REV_FCH_GATING_REQ      TRUE
#define PSW_DEFAULT_STS_SUPPORTED           FALSE
#define PSW_DEFAULT_THREE_XC_CH_SUPPORTED   FALSE
#define PSW_DEFAULT_WLL_SUPPORTED           FALSE

/* no bitmap included */
#define PSW_DEFAULT_SO_BITMAP_IND           SO_0_BITS
#define PSW_DEFAULT_TIERED_SERVICES         FALSE

#define PSW_DEFAULT_QPCH_SUPPORTED          TRUE
#define PSW_DEFAULT_SLOTTED_TIMER_SUPPORTED TRUE
#define PSW_DEFAULT_CHS_SUPPORTED           FALSE
#define PSW_DEFAULT_GATING_RATE_SET         GATING_RATE_SET_00
#define PSW_DEFAULT_EXT_CAP_INCLUDED        FALSE
#define PSW_DEFAULT_MABO_SUPPORTED          FALSE
#define PSW_DEFAULT_SDB_SUPPORTED           FALSE
#define PSW_DEFAULT_RLP_CAP_BLOP_LEN        0
#define PSW_DEFAULT_RLP_CAP_BLOP_MAX_NAK_ROUNDS_FWD   5
#define PSW_DEFAULT_RLP_CAP_BLOP_MAX_NAK_ROUNDS_REV   5
#define PSW_DEFAULT_OTD_SUPPORTED           FALSE
#define PSW_DEFAULT_FCH_SUPPORTED           TRUE
#define PSW_DEFAULT_FCH_FRAME_SIZE          FCH_FS_20MS

/* (1)1x advance feature is required by operator
 * (2)FOR_RC_11/REV_RC_8 is only used for 1x advance
 */
#define PSW_DEFAULT_FWD_FCH_LEN             4
#define PSW_DEFAULT_FWD_FCH_RC_MAP          (FOR_RC_1 | FOR_RC_2 | FOR_RC_3 | FOR_RC_4 | FOR_RC_5 | FOR_RC_11)
#define PSW_DEFAULT_REV_FCH_LEN             3
#define PSW_DEFAULT_REV_FCH_RC_MAP          (REV_RC_1 | REV_RC_2 | REV_RC_3 | REV_RC_4 |REV_RC_8)

#define PSW_DEFAULT_DCCH_SUPPORTED          FALSE
#define PSW_DEFAULT_DCCH_FRAME_SIZE         DCCH_FS_20MS
#define PSW_DEFAULT_FWD_DCCH_LEN            3
#define PSW_DEFAULT_FWD_DCCH_RC_MAP         0
#define PSW_DEFAULT_REV_DCCH_LEN            2
#define PSW_DEFAULT_REV_DCCH_RC_MAP         REV_RC_INVALID

#define PSW_DEFAULT_FWD_SCH_SUPPORTED       TRUE
#define PSW_DEFAULT_REV_SCH_SUPPORTED       TRUE

/* 1x advance feature is not required by SCH */
#define PSW_DEFAULT_FWD_SCH_LEN               3
#define PSW_DEFAULT_FWD_SCH_RC_MAP            (FOR_RC_3 | FOR_RC_4 | FOR_RC_5)
#define PSW_DEFAULT_FWD_SCH_NUM               1

#define PSW_DEFAULT_FWD_TURBO_SUPPORTED_SCH1  TRUE
#define PSW_DEFAULT_FWD_TURBO_SUPPORTED_SCH2  FALSE
#define PSW_DEFAULT_FWD_MAX_TURBO_BLOCK_SIZE  4
#define PSW_DEFAULT_FWD_CONV_SUPPORTED_SCH1   TRUE
#define PSW_DEFAULT_FWD_CONV_SUPPORTED_SCH2   FALSE
#define PSW_DEFAULT_FWD_MAX_CONV_BLOCK_SIZE   4

/* 1x advance feature is not required by SCH */
#define PSW_DEFAULT_REV_SCH_LEN               2
#define PSW_DEFAULT_REV_SCH_RC_MAP            (REV_RC_3 | REV_RC_4 )
#define PSW_DEFAULT_REV_SCH_NUM               1

#define PSW_DEFAULT_REV_TURBO_SUPPORTED_SCH1  TRUE
#define PSW_DEFAULT_REV_TURBO_SUPPORTED_SCH2  FALSE
#define PSW_DEFAULT_REV_MAX_TURBO_BLOCK_SIZE  4
#define PSW_DEFAULT_REV_CONV_SUPPORTED_SCH1   TRUE
#define PSW_DEFAULT_REV_CONV_SUPPORTED_SCH2   FALSE
#define PSW_DEFAULT_REV_MAX_CONV_BLOCK_SIZE   4

#define PSW_DEFAULT_NON_OCTET_ALIGNED_DATA    FALSE
#define PSW_DEFAULT_OCTET_ALIGNED_DATA        FALSE
#define PSW_DEFAULT_REV_SCH_DURATION_UNIT     4       /* 100 ms */
#define PSW_DEFAULT_REV_SCH_DURATION          0x01FF /* infinite */

/* Information record for Extended Multiplex Option */
#define PSW_DEFAULT_FWD_FCH_MO_SUPPORT        (MUX_OPTION_0x1 | MUX_OPTION_0x2)
#define PSW_DEFAULT_REV_FCH_MO_SUPPORT        (MUX_OPTION_0x1 | MUX_OPTION_0x2)
#define PSW_DEFAULT_FWD_DCCH_MO_SUPPORT       0
#define PSW_DEFAULT_REV_DCCH_MO_SUPPORT       0
#define PSW_DEFAULT_FWD_SCH_HIGHEST_RS1_MO    FOR_SCH_0x921_MUX_OPTION
#define PSW_DEFAULT_FWD_SCH_HIGHEST_RS2_MO    FOR_SCH_0x912_MUX_OPTION
#define PSW_DEFAULT_REV_SCH_HIGHEST_RS1_MO    FOR_SCH_0x921_MUX_OPTION
#define PSW_DEFAULT_REV_SCH_HIGHEST_RS2_MO    FOR_SCH_0x912_MUX_OPTION

/* Information record for Geo-location codes */
#define PSW_DEFAULT_GEO_LOC_INCL              TRUE

#if defined(SYS_OPTION_GPS_VASCO)
 #define PSW_DEFAULT_GEO_LOC_TYPE              GL_AFLT_AND_GPS
 #define PSW_DEFAULT_LOC_GPS_ACQ_CAP           3
#elif ((defined SYS_OPTION_GPS_EXTERNAL) || (SYS_OPTION_GPS_HW ==SYS_GPS_LOCAL_INTERNAL))
 #define PSW_DEFAULT_GEO_LOC_TYPE              GL_AFLT_AND_GPS
 #define PSW_DEFAULT_LOC_GPS_ACQ_CAP           1
#else
 #define PSW_DEFAULT_GEO_LOC_TYPE              GL_AFLT
 #define PSW_DEFAULT_LOC_GPS_ACQ_CAP           0
#endif

/* Encryption Mode Supported */
#define PSW_DEFAULT_ENCRYPT_MODE_SUPPORTED    (Basic_Encrypt_Supported)

/* Mobile Station Operation Mode */
#define PSW_DEFAULT_OP_MODE                   ( OP_MODE0 | OP_MODE1 )

#define PSW_DEFAULT_LOC_PILOT_PH_CAP          4  /* Sixteenth Chip Meas Cap */
#define PSW_DEFAULT_LOC_CALC_CAP              0
#define PSW_DEFAULT_LOC_TCP_CAP               TRUE
#define PSW_DEFAULT_LOC_MS_ORIG_CAP           TRUE

/* DCCH Only Ping Timeout 15 second */
#define PSW_DEFAULT_DCCH_ONLY_PING_PERIOD     15000   /* MS resolution */
#define PSW_DEFAULT_DATA_READY_TO_SEND        TRUE    /* DRS */
#define PSW_DEFAULT_CHANNEL_INDICATOR         CH_IND_FCH
#define PSW_DEFAULT_WLL_INCL                  FALSE   /* Not wireless local loop */

/* PDCH params */
#ifdef KDDI_EXTENSIONS
#define PSW_DEFAULT_PDCH_CHM_SUPPORTED        FALSE
#define PSW_DEFAULT_FWD_PDCH_SUPPORTED        FALSE
#define PSW_DEFAULT_PDCH_ACK_DELAY            FALSE
#else
#define PSW_DEFAULT_PDCH_CHM_SUPPORTED        TRUE
#define PSW_DEFAULT_FWD_PDCH_SUPPORTED        TRUE
#define PSW_DEFAULT_PDCH_ACK_DELAY            TRUE
#endif
#define PSW_DEFAULT_EXTENDED_CHAN_INDICATOR   3
#define PSW_DEFAULT_PDCH_NUM_ARQ_CHAN         0
#define PSW_DEFAULT_FWD_PDCH_RC_MAP           0x7
#define PSW_DEFAULT_CH_CONFIG_SUP_MAP         ( FPDCH_EXTCHIND_1 | FPDCH_EXTCHIND_2 |  \
                                                FPDCH_EXTCHIND_3 | FPDCH_EXTCHIND_4 |  \
                                                FPDCH_EXTCHIND_5 | FPDCH_EXTCHIND_6 )

#define PSW_DEFAULT_SDB_DESIRED_ONLY          FALSE
#define PSW_DEFAULT_INFO_REC_CAPABILITY_MAP   (CP_ACCESS_ENTRY_HO_ENABLED | \
                                               CP_ACCESS_PROBE_HO_ENABLED | CP_MAHHO_ENABLED)
/*- - - - - - End of MS CAP DB Defaults - - - - - - - - - - - - - - */

#define PSW_OTAPA_NAM_LOCK_DEFAULT            OTAPA_NAM_LOCK_KEEP_BS_SETTING
#define PSW_OTASP_POWERUP_MODE_DEFAULT        OTASP_COMPLETE_PSW_AUTO_POWERCYCLE

/*****************************************************************************
* $Log: pswcustom.h $
* Revision 1.12  2006/03/16 11:54:42  mclee
* - Add #define for CP_SELECTION_PHASE_SCANTIME
* - Add customized OOSA #defines to support VERIZON_EXTENSIONS
* Revision 1.11  2006/03/03 13:31:45  vsethuraman
* CR7207 :- PN310 CSS and ERI changes.
* Revision 1.10  2006/01/03 10:53:34  wavis
* Merging in VAL.
* Revision 1.9.1.2  2005/11/22 11:51:44  wavis
* Add default customer preferred service options.
* Revision 1.9.1.1  2005/08/17 14:14:40  wavis
* Duplicate revision
* Revision 1.9  2005/08/17 14:14:40  hans
* CR6900 - Enhanced qpch indicator detection algorithm [DSPM/L1D/PSW]. CR6902 - Immediate PSW-L1D wake message for PCH/FCCCH
* Revision 1.8  2005/05/20 14:45:12  vsethuraman
* CR 6254 :- Macros not needed any more as they are replaced by enum's in Ms_cap_db.h.
* Revision 1.7  2005/05/06 18:12:03  hans
* Defined FCCCH_SLOT  to activate PE FCCCH slotted mode code.
* Revision 1.6  2005/03/18 10:04:19  dstecker
* Modifications for the 4.05 merge
* Revision 1.5  2004/09/28 15:54:31  chinh
* Merged changes from cbp6.0.
* Revision 1.4  2004/04/23 10:48:40  asharma
* CBP6.1 (Phase-1) DV Data and Voice Call Setup changes.
* Revision 1.3.1.3  2004/04/19 16:11:19  asharma
* Revision 1.3.1.2  2004/04/14 15:22:34  asharma
* Revision 1.3  2004/04/06 14:00:29  dstecker
* CBP6.1 release 1
* Revision 1.2  2004/03/25 11:46:03  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.3  2004/02/10 15:17:03  bcolford
* Merged CBP4.0 Release 8.05.0 changes.
* Revision 1.30  2003/11/06 11:06:56  ameya
* Added REDIRECTION_END_TIME of 5 minutes.
* Revision 1.29  2003/09/05 10:15:51  mclee
* Add Silent Retry Reset Timeout define
* Revision 1.28  2003/06/27 14:52:58  dstecker
* Removed LSI_PILOT_THRESH_VER definition
* Revision 1.27  2003/06/26 13:47:43  byang
* CR#2394: Added MIN_NGHBR_THR and set it to 36 (-18).
*
* Revision 1.26  2003/05/21 17:15:39  byang
* CR#570: Removed define statement for IS2000A.
* Revision 1.25  2002/12/27 11:23:13  hans
* Bugfix to Label:CP_PS_1.73.0_LAYER2_FixCR570
* Revision 1.24  2002/12/26 12:43:11  hans
* Conditional compile flag IS2000A replaced with IS_2000_REV_A and IS_2000_REV_0 for rev A and Rev 0 specific code.
* Revision 1.23  2002/11/11 15:50:12  byang
* Changed MAX_HISTORY_RECS from 3 to 7.
* Revision 1.22  2002/10/31 11:21:17  snreddy
* Changed OOSA default parms to fix CR 1506
* Revision 1.21  2002/07/31 18:53:01  hans
* BAND_CLASS_x_SUPPORTED defines added for band class 4 to 10 to disable code as option reduce code size
* Revision 1.20  2002/07/26 14:04:38  ameya
* Increased MRU records to 10 and moved maximum positive and negative sids/nids here from css_api.h.
* Revision 1.19  2002/07/15 11:52:15  chinh
* Removed compile switch CBP3_MERGE (CR598).
* Revision 1.18  2002/06/25 14:34:45  dstecker
* Increated value of T_DROP_MIN_THRESH_Q16 in order to pass CDG2 test on Racal
* Revision 1.17  2002/06/04 08:07:22  mshaver
* Added VIA Technologies copyright notice.
* Revision 1.16  2002/05/31 10:28:41  akayvan
* Replaced the Idle Handoff Thresholds with that of CBP3.
* Revision 1.15  2002/05/02 17:23:52  amala
* Corrected CSS re-scan timer timeout values.
* Revision 1.14  2002/01/10 17:06:37  AMALA
* Added emergency mode processing for new system selection.
* Revision 1.13  2002/01/02 10:05:26  AMALA
* Added new system selection changes.
* Revision 1.12  2001/11/20 12:08:04  mclee
* Increase ENG_MAX_BAD_FAGE_FRAME to 3 seconds to fix CR301
* Revision 1.11  2001/11/08 11:53:35  AMALA
* Temporarily changed define value of MAX_MRU_RECORDS
* & added define PRL_MAX_SIZE. Deleted compiler switch
* SYS_SEL_1_CBP3_MERGE define not used anymore.
*
* Revision 1.10  2001/10/29 08:46:24  mclee
* Changed #define CP_SUPPORT_HHO_WITH_RETURN_ON_FAILURE 0 to FALSE
* Revision 1.9  2001/09/21 15:37:24  aslee
* Fix warning
* Revision 1.8  2001/09/20 13:03:29  dstecker
* Corrected traffic handoff set maintaince parameters
* Revision 1.7  2001/09/17 14:09:47  AMALA
* CBP3 System Selection port
* Revision 1.6  2001/07/18 15:21:03  chinh
* cbp3 porting for Idle and Access
* Revision 1.5  2001/06/18 15:21:50  chinh
* Added new constants definitions for information record in FNM and AWIM
* Revision 1.4  2001/02/14 10:41:37  hans
* IS2000A_CCCH flag added to put  common control channels under conditional compilation. Intended for rev.0 code space saving.
* Revision 1.3  2001/01/30 02:05:41Z  dstecker
* new access probe architecture
* Revision 1.2  2000/12/19 11:43:43  plabarbe
*
* --- Added comments ---  plabarbe [2000/12/19 19:44:50Z]
* Changed file to conform a little closer to LSI Naming standards.
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif  /* _CUSTOM_H_ */
/**Log information: \main\CBP80\cbp80_cshen_scbp10283\cbp80_gdeng_scbp10309\1 2012-09-13 09:10:44 GMT gdeng
** scbp10309**/
/**Log information: \main\SMART\1 2013-04-23 06:23:00 GMT yxma
** HREF#22166 , add default custom service option to SMARTFREN
|
|**/
/**Log information: \main\SMART\2 2013-04-24 09:44:37 GMT yxma
** HREF#22179,modify default smartfren sms option
|**/
/**Log information: \main\SMART\3 2013-04-27 08:09:18 GMT yxma
** HREF#22188  , modify smartfren primary voice so**/
/**Log information: \main\Trophy\Trophy_gdeng_href22222\1 2013-06-08 01:22:06 GMT gdeng
** HREF#22222**/
/**Log information: \main\Trophy\2 2013-06-08 01:27:18 GMT cshen
** href#22222**/
/**Log information: \main\Trophy\Trophy_SO73\1 2013-07-09 05:09:38 GMT gdeng
** HREF#0000 **/
/**Log information: \main\Trophy\Trophy_SO73\2 2013-07-18 05:11:58 GMT gdeng
** HREF#0000 enable wb 73**/
/**Log information: \main\Trophy\4 2013-07-18 05:32:44 GMT cshen
** href#22222**/
