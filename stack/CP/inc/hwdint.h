/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDINT_H_
#define _HWDINT_H_
/*****************************************************************************
* 
* FILE NAME   : hwdint.h
*
* DESCRIPTION : Interrupt control for "CDMA" interrupt sources
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#if (SYS_ASIC <= SA_CBP82)
#define HWD_INT_REGISTER_MAX 10
#elif (SYS_ASIC >= SA_MT6735)
#define HWD_INT_REGISTER_MAX 9
#endif

#define HWD_INT_SOURCES_PER_REG  16
#define HWD_INT_SOURCES_MAX  (HWD_INT_SOURCES_PER_REG * HWD_INT_REGISTER_MAX)

/*---------------------**
** L0 Interrupts (IRQ) **
**---------------------*/
     
/* Enable L0 interrupts by applying mask to IMR0 register */
#define HwdIntEnableL0(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR0_L, (mask), 0)
   
/* Disable L0 interrupts by applying mask to IMR0 register */
#define HwdIntDisableL0(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR0_L, (mask), mask)
   
/* Control L0 interrupts by writing mask to ISR0 register */
#define HwdIntClrL0(mask) \
   HwdWrite(HWD_CP_ISR0_L, (mask))  

/* Determine if a L0 interrupt defined in mask is active */

#define HwdIsIntL0(mask) \
      ((HwdRead(HWD_CP_ISR0_L) & (mask))? TRUE : FALSE)  
   
/*---------------------**
** L1 Interrupts (IRQ) **
**---------------------*/

/* Enable L1 interrupts by applying mask to IMR1 register */
#define HwdIntEnableL1(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR1_L, (mask), 0)
   
/* Disable L1 interrupts by applying mask to IMR1 register */
#define HwdIntDisableL1(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR1_L, (mask), (mask))
   
/* Control L1 interrupts by writing mask to ISR1 register */
#define HwdIntClrL1(mask) \
   HwdWrite(HWD_CP_ISR1_L, (mask))  

/* Determine if a L1 interrupt defined in mask is active */
#define HwdIsIntL1(mask) \
   ((HwdRead(HWD_CP_ISR1_L) & (mask))? TRUE : FALSE)  

 
/*---------------------**
** L2 Interrupts (IRQ) **
**---------------------*/

/* Enable L2 interrupts by applying mask to IMR2 register */
#define HwdIntEnableL2(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR2_L, (mask), 0)    //HwdClearBitProtect (HWD_CP_IMR2_L, (mask))
   
/* Disable L2 interrupts by applying mask to IMR2 register */
#define HwdIntDisableL2(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR2_L, (mask) ,(mask))
   
/* Control L2 interrupts by writing mask to ISR2 register */
#define HwdIntClrL2(mask) \
   HwdWrite(HWD_CP_ISR2_L, (mask))  

/* Determine if a L2 interrupt defined in mask is active */

#define HwdIsIntL2(mask) \
   ((HwdRead(HWD_CP_ISR2_L) & (mask))? TRUE : FALSE)  


/*---------------------**
** L3 Interrupts (IRQ) **
**---------------------*/

/* Enable L3 interrupts by applying mask to IMR3 register */
#define HwdIntEnableL3(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR3_L, (mask), 0)
   
/* Disable L3 interrupts by applying mask to IMR3 register */
#define HwdIntDisableL3(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR3_L, (mask), (mask))
   
/* Control L3 interrupts by writing mask to ISR3 register */
#define HwdIntClrL3(mask) \
   HwdWrite(HWD_CP_ISR3_L, (mask))  

/* Determine if a L3 interrupt defined in mask is active */
#define HwdIsIntL3(mask) \
   ((HwdRead(HWD_CP_ISR3_L) & (mask))? TRUE : FALSE)  

/*---------------------**
** L4 Interrupts (IRQ) **
**---------------------*/

/* Enable L4 interrupts by applying mask to IMR4 register */
#define HwdIntEnableL4(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR4_L, (mask), 0)
   
/* Disable L4 interrupts by applying mask to IMR4 register */
#define HwdIntDisableL4(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR4_L, (mask), (mask))
   
/* Control L4 interrupts by writing mask to ISR4 register */
#define HwdIntClrL4(mask) \
   HwdWrite(HWD_CP_ISR4_L, (mask))  

/* Determine if a L4 interrupt defined in mask is active */

#define HwdIsIntL4(mask) \
   ((HwdRead(HWD_CP_ISR4_L) & (mask))? TRUE : FALSE)  



/*---------------------**
** L5 Interrupts (IRQ) **
**---------------------*/

/* Enable L5 interrupts by applying mask to IMR5 register */
#define HwdIntEnableL5(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR5_L, (mask), 0)
   
/* Disable L5 interrupts by applying mask to IMR5 register */
#define HwdIntDisableL5(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR5_L, (mask),(mask))
   
/* Control L5 interrupts by writing mask to ISR5 register */
#define HwdIntClrL5(mask) \
   HwdWrite(HWD_CP_ISR5_L, (mask))  

/* Determine if a L5 interrupt defined in mask is active */
#define HwdIsIntL5(mask) \
   ((HwdRead(HWD_CP_ISR5_L) & (mask))? TRUE : FALSE)  

/*---------------------**
** L6 Interrupts (IRQ) **
**---------------------*/

/* Enable L6 interrupts by applying mask to IMR6 register */
#define HwdIntEnableL6(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR6_L, (mask), 0)
   
/* Disable L6 interrupts by applying mask to IMR6 register */
#define HwdIntDisableL6(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR6_L, (mask), (mask))
   
/* Control L6 interrupts by writing mask to ISR6 register */
#define HwdIntClrL6(mask) \
   HwdWrite(HWD_CP_ISR6_L, (mask))  

/* Determine if a L6 interrupt defined in mask is active */

#define HwdIsIntL6(mask) \
   ((HwdRead(HWD_CP_ISR6_L) & (mask))? TRUE : FALSE)  
 
 
/*---------------------**
** L7 Interrupts (IRQ) **
**---------------------*/

/* Enable L7 interrupts by applying mask to IMR7 register */
#define HwdIntEnableL7(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR7_L, (mask), 0)
   
/* Disable L5 interrupts by applying mask to IMR7 register */
#define HwdIntDisableL7(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR7_L, (mask), (mask))
   
/* Control L7 interrupts by writing mask to ISR7 register */
#define HwdIntClrL7(mask) \
   HwdWrite(HWD_CP_ISR7_L, (mask))  

/* Determine if a L7 interrupt defined in mask is active */
#define HwdIsIntL7(mask) \
   ((HwdRead(HWD_CP_ISR7_L) & (mask))? TRUE : FALSE)  
 
/*---------------------**
** L8 Interrupts (IRQ) **
**---------------------*/

/* Enable L8 interrupts by applying mask to IMR8 register */
#define HwdIntEnableL8(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR8_L, (mask),0)
   
/* Disable L8 interrupts by applying mask to IMR8 register */
#define HwdIntDisableL8(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR8_L, (mask), (mask))
   
/* Control L8 interrupts by writing mask to ISR8 register */
#define HwdIntClrL8(mask) \
   HwdWrite(HWD_CP_ISR8_L, (mask))  

/* Determine if a L8 interrupt defined in mask is active */
#define HwdIsIntL8(mask) \
   ((HwdRead(HWD_CP_ISR8_L) & (mask))? TRUE : FALSE)  

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/*---------------------**
** L9 Interrupts (IRQ) **
**---------------------*/

/* Enable L9 interrupts by applying mask to IMR9 register */
#define HwdIntEnableL9(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR9_L, (mask), 0)
   
/* Disable L5 interrupts by applying mask to IMR5 register */
#define HwdIntDisableL9(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR9_L, (mask), (mask))
   
/* Control L9 interrupts by writing mask to ISR9 register */
#define HwdIntClrL9(mask) \
   HwdWrite(HWD_CP_ISR9_L, (mask))  

/* Determine if a L9 interrupt defined in mask is active */
#define HwdIsIntL9(mask) \
   ((HwdRead(HWD_CP_ISR9_L) & (mask))? TRUE : FALSE)  
#endif

/*---------------------**
** H0 Interrupts (FIQ) **
**---------------------*/

/* Enable H0 interrupts by applying mask to IMR H0 register */
#define HwdIntEnableH0(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR0_H, (mask), 0)
   
/* Disable H0 interrupts by applying mask to IMR H0 register */
#define HwdIntDisableH0(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR0_H, (mask), (mask))
   
/* Control H0 interrupts by writing mask to ISR H0 register */
#define HwdIntClrH0(mask) \
   HwdWrite(HWD_CP_ISR0_H, (mask)) 

/* Determine if a H0 interrupt defined in mask is active */
#define HwdIsIntH0(mask) \
      ((HwdRead(HWD_CP_ISR0_H) & (mask))? TRUE : FALSE)  

/*---------------------**
** H1 Interrupts (FIQ) **
**---------------------*/

/* Enable H1 interrupts by applying mask to IMR H1 register */
#define HwdIntEnableH1(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR1_H, (mask), 0)
   
/* Disable H1 interrupts by applying mask to IMR H1 register */
#define HwdIntDisableH1(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR1_H, (mask), (mask))
   
/* Control H1 interrupts by writing mask to ISR H1 register */
#define HwdIntClrH1(mask) \
   HwdWrite(HWD_CP_ISR1_H, (mask)) 

/* Determine if a H1 interrupt defined in mask is active */
#define HwdIsIntH1(mask) \
      ((HwdRead(HWD_CP_ISR1_H) & (mask))? TRUE : FALSE)  

/*---------------------**
** H2 Interrupts (FIQ) **
**---------------------*/

/* Enable H2 interrupts by applying mask to IMR H2 register */
#define HwdIntEnableH2(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR2_H, (mask), 0)
   
/* Disable H2 interrupts by applying mask to IMR H2 register */
#define HwdIntDisableH2(mask) \
     HwdAssistMaskWrite16 (HWD_CP_IMR2_H, (mask), (mask))
   
/* Control H2 interrupts by writing mask to ISR H2 register */
#define HwdIntClrH2(mask) \
   HwdWrite(HWD_CP_ISR2_H, (mask)) 

/* Determine if a H2 interrupt defined in mask is active */
#define HwdIsIntH2(mask) \
      ((HwdRead(HWD_CP_ISR2_H) & (mask))? TRUE : FALSE)  

/*---------------------**
** H3 Interrupts (FIQ) **
**---------------------*/

/* Enable H3 interrupts by applying mask to IMR H3 register */
#define HwdIntEnableH3(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR3_H, (mask), 0)
   
/* Disable H3 interrupts by applying mask to IMR H3 register */
#define HwdIntDisableH3(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR3_H, (mask),(mask))
   
/* Control H3 interrupts by writing mask to ISR H3 register */
#define HwdIntClrH3(mask) \
   HwdWrite(HWD_CP_ISR3_H, (mask)) 

/* Determine if a H3 interrupt defined in mask is active */
#define HwdIsIntH3(mask) \
      ((HwdRead(HWD_CP_ISR3_H) & (mask))? TRUE : FALSE)  

/*---------------------**
** H4 Interrupts (FIQ) **
**---------------------*/

/* Enable H4 interrupts by applying mask to IMR H4 register */
#define HwdIntEnableH4(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR4_H, (mask), 0)
   
/* Disable H4 interrupts by applying mask to IMR H3 register */
#define HwdIntDisableH4(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR4_H, (mask), (mask))
   
/* Control H4 interrupts by writing mask to ISR H4 register */
#define HwdIntClrH4(mask) \
   HwdWrite(HWD_CP_ISR4_H, (mask)) 

/* Determine if a H4 interrupt defined in mask is active */
#define HwdIsIntH4(mask) \
      ((HwdRead(HWD_CP_ISR4_H) & (mask))? TRUE : FALSE)  

#define HwdIsIntH4Enable(mask) \
   ((HwdRead(HWD_CP_IMR4_H) & (mask))? FALSE : TRUE)

/*---------------------**
** H5 Interrupts (FIQ) **
**---------------------*/

/* Enable H5 interrupts by applying mask to IMR H5 register */
#define HwdIntEnableH5(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR5_H, (mask), 0)
   
/* Disable H5 interrupts by applying mask to IMR H3 register */
#define HwdIntDisableH5(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR5_H, (mask), (mask))
   
/* Control H5 interrupts by writing mask to ISR H5 register */
#define HwdIntClrH5(mask) \
   HwdWrite(HWD_CP_ISR5_H, (mask)) 

/* Determine if a H5 interrupt defined in mask is active */
#define HwdIsIntH5(mask) \
      ((HwdRead(HWD_CP_ISR5_H) & (mask))? TRUE : FALSE)  

/*---------------------**
** H6 Interrupts (FIQ) **
**---------------------*/

/* Enable H6 interrupts by applying mask to IMR H6 register */
#define HwdIntEnableH6(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR6_H, (mask),0)
   
/* Disable H6 interrupts by applying mask to IMR H3 register */
#define HwdIntDisableH6(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR6_H, (mask), (mask))
   
/* Control H6 interrupts by writing mask to ISR H6 register */
#define HwdIntClrH6(mask) \
   HwdWrite(HWD_CP_ISR6_H, (mask)) 

/* Determine if a H6 interrupt defined in mask is active */
#define HwdIsIntH6(mask) \
      ((HwdRead(HWD_CP_ISR6_H) & (mask))? TRUE : FALSE)  

/*---------------------**
** H7 Interrupts (FIQ) **
**---------------------*/

/* Enable H7 interrupts by applying mask to IMR H7 register */
#define HwdIntEnableH7(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR7_H, (mask), 0)
   
/* Disable H7 interrupts by applying mask to IMR H3 register */
#define HwdIntDisableH7(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR7_H, (mask), (mask))
   
/* Control H7 interrupts by writing mask to ISR H7 register */
#define HwdIntClrH7(mask) \
   HwdWrite(HWD_CP_ISR7_H, (mask)) 

/* Determine if a H7 interrupt defined in mask is active */
#define HwdIsIntH7(mask) \
      ((HwdRead(HWD_CP_ISR7_H) & (mask))? TRUE : FALSE)  

/*---------------------**
** H8 Interrupts (FIQ) **
**---------------------*/

/* Enable H8 interrupts by applying mask to IMR H8 register */
#define HwdIntEnableH8(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR8_H, (mask),0)
   
/* Disable H8 interrupts by applying mask to IMR H3 register */
#define HwdIntDisableH8(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR8_H, (mask),(mask))
   
/* Control H8 interrupts by writing mask to ISR H8 register */
#define HwdIntClrH8(mask) \
   HwdWrite(HWD_CP_ISR8_H, (mask)) 

/* Determine if a H8 interrupt defined in mask is active */
#define HwdIsIntH8(mask) \
      ((HwdRead(HWD_CP_ISR8_H) & (mask))? TRUE : FALSE)  

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/*---------------------**
** H9 Interrupts (FIQ) **
**---------------------*/

/* Enable H9 interrupts by applying mask to IMR H9 register */
#define HwdIntEnableH9(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR9_H, (mask),0)
   
/* Disable H9 interrupts by applying mask to IMR H3 register */
#define HwdIntDisableH9(mask) \
   HwdAssistMaskWrite16 (HWD_CP_IMR9_H, (mask), (mask))
   
/* Control H9 interrupts by writing mask to ISR H9 register */
#define HwdIntClrH9(mask) \
   HwdWrite(HWD_CP_ISR9_H, (mask)) 

/* Determine if a H9 interrupt defined in mask is active */
#define HwdIsIntH9(mask) \
      ((HwdRead(HWD_CP_ISR9_H) & (mask))? TRUE : FALSE)  
#endif

#ifdef MTK_DEV_SSDVT
/* Enable interrupts by applying mask to IMR register */
#define HwdIntEnable(addr, mask) \
     HwdAssistMaskWrite16 (addr, (mask), 0)

/* Disable interrupts by applying mask to IMR register */
#define HwdIntDisable(addr, mask) \
     HwdAssistMaskWrite16 (addr, (mask), (mask))

/* Control  interrupts by writing mask to ISR register */
#define HwdIntClr(addr, mask) \
     HwdWrite(addr, (mask)) 
#endif

/*----------------------------------**
** CP Software Interrupt Generation **
**----------------------------------*/

#define HwdIntGenSwInt0() \
   HwdWrite(HWD_C2C_SW0, HWD_SW_INT_GEN)

#define HwdIntGenSwInt1() \
   HwdWrite(HWD_C2C_SW1, HWD_SW_INT_GEN)

/*------------------------**
** System Time Interrupts **
**-------------------------*/

/* FOR ST: 0 is masked, 1 is enabled
   Disable ST interrupts by applying mask to ST_CPINT_MASK register */

#define HwdStIntDisable(mask) \
   HwdClearBitProtect (HWD_ST_CPINT_MASK, (mask))
   
/* Enable ST interrupts by applying mask to ST_CPINT_MASK register */
#define HwdStIntEnable(mask) \
   HwdSetBitProtect (HWD_ST_CPINT_MASK, (mask))
   
/* Clear Status Register */
#define HwdStIntClr(mask) \
   HwdWrite(HWD_ST_CPINT_CLR, (mask))  

#define StIntEnable(mask) \
   SysIntDisable(SYS_IRQ_INT);\
   HwdStIntClr(mask);\
   HwdStIntEnable(mask);\
   SysIntEnable(SYS_IRQ_INT);

#define StReadIntMask() \
	HwdRead(HWD_ST_CPINT_MASK)

/* Determine if a ST interrupt defined in mask is active */
#define HwdStIsInt(mask) \
      ((HwdRead(HWD_ST_CPINT_SRC) & (mask))? TRUE : FALSE)  
   
     
/*------------------------**
** EVDO System Time Interrupts **
**-------------------------*/

/* FOR EVDO ST: 0 is masked, 1 is enabled
   Disable EVDO ST interrupts by applying mask to ST_CPINT_MASK register */

#define HwdStEvIntDisable(mask) \
   HwdClearBitProtect (HWD_STDO_CPINT_MASK, (mask))
   
/* Enable ST interrupts by applying mask to ST_CPINT_MASK register */
#define HwdStEvIntEnable(mask) \
   HwdSetBitProtect (HWD_STDO_CPINT_MASK, (mask))
   
/* Clear Status Register */
#define HwdStEvIntClr(mask) \
   HwdWrite(HWD_STDO_CPINT_CLR, (mask))  

#define StEvIntEnable(mask) \
   SysIntDisable(SYS_IRQ_INT);\
   HwdStEvIntClr(mask);\
   HwdStEvIntEnable(mask);\
   SysIntEnable(SYS_IRQ_INT);

#define StEvReadIntMask() \
	HwdRead(HWD_STDO_CPINT_MASK)

/* Determine if a ST interrupt defined in mask is active */
#define HwdStEvIsInt(mask) \
      ((HwdRead(HWD_STDO_CPINT_SRC) & (mask))? TRUE : FALSE)  

/*****************************************************************************
* $Log: hwdint.h $
* Revision 1.2  2004/03/25 11:45:55  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.3  2004/03/17 17:58:10  agontar
* removed group GPIO interrupt macros
* Revision 1.2  2003/10/21 15:37:22  wavis
* Added HwdIntTriggerSW0() and HwdIntTriggerSW1() definitions.
* Added H3 and L3 interrupt masking macros.
* Revision 1.1  2003/05/12 15:26:18  fpeng
* Initial revision
*
*
* Revision 1.6  2002/06/04 08:07:13  mshaver
* Added VIA Technologies copyright notice.
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
