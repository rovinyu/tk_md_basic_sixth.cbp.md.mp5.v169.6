/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef EXEDEFS_H
#define EXEDEFS_H
/*****************************************************************************
 
  FILE NAME: exedefs.h

  DESCRIPTION:
   
    This file contains function prototypes and type definitions
    for the Executive Unit services.

  DEFINITIONS:

    The following function protoypes are declared in this file:

    Regional Procedures
    -------------------
    ExeInit
  
*****************************************************************************/
#ifndef MTK_PLT_ON_PC
#include "cp_nucleus.h"
#endif
#include "sysdefs.h"
#include "exeapi.h"
#include "exeerrs.h"

/*--------------------------------------------------------------------
* Define Exe constants
*--------------------------------------------------------------------*/

/* HISR stack size in bytes */
#if (defined __OPTIMISE_TIME)
/* stack size definition for -Otime option */
#define EXE_HISR_PRIO0_STACK_SIZE     2000
#define EXE_HISR_PRIO1_STACK_SIZE     1900
#else
/* stack size definition for -Ospace option */
#define EXE_HISR_PRIO0_STACK_SIZE     2000
#define EXE_HISR_PRIO1_STACK_SIZE     1648
#endif


#ifndef MTK_PLT_ON_PC
/* Size of mailbox queue record in unit32 data elements */ /*it is sizeof(ExeMsgDataT)*/
#define EXE_MAIL_QUEUE_REC_SIZE  4 
#endif /* MTK_PLT_ON_PC */

/*---------------------------------------------------------------------
* The structure of the overhead associated with Exe Message Buffers.
*---------------------------------------------------------------------*/
#ifdef MTK_DEV_BUG_FIX_SS
typedef struct
{
   uint16         Index;
   uint16         Freeup;
} ExeBufHeaderT;

#define MSG_BUFHEAD_FREEUP 0xAF12
#define MSG_BUFHEAD_USING  0x8765

#else
typedef struct
{
   uint32         Index;
} ExeBufHeaderT;
#endif

/* EXE buffer overhead is actually sizeof(ExeBufHeaderT)).  However,
 * the pre-processor directives in various modules would fail if
 * "sizeof()" was used.  Therefore, a hard-coded number has to be
 * used!!!  Please, update it when "ExeBufHeaderT" is modified.
 */ 
#define  EXEBUF_OVERHEAD            4

#define  EXE_BUFFER_NU_OVERHEAD     (PM_OVERHEAD + EXEBUF_OVERHEAD)

/* Define constants for ExeMsgBuffer routines */
/* Note: message buffer size suppose to be even to be compatible with IPC interface*/
#define EXE_SIZE_MSG_BUFF_1     32   /* Size of msg data buffer 1 in bytes */
#define EXE_SIZE_MSG_BUFF_2     192  /* Size of msg data buffer 2 in bytes */
#define EXE_SIZE_MSG_BUFF_3     448  /* Size of msg data buffer 3 in bytes */
#if ((defined SYS_OPTION_GPS_EXTERNAL) || (SYS_OPTION_GPS_HW ==SYS_GPS_LOCAL_INTERNAL))
#define EXE_SIZE_MSG_BUFF_4     1024  /* Size of msg data buffer 4 in bytes */
#else
#define EXE_SIZE_MSG_BUFF_4     785  /* Size of msg data buffer 4 in bytes */
#endif
#if (defined MTK_DEV_C2K_IRAT) || (SYS_BOARD >= SB_JADE)
#define EXE_SIZE_MSG_BUFF_5     2048 /* Size of msg data buffer 5 in bytes */
#define EXE_SIZE_MSG_BUFF_6     3600 /* Size of msg data buffer 6 in bytes */
#define EXE_SIZE_MSG_BUFF_7     8000 /* Size of msg data buffer 7 in bytes */
#ifdef __DYNAMIC_ANTENNA_TUNING__
#define EXE_SIZE_MSG_BUFF_8     14000 /* Size of msg data buffer 8 in bytes */
#endif
#endif  /* MTK_DEV_C2K_IRAT */

#ifdef MTK_DEV_ALL_IN_IRAM
#define EXE_NUM_MSG_BUFF_1      10   /* Number of msg data buffers type 1  */
#define EXE_NUM_MSG_BUFF_2       5   /* Number of msg data buffers type 2  */
#define EXE_NUM_MSG_BUFF_3       4   /* Number of msg data buffers type 3  */
#define EXE_NUM_MSG_BUFF_4       2   /* Number of msg data buffers type 4  */
#else
#define EXE_NUM_MSG_BUFF_1      45   /* Number of msg data buffers type 1  */
#define EXE_NUM_MSG_BUFF_2     500   /* Number of msg data buffers type 2  */
#define EXE_NUM_MSG_BUFF_3      26   /* Number of msg data buffers type 3  */
#define EXE_NUM_MSG_BUFF_4      18   /* Number of msg data buffers type 4  */
#if (defined MTK_DEV_C2K_IRAT) || (SYS_BOARD >= SB_JADE)
#define EXE_NUM_MSG_BUFF_5      3    /* Number of msg data buffers type 5  */
#define EXE_NUM_MSG_BUFF_6      2    /* Number of msg data buffers type 6  */
#define EXE_NUM_MSG_BUFF_7      4    /* Number of msg data buffers type 7  */
#ifdef __DYNAMIC_ANTENNA_TUNING__
#define EXE_NUM_MSG_BUFF_8      4    /* Number of msg data buffers type 8 */
#endif
#endif  /* MTK_DEV_C2K_IRAT */
#endif

/* Define the maximum message buffer size */
/* the max msg buf size is size 3 and not size 4 because size 4
   is used specifically for NAM accesses DBM and ETS */
#define EXE_MAX_MSG_BUFF_SIZE   EXE_SIZE_MSG_BUFF_3

/* Define the maximum message buffer size for IOP use. This should only
   be used by IOP. */
#define EXE_MAX_ETS_MSG_SIZE    EXE_SIZE_MSG_BUFF_4
#define  MSG_BUFFER_FREEUP_MARKER     0xAF1234AF
#define  MSG_BUFFER_FREE_CLEAR      0x87654321




/*--------------------------------------------------------------------
* Define Exe message buffer types and the msg info data structure
*--------------------------------------------------------------------*/

typedef enum
{
   EXE_MSG_BUFF_TYPE_1,             /* small                           */
   EXE_MSG_BUFF_TYPE_2,             /* medium                          */
   EXE_MSG_BUFF_TYPE_3,             /* large                           */
   EXE_MSG_BUFF_TYPE_4,             /* super large                     */
#if (defined MTK_DEV_C2K_IRAT) || (SYS_BOARD >= SB_JADE)
   EXE_MSG_BUFF_TYPE_5,             /*extra large, used for RAT change message cross core*/
   EXE_MSG_BUFF_TYPE_6,             /*extra large, used for measure report message cross core*/
   EXE_MSG_BUFF_TYPE_7,             /*extra large, used for measure report message cross core*/
#ifdef __DYNAMIC_ANTENNA_TUNING__
   EXE_MSG_BUFF_TYPE_8,             /*extra large, used for DAT data*/
#endif
#endif
   EXE_NUM_DIFF_MSG_BUFFS           /* Number of different msg buffers */
} ExeMsgBuffTypeT;

typedef struct 
{
    uint32       BuffSize;
    ExeBufferT  *BuffCbP;
} ExeMsgBuffInfoT;

/*--------------------------------------------------------------------
* Define Exe Task Control Block data structure
*--------------------------------------------------------------------*/

typedef struct
{
   int32           NumMsgs;
   int32           NumMsgsInQueue[EXE_NUM_MAILBOX];
   NU_QUEUE        MailQueueCb[EXE_NUM_MAILBOX];
   NU_TASK         TaskCb;
   NU_EVENT_GROUP  EventGroupCb;
} ExeTaskCbT;

typedef PACKED_PREFIX struct
{
   uint32  TaskId;
   uint32  MsgId;
   uint16  MsgLenth;
   uint8   MsgData[EXE_MAX_ETS_MSG_SIZE];
} PACKED_POSTFIX  ExeFautTaskMsgDataT;

/*--------------------------------------------------------------------
*  Define macros used in exeapi.c and exeapi_iram.c
*--------------------------------------------------------------------*/
#ifdef SYS_DEBUG_FAULT_FILE_INFO

/* Functions to check thread id for LISR/HISR state with FILE_INFO compiled IN */
#define CheckThreadId()                                  \
      __CheckThreadId(Filename, Linenumber);
extern void __CheckThreadId (const char *Filename, unsigned Linenumber);

#define CheckHisrThreadId()                              \
      __CheckHisrThreadId(Filename, Linenumber);
extern void __CheckHisrThreadId (const char *Filename, unsigned Linenumber);

extern void CheckHisrThreadIdWithNoFileInfo (void);

#else
/* Functions to check thread id for LISR/HISR state with FILE_INFO compiled OUT */
extern void CheckThreadId (void);
extern void CheckHisrThreadId (void);
extern void CheckHisrThreadIdWithNoFileInfo (void);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*--------------------------------------------------------------------
*  Define constants used in exeapi.c and exeapi_iram.c
*--------------------------------------------------------------------*/

/* Msg buffer size marker */
#define MSG_BUFFER_MARKER  0xed
#define MSG_BUFFER_SENT_MARKER  0xad

/*--------------------------------------------------------------------
*  Define constants used in exeutil.c and exeutil_iram.c
*--------------------------------------------------------------------*/

#define SIZE_MSG_PTR_STATS         EXE_NUM_MSG_BUFF_1 +    \
                                   EXE_NUM_MSG_BUFF_2 +    \
                                   EXE_NUM_MSG_BUFF_3 +    \
                                   EXE_NUM_MSG_BUFF_4 + 1

#if ((defined MTK_DEV_C2K_IRAT) || (SYS_BOARD >= SB_JADE)) && (!defined (MTK_DEV_ALL_IN_IRAM))
#undef  SIZE_MSG_PTR_STATS
#define SIZE_MSG_PTR_STATS         EXE_NUM_MSG_BUFF_1 +    \
                                   EXE_NUM_MSG_BUFF_2 +    \
                                   EXE_NUM_MSG_BUFF_3 +    \
                                   EXE_NUM_MSG_BUFF_4 +    \
                                   EXE_NUM_MSG_BUFF_5 +    \
                                   EXE_NUM_MSG_BUFF_6 +    \
                                   EXE_NUM_MSG_BUFF_7 + 1

#ifdef __DYNAMIC_ANTENNA_TUNING__
#undef  SIZE_MSG_PTR_STATS
#define SIZE_MSG_PTR_STATS         EXE_NUM_MSG_BUFF_1 +    \
                                   EXE_NUM_MSG_BUFF_2 +    \
                                   EXE_NUM_MSG_BUFF_3 +    \
                                   EXE_NUM_MSG_BUFF_4 +    \
                                   EXE_NUM_MSG_BUFF_5 +    \
                                   EXE_NUM_MSG_BUFF_6 +    \
                                   EXE_NUM_MSG_BUFF_7 +    \
                                   EXE_NUM_MSG_BUFF_8 + 1
#endif

#endif

#define SIZE_SPY_MSG_PTR_STATS     12

/*--------------------------------------------------------------------
*  Define data structures used in exeutil.c and exeutil_iram.c
*--------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
   uint8        TaskId;
   uint8        NumMsgs;
} PACKED_POSTFIX MsgMboxStatsT;

typedef PACKED_PREFIX struct
{
   uint16        Alloc;   
   uint16        MaxAlloc;   
   uint16        MaxSize;   
} PACKED_POSTFIX  MsgBuffLogInfoT;

typedef PACKED_PREFIX struct
{
   uint16                BuffAlloc;   
   uint16                BuffMaxAlloc;   
   MsgBuffLogInfoT       BuffInfo[EXE_NUM_DIFF_MSG_BUFFS];
} PACKED_POSTFIX  SpyMsgBuffStatsT;
    
typedef PACKED_PREFIX struct
{
   uint8        BuffState;
   uint8        TaskAlloc;
   uint8        TaskSentTo;
   uint32       MsgId;
   uint8        FunctionName[EXE_MSG_PTR_LOG_FUNCTION_NAME_LENGTH];
   uint16       LineNumber;
} PACKED_POSTFIX  SpyMsgBuffPtrStatsT;

/*
 * Debug message buffer pointer statistics definition -
 * this is used when a MON_HALT occurs.
 * The ExeFault() function will pump out all tasks's buffer pointer statistics to ETS.
 */
typedef struct
{
   void         *BuffPtr;
   uint8        BuffType;
   uint8        BuffState;
   uint8        TaskAlloc;
   uint8        TaskSentTo;
   uint32       MsgId;
   const char*  FunctionNamePtr;
   uint16       LineNumber;
} ExeMsgBuffPtrStatsT;

/*
 *	Keep track of actual sizes of msgbuffers being
 *	allocated so far
 */
#define	EXE_UTIL_MSGBUFFDISTR_BUCKET_NUM	18

typedef PACKED_PREFIX struct 
{
	uint16	Size;	/* size of msgbuffer */
	uint16	Num;	/* number of allocation requests */
} PACKED_POSTFIX  MsgBuffDistrT;

typedef enum 
{
   EXE_MSG_BUFF_FREE_NO_ERR         = 0,
   EXE_MSG_BUFF_FREE_NULL_PTR,
   EXE_MSG_BUFF_FREE_REDUNDANT_FREE,
   EXE_MSG_BUFF_FREE_INVALID_FREE
}MsgBuffFreeErrT;

typedef enum
{
   MSGBUFFSTATE_GET  = 0,
   MSGBUFFSTATE_SENT = 1,
   MSGBUFFSTATE_READ = 2,
   MSGBUFFSTATE_FREE = 3
}MsgBuffState_Enum;

/*****************************************************************************
 
  FUNCTION NAME: ExeInit

  DESCRIPTION:

    This routine performs Exe unit initialization of data
    structures.

  PARAMETERS:
      
    None.

  RETURNED VALUES:

    None.
 
*****************************************************************************/

extern void ExeInit(void);

/*****************************************************************************

  FUNCTION NAME: ExeFault

  DESCRIPTION:

    This routine sends software fault declared by the EXE unit to
    the ETS. ExeFaults by definition are of HALT type which means this
    routine does not return to the caller and the processor executes
    an infinite loop.

  PARAMETERS:

    INPUTS:

    ExeFaultType - an EXE unit error
    ExeFaultData - a void pointer to specific fault data to be delivered to ETS

  RETURNED VALUES:

    None

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.

  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define ExeFault(ExeFaultType, ExeError, ExeFaultData, FaultSize) \
        __ExeFault(ExeFaultType, ExeError, ExeFaultData, FaultSize, FunctionName, Linenumber)

extern void __ExeFault(ExeFaultTypeT ExeFaultType, ExeErrsT ExeError,
                       void *ExeFaultData, uint16 FaultSize,
                       const char *FunctionName, unsigned Linenumber);
#else

extern void ExeFault(ExeFaultTypeT ExeFaultType, ExeErrsT ExeError,
                     void *ExeFaultData, uint16 FaultSize);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
 
  FUNCTION NAME: CallExeFault

  DESCRIPTION:

    This routine calls ExeFault setting the appropriate fault type

  PARAMETERS:
      
    INPUTS:

    Status - Nucleus error status

  RETURNED VALUES:

    None 

  ERROR HANDLING:

    No error codes are returned. This routine handles all Nucleus errors.
  
  TASKING CHANGES:

    None.

*****************************************************************************/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define CallExeFault(Status) __CallExeFault(Status, Filename, Linenumber)

void __CallExeFault(int32 Status, const char *Filename, unsigned Linenumber);

#else

void CallExeFault(int32 Status);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

/*****************************************************************************
* $Log: exedefs.h $
* Revision 1.7  2006/01/19 15:46:20  vxnguyen
* Added the CallExeFault() declaration.
* Revision 1.6  2006/01/03 10:11:09  wavis
* Merging in VAL.
* Revision 1.5.1.2  2005/12/16 10:07:41  wavis
* Increase Msg Buf 4 size to account for larger Response Message struct.
* Revision 1.5.1.1  2004/11/18 11:24:27  wavis
* Duplicate revision
* Revision 1.5  2004/11/18 11:24:27  vxnguyen
* - Increased EXE_NUM_MSG_BUFF_2 from 40 to 100 to support the number of messages
*   that RLP would have to use to send 800 MuxPdu buffers to IOP.
* Revision 1.4  2004/11/02 13:37:03  asharma
* Increased EXE_NUM_MSG_BUFF_4 from 3 to 4 to solve the problem reported in CR3448.
* Revision 1.3  2004/10/06 16:43:11  vxnguyen
* Optimized EXE message buffer sizes for SO33.
* Revision 1.2  2004/03/25 11:45:48  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.4  2004/02/10 11:12:25  vxnguyen
* Merged CBP4.0 Release 8.05.0 changes.
* Revision 1.3  2003/12/18 14:05:44  vxnguyen
* Changed 'NumMsgs' and 'NumMsgsInQueue[]' in 'ExeTaskCbT' to SIGNED integers.
* Revision 1.2  2003/12/10 09:03:19  vxnguyen
* Declared 'ExeFault()' (moved here from exeapi.h).
* Revision 1.1  2003/05/12 15:26:13  fpeng
* Initial revision
* Revision 1.13  2002/09/30 14:46:53  mshaver
* Change EXE_SIZE_MSG_BUFF_4 to 560 (from 600) as the 
* NAM size has decreased as the NAM data structure is now 
* always packed.
* Revision 1.12  2002/07/26 13:52:58  ameya
* Increased EXE_SIZE_MSG_BUFF_4 size to 600 bytes and number to 3.
* Revision 1.11  2002/07/25 09:58:28  etarolli
* Added EXE_IOP_MSG_SIZE definition so IOP unit can use the largest EXE msg buffer size (type 4).
* Revision 1.10  2002/07/15 18:27:31  vxnguyen
* Added buffer type 4 definitions.
* Revision 1.9  2002/06/07 13:49:48  wavis
* Provide compile option for increasing largest EXE message buffer from 406 to 522 bytes (compile option is disabled).
* Revision 1.8  2002/06/04 08:07:10  mshaver
* Added VIA Technologies copyright notice.
* Revision 1.7  2002/05/23 10:04:44  cmastro
* changed bufsize[1] to 64 bytes
* Revision 1.6  2002/05/13 15:33:50  mshaver
* Merge revision 1.5.1.2
* Revision 1.5.1.2  2002/03/13 09:54:27  srodenba
* Modifications to return the Nucleus directory back to its original state (i.e. no CP changes). These changes will allow Nucleus to be upgraded now and in the future with minimal CP changes.
* Revision 1.5.1.1  2002/02/22 17:15:03  srodenba
* Duplicate revision
* Revision 1.5  2002/02/22 17:15:03  mshaver
* Increased the number of message data buffers type 1 from 40 to 45
* and the number of message data buffers type 3 from 10 to 15 as
* the maximum number of buffers in use during calls was getting
* dangerously close to the maximum.
* Revision 1.4  2002/01/09 14:44:26  SNREDDY
* changed EXE_SIZE_MSG_BUFF_3 to 406 from 380.
* Revision 1.3  2001/11/17 17:15:10  snreddy
* Changed Msg Buf 3 size to 380 bytes
* Revision 1.2  2001/10/06 08:57:34  mshaver
* Merge in revision 1.1.1.2
* Revision 1.1.1.2  2001/08/07 08:14:12  mshaver
* Increased maximum message buffer size to 328 from 326 so the
* Dspv encoder / decoder test can extract 160 words on the input
* and output.
* Revision 1.1.1.1  2001/08/01 08:27:00  mshaver
* Duplicate revision
* Revision 1.1  2001/08/01 08:27:00  MSHAVER
* Initial revision
*****************************************************************************/

#endif
/**Log information: \main\3 2012-03-23 07:11:16 GMT yliu
** remove suspicious ^L character**/
