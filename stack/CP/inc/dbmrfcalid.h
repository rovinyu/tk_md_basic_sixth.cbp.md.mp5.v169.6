/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * dbmrfcalid.h
 *
 * Project:
 * --------
 * C2K
 *
 * Description:
 * ------------
 * Header file containing typedefs and definitions pertaining
 * to the RF infrastructure.
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#if defined(MTK_PLT_RF_ORIONC) && (!defined(MTK_DEV_HW_SIM_RF))
/*---- COMMON ----*/
/* 1 */DBM_RF_CAL_ITEM (HWD_AFC_PARMS,                           8,   HwdDefaultAfcValue, HwdCalibAFCParmsProcess, HWD_RF_BAND_UNDEFINED)
/* 2 */DBM_RF_CAL_ITEM (HWD_TEMPERATURE_PARMS,                   24,  HwdDefaultTemperatureCalibData, HwdCalibTemperatureParmsProcess, HWD_RF_BAND_UNDEFINED)
/*---- BAND A ----*/
/* 3 */DBM_RF_CAL_ITEM (HWD_BAND_A_TXAGC,                        112, HwdDefaultTxPwrCalibData_BandA, HwdCalibTxAgcDataProcess, HWD_RF_BAND_A)
/* 4 */DBM_RF_CAL_ITEM (HWD_BAND_A_TXAGC_FREQ_ADJ,               128, HwdDefaultTxAgcFreqAdjCalibData_BandA, HwdCalibTxAgcFreqAdjDataProcess, HWD_RF_BAND_A)
/* 5 */DBM_RF_CAL_ITEM (HWD_BAND_A_TXAGC_TEMP_ADJ,               56,  HwdDefaultTxAgcTempAdjCalibData_BandA, HwdCalibTxAgcTempAdjDataProcess, HWD_RF_BAND_A)
/* 6 */DBM_RF_CAL_ITEM (HWD_BAND_A_TX_HDET,                      6,   HwdDefaultTxPdetCalibData_BandA, HwdCalibTxPwrDetTblProcess, HWD_RF_BAND_A)
/* 7 */DBM_RF_CAL_ITEM (HWD_BAND_A_TX_HDET_FREQ_ADJ,             64,  HwdDefaultTxPdetFreqAdjCalibData_BandA, HwdCalibTxPwrDetVsFreqProcess, HWD_RF_BAND_A)
/* 8 */DBM_RF_CAL_ITEM (HWD_BAND_A_RXAGC_MULTIGAIN_FREQ_ADJ,     288, HwdDefaultMainRxAgcFreqAdjCalibData_BandA, HwdCalibRxAgcMultiGainFreqAdjDataProcess, HWD_RF_BAND_A)
/* 9 */DBM_RF_CAL_ITEM (HWD_BAND_A_RXAGC_TEMP_ADJ,               24,  HwdDefaultMainRxAgcTempAdjCalibData_BandA, HwdCalibRxAgcTempAdjDataProcess, HWD_RF_BAND_A)
/*---- DIVERSITY BAND A ----*/
/* 10 */DBM_RF_CAL_ITEM (HWD_BAND_A_DIV_RXAGC_MULTIGAIN_FREQ_ADJ, 288, HwdDefaultDivRxAgcFreqAdjCalibData_BandA, HwdCalibDivRxAgcMultiGainFreqAdjDataProcess, HWD_RF_BAND_A)
/* 11 */DBM_RF_CAL_ITEM (HWD_BAND_A_DIV_RXAGC_TEMP_ADJ,           24,  HwdDefaultDivRxAgcTempAdjCalibData_BandA, HwdCalibDivRxAgcTempAdjDataProcess, HWD_RF_BAND_A)
/* 12 */DBM_RF_CAL_ITEM (HWD_BAND_A_GPS_CAL,                      16,  dummyCalData, HwdGetGpsCalibDBProcess, HWD_RF_BAND_A)

/*---- BAND B ----*/
/* 13 */DBM_RF_CAL_ITEM (HWD_BAND_B_TXAGC,                        112, HwdDefaultTxPwrCalibData_BandB, HwdCalibTxAgcDataProcess, HWD_RF_BAND_B)
/* 14 */DBM_RF_CAL_ITEM (HWD_BAND_B_TXAGC_FREQ_ADJ,               128, HwdDefaultTxAgcFreqAdjCalibData_BandB, HwdCalibTxAgcFreqAdjDataProcess, HWD_RF_BAND_B)
/* 15 */DBM_RF_CAL_ITEM (HWD_BAND_B_TXAGC_TEMP_ADJ,               56,  HwdDefaultTxAgcTempAdjCalibData_BandB, HwdCalibTxAgcTempAdjDataProcess, HWD_RF_BAND_B)
/* 16 */DBM_RF_CAL_ITEM (HWD_BAND_B_TX_HDET,                      6,   HwdDefaultTxPdetCalibData_BandB, HwdCalibTxPwrDetTblProcess, HWD_RF_BAND_B)
/* 17 */DBM_RF_CAL_ITEM (HWD_BAND_B_TX_HDET_FREQ_ADJ,             64,  HwdDefaultTxPdetFreqAdjCalibData_BandB, HwdCalibTxPwrDetVsFreqProcess, HWD_RF_BAND_B)
/* 18 */DBM_RF_CAL_ITEM (HWD_BAND_B_RXAGC_MULTIGAIN_FREQ_ADJ,     288, HwdDefaultMainRxAgcFreqAdjCalibData_BandB, HwdCalibRxAgcMultiGainFreqAdjDataProcess, HWD_RF_BAND_B)
/* 19 */DBM_RF_CAL_ITEM (HWD_BAND_B_RXAGC_TEMP_ADJ,               24,  HwdDefaultMainRxAgcTempAdjCalibData_BandB, HwdCalibRxAgcTempAdjDataProcess, HWD_RF_BAND_B)
/*---- DIVERSITY BAND B ----*/
/* 20 */DBM_RF_CAL_ITEM (HWD_BAND_B_DIV_RXAGC_MULTIGAIN_FREQ_ADJ, 288, HwdDefaultDivRxAgcFreqAdjCalibData_BandB, HwdCalibDivRxAgcMultiGainFreqAdjDataProcess, HWD_RF_BAND_B)
/* 21 */DBM_RF_CAL_ITEM (HWD_BAND_B_DIV_RXAGC_TEMP_ADJ,           24,  HwdDefaultDivRxAgcTempAdjCalibData_BandB, HwdCalibDivRxAgcTempAdjDataProcess, HWD_RF_BAND_B)
/* 22 */DBM_RF_CAL_ITEM (HWD_BAND_B_GPS_CAL,                      16,  dummyCalData, HwdGetGpsCalibDBProcess, HWD_RF_BAND_B)

/*---- BAND C ----*/
/* 23 */DBM_RF_CAL_ITEM (HWD_BAND_C_TXAGC,                        112, HwdDefaultTxPwrCalibData_BandC, HwdCalibTxAgcDataProcess, HWD_RF_BAND_C)
/* 24 */DBM_RF_CAL_ITEM (HWD_BAND_C_TXAGC_FREQ_ADJ,               128, HwdDefaultTxAgcFreqAdjCalibData_BandC, HwdCalibTxAgcFreqAdjDataProcess, HWD_RF_BAND_C)
/* 25 */DBM_RF_CAL_ITEM (HWD_BAND_C_TXAGC_TEMP_ADJ,               56,  HwdDefaultTxAgcTempAdjCalibData_BandC, HwdCalibTxAgcTempAdjDataProcess, HWD_RF_BAND_C)
/* 26 */DBM_RF_CAL_ITEM (HWD_BAND_C_TX_HDET,                      6,   HwdDefaultTxPdetCalibData_BandC, HwdCalibTxPwrDetTblProcess, HWD_RF_BAND_C)
/* 27 */DBM_RF_CAL_ITEM (HWD_BAND_C_TX_HDET_FREQ_ADJ,             64,  HwdDefaultTxPdetFreqAdjCalibData_BandC, HwdCalibTxPwrDetVsFreqProcess, HWD_RF_BAND_C)
/* 28 */DBM_RF_CAL_ITEM (HWD_BAND_C_RXAGC_MULTIGAIN_FREQ_ADJ,     288, HwdDefaultMainRxAgcFreqAdjCalibData_BandC, HwdCalibRxAgcMultiGainFreqAdjDataProcess, HWD_RF_BAND_C)
/* 29 */DBM_RF_CAL_ITEM (HWD_BAND_C_RXAGC_TEMP_ADJ,               24,  HwdDefaultMainRxAgcTempAdjCalibData_BandC, HwdCalibRxAgcTempAdjDataProcess, HWD_RF_BAND_C)
/*---- DIVERSITY BAND C ----*/
/* 30 */DBM_RF_CAL_ITEM (HWD_BAND_C_DIV_RXAGC_MULTIGAIN_FREQ_ADJ, 288, HwdDefaultDivRxAgcFreqAdjCalibData_BandC, HwdCalibDivRxAgcMultiGainFreqAdjDataProcess, HWD_RF_BAND_C)
/* 31 */DBM_RF_CAL_ITEM (HWD_BAND_C_DIV_RXAGC_TEMP_ADJ,           24,  HwdDefaultDivRxAgcTempAdjCalibData_BandC, HwdCalibDivRxAgcTempAdjDataProcess, HWD_RF_BAND_C)
/* 32 */DBM_RF_CAL_ITEM (HWD_BAND_C_GPS_CAL,                      16,  dummyCalData, HwdGetGpsCalibDBProcess, HWD_RF_BAND_C)

/*---- GPS CALIBR ----*/
/* GPS data are not changed*/
/* 33 */DBM_RF_CAL_ITEM (HWD_GPS_CAL,                             110, dummyCalData, NULL, HWD_RF_BAND_UNDEFINED)
/* 34 */DBM_RF_CAL_ITEM (HWD_GPS_RXAGC_TEMP_ADJ,                  24,  dummyCalData, NULL, HWD_RF_BAND_UNDEFINED)
/* 35 */DBM_RF_CAL_ITEM (HWD_GPS_CHAR_DB,                         32,  dummyCalData, NULL, HWD_RF_BAND_UNDEFINED)
/* 36 */DBM_RF_CAL_ITEM (HWD_BAND_D_GPS_CAL,                      16,  dummyCalData, NULL, HWD_RF_BAND_UNDEFINED)
/* 37 */DBM_RF_CAL_ITEM (HWD_BAND_E_GPS_CAL,                      16,  dummyCalData, NULL, HWD_RF_BAND_UNDEFINED)

#else

        /** Temperature ADC cal data*/
/* 0 */DBM_RF_CAL_ITEM(HWD_AFC_PARMS, sizeof(HwdAfcCalibParmT),  hwdDefaultAfcValue, HwdCalibAFCParmsProcess, HWD_RF_BAND_UNDEFINED)
/* 1 */DBM_RF_CAL_ITEM(HWD_AFC_COMP_PARMS, sizeof(HwdAfcCompCalibParmT),  hwdDefaultAfcCompValue, HwdCalibAfcCompParmsProcess, HWD_RF_BAND_UNDEFINED)
/* 2 */DBM_RF_CAL_ITEM(HWD_TEMP_ADC, sizeof(HwdTempCalibTableT), hwdDefaultTempAdcCalData, HwdCalibTemperatureParmsProcess, HWD_RF_BAND_UNDEFINED)

/* 3 */DBM_RF_CAL_ITEM(HWD_TX_TPC_LEVEL_BAND_A, sizeof(HwdTxPwrCalibDataT), hwdDefaultTxTpcLevel_BandA, HwdCalibTxAgcDataProcess,HWD_RF_BAND_A)
/* 4 */DBM_RF_CAL_ITEM(HWD_TX_TPC_LEVEL_BAND_B, sizeof(HwdTxPwrCalibDataT), hwdDefaultTxTpcLevel_BandB, HwdCalibTxAgcDataProcess,HWD_RF_BAND_B)
/* 5 */DBM_RF_CAL_ITEM(HWD_TX_TPC_LEVEL_BAND_C, sizeof(HwdTxPwrCalibDataT), hwdDefaultTxTpcLevel_BandC, HwdCalibTxAgcDataProcess,HWD_RF_BAND_C)
/* 6 */DBM_RF_CAL_ITEM(HWD_TX_TPC_LEVEL_BAND_D, sizeof(HwdTxPwrCalibDataT), hwdDefaultTxTpcLevel_BandD, HwdCalibTxAgcDataProcess,HWD_RF_BAND_D)
/* 7 */DBM_RF_CAL_ITEM(HWD_TX_TPC_LEVEL_BAND_E, sizeof(HwdTxPwrCalibDataT), hwdDefaultTxTpcLevel_BandE, HwdCalibTxAgcDataProcess,HWD_RF_BAND_E)

        /** Tx PA gain frequency and temperature compensation cal data*/
/* 8 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_1XRTT_BAND_A, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainComp1xRtt_BandA, HwdCalib1xRttTxPaGainCompDataProcess, HWD_RF_BAND_A)
/* 9 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_1XRTT_BAND_B, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainComp1xRtt_BandB, HwdCalib1xRttTxPaGainCompDataProcess, HWD_RF_BAND_B)
/* 10 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_1XRTT_BAND_C, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainComp1xRtt_BandC, HwdCalib1xRttTxPaGainCompDataProcess, HWD_RF_BAND_C)
/* 11 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_1XRTT_BAND_D, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainComp1xRtt_BandD, HwdCalib1xRttTxPaGainCompDataProcess, HWD_RF_BAND_D)
/* 12 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_1XRTT_BAND_E, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainComp1xRtt_BandE, HwdCalib1xRttTxPaGainCompDataProcess, HWD_RF_BAND_E)

/* 13 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_EVDO_BAND_A, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainCompEvdo_BandA, HwdCalibEvdoTxPaGainCompDataProcess, HWD_RF_BAND_A)
/* 14 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_EVDO_BAND_B, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainCompEvdo_BandB, HwdCalibEvdoTxPaGainCompDataProcess, HWD_RF_BAND_B)
/* 15 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_EVDO_BAND_C, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainCompEvdo_BandC, HwdCalibEvdoTxPaGainCompDataProcess, HWD_RF_BAND_C)
/* 16 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_EVDO_BAND_D, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainCompEvdo_BandD, HwdCalibEvdoTxPaGainCompDataProcess, HWD_RF_BAND_D)
/* 17 */DBM_RF_CAL_ITEM(HWD_TX_PAGAIN_COMP_EVDO_BAND_E, sizeof(HwdTxPaGainCompTblT), hwdDefaultTxPaGainCompEvdo_BandE, HwdCalibEvdoTxPaGainCompDataProcess, HWD_RF_BAND_E)

/* 18 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_BAND_A, sizeof(HwdTxDetCouplerLossTblT), hwdDefaultTxDetCpl_BandA, HwdCalibTxDetCplDataProcess, HWD_RF_BAND_A)
/* 19 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_BAND_B, sizeof(HwdTxDetCouplerLossTblT), hwdDefaultTxDetCpl_BandB, HwdCalibTxDetCplDataProcess, HWD_RF_BAND_B)
/* 20 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_BAND_C, sizeof(HwdTxDetCouplerLossTblT), hwdDefaultTxDetCpl_BandC, HwdCalibTxDetCplDataProcess, HWD_RF_BAND_C)
/* 21 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_BAND_D, sizeof(HwdTxDetCouplerLossTblT), hwdDefaultTxDetCpl_BandD, HwdCalibTxDetCplDataProcess, HWD_RF_BAND_D)
/* 22 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_BAND_E, sizeof(HwdTxDetCouplerLossTblT), hwdDefaultTxDetCpl_BandE, HwdCalibTxDetCplDataProcess, HWD_RF_BAND_E)

/* 23 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_A, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplComp1xRtt_BandA, HwdCalib1xRttTxDetCplCompDataProcess, HWD_RF_BAND_A)
/* 24 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_B, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplComp1xRtt_BandB, HwdCalib1xRttTxDetCplCompDataProcess, HWD_RF_BAND_B)
/* 25 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_C, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplComp1xRtt_BandC, HwdCalib1xRttTxDetCplCompDataProcess, HWD_RF_BAND_C)
/* 26 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_D, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplComp1xRtt_BandD, HwdCalib1xRttTxDetCplCompDataProcess, HWD_RF_BAND_D)
/* 27 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_1XRTT_BAND_E, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplComp1xRtt_BandE, HwdCalib1xRttTxDetCplCompDataProcess, HWD_RF_BAND_E)

        /** Tx DET couplerloss frequency and temperature compensation cal data*/
/* 28 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_A, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplCompEvdo_BandA, HwdCalibEvdoTxDetCplCompDataProcess, HWD_RF_BAND_A)
/* 29 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_B, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplCompEvdo_BandB, HwdCalibEvdoTxDetCplCompDataProcess, HWD_RF_BAND_B)
/* 30 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_C, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplCompEvdo_BandC, HwdCalibEvdoTxDetCplCompDataProcess, HWD_RF_BAND_C)
/* 31 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_D, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplCompEvdo_BandD, HwdCalibEvdoTxDetCplCompDataProcess, HWD_RF_BAND_D)
/* 32 */DBM_RF_CAL_ITEM(HWD_TX_DET_COUPLER_LOSS_COMP_EVDO_BAND_E, sizeof(HwdTxDetCouplerLossCompTblT), hwdDefaultTxDetCplCompEvdo_BandE, HwdCalibEvdoTxDetCplCompDataProcess, HWD_RF_BAND_E)

        /** Rx pathloss compensation cal data in high power mode*/
/* 33 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_HPM_BAND_A, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossHpm_BandA, HwdCalibMainRxPathLossHpmDataProcess, HWD_RF_BAND_A)
/* 34 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_HPM_BAND_B, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossHpm_BandB, HwdCalibMainRxPathLossHpmDataProcess, HWD_RF_BAND_B)
/* 35 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_HPM_BAND_C, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossHpm_BandC, HwdCalibMainRxPathLossHpmDataProcess, HWD_RF_BAND_C)
/* 36 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_HPM_BAND_D, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossHpm_BandD, HwdCalibMainRxPathLossHpmDataProcess, HWD_RF_BAND_D)
/* 37 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_HPM_BAND_E, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossHpm_BandE, HwdCalibMainRxPathLossHpmDataProcess, HWD_RF_BAND_E)

/* 38 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_HPM_BAND_A, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossHpm_BandA, HwdCalibDivRxPathLossHpmDataProcess, HWD_RF_BAND_A)
/* 39 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_HPM_BAND_B, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossHpm_BandB, HwdCalibDivRxPathLossHpmDataProcess, HWD_RF_BAND_B)
/* 40 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_HPM_BAND_C, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossHpm_BandC, HwdCalibDivRxPathLossHpmDataProcess, HWD_RF_BAND_C)
/* 41 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_HPM_BAND_D, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossHpm_BandD, HwdCalibDivRxPathLossHpmDataProcess, HWD_RF_BAND_D)
/* 42 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_HPM_BAND_E, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossHpm_BandE, HwdCalibDivRxPathLossHpmDataProcess, HWD_RF_BAND_E)

/* 43 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_HPM_BAND_A, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossHpm_BandA, HwdCalibShdrRxPathLossHpmDataProcess, HWD_RF_BAND_A)
/* 44 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_HPM_BAND_B, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossHpm_BandB, HwdCalibShdrRxPathLossHpmDataProcess, HWD_RF_BAND_B)
/* 45 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_HPM_BAND_C, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossHpm_BandC, HwdCalibShdrRxPathLossHpmDataProcess, HWD_RF_BAND_C)
/* 46 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_HPM_BAND_D, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossHpm_BandD, HwdCalibShdrRxPathLossHpmDataProcess, HWD_RF_BAND_D)
/* 47 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_HPM_BAND_E, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossHpm_BandE, HwdCalibShdrRxPathLossHpmDataProcess, HWD_RF_BAND_E)

/* 48 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_LPM_BAND_A, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossLpm_BandA, HwdCalibMainRxPathLossLpmDataProcess, HWD_RF_BAND_A)
/* 49 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_LPM_BAND_B, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossLpm_BandB, HwdCalibMainRxPathLossLpmDataProcess, HWD_RF_BAND_B)
/* 50 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_LPM_BAND_C, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossLpm_BandC, HwdCalibMainRxPathLossLpmDataProcess, HWD_RF_BAND_C)
/* 51 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_LPM_BAND_D, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossLpm_BandD, HwdCalibMainRxPathLossLpmDataProcess, HWD_RF_BAND_D)
/* 52 */DBM_RF_CAL_ITEM(HWD_MAIN_RX_PATH_LOSS_LPM_BAND_E, sizeof(HwdRxPathLossCompTblT), hwdDefaultMainRxPathLossLpm_BandE, HwdCalibMainRxPathLossLpmDataProcess, HWD_RF_BAND_E)

/* 53 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_LPM_BAND_A, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossLpm_BandA, HwdCalibDivRxPathLossLpmDataProcess, HWD_RF_BAND_A)
/* 54 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_LPM_BAND_B, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossLpm_BandB, HwdCalibDivRxPathLossLpmDataProcess, HWD_RF_BAND_B)
/* 55 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_LPM_BAND_C, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossLpm_BandC, HwdCalibDivRxPathLossLpmDataProcess, HWD_RF_BAND_C)
/* 56 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_LPM_BAND_D, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossLpm_BandD, HwdCalibDivRxPathLossLpmDataProcess, HWD_RF_BAND_D)
/* 57 */DBM_RF_CAL_ITEM(HWD_DIV_RX_PATH_LOSS_LPM_BAND_E, sizeof(HwdRxPathLossCompTblT), hwdDefaultDivRxPathLossLpm_BandE, HwdCalibDivRxPathLossLpmDataProcess, HWD_RF_BAND_E)

/* 58 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_LPM_BAND_A, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossLpm_BandA, HwdCalibShdrRxPathLossLpmDataProcess, HWD_RF_BAND_A)
/* 59 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_LPM_BAND_B, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossLpm_BandB, HwdCalibShdrRxPathLossLpmDataProcess, HWD_RF_BAND_B)
/* 60 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_LPM_BAND_C, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossLpm_BandC, HwdCalibShdrRxPathLossLpmDataProcess, HWD_RF_BAND_C)
/* 61 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_LPM_BAND_D, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossLpm_BandD, HwdCalibShdrRxPathLossLpmDataProcess, HWD_RF_BAND_D)
/* 62 */DBM_RF_CAL_ITEM(HWD_SHDR_RX_PATH_LOSS_LPM_BAND_E, sizeof(HwdRxPathLossCompTblT), hwdDefaultShdrRxPathLossLpm_BandE, HwdCalibShdrRxPathLossLpmDataProcess, HWD_RF_BAND_E)

/* 63 */DBM_RF_CAL_ITEM (HWD_BAND_A_GPS_CAL,                      16,  dummyCalData, HwdGetGpsCalibDBProcess, HWD_RF_BAND_A)
/* 64 */DBM_RF_CAL_ITEM (HWD_BAND_B_GPS_CAL,                      16,  dummyCalData, HwdGetGpsCalibDBProcess, HWD_RF_BAND_B)
/* 65 */DBM_RF_CAL_ITEM (HWD_BAND_C_GPS_CAL,                      16,  dummyCalData, HwdGetGpsCalibDBProcess, HWD_RF_BAND_C)
/* 66 */DBM_RF_CAL_ITEM (HWD_BAND_D_GPS_CAL,                      16,  dummyCalData, NULL, HWD_RF_BAND_UNDEFINED)
/* 67 */DBM_RF_CAL_ITEM (HWD_BAND_E_GPS_CAL,                      16,  dummyCalData, NULL, HWD_RF_BAND_UNDEFINED)

/* 68 */DBM_RF_CAL_ITEM(HWD_AGPS_GRP_DLY_BAND_A, sizeof(uint16), hwdDefaultAGpsGroupDelay_BandA, HwdCalibAGpsGrpDlyProcess, HWD_RF_BAND_A)
/* 69 */DBM_RF_CAL_ITEM(HWD_AGPS_GRP_DLY_BAND_B, sizeof(uint16), hwdDefaultAGpsGroupDelay_BandB, HwdCalibAGpsGrpDlyProcess, HWD_RF_BAND_B)
/* 70 */DBM_RF_CAL_ITEM(HWD_AGPS_GRP_DLY_BAND_C, sizeof(uint16), hwdDefaultAGpsGroupDelay_BandC, HwdCalibAGpsGrpDlyProcess, HWD_RF_BAND_C)
/* 71 */DBM_RF_CAL_ITEM(HWD_AGPS_GRP_DLY_BAND_D, sizeof(uint16), hwdDefaultAGpsGroupDelay_BandD, HwdCalibAGpsGrpDlyProcess, HWD_RF_BAND_D)
/* 72 */DBM_RF_CAL_ITEM(HWD_AGPS_GRP_DLY_BAND_E, sizeof(uint16), hwdDefaultAGpsGroupDelay_BandE, HwdCalibAGpsGrpDlyProcess, HWD_RF_BAND_E)

/* 73 */DBM_RF_CAL_ITEM(HWD_TX_PWR_BACKOFF_BAND_A, sizeof(HwdTxPwrBackOffTblT), hwdTxPwrBackOffRegion_BandA, HwdCalibTxPwrBackOffProcess, HWD_RF_BAND_A)
/* 74 */DBM_RF_CAL_ITEM(HWD_TX_PWR_BACKOFF_BAND_B, sizeof(HwdTxPwrBackOffTblT), hwdTxPwrBackOffRegion_BandB, HwdCalibTxPwrBackOffProcess, HWD_RF_BAND_B)
/* 75 */DBM_RF_CAL_ITEM(HWD_TX_PWR_BACKOFF_BAND_C, sizeof(HwdTxPwrBackOffTblT), hwdTxPwrBackOffRegion_BandC, HwdCalibTxPwrBackOffProcess, HWD_RF_BAND_C)
/* 76 */DBM_RF_CAL_ITEM(HWD_TX_PWR_BACKOFF_BAND_D, sizeof(HwdTxPwrBackOffTblT), hwdTxPwrBackOffRegion_BandD, HwdCalibTxPwrBackOffProcess, HWD_RF_BAND_D)
/* 77 */DBM_RF_CAL_ITEM(HWD_TX_PWR_BACKOFF_BAND_E, sizeof(HwdTxPwrBackOffTblT), hwdTxPwrBackOffRegion_BandE, HwdCalibTxPwrBackOffProcess, HWD_RF_BAND_E)
#endif

