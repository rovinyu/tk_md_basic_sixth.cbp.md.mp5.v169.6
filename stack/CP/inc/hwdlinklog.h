/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _HWDLINKLOG_H_
#define _HWDLINKLOG_H_

// Dbg
#define C2K_DBG_DSV_ISR              0xA000
#define C2K_DBG_APP_MODE             0xD000
#define C2K_DBG_APP_MODE_RSP         0xD001
#define C2K_DBG_SSO_CONN             0xD002
#define C2K_DBG_SSO_CONN_RSP         0xD003
#define C2K_DBG_SSO_DISCONN          0xD004
#define C2K_DBG_SSO_DISCONN_RSP      0xD005
#define C2K_DBG_FWD_CHECK            0xD006
#define C2K_DBG_APPS_RESET           0xD007

#define HWD_DBG_STATE_NOT_INITIALIZED             0xE000
#define HWD_DBG_STATE_READY                       0xE001
#define HWD_DBG_STATE_CODE_DOWNLOADING1           0xE002
#define HWD_DBG_STATE_APP_DOWNLOADING             0xE003
#define HWD_DBG_STATE_CODE_DOWNLOADING2           0xE004
#define HWD_DBG_STATE_AUDIO_INIT                  0xE005
#define HWD_DBG_APPS_SET_VOCODER                  0xE006
#define HWD_DBG_APPS_VOCODER_DNLD                 0xE007
#define HWD_DBG_VOCODER_APP_DNLD                  0xE008

// Trace
// Link trace
//#define C2K_LINK_TRACE_CON_SSO_REQ                0x1000
//#define C2K_LINK_TRACE_PARSE                      0x1001
//#define C2K_LINK_TRACE_CON_SSO_DONE               0x1002
//#define C2K_LINK_TRACE_COD_RDY                    0x1003
//#define C2K_LINK_TRACE_COD_CLOSE                  0x1004
//#define C2K_LINK_TRACE_DISCON_SSO_REQ             0x1005
//#define C2K_LINK_TRACE_DISCON_SSO_DONE            0x1006
//#define C2K_LINK_TRACE_SOCM                       0x1007
//#define C2K_LINK_TRACE_ENC_MAX_RATE               0x1008
//#define C2K_LINK_TRACE_HB_UL                      0x1009 
//#define C2K_LINK_TRACE_HB_DL                      0x100A
//#define C2K_LINK_TRACE_COD_1ST_FRAME              0x100B
//#define C2K_LINK_TRACE_DELRWM                     0x100C
//#define C2K_LINK_TRACE_LBK                        0x100D
//
//// Problem trace                     
//#define C2K_LINK_TRACE_SSO_REQ_REPEAT             0x2000
//#define C2K_LINK_TRACE_DISCON_SSO_REQ_REPEAT      0x2001
//#define C2K_LINK_TRACE_COD_UNRDY                  0x2002
//#define C2K_LINK_TRACE_UL_UNSYNC                  0x2003
//#define C2K_LINK_TRACE_SO_CONFLICT                0x2004
//
//// SAL trace                                             
//#define SAL_SPH_CALL_OPEN                         0x3000 
//#define SAL_SPH_CALL_CLOSE                        0x3001 
//#define SAL_SPH_SET_VALUE                         0x3002 
//#define SAL_SPH_SET_TTY                           0x3003
//#define SAL_SPH_FEATURE_SWITCH                    0x3004
//#define SAL_SPH_GET_ADDR                          0x3005
//#define SAL_SPH_GET_VALUE                         0x3006 
//#define SAL_SPH_SET_COD_STATE                     0x3007
//
//// IPC trace
#define IPC_TRACE_DSPV_ASYN_MAILBOX               0x4000
#define IPC_TRACE_SIGNAL_DSPV_MBOX                0x4001
#define IPC_TRACE_APP_MODE_MSG                    0x4002
#define IPC_TRACE_DSPV_APPS_MAILBOX               0x4003
//
//// CAAL trace
//#define CAAL_TRACE_APP_MODE_MSG                   0x5000
//#define CAAL_TRACE_SSO_CONNECT                    0x5001
//#define CAAL_TRACE_SSO_DISCONNECT                 0x5002
//#define CAAL_TRACE_DL_DATA                        0x5003
//#define CAAL_TRACE_SOCM                           0x5004
//#define CAAL_TRACE_ENC_MAX_RATE                   0x5005
//#define CAAL_TRACE_CHAN_QLTY                      0x5006
//#define CAAL_TRACE_CONN_SSO_DONE                  0x5007
//#define CAAL_TRACE_DISCONN_SSO_DONE               0x5008

// Error
// Link error
#define C2K_LINK_SOCM_VAL_ERR                     0xA000
#define C2K_LINK_ENC_MAX_RATE_VAL_ERR             0xA001
#define C2K_LINK_SO_ERR                           0xA002
#define C2K_LINK_UL_RATE_ERR                      0xA003
#define C2K_LINK_DL_RATE_ERR                      0xA004
#define C2K_LINK_DL_HB_SIZE_ERR                   0xA005
#define C2K_LINK_UL_PKT_MEM_ALLOC_ERR             0xA006
#define C2K_LINK_LBK_UL_PKT_MEM_ALLOC_ERR         0xA007

#endif
