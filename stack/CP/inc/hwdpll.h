/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWD_PLL_H_
#define _HWD_PLL_H_
/*****************************************************************************
*
* FILE NAME   :     HWPLL.h
*
* DESCRIPTION :     PLL lock detect related interfaces.
*
* HISTORY     :     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysapi.h"
#include "exeapi.h"
#include "hwdapi.h"
#include "hwdrfapi.h"

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/

/* PLL function pointer table structure */
typedef struct
{
   void   (*RfPllChannelSet[HWD_RF_MAX_NUM_OF_RX])(HwdRfBandT PllBand, uint32 Channel, uint8 InterfaceType);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   void   (*TxPllChannelSet[HWD_RF_MAX_NUM_OF_TX])(uint8 InterfaceType);
#else
   void   (*TxPllChannelSet[HWD_RF_MAX_NUM_OF_TX])(HwdRfBandT PllBand, uint32 Channel, uint8 InterfaceType);
#endif
   void   (*RfTstRfRegWrite)(uint8 Device, uint8 Address, uint32 Data);
   uint32 (*RfTstRfRegRead)(uint8 Device, uint8 Address, uint8 Hardware);
   void   (*ResetRfConfig)(SysAirInterfaceT Mode);
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   void   (*RfAfcCapIdSet)(uint8 CapIdValue);
   void   (*RfAfcDacSet)(uint16 dacValue);
   int32  (*RfAfcPllSet)(SysAirInterfaceT Mode, int32 FreqAdj);
   void   (*RfAfcParmGet)(uint8* CapIdvalue, uint16* AfcDacValue);
   uint32 (*RxChannelToFrequency) (uint32 channel,SysCdmaBandT bandClass);
   uint32 (*TxChannelToFrequency) (uint32 channel,SysCdmaBandT bandClass);
#endif
} PllFuncPtrTblT;

/* enum type for the PLL trace MON_CP_HWD_RF_PLL_DETAIL_TRACE_ID */
typedef enum
{
   HWD_RF_FCI_7711 = 0,
   HWD_RF_FCI_7760,
   HWD_RF_GRF_6424,
   HWD_RF_GRF_6404,
   HWD_RF_GRF_6407,
   HWD_RF_GRF_6414,
   HWD_RF_FCI_7710,
   HWD_RF_GRF_6402,
   HWD_RF_FCI_7730,
   HWD_RF_FCI_7785,
   HWD_RF_GRF_6411,
   HWD_RF_FCI_7780,
   HWD_RF_GRF_6413,
   HWD_RF_FCI_7790,
   HWD_RF_FCI_7763
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   ,
   HWD_RF_ORIONC,
   HWD_RF_ORION_PLUS
#endif
}PllDeviceTraceT;

typedef struct
{
   void *MainRx;
   void *DivRx;
   void *MainTx;
} PllRfConfigT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef struct
{
   SysAirInterfaceT Mode;
   HwdRfBandT       Band;
   SysCdmaBandT     CdmaBand;
   uint32           Channel;
   uint32           FreqKhz;
   uint32           FreqLoKhz;
   int32            FreqAdjHz;
   int32            FreqAdjPpb;

   /* Calculated values */
   uint32           nIntcs;
   uint32           nFracs;
   uint32           codeType;
} PllT;
#endif
/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/

/* Define PLL trace macros to identify antenna path trace info */
#define MAIN_PATH_RX_PLL       0
#define DIVERSITY_PATH_RX_PLL  1
#define SEC_PATH_RX_PLL        2
#define MAIN_PATH_TX_PLL       3
#define AUX_PATH_TX_PLL        4
#define GPS_PATH_RX_PLL        5


#define RESET_FREQ_ADJUSTEMENT          0x80000000l
#define HWD_ALT_AFC_ADJ_UPPER_LIMIT    ((int32)1000 << 4)
#define HWD_ALT_AFC_ADJ_LOWER_LIMIT    ((int32)700 << 4)
#define HWD_ALT_AFC_SPECIAL_OFFSET     ((int32)HWD_ALT_AFC_ADJ_UPPER_LIMIT + (200 << 4))
#define HWD_ALT_AFC_SLEW_PDM_ADJ       ((int32)2 << 4)
#define HWD_ALT_AFC_SLEW_TIMER         (50)

#define MAX_FREQ_ADJUSTEMENT           ((int32)600 << 4)

#define HWD_AFC_TIMER_ID       1

#define HWD_AFC_FIXED_INIT_DAC         (4096)
#define HWD_AFC_FIXED_SLOP             (1024)
/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern bool   HwdPllAltAfcUpdatePdm[];
#endif
extern uint16 AltAfcInitialOffset[];
extern bool   AltAfcAlgorithm[];
extern int32  TotalFreqAdj[];
extern int32  AfcMaxFreqAdjustement;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern PllFuncPtrTblT  PllFuncPtrTbl;
extern PllRfConfigT*   HwdRfConfiguration;
extern PllT*           HwdPllChannelData;
#ifdef MTK_DEV_HW_SIM_RF
extern PllFuncPtrTblT  PllFuncPtrTblHwSim;
extern PllRfConfigT*   HwdRfConfigurationHwSim;
extern PllT*           HwdPllChannelDataHwSim;
#endif
#else
extern PllRfConfigT  HwdRfConfiguration[SYS_MODE_MAX];
#endif

#ifdef SYS_OPTION_USE_DCXO
extern uint16 AdjCodeStored[SYS_MODE_MAX];
#endif

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void HwdPllLockDetectRfTuned(bool Enable);
extern void HwdPllLockTxEnable(bool Enable);
#endif
extern void HwdPllInit( void );
extern void HwdRfTstRfRegWrite(uint8 Device, uint8 Address, uint32 Data);
extern uint32 HwdRfTstRfRegRead(uint8 Device, uint8 Address, uint8 Local);
extern void HwdMainRfPllBandClear(uint8 InterfaceType);
extern void HwdDivRfPllBandClear(uint8 InterfaceType);
extern int32 HwdRfPllFreqAdjust(SysAirInterfaceT Mode, int32 FreqAdj);
extern void HwdRfPllFreqRestore(uint8 Interface);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void HwdRfPllFreqCodeSet(uint16 AdjCode, SysAirInterfaceT Interface);
#else
extern uint16 HwdRfPllFreqCodeSet(int16 FreqAdj, SysAirInterfaceT Interface);
#endif
extern SysCdmaBandT HwdPllGetCurrentCdmaBandClass(uint8 InterfaceType);
extern SysCdmaBandT HwdPllGetDivCurrentCdmaBandClass(uint8 InterfaceType);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern uint16 HwdSetAlternativeAfcPdmMsg(SysAirInterfaceT Interface, bool Enable, uint16 PdmVal);
#endif
extern uint16 HwdFreezeAfcPdm4ShdrMsg(SysAirInterfaceT Mode, bool Enable, uint16 PdmVal);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void HwdUpdateAltAfcPdmMsg(SysAirInterfaceT Interface);
#endif
extern int32 HwdGetTotalFreqAdj(SysAirInterfaceT Interface);
extern uint32 HwdGetCurrentRxPllFreq(SysAirInterfaceT Interface);
extern void HwdAfcStartTimer(void);
extern void HwdAfcStopTimer(void);
extern void HwdUpdateAltAfcOffset(SysAirInterfaceT Interface);
extern uint8 HwdRfPllIsDCXOUsed(void);
extern void HwdCalSetDcxoAfcLimits(SysAirInterfaceT Interface, bool Valid);
extern bool HwdCalUpdateAfcTable(SysAirInterfaceT Interface);
extern void HwdRfResetRfConfig(SysAirInterfaceT Mode);
#if defined (MTK_DEV_OPTIMIZE_EVL1) && !defined(MTK_PLT_ON_PC)
extern HwdRfBandT HwdPllGetCurrentBand(uint8 InterfaceType);
#else
extern HwdRfBandT HwdPllGetCurrentBand(uint8 InterfaceType);
#endif
extern void HwdTestSetRfPllBandClass(uint8 InterfaceType, uint16 BandClass);
#ifdef MTK_DEV_ENGINEER_MODE
extern void HwdTestSetRfPllChannel(uint8 InterfaceType, uint16 Channel);
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern uint32 HwdPllGetCurrentChannel(uint8 InterfaceType);
extern int32 HwdAfcFoeHzToPpb(uint32 pllFreqKHz, int32 foeHz);
extern int32 HwdAfcFoePpbToHz(uint32 pllFreqKHz, int32 foePpb);
extern PllT* HwdPllGetRfPllChannelData(void);
extern void MainTxPllChannelSet(SysCdmaBandT   PllBand, uint32 Channel, uint8 Mode);
#endif
/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_jluo_href22084\1 2013-04-02 09:25:43 GMT jluo
** href#22084,update the driver**/
/**Log information: \main\Trophy\1 2013-04-03 02:35:57 GMT czhang
** href#22084**/
