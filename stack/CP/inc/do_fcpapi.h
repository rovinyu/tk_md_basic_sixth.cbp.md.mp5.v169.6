/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/

/*****************************************************************************
*
* FILE NAME   : do_fcpapi.h
*
* DESCRIPTION : API definition for FCP (Forward Channnel Processing) Task.
*
* HISTORY     :
*****************************************************************************/
#ifndef _DO_FCPAPI_H_
#define _DO_FCPAPI_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "do_msgdefs.h"
#include "do_rupapi.h"
#include "cpbuf.h"
#include "do_slcapi.h"
#include "do_hwdrxpdefs.h"
#include "hlpapi.h"
#ifdef MTK_PLT_ON_PC_UT
#include "do_rcpapi.h"
#endif

#ifdef EXE_UTE_TST_TASK	 /* test task is ON...*/
  #ifndef DRC_TEST
   #define DRC_TEST
 #endif
#endif


#ifdef SYS_OPTION_DRC_ASIC_TEST /* For ASIC TEST...*/
 #ifndef DRC_TEST
   #define DRC_TEST             1
 #endif
#endif

#ifdef SYS_OPTION_DRC_HW_TEST
 #ifndef DRC_TEST
   #define DRC_TEST
 #endif
#endif

/*
#if  (USE_TST_TASK==TRUE)
 #ifndef DRC_TEST
   #define DRC_TEST
 #endif
#endif
*/
/* Just turn on the DRC_TEST now...*/
/*   #define  DRC_TEST                  */
/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define MAX_TRANS_ATI_NUMBER        10
#define MAX_NUM_AppStream           4

/*----------------------------------------------------------------------------
 Mailbox IDs
----------------------------------------------------------------------------*/
#define FCP_TASK_MAILBOX        EXE_MAILBOX_1_ID
#define FCP_DATA_MAILBOX        EXE_MAILBOX_2_ID

#define FCP_TASK_MAILBOX_EVENT  EXE_MESSAGE_MBOX_1   /* EXE_MAILBOX_1 */
#define FCP_DATA_MAILBOX_EVENT  EXE_MESSAGE_MBOX_2   /* EXE_MAILBOX_2 */

/* DRC Register type...*/
#define DRC_REG_OFFSET         ( HWD_DRC_BASE + 0x0500 )

#define DRC_LEN                (uint32)( HWD_DRC_LEN         -  DRC_REG_OFFSET)
#define DRC_IIRPOLE			       (uint32)( HWD_DRC_IIRPOLE     -  DRC_REG_OFFSET)
#define DRC_ERRIIRPOLE         (uint32)( HWD_DRC_ERRIIRPOLE  -  DRC_REG_OFFSET)
#define DRC_SLMSMU             (uint32)( HWD_DRC_SLMSMU      -  DRC_REG_OFFSET)
#define DRC_FIXEDRXVAL         (uint32)( HWD_DRC_FIXEDRXVAL  -  DRC_REG_OFFSET)
#define DRC_FIXEDTXVAL         (uint32)( HWD_DRC_FIXEDTXVAL  -  DRC_REG_OFFSET)
#define DRC_LVCROSSLEN         (uint32)( HWD_DRC_LVCROSSLEN  -  DRC_REG_OFFSET)
#define DRC_CONTROL            (uint32)( HWD_DRC_CONTRL      -  DRC_REG_OFFSET)
#define DRC_THRBYPASS_1        (uint32)( HWD_DRC_THRBYPASS1  -  DRC_REG_OFFSET)
#define DRC_THRBYPASS_2        (uint32)( HWD_DRC_THRBYPASS2  -  DRC_REG_OFFSET)
#define DRC_LVCROSS            (uint32)( HWD_DRC_LVCROSS     -  DRC_REG_OFFSET)
#define DRC_GLOBALADJ          (uint32)( HWD_DRC_GLOBALADJ   -  DRC_REG_OFFSET)
#define DRC_SLMSERR            (uint32)( HWD_DRC_SLMSERR     -  DRC_REG_OFFSET)
#define DRC_ENDTIME            (uint32)( HWD_DRC_ENDTIME     -  DRC_REG_OFFSET)
#define DRC_SLMSSTEP           (uint32)( HWD_DRC_SLMSSTEP    -  DRC_REG_OFFSET)
#define DRC_C2ISHORT           (uint32)( HWD_DRC_C2ISHORT    -  DRC_REG_OFFSET)
#define DRC_C2ILONG            (uint32)( HWD_DRC_C2ILONG     -  DRC_REG_OFFSET)
#define DRC_SLMSCOEFF          (uint32)( HWD_DRC_SLMSCOEFF_M_START -  DRC_REG_OFFSET)
#define DRC_OFFSET             (uint32)( HWD_DRC_OFFSET_M_START    -  DRC_REG_OFFSET)
#define DRC_C2ITHR             (uint32)( HWD_DRC_C2ITHR_M_START    -  DRC_REG_OFFSET)
#define DRC_THRPUT             (uint32)( HWD_DRC_THRPUT_M_START    -  DRC_REG_OFFSET)
#define DRC_THRAWGN            (uint32)( HWD_DRC_THRAWGN_M_START   -  DRC_REG_OFFSET)
#define DRC_C2IVAL             (uint32)( HWD_DRC_C2IVAL_M_START    -  DRC_REG_OFFSET)
#define DRC_C2IMAX             (uint32)( HWD_DRC_C2IMAX            -  DRC_REG_OFFSET)
#define DRC_C2IMIN             (uint32)( HWD_DRC_C2IMIN            -  DRC_REG_OFFSET)
#define Load_Slmscoeff         (uint32)( HWD_Load_slmscoeff        -  DRC_REG_OFFSET)
#define DRC_VALUE              (uint32)( HWD_DRC_VALUE             -  DRC_REG_OFFSET)
#define Half_slot_ini_val      (uint32)( HWD_DRC_HALF_SLOT_CNT_INIT -  DRC_REG_OFFSET)
#define Drc4mcd                (uint32)( HWD_DRC_DRC4MCD -  DRC_REG_OFFSET)

#define DRC_FN_DELAY       	   (uint32)( HWD_DRC_FN_DELAY       -  DRC_REG_OFFSET)
#define DRC_PREDICTED_C2IDB	   (uint32)( HWD_DRC_PREDICTED_C2IDB-  DRC_REG_OFFSET)
#define DRC_HALF_SLOT_CNT  	   (uint32)( HWD_DRC_HALF_SLOT_CNT  -  DRC_REG_OFFSET)
#define DRC_CELLSW_STATUS  	   (uint32)( HWD_DRC_CELLSW_STATUS  -  DRC_REG_OFFSET)
#define DRC_FN_DELAY_CNT   	   (uint32)( HWD_DRC_FN_DELAY_CNT   -  DRC_REG_OFFSET)
#define DRC_C2I            	   (uint32)( HWD_DRC_C2I            -  DRC_REG_OFFSET)
#define DRC_DEBUG          	   (uint32)( HWD_DRC_DEBUG          -  DRC_REG_OFFSET)
#define DRC_TABLE_ASK		   (uint32)( HWD_DRC_TABLE_ASK		-  DRC_REG_OFFSET)
#define DRC_TABLE_RES		   (uint32)( HWD_DRC_TABLE_RES		-  DRC_REG_OFFSET)
#define DRC_IIR_RST       	   (uint32)( HWD_DRC_IIR_RST       	-  DRC_REG_OFFSET)
#define DRC_SUPER_EN      	   (uint32)( HWD_DRC_SUPER_EN      	-  DRC_REG_OFFSET)
#define DRC_ZERO_CNT_RST  	   (uint32)( HWD_DRC_ZERO_CNT_RST  	-  DRC_REG_OFFSET)
#define DRC_NONZERO_CNT_RST	   (uint32)( HWD_DRC_NONZERO_CNT_RST-  DRC_REG_OFFSET)
#define DRC_ZERO_CNT      	   (uint32)( HWD_DRC_ZERO_CNT      	-  DRC_REG_OFFSET)
#define DRC_NONZERO_CNT   	   (uint32)( HWD_DRC_NONZERO_CNT   	-  DRC_REG_OFFSET)
#define DRC_TENTATIVE     	   (uint32)( HWD_DRC_TENTATIVE     	-  DRC_REG_OFFSET)
#define DRC_ZERO_THR      	   (uint32)( HWD_DRC_ZERO_THR      	-  DRC_REG_OFFSET)
#define DRC_NONZERO_THR   	   (uint32)( HWD_DRC_NONZERO_THR   	-  DRC_REG_OFFSET)
#define DRC_LONGTERM_INI  	   (uint32)( HWD_DRC_LONGTERM_INI  	-  DRC_REG_OFFSET)
#define DRC_SHORTTERM_INI 	   (uint32)( HWD_DRC_SHORTTERM_INI 	-  DRC_REG_OFFSET)
#define DRC_LEVELCROSS_INI	   (uint32)( HWD_DRC_LEVELCROSS_INI	-  DRC_REG_OFFSET)
#define DRC_SFT_RSTN      	   (uint32)( HWD_DRC_SFT_RSTN      	-  DRC_REG_OFFSET)




#define DrcRegisterTypeT       uint16

/*----------------------------------------------------------------------------
     Command Message IDs, for FCP task, for FCP_CMD_MAILBOX, EXE_MAILBOX_1_ID
     The message IDs for components shall also be put in here.
	 Satya: Add all the PKT_SIM related messages into the area that is defined
	 between #ifdef/#endif..Contact me, if you need any clarifications!.
----------------------------------------------------------------------------*/
#include "do_fcpmsg.h"
/*----------------------------------------------------------------------------
     Data Message IDs, for FCP_DATA_MAILBOX
----------------------------------------------------------------------------*/
typedef enum
{
  FTM_PCP_PACKET_MSG  = FCP_DATA_MSGID_START

} FcpDataMsgT;

/*----------------------------------------------------------------------------
     define signals used by FCP task
----------------------------------------------------------------------------*/

#define FCP_TASK_SIG                  EXE_SIGNAL_2

/*----------------------------------------------------------------------------
     Message Formats structure
----------------------------------------------------------------------------*/
typedef enum {
  CCM_INCONFIG,
  CCM_INUSE,
  CCM_MAX_INSTANCE
} CcmInstanceT;

typedef PACKED_PREFIX struct {
  uint32  TKeepAliveTransTime;
} PACKED_POSTFIX CcmSmpKeepAliveCfgT;

typedef PACKED_PREFIX struct{
  uint8 ATIType;
  uint32 ATI;
  uint32 SystemTime;
  bool Use;
} PACKED_POSTFIX ATITypeT;

typedef PACKED_PREFIX struct
{
  ATITypeT *RcvATIList;
} PACKED_POSTFIX CcmAmpATIListMsgT;

typedef PACKED_PREFIX struct
{
  uint8  SubType;
  uint8  CCShortPacketsMACIndex;
} PACKED_POSTFIX CcmAttributeT;

typedef PACKED_PREFIX struct
{
  uint8         CcmCfgReqTranId;
  CcmAttributeT CcmAttribute[2];
} PACKED_POSTFIX CcmInfoT;

typedef PACKED_PREFIX struct
{
  int16 sectorDRCCover;         /* point to sector used in Rx Fixed Rate Mode */
} PACKED_POSTFIX  CsmRxFixedRateReadyMsgT;

/*Forward traffic parameters supported by TrafficChannelAssignment message*/
typedef PACKED_PREFIX struct
{
  uint8      MsgSeq;
  uint8      ChanInc;
  ChanRecGT  Chan;
  uint8      DRCLength;                                /*in unit of slot*/
  int8       DRCChannelGainBase;                       /*in unit of 0.5db, valid range (-9dB,+6dB) */
  int8       ACKChannelGain;                           /* 0.5db, valid range (-3dB,+6dB) */
  uint8      NumPilots;
  uint8      SofterHandoff[SYS_MAX_ACTIVE_LIST_PILOTS];/*1 bit*/
  uint8      MACIndex[SYS_MAX_ACTIVE_LIST_PILOTS];     /* 7 bits, computed MACIndex by LSB&MSB fields*/
  uint8      DRCCover[SYS_MAX_ACTIVE_LIST_PILOTS];     /* 3 bits*/
  uint8      RAChannelGainInc;                         /*1 bits*/
  uint8      RAChannelGain[SYS_MAX_ACTIVE_LIST_PILOTS];/* 2 bits*/
  uint8      DSCChanInc;
  int8       DSCChannelGainBase;                       /*0.5dB*/
  uint8      DSC[SYS_MAX_ACTIVE_LIST_PILOTS];          /* 3 bits each pilot of SofterHandOff=0 */
} PACKED_POSTFIX FTCMACParmsGT;

typedef struct {
  uint32   TimerId;
}PafTimerExpiredMsgT;


typedef enum {
	ACCESSSTREAM=1,
	SERVICESTREAM
}StreamType;


typedef struct {
  bool bXOnOff;
  StreamType StreamID;
} PafHlpFlowCtrlMsgT;

typedef struct {
  StreamConfigAttribT StreamConfigData;
} FcpRcpStreamConfigMsgT;

typedef enum
{
  PAF_Default = 0x0,
  PAF_MultiFlow
}
PafSubTypeT;



typedef struct {
  uint8 Active;
  uint8 lenSeq;
  uint8 lenRLPID;
  uint8 RLPID;
} FlowIdentificationFwdAttribT;

typedef struct {
  uint16 AbortTimer;
  uint16 FlushTimer;
} FlowTimersFwdAttribT;

typedef struct {
  uint8 MaxNumRLPFlowsFwd;
  uint8 MaxActivatedRLPFlowsFwd;
} FwdMaxRLPFlowsAttribT;

#ifdef CBP7_EHRPD

typedef struct {
     HlpProtocolIdentifierT  ProtcolID;
     FwdRohcProtocolParmsT   RohcParms;
} FwdFlowProtocolParametersAttribT;

typedef struct {
     HlpProtocolIdentifierT  ProtcolID;
     FwdRohcProtocolParmsT   RohcParms;
} FwdRouteProtocolParametersAttribT;

typedef struct {
     HlpProtocolIdentifierT  ProtcolID;
     RevRohcProtocolParmsT   RohcParms;
} RevFlowProtocolParametersAttribT;

typedef struct {
     HlpProtocolIdentifierT  ProtcolID;
     RevRohcProtocolParmsT   RohcParms;
} RevRouteProtocolParametersAttribT;
#endif

typedef struct {
  AppSubTypeT pafSubType;

  /*Default Packet Application Attributes*/
  uint8 RANHandoff;

  /*Multi-Flow Packet Application Attributes*/
  uint8 MaxAbortTimer;
  bool FwdFlowNakEnabled [MAX_RLP_FLOW_SUPPORTED];
  uint8 FwdFlowHighLayerProtocol[MAX_RLP_FLOW_SUPPORTED];
  FlowIdentificationFwdAttribT attribFwdFlowIdentification[MAX_RLP_FLOW_SUPPORTED];
  FlowTimersFwdAttribT attribFwdFlowTimer[MAX_RLP_FLOW_SUPPORTED];
  FwdMaxRLPFlowsAttribT attribFwdmaxRLPFlows;

#ifdef CBP7_EHRPD
   /*Enhanced Multi-Flow Packet Application attributes */
   bool             PPPFreeAuthenticationSupport;
   bool            TwoRouteSupported;
   bool            ATNakDelaySupported;
   uint8    attribFwdFlowProtocolPDU[MAX_RLP_FLOW_SUPPORTED];
   uint8    attribFwdFlowDataUnit[MAX_RLP_FLOW_SUPPORTED];
   uint8   attribFwdRouteProtocolPDU[MAX_RLP_FLOW_SUPPORTED];
   bool     attribFwdFlowSimultaneousDeliveryOnBothRoutes[MAX_RLP_FLOW_SUPPORTED];
   bool     attribFwdFlowOutOfOrderDeliveryToRouteProtcol[MAX_RLP_FLOW_SUPPORTED];
   uint8     attribFwdFlowNakDelayTime[MAX_RLP_FLOW_SUPPORTED];
   FwdFlowProtocolParametersAttribT attribFwdFlowProtocolParameters[MAX_RLP_FLOW_SUPPORTED];
   FwdRouteProtocolParametersAttribT attribFwdRouteProtocolParameters[MAX_RLP_FLOW_SUPPORTED];
#endif
}PafConfigDataT;

typedef struct {
  PafConfigDataT ConfigAttribs;
}PafRcpSessionConfigMsgT;

#if defined (MTK_PLT_ON_PC)
  typedef struct {
    DatapktlistGT pktList;
  } PafTestRtmDataIndMsgT;
#endif

#ifdef PKT_TEST
  typedef PACKED_PREFIX struct {
    uint8 nAppType;
    uint16 PktLen;
    CpBufferT* CpBufPtr;
    uint16 PktStartOffset;
  } PACKED_POSTFIX  PafOttsFwdDataIndMsgT;
#endif
typedef PACKED_PREFIX struct
{
  uint8 TCAMsgSeq;
} PACKED_POSTFIX FtapRupTrafficChanAssignMsgT;

typedef PACKED_PREFIX struct
{
  uint8 AlmpST;
} PACKED_POSTFIX FtapAlmpStateupdateMsgT;

typedef PACKED_PREFIX struct
{
  uint8 Protocol;
  uint16 SubApp;
} PACKED_POSTFIX FtaprFtapfInitMsgT;

#ifdef FCP_PACKET_PERF_STAT
/*  Packet performance measurement */
typedef PACKED_PREFIX struct
{
  uint32    TotalFwdPhySlot;      /* Count how many slots have been used.*/
  uint32    TotalBits;            /* in unit of 128 bit*/
  uint32    BadPackets;           /* missing packet on AT side*/
  uint32    TotalTxPacket;        /* Number of packet sent by AN*/
  uint16    TotalCCPacket;
  uint16    FwdPER;               /* BadPackets/TotalTxPacket*/
  uint16    AN_ThrPut;           /* TotalBits/TotalFwdPhySlot*/
  uint16    AT_ThrPut;           /* TotalBits/time*/
} PACKED_POSTFIX FcpPacketPerfStatT;

extern FcpPacketPerfStatT FcpPacketPerfStat;
extern FcpPacketPerfStatT FtapPacketPerfStat;

#if defined (MTK_DEV_OPTIMIZE_EVL2)
/* Periodic packet performance statistics for debug */
typedef PACKED_PREFIX struct
{
    uint32    PeriodStartSystimeFrame;
    uint32    PeriodEndSystimeFrame;
    uint32    CCValidPackets;            /* Number of valid packets received on Control Channel */
    uint32    CCBadPackets;              /* Number of CRC error packets on Control Channel*/
    uint32    CCTotalPackets;            /* Total number of packets on Control Channel*/
    uint32    FTCValidPackets;           /* Number of valid packets received on Forward Traffic Channel */
    uint32    FTCBadPackets;             /* Number of CRC error packets on Forward Traffic Channel */
    uint32    FTCTotalPackets;           /* Total number of packets on Forward Traffic Channel */
    uint32    TotalPackets;              /* Total number of packets on AT side */
} PACKED_POSTFIX FcpPeriodicPacketPerfStatT;

extern FcpPeriodicPacketPerfStatT FcpPeriodicPacketPerfStat;
#endif

extern uint8 FtapParamAssignCmpSent;
extern uint32 FcpPerfStartTimeInSlot;
extern void FcpPerfDataReset(uint32 CurTimeInSlot);

extern void FcpDmaMdmRxDoneLisr (void);

extern uint32  TotalSingUserPacketBytes;
extern uint32  TotalMultUserPacketBytes;
extern BOOL    StaticRfState;
extern uint32  FingerC2IAcc;



#endif

typedef struct {
  DatapktlistGT pktList;
} FtapTestRtmDataIndMsgT;

typedef struct FcpFwdPktMsg
{
  uint32     RxDmaStatus;
  uint8      bMacValid[4];
  uint8      MacId[4];
  uint16     MacPacketTimestamp[4];
  uint16     Drc_C2i[4];
  uint32     RxDmaPakcetRec[4];
  CpBufferT* RxDmaBufPtr[4];
  uint8      PrevFmpMacId[4];
  uint8      CurrFmpMacId[4];
  BOOL       FmpMacIdChange[4];
  uint16     PacketC2I;

}FcpFwdPktMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT      RspInfo;
} PACKED_POSTFIX  PafMfpaRlpStatPeekMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT      RspInfo;
} PACKED_POSTFIX  PafDpaRlpStatPeekMsgT;

typedef struct {
  ExeTimerT TimerCB;
} CcmSupTimerT;


#ifdef MTK_PLT_ON_PC_UT
typedef struct {
    ATITypeT RcvATIList[MAX_TRANS_ATI_NUMBER];
} UtCcmAmpATIListMsgT;

typedef struct {
    uint8    CCShortPacketsMACIndex;
    RcpModeT UtRcpMode;
} UtFcpCcmCommitMsgT;

typedef struct {
    CpBufferT   *bufPtr;
    uint16      offset;
    uint16      dataLen;
} FcpHlpRevDataReqMsgT;

typedef struct {
    uint8                   reqSeq;
    uint8                   ucRsv;
    uint16                  rspAttribDataLen;
    uint8                   AttribId;
    uint8                   aucRsv[3];
} UtFcpFtmSetAttribMsgT;

typedef struct {
    uint8       AttribId;
    uint8       ucRsv[3];
} UtFtmGetAttribMsgT;

typedef struct {
    uint8       AttribId;    
    uint8       usRsv;
    uint16      usLen;
    uint8       *pucData;
} UtFtmGetAttribRspMsgT;


#endif /* MTK_PLT_ON_PC_UT */

/*----------------------------------------------------------------------------
 Global Variables
----------------------------------------------------------------------------*/
typedef enum
{
  FAST_2_ch = 0,
  FAST_1_ch,
  SLOW_ch,
  Stationary_ch,
  AWGN_ch,
  NO_Decision_ch,
  Non_Force_chTyp  /* For test mode*/

}DRCChanTypeT;

typedef enum {
   NO_DECISION,
   RLP_ACK,
   RLP_NAK
} RlpAckNakStatusT;


extern RlpAckNakStatusT RlpAckNakStatus;

extern uint8 FcpPktRecMacId;
extern DRCChanTypeT DRCChannelType;
extern uint8 CurCcMacId;
extern uint8  ChannelSpeed;
/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
extern void  PreambleStatusLog( void );
extern void  DRCRegWrite(DrcRegisterTypeT  RegType, int32  RegValue);
extern void  DRCFixedTxRateDisable(void);
extern void  DRCTxValueMonitor(void);
extern void  DRCTableRegInit(void);
extern void  DRCParameterReinstate(void);

extern void  DRCTestSlotIsr(void);
extern void  FtmFwdTrafficValidFailure(void);
extern void FcpSendPacketRecRawSpy(void);

extern bool GetFtapLoopBackMode(void);
extern bool GetFtapFixedAckMode(void);
extern uint8 GetFixDrcMode(void);

#ifdef DRC_TEST
extern void  DRCTestProc(uint16  SlotCount);
extern void  DRCTestRegInit(void);
#endif


extern uint16 CCCycleStartTime;
extern bool  FirstCCMPktFound;
extern void  FcpGetFwdDataStats(uint32* pRxTotalBytes, uint32* pRxNewDataPkts, uint32* pCurFrameCnt);
extern void  FcpHwTestHwTrigger(uint32 dummy);
extern void  DrcFastChSingleAntImprv(bool enable);

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_yanliu_href21996\1 2013-03-12 07:53:32 GMT yanliu
** HREF#21996: fix for DO MPS 3.3.1 Dynamic Range - Test 2 and Test3 Fail**/
/**Log information: \main\Trophy\1 2013-03-12 08:05:42 GMT yanliu
** HREF#21996 merged, fix DO dynamic range failure**/
