/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef EXEUTIL_H
#define EXEUTIL_H


/*****************************************************************************

  FILE NAME: exeutil.h

  DESCRIPTION:

    This file contains function prototypes and type definitions
    for the Executive utility services.

*****************************************************************************/
#include "exedefs.h"
#include "exeerrs.h"
#include "monapi.h"
#include "iopets.h"

#define EXE_LOG_RAM_BASE_ADDR HWD_SRAM_BASE_ADDR
/*#define SYS_DEBUG_NO_MSG_BUFF_CHECK*/

/*Keep in boot for dump RAM after cp halt.*/
/*#define EXE_DEBUG_KEEPIN_BOOT_FOR_HALT*/

/*use for save continue type MonFault log to flash*/
#define MON_FAULT_BUF_SIZE 40

#define EXE_STACK_DATA_REC_SIZE 10
#define EXE_STACK_DATA_MSG_SIZE (EXE_STACK_DATA_REC_SIZE*4*10)

#define EXE_CONTEXT_SWITCH_HISTORY_SIZE 4000

#define CPU_SPY_OUTPUT_ITEMS    12

/*BOOT_ZI_DATA_BUF_SIZE must be same as the length of BSRAM defined in boot.LNK. */
#define BOOT_ZI_DATA_BUF_SIZE    0x4000

#define EXE_TIMER_CALL_INFO_SIZE    80

#define EVENT_HIST_TRACE_BUFF_SIZE 400

#define EXE_DBG_TASK_EVENT_BUFFER_SIZE 80  /*For every task has 80 ExeDebugTaskEventT buffer*/
#define EXE_DBG_HISR_EVENT_BUFFER_SIZE 240  /*For all HISR has 240 ExeDebugSystemEventT buffer*/

#define EXE_DBG_SYSTEM_EVENT_BUFFER_SIZE 400

#define EXE_UPLOAD_MODE_FLAG      0xEAEEAEEA

#define ETS_DUMP_DATA_TAIL_FLAG 0x88889999

#define EXE_MONFAULT_FILENAME_LENGTH 16
#define EXE_SILENT_LOG_FILENAME_LEN 32

#define EXE_DUMP_DATA_FLAG     0x28821748

#define STACK_FLAG_SIZE 32

/* Converts from 32k ticks to msecs and Q5 format
 * If the real 32k counter is used, frequency is 32768,
 * else, it's 37500 */
#if (SYS_OPTION_32K_CLK_SOURCE != SYS_OPTION_32K_CLK_DIV_TCXO)
#define  TICK_TO_Q5MS(Ticks)           (((Ticks) * 125) >> 7)     /* (Ticks x 2^5 x 1000) / 32768 */
#else
#define  TICK_TO_Q5MS(Ticks)           (((Ticks) << 6) / 75)      /* (Ticks x 2^5 x 1000) / 37500 */
#endif

/* The 32k counter is only 24 bits wide, therefore the result is ANDed with
 * 0xFFFFFF before Q5MS conversion */
#define GET_DELTA_TIME_Q5MS(a, b)      TICK_TO_Q5MS ((a+0x1000000-b) & 0xFFFFFF)


/*Define the buffer for save crash log to RAM buffer.*/
#define CRASH_LOG_RAM_BUF_SIZE         0x18000
#define SHAREDDBG_EE_INFO_SIZE         64
#define SHAREDDBG_SLEEP_INFO_SIZE      1024
#define SHAREDDBG_CCCI_info_SIZE       1024
#define CHECK_STRUCT_LIMIT(T, SIZE)    uint8 STRUCT_LIMIT_##T[(int)(SIZE - sizeof(T))]

/* its size must be multi of 4 bytes, no packet data!
 * More efficient than packet data structure.
 * Task event log use this structure
 */
typedef struct
{
   uint32      TimeStamp;
   uint32      Arg1;
   uint16      Arg2;
   uint16      Event;  /* ExeDebugEventTypeT */
}ExeDebugTaskEventT;

/* its size must be multi of 4 bytes, no packet data!
 * More efficient than packet data structure.
 * HISR event and system event log use this structure.
 */
typedef struct
{
   uint32      TimeStamp;
   uint32      Arg1;
   uint16      Arg2;
   uint8       Event;  /* ExeDebugEventTypeT */
   uint8       ThreadId;
}ExeDebugSystemEventT;

/*its size must be multi of 4 bytes, no packet data!*/
typedef struct
{
   uint32      Index;
   uint32      TimeStamp;
   uint32      Arg1;
   uint16      Arg2;
   uint8       Event;  /* ExeDebugEventTypeT */
   uint8       ThreadId;
}ExeDebugEventTraceLogT;


typedef struct
{
  MonSysTimeT SysTime;
  uint32 FileLine;
  uint8 FileName[EXE_MONFAULT_FILENAME_LENGTH];
} ExeFileInfoT;

typedef struct
{
  uint16 BufIndex;
#ifdef SYS_DEBUG_FAULT_FILE_INFO
  ExeFileInfoT FileInfoBuf[MON_FAULT_BUF_SIZE];
#endif
  MonFaultMsgT MonFaultBuf[MON_FAULT_BUF_SIZE];
} ExeMonFaultBufInfoT;


typedef struct
{
   uint32  MsgInRun;
   uint16  MsgLenth;
   uint8   *MsgPtr;
} ExeTaskMicsDebugInfoT;

typedef struct
{
   uint16  HisrStatus;
   ExeHisrT *HisrTcbPtr;
   int32    HisrRun32kCnt;
} ExeHisrMicsDebugInfoT;

typedef struct
{
   /*low 24 bits save 32k clock value, high 8 bits save thread id. In order to save SRAM usage. */
   uint32   ThreadIdTime32k;
} ExeContextSwitchItemT;


typedef PACKED_PREFIX struct
{
   MonSpyCpuAllotStatsT CpuData[CPU_SPY_OUTPUT_ITEMS];
} PACKED_POSTFIX  ExeFautCpuAllotT;

typedef struct
{
   uint32 TimerCallBackAddr;
   uint32 TimerStart32KTime;
   uint32 TimerEnd32KTime;
   uint32 Consume32kTime;
} ExeFautTimerItemInfoT;

typedef struct
{
   uint16 Index;
   ExeFautTimerItemInfoT ItemInfo[EXE_TIMER_CALL_INFO_SIZE];
} ExeFautTimerInfoT;

typedef PACKED_PREFIX struct
{
   uint32 TimerCallBackAddr;
   uint32 TimeMs;
   uint32 ConsumeTimeMs;
} PACKED_POSTFIX  ExeFautTimerEtsLogT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT         RspInfo;
  bool               EnableAll;
  bool               DisableAll;
  ExeDebugEventTypeT Event;
  bool               OnOff;
} PACKED_POSTFIX  ExeTaskEventLogCtrlMsgT;

typedef PACKED_PREFIX struct
{
  ExeDebugEventTypeT Event;
  bool               OnOff;
} PACKED_POSTFIX EventLogStatusT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  ExeTaskEventLogStatusMsgT;

typedef PACKED_PREFIX struct
{
  EventLogStatusT Events[DBG_EVENT_MAX_ID];
} PACKED_POSTFIX  ExeTaskEventLogStatusRspMsgT;


typedef enum
{
   EXE_SILENT_LOG_START = 0x00,  /*Malloc dump ets data buffer from heap and start dump.*/
   EXE_SILENT_LOG_STOP,
   EXE_SILENT_LOG_READ,
   EXE_SILENT_LOG_RELEASE, /* Release ram buffer to heap or delete file.*/
   EXE_SILENT_LOG_READ_SETTING,
   EXE_SILENT_LOG_DISABLE  /* do EXE_SILENT_LOG_RELEASE work, disable silent log function. save setting to file */
} ExeSilentLogCtrlTypeT;

typedef enum
{
   EXE_SILENT_LOG_2_NONE      = 0x00,
   EXE_SILENT_LOG_2_SRAM,
   EXE_SILENT_LOG_2_FLASH,
   EXE_SILENT_LOG_2_SDCARD,
   EXE_SILENT_LOG_2_READING
}ExeSilentLogMediaTypeT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   ExeSilentLogCtrlTypeT  ControlTpye;
} PACKED_POSTFIX  ExeSilentLogCtrlMsgT;

typedef PACKED_PREFIX struct
{
   ExeSilentLogCtrlTypeT  ControlTpye;
   bool  Result;
} PACKED_POSTFIX  ExeSilentLogCtrlRspMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   ExeSilentLogMediaTypeT  MediaTpye;
   uint32 LogLimitSize;
   bool   EtsLogFreeze;
   bool   SaveSettingToFile;
   uint8  SdFileName[EXE_SILENT_LOG_FILENAME_LEN];
} PACKED_POSTFIX  ExeSilentLogSettingMsgT;

typedef PACKED_PREFIX struct
{
   bool  Result;
   ExeSilentLogMediaTypeT  MediaTpye;
   uint32 LogLimitSize;
   bool   EtsLogFreeze;
   uint8  SdFileName[EXE_SILENT_LOG_FILENAME_LEN];
} PACKED_POSTFIX  ExeSilentLogSettingRspMsgT;

typedef struct
{
    uint32 RamBufAddr;
    uint32 LimitLogSize;
    uint32 CurLogPtr;
    uint32 RamBufFirstPtr;
    uint32 SumLogSize;
    ExeSilentLogMediaTypeT MediaType;
    bool   EtsLogFreeze;
    uint8  FileName[EXE_SILENT_LOG_FILENAME_LEN];
} ExeSilentLogInfoT;

typedef struct
{
  uint32 RamDumpEn;
  uint32 MsgbufStatusEn;
  uint32 SilentLogBufSize;
  ExeSilentLogMediaTypeT SilentLogType;
  bool   EtsLogFreeze;
  bool   PrintfEnable;
  uint8  LogFileName[EXE_SILENT_LOG_FILENAME_LEN];
}ExeDbgSetingFileInfoT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   bool   Enable;
   bool   SaveSettingToFile;
} PACKED_POSTFIX  ExeRamDumpSettingMsgT;

typedef PACKED_PREFIX struct
{
   bool   Enable;
   bool   SaveSettingToFile;
} PACKED_POSTFIX  ExeRamDumpSettingRspMsgT;

typedef struct
{
  uint32 CrashFlag;
  uint16 ReadFlag;
  uint16 CrashCount;
}ExeFaultLogHeaderT;


typedef struct
{
   uint32 StackPtr;
   uint32 StackSize;
}ExeDbgIsrStackItemT;

typedef enum
{
   INT_STACK      = 0x00,
   IRQ_STACK,
   FIQ_STACK,
   SYSTEM_STACK,
   USER_STACK,
   UNDEF_STACK,
   ABORT_STACK,
   HISR0_STACK,
   HISR1_STACK,
   TIMER_STACK,
   ARM_REGS_STACK,  /* it should be the last one. */
   ISR_MAX_ID = ARM_REGS_STACK
}ExeDbgIsrStackId;

typedef enum
{
   EXE_CP_SW_FAULT      = 0x01,
   EXE_ARM_EXCEPT_FAULT,
   EXE_MEM_EXCEPT_FAULT,
   EXE_SOFT_DOG_FAULT,
   EXE_HWD_DOG_FAULT,

   /* Cross Core Trigger Exception. */
   EXE_LTE_FAULT,
   EXE_FAULT_TYPE_MAX
}ExeDbgFaultTypeT;

#define EXE_CRASH_INFO_FILE_NAME_LEN 64
typedef struct
{
  ExeDbgFaultTypeT CrashType;  /*uint8*/
  uint32 CrashThreadId;
  uint8 SwVersion[4];
  char CrashFileName[EXE_CRASH_INFO_FILE_NAME_LEN];
  uint16 CrashFileLine;
#ifdef MTK_DEV_OPTIMIZE_EXCEPTION
  uint32 ErrorCode[3];
#endif
} ExeFaultCrashInfoT;

typedef struct
{
   uint32   cpsr;
   uint32   spsr;
   uint32   r8;
   uint32   r9;
   uint32   r10;
   uint32   r11;
   uint32   r12;
   uint32   sp;
   uint32   lr;
} ExeArmModeRegsT;

typedef struct
{
    uint32* IRAMDumpBuffPtr;    /*Fixed the position*/
    uint32* BootZiDataBufPtr;   /*Fixed the position*/
    uint32 SramHaltlogBufPtr;   /*Fixed the position*/
    uint32 DtcmDumpBufPtr;      /*Fixed the position*/
    uint32 ItcmDumpBufPtr;      /*Fixed the position*/
    uint32 ArmRegsBufPtr;       /*Fixed the position*/
    uint32 ArmModeRegsBufPtr;   /*Fixed the position*/
    uint32* ThreadIdPtr;
    ExeTaskStatusT* TaskStatusPtr;
    ExeTaskCbT** TaskCbPtr;
    ExeMsgBuffPtrStatsT* MsgBuffPtrStatsPtr;
    ExeDbgIsrStackItemT* IsrStackInfoPtr;
    MonVersionRspMsgT* CpVersionPtr;
    FullSystemTimeT* SysTimePtr;
    ExeTaskMicsDebugInfoT* TaskMiscInfoPtr;
    ExeHisrMicsDebugInfoT* HisrMiscInfoPtr;
    ExeContextSwitchItemT* ContextSwitchInfoPtr;
    uint32* ContextSwitchIndexPtr;
    ExeDebugTaskEventT* TaskEventInfoPtr;
    uint32* TaskEventIndexPtr;
    uint32* TaskEventLimitPtr;
    ExeDebugSystemEventT* HisrEventInfoPtr;
    uint32* HisrEventIndexPtr;
    ExeDebugSystemEventT* SystemEventInfoPtr;
    uint32* SystemEventIndexPtr;
    uint32*  HisrListPtr;
    ExeMonFaultBufInfoT* MonFaultInfoPtr;
    ExeFautTimerInfoT* TimerConsumeInfoPtr;
    ExeSilentLogInfoT* SilentLogInfoPtr;
} ExeFaultDataPtrsT;

typedef enum
{
    EXESHAREDDBG_EE_MEM,
    EXESHAREDDBG_SLEEP_MEM,
    EXESHAREDDBG_CCCI_MEM,
    EXESHAREDBG_UNUSED
}ExeSharedDbgMemT;

typedef enum
{
    EXESHAREDDBG_HB_AP_MD3,
    EXESHAREDDBG_HB_MD1_MD3,
    EXESHAREDDBG_HB_INVALID
}ExeSharedDbgHbT;

#define EXE_4K_TOTAL_SZIE                                4096
#if defined(MTK_DEV_SHARED_DBG)
#if (SYS_ASIC < SA_MT6755)
#define EXE_SHARED_DBG_SAVE_EVT(Evt, Tstamp, Arg0, Arg1) ExeSharedDbgSaveEvt(Evt, Tstamp, Arg0)
#define EXE_SHARED_DBG_SAVE_HB(Type, HB)                 ExeSharedDbgSaveHeartbeat(HB)
#define EXE_SHARED_DBG_SAVE_EE_INFO(EType, Pc, Regs)     ExeSharedDbgSaveExcpRegs(EType, Pc, Regs)
#define EXE_SHARED_DBG_PROVIDE(Memp)                     ExeDbgSharedProvide(Memp)
#define EXE_SHARED_DBG_GET_POINTOR(Type, Size)           ExeSharedDbgGetMem(Type, Size)
#define EXE_SHARED_DBG_SAVE_EE_REGS(Regs, Size)          ExeSharedDbgSaveExcpModeRegs(Regs, Size)
#else
#define EXE_SHARED_DBG_SAVE_EVT(Evt, Tstamp, Arg0, Arg1) ExeSharedDbgEvt(Evt, Tstamp, Arg0, Arg1)
#define EXE_SHARED_DBG_SAVE_HB(Type, HB)                 ExeSharedDbgHb(Type, HB)
#define EXE_SHARED_DBG_SAVE_EE_INFO(EType, Pc, Regs)
#define EXE_SHARED_DBG_PROVIDE(Memp)                     ExeDbgSharedProvide(Memp)
#define EXE_SHARED_DBG_GET_POINTOR(Type, Size)           ExeSharedDbgGetMem(Type, Size)
#define EXE_SHARED_DBG_SAVE_EE_REGS(Regs, Size)
#endif
#endif
#if defined(MTK_DEV_SHARED_DBG)
#if (SYS_ASIC < SA_MT6755)
void ExeSharedDbgSaveLastInfo(void);
void ExeSharedDbgSaveWdtKick(uint32 tstamp);
void ExeSharedDbgSaveEvt(ExeDebugEventTypeT Event, uint32 tstamp, uint16 MsgId);
void ExeSharedDbgSaveExcpRegs(uint32 eType, uint32 pc, uint32* eRegs);
void ExeSharedDbgSaveExcpModeRegs(uint32* modeRegs, uint32 size);
void ExeSharedDbgSaveHeartbeat(uint32 heartbeat);
#else
void ExeSharedDbgWdt(uint32 Time32k);
void ExeSharedDbgContext(uint8 TrdIdx);
void ExeSharedDbgInt(bool Enable, uint8 Type);
void ExeSharedDbgHb(ExeSharedDbgHbT Type, uint32 HeartBeat);
void ExeSharedDbgEvt(uint8 Evt, uint32 Time32k, uint32 Arg1, uint32 Arg2);
#endif
void* ExeSharedDbgGetMem(ExeSharedDbgMemT Type, uint32* leng);
#if defined(MTK_DEV_SHARED_AP_PROVIDE)
void ExeDbgSharedProvide(void* memory);
#endif
#endif

void* ExeSharedDbgCCCI(uint32* leng);
void* ExeSharedDbgExcp(uint32* leng);
void* ExeGetSharedDbgAddr(void);



/*****************************************************************************

  FUNCTION NAME: ExeInitMsgBuffStats

  DESCRIPTION:

     This routine initialize all the buffer statistics data structures

  PARAMETERS:

     None

  RETURNED VALUES:

     None

*****************************************************************************/
extern void ExeInitMsgBuffStats(void);


/*****************************************************************************

  FUNCTION NAME: ExeDecMsgBuffStats

  DESCRIPTION:

     This routine decrements all the msg buffer statistics.

  PARAMETERS:

     MsgBuffPtr - Current active msg pointer

  RETURNED VALUES:

     TRUE or FALSE

*****************************************************************************/
extern bool ExeDecMsgBuffStats(void * MsgBuffPtr);


/*****************************************************************************

  FUNCTION NAME: ExeIncMsgBuffSendStats

  DESCRIPTION:

     This routine increments the msg buffer send statistics.

  PARAMETERS:

     MsgBuffPtr - Current active msg pointer
     MsgId      - Msg id of msg in msg buffer
     DstTask    - Task id receiving the message

  RETURNED VALUES:

     TRUE or FALSE

*****************************************************************************/
extern bool ExeIncMsgBuffSendStats(void * MsgBuffPtr, uint32 MsgId, uint32 DstTask);

/*****************************************************************************

  FUNCTION NAME: ExeIncMsgBuffReadStats

  DESCRIPTION:

     This routine increments the msg buffer read statistics.

  PARAMETERS:

     MsgBuffPtr - Current active msg pointer
     FunctionName - The function name to call ExeMessageBufferRead()
     Linenumber   - The code line number to call ExeMessageBufferRead()

  RETURNED VALUES:

     TRUE  - successful
     FALSE - unsuccessful

*****************************************************************************/
bool ExeIncMsgBuffReadStats(void *MsgBuffPtr, const char *FunctionName, unsigned Linenumber);

/*****************************************************************************

  FUNCTION NAME: ExeIncMsgBuffStats

  DESCRIPTION:

     This routine increments all the msg buffer statistics.

  PARAMETERS:

     MsgBuffPtr   - Current active msg pointer
     MsgBuffSize  - Msg buffer size of active msg pointer
     FunctionName - The fuction name to call ExeMessageBufferGet()
     Linenumber   - The code line number to call ExeMessageBufferGet()

  RETURNED VALUES:

     None

*****************************************************************************/
bool ExeIncMsgBuffStats(void * MsgBuffPtr, uint32 MsgBuffSize,
                        const char *FunctionName, unsigned Linenumber);

/*****************************************************************************

  FUNCTION NAME: ExeGetMsgBuffStats

  DESCRIPTION:

     This routine returns the current Message buffer usages

  PARAMETERS:
        The required message buffer size

  RETURNED VALUES:

     None

*****************************************************************************/
extern uint16 ExeGetMsgBuffStats(uint16 MsgBuffSize);


/*****************************************************************************

  FUNCTION NAME: ExeFaultLogDump

  DESCRIPTION:

    Dump the fault data to flash when call IopTxCriticalMsg.

  PARAMETERS:



  RETURNED VALUES:

    None.

  TASKING CHANGES:

    None.

*****************************************************************************/
void ExeFaultLogDump(uint8* ExeFaultData, uint16 FaultSize, IopEtsMsgIdT EtsMsgId);


/*****************************************************************************

  FUNCTION NAME: ExeTxDumpFaultLogToEts

  DESCRIPTION:

    Sent dump fault data to ETS.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

  TASKING CHANGES:

    None.

*****************************************************************************/
void ExeTxDumpFaultLogToEts(void);


/*****************************************************************************

  FUNCTION NAME: ExeCleanDumpFaultLog

  DESCRIPTION:

    Erase the fault log dump flash section.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

  TASKING CHANGES:

    None.

*****************************************************************************/
void ExeCleanDumpFaultLog(MonCleanFaultLogMsgT* Ptr);


/*****************************************************************************

  FUNCTION NAME: ExeUtilCheckNewHaltLog

  DESCRIPTION:

    The routine use as check whether there are new halt log in flash hasn't been read.

  PARAMETERS:


  RETURNED VALUES:

    TRUE or FALSE.

  TASKING CHANGES:

    None.

*****************************************************************************/
extern void ExeUtilCheckNewHaltLog(MonCheckFaultLogRspMsgT* Ptr);


/*****************************************************************************

  FUNCTION NAME: ExeUtilBootRamDumpEn

  DESCRIPTION:

    The routine can enable/disable upload mode. It will write a SIDB(0xeae, 0xeae) for keep the setting
    when the next power on.

  PARAMETERS:
     bool Enable

  RETURNED VALUES:

    TRUE or FALSE.

  TASKING CHANGES:

    None.

*****************************************************************************/
extern void ExeUtilBootRamDumpEn(ExeRamDumpSettingMsgT* MsgPtr);


/*****************************************************************************

  FUNCTION NAME: ExeTimerConsumeTimeSpy

  DESCRIPTION:

    The routine output the timer HISR callback time consume spy data to ETS. Call from
    MON task.

  PARAMETERS:
     None.

  RETURNED VALUES:

    None.

  TASKING CHANGES:

    None.

*****************************************************************************/
extern void ExeTimerConsumeTimeSpy(void);


/*****************************************************************************

  FUNCTION NAME: ExeCpuAllotSpy

  DESCRIPTION:

    The routine output the CPU allotment spy data to ETS. Call from
    MON task.

  PARAMETERS:
     None.

  RETURNED VALUES:

    None.

  TASKING CHANGES:

    None.

*****************************************************************************/
extern void ExeCpuAllotSpy(void);

/*****************************************************************************

  FUNCTION NAME: ExeUtilGetThreadConsumeTime

  DESCRIPTION:

    The routine can calculate the consume time of a thread between Start32kCnt and End32kCnt. The difference of
    Start32kCnt and End32kCnt should less than 1 second. If you want check more time space, please enlarge
    EXE_CONTEXT_SWITCH_HISTORY_SIZE define. The routine can use several ms to run.

  PARAMETERS:
     ThreadId    --The thread ID you want to check.
     Start32kCnt--Start time of 32k clock.
     End32kCnt  --End time of 32k clock.

  RETURNED VALUES:

    the consume time, unit is 32k clock tick. About 33 tick is 1ms.

  TASKING CHANGES:

    None.

*****************************************************************************/
extern uint32 ExeUtilGetThreadConsumeTime(uint32 ThreadId, uint32 Start32kCnt, uint32 End32kCnt);


/*****************************************************************************
  FUNCTION NAME: ExeSaveDspMboxData

  DESCRIPTION:   This routine stores the content of the DSP Mailbox in a
                 fault record prior to a HALT.

  PARAMETERS:    IPC_DSPM_PROC or IPC_DSPV_PROC

  RETURNED VALUES:  None.
*****************************************************************************/
extern void ExeSaveDspMboxData (uint8 DspProc);

/*****************************************************************************
  FUNCTION NAME: ExeSaveDspmCrashData

  DESCRIPTION:   This routine stores the content of the DSPM crash data message

  PARAMETERS:    Buffer pointer

  RETURNED VALUES:  None.
*****************************************************************************/
void ExeSaveDspmCrashData (uint16 *Buffer);

/*****************************************************************************
  FUNCTION NAME: ExeDbgSetSystemEventlog

  DESCRIPTION:   This routine records an event into both the thread events
                 and task event history.

  NOTE:          It should be called with interrupts disabled.

  PARAMETERS:    Event       - event type.
                 Arg1 - receiving task id, event flags, signals or msgid (depending on action).
                 Arg2 - event flags, signals, msgid or msg buffer ptr (depending on action).
                 ForceDump - If TRUE force save the event log to buffer, don't controled by EventEnTab[]

  RETURNED VALUES:  None.
*****************************************************************************/

void ExeDbgSetSystemEventlog (ExeDebugEventTypeT Event, uint32 Arg1, uint16 Arg2, bool ForceDump);


/*****************************************************************************

  FUNCTION NAME: ExeTaskStackCheckMsg

  DESCRIPTION:

    Process Task stack check command.

  PARAMETERS:

    None

  RETURNED VALUES:

    None

*****************************************************************************/
void ExeTaskStackCheckMsg(void);

/*****************************************************************************

  FUNCTION NAME: ExeIntManageStackCheck

  DESCRIPTION:

    Process Task stack check command, out put the stack useage informatio to ETS.

  PARAMETERS:

    None

  RETURNED VALUES:

    None

*****************************************************************************/
void ExeIntManageStackCheck(void);

/*****************************************************************************

  FUNCTION NAME: ExeUtilGetEventTypeState

  DESCRIPTION:

    Check whether a system event log is enabled.

  PARAMETERS:

    Event: ExeDebugEventTypeT type.

  RETURNED VALUES:

    TRUE or FALSE

*****************************************************************************/
bool ExeUtilGetEventTypeState(ExeDebugEventTypeT Event);

/*****************************************************************************

  FUNCTION NAME: ExeUtilSetEventTypeState

  DESCRIPTION:

    Set as enable or disable for a system event log type.

  PARAMETERS:

    MsgPtr: Message ptr from ETS.

  RETURNED VALUES:

    None

*****************************************************************************/
void ExeUtilSetEventTypeState(void* MsgPtr);


/*****************************************************************************

  FUNCTION NAME: ExeUtilQueryEventTypeState

  DESCRIPTION:

    Query status of a system event, output the result to ETS.

  PARAMETERS:

    MsgPtr: Message ptr from ETS.

  RETURNED VALUES:

    None

*****************************************************************************/
void ExeUtilQueryEventTypeState(void* MsgPtr);

/*****************************************************************************

  FUNCTION NAME: ExeSilentLogDump

  DESCRIPTION:

    Dump the monprintf/monspy/montrace data to flash.

  PARAMETERS:



  RETURNED VALUES:

    None.

  TASKING CHANGES:

    None.

*****************************************************************************/
bool ExeSilentLogDump(void* DataPtr, uint16 MsgSize, uint32 MsgId);

void ExeSoftWatchDogCheck(void);

/*****************************************************************************
* $Log: exeutil.h $
* Revision 1.3  2004/10/07 12:39:55  blee
* ExeGetMsgBuffStats is added.
* Revision 1.2  2004/03/25 11:45:49  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.1  2003/05/12 15:26:13  fpeng
* Initial revision
* Revision 1.3  2002/06/04 08:07:10  mshaver
* Added VIA Technologies copyright notice.
* Revision 1.2  2000/11/30 09:54:03  plabarbe
* Removed comment log from CBP3 project.
* Revision 1.1  2000/10/09 08:58:31  mshaver
* Initial revision
*****************************************************************************/

#endif
