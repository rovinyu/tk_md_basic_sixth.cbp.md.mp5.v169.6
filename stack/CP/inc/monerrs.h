/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef MONERRS_H
#define MONERRS_H
/*****************************************************************************
 
  FILE NAME:  monerrs.h

  DESCRIPTION:

    This file contains the fault codes for the MON software unit.

*****************************************************************************/

/*------------------------------------------------------------------------
*  The following definitions are fault ids for MonFault routine.
*-----------------------------------------------------------------------*/

typedef enum 
{
   MON_MSG_ID_ERR                         = 0,
   MON_GEN_SPY_ID_ERR                     = 1,
   MON_SPY_LEN_ERR                        = 2,
   MON_TRACE_ID_ERR                       = 3,
   MON_TRACE_ARGS_ERR                     = 4,
   MON_BB_MODE_ERR                        = 5,
   MON_PRINTF_ARGS_ERR                    = 6,
   MON_PEEK_LEN_ERR                       = 7,
   MON_POKE_LEN_ERR                       = 8,
   MON_SEQ_NUM_ERR                        = 9,
   MON_FLASH_SECTION_ERR                  = 10,
   MON_CHECKSUM_ERR                       = 11,
   MON_VTST_MODE_ERR                      = 12,
   MON_ASIC_REV_ERR                       = 13,
   MON_LOOPBACK_ERR                       = 14,
   MON_RT_LIBRARY_ERR                     = 15,

   MON_DSP_DNLOAD_FLASH_FILE_ID_ERR       = 20,
   MON_DSP_DNLOAD_ASIC_REVISION_ERR       = 21,
   MON_DSP_DNLOAD_ASIC_VERSION_ERR        = 22,
   MON_DSP_DNLOAD_TOO_MANY_PATCHES_ERR    = 23,
   MON_DSP_DNLOAD_NO_PATCH_ERR            = 24,
   MON_DSP_DNLOAD_TIMEOUT_ERR             = 25,
   MON_DSP_DNLOAD_CHECKSUM_ERR            = 26,
   MON_DSP_DNLOAD_COMPLETE_ERR            = 27,
   MON_DSP_DNLOAD_PATCHSET_ID_ERR         = 28,

   MON_EXCEPT_RESET_ERR                   = 29,
   MON_EXCEPT_UNDEFINED_ERR               = 30,
   MON_EXCEPT_SWI_ERR                     = 31,
   MON_EXCEPT_PREFETCH_ABORT_ERR          = 32,
   MON_EXCEPT_DATA_ABORT_ERR              = 33,
   MON_EXCEPT_RESERVED_ERR                = 34,

   MON_BB_NOT_READY_ERR                   = 40,
   MON_BB_FROMBUFFER_NOT_READY_ERR        = 41,
   MON_BB_INVALID_MODE_ERR                = 42,
   MON_BB_OPERATION_FAILED_ERR            = 43,

   MON_SYSREL_ASICVER_ERR                 = 44,
   MON_SYSREL_ASICTYPE_ERR                = 45,
   MON_SYSREL_CPVER_ERR                   = 46,
   MON_SYSREL_DSPVVER_ERR                 = 47,
   MON_SYSREL_DSPMVER_ERR                 = 48,
   MON_TEST_NOT_SUPPORTED_ERR             = 49,
   MON_INVALID_NUM_APP_MSGS_ERR           = 50,
   MON_DYN_CODE_DNLD_FAILED_ERR           = 51,
   MON_INVALID_APP_CHAN_MSG_ID_ERR        = 52,
   MON_INVALID_FILE_OFFSET_ERR            = 53,
   MON_DO_AUTO_SPY_ID_ERR                 = 54,
   MON_UNEXPECTED_DSPV_RESET_ERR          = 55,
   MON_FLASH_SECTION_SIZE_ERR             = 56,
   MON_DEEP_SLEEP_TIME_TOO_LARGE          = 57,
   MON_STACK_OVERFLOW_ERR                 = 58,
   MON_STACK_SIZE_TOO_SMALL_WARNING       = 59,
   MON_DSPM_FAULT_HALT_ERR                = 60,
   MON_DSPV_FAULT_HALT_ERR                = 61,
   MON_DSP_FAILED_TO_START                = 62,
   MON_UNEXPECTED_DSPM_RESET_ERR          = 63,
   MON_REQUESTED_HEAP_NOT_AVAIL           = 64,
   MON_TOO_MANY_HEAP_ALLOC_REQUESTS       = 65,
   MON_NO_DEBUG_HEAP_ALLOCATED_ERR        = 66,
   MON_SILENT_LOG_UPLOAD_ERR              = 67,
   MON_DEEP_SLEEP_MULTIMODE_ERR           = 68,
   MON_ALIGNMENT_ADDR_ERR                 = 69,
   MON_ACCESS_PERMISSION_ERR              = 70,
   MON_WAKE_TIME_CALC_ERR                 = 71,
   MON_DEEP_SLEEP_RATIO_LOW_ERR           = 72,

   MON_DSP_BOOT_START                     = 73,
   MON_DSP_NORMAL_START                   = 74

   
} MonErrsT;

typedef enum
{
   MON_ARM_RESET_EXCEPTION,
   MON_ARM_UNDEFINEDINSTR_EXCEPTION,
   MON_ARM_SWI_EXCEPTION,
   MON_ARM_PREFETCHABORT_EXCEPTION,
   MON_ARM_DATAABORT_EXCEPTION,
   MON_ARM_RESERVED_EXCEPTION,
   MON_MEM_WIRAM_EXCEPTION,
   MON_MEM_WITCM_EXCEPTION,
   MON_MEM_ACCESS_EXCEPTION
}MonExceptionTypeT;

#ifdef MTK_DEV_ENABLE_DSP_DUMP
typedef enum
{
   MON_DSPM_DUMP_SUCCESS,
   MON_DSPM_DUMP_NO_NEED,
   MON_DSPM_DUMP_CHECKSUM_ERR
} MonDspmDumpErrsT;
#endif

#ifdef MTK_DEV_OPTIMIZE_EXCEPTION
extern MonExceptionTypeT monExceptionType;
extern uint32 ExceptionCpsr;
#endif

/*****************************************************************************
* $Log: monerrs.h $
* Revision 1.6  2006/01/03 10:35:26  wavis
* Merging in VAL.
* Revision 1.5.1.3  2005/11/22 11:50:00  wavis
* Removed I2C error codes (moved to HWD).
* Revision 1.5.1.2  2005/11/14 11:45:18  vxnguyen
* Added Dynamic Code Download error codes.
* Revision 1.5  2005/07/29 11:28:26  vxnguyen
* Added new error codes.
* Revision 1.4  2005/03/18 10:38:42  cmastro
* 4.05 merge (CP_HWD_2.9.0_4.05_Merge)
* Revision 1.3  2004/09/28 11:39:08  cmastro
* added MonExceptionTypeT
* Revision 1.2  2004/03/25 11:46:01  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.3  2003/08/04 19:33:59  vxnguyen
* Added new Big Buffer error codes.
* Revision 1.2  2003/05/19 17:03:09  vxnguyen
* Took out MON_CANT_JUMP_WITH_WD_ENABLED_ERR err code.
* Revision 1.1  2003/05/12 15:26:24  fpeng
* Initial revision
* Revision 1.12  2003/03/18 13:34:38  mshaver
* Added new MON error for I2C bus timeouts
*****************************************************************************/
#endif
/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_zlou_href16754\1 2011-03-25 02:56:06 GMT zlou
** HREF#16754 : update the cp code of dspv patch**/
/**Log information: \main\CBP7FeaturePhone\5 2011-03-25 08:12:47 GMT ygwu
** HREF#16754|merge SD's change for IPC size error halt during power up**/
