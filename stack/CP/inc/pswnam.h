/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1997-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/****************************************************************************
 *
 * Module:    pswnam.h
 *
 * Purpose:   Definitions and prototypes for IS-95 NAM data parameters
 *
 ****************************************************************************
 *
 *                          PVCS Header Information
  *
 *  $Log: pswnam.h $
 *  Revision 1.4  2006/03/07 16:50:13  winston
 *  CR7319: OTA Verizon features implementation
 *  Revision 1.3  2006/03/03 13:34:06  vsethuraman
 *  CR7207 :- PN310 CSS and ERI changes.
 *  Revision 1.2  2004/03/25 11:46:04  fpeng
 *  Updated from 6.0 CP 2.5.0
 *  Revision 1.3  2004/02/10 15:17:04  bcolford
 *  Merged CBP4.0 Release 8.05.0 changes. 
 *  Revision 1.14  2003/11/11 11:30:43  blee
 *  remove the bit-field OTA_NAM_LOCKED from OTA_Capability_Map
 *  Revision 1.12  2003/08/22 11:34:10  mclee
 *  Uim Feature Introduction in SW Base
 *  Revision 1.11  2003/08/13 15:07:15  mclee
 *  Add pref mode defines
 *  Revision 1.10  2002/09/30 14:57:54  mshaver
 *  Changed the following data structures to be packed:
 *  ZoneList, SidNidList, IMSIType. Renamed the data
 *  structure IS95Nam to PswIs95NamT and it now contains the
 *  only NAM data structure definition.
 *  Revision 1.9  2002/08/02 17:51:22  bsharma
 *  Added SmsTlMaxRetry to NAM structure
 *  Revision 1.8  2002/07/31 18:55:50  hans
 *  NUM_BANDS_SUPPORTED increased to include band class 4. Band Class 4 (cr726) and support for other band classes (cr474)
 *  added to Band Class enum.
 *  Revision 1.7  2002/07/26 14:06:30  ameya
 *  Added 20 positive sids/nids and 10 negative sids/nids and a stored negative sid/nid parameter to the NAM structure.
 *  Revision 1.6  2002/07/25 10:07:08  etarolli
 *  Changed NAM MIP MAX field size for password and NAI size.
 *  Revision 1.5  2002/06/04 08:07:22  mshaver
 *  Added VIA Technologies copyright notice.
 *  Revision 1.4  2002/02/27 20:48:37  mclee
 *  Modifed the struct IS95Nam to include the new Is801 related Nam params.
 *  Revision 1.3  2002/02/27 10:52:57  etarolli
 *  Increased following NAM fields max size: MN_NAI from 10 -> 20 MN_PASSWRD from 8 -> 10. Also fixed spelling of MN_NAI field
 *  Revision 1.2  2002/02/06 12:00:54  mclee
 *  Add new A_KeyValid, VP_Enable, and FUTURE_EXPAND_FIELDS to Nam structure.
 *  Revision 1.1  2001/11/20 06:46:36  mclee
 *  Initial revision
 *  Revision 1.1.1.1  2001/10/11 13:24:04  mclee
 *  Duplicate revision
 *  Revision 1.1  2001/10/11 13:24:04  mclee
 *  Initial revision
 *
 ****************************************************************************
 ***************************************************************************/

#ifndef _PSWNAM_H_

#define _PSWNAM_H_ 1

/*****************************************************************************
 * Includes
 ****************************************************************************/
#include "sysdefs.h"
#include "pswcustom.h"
#include "hlpapi.h"
#include "cssdefs.h"
/*****************************************************************************
 * Definitions for use with NAM
 ****************************************************************************/
  #define NAM_BANDS_SUPPORTED   5
                                    /* skip, T53, Korean PCS            */

                                      
  #define CP_MAX_TMSI_ZONE_LEN  8
  #define NAM_MEID_SIZE         7    /* size of MEID */
#ifndef MTK_PLT_ON_PC
  #define REMAINING_NAM_SIZE    3
#else
  #define REMAINING_NAM_SIZE    2
#endif

  #define OTA_ALLOW_OTAPA       0x02   /* User flag to reject network OTAPA attemps */
  #define OTA_ENABLE_OTA        0x04   /* User flag to enable OTA functionality in MS */
  #define OTA_ENABLE_SPASM      0x08   /* User flag to enable SPASM function in MS */
  #define OTA_ENABLE_SPC_CHANGE 0x10   /* User flag to enable SPC Change in MS */

  #define AUTH_AKEY_VALID       0x01   /* Akey Valid. OK for Authentication */
  #define AUTH_ENABLE_VP        0x02   /* Voice Privacy Enabled */
  #define MOB_TERM_HOME_ENABLED 0x01   /* receive mobile terminated calls in home system */
  #define MOB_TERM_SID_ENABLED  0x02   /* receive mobile terminated calls in foreign SID system */
  #define MOB_TERM_NID_ENABLED  0x04   /* receive mobile terminated calls in foreign NID system */

  #define ROAM_SETTING_DOMESTIC_VOICE 0x01 /* Sprint Allow voice roaming on domestic system */
  #define ROAM_SETTING_DOMESTIC_DATA  0x02 /* Sprint Allow data roaming on domestic system */
  #define ROAM_SETTING_INT_VOICE      0x04 /* Sprint Allow voice romaing on international system */
  #define ROAM_SETTING_INT_DATA       0x08 /* Sprint Allow data roaming on international system */
  #ifdef MTK_CBP
  #define ROAM_SETTING_LTE_DATA       0x10 /* Sprint Allow LTE data roaming, not used in C2K saved only for AP get */
  #define ROAM_SETTING_USED_BIT_NUM   5    /* Now only the 5 least significant bits used */
  #endif

  #define NAM_MAX_MDN_DIGITS    16     /* max MDN digit number in NAM template */

  #define SPC_MAX_VALUE         999999
#ifdef MTK_CBP
  #define SPC_MAX_RETRY         15
#endif
/*****************************************************************************
 * Registration structures
 ****************************************************************************/

/* Zone based registration list */
typedef NV_PACKED_PREFIX struct
{
    uint16    regZone;      /* registration zone           */
    uint16    sid;          /* system id                   */
    uint16    nid;          /* network id                  */
    uint16    ageTimer;     /* age limit in seconds        */
    bool      timerEnabled;
    uint8     block;        /* PCS block or serving system */
    SysCdmaBandT bandClass;
} NV_PACKED_POSTFIX  ZoneList;

/* Zone based registration list */
typedef PACKED_PREFIX struct
{
    uint16    regZone;      /* registration zone           */
    uint16    sid;          /* system id                   */
    uint16    nid;          /* network id                  */
    uint16    ageTimer;     /* age limit in seconds        */
    bool      timerEnabled;
    uint8     block;        /* PCS block or serving system */
    SysCdmaBandT bandClass;
} PACKED_POSTFIX  PackedZoneList;


/* System/Network registration list */
typedef NV_PACKED_PREFIX struct
{
    uint16    sid;          /* system id                   */
    uint16    nid;          /* network id                  */
    uint16    ageTimer;     /* age limit in seconds        */
    bool      timerEnabled;
    uint8     block;        /* PCS block or serving system */
    SysCdmaBandT bandClass; 
} NV_PACKED_POSTFIX  SidNidList;

/* System/Network registration list */
typedef PACKED_PREFIX struct
{
    uint16    sid;          /* system id                   */
    uint16    nid;          /* network id                  */
    uint16    ageTimer;     /* age limit in seconds        */
    bool      timerEnabled;
    uint8     block;        /* PCS block or serving system */
    SysCdmaBandT bandClass; 
} PACKED_POSTFIX  PackedSidNidList;


/*****************************************************************************
 * IMSI structure definitions
 ****************************************************************************/

  /* An IMSI is <= 15 digits in length. It is composed of:           */
  /*     mcc (3 digits) + nmsi (up to 12 digits)                     */

  /* An NMSI is <= 12 digits in length. Is is composed of:           */
  /*     imsi_11_12 (2 digits) + imsi_s (10 digits)  */
  /* In turn, imsi_s is composed of 2 parts:                         */
  /*    imsi_s1 (upper 3 digits) + imsi_s2 (lower 7 digits)          */

  /* If the imsi is Class 0 then imsiClass must be set to            */
  /*  CP_IMSI_CLASS_0 and addrNum is set to 8                        */
  /* If the imsi is Class 1 then imsiClass must be set to            */
  /*  CP_IMSI_CLASS_1 and addrNum is set to # of digits in NMSI - 4  */

typedef NV_PACKED_PREFIX struct
{
    uint16 mcc;                  /* Mobile Country Code                */
    uint8  imsi_11_12;           /* 7 bit  IMSI_11_12                  */
    uint16 imsi_s2;              /* 10 bit IMSI_S2 value               */
    uint32 imsi_s1;              /* 24 bit IMSI_S1 value               */
    uint8  imsiClass;            /* CP_IMSI_CLASS_0/1 indication       */
    uint8  addrNum;              /* number of digits in NMSI - 4       */
} NV_PACKED_POSTFIX  IMSIType;

typedef PACKED_PREFIX struct
{
    uint16 mcc;                  /* Mobile Country Code                */
    uint8  imsi_11_12;           /* 7 bit  IMSI_11_12                  */
    uint16 imsi_s2;              /* 10 bit IMSI_S2 value               */
    uint32 imsi_s1;              /* 24 bit IMSI_S1 value               */
    uint8  imsiClass;            /* CP_IMSI_CLASS_0/1 indication       */
    uint8  addrNum;              /* number of digits in NMSI - 4       */
} PACKED_POSTFIX  PackedIMSIType;


/*****************************************************************************
 * IS-95 NAM structure
 ****************************************************************************/
typedef enum
{
    NAM_1 = 1,
    NAM_2
} NamNumber;

typedef NV_PACKED_PREFIX struct
{
    /* Permanent Mobile Station Indicators                                */
    uint32    ESN;            /* Place Holder for ESN, Read only         */
    uint8     SCMp[NAM_BANDS_SUPPORTED];
                                /* Station Class Mark for each band       */
    uint8     SLOT_CYCLE_INDEXp; /* slot cycle index                     */
    uint16    MOB_FIRM_REVp;  /* mobile firmware revision                */
    uint8     MOB_MODELp;     /* Manufacturers  model number             */
    uint8     MOB_P_REVp[NAM_BANDS_SUPPORTED];
                                /* Protocol revision number for each band */

    /* Semi-Permanent Mobile Station Indicators                           */
    bool      ValidZoneEntry; /* indicates a valid ZONE_LIST entry        */
    ZoneList  ZONE_LISTsp;    /* zone based registration list             */
    bool      ValidSidNidEntry; /* indicates a valid SID_NID_LISTsp entry */
    SidNidList SID_NID_LISTsp;  /* System/Network registration list       */
    int32     BASE_LAT_REGsp; /* latitude from base last registered on    */
    int32     BASE_LONG_REGsp;/* longitude from base last registered on   */
    uint16    REG_DIST_REGsp; /* registration distance from last base     */
    uint8     LCKRSN_Psp;     /* lock code reason                         */
    uint8     MAINTRSNsp;     /* maintenance reason                       */
    bool      DIGITAL_REGsp;  /* previous registration on digtal system   */

    /* NAM indicators                                                     */
    uint8     PREF_BANDp;     /* No longer used                           */
    uint8     RESERVED0;      /* Previous location for preferred mode     */
    uint8     PREF_BLOCK_BAND1p;/* No longer used                         */
    uint8     PREF_SERV_BAND0p; /* No longer used                         */
#ifndef MTK_CBP
    uint8     RESERVED;       /* Previous location of PRL PRev           */
#else
    uint8     SPCc;             /* left retry count */
#endif
    uint32    RESERVED1[2];    /* Previous Location for A_Key            */
    uint8     RESERVED2[7];    /* Place Holder for previous SSDA          */
    uint8     RESERVED3[8];    /* Place Holder for previous SSDB          */
    uint8     COUNTsp;         /* Call History Parameter                  */
    uint8     ValidIMSImap;    /* Refer to Bit Map text at top of file    */ 
    IMSIType  IMSI_Mp;         /* IMSI_M - min                            */
    IMSIType  IMSI_Tp;         /* IMSI_T                                  */
    uint8     ASSIGNING_TMSI_ZONE_LENsp;
    uint8     ASSIGNING_TMSI_ZONEsp[ CP_MAX_TMSI_ZONE_LEN ];
    /*   assigning tmsi zone                    */
    uint32    TMSI_CODEsp;     /* tmsi code                               */
    uint32    TMSI_EXP_TIMEsp; /* tmsi expiration timer                   */
    uint16    SIDp[MAX_POSITIVE_SIDS]; /* home system ids stored in NAM   */
    uint16    NIDp[MAX_POSITIVE_SIDS]; /* home network ids stored in NAM  */
    uint16    NEG_SIDp[MAX_NEGATIVE_SIDS]; /* negative system ids stored in NAM */
    uint16    NEG_NIDp[MAX_NEGATIVE_SIDS]; /* negative network ids stored in NAM */
    uint8     MAX_SID_NID;     /* Max sid/nid pairs that can be stored in NAM*/
    uint8     STORED_POS_SID_NID;  /* No of Pos sid/nid pairs stored in NAM */
    uint8     STORED_NEG_SID_NID;  /* No of Neg sid/nid pairs stored in NAM */
    uint8     ACCOLCp;         /* access overload class                    */
    uint8     MobTermMap;      /* receive mobile terminated calls          */
    uint8     BCAST_ADDR_LENp; /* broadcast address length (0-not config.) */
    uint8     BCAST_ADDRp[ CP_BCAST_ADDRESS_MAX_SIZE];
                               /* broadcast address data                   */
#ifndef MTK_CBP
    uint32    RESERVED4;       /* Place holder for previous SPCp           */
#else
    uint32    SPCp;             /* Place holder for previous SPCp           */
#endif
    uint8     OTA_Capability_Map; /* See Bit Map explanation at top of file*/
    uint8     MDN_NUM_DIGITS;
    uint8     Mdn[NAM_MAX_MDN_DIGITS];  /* MDN - Mobile directory number   */

    /* Analog support */
    uint16    HOME_SIDp;       /* home sid for analog system support       */
    uint8     EXp;             /* extended address support for analog      */
    uint16    FIRSTCHPp;       /* first paging channel                     */
    uint8     DTXp;            /* Analog DTX Option                        */
    uint16    FCCA, LCCA;      /* Analog Custom Control Channel Set A      */
    uint16    FCCB, LCCB;      /* Custom Control Channel Set B             */
    uint32    NXTREGsp;
    uint16    SIDsp;
    uint16    LOCAIDsp;
    bool      PUREGsp;

    uint16    CPCA, CSCA;      /* CDMA Primary (and Secondary) Channel A   */
    uint16    CPCB, CSCB;      /* CDMA Primary (and Secondary) Channel B   */
    uint8     RESERVED5[125];  /* Previous Mobile IP parameters            */
    uint8     Auth_Capability_Map; /* See Bit Map Explination at top of file */
    uint8     RESERVED6[89];   /* Previous IS801 Data Call parameters      */
    uint8     SmsTlMaxRetry;
    uint32    UIM_ID;
    uint8     RoamingSetting;  /* Only for Sprint                          */
    uint8     SystemSelect;
    /* Following array reserved for future use. If new Nam parameter 
     * needed then decrement from array below. Total Nam size is always 
     * 560 bytes. */
    uint8     FUTURE_EXPAND_FIELDs[REMAINING_NAM_SIZE];

    /* Checksum support */
    uint16   Checksum;            /* checksum,no used */
} NV_PACKED_POSTFIX  PswIs95NamT;

typedef PACKED_PREFIX struct
{
    /* Permanent Mobile Station Indicators                                */
    uint32    ESN;            /* Place Holder for ESN, Read only         */
    uint8     SCMp[NAM_BANDS_SUPPORTED];
                                /* Station Class Mark for each band       */
    uint8     SLOT_CYCLE_INDEXp; /* slot cycle index                     */
    uint16    MOB_FIRM_REVp;  /* mobile firmware revision                */
    uint8     MOB_MODELp;     /* Manufacturers  model number             */
    uint8     MOB_P_REVp[NAM_BANDS_SUPPORTED];
                                /* Protocol revision number for each band */

    /* Semi-Permanent Mobile Station Indicators                           */
    bool      ValidZoneEntry; /* indicates a valid ZONE_LIST entry        */
    PackedZoneList  ZONE_LISTsp;    /* zone based registration list             */
    bool      ValidSidNidEntry; /* indicates a valid SID_NID_LISTsp entry */
    PackedSidNidList SID_NID_LISTsp;  /* System/Network registration list       */
    int32     BASE_LAT_REGsp; /* latitude from base last registered on    */
    int32     BASE_LONG_REGsp;/* longitude from base last registered on   */
    uint16    REG_DIST_REGsp; /* registration distance from last base     */
    uint8     LCKRSN_Psp;     /* lock code reason                         */
    uint8     MAINTRSNsp;     /* maintenance reason                       */
    bool      DIGITAL_REGsp;  /* previous registration on digtal system   */

    /* NAM indicators                                                     */
    uint8     PREF_BANDp;     /* No longer used                           */
    uint8     RESERVED0;      /* Previous location for preferred mode     */
    uint8     PREF_BLOCK_BAND1p;/* No longer used                         */
    uint8     PREF_SERV_BAND0p; /* No longer used                         */
#ifndef MTK_CBP
    uint8     RESERVED;       /* Previous location of PRL PRev           */
#else
    uint8     SPCc;             /* left retry count */
#endif
    uint32    RESERVED1[2];    /* Previous Location for A_Key            */
    uint8     RESERVED2[7];    /* Place Holder for previous SSDA          */
    uint8     RESERVED3[8];    /* Place Holder for previous SSDB          */
    uint8     COUNTsp;         /* Call History Parameter                  */
    uint8     ValidIMSImap;    /* Refer to Bit Map text at top of file    */ 
    PackedIMSIType  IMSI_Mp;         /* IMSI_M - min                            */
    PackedIMSIType  IMSI_Tp;         /* IMSI_T                                  */
    uint8     ASSIGNING_TMSI_ZONE_LENsp;
    uint8     ASSIGNING_TMSI_ZONEsp[ CP_MAX_TMSI_ZONE_LEN ];
    /*   assigning tmsi zone                    */
    uint32    TMSI_CODEsp;     /* tmsi code                               */
    uint32    TMSI_EXP_TIMEsp; /* tmsi expiration timer                   */
    uint16    SIDp[MAX_POSITIVE_SIDS]; /* home system ids stored in NAM   */
    uint16    NIDp[MAX_POSITIVE_SIDS]; /* home network ids stored in NAM  */
    uint16    NEG_SIDp[MAX_NEGATIVE_SIDS]; /* negative system ids stored in NAM */
    uint16    NEG_NIDp[MAX_NEGATIVE_SIDS]; /* negative network ids stored in NAM */
    uint8     MAX_SID_NID;     /* Max sid/nid pairs that can be stored in NAM*/
    uint8     STORED_POS_SID_NID;  /* No of Pos sid/nid pairs stored in NAM */
    uint8     STORED_NEG_SID_NID;  /* No of Neg sid/nid pairs stored in NAM */
    uint8     ACCOLCp;         /* access overload class                    */
    uint8     MobTermMap;      /* receive mobile terminated calls          */
    uint8     BCAST_ADDR_LENp; /* broadcast address length (0-not config.) */
    uint8     BCAST_ADDRp[ CP_BCAST_ADDRESS_MAX_SIZE];
                               /* broadcast address data                   */
#ifndef MTK_CBP
    uint32    RESERVED4;       /* Place holder for previous SPCp           */
#else
    uint32    SPCp;       /* Place holder for previous SPCp           */    
#endif
    uint8     OTA_Capability_Map; /* See Bit Map explanation at top of file*/
    uint8     MDN_NUM_DIGITS;
    uint8     Mdn[NAM_MAX_MDN_DIGITS];  /* MDN - Mobile directory number   */

    /* Analog support */
    uint16    HOME_SIDp;       /* home sid for analog system support       */
    uint8     EXp;             /* extended address support for analog      */
    uint16    FIRSTCHPp;       /* first paging channel                     */
    uint8     DTXp;            /* Analog DTX Option                        */
    uint16    FCCA, LCCA;      /* Analog Custom Control Channel Set A      */
    uint16    FCCB, LCCB;      /* Custom Control Channel Set B             */
    uint32    NXTREGsp;
    uint16    SIDsp;
    uint16    LOCAIDsp;
    bool      PUREGsp;

    uint16    CPCA, CSCA;      /* CDMA Primary (and Secondary) Channel A   */
    uint16    CPCB, CSCB;      /* CDMA Primary (and Secondary) Channel B   */
    uint8     RESERVED5[125];  /* Previous Mobile IP parameters            */
    uint8     Auth_Capability_Map; /* See Bit Map Explination at top of file */
    uint8     RESERVED6[89];   /* Previous IS801 Data Call parameters      */
    uint8     SmsTlMaxRetry;
    uint32    UIM_ID;
    uint8     RoamingSetting;  /* Only for Sprint                          */
    uint8     SystemSelect;
    /* Following array reserved for future use. If new Nam parameter 
     * needed then decrement from array below. Total Nam size is always 
     * 560 bytes. */
    uint8     FUTURE_EXPAND_FIELDs[REMAINING_NAM_SIZE];

    /* Checksum support */
    uint16   Checksum;            /* checksum for checksum support         */
} PACKED_POSTFIX  PackedPswIs95NamT;


/* The goal is to use a EXE_SIZE_MSG_BUFF_3 when writing secure data
 * to Dbm.  Hence, 
 * sizeof(SecureDataStructT) + sizeof(DbmWriteMsgT) = EXE_SIZE_MSG_BUFF_3.
 * sizeof(DbmWriteMsgT) = 14 bytes.  EXE_SIZE_MSG_BUFF_3 = 448
 * SecureDataStructT must be 434 bytes.
 * MobileId_Spread_Structure = 33 bytes.
 * Revision + Akey(1&2) + SSD(1&2) + SPC + SIP + AAA + HA + checksum = 112 bytes.
 * SECURE_DATA_RESERVED_SIZE = 434 - 33 - 112 -> 289 bytes.
 */
#define SECURE_DATA_RESERVED_SIZE     289

typedef NV_PACKED_PREFIX struct 
{
    uint32  *SECURITY_LAYER_1;
    uint16   MOBILE_ID_2;
    uint32  *SECURITY_LAYER_2;
    uint16   MOBILE_ID_4;
    uint32  *SECURITY_LAYER_3;
    uint32  *SECURITY_LAYER_4;
    uint16   MOBILE_ID_1;
    uint32  *SECURITY_LAYER_5;
    uint16   MOBILE_ID_3;
    uint32  *SECURITY_LAYER_6;
    uint8   mobileIdType;
} NV_PACKED_POSTFIX  MobileId_Spread_Structure;

typedef PACKED_PREFIX struct 
{
    uint32  *SECURITY_LAYER_1;
    uint16   MOBILE_ID_2;
    uint32  *SECURITY_LAYER_2;
    uint16   MOBILE_ID_4;
    uint32  *SECURITY_LAYER_3;
    uint32  *SECURITY_LAYER_4;
    uint16   MOBILE_ID_1;
    uint32  *SECURITY_LAYER_5;
    uint16   MOBILE_ID_3;
    uint32  *SECURITY_LAYER_6;
    uint8   mobileIdType;
} PACKED_POSTFIX  PackedMobileId_Spread_Structure;



typedef NV_PACKED_PREFIX struct
{
    uint16    Revision;        /* Secure Data Revision ID                  */
    MobileId_Spread_Structure MSID;
    uint32    AKEY_NAM1[2];    /* Binary Authentication Key for CAVE       */
    uint8     SSDA_NAM1[8];    /* Shared Secret Data 'A'                   */
    uint8     SSDB_NAM1[8];    /* Shared Secret Data 'B'                   */
    uint32    AKEY_NAM2[2];    /* Binary Authentication Key for CAVE       */
    uint8     SSDA_NAM2[8];    /* Shared Secret Data 'A'                   */
    uint8     SSDB_NAM2[8];    /* Shared Secret Data 'B'                   */
    uint32    SPCp;            /* Service Programming Code                 */
    uint8     FutureExpansion[SECURE_DATA_RESERVED_SIZE];
    uint16    checkSum;
} NV_PACKED_POSTFIX  SecureDataStructT;

typedef PACKED_PREFIX struct
{
    uint16    Revision;        /* Secure Data Revision ID                  */
    PackedMobileId_Spread_Structure MSID;
    uint32    AKEY_NAM1[2];    /* Binary Authentication Key for CAVE       */
    uint8     SSDA_NAM1[8];    /* Shared Secret Data 'A'                   */
    uint8     SSDB_NAM1[8];    /* Shared Secret Data 'B'                   */
    uint32    AKEY_NAM2[2];    /* Binary Authentication Key for CAVE       */
    uint8     SSDA_NAM2[8];    /* Shared Secret Data 'A'                   */
    uint8     SSDB_NAM2[8];    /* Shared Secret Data 'B'                   */
    uint32    SPCp;            /* Service Programming Code                 */
    uint8     FutureExpansion[SECURE_DATA_RESERVED_SIZE];
    uint16    checkSum;
} PACKED_POSTFIX  PackedSecureDataStructT;


/*--------------------------------------
 * Global variables  
 *--------------------------------------*/
extern PswIs95NamT Nam;

#endif  /* _NAMDATA_H_ */

