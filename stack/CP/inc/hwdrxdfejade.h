/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/
#ifndef _HWDRXDFE_JADE_H_
#define _HWDRXDFE_JADE_H_

/***********************************************************************************
* 
* FILE NAME   :     hwdrxdfejade.h
*
* DESCRIPTION :     RX DFE signal interface defination
*
* HISTORY     :
*     See Log at end of file
*
************************************************************************************/
#if (SYS_BOARD >= SB_JADE)

#ifndef _HWDRFAPI_H_
#include "hwdrfapi.h"
#endif
#ifndef SYSDEFS_H
#include "sysdefs.h"
#endif
#ifndef _HWDMS_H_
#include "hwdmsapi.h"
#endif

/*----------------------------------------------------------------------------
 Global Defines 
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Variables
----------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
void HwdRxDfePocDcInitJade(void *dcoAdsP,HwdRfBandT rfBand);
void HwdRxDfePocIrrInitJade(void *IrrAdsP,HwdRfBandT rfBand);
void HwdRxDfeCfgGainUpJade(void *GainUpAds);
void HwdRxDfeRdGainUpJade(void *GainUpAds);
void HwdRxDfeGainUpSwitchJade(uint16 PathIndex, int16 InputPower, int16 ManualSwitchPower, int16 *GainUpAdj);
void HwdRxDfeCfgDcIirCoefJade(void *dcCoefAds);
void HwdRxDfeCfgDcoInitValueJade(void *dcoCfgAds);
void HwdRxDfeCfgPhaseJumpJade(void  *phaseJumpAds);
void HwdRxDfeCfgStepGainJade(void  *stepGainAds);
void HwdRxDfeCfgAgcDcJade(void  *agcDcAds);
void HwdRxDfeRdDcIirCoefJade(void *dcCoefAds);
void HwdRxDfeRdDigiGainJade(void  *digiGainAds);
void HwdRxDfeRdWbRssiJade(void  *wbRssiAds);
void HwdRxDfeRdDcoValueJade(void  *dcoReadAds);

void HwdRxDfeRdAgcDcJade(void  *agcDcRdAds);
void HwdRxDfeUpdDcoElementJade(void  *updDcoAds);

void HwdRxDfeRdDcoTblJade(HwdRfMpaEnumT path,
                                   HwdRfBandT    band,
                                   uint16        maxRxDcocToRead,
                                   int16        *rxDcocI,
                                   int16        *rxDcocQ);
void HwdRxDfeEnableJade(SysAirInterfaceT   Interface, HwdRfMpaEnumT RfPath);
void HwdRxDfeDisableJade(SysAirInterfaceT   Interface, HwdRfMpaEnumT RfPath);
void HwdRxDfeSwInitJade(void);
void HwdRxDfeMainInitJade(void);
void HwdRxDfeDivInitJade(void);
void HwdRxDfeRfInitJade(void);
void HwdRxDfeRestoreRegistersJade(SysAirInterfaceT Interface);
void HwdRxDfeSourceSelectJade(SysAirInterfaceT  Interface, 
                                    HwdRfMpaEnumT     RfPath, 
                                    HwdIcPathT        ICPath);
void HwdRxDfeResetDcParmsTimerJade(uint32 TimerId);
void HwdRxDfeSetDcOffsetParmsJade(SysAirInterfaceT AirInterfaceType, uint8 RfPath, uint8 Mode, bool CallbackReq);
void HwdRxDfe1xTrafficStartJade( bool InTraffic);
int16 HwdRxDfeGainUpAdjGetJade(uint16 PathIndex);

void HwdRxDfeIqPhaseImbGainOffsetCfgJade(HwdRfRxEnumT rfMpa1rxPath,HwdRfRxEnumT rfMpa2rxPath,HwdRfBandT  band,uint16 RxMode);
void HwdRxDfeNbifMainJade(SysAirInterfaceT  Interface,  bool HwState);
void HwdRxDfeNbifDivJade(SysAirInterfaceT  Interface,  bool HwState);
void HwdRxDfeNbifFoeAdjustJade(SysAirInterfaceT  Interface,  int32 foe);
void HwdRDfeNbif23Disable(void);

#define M_HwdRxDfePocDcInit(a,b)     HwdRxDfePocDcInitJade(a,b)
#define M_HwdRxDfePocIrrInit(a,b)    HwdRxDfePocIrrInitJade(a,b)
#define M_HwdRxDfeCfgGainUp(a)       HwdRxDfeCfgGainUpJade(a)
#define M_HwdRxDfeRdGainUp(a)        HwdRxDfeRdGainUpJade(a)
#define M_HwdRxDfeGainUpSwitch(a,b,c,d) HwdRxDfeGainUpSwitchJade(a,b,c,d)
#define M_HwdRxDfeCfgDcIirCoef(a)    HwdRxDfeCfgDcIirCoefJade(a)
#define M_HwdRxDfeCfgDcoInitValue(a) HwdRxDfeCfgDcoInitValueJade(a)
#define M_HwdRxDfeCfgPhaseJump(a)    HwdRxDfeCfgPhaseJumpJade(a)
#define M_HwdRxDfeCfgStepGain(a)     HwdRxDfeCfgStepGainJade(a)
#define M_HwdRxDfeCfgAgcDc(a)        HwdRxDfeCfgAgcDcJade(a)
#define M_HwdRxDfeRdDcIirCoef(a)     HwdRxDfeRdDcIirCoefJade(a)
#define M_HwdRxDfeRdDigiGain(a)      HwdRxDfeRdDigiGainJade(a)
#define M_HwdRxDfeRdWbRssi(a)        HwdRxDfeRdWbRssiJade(a)
#define M_HwdRxDfeRdDcoValue(a)      HwdRxDfeRdDcoValueJade(a)
#define M_HwdRxDfeRdDcoCali(a)
#define M_HwdRxDfeRdAgcDc(a)         HwdRxDfeRdAgcDcJade(a)
#define M_HwdRxDfeUpdDcoElement(a)   HwdRxDfeUpdDcoElementJade(a)
#define M_HwdRxDfeRdDcoTbl(a,b,c,d,e)HwdRxDfeRdDcoTblJade(a, b, c, d, e)

#define M_HwdRxDfeCfgDcCal(a, b)     
#define M_HwdRxDfeStartDcCal(a, b)   
#define M_HwdRxDfeStopDcCal(a, b)    
#define M_HwdRxDfeDcCalReset(a)      
#define M_HwdRxDfeEnable(a,b)        HwdRxDfeEnableJade(a,b)
#define M_HwdRxDfeDisable(a,b)       HwdRxDfeDisableJade(a, b)
#define M_HwdRxDfeSwInit()           HwdRxDfeSwInitJade()
#define M_HwdRxDfeMainInit()         HwdRxDfeMainInitJade()
#define M_HwdRxDfeDivInit()          HwdRxDfeDivInitJade()
#define M_HwdRxDfeRfInit()           HwdRxDfeRfInitJade()
#define M_HwdRxDfeRestoreRegisters(a)HwdRxDfeRestoreRegistersJade(a)
#define M_HwdRxDfeSourceSelect(a,b,c)HwdRxDfeSourceSelectJade(a, b, c)
#define M_HwdRxDfeResetDcParmsTimer(a)  HwdRxDfeResetDcParmsTimerJade(a)
#define M_HwdRxDfeSetDcOffsetParms(a,b,c,d)   HwdRxDfeSetDcOffsetParmsJade(a,b,c,d)
#define M_HwdRxDfe1xTrafficStart(a)     HwdRxDfe1xTrafficStartJade(a)
#define M_HwdRxDfeGainUpAdjGet(a)        HwdRxDfeGainUpAdjGetJade(a)

int16 HwdRxDfeRxIqGainOrigResultTranToFinlResultQJade(int32 iq_gain_orig_result);
int16 HwdRxDfeRxIqPhaseOrigResultTranToFinlResultJade(int32 iq_phase_orig_result);
int16 HwdRxDfeRxDigDcOrigResultTranToFinlResultJade(int32 dc_orig_result);

#define M_HwdRxDfeRxIqGainOrigResultTranToFinlResultQ(a)  HwdRxDfeRxIqGainOrigResultTranToFinlResultQJade(a)
#define M_HwdRxDfeRxIqPhaseOrigResultTranToFinlResult(a)  HwdRxDfeRxIqPhaseOrigResultTranToFinlResultJade(a)
#define M_HwdRxDfeRxDigDcOrigResultTranToFinlResult(a)    HwdRxDfeRxDigDcOrigResultTranToFinlResultJade(a)

#ifdef MTK_DEV_DUMP_REG
void HwdRxDfeMainRegLogRdAllJade(void);
void HwdRxDfeDivRegLogRdAllJade(void);
void HwdRxDfeRegLogRdPathOnJade(HwdRfMpaEnumT RfPath);
#endif

#ifdef MTK_DEV_DUMP_REG
#define M_HwdRxDfeMainRegLogRdAll()  HwdRxDfeMainRegLogRdAllJade()
#define M_HwdRxDfeDivRegLogRdAll()   HwdRxDfeDivRegLogRdAllJade()
#define M_HwdRxDfeRegLogRdRfInit()
#define M_HwdRxDfeRegLogRdCfgDc(a)
#define M_HwdRxDfeRegLogRdPathOn(a)  HwdRxDfeRegLogRdPathOnJade(a)
#endif

#endif
#endif
/*****************************************************************************
* End of File
*****************************************************************************/
//#endif

/**Log information: \main\Trophy\Trophy_ylxiao_href22060\1 2013-03-26 07:38:04 GMT ylxiao
** HREF#22060, bugfix for DMA**/
/**Log information: \main\Trophy\1 2013-04-03 02:41:06 GMT hzhang
** HREF#22060 to merge code from Tropby 0.3.x**/
/**Log information: \main\Trophy_0.3.X\1 2013-03-26 02:24:09 GMT fwu
** HREF#21981.**/
/**Log information: \main\Trophy\Trophy_fwu_href22082\1 2013-04-03 02:26:28 GMT fwu
** HREF#22082, Modified to support UART1 to be the AT channel between AP and CP.**/
/**Log information: \main\Trophy\2 2013-04-03 02:56:59 GMT hzhang
** HREF#22082 to merge code.**/

