/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
*
* FILE NAME   : do_rcpapi.h
*
* DESCRIPTION : API definition for RCP (Reverse Channel Processing) task.
*
* HISTORY     :
*****************************************************************************/
#ifndef _DO_RCPAPI_H_
#define _DO_RCPAPI_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "do_msgdefs.h"
#include "hwdapi.h"
#include "hwddefs.h"
#include "ipcapi.h"
#include "sysdefs.h"
#include "cpbuf.h"
#include "hlpapi.h"
#include "hwdgpio.h"
#include "hwdrfapi.h"
#include "do_rmcapi.h"
#if (!defined(MTK_PLT_ON_PC))
#include "hwdmsapi.h"
#endif

/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define MAX_STREAM_NUM          4
#define MAX_MAC_FLOW_NUM        4
#define MAX_NUM_DATAPKT_REC     120
#define MAX_NUM_REVMACPKT_REC   10
#define MAX_NUM_TXDMALLD_REC    120
/* Fenix: Changed from 4 to 6, since Ericsson supports 5. */
#define MAX_RLP_FLOW_SUPPORTED       8
#define MAX_RLP_HLP_PKT_NUM       6

#define MAX_FORMAT_B_SESSION_LAYER_LEN   255
#define INVALID_REVMACID        0xFF
#define MAX_REVMACID            0xFE

#define INVALID_REVMACFLOWID        0xFF

#define CONNLAYER_FORMATA       0
#define CONNLAYER_FORMATB       1

#define TXHW_ACCESS_CHAN        0
#define TXHW_REV_TRAFFIC_CHAN   1


/* SysTime Hw */
#define ST_TX_MIL_0_MASK     0x0000FFFF
#define ST_TX_MIL_1_MASK     0xFFFF0000
#define ST_TX_MIL_1_SHIFT    16

/* Macros used for Tx AGC step sizes, Q=8, ALog2 */
#define TXAGC_STEP_SIZE_ONE_DB_ALOG2      (((int32)1 * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16)>>8)

#define TXAGC_STEP_SIZE_HALF_DB_ALOG2     (TXAGC_STEP_SIZE_ONE_DB_ALOG2 >> 1)
#define TXAGC_STEP_SIZE_QUARTER_DB_ALOG2  (TXAGC_STEP_SIZE_ONE_DB_ALOG2 >> 2)

/* Macros used for Tx AGC close loop adj Max/Min, Q=8, ALog2 */
/* closed loop spec (+/- 24dB) + open loop margin (+/- 10 dB) + temp & RPC initial uncertainty (+/- 6dB) = +/- 40 dB */
#define TXAGC_CLOSE_LOOP_ADJ_MAX_ALOG2    (((int32)40 * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16)>>8)

/* Use -34 instead of - 40 dB to fix TD-data rev low throughput issue because Spirent keep AT tx pwr down in this test - see CR#4359 */
#define TXAGC_CLOSE_LOOP_ADJ_MIN_34_ALOG2     (-1 * (((int32)34 * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16)>>8))

/* Use -34 instead of - 40 dB to fix TD-data rev low throughput issue because Spirent keep AT tx pwr down in this test - see CR#4359 */
#define TXAGC_CLOSE_LOOP_ADJ_MIN_25_ALOG2     (-1 * (((int32)25 * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16)>>8))

#define TXAGC_CLOSE_LOOP_ADJ_MIN_45_ALOG2     (-1 * (((int32)45 * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16)>>8))

/* Use -29 for CMU-200 RETAP call */
#define TXAGC_CLOSE_LOOP_ADJ_CMU_RETAP_MIN_ALOG2     (-1 * (((int32)29 * DBM_TO_ALOG2_CONVERSION_FACTOR_Q16)>>8))

/* Macros used for Tx AGC Clipper threshold settings (should be the same as
** clipper settings sent down to the DSPM for 1xRTT operations */
#define RCP_REV_CLIP_MAX_LEVEL         0x03FF   /* <10,2,u> = 3.996 - effectively disables reverse clipping */
#define RCP_REV_CLIP_THRESHOLD_LIMIT   0x007F   /* <7,0,u>  = i.e., fractional ratio - no value greater than one */
#define RCP_REV_CLIP_DEFAULT           0x02c0   /* same as SYS_DSPM_REV_REVMIXSIGPARM3_DEFAULT */

#define RCP_REV_DELAY_LOAD_WINDOW_DEFAULT           0x3C /* delay loading window value: 60 chips */
#define CHIP_OFFSET_BETWEEN_ON_AIR_AND_SLOT_INT     13
/** Define DDPC control mode */
#define DDPC_BYPASS           0   //DDPC disable in normal mode
#define DDPC_OL_AB            1   //DDPC open loop,Delta using Absolute algorithm in normal mode
#define DDPC_OL_REL           2   //DDPC open loop,Delta using Relative algorithm in normal mode
#define DDPC_CL_AB            3   //DDPC closed loop,Delta using Absolute algorithm in normal mode
#define DDPC_CL_REL           4   //DDPC closed loop,Delta using Relative algorithm in normal mode
#define DDPC_TST_ENABLE       6  //DDPC loop enable, used in calibration mode, couplerLoss calibration
#define DDPC_CAL_ENABLE       7  //DDPC loop diable, used in calibration mode, PA calibration

#ifndef _STYPE_H_
#define _STYPE_H_ 1

#ifndef BOOL
#define BOOL bool
#endif
#ifndef BYTE
#define BYTE uint8
#endif
#ifndef WORD
#define WORD uint16
#endif
#ifndef DWORD
#define DWORD uint32
#endif
#ifndef UBYTE
#define UBYTE uint8
#endif

#endif


/*----------------------------------------------------------------------------
 Mailbox IDs
----------------------------------------------------------------------------*/
#define RCP_CMD_MAILBOX         EXE_MAILBOX_1_ID
#define RCP_DATA_MAILBOX        EXE_MAILBOX_2_ID
#define RCP_OTAMSG_MAILBOX      EXE_MAILBOX_3_ID
#define RCP_TEST_MAILBOX        EXE_MAILBOX_4_ID


#ifdef PKT_SIM
/* temp solution for task ID */
typedef enum   /*_Task_ID_define*/
{
   EXE_HSC_ID_23     = 23,  /* DO defines. */
   EXE_SLC_ID_24     = 24,
   EXE_CLC_ID_25     = 25,
   EXE_RMC_ID_26     = 26,  /* DO defines */
   EXE_FCP_ID_27     = 27,
   EXE_RCP_ID_28     = 28,
   EXE_NUM_TASKS_end

}ExeTaskIdT_PktSim;
#endif

typedef enum
{
   RPC_TXAGC_DISABLED = 0,  /* Tx AGC Outer-Loop processing disabled */
   RPC_TXAGC_DO_ACCESS,     /* DO Tx AGC AC Outer-Loop processing enabled */
   RPC_TXAGC_DO_TRAFFIC,    /* DO Tx AGC RTC Outer-Loop processing enabled */
   RPC_TXAGC_1xRTT_REVERSE, /* 1xRTT Tx AGC Outer-Loop processing enabled
                            ** NOTE: (1xRTT RFC control currently not supported) */
   RPC_TXAGC_TEST_MODE      /* Tx AGC Outer-Loop configured for test purposes */

} RpcTxAgcStateT;

typedef enum
{
    DSA,
    DPA_AccessStream = 0x01,
    DPA_ServiceStream,
    TAP_SUBTYPE,
    MFPA_AccessStream = 0x04,
    MFPA_ServiceStream,
    CIR_SRVCS_NOT_3G1X,
    EMFPA_AccessStream = 0x08,
    EMFPA_ServiceStream,
    ETAP_SUBTYPE = 0x0a,
    BTAP,
    NUM_APP_SUBTYPE_SUPPORTED
} AppSubTypeT;

typedef enum
{
    RTM_INACTIVE = 0,
    RTM_SETUP,
    RTM_SETUP_NORF,
    RTM_OPEN,
    RTM_OPEN_NORF,
    RTM_INACTIVE_NORF,
    RTM_SETUP_SILENT,
    RTM_SETUP_NORF_SILENT,
    RTM_OPEN_SILENT,
    RTM_OPEN_NORF_SILENT
} RtmRmmStateT;

typedef enum
{
    /* TX_SETTLE_DOWN_ST means TX is already at opened state or closed state
       and doesn't plan to open or close */
    TX_SETTLE_DOWN_ST = 0,
    TXPATH_WAIT_FOR_SET_ST,
    TXPATH_WAIT_FOR_RELEASE_ST,
    TXAGC_WAIT_FOR_ON_ST,
    TXAGC_WAIT_FOR_OFF_ST,
    TX_INVALID_ST
} RtmTxStateT;

typedef enum
{
    RTM_TX_PATH_RELEASE_EV = 0,
    RTM_TX_PATH_SET_EV,
    RTM_RUP_ACTIVATE_EV,
    RTM_RUP_DEACTIVATE_EV,
    RTM_TXAGC_OFF_EV,
    RTM_TXAGC_ON_EV,
    RTM_RF_RELEASE_EV,
    RTM_RF_READY_EV,
    RTM_RTC_ACK_EV,
    RTM_SILENT_ENTRY_EV,
    RTM_SILENT_EXIT_EV,
    RTM_RCP_TASK_SIG_EV,
    RTM_INVALID_EV
} RtmEvent;

/*----------------------------------------------------------------------------
     Command Message IDs, for RCP task, for RCP_CMD_MAILBOX, EXE_MAILBOX_1_ID
     The message IDs for components shall also be put in here.
----------------------------------------------------------------------------*/
#include "do_rcpmsg.h"


/*----------------------------------------------------------------------------
     OTA Message IDs, for RCP_OTAMSG_MAILBOX
----------------------------------------------------------------------------*/
typedef enum /*_Message_ID_define*/
{
   /* ACM_OTA_SIGNALING_MSG =  RCP_OTAMSG_MSGID_START, */
   ACM_ACK_MSG,                       /* 0x00 */
   ACM_ACCESS_PARMS_MSG,    /* 0x01 */
   ACM_ATTRIBUTE_UPDATE_REQUEST_MSG = 0x52,
   ACM_ATTRIBUTE_UPDATE_ACCEPT_MSG,
   ACM_ATTRIBUTE_UPDATE_REJECT_MSG,

   RCP_OTA_MSG_MSGID_LAST
} RcpOTAMsgT;


/*----------------------------------------------------------------------------
     RCP Test Message IDs, for RCP_TEST_MAILBOX
----------------------------------------------------------------------------*/
typedef enum /*_Message_ID_define*/
{
    /* RCP_ETS_TX_AGC_GAIN_SET_MSG =  RCP_TEST_MSGID_START, */
#if defined (MTK_PLT_ON_PC)
    RCP_ETS_TX_AGC_GAIN_SET_MSG,
    RCP_ETS_TX_AGC_TEST_MODE_MSG,
#endif
    RCP_ETS_GET_GAIN_SCALE_MSG,
    RCP_ETS_SET_GAIN_SCALE_MSG,
    RCP_ETS_GET_TX_GAIN_DELAY_VARS_MSG,
    RCP_ETS_SET_TX_GAIN_DELAY_VARS_MSG,
    RCP_ETS_TEST_TX_GAIN_SWITCH_MSG,
    RCP_ETS_GET_TX_CLIP_LEVEL_MSG,
    RCP_ETS_SET_TX_CLIP_LEVEL_MSG,
    RCP_ETS_GET_DELAY_LOAD_WINDOW_ST_MATCH_MSG,
    RCP_ETS_SET_DELAY_LOAD_WINDOW_ST_MATCH_MSG,
    RCP_ETS_SET_HW_TRIGGER_MSG,
    RCP_ETS_GET_TX_AGC_CL_RANGE_MSG,
    RCP_ETS_SET_TX_AGC_CL_RANGE_MSG,
    RCP_ETS_GET_TX_AVAILABLE_PWR_MARGIN_MSG,
    RCP_ETS_SET_TX_AVAILABLE_PWR_MARGIN_MSG,

    RCP_ETS_TX_AVAILABLE_PWR_TEST_MODE_MSG,

    RCP_ETS_GET_TX_AGC_HYST_POINT_CHANGE_MSG,
    RCP_ETS_SET_TX_AGC_HYST_POINT_CHANGE_MSG,

    RCP_ETS_RTAP_PERF_TEST_MODE_MSG,

#if (!defined(MTK_PLT_ON_PC))
    RCP_ETS_TX_AGC_CFG_MTK_MSG,
    RCP_ETS_TX_CDMA_CTRL_MTK_MSG,
    RCP_ETS_TX_POWER_GET_MTK_MSG,
    RCP_ETS_TX_POWER_DDPC_LOOP_MSG,
#endif


    /* RTM Test Msgs */
    RTM_TEST_CFG_MSG,
    RTM_TEST_MBPMACBITSOUTPUT_MSG,
    RTM_TEST_MFCGRANTCAL23_MSG,
    RTM_TEST_PARM_GET_MSG,
    RTM_TEST_PARM_SET_MSG,
    RTM_TEST_TXCHANSTAT_GET_MSG,

    /* ACM Test Msgs */
    ACM_TEST_CFG_MSG,
    ACM_TEST_ACACK_MSG,

    /* PHY CHAN Test Msgs */
    RCP_PHY_ACH_TEST_MSG,
    RCP_PHY_TCH0_TEST_MSG,
    RCP_PHY_TCH2_TEST_MSG,
    RCP_TXDMA_TEST_CFG_MSG,
    RCP_TXDMA_TEST_START_MSG,
    RCP_TXDMA_TEST_READ_MSG,
    RCP_PHY_TCH_TEST_DATA_WRITE_MSG,
    RCP_PACKET_PERF_STAT_RESET_MSG,
    RCP_RTM_QOS_TXMODE_SELECTION_MSG,

    /* CpBuf Test Msgs */
    SYS_CPBUF_TEST_GET_CMD_MSG,
    SYS_CPBUF_TEST_FREE_CMD_MSG,
    SYS_CPBUF_TEST_STATS_CMD_MSG,

    TAP_TEST_INIT_MSG,
    TAP_CONN_CLOSE_MSG,

    FTAP_TEST_INIT_MSG,
    FTAP_CONN_CLOSE_MSG,

    ETS_SET_STRAPP_MSG,
    ETS_MFPA_SET_SIMPLE_ATTR,
    ETS_MFPA_SET_FlowNNIdentification_Attr,
    ETS_MFPA_SET_FlowNNReservation_Attr,
    ETS_MFPA_SET_FlowNNTimers_Attr,
    ETS_MFPA_SET_SupportedHLProtocols_Attr,
    ETS_MFPA_SET_ATSupportedQoSProfiles_Attr,
    ETS_MFPA_READ_SUBSCRIB_QOS_PROFILE,
    ETS_MFPA_IPFlow_Request_Attr,
    ETS_MFPA_SET_CURLABEL_Attr,
    ETS_MFPA_Delete_Flow,
    ETS_MFPA_PPP_OPEN,
    ETS_MFPA_RLP_STAT_RESET_MSG,
    ETS_MFPA_RLP_STAT_PEEK_MSG,
    ETS_MFPA_SEND_DOS_MSG,

    ETS_DSAR_MSG_BLK_MSG,
    RCP_TEST_MSG_MSGID_LAST

} RcpTestMsgT;

/* DecodeMessageResult*/
typedef enum
{
   RCP_DECODE_MSG_OK,
   RCP_DECODE_MSG_DUP,  /*duplicate message, discard but not print error*/
   RCP_DECODE_MSG_ERROR
} RcpDecodeMsgResultE;

/*----------------------------------------------------------------------------
     define signals used by RCP task
----------------------------------------------------------------------------*/
#define RCP_CMD_MAILBOX_EVENT           EXE_MESSAGE_MBOX_1   /* EXE_MAILBOX_1 */
#define RCP_DATA_MAILBOX_EVENT          EXE_MESSAGE_MBOX_2   /* EXE_MAILBOX_2 */
#define RCP_OTAMSG_MAILBOX_EVENT        EXE_MESSAGE_MBOX_3   /* EXE_MAILBOX_3 */
#define RCP_TEST_MAILBOX_EVENT          EXE_MESSAGE_MBOX_4   /* EXE_MAILBOX_4 */

#define RCP_TASK_SIGNAL                 EXE_SIGNAL_1
#define ACM_PREAMBLE_SIGNAL             EXE_SIGNAL_2
#define ACM_DATA_SIGNAL                 EXE_SIGNAL_3
#define ACM_PST_DELAY_SIGNAL            EXE_SIGNAL_4
#define ACM_INTER_SEQUENCE_SIGNAL       EXE_SIGNAL_5
#define PAR_HLP_DATA_TX_SIGNAL          EXE_SIGNAL_6
#define PAR_HLP_DATA_TX2_SIGNAL         EXE_SIGNAL_7
#define PAR_PHYMISSEDLIST_UPDATE_SIGNAL EXE_SIGNAL_8
#define ACM_TXOFF_CB_SIGNAL             EXE_SIGNAL_9
#define ACM_TXON_CB_SIGNAL              EXE_SIGNAL_10
#define RTM_TXAGC_ON_CB_SIGNAL          EXE_SIGNAL_11
#define RTM_TXPATH_REL_CB_SIGNAL        EXE_SIGNAL_12
/*----------------------------------------------------------------------------
     Message Formats structure
----------------------------------------------------------------------------*/
/* RCP_ETS_TX_AGC_GAIN_SET_MSG */
typedef enum
{
   RCP_TXAGC_MODE_AUTO = 0,
   RCP_TXAGC_MODE_DISABLED,
   RCP_TXAGC_MODE_MANUAL,
   RCP_TXAGC_MODE_FIXPWR,
   RCP_TXAGC_MODE_NORPWR
} RcpTxAgcTstModeT;

/** Define TX Calibration Type */
typedef enum
{
   RCP_TXCAL_TYPE_PILOT = 0,
   RCP_TXCAL_TYPE_ST2
}RcpTxCalTypeT;

typedef enum
{
   RCP_TXAGC_METHOD_DACVAL = 0,
   RCP_TXAGC_METHOD_DBGAIN

} RcpTxAgcTstMethodT;

typedef PACKED_PREFIX struct
{
   uint8  ControlMode;  /* Tx AGC control mode (i.e., auto, disabled, or manual */
   uint8  Method;       /* Tx AGC method using specified Hardware DAC value or dB gain */
   uint16 HwValue;      /* Hardware DAC value */
   uint8  TxGainState;  /* Tx AGC gain state */
   int16  PowerdBm;     /* Power in dBm, Q=7 */

} PACKED_POSTFIX  RcpEtsTxAgcGainSetMsgT;

/* RCP_ETS_TX_AGC_TEST_MODE_MSG */
typedef enum
{
   RCP_TXAGC_POWER_BASE_TRAFFIC_CELL = 0,
   RCP_TXAGC_POWER_BASE_TRAFFIC_PCS,
   RCP_TXAGC_POWER_BASE_SPECIFIED

} RcpTxAgcPowerBaseTypeT;

typedef struct
{
   uint8  TestMode;
   uint8  TxPath;
} RcpEtsTxAgcTestModeMsgT;

/* RCP_ETS_GET_GAIN_SCALE_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8      RevChanScale;

} PACKED_POSTFIX  RcpEtsGetGainScaleMsgT;

typedef PACKED_PREFIX struct
{
   uint8  RevChanScale;
   uint8  Mode;
   int16  GainValueDb;     /* Gain in dB, Q=7 */
   uint16 GainValueLinear; /* Linear Gain, Q=7 */
   uint16 RegValue;

} PACKED_POSTFIX  RcpEtsGetGainScaleRspT;

/* RCP_ETS_SET_GAIN_SCALE_MSG */
typedef enum
{
   RCP_REV_GAIN_SCALE_DB = 0,
   RCP_REV_GAIN_SCALE_LINEAR,
   RCP_REV_GAIN_SCALE_REGISTER

} RcpEtsGainScaleFormatT;

typedef PACKED_PREFIX struct
{
   uint8  RevChanScale;
   uint8  Mode;
   uint8  ScaleFormat;
   int16  GainValueDb;     /* Gain in dB, Q=7 */
   uint16 GainValueLinear; /* Linear Gain, Q=7 */
   uint16 RegValue;

} PACKED_POSTFIX  RcpEtsSetGainScaleMsgT;

/* RCP_ETS_GET_TX_GAIN_DELAY_VARS_MSG */
typedef enum
{
   RCP_TX_ON0_CTRL = 0,
   RCP_TX_ON1_CTRL,
   RCP_TX_ON2_CTRL,
   RCP_TX_ON3_CTRL,
   RCP_TX_ON4_CTRL,
   RCP_TX_ON5_CTRL,
   RCP_TX_ON6_CTRL,
   RCP_TX_ON7_CTRL,
   RCP_PDM0_CTRL,
   RCP_PDM1_CTRL,
   RCP_PDM2_CTRL,
   RCP_PDM3_CTRL,
   RCP_PDM4_CTRL,
   RCP_PDM5_CTRL,
   RCP_RFSPI_CTRL,

   RCP_NA_CTRL     /* Non-applicable delayed load hardware for control unit */

} RcpDelayHwUnitT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   uint8      DelayCtrlUnit;

} PACKED_POSTFIX  RcpEtsGetTxGainDelayVarsMsgT;

typedef PACKED_PREFIX struct
{
   uint8  DelayCtrlUnit;
   uint8  RevProtocol;
   uint8  DelayHw;
   uint16 LowToMedValSlot;
   uint16 MedToLowValSlot;
   uint16 MedToHighValSlot;
   uint16 HighToMedValSlot;
   uint16 LowToMedValHslot;
   uint16 MedToLowValHslot;
   uint16 MedToHighValHslot;
   uint16 HighToMedValHslot;
   int16 RcpSlotGainOffset;
   int16 RcpHslotGainOffset;

} PACKED_POSTFIX  RcpEtsGetTxGainDelayVarsRspT;

/* RCP_ETS_SET_TX_GAIN_DELAY_VARS_MSG */
typedef PACKED_PREFIX struct
{
   uint8  DelayCtrlUnit;
   uint8  Mode;
   uint8  RevProtocol;
   uint8  DelayHw;
   uint16 LowToMedValSlot;
   uint16 MedToLowValSlot;
   uint16 MedToHighValSlot;
   uint16 HighToMedValSlot;
   uint16 LowToMedValHslot;
   uint16 MedToLowValHslot;
   uint16 MedToHighValHslot;
   uint16 HighToMedValHslot;
   int16 RcpSlotGainOffset;
   int16 RcpHslotGainOffset;

} PACKED_POSTFIX  RcpEtsSetTxGainDelayVarsMsgT;

/* RCP_ETS_TEST_TX_GAIN_SWITCH_MSG */
typedef PACKED_PREFIX struct
{
   uint8  TxGainState;    /* Tx AGC gain state */
   int16  PowerdBm;       /* Power in dBm, Q=7 */
   uint8  TxLoadBoundary; /* RcpTxLoadBoundaryT */

} PACKED_POSTFIX  RcpEtsTestTxGainSwitchMsgT;

/* RCP_ETS_GET_TX_CLIP_LEVEL_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;

} PACKED_POSTFIX  RcpEtsGetTxClipLevelMsgT;

typedef PACKED_PREFIX struct
{
   uint16 TxClipLevel;

} PACKED_POSTFIX  RcpEtsGetTxClipLevelRspT;

/* RCP_ETS_SET_TX_CLIP_LEVEL_MSG */
typedef PACKED_PREFIX struct
{
   uint16 TxClipLevel;

} PACKED_POSTFIX  RcpEtsSetTxClipLevelMsgT;

/* RCP_ETS_GET_DELAY_LOAD_WINDOW_ST_MATCH_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;

} PACKED_POSTFIX  RcpEtsGetTxDelayLoadWindowMsgT;

typedef PACKED_PREFIX struct
{
   uint16 TxDelayLoadWindow;

} PACKED_POSTFIX  RcpEtsGetTxDelayLoadWindowRspT;

/* RCP_ETS_SET_DELAY_LOAD_WINDOW_ST_MATCH_MSG */
typedef PACKED_PREFIX struct
{
   uint16 TxDelayLoadWindow;

} PACKED_POSTFIX  RcpEtsSetTxDelayLoadWindowMsgT;

/* RCP_ETS_GET_TX_AGC_CL_RANGE_MSG*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;

} PACKED_POSTFIX  RcpEtsGetTxAgcCLRangeMsgT;

typedef PACKED_PREFIX struct
{
   uint8 UpLimit;                      /* DB */
   uint8 DownLimit;
} PACKED_POSTFIX  RcpEtsGetTxAgcCLRangeRspT;


/* RCP_ETS_SET_TX_AGC_CL_RANGE_MSG*/
typedef PACKED_PREFIX struct
{
   uint8 UpLimit;                      /* DB */
   uint8 DownLimit;
   bool UseC2IThr4DownLimit;
   uint16 C2IThr4DownLimit;                   /* Q9, 2's comp, Linear */
} PACKED_POSTFIX  RcpEtsSetTxAgcCLRangeMsgT;

/* RCP_ETS_GET_TX_AGC_HYST_POINT_CHANGE_MSG*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;

} PACKED_POSTFIX  RcpEtsGetTxAgcHystPointMsgT;

typedef PACKED_PREFIX struct
{
   int8 UpperThr;                  /* DB */
   int8 LowerThr;
} PACKED_POSTFIX  RcpEtsGetTxAgcHystPointRspT;


/* RCP_ETS_SET_TX_AGC_HYST_POINT_CHANGE_MSG*/
typedef PACKED_PREFIX struct
{
   int8 UpperThr;
   int8 LowerThr;
} PACKED_POSTFIX  RcpEtsSetTxAgcHystPointMsgT;

/* RCP_ETS_RTAP_PERF_TEST_MODE_MSG */
typedef PACKED_PREFIX struct
{
   bool TestMode;
} PACKED_POSTFIX  RcpEtsTxPerfTestModeMsgT;

/* RCP_ETS_GET_TX_AVAILABLE_PWR_MARGIN_MSG*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;

} PACKED_POSTFIX  RcpEtsGetTxAvailablePwrMarginMsgT;

typedef PACKED_PREFIX struct
{
   int16 PwrMarginQ7;                      /* Q7 DB */
} PACKED_POSTFIX  RcpEtsGetTxAvailablePwrMarginRspT;


/* RCP_ETS_SET_TX_AVAILABLE_PWR_MARGIN_MSG*/
typedef PACKED_PREFIX struct
{
   int16 PwrMarginQ7;                      /* Q7 DB */
} PACKED_POSTFIX  RcpEtsSetTxAvailablePwrMarginMsgT;

/* RCP_ETS_TX_AVAILABLE_PWR_TEST_MODE_MSG*/
typedef enum
{
   TX_AVAILABLE_PWR_NORMAL_MODE = 0,
   TX_AVAILABLE_PWR_NO_ACK_MODE,
   TX_AVAILABLE_PWR_NO_ACK_DSC_MODE
} RcpTxAvailPwrTestModeT;

typedef PACKED_PREFIX struct
{
   RcpTxAvailPwrTestModeT Mode;
} PACKED_POSTFIX  RcpEtsTxAvailablePwrTestModeMsgT;


/* RCP_ETS_SET_HW_TRIGGER_MSG*/
typedef PACKED_PREFIX struct
{
   uint8 Trigger;  /* Disable, Access, Traffic*/
   uint8 FrameDelay;
   uint16 LongCodeMIL0;  /* non-zero is for long code mask override*/
   uint16 LongCodeMIL1;
   uint16 LongCodeMIH0;
} PACKED_POSTFIX  RcpEtsSetHwTriggerMsgT;
/* HW trigger type */
typedef enum
{
   HW_TRIGGER_DISABLE=0,
   HW_TRIGGER_ACCESS,
   HW_TRIGGER_TRAFFIC
} RcpHwTriggerT;

/* array size for long code mask */
#define RCP_LONG_CODE_MASK_SIZE     6

typedef PACKED_PREFIX struct
{
    uint16 frameOffset;
    uint16 prePilotScale;         /* 9,3,u */
    uint16 pilotScale;              /* 9,3,u */
    uint16 rriDataAck0;           /* Access Rate 1:9.6k, 2:19.2k, 3:38.4k */
    uint16 dataScale0Ack;       /* 11,6,u */
    uint16 sDScale1;               /* 4,3,t */
    uint16 sDScale2;               /* 4,3,t */
    uint32 frameData[32];       /* 2 frame data 512 bits */
    uint16 bbCaptureFrame;
    uint16 testCase;
} PACKED_POSTFIX RcpPhyAchTestMsgT;

typedef PACKED_PREFIX struct
{
    uint16 pilotScale;              /* 9,3,u */
    uint16 dRCGating;
    uint16 dRCLen;
    uint16 dRCScale;
    uint16 ackSUPScale;
    uint16 ackMUPScale;
    uint16 rriDataAck1[4];           /* Access Rate 1:9.6k, 2:19.2k, 3:38.4k */
    uint16 dataScale0Ack[4];       /* 11,6,u per frame */
    uint16 sDScale1[4];               /* 4,3,t */
    uint16 sDScale2[4];               /* 4,3,t */
    uint16 dRCCover;
    uint16 dRCValue;
    uint16 rRIScaleAck;
    uint16 bbCaptureFrame;
    uint16 testCase;
} PACKED_POSTFIX RcpPhyTch01TestMsgT;

typedef PACKED_PREFIX struct
{
    uint16 pilotScale;              /* 9,3,u */
    uint16 dRCGating;
    uint16 dRCLen;
    uint16 dRCScale;
    uint16 ackSUPScale;
    uint16 ackMUPScale;
    uint16 rriDataAck2[12];
    uint16 rriDataNak2[12];
    uint16 dataScale0Ack[12];       /* 11,6,u per frame */
    uint16 dataScale0Nak[12];       /* 11,6,u per frame */
    uint16 dataScale1Ack[12];       /* 11,6,u per frame */
    uint16 dataScale1Nak[12];       /* 11,6,u per frame */
    uint16 dataScale2Ack[12];       /* 11,6,u per frame */
    uint16 dataScale2Nak[12];       /* 11,6,u per frame */
    uint16 dataScale3Ack[12];       /* 11,6,u per frame */
    uint16 dataScale3Nak[12];       /* 11,6,u per frame */
    uint16 dataScaleAck[12];       /* 11,6,u per frame */
    uint16 revAckData[12];
    uint16 twoBitAckNak[12];
    uint16 dRCLenBoost[12];
    uint16 dSCLenBoost[12];
    uint16 fwdAckData[48];
    uint16 rRIScaleAck;
    uint16 rRIScaleNak;
    uint16 dSCScale;
    uint16 dRCScaleBoost;
    uint16 dSCScaleBoost;
    uint16 auxPilotMinPayload;
    uint16 auxPilotScale;
    uint16 bbCaptureFrame;
    uint16 testCase;
} PACKED_POSTFIX RcpPhyTch2TestMsgT;

typedef PACKED_PREFIX struct
{
    uint32 offsetAddr;
    uint32 dataLen;
    uint32 testData[128];
} PACKED_POSTFIX RcpPhyTch01TestDataWriteMsgT;

typedef PACKED_PREFIX struct
{
    uint32 lldAddress;
    uint32 srcAddress;
    uint32 destAddress;
    uint32 nextLldAddress;
    uint8 srcData[32];
    uint16 dataLen;
    uint16  ackNak;
    uint16  firstLld;
    uint16  width;
} PACKED_POSTFIX RcpTxDMATestMsgT;

typedef PACKED_PREFIX struct
{
    bool   ackNak;
} PACKED_POSTFIX RcpTxDMATestStartMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint16            NumWords;
   uint32            Address;
} PACKED_POSTFIX  RcpTxDmaReadMsgT;

typedef PACKED_PREFIX struct
{
   uint8            Data[1];
} PACKED_POSTFIX  RcpTxDmaReadRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16  slot_no;

   int16  ksd_scale_1st;
   int16  ksd_scale_2nd;
   int16  ksd_log2_1st;
   int16  ksd_log2_2nd;
   int16  ksd_index_1st;
   int16  ksd_index_2nd;
} PACKED_POSTFIX  RcpTxKsDataT;

typedef PACKED_PREFIX struct
{
  uint32 TransATI;
  uint8   TransATIType;
} PACKED_POSTFIX AcmAmpATIListMsgT;

typedef PACKED_PREFIX struct
{
   bool    bUpdated;
   uint16  AccessSignature;
   uint8   ColorCode;
   uint32  SectorId;
   uint8    ReverseLinkSilencePeriod;
   uint8    ReverseLinkSilenceDuration;
} PACKED_POSTFIX AcmOmpUpdatedMsgT;

typedef PACKED_PREFIX struct
{
   uint8  reason;     /* 0--normal; 1--after preempt; */
} PACKED_POSTFIX AcmRupActivateMsgT;

typedef PACKED_PREFIX struct {
  uint8   action;    /* 0--reset msg queue; 1--stop current msg; 2--*/
} PACKED_POSTFIX AcmProtStopAccessMsgT;


/* ETS ACM Test Cmd */
typedef PACKED_PREFIX struct
{
   uint8  AcmTestMode;
   uint8  AccessDataRate;
   uint8  AccessCycleDuration;
   uint8  PreambleLength;
   uint8  ProbeSequenceMax;
   uint8  ProbeNumStep;
   uint8  ColorCode;
   uint32  SectorId;
   uint8  RpcTestMode;
} PACKED_POSTFIX AcmTestCfgMsgT;

typedef PACKED_PREFIX struct
{
   BOOL  TxStatus;

} PACKED_POSTFIX RtmDrcTxControlMsgT;


#if (!defined(MTK_PLT_ON_PC))

/** define Tx AGC configure message*/
typedef PACKED_PREFIX struct
{
   /* Manual for Cal*/
   uint8   CtrlMode;

   /**  DDPC contrl mode */
   uint16  DdpcCtrl;

   /** Bypass Tx pre-burst calibration */
   uint16  ByPassPreBusrtCal;

   /* Tx Power in dBm, Q6*/
   int16   TxPwr;

   /* bit2~bit0: PA mode,        for Denali & Jade : 0: high, 1: middle, 2: low
    * bit4~7: Pa Section index,  for Denali : no use
    *                            for JADE   : 0~7 section index
    */
   uint8   PaMode;

   /* PA gain in dBm, Q6*/
   int16   PaGain;

   /* 0/1*/
   uint8   Vm1;

   /*0/1*/
   uint8   Vm2;

   /*Coupler loss  in dB, Q6*/
   int16 CouplerLoss;

   /* DAC for voltage*/
   uint16   VoltctrlDac;

   /* Response info*/
   ExeRspMsgT  RspInfo;
} PACKED_POSTFIX  RcpEtsTxAgcCfgMtkMsgT;

/** define get Tx power message*/
typedef PACKED_PREFIX struct
{
   /* Response info*/
   ExeRspMsgT  RspInfo;

} PACKED_POSTFIX  RcpEtsTxPowerGetMtkMsgT;

/** define power response message*/
typedef PACKED_PREFIX struct
{
   /* Tx power in dBm, Q6*/
   int16 TxPwr;

} PACKED_POSTFIX  RcpEtsTxPowerGetRspMtkMsgT;

/** define Tx CDMA channel contorl message*/
typedef PACKED_PREFIX struct
{
   /* Enable/disable TxH*/
   uint8   Action;

 /* Response info*/
   ExeRspMsgT  RspInfo;

} PACKED_POSTFIX  RcpEtsTxCdmaCtrlMtkMsgT;

#endif


/*----------------------------------------------------------------------------
 Global typedefs
----------------------------------------------------------------------------*/

/* RCP Mode */
typedef enum
{
   RCP_INACTIVE=0,
   RCP_ACCESS,
   RCP_TRAFFIC,
   RCP_TX_CHAN_SWEEP
} RcpModeT;


/* RCP Component Protocol Types */
typedef enum
{
   RCP_ACM=2,
   RCP_RTM=4
} RcpProtocolTypeT;

typedef enum
{
 AccessChannel,
 TrafficChannel,
 AccessTrafficChannel
}RevChannelTypeT;

typedef  struct {
  uint8 pktpriority;
  uint32 timestamp;
} BestPktInfoT;


/* Grant Allocation */
/* The Grant Allocation consists of the flows which are allocated for this grant,
   for each subframe (phy2) or frame (phy0/1).
   Each flow's macFlowId and the grantSize in bytes are calculated by the reverse
   MAC and given to PCP and higher layer applications to fulfill the grant.
   Note that each grant is identified by a revMacId when the grant is allocated. */

typedef struct
{
   uint8  macFlowID;
   uint16 grantByteSize;
} GrantAllocationT;

typedef struct
{
   uint8 NumFlowsRcvingThisGrant;
   uint8 revMacId;
   GrantAllocationT Grants[ MAX_MAC_FLOW_NUM];
} PcpAcmRtmGrantAllocationT;



/* Datapkt */
/* This is a data pkt which contains a pointer to a chunk of cpbuffer.
   The offset and len specifies the location of this data pkt */
typedef struct datapkt
{
#ifdef PKT_SIM
   BestPktInfoT pktinfo;  // SS: Added this here. It will save lot of test-code
   uint8 pktSrc;          // SS: helps debugging for-B pkts. Identifies, the App-Q
                          // from which the pkt was assembled from.
#endif
   uint16      len;/* The length of this datapkt.  */
   uint16      offset;/* The starting location for the datapkt.  */
   CpBufferT   *buffer;/* The buffer which makes up this datapkt. This is a ptr */
                                /* to a single cpbuffer in the cpbuffer pool.  */
   struct datapkt *next;
} DatapktGT;


/* Datapktlist */
/* A List descriptor which keeps track of the DataPkt queue params */
typedef struct
{
   uint8        count;
   uint8        hdrcount;           /* The number of hdr pkts */
   uint8        connLayerFormat;    /* Format A/B   */
   uint32       totalLen;           /* Total length of all the Datapkts in this linked-list*/
   DatapktGT    *head;
   DatapktGT    *tail;
   DatapktGT   *lastHdr;
} DatapktlistGT;



/* RevMacPkt */
/* The reverse MAC Pkt that is sent every frame/subframe, consisting of a DatapktList
   of security/conn layer packets */
typedef struct revMacPktGT
{
   DatapktlistGT    pktList;        /* linked-list of machdr, sechdr, connlayer pkts, sectrailer, mactrailer */
   uint8            revMacPktId;    /* used for correlating the revMacPkt with the app pkt sent */

   uint8            revRate;        /* For prog RRIData: RTM0/1/2=rateIndex */
   uint8            revPktSz;       /* For prog RRIData: RTM3=pkSzIndex */
   uint8            macLayerFormat; /* For prog MAC trailer */
                                    /* RTM3: This will be the Transmission Mode:
                                       1-LowLatency, 0-HighCapacity. */

   uint16           dataGainScale;  /* RTM0/1=(dataOffNom+dataOffxx) -> from SCM cfg data *//* store as Q7 */
                                    /* RTM2/3=(T2Pxxx): MFC will prov this each time */ /* store as Q5 */

   /* Note: RTM2/3=>The MFC will recalc this for old/new pkts based on T2P, since it will be diff for each subpkt */
   uint16           dataGainScale0; /* RTM3 only */
   uint16           dataGainScale1; /* RTM3 only */
   uint16           dataGainScale2; /* RTM3 only */
   uint16           dataGainScale3; /* RTM3 only */
   uint16           rriGainScale;   /* RTM3 only: RRI gain assoc with the revRate */ /* store as Q6 */
                                    /* MFC will provide this for old/new pkt, since it dep on subpktN */

   struct revMacPktGT  *next;
} RevMacPktGT;

typedef struct RevMacPktListGT
{
   RevMacPktGT  *head;
   RevMacPktGT  *tail;
   uint32 count;

}RevMacPktListGT;


/* 1X Slotted Overlap */
typedef struct
{
   bool      valid;
   FrameRecT startFrame1x;  /* 1X frame in which 1X slotted wakes up */ /* TODO: need to convert these to the DO times */
   uint8     startSlot1x;   /* 1X slot in which 1X slotted wakes up */
   FrameRecT startFrame;    /* DO frame in which 1X slotted overlap start slot overlaps */
   uint8     startSubFrame; /* DO subframe in which 1X slotted overlap start slot overlaps. from 0-3 */
} Rcp1XSlotOverlapT;


/* Sent by the SCP ==> RCP, after session negotiation. */
/* It is going to be populated with the Phy-SubType information after it is negotiated. */
typedef struct
{
  uint8 phySubType;

} RcpSubTypeInfoT;



/*----------------*/
/* TxH Structures */
/*----------------*/
/* Define RCP Test Mode Flags */
typedef struct
{
   bool TxAgcConfigTestMode;
   bool TxAgcGainTestMode;

#if (!defined(MTK_PLT_ON_PC))
   uint16     DdpcCtrl;
   bool       ByPassPreBusrtCal;
   bool       FixTxPwrInd;
   bool       FixNorTrans;
   bool       FixRatiInd;
   bool       TxagcImmedModeInd;
   /** When seeting TRUE, In order to workaround delayloader mode in bringup period for debug**/
   bool       BypassDeltaGainComp;
   /** 0: Normal Transmit power, 1: Fixed Transmit power */
   bool       PrwStatus;
   /** Indicates whether support multiple channel calibration in ST2 */
   bool       TxCalSt2Ind;

   /** EVDO Cal type: Pilot only or ST2 mutiple Channel */
   int8       CalType;

   /** The DDPC meas Tx power, in 1/32 dB */
   int16      DdpcMeasTxPower;

#if (SYS_BOARD >= SB_JADE)
   /** The DDPC meas Tx power, in 1/32 dB */
   int16      DdpcMeasTxPowerAve;
#endif

   /** The Actual Request Tx power after DDPC loop, in 1/32 dB */
   int16      ActualTargetTxPower;

   /** The Target Tx power, in 1/32 dB */
   int16      TargetTxPower;

   /** DDPC output*/
   int16      DdpcOutput;

   /** DDPC meas counter*/
   int16     DdpcMeasCnt;

   /** The digital gain GBB1 value, in 1/32 dB */
   int16      Gbb1;
   /** The residual analog gain that should be applied in digital gain GBB0, in 1/32 dB. */
   int16      Gbb0;

   /** The coupler loss, unit is 1/32dB. */
   int16      CouplerLoss;

   /** The TxDet Gain, L1D should minus it from DDPC result, unit is 1/32dB. */
   int16      TxDetGain;

   /** The reference power output PA without feq/temp compensation, in 1/32 dBm. */
   int16      PaGain;

   /* bit2~bit0: PA mode,        for Denali & Jade : 0: high, 1: middle, 2: low
    * bit4~7: Pa Section index,  for Denali : no use
    *                            for JADE   : 0~7 section index
    */
   uint16     PaMode;

   /** The PA control RFGPOs level */
   uint16     PaVm0;
   uint16     PaVm1;

   /** PA BUCK supply voltage level, unit is mv*/
   uint16     PaVdd;

   /* Response info*/
   ExeRspMsgT RspInfo;
#endif
} RcpTestModesT;

/* Generic Tx AGC Data structure used for tracking AGC variables and providing periodic
** ETS Tx AGC measurement reports - can be used regardless of active air interface */
typedef struct
{
   uint16  TxAgcMode;           /* Tx AGC Mode of operation:  Bryan_Keep */

   /* Open Loop Tx Power estimations */
   int16  PowerBaseALog2;       /* Tx power base from Stack (i.e, L1D, ACMAC or RTCMAC), in ALog2: Bryan_Keep */
   int16  OpenLoopRxPwrFiltALog2; /** Bryan_Keep */
   int16  PowerBase;            /* Tx power base from Stack (i.e, L1D, ACMAC or RTCMAC), in dBm:  Bryan_Keep */
   int16  OpenLoopRxPwrFilt;    /* The filtered estimate of Rx Power, in dBm: Bryan_Keep */

   /* Close Loop Tx Power adjustments */
   int16  CloseLoopAcc;         /* The accumulation of all closed loop corrections over DO frame period:  Bryan_NOK */
   int16  CloseLoopAdjALog2;    /** Bryan_Keep */
   int16  CloseLoopAdj;         /** Bryan_Keep */

   /* Tx Power Adjustments */
   int16  UnadjPwrALog2;        /** Bryan_Keep */
   int16  TotalAdjALog2;        /** Bryan_keep */
   int16  TxPwrALog2Slot;       /** Bryan_keep */
   int16  TxPwrALog2Hslot;      /** Bryan_keep */
   int16  UnadjPwr;             /* Unadjusted Tx antenna power, in dBm */
   int16  TotalAdj;             /* Total Tx AGC adjustments, in dBm */
   int16  TotalTxPwrSlot;       /* Adjusted Tx antenna power, in dBm */
   int16  TotalTxPwrHslot;      /* Adjusted Tx antenna power, in dBm */

   /* Tx Gain Scaling values */
   uint16 Slot_Ks;              /* Ks gain scaling value, in linear units: Bryan_Keep */
   int16  Slot_KsALog2;         /* Ks gain scaling value, in Alog2: Bryan_Keep */
   int16  Slot_KsDbm;           /* Ks gain scaling value, in dBm */
   uint16 Slot_KsIdx;           /* Ks hardware index value, ~3dB steps */

   uint16 Hslot_Ks;             /* Ks gain scaling value, in linear units: Bryan_Keep */
   int16  Hslot_KsALog2;        /* Ks gain scaling value, in Alog2 */
   int16  Hslot_KsDbm;          /* Ks gain scaling value, in dBm */
   uint16 Hslot_KsIdx;          /* Ks hardware index value, ~3dB steps */

   /* Max Allowed Tx Power */
   int16  TotalMaxTxPwrALog2;
   int16  TotalMaxTxPwr;        /* Adjusted Max Tx antenna power allowed, in dBm */

   /* Min Allowed Tx Power */
   int16  TotalMinTxPwrALog2;
   int16  TotalMinTxPwr;        /* Adjusted Min Tx antenna power allowed, in dBm */

   /* External RF settings */
   uint16 TxGainStateSlot;      /* Current Tx AGC gain state: Bryan_Keep */
   uint16 TxGainStateHslot;     /* Current Tx AGC gain state: Bryan_Keep */
   uint16 GainTransitions;      /* Number of gain state transitions over DO frame period */

   uint16 TxDacValueSlot;       /* Final hardware Tx AGC DAC value */
   uint16 TxDacValueHslot;      /* Final hardware Tx AGC DAC value */

   uint16 TxSpdmValueSlot;      /* Final hardware Tx AGC SPDM value */
   uint16 TxSpdmValueHslot;     /* Final hardware Tx AGC SPDM value */

} RcpTxAgcDataT;

typedef struct
{
#if defined (MTK_PLT_ON_PC)
    int16 TotalAdjALog2[HWD_NUM_HYST_STATES_TXAGC];
#else
    int16 TotalAdjALog2[HWD_PA_MODE_NUM];
#endif
} RcpTxAgcAdjDataT;

typedef struct
{
   /* Max Allowed Tx Power */
   int16  TotalMaxTxPwrALog2;
   int16  TotalMaxTxPwr;     /* Adjusted Max Tx antenna power allowed, in dBm */

   /* Min Allowed Tx Power */
   int16  TotalMinTxPwrALog2;
   int16  TotalMinTxPwr;     /* Adjusted Min Tx antenna power allowed, in dBm */
} RcpTxAgcMaxMinDataT;

/* Define RCP Tx AGC operation parameters */
typedef struct
{
    uint16  OpenLoopMaxSlew;        /* max slew rate of the open loop Rx pwr estimate   */
    uint16  OpenLoopCorrGain;       /* open loop correction gain in OPEN_LOOP_CORR_Q    */

} RcpTxAgcParmT;

/* Defince RCP Boundary load identifiers */
typedef enum
{
   TX_IMMEDIATE_LOAD=0,        /* Load/latch Tx values immediately */
   TX_SLOT_BOUNDARY_LOAD,      /* Load/latch Tx values on DO Slot boundary */
   TX_HALFSLOT_BOUNDARY_LOAD   /* Load/latch Tx values on DO Half-slot boundary */

} RcpTxLoadBoundaryT;

/* DO-specific data structure for tracking gain scaling data on a half-slot basis */
typedef  struct
{
   uint16 Slot_Ks;
   int16  Slot_KsALog2;
   uint16  Slot_KsIdx;

   uint16 Hslot_Ks;
   int16  Hslot_KsALog2;
   uint16  Hslot_KsIdx;

} RcpDoSlotKsDataT;

/* DO-specific data structure for tracking Tx AGC on a half-slot basis with DO
** reverse channel format */
#if defined (MTK_PLT_ON_PC)
typedef  struct
{
   int16  Slot_TxPwrALog2;
   uint16 Slot_TxGainState;

   int16  Hslot_TxPwrALog2;
   uint16 Hslot_TxGainState;

   uint16 RpcInfo; /* bit1 : valid, bit 0 : up/down */
   uint16 tx_ctrl_reg;

   int16  Slot_KsALog2;          /* Ks gain scaling value */
   int16  Hslot_KsALog2;         /* Ks gain scaling value */

   int16  ReqPwrAlog2Slot;       /** Requested transmit power for first half slot, in ALOG2 */
   int16  ReqPwrAlog2Hslot;      /** Requested transmit power for second half slot, in ALOG2 */

   int16  TotalTxPwrAlog2Slot;   /* Adjusted Tx antenna power */
   int16  TotalTxPwrAlog2Hslot;  /* Adjusted Tx antenna power */

   uint16 TxDacValueSlot;        /* Final hardware Tx AGC DAC value */
   uint16 TxDacValueHslot;       /* Final hardware Tx AGC DAC value */

} RcpDoSlotTxAgcDataT;
#else
typedef  struct
{
   uint16 RpcInfo;               /* bit1 : valid, bit 0 : up/down */
   uint16 tx_ctrl_reg;           /* indicate channels */

   int16  UnadjPwrALog2;          /* Tx Power Adjustments, only for first half slot */
   int16  OpenLoopRxPwrFiltALog2; /** Open loop filtered mean input power */
   int16  CloseLoopAdjALog2;      /** Close loop adjustment */

   int16  ReqPwrAlog2Slot;       /** Requested transmit power for first half slot, in ALOG2 */
   int16  ReqPwrSlot;            /** Requested transmit power for first half slot, in dB */
   int16  ReqPwrAlog2Hslot;      /** Requested transmit power for second half slot, in ALOG2 */
   int16  ReqPwrHslot;           /** Requested transmit power for second half slot, in dB */
   int16  TotalTxPwrAlog2Slot;   /** Target power for first slot, in ALOG2 */
   int16  TotalTxPwrSlot;        /** Target power for first slot, in dB */
   int16  TotalTxPwrAlog2Hslot;  /** Target power for second slot, in ALOG2 */
   int16  TotalTxPwrHslot;       /** Target power for second slot, in dB */

   int16  KsErrSlot;             /** Ks Error for first half slot */
   int16  KsErrHslot;            /** Ks Error for second half slot */
   int16  DeltaGainSlot;         /** Delta gain for first half slot */
   int16  DeltaGainHslot;        /** Delta gain for second half slot */
   int16  targetGainSlot;        /** Target gain for first half slot */
   int16  targetGainHslot;       /** Target gain for second half slot */
   int16  RfGainDetSlot;         /** Detected RF gain for first half slot */
   int16  RfGainDetHslot;        /** Detected RF gain for second half slot */

   bool   DdpcEnSlot;            /** DDPC Loop Enable Flag for first half slot */
   bool   DdpcEnHslot;           /** DDPC Loop Enable Flag for second half slot */
   bool   FirstTrans;            /** Check whether this is first transmission */
   bool   OlControlSlot;         /** OL or CL indication for first half slot */
   bool   OlControlHslot;        /** OL or CL indication for second half slot */
   bool   TxupcTagetAbSlot;      /** Target gain is for absolute error only or not for first half slot */
   bool   TxupcTagetAbHslot;     /** Target gain is for absolute error only or not for second half slot */
   bool   dummy;                 /** Dummy word */

   int16  PgaGainSlot;           /** PGA gain for first half slot */
   int16  PgaGainHslot;          /** PGA gain for second half slot */
   int16  PaGainSlot;            /** PA gain for first half slot */
   int16  PaGainHslot;           /** PA gain for second half slot */
   int16  Gbb0Slot;              /** GBB0 gain for first half slot */
   int16  Gbb0Hslot;             /** GBB0 gain for second half slot */
   int16  Gbb1Slot;              /** GBB1 gain for first half slot */
   int16  Gbb1Hslot;             /** GBB1 gain for second half slot */
   int16  Gbb1CwSlot;            /** GBB1 codeword for first half slot */
   int16  Gbb1CwHslot;           /** GBB1 codeword for second half slot */
   int16  ThetaSlot;             /** Theta value for first half slot */
   int16  ThetaHslot;            /** Theta value for second half slot */
   int16  PaModeSlot;            /** PA mode for first half slot */
   int16  PaModeHslot;           /** PA mode for second half slot */
   int16  TxupcThrSlot;          /** TxUPC threshold for first half slot */
   int16  TxupcThrHslot;         /** TxUPC threshold for first half slot */
   int16  couplerLossSlot;       /** The coupler loss, L1 should add the value to DDPC result, unit is 1/32dB */
   int16  couplerLossHslot;      /** The coupler loss for second half slot */
   int16  txDetGainSlot;         /** The TxDet Gain, L1 should minus it from DDPC result, unit is 1/32dB. */
   int16  txDetGainHslot;        /** The TxDet Gain for second half slot */
   int16  pgaModeSlot;  /**  The PGA gain type, 0: PGA-A table, 1: PGA-B table. It just be used for L1 logging purpose */
   int16  pgaModeHSlot;  /**  The PGA gain type for second half slot   */
   int16  TxdcoEstISlot; /** The estimate DC offset  TxPath:S0.14*/
   int16  TxdcoEstQSlot; /** The estimate DC offset  TxPath:S0.14*/
   int16  TxphaseEstSlot; /** The estimate phase error,TxPath:S-4.10*/
   int16  TxgainEstSlot; /** The estimate gain error, TxPath:S-4.10*/
   int16  TxdcoEstIHSlot; /** The estimate DC offset  TxPath:S0.14*/
   int16  TxdcoEstQHSlot; /** The estimate DC offset  TxPath:S0.14*/
   int16  TxphaseEstHSlot; /** The estimate phase error,TxPath:S-4.10*/
   int16  TxgainEstHSlot; /** The estimate gain error, TxPath:S-4.10*/
   int16  DetdcoEstISlot; /** The estimate DC offset  DetPath:S0.11*/
   int16  DetdcoEstQSlot; /** The estimate DC offset  DetPath:S0.11*/
   int16  DetphaseEstSlot; /** The estimate phase error,DetPath:S-4.10*/
   int16  DetgainEstSlot; /** The estimate gain error, DetPath:S-4.10*/
   int16  DetdcoEstIHSlot; /** The estimate DC offset  DetPath:S0.11*/
   int16  DetdcoEstQHSlot; /** The estimate DC offset  DetPath:S0.11*/
   int16  DetphaseEstHSlot; /** The estimate phase error,DetPath:S-4.10*/
   int16  DetgainEstHSlot; /** The estimate gain error, DetPath:S-4.10*/
}RcpDoSlotTxAgcDataT;
#endif
/* DO-specific data structure for tracking Tx Clipper settings on a half-slot basis */
typedef  struct
{
   uint16 Slot_TxClipTresh;

   uint16 Hslot_TxClipTresh;

} RcpDoSlotClipperDataT;

/* DO RCP protocol subtype identifiers */
typedef enum
{
   RCP_PROTOCOL_SUBTYPE_0_1=0,
   RCP_PROTOCOL_SUBTYPE_2

} RcpProtocolSubtypeT;

/* DO-specifc Reverse Channel identifiers */
typedef enum
{
   RCP_REV_PILOT_CHANNEL=0,
   RCP_REV_AUX_PILOT_CHANNEL,
   RCP_REV_RRI_CHANNEL,
   RCP_REV_DSC_CHANNEL,
   RCP_REV_DRC_CHANNEL,
   RCP_REV_ACK_CHANNEL,
   RCP_REV_DATA_CHANNEL

} RcpRevChanT;

/* Identifier use to indicate channel scale units (i.e., in dB or in linear gain) */
typedef enum
{
   RCP_CHAN_SCALE_TYPE_DB=0,
   RCP_CHAN_SCALE_TYPE_LINEAR

} RcpChanScaleTypeT;

/* DO-specifc Reverse Channel Scales used to program RCP/Transmitter hardware */
typedef enum
{
   RCP_REV_PILOT_SCALE=0,
   RCP_REV_PREAMBLE_SCALE,  /* Pilot Channel scale during preample portion of Access Channel */
   RCP_REV_AUX_PILOT_ACK_SCALE,
   RCP_REV_AUX_PILOT_NAK_SCALE,
   RCP_REV_RRI_ACK_SCALE,   /* RRI Channel when transmitting in response to an ACK */
   RCP_REV_RRI_NAK_SCALE,   /* RRI Channel when transmitting in response to a NAK */
   RCP_REV_ACK_SUP_SCALE,   /* ACK Channel when acknowledging a single-user packet */
   RCP_REV_ACK_MUP_SCALE,   /* ACK Channel when acknowledging a multi-user packet */
   RCP_REV_DSC_SCALE,
   RCP_REV_DSC_BOOST_SCALE,
   RCP_REV_DRC_SCALE,
   RCP_REV_DRC_BOOST_SCALE,
   RCP_REV_DATA_ACK_SCALE,  /* Data Channel scale in reponse to an ACK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_ACK_SCALE0, /* Data Channel scale in reponse to an ACK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_ACK_SCALE1, /* Data Channel scale in reponse to an ACK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_ACK_SCALE2, /* Data Channel scale in reponse to an ACK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_ACK_SCALE3, /* Data Channel scale in reponse to an ACK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_NAK_SCALE,  /* Data Channel scale in reponse to an NAK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_NAK_SCALE0, /* Data Channel scale in reponse to an NAK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_NAK_SCALE1, /* Data Channel scale in reponse to an NAK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_NAK_SCALE2, /* Data Channel scale in reponse to an NAK dependant on packet sizes (see DO transmitter spec) */
   RCP_REV_DATA_NAK_SCALE3, /* Data Channel scale in reponse to an NAK dependant on packet sizes (see DO transmitter spec) */

   RCP_REV_NUM_SCALE_REGS   /* Number of hardware RCP scale registers */

} RcpRevChanScaleT;

/* Define RCP Gain Scaling tracking parameters */
typedef struct
{
   int16  ScaleDb;         /* Reverse Channel Scaling Adjustment, in dB, Q=7 */
   uint16 ScaleLinear;     /* Reverse Channel Scaling ADjustment, in linear notation, Q=7 */
   uint32 HwdRegAddr;      /* Hardware register address associated with scaling value */
   uint8  RegQValue;       /* Hardware register Q-value (is not uniform across the hw interface) */
   uint8  TestMode;        /* Set to TRUE if using ETS manual override values */

} RcpRevChanScaleParamsT;

/* Define RCP Gain Control Unit enum and data structure - these are very similiar to the
** HwdGainControlUnitTstructure defined in hwdrfapi.h to manage the external RF interfaces
** during a Tx AGC gain state transition */
typedef enum
{
   RCP_TXAGC_PA1 = 0,
   RCP_TXAGC_PA2,
   RCP_TXAGC_PDM1,
   RCP_TXAGC_PDM2,
   RCP_TXAGC_RFSPI,

   RCP_TXAGC_MAX_CTRL_UNITS /* Number of Tx AGC gain state control units */

} RcpTxAgcCtrlUnitT;

typedef enum
{
   RCP_FIRST_DELAY_ADDR = 0,
   RCP_SECOND_DELAY_ADDR,

   RCP_MAX_DELAY_ADDR

} RcpDelayAddrT;

/* for delay load mode selection */
typedef enum
{
   HWD_DELAY_TX_DAC=0,
   HWD_DELAY_TX_ON0,
   HWD_DELAY_TX_ON1,
   HWD_DELAY_TX_ON2,
   HWD_DELAY_TX_ON3,
   HWD_DELAY_TX_ON4,
   HWD_DELAY_TX_ON5,
   HWD_DELAY_TX_ON6,
   HWD_DELAY_TX_ON7,
   HWD_DELAY_TX_ON8,
   HWD_DELAY_TX_MAX
} HwdDelayTxOnTypeT;

typedef enum
{
   HWD_DELAY_MASK_TX_ON0=0,
   HWD_DELAY_MASK_TX_ON1,
   HWD_DELAY_MASK_TX_ON2,
   HWD_DELAY_MASK_TX_ON3,
   HWD_DELAY_MASK_TX_ON4,
   HWD_DELAY_MASK_TX_ON5,
   HWD_DELAY_MASK_TX_ON6,
   HWD_DELAY_MASK_TX_ON7,
   HWD_DELAY_MASK_TX_ON8,
   HWD_DELAY_MASK_TX_DAC,
   HWD_DELAY_MASK_MAX
} HwdDelayModeTxOnTypeT;

typedef enum
{
   HWD_DELAY_PDM0 = 0,
   HWD_DELAY_PDM1 = 1,
   HWD_DELAY_PDM2 = 2,
   HWD_DELAY_PDM3 = 3,
   HWD_DELAY_PDM4 = 4,
   HWD_DELAY_PDM5 = 5,
   HWD_DELAY_PDM6 = 6
}HwdDelayPdmT;

typedef struct
{
   uint8  TestMode;        /* Set to TRUE if using ETS manual override values */
   uint32 RcpDelayAddress[RCP_MAX_DELAY_ADDR];
   uint8  RcpSlotDelayValue[NUMBER_OF_TRANS];
   uint8  RcpHslotDelayValue[NUMBER_OF_TRANS];

} RcpGainControlUnitT;

/* Define Tx AGC Closed-Loop Step Size enum */
typedef enum
{
   RCP_TXAGC_STEP_SIZE_HALF_DB = 0,
   RCP_TXAGC_STEP_SIZE_ONE_DB,
   RCP_TXAGC_STEP_SIZE_QUARTER_DB

} RcpTxAgcClosedLoopStepSizeT;

/* Define Max Limit Threshold clip enum */
typedef enum
{
   RCP_TX_PILOT_PWR_CLIPPED = 0,
   RCP_TX_PILOT_PWR_UNCLIPPED

} RcpTxPilotPwrClipT;

/* Define Slot/Hslot/Max(Slot,Hslot) for tx pwr enum */
typedef enum
{
   RCP_TX_PWR_SLOT = 0,
   RCP_TX_PWR_HALFSLOT,
   RCP_TX_PWR_MAX

} RcpTxPwrSlotHslotT;

/* Define Traffic Release Type enum */
typedef enum
{
   RCP_TRAFFIC_RELEASE_IDLE = 0,
   RCP_TRAFFIC_RELEASE_STANDBY

} RcpTxTrafficReleaseTypeT;


/*---------------*/
/* ETS Test Cmds */
/*---------------*/
typedef PACKED_PREFIX struct
{
   uint8         type;         /* For CpBufGet, CpBufStats. The pool partition type */
} PACKED_POSTFIX CpBufTestCmdMsgT;

typedef PACKED_PREFIX struct
{
   uint8         freeAllFlag;  /* For freeing all the cpbufs alloc earlier */
   uint32        freeIndex;    /* For CpBufFree, index of the cpbuf alloc previously */
} PACKED_POSTFIX CpBufTestFreeCmdMsgT;


/*---------------*/
/* DSAR, PCPR Structures */
/*---------------*/
typedef PACKED_PREFIX struct {
    uint8 msgSeq;       /* sequence number of msg this Ack is for */
} PACKED_POSTFIX  DsarAckMsgInfoT;

typedef PACKED_PREFIX struct {
    uint8 msgSeq;       /* sequence number of msg requiring Ack */
} PACKED_POSTFIX  DsarAckReqMsgInfoT;

typedef DsarAckMsgInfoT DsarResetMsgT;

typedef PACKED_PREFIX struct
{
   uint8  MacFlowId;
   uint8  SubStreamNum;
} PACKED_POSTFIX  MacFlow2StrFlowInfoT;

typedef PACKED_PREFIX struct
{
  MacFlow2StrFlowInfoT     Info[MAX_RLP_FLOW_SUPPORTED];
  uint8                    Num;
} PACKED_POSTFIX MacFlow2StrFlowMappingT;


/*------------------------*/
/* PAR message structure definitions*/
/*------------------------*/
/* structure for message with HLP  */

typedef PACKED_PREFIX struct
{
   uint8  nAppType;
   uint8  nRlpFlowId;
   bool   bBusy;
} PACKED_POSTFIX  HlpParBufStatMsg;

typedef  struct {
   CpBufferT   *dataPtr;
   uint16   offset;
   uint16   dataLen;
   uint16 tcpTag;
   uint32 tcpPort;
   uint32 srcIpAddr;
   uint32 dstIpAddr;
} ParHlpRevDataPktT;

typedef  struct
{
   uint8       nAppType;
   uint8       nRLPFlowId;
   uint8       numHlpFrames;
   ParHlpRevDataPktT   hlpFrames[MAX_RLP_HLP_PKT_NUM];
} ParHlpDataTxMsgT;/*PAR_HLP_DATA_TX_MSG*/

typedef PACKED_PREFIX struct
{
   uint8 nAppType;
   uint8 nRlpFlowId;
   uint8 Route; /*0-Route A, 1-Route B*/
   uint8 nRevLable;
   bool  bAckRequired;
   uint8 Reset;
   uint16     dataLen;
   uint16     Offset;
   CpBufferT   *dataPtr;
   uint8  nPriority;
} PACKED_POSTFIX  ParHlpDOSTxMsgT;/*PAR_HLP_DOS_TX_MSG*/

/*------------------------*/
/* structure for Flow Control Protocol */
/*------------------------*/

typedef enum
{
   PAR_FLOWCTRL_CLOSE_ST,
   PAR_FLOWCTRL_OPEN_ST,
   PAR_FLOW_CTL_MAX
} ParFlowCtrlStateT;

typedef enum
{
   PAR_AccessStream,
   PAR_ServiceStream,
   PAR_STREAM_MAX
} PARAppStreamT;

typedef PACKED_PREFIX struct
{
   PARAppStreamT     StreamType;
   ParFlowCtrlStateT Status;
} PACKED_POSTFIX  ParPafFlowCtlStatMsgT;
typedef PACKED_PREFIX struct
{
   uint8 AppType;
   uint16 ProtocolType;
   uint8 RlpFlow;
} PACKED_POSTFIX ParPafRlpResetReqMsgT;

#ifdef CBP7_EHRPD
typedef enum {
   RSP_AOPENBSETTING_ST,
   RSP_AOPENBRISING_ST,
   RSP_ASETTINGBOPEN_ST,
   RSP_ARISINGBOPEN_ST,
   RSP_MAX_ST
}ParRspStateT;

typedef enum {
   OCTET_STREAM,
   PACKET_STREAM
} RlpStreamPduT;

typedef enum {
   OCTET_DATAUNIT,
   PACKET_DATAUNIT
} RlpDataUnitT;

typedef enum {
   RSP_ROUTE_A,
   RSP_ROUTE_B,
   RSP_MAX_RT
} ParRspRouteT;


typedef struct {
   uint8 FlowProtocolPDU;
   uint8 RouteProtocolPDU;
   uint8 FlowDataUnit;
   uint8 FlowProtocolID;
   RevRohcProtocolParmsT FlowProtocolParms;
   uint8 RouteProtocolID;
   RevRohcProtocolParmsT RouteProtocolParms;
} ParFlowInstanceT;

typedef struct {
   PARAppStreamT     StreamType;
   ParRspStateT            stRsp;
} ParPafRspStatusMsgT;
#endif

typedef PACKED_PREFIX struct
{
  uint16 FwdSysTime;   /* in uint of 16 slot! */
  uint8 TCAMsgSeq;
  uint8 DRCCover;
  uint8 FwdPhysSlots;
  uint8 FwdMACPkts;
  uint8 FwdPayloadSizeIndex; /*used for fetap*/
  uint16 FwdSeq;
} PACKED_POSTFIX FtaprFcpTestPktMsgT;

typedef PACKED_PREFIX struct
{
  bool bActive;
} PACKED_POSTFIX FtaprLoopBackStatusMsgT;


typedef PACKED_PREFIX struct
{
  uint16 AppType[4];
} PACKED_POSTFIX EtsAppCfgT;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT      RspInfo;
} PACKED_POSTFIX  ParRlpStatPeekMsgT;

typedef struct
{
  CpBufferT* pBuf;
}ParSnKKQosReqMsgT;

typedef PACKED_PREFIX struct
{
  uint8  Link;/*0-Rev, 1-Fwd*/
  uint8  Label;
} PACKED_POSTFIX ReservationInfoT;

typedef PACKED_PREFIX struct
{
  bool    bOn;
  uint8   Count;
  ReservationInfoT LabelInfo[8];
} PACKED_POSTFIX ParRlpReservationOnOffMsgT;

typedef PACKED_PREFIX struct
{
  int32 EVDORLPStatsFlowId;
  uint32 EVDORLPStatsResetTime;
  int32 EVDORLPStatsNaksReceived;
  int32 EVDORLPStatsReXmitsNotFound;
  int64 EVDORLPStatsANNakBytesRequested;
  int64 EVDORLPStatsRxDuplicateBytes;
  int64 EVDORLPStatsRxReXmitsBytes;
  int64 EVDORLPStatsRxNewDataBytes;
  int64 EVDORLPStatsRxTotalBytes;
  int32 EVDORLPStatsNaksSent;
  int64 EVDORLPStatsATNakBytesRequested;
  int64 EVDORLPStatsTxReXmitsBytes;
  int64 EVDORLPStatsTxNewDataBytes;
  int64 EVDORLPStatsTxTotalBytes;
  int32 EVDORLPStatsNakTimesOuts;
  int32 EVDORLPStatsResetCount;
  int32 EVDORLPStatsATResetRequestCount;
  int32 EVDORLPStatsANResetAckCount;
  int32 EVDORLPStatsANResetRequestCount;
} PACKED_POSTFIX  EVDORLPStatsMeasT;

typedef PACKED_PREFIX struct
{
  EVDORLPStatsMeasT rlpMeas;
} PACKED_POSTFIX  EVDORLPStatsEventT;

typedef PACKED_PREFIX struct
{
   uint8 protocolType;
   uint8 msgId;
   bool bNeedTxStatus;
} PACKED_POSTFIX DsarDelMsgCmdMsgT;

typedef PACKED_PREFIX struct
{
   /* Change for CR ALPS02665609,  if  bEmptyConfigReqsOnly is TRUE, only delete pending un-acked ConfigurationRequests.*/
   bool bEmptyConfigReqsOnly;
} PACKED_POSTFIX DsarAmpResetMsgCmdMsgT;

typedef PACKED_PREFIX struct {
   uint16   Stream0Configuration;
   uint16   Stream1Configuration;
   uint16   Stream2Configuration;
   uint16   Stream3Configuration;
} PACKED_POSTFIX  DoStreamConfigurationSetMsgT;

typedef PACKED_PREFIX struct
{
   bool enable;
} PACKED_POSTFIX LUPUnsolicitedCfgMsgT;


/*  Packet performance measurement */
typedef PACKED_PREFIX struct
{
  uint32    TotalRevPhySubFrame;      /* Count how many subframes have been used.*/
  uint32    TotalAckBits;                     /* in unit of 128 bit*/
  uint32    NakSubPackets;                 /* missing sub-packet on AN side*/
  uint32    NakPackets;                      /* missing packet on AN side*/
  uint32    TotalTxSubPacket;             /* Number of sub-packet sent by AT */
  uint32    TotalTxPacket;                   /* Number of packet sent by AT */
  uint32    RevSubPER;                      /* NakSubPackets/TotalTxPacket*/
  uint32    RevPER;                           /* NakPackets/TotalTxPacket*/
  uint32    PHY_ThrPut;                      /* TotalAckBits/TotalRevPhySubFrame*/
  uint32    AT_ThrPut;                        /* TotalBits/time*/
} PACKED_POSTFIX RcpPacketPerfStatT;

extern RcpPacketPerfStatT RcpPacketPerfStat;
extern void RcpPerfDataReset(void);

typedef PACKED_PREFIX struct
{
   bool Mode;
} PACKED_POSTFIX RcpRtmQosTxModeSelectionAlgoMsgT;

/*----------------------------------------------------------------------------
 define for RCP_ADJ_UPDATE_MSG
----------------------------------------------------------------------------*/
typedef struct
{
   int16 TxMaxPwr;
   int16 TxMinPwr;
   void *TxAdjPtr;
}RcpAdjUpdateT;


#ifdef MTK_PLT_ON_PC_UT
/* transmission rate  used to index into max payload tables and etc. */
#define ACM_MAX_NUM_RATES     4

/* for saving into personality file, use PACKED struct */
typedef PACKED_PREFIX struct
{
   uint8  APersistenceOverride;
   uint8  TerminalAccessRateMax;
   uint8  AccessTerminalClassOverride;
   uint8  ProbeSeqMax;
   uint8  ProbeBackoff;
   uint8  ProbeSeqBackoff;
   int8   DataOffset[ACM_MAX_NUM_RATES];  /* 2's complement, [0] in 0.5dB, others in 0.25dB */
   /* 0=DataOffsetNom(Q1),  1=DataOffset9k6(Q2), 2=DataOffset19k2(Q2),  3=DataOffset38k4(Q2) */
} PACKED_POSTFIX  AcmAttributesT;
#endif


/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
extern RcpModeT  RcpMode;

/* RCP Test Mode variables */
extern RcpTestModesT RcpTestModes;

/* RCP RF Control AGC Management Boolean flags */
/* Trigger Tx_Ons in TxAGC after 4 slots delay - for tx power gating at the start time*/
extern bool RcpRtcTxAgcStartInd, RcpAcTxAgcStartInd;

extern bool RcpTxAgcEnabled;

/* Tx AGC Data structure for ISR tracking variables (declared in rmcisr.c for
** use by the RMC/RCP Outer-Loop ISRs) */
extern RcpTxAgcDataT RcpTxAgcData;

extern RcpTxAgcAdjDataT CurrRcpTxAgcAdjData;
extern RcpTxAgcMaxMinDataT CurrRcpTxAgcMaxMinData;

/* DO-specific data structure for tracking Tx AGC on a half-slot with DO
** reverse channel format) declared in rmcisr.c for use by the RMC/RCP
** Outer-Loop ISRs) */
extern uint8                 RcpDoSlotIdx;
extern RcpDoSlotTxAgcDataT   RcpDoSlotTxAgcData[MAX_DO_SLOTS_PER_FRAME];
extern RcpDoSlotKsDataT      RcpDoSlotKsData[MAX_DO_SLOTS_PER_FRAME];
extern RcpDoSlotClipperDataT RcpDoSlotClipperData[MAX_DO_SLOTS_PER_FRAME];

/* Declare RCP Tx AGC Closed-Loop Step Gain size variable and default to 1dBm - this
** parameter is updated by the forward channel processing unit when a power parameters
** message is received. Q=RMC_RCP_GAIN_ALOG2_Q (i.e., Q=7 */
extern uint16 RcpTxAgcClosedLoopStepSize;
extern uint16 RcpTxAgcClosedLoopDownStepSize;
extern uint16 RcpTxAgcClosedLoopUpStepSize;

/* Declare RCP Clipper Level variable used to program the Tx MXS Clipper Level Settings */
extern uint16 RcpTxClipLevel;

/* for closed loop down limit control based on finger c2i */
extern int16  CLRangeUpLimitAlog2, CLRangeDownLimitAlog2, CLRangeTestDownLimitAlog2;
extern bool   CLRangeUseC2IThr4DownLimit;
extern uint16 CLRangeC2IThr4DownLimit;

/* delay mode register control bit position - RCP local variable */
extern uint8  RcpTxDlyModeBitMaskPA, RcpTxDlyModeBitMaskGATE, RcpTxDlyModeBitMaskPDM;
extern uint8  RcpTxDlyModeBitMaskPA2;

extern uint32 RcpPA1DlyMaskRegAddress;
extern uint32 RcpPA2DlyMaskRegAddress;
extern uint32 RcpPA1DlyMaskReadRegAddress;
extern uint32 RcpPA2DlyMaskReadRegAddress;

/* delay mask register control bit position - RCP local variable */
extern uint8 RcpTxDlyMaskBitMaskPA, RcpTxDlyMaskBitMaskGATE;
extern uint8 RcpTxDlyMaskBitMaskPA2;

extern HwdRfBandT RmcIsrCurrentMainRfBand;
extern HwdRfBandT RmcIsrCurrentDivRfBand;

extern uint8 accessDataRate, trafficRev0DataRate,  trafficRevAAckDataRate, trafficRevANakDataRate;

extern bool RcpHwdRfSpiDlyEnable;

extern RtmRmmStateT RtmState;

/*----------------------------------------------------------------------------
 Global DO TXHA Register Macro Prototypes
----------------------------------------------------------------------------*/

/* Define Macros that manage the HWD_DO_TXHA_RRI_ACK0 register */
#define DOTX_RRI_ACCESS_RATE_0BPS      0x0000
#define DOTX_RRI_ACCESS_RATE_9600BPS   0x0011
#define DOTX_RRI_ACCESS_RATE_19200BPS  0x001a
#define DOTX_RRI_ACCESS_RATE_38400BPS  0x002b
#define DOTX_RRI_ACCESS_RATE_76800BPS  0x003c
#define DOTX_RRI_ACCESS_RATE_153600BPS 0x004d

#define RcpDoRriDataAckAccessRate(Data) \
   HwdWrite(HWD_DO_TXHA_RRI_DATA_ACK0, (Data)); \
   accessDataRate = Data&0x7

/* Define Macros that manages the HWD_DO_TXHA_RRI_ACK1 register */
#define DOTX_RRI_SYMBOL_RATE_ZERO       0x0000
#define DOTX_RRI_SYMBOL_RATE_9600BPS    0x0011
#define DOTX_RRI_SYMBOL_RATE_19200BPS   0x001a
#define DOTX_RRI_SYMBOL_RATE_38400BPS   0x002b
#define DOTX_RRI_SYMBOL_RATE_76800BPS   0x003c
#define DOTX_RRI_SYMBOL_RATE_153600BPS  0x004d

#define RcpDoRriDataAckSymbolRate(Data) \
   HwdWrite(HWD_DO_TXHA_RRI_DATA_ACK1, (Data)); \
   trafficRev0DataRate = Data&0x7

/* Define Macros that manages the HWD_DO_TXHA_RRI_ACK2 register */
#define DOTX_RRI_PAYLOAD_IDX_0          0x0000 << 2 /* HWD_DO_TXHA_RRI_ACK2[5:2] */
#define DOTX_RRI_PAYLOAD_IDX_128        0x0001 << 2
#define DOTX_RRI_PAYLOAD_IDX_256        0x0002 << 2
#define DOTX_RRI_PAYLOAD_IDX_512        0x0003 << 2
#define DOTX_RRI_PAYLOAD_IDX_768        0x0004 << 2
#define DOTX_RRI_PAYLOAD_IDX_1024       0x0005 << 2
#define DOTX_RRI_PAYLOAD_IDX_1536       0x0006 << 2
#define DOTX_RRI_PAYLOAD_IDX_2048       0x0007 << 2
#define DOTX_RRI_PAYLOAD_IDX_3072       0x0008 << 2
#define DOTX_RRI_PAYLOAD_IDX_4096       0x0009 << 2
#define DOTX_RRI_PAYLOAD_IDX_6144       0x000A << 2
#define DOTX_RRI_PAYLOAD_IDX_8192       0x000B << 2
#define DOTX_RRI_PAYLOAD_IDX_12288      0x000C << 2

#define DOTX_RRI_SUBPACKET_IDX_0        0x0000      /* HWD_DO_TXHA_RRI_ACK2[1:0] */
#define DOTX_RRI_SUBPACKET_IDX_1        0x0001
#define DOTX_RRI_SUBPACKET_IDX_2        0x0002
#define DOTX_RRI_SUBPACKET_IDX_3        0x0003

/* payload first and then subpacketId set - always pair */

/* Define Macros that manages the HWD_DO_TXHA_RRI_ACK2 register RRI ACK Payload and
** Subpacket Indexes */

#define RcpDoRriDataAckPayloadIdx(Data) \
   HwdWrite(HWD_DO_TXHA_RRI_DATA_ACK2, 0x3f & Data); \
   trafficRevAAckDataRate = (Data>>2)&0xf

#define RcpDoRriDataAckSubPacketIdx(Data) \
   HwdWrite(HWD_DO_TXHA_RRI_DATA_ACK2,  (HwdRead(HWD_DO_TXHA_RRI_DATA_ACK2)&0x003c) | Data)

/* Define Macros that manages the HWD_DO_TXHA_RRI_NAK register RRI NAK Payload and
** Subpacket Indexes uses same macros for the RRI_ACK2 register */

#define RcpDoRriDataNakPayloadIdx(Data) \
   HwdWrite(HWD_DO_TXHA_RRI_NAK, 0x3f & Data); \
   trafficRevANakDataRate = (Data>>2)&0xf


#define RcpDoRriDataNakSubPacketIdx(Data) \
   HwdWrite(HWD_DO_TXHA_RRI_NAK, (HwdRead(HWD_DO_TXHA_RRI_NAK)&0x003c) | Data)

/* Define Macro that manages the HWD_DO_TXHA_DRC_COVER register */
#define RcpDoDrcCoverWalshIdx(Data) \
   HwdWrite_shadow(HWD_DO_TXHA_DRC_COVER, (Data))

/* Define Macro that manages the HWD_DO_TXHA_DRC_VALUE register */
#define RcpDoDrcRateValue(Data) \
   HwdWrite(HWD_DO_TXHA_DRC_VALUE, (Data))

/* Define Macro that manages the HWD_DO_TXHA_DSC_DATA register */
#define RcpDoDscDataSource(Data) \
   HwdWrite(HWD_DO_TXHA_DSC_DATA, (Data))

/* Define Macros that manages the HWD_DO_TXHA_CHNL_TYPE register */
#define DOTX_CHNL_TYPE_ACCESS           0x0000
#define DOTX_CHNL_TYPE_TRAFFIC          0x0001

#define RcpDoSetChanType(Data) \
   HwdWrite(HWD_DO_TXHA_CHNL_TYPE, (Data))

/* Define Macros that manages the HWD_DO_TXHA_PROTOCOL_SUBTYP register */
#define DOTX_CHNL_SUBTYPE_0_ACCESS      0x0000
#define DOTX_CHNL_SUBTYPE_1_ACCESS      0x0001
#define DOTX_CHNL_SUBTYPE_2_ACCESS      0x0001
#define DOTX_CHNL_SUBTYPE_0_TRAFFIC     0x0000
#define DOTX_CHNL_SUBTYPE_1_TRAFFIC     0x0000
#define DOTX_CHNL_SUBTYPE_2_TRAFFIC     0x0001

#define RcpDoSetChanSubtype(Data) \
   HwdWrite(HWD_DO_TXHA_PROTOCOL_SUBTYP, (Data))

/* Define Macros that manages the HWD_DO_TXHA_TX_POWER register */
#define RcpDoTxPowerOn() \
   HwdWrite(HWD_DO_TXHA_TX_POWER, HwdRead32(HWD_DO_TXHA_TX_POWER)|0x0001)

#define RcpDoTxPowerOff() \
   HwdWrite(HWD_DO_TXHA_TX_POWER, HwdRead32(HWD_DO_TXHA_TX_POWER)&~0x0001)
/* Define Macros that manages the HWD_DO_TXHA_TX_RESET register */
#define HWD_DO_TXHA_TX_RESET_PWR           0x0001
#define HWD_DO_TXHA_TX_RESET_RESET        0x0002
#define HWD_DO_TXHA_TX_RESET_FCS            0x0004
#define HWD_DO_TXHA_TX_RESET_SYMBOL      0x0008

#define RcpDoTxSoftReset() \
   { \
       int i; \
       HwdWrite(HWD_DO_TXHA_TX_RESET, HwdRead32(HWD_DO_TXHA_TX_RESET)&(~HWD_DO_TXHA_TX_RESET_RESET)); \
       for(i=0;i<100;i++); \
       HwdWrite(HWD_DO_TXHA_TX_RESET, HwdRead32(HWD_DO_TXHA_TX_RESET)|HWD_DO_TXHA_TX_RESET_RESET); \
   }

/* Define Macros that manages the HWD_DO_TXHA_STATUS register */
#define HWD_DO_TXHA_STATUS_INTERLACE_MODE           0x0001   /* for data rate 12 */
#define HWD_DO_TXHA_STATUS_TX_EARLY_EN                  0x0002
#define HWD_DO_TXHA_STATUS_FCS_IDLE                         0x0004
#define HWD_DO_TXHA_STATUS_FCS_ERROR                      0x0008
#define HWD_DO_TXHA_STATUS_SYMBOL_ERROR                0x0010
#define HWD_DO_TXHA_STATUS_CHANNEL_RD_ERROR       0x0020

/* txdma transfer start from slot3 in revA and slot15 in rev0 for sw kick-off (test) mode */
#define RcpDoTxDmaEarlyEnable() \
    HwdWrite(HWD_DO_TXHA_STATUS, HwdRead32(HWD_DO_TXHA_STATUS)|HWD_DO_TXHA_STATUS_TX_EARLY_EN );


#if (!defined(MTK_PLT_ON_PC))
/* Define Macros that manages the HWD_DO_TXHA_TX_ENABLE register */
#define RcpDoTxEnable() \
{ \
   HwdWrite(HWD_DO_TXHA_TX_ENABLE, 0x0001); \
   HwdWrite32( HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL)|((uint32)TXHA_DMA_TX_ENABLE)) ); \
   MonTrace(MON_CP_HWD_DFE_TRACE_ID, 3, SYS_MODE_EVDO, HWD_RF_MPA_TX1, HWD_TX_DFE_SW_ON);\
}

#define RcpDoTxDisable() \
{ \
   HwdWrite(HWD_DO_TXHA_TX_ENABLE, 0x0000); \
   HwdWrite32( HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL)&~((uint32)TXHA_DMA_TX_ENABLE)) ); \
   MonTrace(MON_CP_HWD_DFE_TRACE_ID, 3, SYS_MODE_EVDO, HWD_RF_MPA_TX1, HWD_TX_DFE_SW_OFF);\
}
#else
/* Define Macros that manages the HWD_DO_TXHA_TX_ENABLE register */
#define RcpDoTxEnable() \
{ \
   HwdWrite(HWD_DO_TXHA_TX_ENABLE, 0x0001); \
   HwdWrite32( HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL)|((uint32)TXHA_DMA_TX_ENABLE)) ); \
}


#define RcpDoTxDisable() \
{ \
   HwdWrite(HWD_DO_TXHA_TX_ENABLE, 0x0000); \
   HwdWrite32( HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL)&~((uint32)TXHA_DMA_TX_ENABLE)) ); \
}
#endif

#define RcpDoTxAbort() \
   HwdWrite(HWD_DO_TXHA_TX_ABORT, 0x0001)

#define RcpDoTxAbortOff() \
   HwdWrite(HWD_DO_TXHA_TX_ABORT, 0x0000)

/* Define Macro that manages the HWD_DO_TXHA_ACCESS_START register */
#define RcpDoAccessStart(Data) \
   HwdWrite(HWD_DO_TXHA_ACCESS_START, (Data))

/* Define Macros that manages the HWD_DO_TXHA_DRC_GATING register */
#define RcpDoDrcGatingEnable() \
   HwdWrite(HWD_DO_TXHA_DRC_GATING, 0x0001)

#define RcpDoDrcGatingDisable() \
   HwdWrite(HWD_DO_TXHA_DRC_GATING, 0x0000)

/* Define Macros that manages the HWD_DO_TXHA_DRC_LENGTH register */
#define DOTX_DRC_LENGTH_1_SLOT      0x0000
#define DOTX_DRC_LENGTH_2_SLOTS     0x0001
#define DOTX_DRC_LENGTH_4_SLOTS     0x0002
#define DOTX_DRC_LENGTH_8_SLOTS     0x0003

#define RcpDoDrcLength(Data) \
   HwdWrite(HWD_DO_TXHA_DRC_LENGTH, (Data))

/* Define Macros that manages the HWD_DO_TXHA_DSC_LENGTH register */
#define DOTX_DSC_LENGTH_8_SLOTS     0x0001
#define DOTX_DSC_LENGTH_16_SLOTS    0x0002
#define DOTX_DSC_LENGTH_24_SLOTS    0x0003
#define DOTX_DSC_LENGTH_32_SLOTS    0x0004
#define DOTX_DSC_LENGTH_40_SLOTS    0x0005
#define DOTX_DSC_LENGTH_48_SLOTS    0x0006
#define DOTX_DSC_LENGTH_56_SLOTS    0x0007
#define DOTX_DSC_LENGTH_64_SLOTS    0x0008
#define DOTX_DSC_LENGTH_72_SLOTS    0x0009
#define DOTX_DSC_LENGTH_80_SLOTS    0x000A
#define DOTX_DSC_LENGTH_88_SLOTS    0x000B
#define DOTX_DSC_LENGTH_96_SLOTS    0x000C
#define DOTX_DSC_LENGTH_104_SLOTS   0x000D
#define DOTX_DSC_LENGTH_112_SLOTS   0x000E
#define DOTX_DSC_LENGTH_120_SLOTS   0x000F
#define DOTX_DSC_LENGTH_128_SLOTS   0x0010
#define DOTX_DSC_LENGTH_136_SLOTS   0x0011
#define DOTX_DSC_LENGTH_144_SLOTS   0x0012
#define DOTX_DSC_LENGTH_152_SLOTS   0x0013
#define DOTX_DSC_LENGTH_160_SLOTS   0x0014
#define DOTX_DSC_LENGTH_168_SLOTS   0x0015
#define DOTX_DSC_LENGTH_176_SLOTS   0x0016
#define DOTX_DSC_LENGTH_184_SLOTS   0x0017
#define DOTX_DSC_LENGTH_192_SLOTS   0x0018
#define DOTX_DSC_LENGTH_200_SLOTS   0x0019
#define DOTX_DSC_LENGTH_208_SLOTS   0x001A
#define DOTX_DSC_LENGTH_216_SLOTS   0x001B
#define DOTX_DSC_LENGTH_224_SLOTS   0x001C
#define DOTX_DSC_LENGTH_232_SLOTS   0x001D
#define DOTX_DSC_LENGTH_240_SLOTS   0x001E
#define DOTX_DSC_LENGTH_248_SLOTS   0x001F
#define DOTX_DSC_LENGTH_256_SLOTS   0x0020
#define DOTX_DSC_LENGTH_264_SLOTS   0x0021
#define DOTX_DSC_LENGTH_272_SLOTS   0x0022
#define DOTX_DSC_LENGTH_280_SLOTS   0x0023
#define DOTX_DSC_LENGTH_288_SLOTS   0x0024
#define DOTX_DSC_LENGTH_296_SLOTS   0x0025
#define DOTX_DSC_LENGTH_304_SLOTS   0x0026
#define DOTX_DSC_LENGTH_312_SLOTS   0x0027
#define DOTX_DSC_LENGTH_320_SLOTS   0x0028
#define DOTX_DSC_LENGTH_328_SLOTS   0x0029
#define DOTX_DSC_LENGTH_336_SLOTS   0x002A
#define DOTX_DSC_LENGTH_344_SLOTS   0x002B
#define DOTX_DSC_LENGTH_352_SLOTS   0x002C
#define DOTX_DSC_LENGTH_360_SLOTS   0x002D
#define DOTX_DSC_LENGTH_368_SLOTS   0x002E
#define DOTX_DSC_LENGTH_376_SLOTS   0x002F
#define DOTX_DSC_LENGTH_384_SLOTS   0x0030
#define DOTX_DSC_LENGTH_392_SLOTS   0x0031
#define DOTX_DSC_LENGTH_400_SLOTS   0x0032
#define DOTX_DSC_LENGTH_408_SLOTS   0x0033
#define DOTX_DSC_LENGTH_416_SLOTS   0x0034
#define DOTX_DSC_LENGTH_424_SLOTS   0x0035
#define DOTX_DSC_LENGTH_432_SLOTS   0x0036
#define DOTX_DSC_LENGTH_440_SLOTS   0x0037
#define DOTX_DSC_LENGTH_448_SLOTS   0x0038
#define DOTX_DSC_LENGTH_456_SLOTS   0x0039
#define DOTX_DSC_LENGTH_464_SLOTS   0x003A
#define DOTX_DSC_LENGTH_472_SLOTS   0x003B
#define DOTX_DSC_LENGTH_480_SLOTS   0x003C
#define DOTX_DSC_LENGTH_488_SLOTS   0x003D
#define DOTX_DSC_LENGTH_496_SLOTS   0x003E
#define DOTX_DSC_LENGTH_504_SLOTS   0x003F
#define DOTX_DSC_LENGTH_512_SLOTS   0x0040

#define RcpDoDscLength(Data) \
   HwdWrite(HWD_DO_TXHA_DSC_LENGTH, (Data))

/* Define Macros that manages the HWD_DO_TXHA_DRC_BOOST_LEN register */
#define DOTX_DRC_BOOST_LENGTH_2_SLOTS    0x0001
#define DOTX_DRC_BOOST_LENGTH_4_SLOTS    0x0002
#define DOTX_DRC_BOOST_LENGTH_6_SLOTS    0x0003
#define DOTX_DRC_BOOST_LENGTH_8_SLOTS    0x0004
#define DOTX_DRC_BOOST_LENGTH_10_SLOTS   0x0005
#define DOTX_DRC_BOOST_LENGTH_12_SLOTS   0x0006
#define DOTX_DRC_BOOST_LENGTH_14_SLOTS   0x0007
#define DOTX_DRC_BOOST_LENGTH_16_SLOTS   0x0008
#define DOTX_DRC_BOOST_LENGTH_18_SLOTS   0x0009
#define DOTX_DRC_BOOST_LENGTH_20_SLOTS   0x000A
#define DOTX_DRC_BOOST_LENGTH_22_SLOTS   0x000B
#define DOTX_DRC_BOOST_LENGTH_24_SLOTS   0x000C
#define DOTX_DRC_BOOST_LENGTH_26_SLOTS   0x000D
#define DOTX_DRC_BOOST_LENGTH_28_SLOTS   0x000E
#define DOTX_DRC_BOOST_LENGTH_30_SLOTS   0x000F
#define DOTX_DRC_BOOST_LENGTH_32_SLOTS   0x0010
#define DOTX_DRC_BOOST_LENGTH_34_SLOTS   0x0011
#define DOTX_DRC_BOOST_LENGTH_36_SLOTS   0x0012
#define DOTX_DRC_BOOST_LENGTH_38_SLOTS   0x0013
#define DOTX_DRC_BOOST_LENGTH_40_SLOTS   0x0014
#define DOTX_DRC_BOOST_LENGTH_42_SLOTS   0x0015
#define DOTX_DRC_BOOST_LENGTH_44_SLOTS   0x0016
#define DOTX_DRC_BOOST_LENGTH_46_SLOTS   0x0017
#define DOTX_DRC_BOOST_LENGTH_48_SLOTS   0x0018
#define DOTX_DRC_BOOST_LENGTH_50_SLOTS   0x0019
#define DOTX_DRC_BOOST_LENGTH_52_SLOTS   0x001A
#define DOTX_DRC_BOOST_LENGTH_54_SLOTS   0x001B
#define DOTX_DRC_BOOST_LENGTH_56_SLOTS   0x001C
#define DOTX_DRC_BOOST_LENGTH_58_SLOTS   0x001D
#define DOTX_DRC_BOOST_LENGTH_60_SLOTS   0x001E
#define DOTX_DRC_BOOST_LENGTH_62_SLOTS   0x001F
#define DOTX_DRC_BOOST_LENGTH_64_SLOTS   0x0020

#define RcpDoDrcBoostLength(Data) \
   HwdWrite(HWD_DO_TXHA_DRC_BOOST_LEN, (Data))

/* Define Macros that manages the HWD_DO_TXHA_DSC_BOOST_LENGTH register */
#define DOTX_DSC_BOOST_LENGTH_8_SLOTS     0x0001
#define DOTX_DSC_BOOST_LENGTH_16_SLOTS    0x0002
#define DOTX_DSC_BOOST_LENGTH_24_SLOTS    0x0003
#define DOTX_DSC_BOOST_LENGTH_32_SLOTS    0x0004
#define DOTX_DSC_BOOST_LENGTH_40_SLOTS    0x0005
#define DOTX_DSC_BOOST_LENGTH_48_SLOTS    0x0006
#define DOTX_DSC_BOOST_LENGTH_56_SLOTS    0x0007
#define DOTX_DSC_BOOST_LENGTH_64_SLOTS    0x0008
#define DOTX_DSC_BOOST_LENGTH_72_SLOTS    0x0009
#define DOTX_DSC_BOOST_LENGTH_80_SLOTS    0x000A
#define DOTX_DSC_BOOST_LENGTH_88_SLOTS    0x000B
#define DOTX_DSC_BOOST_LENGTH_96_SLOTS    0x000C
#define DOTX_DSC_BOOST_LENGTH_104_SLOTS   0x000D
#define DOTX_DSC_BOOST_LENGTH_112_SLOTS   0x000E
#define DOTX_DSC_BOOST_LENGTH_120_SLOTS   0x000F
#define DOTX_DSC_BOOST_LENGTH_128_SLOTS   0x0010
#define DOTX_DSC_BOOST_LENGTH_136_SLOTS   0x0011
#define DOTX_DSC_BOOST_LENGTH_144_SLOTS   0x0012
#define DOTX_DSC_BOOST_LENGTH_152_SLOTS   0x0013
#define DOTX_DSC_BOOST_LENGTH_160_SLOTS   0x0014
#define DOTX_DSC_BOOST_LENGTH_168_SLOTS   0x0015
#define DOTX_DSC_BOOST_LENGTH_176_SLOTS   0x0016
#define DOTX_DSC_BOOST_LENGTH_184_SLOTS   0x0017
#define DOTX_DSC_BOOST_LENGTH_192_SLOTS   0x0018
#define DOTX_DSC_BOOST_LENGTH_200_SLOTS   0x0019
#define DOTX_DSC_BOOST_LENGTH_208_SLOTS   0x001A
#define DOTX_DSC_BOOST_LENGTH_216_SLOTS   0x001B
#define DOTX_DSC_BOOST_LENGTH_224_SLOTS   0x001C
#define DOTX_DSC_BOOST_LENGTH_232_SLOTS   0x001D
#define DOTX_DSC_BOOST_LENGTH_240_SLOTS   0x001E
#define DOTX_DSC_BOOST_LENGTH_248_SLOTS   0x001F
#define DOTX_DSC_BOOST_LENGTH_256_SLOTS   0x0020
#define DOTX_DSC_BOOST_LENGTH_264_SLOTS   0x0021
#define DOTX_DSC_BOOST_LENGTH_272_SLOTS   0x0022
#define DOTX_DSC_BOOST_LENGTH_280_SLOTS   0x0023
#define DOTX_DSC_BOOST_LENGTH_288_SLOTS   0x0024
#define DOTX_DSC_BOOST_LENGTH_296_SLOTS   0x0025
#define DOTX_DSC_BOOST_LENGTH_304_SLOTS   0x0026
#define DOTX_DSC_BOOST_LENGTH_312_SLOTS   0x0027
#define DOTX_DSC_BOOST_LENGTH_320_SLOTS   0x0028
#define DOTX_DSC_BOOST_LENGTH_328_SLOTS   0x0029
#define DOTX_DSC_BOOST_LENGTH_336_SLOTS   0x002A
#define DOTX_DSC_BOOST_LENGTH_344_SLOTS   0x002B
#define DOTX_DSC_BOOST_LENGTH_352_SLOTS   0x002C
#define DOTX_DSC_BOOST_LENGTH_360_SLOTS   0x002D
#define DOTX_DSC_BOOST_LENGTH_368_SLOTS   0x002E
#define DOTX_DSC_BOOST_LENGTH_376_SLOTS   0x002F
#define DOTX_DSC_BOOST_LENGTH_384_SLOTS   0x0030
#define DOTX_DSC_BOOST_LENGTH_392_SLOTS   0x0031
#define DOTX_DSC_BOOST_LENGTH_400_SLOTS   0x0032
#define DOTX_DSC_BOOST_LENGTH_408_SLOTS   0x0033
#define DOTX_DSC_BOOST_LENGTH_416_SLOTS   0x0034
#define DOTX_DSC_BOOST_LENGTH_424_SLOTS   0x0035
#define DOTX_DSC_BOOST_LENGTH_432_SLOTS   0x0036
#define DOTX_DSC_BOOST_LENGTH_440_SLOTS   0x0037
#define DOTX_DSC_BOOST_LENGTH_448_SLOTS   0x0038
#define DOTX_DSC_BOOST_LENGTH_456_SLOTS   0x0039
#define DOTX_DSC_BOOST_LENGTH_464_SLOTS   0x003A
#define DOTX_DSC_BOOST_LENGTH_472_SLOTS   0x003B
#define DOTX_DSC_BOOST_LENGTH_480_SLOTS   0x003C
#define DOTX_DSC_BOOST_LENGTH_488_SLOTS   0x003D
#define DOTX_DSC_BOOST_LENGTH_496_SLOTS   0x003E
#define DOTX_DSC_BOOST_LENGTH_504_SLOTS   0x003F
#define DOTX_DSC_BOOST_LENGTH_512_SLOTS   0x0040

#define RcpDoDscBoostLength(Data) \
   HwdWrite(HWD_DO_TXHA_DSC_BOOST_LEN, (Data))

/* Define Macros that manages the DO_TXHA_AUXPLTMIN_PYLD register */
#define DOTX_AUXPILOT_MIN_PYLD_128_SLOTS      0x0000
#define DOTX_AUXPILOT_MIN_PYLD_256_SLOTS      0x0001
#define DOTX_AUXPILOT_MIN_PYLD_512_SLOTS      0x0002
#define DOTX_AUXPILOT_MIN_PYLD_768_SLOTS      0x0003
#define DOTX_AUXPILOT_MIN_PYLD_1024_SLOTS     0x0004
#define DOTX_AUXPILOT_MIN_PYLD_1536_SLOTS     0x0005
#define DOTX_AUXPILOT_MIN_PYLD_2048_SLOTS     0x0006
#define DOTX_AUXPILOT_MIN_PYLD_3072_SLOTS     0x0007
#define DOTX_AUXPILOT_MIN_PYLD_4096_SLOTS     0x0008
#define DOTX_AUXPILOT_MIN_PYLD_6144_SLOTS     0x0009
#define DOTX_AUXPILOT_MIN_PYLD_8192_SLOTS     0x000A
#define DOTX_AUXPILOT_MIN_PYLD_12288_SLOTS    0x000B

#define RcpDoAuxPilotMinPayload(Data) \
   HwdWrite(HWD_DO_TXHA_AUXPLTMIN_PYLD, (Data))

#define RcpDo2BitAck(Data) \
   HwdWrite( HWD_DO_TXHA_2BIT_ACK, (Data))

#define RcpDo2BitNak(Data) \
   HwdWrite( HWD_DO_TXHA_2BIT_NAK, (Data))

/* Define Macro that manages the HWD_DO_TXHA_CLIP_THR1 register */
#define RcpSetSlotClipThreshold(Data) \
   HwdWrite(HWD_DO_TXHA_CLIP_THR1, (Data))

/* Define Macro that manages the HWD_DO_TXHA_CLIP_THR2 register */
#define RcpSetHalfSlotClipThreshold(Data) \
   HwdWrite(HWD_DO_TXHA_CLIP_THR2, (Data))

#if defined (MTK_PLT_ON_PC)
/* Define Macro that manages the HWD_STDO_PDM_SU register */
#define RcpDelayLoadingWindowSize(Data) \
   HwdWrite(HWD_STDO_PDM_SU, (Data))
#endif

/* Define Macro that manages the HWD_STDO_TX_EARLY_STB register */
#define RcpTxEarlyStbTime(Data) \
   HwdWrite(HWD_STDO_TX_EARLY_STB, (Data))

/* Define Macro that manages the HWD_STDO_KS_CALC_STB register */
#define RcpKsCalcStbTime(Data) \
   HwdWrite(HWD_STDO_TX_KS_CALC_STB, (Data))

#define RcpDelayLoadingRegAccessEnableCP()
#define RcpDelayLoadingRegAccessDisableCP()

/* Define Macro that manages the HWD_TXON_DELAY_LOAD_DIS_CP register */
/* Jason : need to control bit by bit later */
#define RcpDelayLoadingTxONRegAccessEnableCP() \
   HwdWrite(HWD_TXON_DELAY_LOAD_DIS_CP, 0x3ff)

/* Define Macro that manages the HWD_TXON_DELAY_LOAD_DIS_CP register */
/* Jason : need to control bit by bit later */
#define RcpDelayLoadingTxONRegAccessDisableCP() \
   HwdWrite(HWD_TXON_DELAY_LOAD_DIS_CP, 0x0)

/* Define Macro that manages the HWD_PDM_DELAY_LOAD_DIS_CP register */
/* Jason : need to control bit by bit later */
#define RcpDelayLoadingPDMRegAccessEnableCP() \
   HwdWrite(HWD_PDM_DELAY_LOAD_DIS_CP, 0x7f)

/* Define Macro that manages the HWD_PDM_DELAY_LOAD_DIS_CP register */
/* Jason : need to control bit by bit later */
#define RcpDelayLoadingPDMRegAccessDisableCP() \
   HwdWrite(HWD_PDM_DELAY_LOAD_DIS_CP, 0x0)

/* Define Macro that manages the HWD_SPI_DELAY_LOAD_DIS_CP register */
/* Jason : need to control bit by bit later */
#define RcpDelayLoadingSPIRegAccessEnableCP() \
   HwdWrite(HWD_SPI_DELAY_LOAD_DIS_CP, 0x1)

/* Define Macro that manages the HWD_SPI_DELAY_LOAD_DIS_CP register */
/* Jason : need to control bit by bit later */
#define RcpDelayLoadingSPIRegAccessDisableCP() \
   HwdWrite(HWD_SPI_DELAY_LOAD_DIS_CP, 0x0)

#if defined (MTK_PLT_ON_PC)
/* Define Macro that manages the HWD_DIS_DELAY_LOAD_CNT register */
#define RcpDelayLoadingCounterEnable() \
   HwdWriteDLCnt(HWD_DIS_DELAY_LOAD_CNT, 0x0)
#endif

/* Define Macro that manages the HWD_DO_TXHA_BYTE_SWAP register */
#define RcpByteSwapEnable() \
   HwdWrite(HWD_DO_TXHA_BYTE_SWAP, 0x1)

/* Define Macro that manages the HWD_DO_TXHA_BYTE_SWAP register */
#define RcpByteSwapDisable() \
   HwdWrite(HWD_DO_TXHA_BYTE_SWAP, 0x0)

/* Define Macro that manages the HWD_DO_TXHA_FREEZE register */
#define RcpDoTxFreeze() \
   HwdWrite(HWD_DO_TXHA_FREEZE, 0x1)

/* Define Macro that manages the HWD_DO_TXHA_FREEZE register */
#define RcpDoTxUnfreeze() \
   HwdWrite(HWD_DO_TXHA_FREEZE, 0x0)

/* Define Macro that initialize all channel scale except DRC and Pilot */
#define RcpDoInitializeChannelScale() \
      RcpSetChannelScale( RCP_REV_DSC_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 );   \
      RcpSetChannelScale( RCP_REV_ACK_SUP_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DRC_BOOST_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DSC_BOOST_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 );  \
      RcpSetChannelScale( RCP_REV_ACK_MUP_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 );  \
      RcpSetChannelScale( RCP_REV_DATA_NAK_SCALE0, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_NAK_SCALE1, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_NAK_SCALE2, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_NAK_SCALE3, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_NAK_SCALE,  RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 );  \
      RcpSetChannelScale( RCP_REV_DATA_ACK_SCALE0, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_ACK_SCALE1, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_ACK_SCALE2, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_ACK_SCALE3, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_DATA_ACK_SCALE,  RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 );  \
      RcpSetChannelScale( RCP_REV_RRI_ACK_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_RRI_NAK_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 );   \
      RcpSetChannelScale( RCP_REV_AUX_PILOT_ACK_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 ); \
      RcpSetChannelScale( RCP_REV_AUX_PILOT_NAK_SCALE, RCP_CHAN_SCALE_TYPE_LINEAR, 0, 0 )

/* Define Macro that manages the HWD_DO_TXHA_CL_TEST_MODE register */
#define RcpDoTxClSetTestMode() \
   HwdWrite(HWD_DO_TXHA_CL_TEST_MODE, 0x1)

/* Define Macro that manages the HWD_DO_TXHA_CL_TEST_MODE register */
#define RcpDoTxClSetFunctionalMode() \
   HwdWrite(HWD_DO_TXHA_CL_TEST_MODE, 0x0)

/* Define Macro that manages the HWD_DO_TXHA_CL_TEST_DATA register - bit2:mutil/signle user, bit1: ack/nak, bit0 : ack channel enable */
#define RcpDoTxClSetFwdAckData(Data) \
   HwdWrite(HWD_DO_TXHA_CL_TEST_DATA, (HwdRead(HWD_DO_TXHA_CL_TEST_DATA) & ~(uint16)0x0007) | (Data&0x7))

/* Define Macro that manages the HWD_DO_TXHA_CL_TEST_DATA register - bit3*/
#define RcpDoTxClSetRevAckData(Data) \
   HwdWrite(HWD_DO_TXHA_CL_TEST_DATA, (HwdRead(HWD_DO_TXHA_CL_TEST_DATA) & ~(uint16)0x0008) | ((Data&0x1)<<3))

/* Define Macro that manages the HWD_DO_TXHA_CL_TEST_DATA register - bit4 */
#define RcpDoTxClSetNoFwdDataInd(Data) \
   HwdWrite(HWD_DO_TXHA_CL_TEST_DATA, (HwdRead(HWD_DO_TXHA_CL_TEST_DATA) & ~(uint16)0x0010) | ((Data&0x1)<<4))

/* Define Macro that manages the HWD_DMA_TX_CTRL register */
#define RcpDoTxDMAHwAckEnable() \
   HwdWrite32(HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL) | (uint32)TXHA_DMA_TX_HW_ACK_MASK) )

/* Define Macro that manages the HWD_DMA_TX_CTRL register */
#define RcpDoTxDMAHwAckDisable() \
   HwdWrite32(HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL) & ~(uint32)TXHA_DMA_TX_HW_ACK_MASK) )

/* Define Macro that manages the HWD_DMA_TX_CTRL register */
#define RcpDoTxDMAEnable() \
   HwdWrite32(HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL) | (uint32)TXHA_DMA_TX_ENABLE) )

/* Define Macro that manages the HWD_DMA_TX_CTRL register */
#define RcpDoTxDMADisable() \
   HwdWrite32(HWD_DMA_TX_CTRL, (HwdRead32(HWD_DMA_TX_CTRL) & ~(uint32)TXHA_DMA_TX_ENABLE) )

/* Define Macro that manages the HWD_DMA_CHAN4_7_REQ_CONNECTION register - channel 7 for TxFIFO */
#define RcpDoTxFIFOReqEnable() \
   HwdWrite32(HWD_DMA_CHAN4_7_REQ_CONNECTION,(HwdRead32(HWD_DMA_CHAN4_7_REQ_CONNECTION)&(~(uint32)TXHA_DMA_CHAN7_REQ_CONNECTION_MASK))|((uint32)TXHA_DMA_TXFIFO_REQ_CONNECTION) );  /* TxFifo */

/*
   HwdWrite32(HWD_DMA_CHAN4_7_REQ_CONNECTION,(HwdRead32(HWD_DMA_CHAN4_7_REQ_CONNECTION)&(~(uint32)TXHA_DMA_CHAN7_REQ_CONNECTION_MASK))|((uint32)TXHA_DMA_TXFIFO_REQ_CONNECTION) );
*/


#if defined (MTK_PLT_ON_PC)
/* In DO Delay load mode, Set a selected TX_ON for Delay load mode (active high) */
#define DoDelayTxOnSet(TxOnDelayMode, hslot_boundary) \
    { \
      if (hslot_boundary) { \
         uint32 DlyDinReg = (hslot_boundary==TX_SLOT_BOUNDARY_LOAD)? HWD_TX_ON_DLY_DIN1_CFG : HWD_TX_ON_DLY_DIN2_CFG; \
         HwdSetBit16(DlyDinReg, (0x1<<TxOnDelayMode) ); \
      } \
    }

/* In DO Delay load mode, Clear a selected TX_ON for Delay load mode (active high) */
#define DoDelayTxOnClear(TxOnDelayMode, hslot_boundary) \
    { \
      if (hslot_boundary) { \
         uint32 DlyDinReg = (hslot_boundary==TX_SLOT_BOUNDARY_LOAD)? HWD_TX_ON_DLY_DIN1_CFG : HWD_TX_ON_DLY_DIN2_CFG; \
         HwdClearBit16(DlyDinReg, (0x1<<TxOnDelayMode) ); \
      } \
    }
#endif

/* Define default ks_calc strobe setup time before fiq - HWD_STDO_KS_CALC_STB register */
#define DO_TXHA_KS_CALC_STB_SETUP_FIQ 4

extern const uint16 EnableTxDlyDin1Mask[];
extern const uint16 EnableTxDlyDin2Mask[];
extern const uint16 DisableTxDlyDin1Mask[];
extern const uint16 DisableTxDlyDin2Mask[];

/* Define Macro that manages the HWD_TXON_DLY_MASK and HWD_TXON_DLY_MASK2 registers for
** enabling/disabling TXON delay load data */
#define DisableTxOnDelayLoad() \
   { \
      HwdWrite(RcpPA1DlyMaskRegAddress, HwdRead(RcpPA1DlyMaskReadRegAddress) & (DisableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA]) );  \
      HwdWrite(RcpPA1DlyMaskRegAddress, HwdRead(RcpPA1DlyMaskReadRegAddress) & (DisableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA]) );  \
      if ( RcpPA2DlyMaskRegAddress ) \
      { \
         HwdWrite(RcpPA2DlyMaskRegAddress, HwdRead(RcpPA2DlyMaskReadRegAddress) & (DisableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA2]) ); \
         HwdWrite(RcpPA2DlyMaskRegAddress, HwdRead(RcpPA2DlyMaskReadRegAddress) & (DisableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA2]) ); \
      } \
   }

#define DisableTxOnDelayLoadFirst() \
   { \
      HwdWrite(RcpPA1DlyMaskRegAddress, HwdRead(RcpPA1DlyMaskReadRegAddress) & (DisableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA]) );  \
      if ( RcpPA2DlyMaskRegAddress ) \
         HwdWrite(RcpPA2DlyMaskRegAddress, HwdRead(RcpPA2DlyMaskReadRegAddress) & (DisableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA2]) ); \
   }

#define DisableTxOnDelayLoadSecond() \
   { \
      HwdWrite(RcpPA1DlyMaskRegAddress, HwdRead(RcpPA1DlyMaskReadRegAddress) & (DisableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA]) );  \
      if ( RcpPA2DlyMaskRegAddress ) \
         HwdWrite(RcpPA2DlyMaskRegAddress, HwdRead(RcpPA2DlyMaskReadRegAddress) & (DisableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA2]) ); \
   }

#define EnableTxOnDelayLoad() \
   { \
       HwdWrite(HWD_TXON_DLY_MASK, 0 ); \
       HwdWrite(HWD_TXON_DLY_MASK2, 0 ); \
       HwdWrite(RcpPA1DlyMaskRegAddress, (EnableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA]) | (uint16)(EnableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA]) ); \
      if( RcpPA2DlyMaskRegAddress ) \
         HwdWrite(RcpPA2DlyMaskRegAddress, HwdRead(RcpPA2DlyMaskReadRegAddress) | \
            (EnableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA2]) | (EnableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA2]) ); \
   }

#define EnableTxOnDelayLoadFirst() \
   { \
      HwdWrite(RcpPA1DlyMaskRegAddress, HwdRead(RcpPA1DlyMaskReadRegAddress) | (EnableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA]) ); \
      if( RcpPA2DlyMaskRegAddress ) \
         HwdWrite(RcpPA2DlyMaskRegAddress, HwdRead(RcpPA2DlyMaskReadRegAddress) | (EnableTxDlyDin1Mask[RcpTxDlyMaskBitMaskPA2]) ); \
   }

#define EnableTxOnDelayLoadSecond() \
   { \
      HwdWrite(RcpPA1DlyMaskRegAddress, HwdRead(RcpPA1DlyMaskReadRegAddress) | (EnableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA]) ); \
      if( RcpPA2DlyMaskRegAddress ) \
         HwdWrite(RcpPA2DlyMaskRegAddress, HwdRead(RcpPA2DlyMaskReadRegAddress) | (EnableTxDlyDin2Mask[RcpTxDlyMaskBitMaskPA2]) ); \
   }

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
/* rcptask.c */
extern void RcpInit( void );
extern void RcpTask(uint32 argc, void *argv);

/**************************************************************************************
 SS: the CpBufTypeT parameter was added to take care of the
 special case, when the DatapktGT* was being allocated for a
 HDR i.e. when used as: DatapktGet( CPBUF_HEADER ); Refer code for details..
 In all other cases, the type field is a don't care. i.e. when invoked as follows
 (a) DatapktGet( CPBUF_REV ) (b) DatapktGet( CPBUF_SIGNALING_MSG )
*************************************************************************************/
extern DatapktGT* DatapktGet( CpBufTypeT type );
                               /* The CpBufTypeT parameter was added to take care of the
                                  special case, when the DatapktGT* was being allocated for a
                                  HDR i.e. when used as: DatapktGet( CPBUF_HEADER ); Refer code for details..
                                  In all other cases, the type field is a don't care. i.e. when invoked as follows
                                  (a) DatapktGet( CPBUF_REV ) (b) DatapktGet( CPBUF_SIGNALING_MSG ) */

extern void       DatapktFree( DatapktGT *pdu );        /* Frees pdu, add back to Pool */
extern void       DatapktlistAdd( DatapktlistGT *pktlist, DatapktGT *newDatapkt );  /* Adds to tail of a pktlist */
extern void       DatapktlistInit( DatapktlistGT *pktlist );
extern void       DatapktlistRemoveLast(DatapktlistGT * pktlist);

extern RevMacPktGT* RevMacPktGet( void );               /* Get a new pdu from RevMacPktPool */
extern void         RevMacPktFree( RevMacPktGT *pkt );  /* Frees revMacPkt */

extern void RcpStHwProgLongCode( uint32 lcmL, uint16 lcmH );
extern void RcpStHwProgFrameOffset( uint8 frameOffset );


/* rcptest.c */
extern bool   RcpProcTestMsg(void *MsgDataPtr, RcpTestMsgT MsgId, uint32 MsgSize);

/* CPBuf Test Cmds */
extern uint8 CpBufTestBufferCount;


/* rcptxh.c */
extern void   RcpTxhInit(void);
extern void   RcpAccessChannelPathSet(int16 PowerBaseDbmQ7);
extern void   RcpAccessChannelPathRelease(void);
extern void   RcpTrafficChannelPathSet(int16 PowerBaseDbmQ7, bool isSlientExit);
extern void   RcpTrafficChannelPathRelease(RcpTxTrafficReleaseTypeT ReleaseType);
extern void   RcpSetChannelScale(RcpRevChanScaleT Channel, RcpChanScaleTypeT Type, int16 ChanGain, uint8 Qval);
extern uint32 RcpLinearChanScaleConv(int16 Scale);
extern int16  RcpGetCurrentMeanRxPwr(void);
extern int16  RcpGetCurrentTxPilotPwr(RcpTxPilotPwrClipT TxPilotPwrClip, bool ClosedLoopEnable, bool AdjEnable);
extern int16  RcpGetCurrentTxTotalPwr(RcpTxPwrSlotHslotT TxPwrSlotHslot);
extern int16  RcpGetCurrentKsAdjust(void);
extern int16  RcpGetTchInitialKsAdjust(int8 drcChanGainBase, int8 ackChanGainBase);
extern int16  RcpGetMaxTxPwr(void);
extern int16  RcpGetAvailableTxPwr(void);
extern int16  RcpGetCurrentKs(RcpTxPwrSlotHslotT TxPwrSlotHslot);
extern void   RcpSetTxAgcClosedLoopStepSize(RcpTxAgcClosedLoopStepSizeT StepSize);
extern void   RcpHwdSetTxMaxPwr(int16 txMaxPwr, HwdRfBandT RfBand);
extern void   RcpHwdSetTxMinPwr(int16 txMinPwr, HwdRfBandT RfBand);
extern void   RcpHwdSetTxPwrAdj(int16 txPwrAdjTotal, uint8 GainState, HwdRfBandT RfBand);
extern void   RcpTxAgcDataCollect(void);
extern void   RcpTxAgcSpyReport(void);
extern void   RcpDoSlotTxAgcSpyReport(void);
extern void   RcpDoSlotKsSpyReport(void);
extern void   RcpDoSlotClipperSpyReport(void);
extern void   RcpDoSlotDRCChannelSpyReport(void);
#if (defined(MTK_PLT_ON_PC))
extern void   RcpPhyTchTestDataWrite(RcpPhyTch01TestDataWriteMsgT *MsgDataPtr);
extern void   RcpPhyAchTest(RcpPhyAchTestMsgT *MsgDataPtr);
extern void   RcpPhyTch01Test(RcpPhyTch01TestMsgT *MsgDataPtr);
extern void   RcpPhyTch2Test(RcpPhyTch2TestMsgT *MsgDataPtr);
extern void   RcpPhyTxDMATestCfg(RcpTxDMATestMsgT *MsgDataPtr);
extern void   RcpPhyTxDMATestStart(RcpTxDMATestStartMsgT *MsgDataPtr);
extern void   RcpRtmQosTxModeSelectionAlgo(RcpRtmQosTxModeSelectionAlgoMsgT *MsgDataPtr);

extern void   CpBufProcessTestStatsCmd( CpBufTestCmdMsgT *MsgDataPtr);
extern void   CpBufProcessTestGetCmd( CpBufTestCmdMsgT *MsgDataPtr);
extern void   CpBufProcessTestFreeCmd( CpBufTestFreeCmdMsgT *MsgDataPtr);
#endif

extern void   RcpPhyTxDMATestRead(RcpTxDmaReadMsgT *MsgDataPtr);

/* rcptxh_isr.c */
extern void   RcpProgramTxGainCtrl(uint8 TxGainState, uint8 TxGainStatePrev, int16 TxPower, RcpTxLoadBoundaryT TxLoadBoundary);

/*pcpr.c*/
extern uint32 PcpRFormRevDummyMacPkt ( PcpAcmRtmGrantAllocationT *grant, DatapktlistGT* pktList);
extern uint32 PcpRProcessGrant( PcpAcmRtmGrantAllocationT *grant, DatapktlistGT* datapktList) ;
extern uint8  PcpRProcessGrantAcm(PcpAcmRtmGrantAllocationT *rcvdGrant, DatapktlistGT* datapktList);
extern uint8  PcprMacFlowListAddFuncPtrs(uint16 SubType,
                                                               MacFlow2StrFlowMappingT* pTable,
                                                               bool  (*getMaxPktPriorityFunc)( BestPktInfoT*, uint8),
                                                               void  (*phyTrafficAckedFunc)(uint8,uint8),
                                                               void  (*phyTrafficMissedFunc)(uint8,uint8),
                                                               void  (*pktSentFunc)(uint8,uint8),
                                                               void  (*pktNotSentFunc)(uint8,uint8),
                                                               uint16 (*grant)( DatapktlistGT* , uint16 grantSize, uint8 revMacId, uint8 subStreamNum),
                                                               uint32 (*getPktQueSz)(uint8 macFlowId, uint8 subStreamNum)
                                                               );
extern bool PcpRAddPadBits( DatapktlistGT* datapktList, uint16 val );
extern void PcpRPktSent( uint8 revMacId );
extern void PcpRPktNotSent( uint8 revMacId );
extern void PcpRPhyTrafficAcked( uint8 revMacId );
extern void PcpRPhyTrafficMissed( uint8 revMacId );
extern void RcpAddNodeToMacFlowList( uint8 macFlowId, uint16 streamId, uint8 substreamId, bool NoCheckRlpActive );
extern void RcpAddAppToMacFlow(uint16 AppType, uint8 StreamNum);
extern uint16 RcpFindAppTypeByStreamNum(bool InUse,uint8 StreamNum);
extern uint8  RcpFindStreamNumByAppType(bool InUse,uint16 AppType);
extern void RcpDeleteNodeFromMacFlowList( void );

extern void StreamInit(void);
extern void StreamHandleStrCommitMsg(void);
extern void StreamHandleStrReconfiguredMsg(void);
extern void StreamCommit(void);

extern bool GetRtapTestPktMode(void);

extern void PcpProcessGrantRtmTest
(
   PcpAcmRtmGrantAllocationT *grant,
   DatapktlistGT             *pktlist );

/* NST: used in NST */
extern void PcpProcessGrantRtmNST(DatapktlistGT *pktlist);

#ifdef MTK_DEV_C2K_IRAT
extern void RcpIratTxPowerReqHandler(void);
#endif /* MTK_DEV_C2K_IRAT */

/* Timing Profile Measurement */
/* Macro does nothing */
#define RCP_TIME_PROFILE_START() ((void) 0)
#define RCP_TIME_PROFILE_END()   ((void) 0)

void  RcpGetTxDataStat(uint32* pTotalTxBytes, uint32* pTotalTxNewPkts);
void  RcpParSetDefServiceRlpFlow(uint8 RlpFlow);
uint8 RcpParGetDefServiceRlpFlow(void);

/* txdma.c */
extern void RcpDmaMdmTxDoneLisr (void);
extern void TxDmaReset(void);

#if (!defined(MTK_PLT_ON_PC))
bool RcpPreburstIsOn(void);
#endif

extern void RcpTxChanSweepModeRtmCommit(void);
extern void RtmTxSettleDownStHandleRcpTaskSig(void);
extern void RtmTxhProgActivateTCH(void);
extern void RtmTxPathNotSetStHandleRcpTaskSig(void);
extern void RtmTxAgcNotOnStHandleRcpTaskSig(void);
extern uint8 RtmStateSilentChk(void);

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:15:23 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:19:41 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
