/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _HWDLINKAPI_H_
#define _HWDLINKAPI_H_

#include "ipctaskapi.h"
#include "lmdapi.h"
#include "hwdlinklog.h"
#include "dspiapi.h"

// For bringup not use DTS
//#ifdef MTK_AUDIO_BRINGUP
    #define C2K_DTS_DISABLE
//#endif

// For no SAL on integrate stage
//#define C2K_SAL_DISABLE

// For no Sph Drv on integrate stage
//#define C2K_SDRV_DISABLE

// For bring up
//#define C2K_BRING_UP

// For LBK regular timer
//#define C2K_LBK_TIMER_ENABLE

//#define SALI_DISABLE
#define C2K_AP_ENCRYPTION

#define C2K_SPH_EVRCNW_SUPPORT

#define C2K_COD_RS_1      1
#define C2K_COD_RS_2      2
#define C2K_COD_RS_UNDEF  0x29

#define C2K_COD_RS_1_BLANK_LEN_WORD16          0
#define C2K_COD_RS_1_EIGHTH_LEN_WORD16         1
#define C2K_COD_RS_1_QUARTER_LEN_WORD16        3
#define C2K_COD_RS_1_HALF_LEN_WORD16           5 
#define C2K_COD_RS_1_FULL_LEN_WORD16           11 
#define C2K_COD_RS_1_ERASURE_LEN_WORD16        0
#define C2K_COD_RS_1_FULL_LIKELY_LEN_WORD16    11

#define C2K_COD_RS_2_BLANK_LEN_WORD16          0
#define C2K_COD_RS_2_EIGHTH_LEN_WORD16         2
#define C2K_COD_RS_2_QUARTER_LEN_WORD16        4
#define C2K_COD_RS_2_HALF_LEN_WORD16           8 
#define C2K_COD_RS_2_FULL_LEN_WORD16           17 
#define C2K_COD_RS_2_ERASURE_LEN_WORD16        0

#define C2K_COD_RATE_MAX_LEN_WORD16            17

#define C2K_COD_QCELP8K         0x30
#define C2K_COD_QCELP13K        0x31
#define C2K_COD_EVRCA           0x32
#define C2K_COD_EVRCB           0x33
#define C2K_COD_EVRCNW_NB       0x34
#define C2K_COD_EVRCNW_WB       0x35
#define C2K_COD_UNDEF           0x39

#define C2K_LMD_LBK     1
#define C2K_CAAL_LBK    2

// SOCM
#define C2K_SOCM_EVRCA_RATE_REDUC_MIN       0
#define C2K_SOCM_EVRCA_RATE_REDUC_MAX       4
#define C2K_SOCM_QCELP13K_RATE_REDUC_MIN    0
#define C2K_SOCM_QCELP13K_RATE_REDUC_MAX    4
#define C2K_SOCM_EVRCB_RATE_REDUC_MIN       0
#define C2K_SOCM_EVRCB_RATE_REDUC_MAX       7
#define C2K_SOCM_EVRCNW_RATE_REDUC_MIN      0
#define C2K_SOCM_EVRCNW_RATE_REDUC_MAX      7

#define C2K_SOCM_RATE_REDUC_SHIFT       5
#define C2K_SOCM_MOBILE_TO_MOBILE_SHIFT 1

// DelR/W/M (ms)
#define C2K_DELR     8.25       
#define C2K_DELW     3
#define C2K_DELM     4             

// Dbg
#define C2K_DBG_DSV_ISR              0xA000
#define C2K_DBG_APP_MODE             0xD000
#define C2K_DBG_APP_MODE_RSP         0xD001
#define C2K_DBG_SSO_CONN             0xD002
#define C2K_DBG_SSO_CONN_RSP         0xD003
#define C2K_DBG_SSO_DISCONN          0xD004
#define C2K_DBG_SSO_DISCONN_RSP      0xD005
#define C2K_DBG_FWD_CHECK            0xD006
#define C2K_DBG_APPS_RESET           0xD007

#define HWD_DBG_STATE_NOT_INITIALIZED             0xE000
#define HWD_DBG_STATE_READY                       0xE001
#define HWD_DBG_STATE_CODE_DOWNLOADING1           0xE002
#define HWD_DBG_STATE_APP_DOWNLOADING             0xE003
#define HWD_DBG_STATE_CODE_DOWNLOADING2           0xE004
#define HWD_DBG_STATE_AUDIO_INIT                  0xE005

#define LMD_ASYN_MAILBOX           EXE_MESSAGE_MBOX_5

typedef struct
{
    uint32 u4HBFrmCountUL1, u4HBFrmCountUL2;
    uint32 u4HBFrmCountDL1, u4HBFrmCountDL2;
    bool bSta;
    bool bCodRdy;
    bool bFstDLFrm;
    bool bAppSta;
    bool bLMDLbk;
    bool bCAALLbk;
    uint16 u2Cod;
    uint16 u2RS;
    uint16 u2HBTemp[11];

}SPC2K_Struct;


typedef struct
{
    uint16        u2Mode;
    uint16        u2SO;
    bool          bStart;
        
}HwdSphLbkMsgT;

enum CAALMsgId
{
    CAAL_SSO_CONN_RSP_MSG = 0,
    CAAL_SSO_DISCONN_RSP_MSG,
    CAAL_SPH_DATA_UL_MSG
};

enum SDrvMsgId
{
    SDRV_SSO_CONN_DONE = IPC_CP_FROM_SDRV_MIN_MSG,
    SDRV_SSO_DISCONN_DONE,
    SDRV_UL_GET_FRAME
    
};

enum SphEtsMsgId
{
    SPH_ETS_READ_DSP_MEM = IPC_CP_FROM_SPH_ETS_MIN_MSG,
    SPH_ETS_LBK,
    SPH_ETS_FRC_TONE
};

void SPC2K_init( void );
void SPC2K_ConSSO_Req( uint16 u2SrvOpt, uint16 u2MaxEncRate);
void SPC2K_DisconSSO_Req( void );
void SPC2K_ConSSO_Done( void );
void SPC2K_DisconSSO_Done( void );
void SPC2K_ConSSO_Done_Rsp( void );
void SPC2K_DisconSSO_Done_Rsp( void );
bool SPC2K_State( void );
void SPC2K_UL_GetSpeechFrame( void );
#ifdef C2K_AP_ENCRYPTION
void SPC2K_UL_GetSpeechFrame_HISR( void );
#endif
void SPC2K_DL_PutSpeechFrame(IpcDsvSendSpkrVoiceFwdChPcktDataMsgT *MsgFLDataP);
void SPC2K_DL_PutSpeechFrame_Do(IpcDsvSendSpkrVoiceFwdChPcktDataMsgT *MsgFLDataP);
void SPC2K_SOCM_Set(IpcDsvSendSsoCtrlParamsMsgT *pMsg);
void SPC2K_EncMaxRate_Set(IpcDsvSetMicVoiceEncMaxRateMsgT *pMsg);
void SPC2K_GetSyncDelayRW( uint16 *u2DelR, uint16 *u2DelW, uint16 *u2DelM);
#ifdef MTK_PLT_AUDIO
void SPC2K_Debug_Info(IpcDsvSendAudioChanQltyMsgT * pMsg);
void SPC2K_Link_Lbk(HwdSphLbkMsgT *pMsg);
#endif

extern SPC2K_Struct spc2k;
extern uint16 au2ToneTab1K[11];

#endif
