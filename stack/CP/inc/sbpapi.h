/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/** \file
 *
 * Filename:
 * ---------
 *   sbpapi.h
 *
 * Project:
 * --------
 *   C2K
 *
 * Description:
 * ------------
 * This file defines the enumerations, structures and interfaces of C2K SBP feature.
 *
 * Author:
 * -------
 * Clin Yang
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Log$
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/
#ifndef _SBPAPI_H
#define _SBPAPI_H

/*****************************************************************************
* DESCRIPTION
*   This enum is defined for Single Binary Platform (SBP) features
*   NVRAM saves one binary value for each feature
*****************************************************************************/
typedef enum
{
    SBP_STATUS_SLC_INVALID_SECTORID_CHK,    /*  SLC_INVALID_SECTORID_CHK;   Whether need to check invalid sectorID for EV-DO.   */
    SBP_STATUS_EXT_SMS_PB_INIT,    /*   SYS_EXT_SMS_INIT_FEATURE in CarrierFeatureMatrix[] of current C2K code  ;   SMS and Phonebook save in share memory during initialization period.    */
    SBP_STATUS_MODEM_TURNON_AUTO,    /* FEATURE_PS_NOT_TURNON_AUTO  ;   Modem auto turn on without AP   */
    SBP_STATUS_CHINATELECOM_CARDLOCK,    /* CHINATELECOM_CARDLOCK   ;   Whether only CT card is allowed */
    SBP_STATUS_CHINATELECOM_EXTENSIONS,    /*   CHINATELECOM_EXTENSIONS ;   Carrier switch, extending for China Telecom. +VROM, +VSER and other URCs are turned OFF by defined when this macro is defined. UTK response messages are required when this macro is defined.   */
    SBP_STATUS_SYS_OPTION_OTASP,    /*  SYS_OPTION_OTASP;   OTASP Enable/disable switch     */
    SBP_STATUS_ALTERNATE_PN,    /*  ALTERNATE_PN;   enable/disable ALTERNATE PN feature     */
    SBP_STATUS_32K_LESS,    /*  MTK_DEV_32K_LESS_ON ;   MTK_DEV_32K_LESS_ON */
    SBP_STATUS_CO_CLOCK,    /*  MTK_DEV_FIX_AFC_OFF ;   Not Supported (Macro)   */
    SBP_STATUS_SHDR_HYBRID,    /*   MTK_DEV_HSC_SHDR;   Single Bin (NVRAM customer file)    */
    SBP_STATUS_1X_ADV_SUPPORT,    /*    Function;   support 1x Adv feature or not   */
    SBP_STATUS_SYS_REGISTRATION_THROTTLING_FEATURE,    /*   Function;   Registration Throttling to avoid draining battery in area where MS continously fails to register due to Max Access  Probe failures. */
    SBP_STATUS_SYS_SAFETY_NET_REGISTRATION_FEATURE,    /*   Function;   Perform safety net registration in network that may result in the network not knowing the whereabouts of the MS for extended periods.   */
    SBP_STATUS_SYS_ENH_SYSTEM_SELECT_FEATURE,    /* Function;   Enhanced System Select with AutoA and AutoB options.    */
    SBP_STATUS_SYS_ERI_FEATURE,    /*   Function;   Enhanced Roaming Indicator. */
    SBP_STATUS_SYS_CSS_1X_MAPE_HOME_SYS_AVOID_FEATURE,    /*    Function;   Home system avoidance upon MAPE rejistration failure    */
    SBP_STATUS_SYS_CSS_1X_RESTRICT_SILENTRETRY_TO_SAME_GEO_FEATURE,    /*   Function;   Only same GEO Systems will be accepted in Silent Retry state    */
    SBP_STATUS_SYS_CSS_1X_USE_NAM_FOR_VALIDATION_FEATURE,    /* Function;   A 1x System not found in the PRL,not negative in NAM but found in the NAM positive SID NID list will be declared Home and accepted(pref_only=false) */
    SBP_STATUS_SYS_CSS_1X_ONLY_REJECT_REDIR_IF_NEG_IN_PRL_FEATURE,    /*    Function;   Reject the redirected system only if found negative in PRL or NAM   */
    SBP_STATUS_MIP,    /*   Function;   MIP feature */
    SBP_STATUS_MIP_DMU,    /*   SYS_OPTION_MIP_DMU  ;   whether support DMU for MIP or not. Supported only when SBP_STATUS_MIP is 1 */
    SBP_STATUS_IPV6,    /*  Function;   IPv6 feature, protocol stack    */
    SBP_STATUS_VZW_DATA_ENHANCEMENT,    /*  Function;   VZW data service special feature    */
    SBP_STATUS_GMSS_MRU, /* C2K MRU stored in GMSS */
    SBP_STATUS_IDLE_GEO_LIST, /* Idle GEO list scan algorithm */
    SBP_STATUS_1X_PS_DELAY, /* delay to establish data connecivity on 1xRTT */
    SBP_STATUS_VZW_TDO, /* delay to select a EVDO system if 1xRTT system has not been found */
    SBP_STATUS_VZW_E911, /*  E911 trying on all RATs */
    SBP_STATUS_SILENT_REDIAL_ON_SIB8, /* hVolte silent redial using cells in SIB8 */
    SBP_STATUS_VZW_MEID_ME, /* Report MEID_ME to NW through (extended)status request/response message */
    SBP_STATUS_VZW_OTA, /* Code for VZW_OTA use. */
    SBP_STATUS_VZW_SMS,
    SBP_STATUS_VZW_SMS_CMAS,
    SBP_STATUS_DO_APERSISTENCE_AVOID, /* abandon the EVDO network if Apersistence is 0x3F */
    SBP_STATUS_HVOLTE,          /* to define volte feature */
    SBP_STATUS_BTSAP,  /* Function; Bluetooth access SIM profile feature   */
    SBP_STATUS_VZW_TX_CLOSELOOP,
    SBP_STATUS_C2KONLY_OOSA_RETRIEVE_OPTIMIZE, /* SBP_STATUS_C2KONLY_OOSA_RETRIEVE_OPTIMIZE */
    SBP_STATUS_VZW_LTE_SIB8_TIMING,
    SBP_STATUS_VZW_SUPL,    /* code for VZW LBS SUPL use */
    SBP_STATUS_VZW_DISABLE_EVDO_SUSPEND_TIMING, /* DataRequirements-VZ-REQ-DATA-10470: Disable EVDO suspend time */
    SBP_STATUS_VZW_SMS_BSP,
    SBP_STATUS_CT_AUTO_REG_WHEN_MCC_CHANGED,
    SBP_STATUS_SPRINT_QC_REDIRECT,   /* code for SPRINT QC Redirect feature */
    SBP_STATUS_VZW_FEMTOCELL_FEATURE,/* Function: To Indicate whether the current system is in femtocell according to VzW spec */
    SBP_STATUS_VZW_LTE_MMO_SCAN,
    SBP_STATUS_SPRINT_1X_SYS_LOST,    /* code for ALFMS00735062 */
    SBP_STATUS_SID_MAPPING_MCC, /* use SID mapping MCC unless mapping result is invalid */
    SBP_STATUS_SPRINT_BSR,          /*Per Sprint's req,Some Sprint special BSR requirements are classified to this SBP*/
    SBP_STATUS_LBS_DATA_LOGGING, /* code for LBS data logging */
    SBP_STATUS_CDG_BSR,             /*Per Sprint's req,Some CDG requested BSR requirements are classified to this SBP.*/
    SBP_STATUS_CDG_AVOIDANCE,       /*Per Sprint's req,Some CDG requested Avoidance requirements are classified to this SBP.*/
    SBP_STATUS_SPRINT_TRAFF_NEW_CH_NOT_LOGGING_MRU, /* code for ALFMS00825294 */
    SBP_STATUS_SPRINT_PERSISTENCE_FAIL, /* Persistence fail return to IDLE for Sprint */
    SBP_STATUS_SPRINT_MO_SMS_BREAK_BSR, /* MO SMS will break BSR after power on acquiring a less preferred system */
    SBP_STATUS_SPRINT_SMS,/*sprint in ECBM send e911 sms*/
    SBP_STATUS_SESSION_NEGO_OPTM_POWERON,  /* session restore while not re-nego while Poweron */
    SBP_STATUS_SPRINT_AT_CMD, /* SPRINT requirement for AT cmd(AT$ROAM,AT+GCAP,Engineering mode) */
    SBP_PRL_ENHANCE_FOR_INT_ROAM,      /* PRL Enhancements for International Roaming per CDG 86 */
    SBP_CDG143_MAPE_SYS_AVOID_FEATURE, /* CDG143 Req. The channel over which access failed is placed last in the channels list */
    SBP_VOICE_ROAM_BARRING,           /* Ability to reject International and or Domestic roaming based on ERI */
    SBP_USE_RESTRICTIVE_SYSTEM_MATCH_FEATURE, /* When matching an SID/NID/Subnet ID in the PRL, use more restrictive SID/NID/Subnet ID matching using the band class and channel */
    SBP_ASSIGN_MRU0_IF_EMPTY,        /* Customer Equipment MUST assign the first mode/band/channel combination listed on the PRL to MRU(0) if MRU is empty at power-up. */
    SBP_STATUS_SPRINT_SMS_DISABLE_SO_NEG,        /* Sprint's req, service option negotiation is not supported for SMS service. */
    SBP_STATUS_SPRINT_SMS_CMAS,/*sprint cmas sms*/
    SBP_STATUS_POWERUP_1X_SCAN, /* scan 1x and not waiting for GMSS cmd */
    SBP_FORCE_VOICE_GATING_ENABLE,        /* Enable the Force Voice Gating Funciton in CT and Sprint*/
    SBP_STATUS_SPRINT_BSR_DATA_HOLD,          /*Per Sprint's req,Some Sprint special BSR requirements are classified to this SBP*/
    SBP_STATUS_SPRINT_OPTM_WARM_RESET, /* session restore and not re-nego IDP and keepalive while warm reset for SPRINT */
    SBP_BAR_DATA_ROAMING,
    SBP_VZW_PSW_BYPASS_BAD_SECTOR, /* bypass bad sector in sync acq phase for VZW. */
    SBP_SO2_OPEN_GPS_EARLY,/*open gps early when so=2 */
    SBP_STATUS_SPRINT_REPORT_SO, /* report so to user */
    SBP_PAGING_MONITOR_TIMEOUT_TO_GEOSCAN, /* enter GEO scan when PAGING_MONITOR_TIMEOUT received in idle */
    SBP_STATUS_DO_IRAT_PRIORITY_DOWNGRADING, /* downgrade IRAT priortiy if specfic conditions are matched */
    SBP_STATUS_SPRINT_CALL_RELEASE, /* sprint feature, Upon call release MS try to acquire: traffic[last](if with pch), MRU[0], system lost based on MRU[0]  */
    SBP_STATUS_DO_PRL_LIST_ASSOCIATED_ONLY, /* only do channels associated to 1x included in do prl list */
    SBP_VZW_INTERNATIONAL_ROAMING_REG, /* for Vzw CAN Roaming GFIT case, delete registration list when do CS service in 3GPP */
    SBP_STATUS_SPRINT_TRAFF_NEW_CH_LOGGING_LAST_CHANNEL, /*for SPRINT IOT ECAM cases, save the channel in ECAM as last channel even the channel not in CCLM*/
    SBP_VZW_REFRESH_PROCEED_DURING_THE_CALL,/* according to VZW LTE_Smartphone.doc,5.2.2.1, when ISIM/USIM/CSIM refresh comes, the current call will be torn down,and refresh will be allowed */
    SBP_DEACTIVATE_C2K_WHEN_UIM_ERROR,/* turnoff c2k when uim error */
    SBP_STATUS_PS_REG_SRCH_IN_SAME_CYCLE_OPTI, /* GMSS PS_REG search in same MSPL search cycle optimization */
    SBP_STATUS_VZW_UNSOLICITED_LOCATION_NOTIFICATION, /* Unsolicited location notification update.*/
    SBP_1XRTT_ENHANCE_SILENT_RETRY, /* for CS performance, when exhaust SRSL, do system lost scan*/
    SBP_1XRTT_ENHANCE_AEHO, /* for Dallas CS Performance, open access entry handoff and UOI enhance handoff. */
    SBP_STATUS_VZW_1XRTT_SYSTEM_LOST_SEARCH_FOR_UOI_SR, /* for Dallas CS Perfromance, when system lost in UOI, do system lost scan for silent retry. */
    SBP_STATUS_VZW_1XRTT_DISABLE_AHO_OPTIMIZE, /* for Dallas CS Perfromance, disable AHO(after receiving ACK from NW) if NW not support */
    SBP_STATUS_SYS_REG_THROTTLING_ENHANCE, /* for Gionee, when report register access fail, enable throttling faeture */
    SBP_STATUS_OP01_LOCK_PROTECT,    /*    for CMCC, into limited service if protocol 1 is not CMCC card and protocal 2 is CMCC */
    SBP_STATUS_SPRINT_DDTM, /* DDTM feature switch for Sprint */
    SBP_1XRTT_DISABLE_VOICE_CALL, /* for tablet project, disable 1xRTT voice call function */
    SBP_ACG_OPS_TETHERING,
    /* new features should be added before SBP_STATUS_MAX_FEATURE. */
    SBP_STATUS_MAX_FEATURE
}c2k_sbp_status_enum;

/*****************************************************************************
* DESCRIPTION
*   This enum is defined for Single Binary Platform (SBP) features
*   NVRAM saves one byte value for each feature
*****************************************************************************/
typedef enum
{
    SBP_DATA_SUPPORT_PERSONALITY_COUNT,    /*   Count of EV-DO session personalities supported by the AT.   */
    SBP_DATA_SUPPORT_NAM_COUNT,    /*   how many group of NAM that supported by UE  */
    SBP_DATA_RUIM,    /*  RUIM is enabled or not  */
    /* new features should be added before SBP_DATA_MAX_FEATURE. */
    SBP_DATA_MAX_FEATURE
}c2k_sbp_data_enum;

/*****************************************************************************
* DESCRIPTION
*   This enum is TO define sbp_id.
*****************************************************************************/
typedef enum
{
    SBP_ID_GENERIC = 0x00,         /*   Open market.   */
    SBP_ID_CMCC = 0x01,            /*   CMCC.   */
    SBP_ID_CHINATELECOM = 0x09,    /*   CHINATELECOM.   */
    SBP_ID_VERIZON = 0x0C,         /*   VERIZON  */
    SBP_ID_SPRINT = 0x14,          /*   SPRINT  */
    SBP_ID_OPENMOBILE = 0xD4,      /*   USA Open Mobile operator 212  for LGE prj */ 
    SBP_ID_ACG   = 0xDE,           /*   sprint ACG virtual operators 222  */ 
    /* new sbp_id should be added before SBP_ID_MAX_NUMBER. */
    SBP_ID_MAX_NUMBER
}c2k_sbp_id_enum;

/*****************************************************************************
* DESCRIPTION
*   This enum is TO define solution .
*****************************************************************************/
typedef enum
{
    SBP_SOLUTION_TK,
    SBP_SOLUTION_BSP
}c2k_sbp_soluiton_enum;

/*****************************************************************************
* DESCRIPTION
*   This enum is TO define RUIM configuration.
*****************************************************************************/
typedef enum
{
    SBP_RUIM_DISABLED = 0x00,    /*   Non-RUIM configuration   */
    SBP_RUIM_ENABLED = 0x01    /*   RUIM configuration   */
}c2k_sbp_ruim_config_enum;

/**
 * SBP(Single Binary Platform) modem configuration
 * a bitmap for all modem configurable feature
 **/
typedef struct
{
    bool            first_boot;
    c2k_sbp_id_enum sbp_id;
    uint8           c2k_sbp_status_config[(SBP_STATUS_MAX_FEATURE/8)+1];
} nvram_ef_c2k_sbp_status_config_struct;

#define NVRAM_EF_C2K_SBP_STATUS_CONFIG_TOTAL (1)
#define NVRAM_EF_C2K_SBP_STATUS_CONFIG_SIZE (sizeof(nvram_ef_c2k_sbp_status_config_struct))

/**
 * SBP(Single Binary Platform) modem data configuration
 * a byte for each modem configurable feature
 **/
typedef struct
{
    uint8 c2k_sbp_data_config[SBP_DATA_MAX_FEATURE];
} nvram_ef_c2k_sbp_data_config_struct;

#define NVRAM_EF_C2K_SBP_DATA_CONFIG_TOTAL (1)
#define NVRAM_EF_C2K_SBP_DATA_CONFIG_SIZE (sizeof(nvram_ef_c2k_sbp_data_config_struct))


/*****************************************************************************
* FUNCTION
*   sbp_query_status_feature()
*
* DESCRIPTION
*   This function is used to query c2k sbp status feature configuration
*
* PARAMETERS
*   feature [IN]    c2k sbp status feature
*
* RETURNS
*   KAL_TRUE    : if this feature is turned on
*   KAL_FALSE   : if this feature is turned off
*****************************************************************************/
extern bool sbp_query_status_feature(c2k_sbp_status_enum feature);


/*****************************************************************************
* FUNCTION
*   sbp_query_data_feature()
*
* DESCRIPTION
*   This function is used to query modem configuration data
*
* PARAMETERS
*   feature [IN]    modem feature
*
* RETURNS
*   the unsigned byte value for the feature
*****************************************************************************/
extern uint8 sbp_query_data_feature(c2k_sbp_data_enum feature);


/*****************************************************************************
* FUNCTION
*   sbp_query_sbp_id()
*
* DESCRIPTION
*   This function is used to query SBP ID received by modem
*
* PARAMETERS
*   [IN]    N/A
*
* RETURNS
*   the c2k_sbp_id_enum value for the SBP ID
*****************************************************************************/
extern c2k_sbp_id_enum sbp_query_sbp_id(void);

/*****************************************************************************
 * FUNCTION
 *  sbp_set_sbp_id
 * DESCRIPTION
 * Set SBP id by operator macro, this function is only used for Modis IT/UT and
 * target which doesn't support CCCI.
 * PARAMETERS
 *  void
 * RETURNS
 *   void
 *****************************************************************************/
extern void sbp_set_sbp_id(void);


/*****************************************************************************
 * FUNCTION
 *  custom_nvram_set_sbp_id
 * DESCRIPTION
 * Set SBP features and data according to SBP ID.
 * PARAMETERS
 *  sbp_id  [IN]
 * RETURNS
 *   TRUE    : Set SBP ID successfully
 *   FALSE   : Error happens when setting SBP ID
 *****************************************************************************/
extern void custom_nvram_set_sbp_id(c2k_sbp_id_enum sbp_id);

/*****************************************************************************
 * FUNCTION
 *  sbp_set_solution_feature
 * DESCRIPTION
 * Set solution is Turkey or BSP for customer.
 * PARAMETERS
 *  tk_bsp, enum, TK or BSP
 * RETURNS
 *   void
 *****************************************************************************/
extern void sbp_set_solution_feature(c2k_sbp_soluiton_enum solu);


#endif /* _SBPAPI_H */

