/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2011 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************

  FILE NAME:  monapi.h

  DESCRIPTION:

    This file contains all the constants, mail message definition and
    function prototypes exported by the MON unit.

*****************************************************************************/
#ifndef MONAPI_H
#define MONAPI_H

#include "dspiapi.h"
#include "sysapi.h"
#include "sysdefs.h"
#include "exedefs.h"
#include "dbmapi.h"

#if defined MTK_PLT_ON_PC
#ifdef INLINE
#undef INLINE
#endif
#define INLINE __inline
#endif

/*----------------------------------------------------------------------------
 Defines Macros used in this file
----------------------------------------------------------------------------*/
#if defined (MTK_DEV_LOG_FILTER_NVRAM)
#define SPY_INFO_SIZE               ((MON_NUM_SPY_ID >> 3) + 1)
#define TRACE_INFO_SIZE             ((MON_NUM_TRACE_ID >> 3) + 1)
#define DSP_NUM_SPYTRACE_IDS        0x100                                 /* # of SPY's / TRACE's defined in DSP's */
#define DSP_SPYTRACE_INFO_SIZE      ((DSP_NUM_SPYTRACE_IDS >> 4) + 1)     /* 16 per word */
typedef NV_PACKED_PREFIX struct
{
    uint8 StartupLogFlag; /* 1: Startup Log On;   0: Startup Log Off. */
    uint8 SilentRebootFlag; /* 1: SilentReboot On;   0: SilentReboot Off. */
    uint8 Reserved0;
    uint8 Reserved1;
} NV_PACKED_POSTFIX NvramLogCtl; /* This struct is defined in uidbm.cpp */

extern const NvramLogCtl MonNvramLogCtl;
extern NvramLogCtl GlobalMonLogCtl;
#endif

#ifdef BOOT_BUILDING
#define MonTrace(TraceId, ...) do{ }while(0)
#define MonSpy(SpyId, ...) do{ }while(0)
#else
#ifdef MTK_PLT_ON_PC /* Enable all the Trace ID on PC */
#define MonTrace(TraceId, ...) _MonTrace(TraceId, __VA_ARGS__)
#define MonTraceInquire(TraceId) (TRUE) 
#define MonSpy(SpyId, ...) _MonSpy(SpyId, __VA_ARGS__)
#else
#ifndef SYS_DEBUG_NO_TRACE
extern MonTraceInfoT MonTraceInfo[];
#define MonTrace(TraceId, ...) do{if(( MonTraceInfo[(TraceId) >> 3] >> ((TraceId) & 0x07)) & 0x01) _MonTrace(TraceId, __VA_ARGS__);}while(0)
#define MonTraceInquire(TraceId) (( MonTraceInfo[(TraceId) >> 3] >> ((TraceId) & 0x07)) & 0x01) 
#else
#define MonTrace(TraceId, ...) _MonTrace(TraceId, __VA_ARGS__)
#define MonTraceInquire(TraceId) FALSE 
#endif

#define MonSpy(SpyId, ...) _MonSpy(SpyId, __VA_ARGS__)
#endif /*MTK_PLT_ON_PC*/
#endif /*BOOT_BUILDING*/

#ifdef MTK_CBP
#define MonAssert(Expr, UnitNum, FaultCode1, FaultCode2, FaultType) do {if(!(Expr))\
	                                                                        MonFault(UnitNum, FaultCode1, FaultCode2, FaultType);\
                                                                     } while (0)
#endif/*MTK_CBP*/


/*----------------------------------------------------------------------------
 Defines Constants used in this file
----------------------------------------------------------------------------*/

/* Define MON command mailbox ids */
#define MON_MAILBOX              EXE_MAILBOX_1_ID     /* Command mailbox */
#define MON_MAILBOX_EVENT        EXE_MESSAGE_MBOX_1
#define MON_MAILBOX_DNLD         EXE_MAILBOX_2_ID     /* DSP download mailbox */
#define MON_MAILBOX_DNLD_EVENT   EXE_MESSAGE_MBOX_2

#define MON_DIRECT_BUF_SIGNAL    EXE_SIGNAL_1      /* Signal from IPC direct buffer mailbox has data */
#define MON_PROFILE_SPY_SIGNAL   EXE_SIGNAL_2      /* Signal from timer to send Profile Data Spy */
#define MON_STARTUP_SYNC_SIGNAL  EXE_SIGNAL_3
#define MON_HWD_PROFILE_TIMER_SIGNAL  EXE_SIGNAL_4 /* Signal from the Hwd Profile timer */
#define MON_LOG_2_SD_CARD_SIGNAL  EXE_SIGNAL_5

/* Define MON CP peek/poke max data size in uint16 units */
#define MON_MAX_PEEK_SIZE        123
#define MON_MAX_POKE_SIZE        123

/* Define max spy data size in bytes */
#define MON_MAX_SPY_SIZE         1024 //(EXE_MAX_MSG_BUFF_SIZE - MON_SPY_HEADER_SIZE)

/* Define max printf string size in chars */
#define MON_MAX_PRINTF_STR_SIZE  128

/* Define max file info string size in chars */
#define MON_MAX_FILEINFO_STR_SIZE  96

/* Define max trace and printf number of arguments */
#define MON_MAX_TRACE_ARG_SIZE   10
#define MON_MAX_PRINTF_ARG_SIZE  10

/* Define voice test hardware control constants */
#define MON_DAI_RX_ENABLE        0x1
#define MON_DAI_TX_ENABLE        0x2
#define MON_DAI_TX_RX_ENABLE     0x3

#ifdef MTK_MODEL_STRING
#define MANU_STRING              {"MediaTek Inc."}  /* no more than 20 char */

#define MODEL_STRING             {MTK_MODEL_STRING} /* no more than 20 char */
#define OPERATOR_STRING          {"GENERIC"}      /* no more than 20 char */
#else
#define MANU_STRING              {"VIA TeleCom"}  /* no more than 20 char */

#define MODEL_STRING             {"CBP8.2"}       /* no more than 20 char */
#define OPERATOR_STRING          {"GENERIC"}      /* no more than 20 char */
#endif

/* Define deep sleep voting flags for the different units that need them.
   These defines are used as bit masks in calls to MonDeepSleepSuspend
   and MonDeepSleepResume */
#ifndef MTK_CBP
typedef enum {
   MON_HWD_SLEEPOVER_FLAG       = 0x00000001,  /* HWD: for sleepover ISR */
   MON_HWD_SLEEP_AUDIOPATH_FLAG = 0x00000002,  /* HWD: for audio path in use */
   MON_HWD_GPIO_INT_C108_FLAG   = 0x00000004,  /* HWD: for gpio CKT108 activity */
   MON_HWD_SLEEP_USBDATA_FLAG   = 0x00000008,  /* HWD: for USB data activity */
   MON_IOP_ETS_TX_SUSPEND_FLAG  = 0x00000010,  /* IOP: for ETS Tx Uart activity */
   MON_IOP_ETS_RX_SUSPEND_FLAG  = 0x00000020,  /* IOP: for ETS Rx Uart activity */
   MON_IOP_AP_UART_RX_FLAG      = 0x00000040,  /* IOP: for AP Rx Uart activity */
   MON_BM_CHRG_FLAG             = 0x00000080,  /* MON: battery charge activity */
   MON_HSC_1X_MODEM_ACTIVE_FLAG = 0x00000100,  /* HSC: for 1x modem activity */
   MON_HSC_DO_MODEM_ACTIVE_FLAG = 0x00000200,  /* HSC: for DO modem activity */
   MON_HWD_TS_SLEEP_FLAG        = 0x00000400,  /* MON: Thouch screen is activity */
   MON_HWD_SLEEP_USB_DETECT_FLAG= 0x00000800,  /* MON: For usb detect activity */
   MON_VAL_BLUETOOTH_SLEEP_FLAG        = 0x00001000,  /* val: Bluetooth FLAG */
   MON_HWD_BLUETOOTH_WAKE_UP_FLAG        = 0x00002000,  /*HWD: for bluetooth wake up*/
   MON_IOP_TX_VETO_FLAG        = 0x00004000,  /* MON: For 4 line wakeup */
   MON_VAL_SLEEP_SND_FLAG       = 0x00008000,  /* used for sound play */
   MON_HWD_KEYPAD_ACTION_FLAG   = 0x00010000,  /* HWD: for Keypad activity */
   MON_VAL_LIGHT_FLAG           = 0x00020000,  /* MON: For LCD backlight activity */
   MON_HWD_SLEEP_DSPV_INIT_FLAG = 0x00040000,  /* used for Dspv init */
   MON_HWD_SLEEP_DSPV_BUSY_FLAG = 0x00080000,  /* HWD: for DSPv busy */
   MON_HWD_SLEEP_APRDY_FLAG     = 0x00100000,  /* HWD: for communication with AP */
   MON_HWD_GENERIC_GPINT_FLAG   = 0x00200000,  /* HWD: for generic GPINT activity */
   MON_HWD_HEADSETKEY_FLAG      = 0x00400000,  /* MON: For headset key activity */
   MON_HWD_SLEEP_UIM2_FLAG      = 0x00800000,  /* MON: for UIM2 activity*/
   MON_HWD_SLEEP_UIM_FLAG       = 0x01000000,  /* HWD: for UIM activity */
   MON_HWD_SLEEP_VIBRATE_FLAG   = 0x02000000,  /* HWD: for vibrate, not used in fact */
   MON_HWD_SLEEP_MMAPPS_FLAG    = 0x04000000,  /* HWD: for multi-media Apps */
   MON_HWD_SLEEP_AUXADC_FLAG    = 0x08000000,  /* HWD: for aux adc conversions */

   MON_LEC_GPS_ACTIVITY_FLAG    = 0x10000000,  /* LEC: for GPS activity */
   MON_EEP_GPS_ACTIVITY_FLAG    = 0x20000000,  /* EEP: for GPS activity */

   MON_IOP_RX_VETO_FLAG       = 0x40000000,   /* MON: For 4 line wakeup */

   /* don't use > 0x7FFF FFFF */
} MonDeepSleepVetoT;
#else
typedef enum {
   MON_HWD_SLEEPOVER_FLAG       = 0x00000001,  /* HWD: for sleepover ISR */
   MON_HWD_SLEEP_AUDIOPATH_FLAG = 0x00000002,  /* HWD: for audio path in use */
   MON_HWD_GPIO_INT_C108_FLAG   = 0x00000004,  /* HWD: for gpio CKT108 activity */
   MON_HWD_SLEEP_USBDATA_FLAG   = 0x00000008,  /* HWD: for USB data activity */
   MON_IOP_ETS_TX_SUSPEND_FLAG  = 0x00000010,  /* IOP: for ETS Tx Uart activity */
   MON_IOP_ETS_RX_SUSPEND_FLAG  = 0x00000020,  /* IOP: for ETS Rx Uart activity */
   MON_IOP_AP_UART_RX_FLAG      = 0x00000040,  /* IOP: for AP Rx Uart activity */
   MON_32K_LESS_FLAG            = 0x00000080,  /* used for 32K-less */
   MON_HSC_1X_MODEM_ACTIVE_FLAG = 0x00000100,  /* HSC: for 1x modem activity */
   MON_HSC_DO_MODEM_ACTIVE_FLAG = 0x00000200,  /* HSC: for DO modem activity */
   #ifdef MTK_GPS_SYNC_DEV
   MON_GPS_SYNC_FLAG            = 0x00000400,  /* For GPS sync  */
   #endif 
   MON_HWD_SLEEP_USB_DETECT_FLAG= 0x00000800,  /* MON: For usb detect activity */
   MON_1X_TIME_COPY_TO_DO_FLAG  = 0x00001000,
   MON_ETS_CMD_WAKEUP_DSPM_FLAG = 0x00002000,  /*HWD: ETS command wakeup dspm*/
   MON_IOP_TX_VETO_FLAG         = 0x00004000,  /* MON: For 4 line wakeup */
   MON_VAL_SLEEP_SND_FLAG       = 0x00008000,  /* used for sound play */
   MON_HWD_KEYPAD_ACTION_FLAG   = 0x00010000,  /* HWD: for Keypad activity */
   MON_VAL_LIGHT_FLAG           = 0x00020000,  /* MON: For LCD backlight activity */
   MON_HWD_SLEEP_DSPV_INIT_FLAG = 0x00040000,  /* used for Dspv init */
   MON_HWD_SLEEP_DSPV_BUSY_FLAG = 0x00080000,  /* HWD: for DSPv busy */
   MON_FLAG_CAN_USE_3           = 0x00100000,  /* Can use bitmap: can change name when used  */
   MON_HWD_GENERIC_GPINT_FLAG   = 0x00200000,  /* HWD: for generic GPINT activity */
   MON_HWD_HEADSETKEY_FLAG      = 0x00400000,  /* MON: For headset key activity */
   MON_RMC_GM_GAP_FLAG          = 0x00800000,  /* RMCGM: For Idle/Conn Gap offered  */
   MON_HWD_SLEEP_UIM_FLAG       = 0x01000000,  /* HWD: for UIM activity */
   MON_HWD_SLEEP_VIBRATE_FLAG   = 0x02000000,  /* HWD: for vibrate, not used in fact */
   MON_HWD_SLEEP_MMAPPS_FLAG    = 0x04000000,  /* HWD: for multi-media Apps */
   MON_HWD_SLEEP_AUXADC_FLAG    = 0x08000000,  /* HWD: for aux adc conversions */

   MON_LEC_GPS_ACTIVITY_FLAG    = 0x10000000,  /* LEC: for GPS activity */
   MON_EEP_GPS_ACTIVITY_FLAG    = 0x20000000,  /* EEP: for GPS activity */

   MON_IOP_RX_VETO_FLAG         = 0x40000000,  /* MON: For 4 line wakeup */

   /* don't use > 0x7FFF FFFF */
} MonDeepSleepVetoT;

#endif
/*------------------------------------------------------------------------
* Define typedefs used in MON API
*------------------------------------------------------------------------*/

/* Define fault halt/continue flag */
typedef enum
{
   MON_CONTINUE  = 0x00,
   MON_HALT      = 0x01
} MonFaultTypeT;

/* Define all unit fault code numbers */
typedef enum
{
   MON_CP_FAULT_UNIT   = 0x00,
   MON_DBM_FAULT_UNIT  = 0x01,
   MON_EXE_FAULT_UNIT  = 0x02,
   MON_IPC_FAULT_UNIT  = 0x03,
   MON_IOP_FAULT_UNIT  = 0x04,
   MON_L1D_FAULT_UNIT  = 0x05,
   MON_LMD_FAULT_UNIT  = 0x06,
   MON_MON_FAULT_UNIT  = 0x07,
   MON_PSW_FAULT_UNIT  = 0x08,
   MON_HWD_FAULT_UNIT  = 0x09,
   MON_TST_FAULT_UNIT  = 0x0A,
   MON_VAL_FAULT_UNIT  = 0x0B,
   MON_SYS_FAULT_UNIT  = 0x0C,
   MON_RLP_FAULT_UNIT  = 0x0D,
   MON_HLP_FAULT_UNIT  = 0x0E,
   MON_MEDIA_FAULT_UNIT= 0x0F,
   MON_UIM_FAULT_UNIT  = 0x10,
   MON_UI_FAULT_UNIT   = 0x11,
   MON_FSM_FAULT_UNIT  = 0x12,
   MON_CLC_FAULT_UNIT  = 0x13,
   MON_FCP_FAULT_UNIT  = 0x14,
   MON_HSC_FAULT_UNIT  = 0x15,
   MON_RMC_FAULT_UNIT  = 0x16,
   MON_RCP_FAULT_UNIT  = 0x17,
   MON_SLC_FAULT_UNIT  = 0x18,
   MON_CSS_FAULT_UNIT  = 0x19,
   MON_LEC_FAULT_UNIT  = 0x1A,
   MON_SEC_FAULT_UNIT  = 0x1B,
   MON_A2DP_FAULT_UNIT = 0x1C,
   MON_AV_FAULT_UNIT   = 0x1D,
   MON_ETS_FAULT_UNIT  = 0x1E,
   MON_TLS_FAULT_UNIT  = 0x1F,
   MON_DM_FAULT_UNIT   = 0x20,
   MON_UNUSED_FAULT_UNIT = 0x21,
   MON_MMC_FAULT_UNIT = 0x22,

#if (defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC)))
   MON_FHC_FAULT_UNIT  = 0x23,
   MON_LOGIQ_FAULT_UNIT= 0x24,
#endif 

#if (defined(MTK_PLT_AUDIO))
	MON_VALSPH_FAULT_UNIT  = 0x25,
	MON_HWDSPH_FAULT_UNIT  = 0x26,
#endif //  (defined(MTK_PLT_AUDIO))
   MON_SBP_FAULT_UNIT  = 0x27,
   MON_NUM_FAULT_UNIT,

   MON_CP_BOOT_FAULT_UNIT = 0x30, /* these 2 are only used by boot code and therefore  */
   MON_BOOT_FAULT_UNIT    = 0x31  /* should not be accounted for in MON_NUM_FAULT_UNIT */

} MonFaultUnitT;

/* Define Big Buffer modes */
typedef enum
{
   MON_BB_BB2MDM_CONTINUOUS         = 1,
   MON_BB_BB2MDM_SINGLE             = 2,
   MON_BB_MDM2BB_RX_WRITE           = 3,
   MON_BB_MDM2BB_TX_WRITE           = 4,
   MON_BB_SERIAL_DOWNLOAD_INIT      = 5,
   MON_BB_SERIAL_UPLOAD_INIT        = 6,
   MON_BB_ECP_DOWNLOAD_INIT         = 7,
   MON_BB_ECP_UPLOAD_INIT           = 8,
   MON_BB_DISABLE                   = 9,
   MON_BB_BB2DOMDM_CONTINUOUS       = 10,
   MON_BB_BB2DOMDM_SINGLE           = 11,
   MON_BB_DOMDM2BB_RX_WRITE         = 12,
   MON_BB_DOMDM2BB_TX_WRITE         = 13,
   MON_BB_BB2GPS_CONTINUOUS         = 14,
   MON_BB_BB2GPS_SINGLE             = 15,
   MON_BB_GPS2BB_RECORD             = 16
} MonBbModeTypeT;


/* Define MON Task msg Ids */
typedef enum
{
   MON_PEEK_MSG             = 0x00,
   MON_POKE_MSG             = 0x01,
   MON_SPY_MSG              = 0x02,
   MON_TRACE_MSG            = 0x03,
   MON_MEM_TEST_MSG         = 0x04,
   MON_VERSION_MSG          = 0x05,
   MON_CBP_VERSION_MSG      = 0x06,
   MON_DSPM_DOWN_COMP_MSG   = 0x07,
   MON_DSP_HEART_BEAT_MSG   = 0x08,
   MON_BB_CONFIG_MSG        = 0x09,
   MON_BB_SYNC_START_MSG    = 0x0A,
   MON_BB_TIM_CHANGE_MSG    = 0x0B,
   MON_BB_WRITE_MSG         = 0x0C,
   MON_BB_READ_MSG          = 0x0D,
   MON_BB_TX_CONFIG_MSG     = 0x0E,
   MON_FLASH_ID_MSG         = 0x0F,
   MON_RESET_PROCESSOR_MSG  = 0x10,
   MON_VOCODER_TEST_MSG     = 0x11,
   MON_SPEECH_WRITE_MSG     = 0x12,
   MON_SPEECH_READ_MSG      = 0x13,
   MON_PIN_MUXING_MSG       = 0x14,
   MON_DSP_INFO_MSG         = 0x15,
   MON_SLEEP_CONTROL_MSG    = 0x16,
   MON_FAULT_CTRL_MSG       = 0x17,
   MON_BLK_WRITE_DB_MSG     = 0x18,
   MON_BLK_READ_DB_MSG      = 0x19,
   MON_DBUF_LOOPBACK_MSG    = 0x1A,
   MON_DSP_DOWNLOAD_CNF_MSG = 0x1B,
   MON_REV_CHANNEL_DATA_MSG = 0x1C,
   MON_SET_CLEAR_GPIO_MSG   = 0x1E,
   MON_READ_GPIO_MSG        = 0x1F,
   MON_JUMP_TO_BOOT_MSG     = 0x20,
   MON_SLEEP_ENTER_MSG      = 0x21,
   MON_MONITOR_GPIO_MSG     = 0x22,
   MON_DSPM_PEEK_MSG        = 0x23,
   MON_DSPV_PEEK_MSG        = 0x24,
   MON_DSPM_PEEK_RSP_MSG    = 0x25,
   MON_DSPV_PEEK_RSP_MSG    = 0x26,
   MON_DSPM_FAULT_MSG       = 0x27,
   MON_DSPV_FAULT_MSG       = 0x28,
   MON_CP_CONFIG_INFO_MSG   = 0x2B,
   MON_BB_CHECKSUM_MSG      = 0x2C,
   MON_TIMESTAMP_CTRL_MSG   = 0x2D,
   MON_TIMESTAMP_SETUP_MSG  = 0x2E,
   MON_BITWISE_16_OPERATION_MSG= 0x2F,
   MON_TRACE_CACHE_MSG      = 0x30,
   MON_SPY_CACHE_MSG        = 0x31,
   MON_TRACE_DUMP_MSG       = 0x32,
   MON_BOOT_VERSION_MSG     = 0x33,
   MON_EXE_MSG_BUFF_BUCKETS_RESET_MSG           = 0x34,
   MON_DSPV_DOWN_COMP_MSG                       = 0x35,
   MON_DSPM_CODE_PEEK_MSG                       = 0x36,
   MON_DSPV_CODE_PEEK_MSG                       = 0x37,
   MON_DSPM_CODE_PEEK_RSP_MSG                   = 0x38,
   MON_DSPV_CODE_PEEK_RSP_MSG                   = 0x39,
   MON_DSP_JTAG_POWER_MSG                       = 0x3A,
   MON_DSP_SIM_INIT_MSG                         = 0x3B,
   MON_DSP_SIM_CMD_MSG                          = 0x3C,
   MON_DSP_SIM_EXECUTE_MSG                      = 0x3D,
   MON_TEST_DSPM_BOOT_CODE_MSG                  = 0x3E,
   MON_TEST_DSPV_BOOT_CODE_MSG                  = 0x3F,
   MON_ENTER_CALIBRATION_MODE_MSG               = 0x40,
   MON_FLASH_SECTION_INFO_MSG                   = 0x41,
   MON_CHECK_TASK_STACK_MSG                     = 0x42,
   MON_TX_FAULT_DUMP_LOG_TO_ETS                 = 0x43,
   MON_CLR_FAULT_DUMP_LOG                       = 0x44,
   MON_CHECK_MEMORY_POOL_MSG                    = 0x45,
   MON_GET_MEMORY_POOL_INFO_MSG                 = 0x47,
   MON_ENABLE_BOOT_DUMP_RAM_MSG                 = 0x48,
   MON_CHECK_NEW_HALT_LOG_MSG                   = 0x49,
   
   MON_PROFILE_START_MSG                        = 0x4B,
   MON_PROFILE_STOP_MSG                         = 0x4C,
   MON_PROFILE_GET_DATA_SIZE_MSG                = 0x4D,
   MON_PROFILE_GET_DEBUG_INFO_MSG               = 0x4E,
   MON_PROFILE_SET_SPY_DISPLAY_FILTERS_MSG      = 0x4F,
   MON_MEMORY_COPY_MSG                          = 0x50,
   MON_PRINTF_CTRL_MSG                          = 0x51,
   MON_CPBUF_STATUS_MSG                         = 0x52,

   MON_SDIO_TEST_MSG                            = 0x53,
   MON_SDIO_S2M_DATA_RCVD_MSG                   = 0x54,
   MON_SDIO_M2S_DATA_RCVD_MSG                   = 0x55,

   MON_PROFILE_SET_MODE_MSG                     = 0x57,
   MON_PROFILE_GET_MODE_MSG                     = 0x58,

   MON_MPU_POPULATED_BLOCK_CTRL_MSG             = 0x59,
   MON_MPU_UNPOPULATED_SEGMENT_CTRL_MSG         = 0x5A,
   MON_MPU_ITCM_WRITE_CTRL_MSG                  = 0x5B,
   MON_GET_USB_IDS_MSG                          = 0x5C,
   MON_DSPV_JTAG_ENABLE_MSG                     = 0x5D,

   MON_TIMER_CONSUME_SPY_MSG                    = 0x5E,
   MON_CPU_ALOT_SPY_MSG                         = 0x5F,
   MON_EXE_SILENT_LOG_CTRL_MSG                  = 0x60,
   MON_EVENT_HISTORY_TRACE_MSG                  = 0x61,
   MON_CP_BUF_BUSY_TRACE_MSG                    = 0x62,
   MON_TASK_EVENT_LOG_SET_MSG                   = 0x63,
   MON_TASK_EVENT_LOG_STATUS_MSG                = 0x64,

   MON_CP_LOOPBACK                              = 0x65,
   MON_CBP_SW_VERSION_MSG                       = 0x66,
   MON_DSPM_VER_RSP_MSG                         = 0x67,
   MON_DSPV_VER_RSP_MSG                         = 0x68,
   MON_FOTA_UPDATE_MSG                          = 0x69,
   MON_SLEEP_VETO_CTRL_MSG                      = 0x6A,
   MON_GET_UNIT_BUILD_INFO_MSG                  = 0x6B,

   MON_DSPV_SPY_MSG                             = 0x6C,
   MON_DSPV_TRACE_MSG                           = 0x6D,
   MON_DSPM_SPY_MSG                             = 0x6E,
   MON_DSPM_TRACE_MSG                           = 0x6F,

   MON_SLEEP_CANCEL_MSG                         = 0x70,
   MON_SIM_CP_MSG_MSG                           = 0x71,
   MON_BITWISE_32_OPERATION_MSG                 = 0x72,
   MON_MMU_CTL_MSG                              = 0x73,
   MON_MMU_GET_TLB_ENTRY_MSG                    = 0x74,
   MON_MMU_GET_TLB_CONFIG_MSG                   = 0x75,
   MON_MEMORY_SEGMENTS_MSG                      = 0x76,
   MON_CP_OR_BOOT_MODE_MSG                      = 0x77,   
   MON_EXE_SILENT_LOG_SETTING_MSG               = 0x78,
   MON_ADC_MEAS_REQUEST_MSG                     = 0x82,

   MON_HWD_PROFILE_CP_START_MSG                 = 0x83,
   MON_HWD_PROFILE_CP_STOP_MSG                  = 0x84,
   MON_HWD_PROFILE_CP_GET_SPY_DATA_MSG          = 0x85,
   MON_HWD_PROFILE_DSPM_START_MSG               = 0x86,
   MON_HWD_PROFILE_DSPM_STOP_MSG                = 0x87,
   MON_HWD_PROFILE_DSPV_START_MSG               = 0x88,
   MON_HWD_PROFILE_DSPV_STOP_MSG                = 0x89,
   MON_HWD_PROFILE_DSP_DATA_MSG                 = 0x8A,

   MON_GET_SYS_FEATURES_MSG                     = 0x8B,
   MON_CP_CONFIG_UIMENABLED_MSG                 = 0x8C,
   MON_MEMORY_SPEED_TST_MSG                     = 0x8D,
   
#ifdef MTK_DEV_RF_CUSTOMIZE
   MON_SEND_CP_CFG_INFO_MSG                     = 0x8F,
#endif /* MTK_DEV_RF_CUSTOMIZE */
#if defined(SYS_OPTION_CCIRQ_MSG_TEST)
   MON_CCIRQ_MSG_TEST_MSG					    = 0x90,
#endif
#if ((defined(MTK_CBP)&& (!defined(MTK_PLT_ON_PC))) && defined(MTK_DEV_LOGIQ))
   MON_CP_LOGIQ_TX_MSG				            = 0x91,   
#endif
#ifdef MTK_EMI_PROFILING
   MON_EMI_PROFILING_MSG,
#endif
#if (defined(MTK_CBP)&& (!defined(MTK_PLT_ON_PC))) && defined(LOG_STATISTICS_PROFILE)
   MON_LOG_STATISTICS_PROFILE_MSG               = 0x92, 
#endif
#ifdef MTK_DEV_LOG_SPLM
   MON_LOG_SPLM_FILTER_FILE_CREATE_MSG          = 0x93,
#endif
#ifdef MTK_C2K_ELM
	  MON_C2K_ELM_MSG                           = 0x94,
#endif
#ifdef MTK_CBP       
   MON_SYS_32KLESS_TEST_MSG                     = 0x95,
   MON_SLEEP_STATIST_CTRL_MSG                   = 0x96,
#endif

   MON_LAST_DUMMY_MSG                           = 0xFF
} MonMsgIdT;

#ifdef MTK_DEV_LOG_SPLM
typedef enum
{
    MON_LOG_SPLM_CMD_SUCCESS = 0,
    MON_LOG_SPLM_FILE_OPEN_ERR,
    MON_LOG_SPLM_FILE_WRITE_ERR
} MonLogSplmStatusT;

typedef PACKED_PREFIX struct
{
    MonLogSplmStatusT  Status;
} PACKED_POSTFIX MonLogSplmCfgCreateRspMsgT;
#endif

typedef PACKED_PREFIX struct
{
   uint8 *SrcAddress;
   uint8 *DstAddress;
   uint16 NumBytes;
} PACKED_POSTFIX  MonMemoryCopyMsgT;

/* MON_SLEEP_CONTROL_MSG structure */
typedef PACKED_PREFIX struct
{
   bool  LightSleepEnableFlag;   /* Light sleep enable flag   */
   bool  DeepSleepEnableFlag; /* deep sleep enable flag    */
} PACKED_POSTFIX  MonSleepControlMsgT;

/* MON_SLEEP_CANCEL_MSG structure */
typedef PACKED_PREFIX struct
{
   uint8    Interface;
   uint8    WakeCs;
} PACKED_POSTFIX  MonSleepCancelMsgT;

/* MON_SLEEP_ENTER_MSG */
typedef PACKED_PREFIX struct {
   uint8    Interface;
   uint32   Duration; /* time to spend in deep sleep, 32KHz-tick units */
   bool     ProgramResync;
} PACKED_POSTFIX  MonSleepEnterMsgT;

/* MON_SLEEP_VETO_CTRL_MSG */
typedef PACKED_PREFIX struct {
   ExeRspMsgT     RspInfo;
   uint8          Interface;
   uint8          Control;  /* Suspend=1, Resume=0 */
   uint32         BitMask;
} PACKED_POSTFIX  MonSleepVetoCtrlMsgT;

typedef PACKED_PREFIX struct {
   uint32         BitMask;
} PACKED_POSTFIX  MonSleepVetoCtrlRspMsgT;

/* MON_GET_UNIT_BUILD_INFO_MSG */
typedef enum 
{
   UNIT_AV       = 0,
   UNIT_CLC,
   UNIT_CSS,
   UNIT_DBM,
   UNIT_EXE,
   UNIT_FCP,
   UNIT_FSM,
   UNIT_HLP,
   UNIT_HSC,
   UNIT_HWD,
   UNIT_IOP,
   UNIT_IPC,
   UNIT_L1D,
   UNIT_LEC,
   UNIT_LMD,
   UNIT_MON,
   UNIT_PS_LOCSRV,
   UNIT_PS_OTA,
   UNIT_PS_PE,
   UNIT_PS_PSW,
   UNIT_PS_SCC,
   UNIT_PS_SMS,
   UNIT_RCP,
   UNIT_RLP_RLPE,
   UNIT_RLP_RLPW,
   UNIT_RMC,
   UNIT_SEC,
   UNIT_SLC,
   UNIT_SYS,
   UNIT_UI,
   UNIT_UIM,
   UNIT_VAL,
   UNIT_VAL_ATC,
   UNIT_ALL_UNITS    = 0xffff
}MonUnitListEnumT;

typedef PACKED_PREFIX struct {
   ExeRspMsgT     RspInfo;
   uint16         Unit;
} PACKED_POSTFIX  MonGetUnitBuildInfoMsgT;

typedef PACKED_PREFIX struct {
   uint16      Unit;
   uint8       Month;
   uint8       Day;
   uint8       Year;
   uint8       Hour;
   uint8       Min;
   uint8       Revision[11];
   uint8**     ChangeList;
} PACKED_POSTFIX  MonGetUnitBuildInfoRspT;


/* Define Fault msg structure */
typedef PACKED_PREFIX struct
{
   MonFaultUnitT  UnitId;
   MonSysTimeT    SysTime;
   uint32         FaultCode1;
   uint32         FaultCode2;
   MonFaultTypeT  FaultType;
} PACKED_POSTFIX  MonFaultMsgT;

/* Define Fault control msg structure */
typedef PACKED_PREFIX struct
{
   bool  Control;
} PACKED_POSTFIX  MonFaultCtrlMsgT;

/* Define Fault control msg types */
typedef enum
{
   MON_FAULT_DISABLED = 0x00,
   MON_FAULT_ENABLED
} MonFaultCtrlTypeT;

/* Define MonPrintf control msg structure */
typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
  bool Enable;
  bool SaveSettingToFile;
} PACKED_POSTFIX  MonPrintfCtrlMsgT;

typedef PACKED_PREFIX struct
{
  bool Enable;
  bool SaveSettingToFile;
} PACKED_POSTFIX  MonPrintfCtrlRspMsgT;

/* Define printf msg structure */
typedef PACKED_PREFIX struct
{
   MonSysTimeT    SysTime;
   uint8          String[MON_MAX_PRINTF_STR_SIZE];
   uint32         Args[1];
} PACKED_POSTFIX  MonPrintfMsgT;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   bool       EventEn;
   bool       ContextEn;
} PACKED_POSTFIX MonThreadEventTraceCtrlMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   bool       TraceEn;
} PACKED_POSTFIX MonEnableCpbufBusyTraceMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX MonReadHaltDumpLogMsgT;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
   bool CleanCrashLog;
   bool CleanCrashCount;
} PACKED_POSTFIX  MonCleanFaultLogMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT RspInfo;
} PACKED_POSTFIX  MonCheckFaultLogMsgT;

typedef PACKED_PREFIX struct
{
   bool IsNewLog;
   uint16 TotalCrashCount;
} PACKED_POSTFIX  MonCheckFaultLogRspMsgT;

/* Define printf msg structure */
typedef PACKED_PREFIX struct
{
   MonSysTimeT    SysTime;
   uint8          String[MON_MAX_FILEINFO_STR_SIZE];
} PACKED_POSTFIX  MonFileInfoMsgT;

/* Define Big Buffer write msg types */
typedef enum
{
   MON_BB_WR_INIT_TYPE     = 0x00,
   MON_BB_WR_DATA_TYPE
} MonBbWriteTypeT;

/* Define Mon ack types */
typedef enum
{
   MON_ACK_TYPE     = 0x00,
   MON_NACK_TYPE
} MonAckTypeT;

/* Define Flash Program msg types */
typedef enum
{
   MON_FLASH_PROG_INIT_TYPE   = 0x00,
   MON_FLASH_PROG_DATA_TYPE
} MonFlashProgTypeT;

/* Define flash sections */
typedef enum
{
   MON_CP_BOOT_FLASH_SECTION       = 0x00,
   MON_CP_CODE_FLASH_SECTION,
   MON_DSPM_CODE_FLASH_SECTION, 
   MON_DSPV_CODE_FLASH_SECTION, 
   MON_FSM_DATA_FLASH_SECTION,
   MON_CDROM_IMAGE_SECTION,
   MON_ALL_FLASH_NOBOOT_SECTION,
   MON_ALL_FLASH_SECTION,
   MON_UA_BOOT_FLASH_SECTION,
   MON_DELTA_FILE_FLASH_SECTION,
   MON_FSM_USER_FLASH_SECTION,
   MON_DDR_TUNING_FLASH_SECTION, 
   MON_FAULT_LOG_FLASH_SECTION,
   MON_PRI_FLASH_SECTION,
   MON_RES_WALL_PAPER_SECTION,
   MON_RES_MAIN_TOPIC_SECTION,
   MON_LOGO_FLASH_SECTION,
   MON_FLASH_SECTION_MAX
} MonFlashSectionT;

/* Define Speech write msg types */
typedef enum
{
   MON_SPEECH_WR_INIT_TYPE     = 0x00,
   MON_SPEECH_WR_DATA_TYPE
} MonSpeechWriteTypeT;

/* Define download complete mgs */
typedef PACKED_PREFIX struct
{
   uint16      Checksum;
} PACKED_POSTFIX  MonDownCompMsgT;

typedef PACKED_PREFIX struct 
{
   uint16 MemPoolCount;
   ExeMemoryPoolInfoT MemPoolList[1];
} PACKED_POSTFIX  MonGetMemoryPoolInfoRspMsgT;

typedef PACKED_PREFIX struct 
{
   uint32 Code2DataSpeedKB;  /* Copy data from code area to ZI data area speed KB/s */
   uint32 Data2DataSpeedKB;  /* Copy data from ZI data area to ZI data area speed KB/s */
   uint32 MemcpyData2DataSpeedKB;  /* memcpy() data from ZI data area to ZI data area speed KB/s */
   uint32 IRAM2DataSpeedKB;  /* Copy data from IRAM area to ZI data area speed KB/s */
   uint32 Cpbuf2DataSpeedKB;  /* Copy data from cp buf area to ZI data area speed KB/s */
   uint32 FlashMipsSpeed;     /* The CPU run flash code speed, no data read and write, unit is MIPS */
   uint32 IramMipsSpeed;      /* The CPU run IRAM code speed, no data read and write, unit is MIPS */
} PACKED_POSTFIX  MonMemorySpeedTstRspMsgT;

/* Define peek/poke address types */
typedef enum
{
   MON_ADDRTYPE_MEMORY   = 0,
   MON_ADDRTYPE_REGISTER,
   MON_ADDRTYPE_32BITDATA  /* to access 32bit-only registers */
} MonAddrTypeT;

/* Define MON peek msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   uint32         StartAddr;
   uint8          NumUint16;
   MonAddrTypeT   AddrType;
} PACKED_POSTFIX  MonPeekMsgT;

/* Define MON peek response msg */
typedef PACKED_PREFIX struct
{
   uint32      StartAddr;
   uint8       NumUint16;
   uint16      Data[1];
} PACKED_POSTFIX  MonPeekRspMsgT;

/* Define MON poke msg command */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   uint32         StartAddr;
   uint8          NumUint16;
   MonAddrTypeT   AddrType;
   uint16         Data[1];
} PACKED_POSTFIX  MonPokeMsgT;

/* Define MON poke response msg */
typedef PACKED_PREFIX struct
{
   uint32      StartAddr;
   uint8       NumUint16;
} PACKED_POSTFIX  MonPokeRspMsgT;

/* Define MON_BITWISE_OPERATION_MSG command */
typedef enum
{
   MON_BITOP_TOGGLE = 0,
   MON_BITOP_SET,
   MON_BITOP_CLEAR
} MonBitwiseOperatorT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT           RspInfo;
   uint16               *Address;
   MonBitwiseOperatorT  Operator;
   uint16               Mask;
} PACKED_POSTFIX  MonBitOp16MsgT;

/* Define MON_BITWISE_OPERATION_MSG response msg */
typedef PACKED_PREFIX struct
{
   uint16         OldValue;
   uint16         NewValue;
} PACKED_POSTFIX  MonBitOp16RspMsgT;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT           RspInfo;
   uint32               *Address;
   MonBitwiseOperatorT  Operator;
   uint32               Mask;
} PACKED_POSTFIX  MonBitOp32MsgT;

/* Define MON_BITWISE_OPERATION_MSG response msg */
typedef PACKED_PREFIX struct
{
   uint32         OldValue;
   uint32         NewValue;
} PACKED_POSTFIX  MonBitOp32RspMsgT;

/* Define MON spy msg command */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint16      SpyId;
   uint8       SpyActive;
} PACKED_POSTFIX  MonSpyMsgT;


/* Fault message */
typedef PACKED_PREFIX struct
{
   uint16       UnitId;
   MonSysTimeT  SysTime;
   uint16       FaultId1;
   uint16       FaultId2;
   uint16       FaultType;
} PACKED_POSTFIX  MonIpcCpFaultMsgT;


/* Spy response message */
typedef PACKED_PREFIX struct
{
   uint16       SpyId;
   MonSysTimeT  SysTime;
   uint16       Data[1];
} PACKED_POSTFIX  MonIpcCpSpyRspMsgT;

/* Trace response message */
typedef PACKED_PREFIX struct
{
   uint16       TraceId;
   MonSysTimeT  SysTime;
   uint16       Args[1];
} PACKED_POSTFIX  MonIpcCpTraceRspMsgT;

/* Define MON spy msg response */
typedef PACKED_PREFIX struct
{
   uint16      SpyId;
   MonSysTimeT    SysTime;
   uint8       Data[1];
} PACKED_POSTFIX  MonSpyRspMsgT;

#ifndef SYS_TIME_MULTIPLE_TYPES   
#define  MON_SPY_HEADER_SIZE             6                   /* sizeof MonSpyRspMsgT minus Data[] */
#else
#define  MON_SPY_HEADER_SIZE  (2 + 4 + 8*MON_SYS_TIME_TYPE_MAX)  /* sizeof MonSpyRspMsgT minus Data[] */
#endif

/* Define MON trace msg command */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint16      TraceId;
   uint8       TraceData[1];
} PACKED_POSTFIX  MonTraceMsgT;




/* Define MON trace response msg */
typedef PACKED_PREFIX struct
{
   uint16      TraceId;
   MonSysTimeT    SysTime;
   uint32      Args[1];
   
} PACKED_POSTFIX  MonTraceRspMsgT;

/* Define MON memory test msg command */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint32      StartAddr;
   uint32      NumBytes;
   uint8     BytePattern;
   uint8     TestNum;
} PACKED_POSTFIX  MonMemTestMsgT;

/* Define MON memory test response msg */
typedef PACKED_PREFIX struct
{
   bool        Result;
   uint32      ErrAddr;
   uint32       Write;
   uint32       Read;
   uint8       PatternIdx;
} PACKED_POSTFIX  MonMemTestRspMsgT;


/* Define MON version type */
typedef PACKED_PREFIX struct
{
   uint8       UnitNum;
   uint8       VerInfo[3];
   uint8       TimeInfo[5];
} PACKED_POSTFIX  MonVersionT;

/* Define MON version msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  MonVersionMsgT;

/* Define MON version response msg */
typedef PACKED_PREFIX struct
{
   MonVersionT  Info[2];
} PACKED_POSTFIX  MonVersionRspMsgT;

/* Define MON CBP version msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  MonCBPVersionMsgT;

/* DSP patch RAM structure */
typedef PACKED_PREFIX struct
{
   uint16   PatchAddr;
   uint16   MatchAddr;
} PACKED_POSTFIX  PatchInfoT;

typedef PACKED_PREFIX struct
{
   uint16      NumPatches;
   uint8       BuildDate[13];
   uint8       BuildTime[8];
   uint8       PatchRevision[11];
   uint8       IdNumber;
   uint8       IdString[16];
   uint16      CodeSize;
} PACKED_POSTFIX  BuildInfoT;

/* Define MON CBP version response msgs
   (the RAM version does not have any patch information) */
typedef PACKED_PREFIX struct
{
   uint8       AsicType[14];
   uint16      ChipIdHi;
   uint16      ChipIdLo;
   BuildInfoT  BuildInfo[2]; /* DSPM and DSPV */
} PACKED_POSTFIX  MonCBPVersionRspMsgT;

/* Define MON CBP SW Version msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  MonCBPSwVersionMsgT;

/* Define MON version response msg */
typedef PACKED_PREFIX struct
{
   uint8     CbpSwVerStrLen;
   uint8     CbpSwVersion[MON_MAX_PRINTF_STR_SIZE];
} PACKED_POSTFIX  MonCBPSwVersionRspMsgT;

/* Define MON DSP heart beat msg */
typedef PACKED_PREFIX struct
{
   uint16   ProcessorId;
} PACKED_POSTFIX  MonDspHeartBeatMsgT;

/* Define MON Big Buffer Configuration msg */
typedef PACKED_PREFIX struct
{
   MonBbModeTypeT   Mode;
   uint32           StartAddr;
   uint32           StopAddr;
} PACKED_POSTFIX  MonBbConfigMsgT;

/* Define MON Big Buffer Synchronous start msg */
typedef PACKED_PREFIX struct
{
   uint8    Mode;
   uint32   StartAddr;
   uint32   StopAddr;
   uint16   LongCodeState[3];
   uint16   LongCodeRxMask[3];
   uint16   LongCodeTxMask[3];
   uint16   RxChanSel;
   uint16   LcTxRxDly;
} PACKED_POSTFIX  MonBbSyncStartMsgT;

/* Define MON Big Buffer timing change msg */
typedef PACKED_PREFIX struct
{
   uint16 FrameOffset;
} PACKED_POSTFIX  MonBbTimChangeMsgT;

/* Define MON Big Buffer Write msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint32            SeqNum;
   uint16            NumWords;
   uint16            Data[1];
} PACKED_POSTFIX  MonBbWriteMsgT;

/* Define MON Big Buffer Write response msg */
typedef PACKED_PREFIX struct
{
   uint32            SeqNum;
   MonAckTypeT       AckType;
} PACKED_POSTFIX  MonBbWriteRspMsgT;

/* Define MON Big Buffer Read msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint32            SeqNum;
   uint16            NumWords;
} PACKED_POSTFIX  MonBbReadMsgT;

/* Define MON Big Buffer Read response msg */
typedef PACKED_PREFIX struct
{
   uint32            SeqNum;
   uint16            NumWords;
   uint16            Data[1];
} PACKED_POSTFIX  MonBbReadRspMsgT;

/* Define MON Big Buffer Checksum msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint16            Checksum;
} PACKED_POSTFIX  MonBbChecksumMsgT;

/* Define MON Big Buffer Checksum response msg */
typedef PACKED_PREFIX struct
{
   uint16            Checksum;
} PACKED_POSTFIX  MonBbChecksumRspMsgT;

/* Define MON tx hw test msg */
typedef PACKED_PREFIX struct
{
   uint16 FrameOffset;
   uint16 TxOffset;
   uint16 TxPh;
   uint16 TxRxOffset;
} PACKED_POSTFIX  MonBbTxConfigMsgT;

/* Define Flash ID msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  MonFlashIdMsgT;

/* Define Flash ID response msg */
typedef PACKED_PREFIX struct
{
   uint16   ManfId;
   uint16   DevId[3];
   uint32   MemSize;
} PACKED_POSTFIX  MonFlashIdRspMsgT;

/* Define Flash Get Sector Info msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  MonFlashSectionInfoMsgT;

/* Define Flash Get Section Info response msg.
 * Flash Section info returned in the order defined by MonFlashSectionT enum:
 *        CP Boot, CP, DSPM, DSPV, FSM DATA, ALL
 */
typedef PACKED_PREFIX struct
{
    PACKED_PREFIX struct
    {
       uint8    SectionId;
       uint32   SectionStartAddr;
       uint32   SectionSize;
    } PACKED_POSTFIX  SectionList [MON_FLASH_SECTION_MAX];
} PACKED_POSTFIX  MonFlashSectionInfoRspMsgT;

/*
   This data structure is needed even though there is
   no message ID defined. It is used by the boot loader
   code
*/
/* Define Flash erase msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   MonFlashSectionT  FlashSection;
} PACKED_POSTFIX  MonFlashEraseMsgT;

/*
   This data structure is needed even though there is
   no message ID defined. It is used by the boot loader
   code
*/
/* Define Flash erase response msg */
typedef PACKED_PREFIX struct
{
   MonFlashSectionT  FlashSection;
} PACKED_POSTFIX  MonFlashEraseRspMsgT;

/*
   This data structure is needed even though there is
   no message ID defined. It is used by the boot loader
   code
*/
/* Define Flash program msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT         RspInfo;
   uint32             SeqNum;
   MonFlashProgTypeT  MsgType;
   MonFlashSectionT   FlashSection;
   uint16             Checksum;
   uint16             NumBytes;
   uint8              Data[1];
} PACKED_POSTFIX  MonFlashProgMsgT;

/*
   This data structure is needed even though there is
   no message ID defined. It is used by the boot loader
   code
*/
/* Define Flash program response msg */
typedef PACKED_PREFIX struct
{
   uint32             SeqNum;
   MonAckTypeT        AckType;
} PACKED_POSTFIX  MonFlashProgRspMsgT;

/* Define Flash erase msg */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT        RspInfo;    
   uint32  StartAddr;
   uint32  Length;
} PACKED_POSTFIX  MonFlashAddrEraseMsgT;

/* 
   This data structure is needed even though there is
   no message ID defined. It is used by the boot loader
   code 
*/
/* Define Flash erase response msg */
typedef PACKED_PREFIX struct 
{
   uint32  StartAddr;
   uint32  Length;
} PACKED_POSTFIX  MonFlashAddrEraseRspMsgT;

/* 
   This data structure is needed even though there is
   no message ID defined. It is used by the boot loader
   code 
*/
/* Define Flash program msg */
typedef PACKED_PREFIX struct 
{
   ExeRspMsgT         RspInfo;    
   uint32             SeqNum;
   MonFlashProgTypeT  MsgType;
   uint32             FlashStartAddr;
   uint32             Length;
   uint16             Checksum;
   uint16             NumBytes;
   uint8              Data[1];
} PACKED_POSTFIX  MonFlashAddrProgMsgT;

/* 
   This data structure is needed even though there is
   no message ID defined. It is used by the boot loader
   code 
*/
/* Define Flash program response msg */
typedef PACKED_PREFIX struct 
{
   uint32             SeqNum;
   MonAckTypeT        AckType;
} PACKED_POSTFIX  MonFlashAddrProgRspMsgT;

/* Define processor reset msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT    RspInfo;
   IpcProcIdT    ProcessorId;
} PACKED_POSTFIX  MonResetProcessorMsgT;

/* Define processor reset response msg */
typedef PACKED_PREFIX struct
{
   IpcProcIdT    ProcessorId;
} PACKED_POSTFIX  MonResetProcessorRspMsgT;

/* Enumerated type for specifying Vocoder Test Mode */
 typedef enum
{
  MON_ENCODER_DECODER_INVALID    = 0x0,
  MON_ENCODER_ONLY               = 0x1,
  MON_DECODER_ONLY               = 0x2,
  MON_ENCODER_DECODER            = 0x4
} MonVTstModeT;

/* Structure for sending Vocoder Test Mode */
typedef PACKED_PREFIX struct
{
  ExeRspMsgT    RspInfo;
  uint32        DownloadStartAddr;
  uint32        UploadStartAddr;
  uint32        DownloadLength;
  uint32        UploadLength;
  MonVTstModeT  TestMode;
  bool          EnableHw;
  uint32        PacketSize;
  uint16        AudioSpchSrvcOptMaxRate;   /* maximum voice encode rate */
} PACKED_POSTFIX  MonVTstMsgT;

typedef PACKED_PREFIX struct
{
  bool          TestComplete;
  uint32        DownloadStartAddr;
  uint32        DownloadWordsUsed;
  uint32        UploadStartAddr;
  uint32        UploadWordsCreated;
  uint32        MaxCompPackets;
  uint32        NumCompPackets;
  uint32        MaxPcmWords;
  uint32        NumPcmWords;
} PACKED_POSTFIX  MonVTstRspT;

/* Define MON Speech data Write msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT           RspInfo;
   uint32               SeqNum;
   MonSpeechWriteTypeT  MsgType;
   uint32               WriteAddr;
   uint16               Checksum;
   uint8                NumBytes;
   uint8                Data[1];
} PACKED_POSTFIX  MonSpeechWriteMsgT;

/* Define MON Speech data Write response msg */
typedef PACKED_PREFIX struct
{
   uint32            SeqNum;
   MonAckTypeT       AckType;
} PACKED_POSTFIX  MonSpeechWriteRspMsgT;

/* Define MON Speech Data Read msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
   uint32            SeqNum;
   uint32            ReadAddr;
   uint8             NumBytes;
} PACKED_POSTFIX  MonSpeechReadMsgT;

/* Define MON Speech data Read response msg */
typedef PACKED_PREFIX struct
{
   uint32            SeqNum;
   uint16            Checksum;
   uint8             NumBytes;
   uint8             Data[1];
} PACKED_POSTFIX  MonSpeechReadRspMsgT;

/* Define MON DSPM/V Config message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT        RspInfo;
} PACKED_POSTFIX  MonDspConfigMsgT;

/* Define MON DSPM/V Config response message */
typedef PACKED_PREFIX struct
{
   uint16            ConfigDataL;
   uint16            ConfigDataH;
} PACKED_POSTFIX  MonDspConfigRspMsgT;

/* Define MON DSP information msg received from DSPM and DSPV */
typedef PACKED_PREFIX struct
{
   uint16            ProcessorId;
   uint16            ConfigDataL;
   uint16            ConfigDataH;
} PACKED_POSTFIX  MonDspInfoMsgT;

/* Define MON block data base write message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT         RspInfo;
   uint32             SeqNum;
   DbmProgTypeT       MsgType;
   DbmBlkDataBaseIdT  DataBaseId;
   uint32             Offset;
   uint16             Checksum;
   uint16             NumBytes;
   uint8              Data[1];
} PACKED_POSTFIX  MonBlkDbWriteMsgT;

/* Define MON block data base write response message */
typedef PACKED_PREFIX struct
{
   uint32             SeqNum;
   DbmBlkDataBaseIdT  DataBaseId;
   DbmAckTypeT        AckType;
} PACKED_POSTFIX  MonBlkDbWriteRspMsgT;

/* Define MON block data base read message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT         RspInfo;
   uint32             SeqNum;
   DbmBlkDataBaseIdT  DataBaseId;
   uint32             Offset;
   uint16             NumBytes;
} PACKED_POSTFIX  MonBlkDbReadMsgT;

/* Define MON block data base read response message */
typedef PACKED_PREFIX struct
{
   uint32             SeqNum;
   DbmBlkDataBaseIdT  DataBaseId;
   uint32             DataBaseSize;
   uint32             Offset;
   uint16             Checksum;
   uint16             NumBytes;
   uint8              Data[1];
} PACKED_POSTFIX  MonBlkDbReadRspMsgT;

/* Define MON direct buffer mailbox loopback message */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint32      NumLoops;
   uint16      NumWords;
   uint16      Data[1];
} PACKED_POSTFIX  MonDBufLoopbackMsgT;

/* Define MON direct buffer mailbox loopback response msg */
typedef PACKED_PREFIX struct
{
   bool        Result;
} PACKED_POSTFIX  MonDBufLoopbackRspMsgT;

/* Define MON DSP download confirmation message */
typedef PACKED_PREFIX struct
{
  bool         Result;   /* False return result indicates error in download */
} PACKED_POSTFIX  MonDspDownloadCnfMsgT;

/* Define MON REV channel data message */
typedef PACKED_PREFIX struct
{
  uint16  ChnlDataReady;   /* Bitfield indicating which channels contain data */
  uint16  FundRate;        /* Fundamental rate, if assigned */
  uint16  NumWords;
  uint16  Data[1];
} PACKED_POSTFIX  MonRevChnlDataMsgT;

/* Define set/clear GPIO actions used by MonSetClearGPIOMsgT */
typedef enum
{
   MON_CLEAR_GPIO = 0x00,
   MON_SET_GPIO
} MonGPIOActionT;

/* Define MON Set Clear GPIO msg */
typedef PACKED_PREFIX struct
{
   uint8          GPIONum;
   MonGPIOActionT Action;
} PACKED_POSTFIX  MonSetClearGPIOMsgT;

/* Define MON Read GPIO msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint8       GPIONum;
} PACKED_POSTFIX  MonReadGPIOMsgT;

/* Define MON Read GPIO Response msg */
typedef PACKED_PREFIX struct
{
   bool  Data;
} PACKED_POSTFIX  MonReadGPIORspMsgT;

/* Define MON Monitor GPIO msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo;
   uint8       GPIONum;
} PACKED_POSTFIX  MonMonitorGPIOMsgT;

/* Define MON Read GPIO Response msg */
typedef PACKED_PREFIX struct
{
    uint8  Direction; /* GPIO_OUTPUT=0, GPIO_INPUT=1, Invalid = 0xff */
    uint8  Data;
} PACKED_POSTFIX  MonMonitorGPIORspMsgT;

/* Define MON DSP peek msg */
typedef PACKED_PREFIX struct
{
   uint16       StartAddr;
   uint16       NumWords;
} PACKED_POSTFIX  MonDspPeekMsgT;

/* Define MON DSP peek response message */
typedef PACKED_PREFIX struct
{
   uint16       StartAddr;
   uint16       NumWords;
   uint16       RspTaskId; /* CP task id to which the peek response should be routed */
   uint16       RspMboxId; /* CP mailbox id to which the peek response should be routed */
   uint16       Data[1];
} PACKED_POSTFIX  MonDspPeekRspMsgT;

/* Define MON Boot version msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  MonBootVersionMsgT;

/* Define MON Boot version response msg */
typedef PACKED_PREFIX struct
{
   MonVersionT  Info[2];
} PACKED_POSTFIX  MonBootVersionRspMsgT;

/*=========================================*/

/* Data Sturcture for CP Config Info Msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  MonCpConfigInfoMsgT;

typedef enum
{
   ESN_Only = 1,
   MEIDorESN= 2

} CPMobileIDType;

/* Data Sturcture for CP Config Info Rsp Msg */
typedef PACKED_PREFIX struct
{
   uint8    AsicType;
   uint8    CPVersion;
   uint8    DSMPatchId;
   uint8    DSVPatchId;
   uint8    RFOption;
   uint8    RefClockFreq;
   bool     UIMEnabled;
   bool     FileSysEnabled;
   bool     DataSvcEnabled;
   bool     SchEnabled;
   bool     DiversityEnabled;
   bool     GPSEnabled;
   bool     AuxAdcPollingEnabled;
   bool     DigitalRxAgcEnabled;
   uint8    ManufactureId[20];
   uint8    ModelId[20];
   uint8    VerInfo[3];
   CPMobileIDType    MobileIdType;
   bool     EPRL_Supported;
#if defined(SYS_FLASH_LESS_SUPPORT) && defined(MTK_DEV_VERSION_CONTROL)
   RfFileCalStateT RfFileCalState;
#endif
} PACKED_POSTFIX  MonCpConfigInfoRspMsgT;

/* Struct for MON_CP_CONFIG_UIMENABLED_MSG */
typedef PACKED_PREFIX struct {
   bool     UIMEnabled;   /* UICC factory TRUE == UIM enabled FALSE */
} PACKED_POSTFIX  MonCpConfigUimenabledMsgT;

/* Struct for IOP_CP_CONFIGURATION_UPDATE_ETS */
typedef PACKED_PREFIX struct {
   bool     UIMEnabled;
} PACKED_POSTFIX  MonCpConfigUpdateMsgT;

   /* MON_TIMESTAMP_CTRL_MSG */
typedef PACKED_PREFIX struct {
   uint8 Cmd;     /* cmd id, see MonTimeStampCtrlCmdT */
} PACKED_POSTFIX  MonTimestampCtrlMsgT;

   /* MON_TIMESTAMP_SETUP_MSG */
typedef PACKED_PREFIX struct {
   uint8 Cmd;     /* cmd id, see MonTimeStampSetupCmdT */
   uint8 Mode;    /* 0: disable, 1: enable */
   uint8 NumIds;     /* entries in Id[] */
   uint8 Id[1];      /* list of IDs */
} PACKED_POSTFIX  MonTimestampSetupMsgT;

   /* MON_TRACE_CACHE_MSG */
   /* MON_SPY_CACHE_MSG */
typedef PACKED_PREFIX struct {
   uint8 Mode;    /* 0: uncache, !=0: cache */
   uint8 NumIds;  /* entries in array */
   uint16   Id[1];   /* trace/spy ids */
} PACKED_POSTFIX  MonInfoCacheMsgT;

/*====== MON Pin Muxing Message Definitions ======*/
typedef enum
{
    MON_DSP_ICE_UIM_PIN
} MonPinSelectT;

#define MON_DSP_ICE_SELECT  0
#define MON_UIM_SELECT      1

typedef PACKED_PREFIX struct
{
   MonPinSelectT PinSelect;
   uint8         ModeSelect;   /* Use #defines above */
} PACKED_POSTFIX  MonPinMuxingMsgT;

/* Data Structure for DSP JTAG Power Msg */
typedef PACKED_PREFIX struct
{
   uint8  ProcessorId;
   uint8  Power;
} PACKED_POSTFIX  MonDspJtagPowerMsgT;

   /* MON_DSP_SIM_INIT_MSG */
/* There is no message body for this cmd */

   /* MON_DSP_SIM_CMD_MSG */
/* There is no message body for this cmd */

   /* MON_DSP_SIM_EXECUTE_MSG */
typedef PACKED_PREFIX struct
{
   uint8    PcgNum;     /* PCG number at which to start sending SIM msgs to DSPM */
} PACKED_POSTFIX  MonDspSimExecuteMsgT;

typedef PACKED_PREFIX struct
{
   uint16        TaskId;
   uint16        CpuAllotPercent;
} PACKED_POSTFIX  MonSpyCpuAllotStatsT;

/* MON_GET_EXCEPTION_MSG */
typedef PACKED_PREFIX struct
{
   uint32 CallbackAddr;
   uint32 Consume32kTime;
} PACKED_POSTFIX  MonSpyTimerConsumeTimeT;

/* MON_CPBUF_STATUS_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   bool           ResetAllWaterMarksAfterUse;
} PACKED_POSTFIX  MonCpBufferStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint16         NumOfBusyFwdIram;
   uint16         HiWaterMarkFwdIram;
   uint16         NumOfBusyFwdSram;
   uint16         HiWaterMarkFwdSram;
   uint16         NumOfBusyRevIram;
   uint16         HiWaterMarkRevIram;
   uint16         NumOfBusyRevSram;
   uint16         HiWaterMarkRevSram;
   uint16         NumOfBusySigIram;
   uint16         HiWaterMarkSigIram;
   uint16         NumOfBusySigSram;
   uint16         HiWaterMarkSigSram;
   uint16         NumOfBusyHdrIram;
   uint16         HiWaterMarkHdrIram;
   uint16         NumOfBusyHdrSram;
   uint16         HiWaterMarkHdrSram;
   uint16         NumOfBusyFwd1xSram;
   uint16         HiWaterMarkFwd1xSram;
} PACKED_POSTFIX  MonCpBufferStatusRspMsgT;

/* MON_SDIO_S2M_DATA_RCVD_MSG and MON_SDIO_M2S_DATA_RCVD_MSG message structure */
typedef enum
{
    MON_SDIO_SUCCESS,
    MON_SDIO_INVAL_RD_CNT,
    MON_SDIO_NO_BUF_AVAIL,
    MON_SDIO_READ_FAIL,
    MON_SDIO_WRITE_FAIL,
    MON_SDIO_COMPARE_ERR,
    MON_SDIO_LEN_MISMATCH
} MonSdioStatusT;

typedef struct
{
    MonSdioStatusT Status;
    uint16         NumBytesRead;
    uint8         *ReadBufP;
} MonSdioDataRcvdMsgT;

/* MON_SIM_CP_MSG_MSG */    
typedef PACKED_PREFIX struct 
{
   ExeTaskIdT      TaskId;
   ExeMailboxIdT   MboxId;
   uint16          MsgId;
   uint8           MsgBuf[EXE_MAX_ETS_MSG_SIZE];
} PACKED_POSTFIX  MonCpSimMsgT;


/* MON_MMU_CTL_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   uint32         mmuMask;
   uint32         enabled;   
} PACKED_POSTFIX  MonMmuCtlMsgT; 

typedef PACKED_PREFIX struct
{
   uint32         mmuMask;
} PACKED_POSTFIX  MonMmuCtlRspMsgT; 



/* MON_MMU_GET_TLB_ENTRY_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   uint32         address;
   uint32         params;
   uint32         flags;

} PACKED_POSTFIX  MonMmuGetTlbEntryMsgT; 

typedef PACKED_PREFIX struct
{
   uint32         address;
   uint32         tlbEntry;
   uint32         secondLevelAddress;
   uint32         secondLevelTlbEntry;   
   uint8          entryType;
   uint8          CBbits;
   uint8          Domainbits;
   uint8          APbits;
   uint32         mmuAccessAttribute;

   
} PACKED_POSTFIX  MonMmuGetTlbEntryRspMsgT; 



/* MON_MMU_GET_TLB_ENTRY_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  MonMmuGetTlbConfigMsgT; 

typedef PACKED_PREFIX struct
{
	uint32 basePhyAddr;  
	uint32 pageSize; /* how many 1M pages */
	uint32 firstLevelPageType;
	uint32 firstLevelCBbits;
	uint32 firstLevelAPbits;
	uint32 secondLevelBaseAddr;
	uint32 valid;

	char tag[16];   
} PACKED_POSTFIX  MonMmuGetTlbConfigRspMsgT; 



/*---------------------------------------------------------------
*  MonProfile Definitions
*----------------------------------------------------------------*/

/* Max number of thread ids that can be displayed on the Performance Data spy */
#define  MAX_SPY_RECORDS   35

/* Profile database definitions */
typedef PACKED_PREFIX struct
{
   uint16      ThreadId;
   uint32      DeltaTime;
   uint32      TimeStamp;
} PACKED_POSTFIX  ProfileLogT;

/* Performance Data spy structure definitions */
typedef PACKED_PREFIX struct
{
   uint16      ThreadId;
   uint16      Average;
   uint32      Total;
   uint16      Min;
   uint16      Max;
} PACKED_POSTFIX  OverallAveT; /* similar to OverallDataT, except Min and Max are 16 bits */

typedef PACKED_PREFIX struct
{
   uint32         ClockRate;
   uint32         TotalTicks;
   uint32         TotalTime;
   uint32         TotalFrames;
   OverallAveT    OverallData[MAX_SPY_RECORDS];
} PACKED_POSTFIX  MonProfileSpyMsgT;

/* ETS Profile Start Message definitions */
typedef enum
{
   START_IMMEDIATE = 0,
   START_BY_THREAD_OCCURRENCE,
   START_BY_MIN_THREAD_TIME_PER_FRAME
} StartOptionT;

typedef enum
{
   STOP_NEVER = 0,
   STOP_BY_THREAD_OCCURRENCE,
   STOP_BY_MIN_THREAD_TIME_PER_FRAME,
   STOP_BY_TIME,
   STOP_WHEN_BUFFER_FULL
} StopOptionT;

typedef PACKED_PREFIX struct
{
   StartOptionT   StartTriggerOption;
   uint16         StartThreadId;
   uint32         StartTime;           /* scale = Q5 */

   StopOptionT    StopTriggerOption;
   uint16         StopThreadId;
   uint32         StopTime;            /* scale = Q5 */

   bool           CaptureScheduler;

   uint16         SpyInterval;
   bool           DisplayTaskIds;
   bool           DisplayLisrIds;
   bool           DisplayHisrIds;
   bool           DisplayScheduler;

   bool           ProfileMonIdleOnly;
   bool           PeriodicDataReset;
} PACKED_POSTFIX  MonProfileStartMsgT;

/* ETS Profile Stop Message definitions */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  MonProfileStopMsgT;

typedef PACKED_PREFIX struct
{
   uint32         Addr;
   uint32         Size;    /* in 16-bit words */
} PACKED_POSTFIX  DataSectionT;

typedef PACKED_PREFIX struct
{
   DataSectionT   Sections[2];
} PACKED_POSTFIX  MonProfileStopRspMsgT;

/* ETS Profile Get Data Size Message definitions */
typedef MonProfileStopMsgT       MonProfileGetDataSizeMsgT;
typedef MonProfileStopRspMsgT    MonProfileGetDataSizeRspMsgT;

/* ETS Profile Get Debug Info Message definitions */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  MonProfileGetDebugInfoMsgT;

typedef PACKED_PREFIX struct
{
   uint32      InstalledSramSize;
   uint32      AvailableSramSize;

   uint32      AvailableSramAddr;
   uint32      MaxNumOfEntries;

   uint32      DataAddr1;
   uint32      DataSize1;     /* in 16-bit words */
   uint32      NumOfEntries1;

   bool        BufferRolledOver;

   uint32      DataAddr2;
   uint32      DataSize2;     /* in 16-bit words */
   uint32      NumOfEntries2;
} PACKED_POSTFIX  MonProfileGetDebugInfoRspMsgT;

/* ETS Profile Set Spy Display Filters Message definitions */
typedef PACKED_PREFIX struct
{
   uint16         SpyInterval;
   bool           DisplayTaskIds;
   bool           DisplayLisrIds;
   bool           DisplayHisrIds;
   bool           DisplayScheduler;
} PACKED_POSTFIX  MonProfileSetSpyDisplayFiltersMsgT;

/* ETS Profile Set Mode Message definitions */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   uint8          Mode;
} PACKED_POSTFIX  MonProfileSetModeMsgT;

typedef PACKED_PREFIX struct
{
   uint8          Mode;
} PACKED_POSTFIX  MonProfileSetModeRspMsgT;

/* ETS Profile Get Mode Message definitions */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  MonProfileGetModeMsgT;

typedef PACKED_PREFIX struct
{
   uint8          Mode;
} PACKED_POSTFIX  MonProfileGetModeRspMsgT;

/*---------------------------------------------------------------
*  Memory Protection Unit (MPU) Definitions
*----------------------------------------------------------------*/

/* MON_MPU_POPULATED_BLOCK_CTRL_MSG */
typedef enum
{
   MPU_BLOCKSIZE_1BYTE = 0,
   MPU_BLOCKSIZE_2BYTES,
   MPU_BLOCKSIZE_4BYTES,
   MPU_BLOCKSIZE_8BYTES
} MpuBlockSizeT;

typedef PACKED_PREFIX struct
{
   uint32         BlockAddr;
   bool           BlockNum;
   MpuBlockSizeT  BlockSize;
   bool           EnableRead;
   bool           EnableWrite;
   uint8          MasterMask;
} PACKED_POSTFIX  MonMpuPopulatedBlockCtrlMsgT;

/* MON_MPU_UNPOPULATED_SEGMENT_CTRL_MSG */
typedef enum
{
   MPU_SEGSIZE_2MB,
   MPU_SEGSIZE_4MB,
   MPU_SEGSIZE_8MB,
   MPU_SEGSIZE_16MB,
   MPU_SEGSIZE_32MB,
   MPU_SEGSIZE_64MB,
   MPU_SEGSIZE_128MB,
   MPU_SEGSIZE_0MB,
   MPU_SEGSIZE_MIN = MPU_SEGSIZE_0MB
} MpuSegSizeT;

typedef PACKED_PREFIX struct
{
   bool           SegNum;
   MpuSegSizeT    SegSize;
   bool           EnableRead;
   bool           EnableWrite;
   uint8          MasterMask;
} PACKED_POSTFIX  MonMpuUnpopulatedSegmentCtrlMsgT;

/* Define MON Get USB IDs msg */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;
} PACKED_POSTFIX  MonGetUsbIdsMsgT;

typedef PACKED_PREFIX struct
{
  uint16         VendorID;
  uint16         ProductID;
} PACKED_POSTFIX MonGetUsbIdsRspMsgT;

/*---------------------------------------------------------------
*  ETS MonStackChk Definitions
*----------------------------------------------------------------*/
/* Must match mon_spy.txt */
#define MAX_STACK_USAGE_SPY_RECORDS  50
/* Stack Usage spy structure definitions */
typedef PACKED_PREFIX struct
{
   uint16      ThreadId;
   uint16      StackSize;
   uint16      StackUsed;
   uint16      StackUsedPercent;
   uint16      StackCurrentUse;
} PACKED_POSTFIX  StackUsageT; 

typedef PACKED_PREFIX struct
{
   StackUsageT    SpyData[MAX_STACK_USAGE_SPY_RECORDS];
} PACKED_POSTFIX  MonStackUsageSpyMsgT;

typedef PACKED_PREFIX struct
{
   uint16      ThreadId;
   uint32      StackLimit;
   uint16      StackSize;
   uint16      StackUsedPercent;
   uint16      StackCurrentUse;
} PACKED_POSTFIX  FaultStackUsageT; 
typedef PACKED_PREFIX struct
{
   FaultStackUsageT    Data[MAX_STACK_USAGE_SPY_RECORDS];
} PACKED_POSTFIX  MonStackUsageFaultMsgT;

/* Define each memory segmet 
msg MON_MEMORY_SEGMENTS_MSG */
typedef enum
{
	SEG_CP_FLASH,
//	SEG_SRAM0,
	SEG_SRAM,
	SEG_IRAM,
//	SEG_FLASHV,

	SEG_SEGEMENT_LAST
	
}MonSegmentType;

#define SEG_TAG_NAME_SIZE 12

typedef PACKED_PREFIX struct
{
   uint32      seg_startAdr;
   uint32      seg_size;
   uint32      seg_type;
   char        seg_name[SEG_TAG_NAME_SIZE];
} PACKED_POSTFIX MonMemSegAdrAndSizeEntry ;

/* Define the table of segments */
typedef PACKED_PREFIX struct
{
   uint32      NumOfMemSegment;
   MonMemSegAdrAndSizeEntry   segment[SEG_SEGEMENT_LAST];
} PACKED_POSTFIX  MonMemSegAdrAndSize;


/* Define the Response for the Segments address and sizes*/
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   MonMemSegAdrAndSize  RspDatal;
} PACKED_POSTFIX  MonMemSegAdrAndSizeMsgT;

typedef enum 
{
   BOOT_MODE,
   CP_MODE      
}BootCpMode;

/* MON_CP_OR_BOOT_MODE_MSG */
typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
   BootCpMode   boolCp;
} PACKED_POSTFIX  MonCpOrBootModeMsgT;


typedef PACKED_PREFIX struct
{
   BootCpMode   boolCp;
} PACKED_POSTFIX  MonCpOrBootModeRspMsgT;

#if ((defined(MTK_CBP)&& (!defined(MTK_PLT_ON_PC))) && defined(MTK_DEV_LOGIQ))
typedef enum 
{
    LOGIQ_CONTINUEOUS_MODE = 0,
    LOGIQ_ONCE_MODE  
}tLogiqTxOpMode;

/* Define Mon Logiq Msg. */
typedef PACKED_PREFIX struct
{
   uint32  BufAddr;
   uint32  BufLen;
} PACKED_POSTFIX  MonLogiqTxMsgT;
#endif

/*---------------------------------------------------------------
*  System Features Get
*----------------------------------------------------------------*/
 /* MON_GET_SYS_FEATURES_MSG */
typedef PACKED_PREFIX struct
{
  SysFeatureId  feature;
  BOOL          supported;
  UINT32        hwVariation;
  UINT32        swVariation;
} PACKED_POSTFIX  MonSysFeatureSupport;


typedef PACKED_PREFIX struct
{
   ExeRspMsgT     RspInfo;
} PACKED_POSTFIX  MonGetSysFeaturesMsgT;

typedef PACKED_PREFIX struct
{
   SysCarrierId           carrierId;
   uint8                  numFeatures;
   MonSysFeatureSupport   featureSupport[SYS_NUM_FEATURES];
} PACKED_POSTFIX  MonGetSysFeaturesRspMsgT;


/*---------------------------------------------------------------
*  MonHwdProfile Definitions
*----------------------------------------------------------------*/

/* Max number of thread ids that can be displayed on the Hwd Profile spies */
#define  MAX_HWD_PROFILE_SPY_RECORDS   70

/* Hwd Profile spy structure definitions */
typedef PACKED_PREFIX struct
{
   uint16      ThreadId;
   uint16      Average;
   uint16      Max;
} PACKED_POSTFIX  MonHwdProfileStatsT;

typedef PACKED_PREFIX struct
{
   uint32               TotalTime;
   MonHwdProfileStatsT  Stats[MAX_HWD_PROFILE_SPY_RECORDS];
} PACKED_POSTFIX  MonHwdProfileSpyMsgT;

/* ETS Hwd Profile Start Message definitions */
typedef enum
{
   START_TRIG_IMMEDIATE    = 0,
   START_TRIG_CONDITIONAL
} StartTriggersT;

typedef enum
{
   STOP_TRIG_NONE          = 0,
   STOP_TRIG_CONDITIONAL
} StopTriggersT;

typedef PACKED_PREFIX struct
{
   StartTriggersT    StartTrigger;
   StopTriggersT     StopTrigger;
   uint16            SpyInterval;
   bool              PeriodicDataReset;
} PACKED_POSTFIX  MonHwdProfileCpStartMsgT;

typedef PACKED_PREFIX struct
{
   uint8    Emi_Monitor_Rst;
   uint8    Latency_Monitor_En;
   uint8    Type_Monitor_Sel;
   uint8    Type_Monitor_Port_Sel;
   uint8    Type_Monitor_Master_Sel;
   uint8    Type_Monitor_Trans_Type_Sel;
   uint8    Type_Monitor_Beat_Type_sel;
   uint8    Type_Monitor_Size_Type_Sel; 
   uint8    Type_Monitor_Watch_Type_Sel;
} PACKED_POSTFIX  MonHWDEMIProfilingMsgT;

#ifdef MTK_C2K_ELM
typedef PACKED_PREFIX struct
{
   uint8    Latency_Monitor_En;
   uint8    BandWidth_Monitor_En;
} PACKED_POSTFIX  MonHWDELMProfilingMsgT;
#endif

typedef  MonHwdProfileCpStartMsgT   MonHwdProfileDspStartMsgT;


/*------------------------------------------------------------------------
*  Define Global Data
*------------------------------------------------------------------------*/

/*------------------------------------------------------------------------
*  Define Global Function Prototypes
*------------------------------------------------------------------------*/

#ifdef __cplusplus
extern "C" {
#endif
#ifndef MTK_PLT_AUDIO
extern bool MonRestartDspv (void);
#endif
extern bool MonRestartDspm (void);
extern void MonDspAliveInd (uint8 DspProc);
extern void MonSdioGetReadBuf (uint8 **ReadBufPtrP, uint16 *ReadBufLenP);
extern bool MonIsRestartDspvOk (void);
extern bool MonIsRestartDspmOk (void);
extern void MonGetCBPSwVersion (MonCBPSwVersionRspMsgT *RspMsgP);

/*------------------------------------------------------------------------
 *  monsleep.c interface
 *------------------------------------------------------------------------*/
extern void    MonDeepSleepCancel( SysAirInterfaceT Interface, bool WakeCs );
extern void    MonDeepSleepRequest(SysAirInterfaceT Interface, uint32 WakeTime );
extern uint32  MonDeepSleepSuspend( SysAirInterfaceT Interface, MonDeepSleepVetoT BitMask );
extern uint32  MonDeepSleepResume( SysAirInterfaceT Interface, MonDeepSleepVetoT BitMask );
extern bool    MonDeepSleepIsVetoed( SysAirInterfaceT Interface );
extern uint32  MonDeepSleepVetoMaskGet( SysAirInterfaceT Interface );
extern void    MonDeepSleepPendingRequest( SysAirInterfaceT Interface );
#ifdef MTK_CBP
extern void    MonDeepSleepUpdateWakeTime( SysAirInterfaceT Interface, uint32 WakeTime );
extern void    MonDeepSleepStatistInit(bool SetDefaultCheckLength);
extern void    MonDeepSleepStatistCtrlMsg(uint32* CheckLengthP);
#endif
extern uint32  MonDeepSleepGetWakeTime( SysAirInterfaceT Interface );
extern void    MonDeepSleepRestoreRequest( SysAirInterfaceT Interface );

/*------------------------------------------------------------------------
 *  MonFault (and family) interface
 *------------------------------------------------------------------------*/

#ifdef SYS_DEBUG_FAULT_FILE_INFO

#define MonFault(Unit, Code1, Code2, Type)  __MonFault(Unit, __MODULE__, __LINE__, Code1, Code2, Type)

extern void __MonFault(MonFaultUnitT UnitNum, const char *filename, unsigned line,
                       uint32 FaultCode1, uint32 FaultCode2, MonFaultTypeT FaultType);

#else

extern void MonFault(MonFaultUnitT UnitNum, uint32 FaultCode1, uint32 FaultCode2,
                     MonFaultTypeT FaultType);

#endif /* SYS_DEBUG_FAULT_FILE_INFO */

extern void MonHaltDeferred (MonFaultUnitT UnitNum, uint32 FaultCode1, uint32 FaultCode2);

#ifdef MTK_DEV_ENABLE_DSP_DUMP
extern uint32 MonFaultDumpFromDspm (void * DataP, uint32 * SizeInBytesP);
extern void   MonForceFaultToDspm (void);
#endif

extern void _MonTrace( uint32 TraceId, ... );

extern void _MonSpy(uint16 SpyId, uint8 *DataP, uint32 NumBytes);

typedef struct {
   uint8 *DataP;
   uint32   NumBytes;
} MonSpyGatherItemT;

extern void MonSpyGather(uint16 SpyId, uint8 NumSeg, MonSpyGatherItemT * ListP );

extern bool MonSpyInquire(uint16 SpyId);

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/*****************************************************************************
 
  FUNCTION NAME: MonSpyRequire

  DESCRIPTION:

    This routine set the state of a spy to be open.

  PARAMETERS:
 
    SpyId      - The SPY ID

  RETURNED VALUES:

    None.
 
*****************************************************************************/
extern void MonSpyRequire(uint16 SpyId);
#endif

extern void MonSpyDoAutoMsg(uint16 SpyId, uint8 *DataP, uint32 NumBytes );
extern void MonSpyDoSigLog(uint16 SpyId, uint32 Status);

#ifdef MTK_DEV_STARTUP_LOG
extern void MonStartupLogDisable ( void );
extern bool MonStartupLogGetStatus ( void );
#endif

#ifdef MTK_DEV_LOG_SPLM
extern bool MonSilentLogSplmGetFilterHdr ( uint8 * pBuf, uint32 BufLen );
extern void MonSilentLogSplmCfgCreate ( ExeRspMsgT *RspInfoP, char *FileName );
extern void MonSilentLogSplmFilterHandler ( uint8 * pData, uint32 BufLen );
#endif

/* --- Silent Logging API's --- */
/* The following Silent Logging API's are called from IOP */
extern bool MonIsSilentLogEnabled (void);
extern void MonSilentLogDisable (void);
extern bool MonSilentLogMsgStore (uint16 MsgId, uint16 MsgSize, uint8 *MsgP);

/* DBM task uploads silent logging data to ETS using this API */
extern uint16 MonSilentLogUpload (uint8 *DstP, uint16 NumBytes, uint32 Offset, uint32 *TotSizeP);

/* The following Silent Logging API's are called by VAL message interface */
extern void MonSilentLogCfgCreate   (ExeRspMsgT *RspInfoP, char *FileName);
extern void MonSilentLogParmsSet    (ExeRspMsgT *RspInfoP, void *ParmsP, char *FileName);
extern void MonSilentLogParmsGet    (ExeRspMsgT *RspInfoP);
extern bool MonSilentLogParmsUpdate (bool ClearActive, void *ParmsP);
extern void MonSilentLogDataInfoGet (ExeRspMsgT *RspInfoP);
extern void MonSilentLogPause (void);
extern void MonSilentLogResume (void);

#if defined (MTK_DEV_LOG_FILTER_NVRAM)
extern void MonNvramLogControlUpdate( NvramLogCtl * pNvramLogCtl );
extern void MonNvramLogFilterConfigUpdate(void);    
#endif

/*------------------------------------------------------------------------
 *  MonPrintf (and family) interface
 *------------------------------------------------------------------------*/

extern void MonPrintfCritical( char *Fmt, ... );

#ifndef SYS_PRINTF
   extern void MonPrintf(char *fmt, ...);
#else
   #define MonPrintf  MonSimPrintf
   #include <stdio.h>
   extern void MonSimPrintf(char *fmt, ...);
#endif

#define SYS_MAX_SPRINTF_STR_SIZE 80
extern void MonSprintf (char *Buf, const char *Fmt, ...);

#ifdef SYS_DEBUG_FAULT_FILE_INFO
extern void MonPrintFileInfo(char *FileInfo);
#endif /* SYS_DEBUG_FAULT_FILE_INFO */
/*****************************************************************************
  FUNCTION NAME: MonProfileStart

  DESCRIPTION:   This function can be called from any CP task/HISR to start
                 profiling.  It allows the developers to start profiling at
                 a specific point in their software.

                 The default options are:
                     StartTriggerOption = START_IMMEDIATE;
                     StopTriggerOption  = STOP_WHEN_BUFFER_FULL;
                     and the other remaining options are set to TRUE.

                 Users can modify any or all of these options.

  PARAMETERS:    None.

  RETURNED VALUES:  None.
*****************************************************************************/
extern void MonProfileStart (void);

/*****************************************************************************
  FUNCTION NAME: MonProfileStop

  DESCRIPTION:   This function can be called from any CP task/HISR to stop
                 profiling.  It allows the developers to stop profiling at
                 a specific point in their software.

  PARAMETERS:    None.

  RETURNED VALUES:  None.
*****************************************************************************/
extern void MonProfileStop (void);

/*****************************************************************************
  FUNCTION NAME: MonProfileGetDebugInfo

  DESCRIPTION:   Populates a ProfiletGetDebugInfo response structure.

  PARAMETERS:    MsgPtr      - pointer to the response structure.
                 CurrentFlag - FALSE: caller does not care if data is CURRENT or not
                               TRUE:  caller is requesting ONLY current data,
                                      ie, profiling must be in active state

  RETURNED VALUES:  None.
*****************************************************************************/
extern void MonProfileGetDebugInfo (MonProfileGetDebugInfoRspMsgT *RspMsgP, bool CurrentFlag);

/*****************************************************************************
  FUNCTION NAME: MonProfileClose

  DESCRIPTION:   This function adds one last entry to the profiling database when
                 a MON_HALT has occured.

  PARAMETERS:    HaltTime - time of when fault logging starts.

  RETURNED VALUES:  None.
*****************************************************************************/
extern void MonProfileClose (uint32 HaltTime);

/*****************************************************************************
  FUNCTION NAME: MonProfileSpyGetSnapshot

  DESCRIPTION:   Gets a snapshot of the Performance Data spy based on the
                 last x msecs of the data.

  PARAMETERS:    Input:  Duration  - time in msec
                 Output: SpyDataPP - double pointer to spy data
                         SpySizeP  - pointer to spy size

  RETURNED VALUES:  Profile data time
*****************************************************************************/
extern uint32 MonProfileSpyGetSnapshot (uint32 Duration, uint8 **SpyDataPP, uint32 *SpySizeP);

extern void MonProfileSetIdSystem (NU_TASK *TcbP);
extern void MonProfileSetIdScheduler (void);
extern void MonProfileSetIdLisr (uint32 ThreadId);
extern void MonProfileSetIdFiqLisr (uint32 ThreadId);
extern void MonProfileRestoreIdFiqLisr (void);
extern uint32 MonProfileGetTimeStamp (void);

/*------------------------------------------------------------------------
 *  monstackchk.c interface
 *------------------------------------------------------------------------*/
extern void MonStackChkInit (void);
extern void MonStackChk (void);
extern void MonStackChkFaultUpdate (FaultStackUsageT** pStackUsage, uint16* NumStasks);

/*------------------------------------------------------------------------
 *  monhwdprofile.c interface
 *------------------------------------------------------------------------*/

extern void MonHwdProfileSetIdSystem (NU_TASK *TcbP);
extern void MonHwdProfileSetIdScheduler (void);
extern void MonHwdProfileSetIdLisr (uint32 ThreadId);
extern void MonHwdProfileSetIdFiqLisr (uint32 ThreadId);
extern void MonHwdProfileRestoreIdFiqLisr (void);

#ifdef __cplusplus
}
#endif /* __cplusplus */
/*------------------------------------------------------------------------
 *  montimestamp.c interface
 *------------------------------------------------------------------------*/

   /* CmdId for MonTimeStampCtrl() */
typedef enum {
   MON_TSCMD_INIT,      /* init timestamp data */
   MON_TSCMD_START,  /* start timer */
   MON_TSCMD_RESET,  /* stop timer, flush data */
   MON_TSCMD_SPY     /* show data, reset */
} MonTimeStampCtrlCmdT;

   /* CmdId for MonTimeStampSetup() */
typedef enum {
   MON_TSCMD_STATUS, /* enable/disable timestamping */
   MON_TSCMD_IDS,    /* ids to enable/disable */
   MON_TSCMD_ALL     /* enable/disable all timestamps */
} MonTimeStampSetupCmdT;

   /* if SYS_DEBUG_MONTIMESTAMP is disabled, make timestamp no-ops */
#ifdef SYS_DEBUG_MONTIMESTAMP
extern void MonTimeStampCtrl( MonTimeStampCtrlCmdT CmdId );
extern void MonTimeStampSetup( MonTimestampSetupMsgT *CmdArg );
extern void MonTimeStampAdd( uint8 Id );

#endif /* SYS_DEBUG_MONTIMESTAMP */

/*------------------------------------------------------------------------
 * moninfocache.c critical interface
 *------------------------------------------------------------------------*/
#ifdef SYS_DEBUG_MONINFOCACHE

extern void MonInfoCacheCriticalDump( void );

#endif /* SYS_DEBUG_MONINFOCACHE */

#ifdef SYS_OPTION_PACKAGE_DSP_IMG
void * MonGetDspm(void);
unsigned long int MonGetDspmSize(void);
#ifndef MTK_PLT_AUDIO
void * MonGetDspv(void);
unsigned long int MonGetDspvSize(void);
#endif
#endif

#ifdef MTK_DEV_ADVANCE_PROFILER
/**********************************************
* descriptor:    users can add a prober in their code for detect the
                       users' code executing
                       
* @id:              a Id provided by caller
* @exe_cnt:     the counter of the AddProber executed, 0 means for ever
**********************************************/
void AddProber(uint8 id, uint32 exe_cnt);

/**********************************************
* descriptor:    The API is used to insert a frame counte value,
                       when the frame counter is changed, call the API
                       
* @intf:           the air interface, 1X or evdo
**********************************************/
void AddFrameRecord(SysAirInterfaceT intf);

/**********************************************
* descriptor:    The API is used to export all the profile record info,
                      It is called in the ETS task now
**********************************************/
void ExportProfileRecords(void);

/**********************************************
* descriptor:    The API is used to start profiling, it provides a 
                      mean to startup by user in code, not bt ETS Tool
      
* @ms:           the time interval to export profile record info.
                      It must be greater than or equal to 20
**********************************************/
void StartupRecord(uint16 ms);

/**********************************************
* descriptor:    The API is used to stop profiling, it provides a 
                      mean to stop by user in code, not bt ETS Tool
      
**********************************************/
void StopRecord(void);
#endif


/*--------------------------------------------
 * monutil.c - miscellaneous I/F
 *--------------------------------------------*/
extern uint16 MonReadChipId(void);

#define MonUtilAllocHeap(SizeP)    __MonUtilAllocHeap(SizeP, __MODULE__, __LINE__)
extern uint32 __MonUtilAllocHeap (uint32 *SizeP, const char *Filename, unsigned Linenumber);
extern void MonEnablePrintf(bool Enable);

#ifdef SYS_DEBUG_UART_PRINTF
/*--------------------------------------------
 * mondbgprint.c - miscellaneous I/F
 *--------------------------------------------*/
extern void dbg_print(char *fmt,...);
#endif

#ifdef MTK_DEV_LOG_FILTER_NVRAM
extern void MonNvramLogFilterConfigGet ( void );
#endif

#endif /* MONAPI_H */

/*****************************************************************************
* End of File
*****************************************************************************/
/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:15:43 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:20:11 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
/**Log information: \main\Trophy\Trophy_hzhang_href22149\1 2013-04-17 01:42:05 GMT hzhang
** HREF#22149 to change MON_MAX_FILEINFO_STR_SIZE.**/
/**Log information: \main\Trophy\2 2013-04-18 02:09:18 GMT zlin
** HREF#22149, merge code.**/
/**Log information: \main\Trophy\Trophy_SPI\1 2014-01-08 04:09:38 GMT fwu
** HREF#0000: Modified  for ESPI solution.**/
/**Log information: \main\Trophy\Trophy_fwu_href22348\1 2014-01-08 06:17:30 GMT fwu
** HREF#22348. Merge ESPI related source code.**/
/**Log information: \main\Trophy\3 2014-01-09 06:34:44 GMT zlin
** HREF#22348, merge code.**/
