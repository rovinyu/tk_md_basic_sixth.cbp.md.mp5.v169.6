/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 1998-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _MONVTST_H_
#define _MONVTST_H_
/*****************************************************************************
* 
* FILE NAME   : monvtst.h
*
* DESCRIPTION : Vocoder Test 
*
*   The vocoder test init function and ETS message function are defined here.
*   The common variables used to determine when the vocoder testing is complete
*   are also defined here.  There are two variables based on types defined here.
*
*  
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "sysdefs.h"

/*----------------------------------------------------------------------------
 Global MON Data
----------------------------------------------------------------------------*/

/* Info passed down from ETS */
extern uint16  *MonVTstDownloadAddr;        
extern uint16  *MonVTstUploadAddr;        
extern uint16  *MonVTstDownloadStartAddr;        
extern uint16  *MonVTstUploadStartAddr;        
extern uint32   MonVTstDownloadLength;  
extern uint32   MonVTstUploadLength;         
extern bool     MonVTstTransmitPacket;
extern bool     MonVTstEnableHw;
extern uint32   MonVTstPacketSize;

/* Test variables */
extern uint32        MonVTstCounter;
extern MonVTstModeT  MonVTstMode;         

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
extern void  MonVTstRsp(void);
extern void  MonVTstInit(void);
extern void  MonVTstMsg(void* MsgDataP);
extern void  MonVtstUploadSpeechPacket(uint16* MsgDataP, uint32 MsgSize);
extern void  MonVTstLisr(uint32 VocoderStatus);

/*****************************************************************************
* $Log: monvtst.h $
* Revision 1.2  2004/03/25 11:46:02  fpeng
* Updated from 6.0 CP 2.5.0
* Revision 1.1  2003/05/12 15:26:25  fpeng
* Initial revision
* Revision 1.3  2002/06/04 08:07:21  mshaver
* Added VIA Technologies copyright notice.
* Revision 1.2  2001/12/18 13:53:11  cmastro
* monvtst.h - 1.2 - added *StartAddr variables
* Revision 1.1  2001/10/17 07:50:22  mshaver
* Initial revision
* Revision 1.2  2001/10/16 18:23:26  vxnguyen
* Added new function prototypes.
* Revision 1.1  2000/10/06 13:53:24  mshaver
* Initial revision
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
