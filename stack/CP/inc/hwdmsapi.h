/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _HWDMS_H_
#define _HWDMS_H_
/***********************************************************************************
* 
* FILE NAME   :     hwdmsapi.h
*
* DESCRIPTION :     Mix signal interface defination
*
* HISTORY     :
*     See Log at end of file
*
************************************************************************************/

#include "hwdadcapi.h"
#include "hwdrfapi.h"

/*----------------------------------------------------------------------------
 Global Typedefs
----------------------------------------------------------------------------*/
typedef enum
{ 
   PDM_NUMBER0 = 0,
   PDM_NUMBER1 = 1, 
   PDM_NUMBER2 = 2, 
   PDM_NUMBER3 = 3, 
   PDM_NUMBER4 = 4,
   PDM_NUMBER5 = 5,
   PDM_NUMBER6 = 6,
   PDM_MAX_NUM_SUPPORTED = 7
}PdmNumberT;

/* The following target Rx ADC Vrms input level identifiers are use to
** configure the Rx SD digital gain and A1/A2 select bits. */
typedef enum
{
   RX_ADC_INPUT_TARGET_8MV_RMS = 0, /* Nominal */
   RX_ADC_INPUT_TARGET_6MV_RMS = 1,
   RX_ADC_INPUT_AUTO_ADJUSTED  = 2,  /* Rx ADC input level managed by DSPM (i.e. digital Rx AGC) */
   RX_ADC_INPUT_TARGET_4MV_RMS = 3,
   RX_ADC_INPUT_TARGET_2MV_RMS = 4
} HwdRxAdcInputLevelT;

/* The following Rx DC bias rate identifiers are used to configure various
** DC Bias decimation parameters used by the DSPM */
typedef enum
{
   RX_ADC_NORMAL_DC_BIAS = 0,
   RX_ADC_FAST_DC_BIAS   = 1
} HwdRxAdcDcBiasRateT;

/* The following Rx ADC Power level identifiers are used to configure the
** power consumption in the Rx Stage1 (2nd order) and Rx Stage2 (4th order)
** filters */
typedef enum
{
   RX_ADC_PWR_QUARTER_LESS_THAN_NOMINAL = 0,
   RX_ADC_PWR_EIGHTH_LESS_THAN_NOMINAL  = 1,
   RX_ADC_PWR_NOMINAL                   = 2,
   RX_ADC_PWR_EIGHTH_MORE_THAN_NOMINAL  = 3
} HwdRxAdcStagePwrLvlT;

/* Common Voltage Source configuration options */
typedef enum
{
   TX_COMMON_MODE_VOLTAGE_EXTERNAL = 0,
   TX_COMMON_MODE_VOLTAGE_INTERNAL = 1
} HwdTxCmvSourceT;

typedef enum
{ 
   HWD_IC_MAIN_RX = 0,
   HWD_IC_DIV_RX,
   HWD_IC_MAIN_TX,
   HWD_IC_PATH_MAX
}HwdIcPathT;

/* The following gain settings roughly conform (i.e. rounded) to the register
** settings used by the CF_TXSD_COARSE_GAIN_SEL hardware register */
#define SYS_OPTION_USE_TX_GAIN_OFFSET

typedef enum
{
#ifndef SYS_OPTION_USE_TX_GAIN_OFFSET
    TX_IQ_250MV_VPTPD_SETTING,
    TX_IQ_450MV_VPTPD_SETTING,
    TX_IQ_500MV_VPTPD_SETTING,
    TX_IQ_750MV_VPTPD_SETTING,
    TX_IQ_1000MV_VPTPD_SETTING,
    TX_IQ_2000MV_VPTPD_SETTING
#else
    TX_IQ_250MV_VPTPD_SETTING,
    TX_IQ_450MV_VPTPD_SETTING,
    TX_IQ_500MV_VPTPD_SETTING,
    TX_IQ_890MV_VPTPD_SETTING,
    TX_IQ_500MV_VPTPD_SETTING_ADJ,
    TX_IQ_750MV_VPTPD_SETTING_ADJ,
    TX_IQ_890MV_VPTPD_SETTING_ADJ,
    TX_IQ_1000MV_VPTPD_SETTING_ADJ,
    TX_IQ_1250MV_VPTPD_SETTING_ADJ,
    TX_IQ_1400MV_VPTPD_SETTING_ADJ,
    TX_IQ_1500MV_VPTPD_SETTING_ADJ
#endif   // SYS_OPTION_USE_TX_GAIN_OFFSET
} HwdTxIQLevelT;

/* The following gain settings are used to identify a target Tx clipper
** setting which are subsequently used to configure a set of variables
** sent the the DSPM */
typedef enum
{
    TX_CLIPPER_NOMINAL_SETTING = 0,
    TX_CLIPPER_DISABLED        = 1
} HwdTxClipperLevelT;

typedef enum
{
    DEFAULT_FILTER_MODE = 0,
    NEW_IIR5_MODE  = 1,
    CFG3_FILTER_MODE  = 2
} HwdRxFilterModeT;

/* The following data structure is used to configure RF-Dependent MXS variables */
typedef struct
{
   /* Main RF path Rx ADC Power consumption setting */
   uint16 MainRxSdCurSw;          /* CF_RXSD_CURSW register setting */

   /* Diversity RF path ADC Power consumption setting */
   uint16 DivRxSdCurSw;           /* CF_RXSD_CURSW register setting */

   /* Tx Common Mode Voltage source */
   uint16 TxCmvSource;        /* CF_TXSD_CMVSEL register setting */

   /* Tx I/Q Vptp gain select */
   uint16 TxGainSelect;       /* CF_TXSD_COARSE_GAIN_SEL register setting */
   uint16 TxGainSelect3bitDac;/* CF_TXSD_COARSE_GAIN_SEL register setting */
   uint16 TxTrimSetting;      /* CF_TXSD_GDIQ register setting */
   uint16 TxGainCompA1;       /* DSPM TX_GAIN_COMP1 register setting */
   uint16 TxGainCompA2;       /* DSPM TX_GAIN_COMP2 register setting */
   uint16 TxGainSelectDspm;   /* DSPM TX_GAIN_SEL register (if Tx SD configured for DSPM control) */

   /* Tx DAC Clipper settings */
   uint16 TxClipperLevel;     /* Tx (reverse) Clipper level */
   uint16 TxClipperThresh;    /* Tx (reverse) Clipper threshold */
   uint16 TxClipperMaxSigmaX; /* Tx (reverse) Clipper Max allowed Sigma X (input RMS value) */
   uint16 TxClipperSigmaP;    /* Tx (reverse) Clipper Sigma P (pilot RMS value) */

   /* Main RF path Phase Equalizer setting */
   uint16 MainPhaseEqualizerCtrl; /* CF_RXSD_PHASE_EQ register setting */

   /* Diversity RF path Phase Equalizer setting */
   uint16 DivPhaseEqualizerCtrl;  /* CF_RXSD_PHASE_EQ register setting */

   /* Sec/Aux RF path Phase Equalizer setting */
   uint16 SecPhaseEqualizerCtrl;  /* CF_RXSD_PHASE_EQ register setting */

   uint16 MainRxSdEnableDO;   /* HWD_M_RXSD_ENABLES register setting for DO mode */
   uint16 MainRxSdEnable1X;   /* HWD_M_RXSD_ENABLES register setting for 1X mode */

   uint16 DivRxSdEnableDO;    /* HWD_D_RXSD_ENABLES register setting for DO mode */
   uint16 DivRxSdEnable1X;    /* HWD_D_RXSD_ENABLES register setting for 1X mode */

   uint16 RxGainUpRegMain;
   
   uint16 RxsdApIirA2Main;
   uint16 RxsdApIirA1Main;

   uint16 RxGainUpRegDiv;
   
   uint16 RxsdApIirA2Div;
   uint16 RxsdApIirA1Div;

   uint16 RxGainUpRegSec;
   
   uint16 RxsdApIirA2Sec;
   uint16 RxsdApIirA1Sec;

   uint16 SecRxSdEnableDO;    /* HWD_S_RXSD_ENABLES register setting for DO mode */
   uint16 SecRxSdEnable1X;    /* HWD_S_RXSD_ENABLES register setting for 1X mode */

} HwdMxsConstantsT;

typedef struct
{
   uint16 A1Val;              /* TX_GAIN_COMP1 register setting */
   uint16 A2Val;              /* TX_GAIN_COMP2 register setting */
   int16 GainAdj;             /* DO Tx gain adjustment - Q8 ALog2 */

} HwdTxA1A2GainCompT;

/* DO Gain Reductions */
typedef enum
{
   DO_GAIN_ADJ_0_0_DB,
   DO_GAIN_ADJ_0_5_DB,
   DO_GAIN_ADJ_1_0_DB,
   DO_GAIN_ADJ_1_5_DB,
   DO_GAIN_ADJ_2_0_DB,
   DO_GAIN_ADJ_2_5_DB,
   DO_GAIN_ADJ_3_0_DB,
   DO_GAIN_ADJ_3_5_DB,
   DO_GAIN_ADJ_4_0_DB,
   DO_GAIN_ADJ_4_5_DB,
   MAX_GAIN_ADJ
}DoGainAdjT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* Sine tone generation configuration parameters */
typedef enum
{
    HWD_MS_SIN_RANGE_0_DB = 0,    /* +-1V */
    HWD_MS_SIN_RANGE_NEG_6_DB,      /* +-1/2V */
    HWD_MS_SIN_RANGE_NEG_12_DB,      /* +-1/4V */
    HWD_MS_SIN_RANGE_NEG_18_DB,      /* +-1/8V */
    HWD_MS_SIN_RANGE_NEG_24_DB,     /* +-1/16V */
    HWD_MS_SIN_RANGE_NEG_30_DB,     /* +-1/32V */

    HWD_MS_SIN_RANGE_7_8V,      /* +-7/8V */
    HWD_MS_SIN_RANGE_3_4V,      /* +-3/4V */
    HWD_MS_SIN_RANGE_5_8V,      /* +-5/8V */
    HWD_MS_SIN_RANGE_3_8V,      /* +-3/8V */
    HWD_MS_SIN_RANGE_NUM,
}HwdMsSinRangeT;

typedef struct
{
    bool enI;                   /* [in]  Enable/Disable sine tone at I path */
    HwdMsSinRangeT rangeI;      /* [in]  sine wave swing at I path */
    uint16 phaseI;              /* [in]  sine wave start phase at I path. In unit of degrees */
    uint32 freqI;               /* [in]  sine wave frequency at I path. In unit of Herz */
    bool enQ;                   /* [in]  Enable/Disable sine tone at Q path */
    HwdMsSinRangeT rangeQ;      /* [in]  sine wave swing at Q path */
    uint16 phaseQ;              /* [in]  sine wave start phase at Q path. In unit of degrees */
    uint32 freqQ;               /* [in]  sine wave frequency at Q path. In unit of Herz */

    uint32 realFreqI;           /* [out] The actual frequency generated at I path. In uint of Herz */
    uint32 realFreqQ;           /* [out] The actual frequency generated at Q path. In uint of Herz */
}HwdMsSinCfgT;

typedef struct
{
    bool en;
}HwdMsSinGenT;
#endif
/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
#if (SYS_ASIC <= SA_CBP82)
extern uint32  PdmCtrlAddressTable[PDM_MAX_NUM_SUPPORTED];
extern uint32  PdmDoutAddressTable[PDM_MAX_NUM_SUPPORTED];
extern uint32  PdmDinAddressTable[PDM_MAX_NUM_SUPPORTED];
extern uint32  PdmDoutSecondAddressTable[PDM_MAX_NUM_SUPPORTED];
#endif
extern HwdMxsConstantsT HwdMxsConstants;
#ifdef SYS_OPTION_USE_TX_GAIN_OFFSET
extern DoGainAdjT A1A2TblIndex1X, A1A2TblIndexDO, DoReduction;
extern HwdTxA1A2GainCompT   TxA1A2GainCompTable[];
extern int16 TxGainAdjALog2;
#endif

extern uint16 LowerBoundary, UpperBoundary;
extern bool GsmCdmaInterferSolutionEnable;
/*----------------------------------------------------------------------------
 Global Defines 
----------------------------------------------------------------------------*/

/*  CBP7x RxSD REGSEL Control bit fields */
#define HWD_RXSD_REGSEL_CP_SELECT   0x0000
#define HWD_RXSD_REGSEL_DM_SELECT   0x0004

/* Processor control select */
#define HWD_MS_CP_SELECT        0x0000
#define HWD_MS_DM_SELECT        0x0001
#define HWD_MS_TX_REGSEL        HWD_MS_CP_SELECT

/* in RAM has to have the same definition */
#define HWD_MS_PDM0_REGSEL      HWD_MS_CP_SELECT
#define HWD_MS_PDM1_REGSEL      HWD_MS_DM_SELECT
#define HWD_MS_PDM2_REGSEL      HWD_MS_DM_SELECT
#define HWD_MS_PDM3_REGSEL      HWD_MS_CP_SELECT
#define HWD_MS_PDM4_REGSEL      HWD_MS_CP_SELECT
#define HWD_MS_PDM5_REGSEL      HWD_MS_CP_SELECT
#define HWD_MS_PDM6_REGSEL      HWD_MS_CP_SELECT

/*- - - - - - - - - - - - - - - - -*/
/* Define mix signal configuration */
/*- - - - - - - - - - - - - - - - -*/

#define HWD_TX_DAC_NOT_GATED_PWR_CTRL   0
#define HWD_TX_DAC_GATED_PWR_CTRL       1
#define HWD_TX_DAC_PWR_CTRL             HWD_TX_DAC_GATED_PWR_CTRL

/* Voice ADC/DAC constants */
#define HWD_POWER_DOWN              0x0001
#define HWD_POWER_UP                0x0000
#define HWD_RX_2X2_DISABLED         0x0002
#define HWD_RX_2X2_ENABLED          0x0000
#define HWD_RX_2X2_1X_USED          HWD_RX_2X2_DISABLED
#define HWD_RX_2X2_DO_USED          HWD_RX_2X2_ENABLED

/* HWD_RXSD_CRSW (0x0b880010)*/
#define HWD_RX_POWER_CONS_CTRL      0x0000  /* Rx SD Power consumption -25% of nominal */
#define HWD_RX_2X2_POWER_CONS_CTRL  0x0004  /* Rx SD Power consumption - nominal */
#define HWD_RX_POWER_CONSUMPTION    (HWD_RX_POWER_CONS_CTRL | HWD_RX_2X2_POWER_CONS_CTRL)


/* HWD_RXSD_ENABLES  (0x0B880078) */
#define HWD_IIR5_NEW_ENABLE         0x0001
#define HWD_IIR5_NEW_DISABLE        0x0000
#define HWD_EXT_DC_OFFSET_ENABLE    0x0002
#define HWD_EXT_DC_OFFSET_DISABLE   0x0000
#define HWD_STAGE2_ENABLE           0x0004
#define HWD_STAGE2_DISABLE          0x0000
#define HWD_IIR5_H4_ENABLE          0x0000	/* 1--> disable, 0 --> enable*/
#define HWD_IIR5_H4_DISABLE         0x0008	/* 1--> disable, 0 --> enable*/
#define HWD_STAGE2_2X1_ENABLE       0x0010
#define HWD_STAGE2_2X1_DISABLE      0x0000

#define HWD_RX_ENABLES_CDMA         (HWD_EXT_DC_OFFSET_DISABLE | HWD_STAGE2_DISABLE | HWD_IIR5_H4_ENABLE | HWD_IIR5_NEW_DISABLE)
#define HWD_RX_ENABLES_DO           (HWD_EXT_DC_OFFSET_DISABLE | HWD_STAGE2_ENABLE  | HWD_IIR5_H4_ENABLE | HWD_IIR5_NEW_DISABLE)

#define HWD_RX_ENABLES_CDMA_NEW_IIR5 (HWD_EXT_DC_OFFSET_DISABLE | HWD_STAGE2_DISABLE | HWD_IIR5_H4_DISABLE | HWD_IIR5_NEW_ENABLE)
#define HWD_RX_ENABLES_DO_NEW_IIR5   (HWD_EXT_DC_OFFSET_DISABLE | HWD_STAGE2_ENABLE  | HWD_IIR5_H4_DISABLE | HWD_IIR5_NEW_ENABLE)

#define HWD_RX_ENABLES_CDMA_FILTER_CFG3 (HWD_EXT_DC_OFFSET_DISABLE | HWD_STAGE2_DISABLE | HWD_IIR5_H4_ENABLE | HWD_IIR5_NEW_ENABLE)
#define HWD_RX_ENABLES_DO_FILTER_CFG3   (HWD_EXT_DC_OFFSET_DISABLE | HWD_STAGE2_ENABLE  | HWD_IIR5_H4_ENABLE | HWD_IIR5_NEW_ENABLE)

/*  CBP7x Rx AGC Control bit fields */
#define HWD_RXAGC_CTRL_DIGIGAIN_SRC_SELECT   (1<<0)
#define HWD_RXAGC_CTRL_RSS_CLEAR             (1<<1)
#define HWD_RXAGC_CTRL_UPDT_CLEAR            (1<<2)
#define HWD_RXAGC_CTRL_RSS_DISABLE           (1<<3)
#define HWD_RXAGC_CTRL_DIS_INNERLOOP_I       (1<<4)
#define HWD_RXAGC_CTRL_DIS_INNERLOOP_Q       (1<<5)
#define HWD_RXAGC_CTRL_CLR_PWR_CORR          (1<<6)
#define HWD_RXAGC_CTRL_OUTLOOP_SEL           (1<<7)
#define HWD_RXAGC_CTRL_DIS_CLR_SG            (1<<8)
#define HWD_RXAGC_CTRL_IQ_SWAP               (1<<9)

/* Bit definitions for HWD_(M/D)_RXSD_DC_IIR_COEF */
#define RXSD_DC_IIR_COEF_FAST            (0x001C)
#define RXSD_DC_IIR_COEF_FAST_DO         (0x001B)
#define RXSD_DC_IIR_COEF_FAST_1X         (0x001B)
#define RXSD_DC_IIR_COEF_NOM             (0x001A)
#define RXSD_DC_IIR_COEF_SLOW            (0x0018)
#define RXSD_DC_IIR_COEF_VERY_SLOW       (0x0017)
#define RXSD_DC_IIR_MASK                 (0x001F)

#define RXSD_CIC_ORDER_SEL                0x0020
#define RXSD_DC_IIR_COEF_GSM_CDMA_HPF    (RXSD_DC_IIR_COEF_FAST)

#define RXSD_DC_IIR_COEF_IDLE_DO_DEFAULT  RXSD_DC_IIR_COEF_NOM

#define RXSD_DC_IIR_COEF_IDLE_DO_DEFAULT  RXSD_DC_IIR_COEF_NOM
#define RXSD_DC_IIR_COEF_1X_TRAFFIC      (RXSD_DC_IIR_COEF_SLOW)

/* Bit definitions for ABB HWD_HIFI_PLL_M register */
#define HWD_HIFI_PLL_M_DIV                0x01FF       

/* Bit definitions for ABB HWD_HIFI_PLL_N register */
#define HWD_HIFI_PLL_N_DIV                0x007F       

/* Bit definitions for ABB HWD_HIFI_CLK_CTRL register */
#define HWD_HIFI_CLKDIV_SEL               0x0F         

/* Bit definitions for ABB HWD_CBB_VADCM_CTRL and HWD_CBB_VADCN_CTRL registers */
#define HWD_VADC_PD               (1<<0)   /* Main voice ADC analog circuits power down */
#define HWD_VADC_LNAGAIN          (1<<1)   /* Main Voice ADC LNA powered up */
#define HWD_VADC_SEL              (3<<2)   /* Select voice ADC input */
#define HWD_VADC_SEL_MAIN         (0<<2)   /* Select voice ADC input from main MIC */
#define HWD_VADC_SEL_AUX          (1<<2)   /* Select voice ADC input from AUX MIC  */
#define HWD_VADC_SEL_SPKOUT       (2<<2)   /* Select voice ADC input from AUX MIC  */
#define HWD_VADC_PGA              (7<<4)   /* Main VADC PGA */
#define HWD_VADC_RESETN           (1<<7)   /* Main Voice ADC analog is out of reset */

/* Bit definitions for ABB HWD_CBB_MIC_HEADSET_CTRL register */
#define HWD_MICPD                 (1<<0)
#define HWD_HEADDET_EN            (1<<1)
#define HWD_HEADSET_DB            (0x3F<<2)

/* Bit definitions for ABB HWD_HIFI_PLL_CTRL register */
#define HWD_HIFI_PLL_PD           (1<<0)   /* HIFI PLL power down */
#define HWD_HIFI_PLL_G            (7<<1)   /* HIFI PLL charge pump control bits */
#define HWD_HIFI_PLLCK_EN         (1<<4)   /* Select PLL clock */
#define HWD_HPLL_BP_LOCK          (1<<5)   /* Bypass HW PLL lock indicator */
#define HWD_HPLL_SW_LOCK          (1<<6)   /* SW lock indicator */
#define HWD_HPLL_MODE             (1<<7)   /* (R only) HIFI clock is generated by the HIFI PLL clock out */
#define HWD_HIFI_PLL_LOCK         (1<<8)   /* (R only) PLL locked */

/* Bit definitions for ABB HWD_CBB_HIFIDAC_CH_PD register */
#define HWD_HIFILPD                    (1<<0)   /* HiFi DAC left-channel power down */
#define HWD_HIFIRPD                    (1<<1)   /* HiFi DAC right-channel power down */
#define HWD_HIFI_VCMPD                 (1<<2)   /* HiFi VCM & VBG power down */
#define HWD_HIFIL_SCFPD                (1<<3)   /* HiFi left channel SCF power down */
#define HWD_HIFIR_SCFPD                (1<<4)   /* HiFi right channel SCF power down */
#define HWD_HIFIL_SEPD                 (1<<5)   /* HiFi SE Left chan short to GND */
#define HWD_HIFIR_SEPD                 (1<<6)   /* HiFi SE Right chan short to GND */
#define HWD_HIFIL_PGAPD                (1<<7)   /* HiFi Left channel PGA power down */
#define HWD_HIFIR_PGAPD                (1<<8)   /* HiFi Right channel PGA power down */
#define HWD_PD_SEVCM                   (1<<9)   /* 0 = Short HiFi SE output to VCM
                                                   1 = Power down VCM circuit for HiFi SE out */

/* Bit definitions for ABB HWD_CBB_HIFIDAC_POP register */
#define HWD_VDACPOP                    (1<<0)   /* Disable Spkr SE path de-pop */
#define HWD_VDACPOPRES                 (3<<1)   /* POP resistor mask for Spkr SE Path */
#define HWD_VDACPOPRES_50_OHM          (0<<1)   /* Select 0.05k resistor for Spkr SE path */
#define HWD_VDACPOPRES_100_OHM         (1<<1)   /* Select 0.1k resistor for Spkr SE path */
#define HWD_VDACPOPRES_250_OHM         (2<<1)   /* Select 0.25k resistor for Spkr SE path */
#define HWD_VDACPOPRES_500_OHM         (3<<1)   /* Select 0.5k resistor for Spkr SE path */

#define HWD_NORPOP                     (1<<3)   /* Disable Spkr Diff path de-pop */
#define HWD_NORPOPRES                  (3<<4)   /* POP resistor mask for Spkr Diff Path */
#define HWD_NORPOPRES_500_OHM          (0<<4)   /* Select 0.5k resistor for Spkr Diff path */
#define HWD_NORPOPRES_1K_OHM           (1<<4)   /* Select 1k resistor for Spkr Diff path */
#define HWD_NORPOPRES_5K_OHM           (2<<4)   /* Select 5k resistor for Spkr Diff path */
#define HWD_NORPOPRES_10K_OHM          (3<<4)   /* Select 10kk resistor for Spkr Diff path */

#define HWD_RNGLPOP                    (1<<6)   /* Disable Rngr Left Diff path de-pop */
#define HWD_RNGLPOPRES                 (3<<7)   /* POP resistor mask for Rngr Left Diff Path */
#define HWD_RNGLLPOPRES_500_OHM        (0<<7)   /* Select 0.5k resistor for Rngr Left Diff path */
#define HWD_RNGLPOPRES_1K_OHM          (1<<7)   /* Select 1k resistor for Rngr Left Diff path */
#define HWD_RNGLPOPRES_5K_OHM          (2<<7)   /* Select 5k resistor for Rngr Left Diff path */
#define HWD_RNGLPOPRES_10K_OHM         (3<<7)   /* Select 10kk resistor for Rngr Left Diff path */

#define HWD_RNGRPOP                    (1<<9)   /* Disable Rngr Right Diff path de-pop */
#define HWD_RNGRPOPRES                 (3<<10)  /* POP resistor mask for Rngr Right Diff Path */
#define HWD_RNGRLPOPRES_500_OHM        (0<<10)  /* Select 0.5k resistor for Rngr Right Diff path */
#define HWD_RNGRPOPRES_1K_OHM          (1<<10)  /* Select 1k resistor for Rngr Right Diff path */
#define HWD_RNGRPOPRES_5K_OHM          (2<<10)  /* Select 5k resistor for Rngr Right Diff path */
#define HWD_RNGRPOPRES_10K_OHM         (3<<10)  /* Select 10kk resistor for Rngr Right Diff path */

/* Bit definitions for ABB HWD_CBB_HIFIDAC_ZC_CMPPD register */
#define HWD_PD_HIFIZCL                 (1<<0)   /* HiFi Left-chan zero-crossing comparator power down */
#define HWD_PD_HIFIZCR                 (1<<1)   /* HiFi Right-chan zero-crossing comparator power down */

/* Bit definitions for ABB HWD_CBB_HIFIDAC_ZC_SEL register */
#define HWD_HIFILPGA_SEL               (1<<0)   /* Hifi Left-chan PGA zero-crossing update path sel */
#define HWD_HIFIRPGA_SEL               (1<<1)   /* Hifi Right-chan PGA zero-crossing update path sel */

/* Bit definitions for ABB HWD_VADC_CLK_CTRL register */
#define HWD_VC_CLK_SEL                 (1<<0)   /* 0 = 2.4MHz, 1 = 4.8MHz */

/* Mode detting for HwdMsSetDcOffsetParms() function */
#define HWD_MS_DC_IIR_COEF_FAST            0
#define HWD_MS_DC_IIR_COEF_NOM             1
#define HWD_MS_DC_IIR_COEF_SLOW            2
#define HWD_MS_DC_IIR_COEF_VERY_SLOW       3

/* RXSD_ADC_MODE control bit fields */
#define HWD_RXSD_ADC_CDMA_MODE      0x0001
#define HWD_RXSD_ADC_GPS_MODE       0x0002
#define HWD_RXSD_ADC_DO_MODE        0x0004

/* HWD_TXSD_OPER (0x0B88010C) */
#define HWD_TX_AUX_DISABLE          0x01 /* Tx sigma-delta Aux ADC isolation mux disabled */
#define HWD_TX_TUNE_PD              0x02 /* Tx filter calibration, tune oscillator circuit is powered down */
#define HWD_TX_NORMAL_OPER          (HWD_TX_AUX_DISABLE | HWD_TX_TUNE_PD)
/* HWD_TXSD_CMVSEL (0x0b880118) */
#define HWD_TX_CMV_INT_SOURCE       0x01
#define HWD_TX_CMV_EXT_SOURCE       0x00
#define HWD_TX_CMV_SOURCE_SEL       HWD_TX_CMV_INT_SOURCE
/* HWD_TXSD_GAIN_SEL (0x0b88011C) */

#define HWD_TX_025_VPTOPD_3BIT      0x07
#define HWD_TX_050_VPTOPD_3BIT      0x05
#define HWD_TX_075_VPTOPD_3BIT      0x04
#define HWD_TX_100_VPTOPD_3BIT      0x03
#define HWD_TX_125_VPTOPD_3BIT      0x02
#define HWD_TX_150_VPTOPD_3BIT      0x01
#define HWD_TX_200_VPTOPD_3BIT      0x00

#define HWD_TX_025_VPTOPD           0x00
#define HWD_TX_050_VPTOPD           0x03
#define HWD_TX_075_VPTOPD           0x04
#define HWD_TX_100_VPTOPD           0x05
#define HWD_TX_150_VPTOPD           0x06
#define HWD_TX_200_VPTOPD           0x07

#define HWD_TX_GAIN_SEL_AMPS        HWD_TX_025_VPTOPD
#define HWD_TX_GAIN_SEL_CDMA        HWD_TX_100_VPTOPD
/* HWD_TXSD_PWRSEL (0x0b88014C) */
/* HWD_TXSD_GDIQ (0x0b880150) */
#define HWD_TX_SD_DAC_TRIM_IQ           0x88 /* Tx SD DAC nominal output setting */
/* HWD_CF_AA_SCALE (0x0b880310) */
#define HWD_AA_SCALE_VALUE              0x01

/* CF_RESERVED1 (0x0b880448) Bit definitions */
#define HWD_CF_BYPASS_AUXADC1          (1<<3)
#define HWD_CF_BYPASS_AUXADC0          (1<<2)
#define HWD_CF_BYPASS_HPADC            (1<<1)
#define HWD_CF_BYPASS_HPDAC_DE         (1<<0)

/* CF_RESERVED (0x0b880444) Bit definitions */
#define HWD_CF_VC1_DIS                 (1<<15)
#define HWD_CF_VC_DIS                  (1<<14)
#define HWD_CF_USB_TEST                (0x1f<<9) /* these bits has to be zeroed for normal operation */
#define HWD_CF_PDM4_DIS                (1<<8)
#define HWD_CF_PDM3_DIS                (1<<7)
#define HWD_CF_PDM2_DIS                (1<<6)
#define HWD_CF_PDM1_DIS                (1<<5)
#define HWD_CF_PDM0_DIS                (1<<4)
#define HWD_CF_PDMs_DIS                (HWD_CF_PDM1_DIS | HWD_CF_PDM2_DIS | HWD_CF_PDM3_DIS)
#define HWD_CF_TX_DIS                  (1<<3)
#define HWD_CF_RXSD_DIS_D              (1<<2)
#define HWD_CF_RXSD_DIS_M              (1<<1)

#define HWD_CF_RXSD_DIS_S              (1<<0)

/*------------------------------------------------------------------------
*  Voice Codec CF_VC_SEL register bit definitions
*------------------------------------------------------------------------*/

/* HWD_CF_VC_ADCPD (0x0b880400) */
#define HWD_VC_ADC_PD_MASK          0x01
#define HWD_VC_ADC_LNA_EN_MASK      0x02
#define HWD_RX_LNA_PD_MASK          0x04 /* BB7 only */

/* HWD_CF_VC_DACPOWER (0x0b880404) */
#define HWD_VDAC_PD_RNGL            0x01 /* 0 = Differential ringer left output PA power up (stereo left) 
                                            1 = Differential ringer left output PA power down (stereo left) */
#define HWD_VDAC_PD_RNGR            0x02 /* 0 = Differential ringer right output PA power up (stereo right)      
                                            1 = Differential ringer right output PA power down (stereo right) */ 
#define HWD_VDAC_PD_HIFIL           0x04 /* 0 = Hifi left channel output PA power up
                                            1 = Hifi left channel output PA power down */
#define HWD_VDAC_PD_HIFIR           0x08 /* 0 = Hifi right channel output PA power up
                                            1 = Hifi right channel output PA power down */
#define HWD_VDAC_PD_NOR             0x10 /* 0 = Differential handset output PA power up (stereo right)
                                            1 = Differential handset output PA power down (stereo right) */
#define HWD_VDAC_PD_DRV             0x20 /* 0 = All HiFi DAC output amplifiers power up 
                                            1 = All HiFi DAC output amplifiers power down */

/* HWD_CF_VC_SEL (0x0b880414) */
#define HWD_VDAC_SW_CTRL_ALL        0x7F 
#define HWD_VDAC_HIFI_MSEL          0x01 /* "0" Single ended headset output impedances are low    
                                            "1" Single ended headset output impedances are high */
#define HWD_VDAC_NOR_MSEL           0x02 /* "0" Differential handset output impedance is low     
                                            "1" Differential handset output impedance is high */ 
#define HWD_VDAC_HIFI_SWL           0x04 /* "0" Disconnect SE left channel amp input from left VDAC 
                                            "1" Connect SE left channel amp input from left VDAC */ 
#define HWD_VDAC_HIFI_SWR           0x08 /* "0" Disconnect SE right channel amp input from right VDAC 
                                            "1" Connect SE right channel amp input from right VDAC */ 
#define HWD_VDAC_HIFIL_RNGSW        0x10 /* "0" Disconnect differential ringer amp from left VDAC 
                                            "1" Connect differential ringer amp from left VDAC */ 
#define HWD_VDAC_HIFIR_RNGSW        0x20 /* "0" Disconnect differential ringer amp from right VDAC 
                                            "1" Connect differential ringer amp from right VDAC */ 
#define HWD_VDAC_HIFI_SPKSW         0x40 /* "0" Disconnect handset speaker amp from VDAC 
                                            "1" Connect handset speaker amp from VDAC */ 

/* HWD_CF_VC_CLKPD defs */
#define HWD_VC_DAC_CLKPD            0x01 /* Voice analog/digital clock power down */
#define HWD_VC_HIFI_CLKPD           0x02 /* Hifi voice DAC clock power down */
#define HWD_VC_CLKPD_MAIN           0x01 /* Main Voice ADC digital filter clock gated off */
#define HWD_VC_CLKPD_NOISE          0x04 /* Noise cancelling Voice ADC digital filter clock gated off */

/*------------------------------------------------------------------------
* Mixed signal Voice Codec CF_VC_CTRL register bit definitions (CBP8 only)
*------------------------------------------------------------------------*/
/* HWD_CF_VC_CTRL (0xb820444) */
#define HWD_CF_VC_DISABLE_DV        0x02 /* Disable DSPv access to shared CF_VC* registers */
#define HWD_HIFI_BYPASS_DE          0x08
#define HWD_VC_BYPASS_HPADC         0x10
#define HWD_SEL_16XSH               0x20

/*------------------------------------------------------------------------
* Mixed signal Voice Codec CF_VC_DIGPOWER register bit definitions 
*------------------------------------------------------------------------*/

#define HWD_CF_VC_DIGPOWER_HIFI_RESETN         0x0008  /* HIFI digital logic in reset */

#define HWD_VC_DIGPD_MAIN       0x0001  /* Main Voice ADC digital filter in reset  */
#define HWD_VC_DIGPD_NOISE      0x0010  /* Noise Canceller Voice ADC digital filter in reset */


/*------------------------------------------------------------------------
* Mixed signal Voice Codec CF_VC1_POP register bit definitions 
*------------------------------------------------------------------------*/
#define HWD_CF_VC1_POP_VDACG_MASK     0x00ff
#define HWD_CF_VC1_POP_CTRL_DISC      0x0100   /* "0" Connect selected pop control resistor
                                                  "1" Open circuit pop control resistor */

/* De-pop resistor mask and values */
#define HWD_CF_VC1_POP_RESISTOR_MASK  0x0600  
#define HWD_CF_VC1_POP_500_OHM_RES    0x0000
#define HWD_CF_VC1_POP_1K_OHM_RES     0x0200
#define HWD_CF_VC1_POP_5K_OHM_RES     0x0400
#define HWD_CF_VC1_POP_10K_OHM_RES    0x0600

/*------------------------------------------------------------------------
* Hifi DAC FM Select bit definitions (HWD_CF_FMSEL_PGA)
*-------------------------------------------------------------------------*/
/* FM PGA Definitions */
#define HWD_CF_FM_PGA_MASK            3

#define HWD_CF_FM_PGA_18_DB           0
#define HWD_CF_FM_PGA_12_DB           1
#define HWD_CF_FM_PGA_6_DB            2
#define HWD_CF_FM_PGA_0_DB            3

/* FM Input select mask and definitions */
#define HWD_CF_FMSEL_INP_SEL_MASK     0x0C
#define HWD_CF_FMSEL_VDAC_INP_SEL     0x00  /* Select voice DAC input to output amplifiers */
#define HWD_CF_FMSEL_LINE_IN_INP_SEL  0x04  /* Select FM line-in input to output amplifiers */
#define HWD_CF_FMSEL_SUM_BOTH_INP_SEL 0x08  /* Select summation of DAC input and FM line input */
#define HWD_CF_FMSEL_INP_SEL_NONE     0x0C  /* None */

#define HWD_CF_FMSEL_HPF_EN           0x10  /* Enable FM HPF */


#define HWD_VOICE_DAC_MUTE          1
#define HWD_VOICE_DAC_ENABLE        0
#define HWD_32OHM_DAC_OUT           1
#define HWD_64OHM_DAC_OUT           0
#define HWD_ADC_AUX_INPUT           1
#define HWD_ADC_HANDSET_INPUT       0

#define HWD_VC_DACPD_MASK           0x01
#define HWD_VC_DACAMP1_MASK         0x02
#define HWD_VC_DACAMP2_MASK         0x04

#define HWD_ANALOG_GAIN_0_DB        0x00
#define HWD_ANALOG_GAIN_3_DB        0x01
#define HWD_ANALOG_GAIN_6_DB        0x02
#define HWD_ANALOG_GAIN_9_DB        0x03
#define HWD_ANALOG_GAIN_12_DB       0x04
#define HWD_ANALOG_GAIN_15_DB       0x05
#define HWD_ANALOG_GAIN_18_DB       0x06
#define HWD_ANALOG_GAIN_21_DB       0x07

#define HWD_DIGITAL_GAIN_0_DB       0x00
#define HWD_DIGITAL_GAIN_6_DB       0x01
#define HWD_DIGITAL_GAIN_12_DB      0x02
#define HWD_DIGITAL_GAIN_18_DB      0x03
#define HWD_DIGITAL_GAIN_24_DB      0x04

#define HWD_VC_NORMAL_OPERATION     0

/* HWD_TX_DSM_SEL_1X */
#define HWD_TX_DSM_SEL_3BIT         1

/* HWD_TX_MULT_GAIN_1X */
#define HWD_TX_MULT_GAIN_DEFAULT    0x1f

/* HWD_TX_SHIFT_GAIN_1X */
#define HWD_TX_SHIFT_DEFAULT        0x01

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/* HWD_TX_FIR_H_1X */
#define HWD_TX_DSM_DROOP_EN         (0x1 << 8)
#else
#define HWD_TX_DSM_DROOP_EN         (0x1 << 8)
#define HWD_TX_DSM_DROOP_DIS        (0x0 << 8)
#endif
#define HWD_TX_DSM_FIR_H_DEFAULT    0xf0

/*------------------------------------------------------------------------
* Hifi DAC Filter Coefficients
*-------------------------------------------------------------------------*/
#define HWD_HIFI_DAC_IIRA2_COEFF    0x1455    /* HWD_CF_VC_LOOPBACKD */
#define HWD_HIFI_DAC_IIRB2_COEFF    0x3cb3    /* HWD_CF_VC_BPA       */
#define HWD_HIFI_DAC_IIRB1_COEFF    0x0ef8    /* HWD_CF_VC_BPD       */

#define HWD_HIFI_DAC_LOOPBACKD_ENA  0x8000    /* In HWD_CF_VC_LOOPBACKD reg */

/*------------------------------------------------------------------------
* HWD_CBB_TX_PWR_CTRL1/2 registers
*-------------------------------------------------------------------------*/
#define HWD_TXM_PWRONI             (0x01)
#define HWD_TXM_PWRONQ             (0x02)
#define HWD_TXM_PWRSEL             (0x04)
#define HWD_TXM_SELEXTI            (0x08)
#define HWD_TXM_SELEXTQ            (0x10)

#if 0//#ifdef MTK_CBP
#define HWD_TXM_DEFAULT_EN         (HWD_TXM_PWRSEL | HWD_TXM_SELEXTI | HWD_TXM_SELEXTQ)
#else
#define HWD_TXM_DEFAULT_EN         (HWD_TXM_PWRONI | HWD_TXM_PWRONQ | HWD_TXM_PWRSEL | HWD_TXM_SELEXTI | HWD_TXM_SELEXTQ)
#endif
#define HWD_TXM_DEFAULT_DIS        (HWD_TXM_SELEXTI | HWD_TXM_SELEXTQ)

/*- - - - - - - - - - - - - - -	-*
 * Mix signal register interface *
 *- - - - - - - - - - - - - - - -*/

#define HwdMsSetAdcmAnalogGain(gain)  HwdMsRegisterWrite (HWD_CBB_VADCM_CTRL, \
                                           HWD_VADC_PGA, ((gain & 0x7) << 4))
#define HwdMsSetAdcnAnalogGain(gain)  HwdMsRegisterWrite (HWD_CBB_VADCN_CTRL, \
                                           HWD_VADC_PGA, ((gain & 0x7) << 4)) 

#define HwdMsSetAdcDigitalGain(gain) HwdWrite(HWD_CF_VC_ADCRANGE, gain); 

#define HwdMsGetDacAnalogGain()     (HwdMsValueRead(HWD_CBB_HIFIDAC_PGA))
#define HwdMsSetDacAnalogGain(gain) HwdMsRegisterWrite(HWD_CBB_HIFIDAC_PGA, 0x3ff, gain)

#define HwdMsGetDacDigitalGain()     HwdRead8(HWD_CF_VC_DACPGA)
#define HwdMsSetDacDigitalGain(gain) HwdWrite8(HWD_CF_VC_DACPGA, (gain)) 


#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define    HWD_MAIN_RESET_TIMER_ID          1
#define    HWD_DIV_RESET_TIMER_ID           2
#endif

#ifdef MTK_PLT_DENALI
/* TX DAC output power in units of 1/32 dBm. 
 * Defult to 4.3dBm for Everest if GBB0 and GBB1 both equal to 0 dB
 Defult to 4.74dBm for Jade if GBB0 is 0*/
#define HWD_MS_TX_DAC_OUT_PWR        ((uint16)((43 * 32 + 5) / 10))
#define HWD_MS_TX_DAC_OUT_PWR_SIM        ((uint16)((474 * 32 + 50) / 100))
#else
#define HWD_MS_TX_DAC_OUT_PWR        ((uint16)((474 * 32 + 50) / 100))
#endif

#if (SYS_ASIC >= SA_MT6735)
/* PDM is removed */
#define HwdMsPdmRegSelect(num, sel)
#define HwdMsSetNormalPdmMode()
#define HwdMsSetTestPdmMode()
#define HwdMsPdmWrite(num, data)
#define HwdMsPdmSecondWrite(num, data)
#define HwdMsPdmPowerDown(num)
#define HwdMsPdmPowerUp(num)
#else
/*- - - - - - - -*
 * PDM Interface *
 *- - - - - - - -*/
/* sel = 00 CP control
         01 DSPM control
         10 DSPV control */
#define HwdMsPdmRegSelect(num, sel)	HwdWrite(PdmCtrlAddressTable[num], sel)

/* bit = 0 selects internal PDM generator output as input to analog PDM cell (normal operation)
         1 selects external test input (MXS test mode) */
#define HwdMsSetNormalPdmMode() 	HwdWrite(HWD_PDM_TESTSEL, 0)
#define HwdMsSetTestPdmMode() 	HwdWrite(HWD_PDM_TESTSEL, 1)

/* num - pdm number */
#define HwdMsPdmWrite(num, data)  HwdWrite(PdmDoutAddressTable[num], data)
#define HwdMsPdmSecondWrite(num, data)  HwdWrite(PdmDoutSecondAddressTable[num], data)

#define HwdMsPdmPowerDown(num) \
         HwdWrite(HWD_PDM_PWR_DN, HwdRead(HWD_PDM_PWR_DN) | (1 << num)); \
         HwdMsRegisterWrite(HWD_CBB_PDM_PWR_CTRL, 0x1<<num, 0x1<<num)

#define HwdMsPdmPowerUp(num) \
         HwdWrite(HWD_PDM_PWR_DN, HwdRead(HWD_PDM_PWR_DN) & ~(1 << num)); \
         HwdMsRegisterWrite(HWD_CBB_PDM_PWR_CTRL, 0x1<<num, 0x0<<num)
#endif

#define HwdMsBgVoltageTrim(bits)    HwdMsRegisterWrite(HWD_CBB_BG_CTRL, 0x1ff, (bits<<3))

/* Define Macro that manages the HWD_TX_GAIN_COMP1_DO register */
#define HwdSetDOGainComp1(Data) \
   HwdWrite(HWD_TX_GAIN_COMP1_DO, (Data))

/* Define Macro that manages the HWD_TX_GAIN_COMP1_DO register */
#define HwdSetDOGainComp2(Data) \
   HwdWrite(HWD_TX_GAIN_COMP2_DO, (Data))

/* Define Macro that manages the HWD_TX_GAIN_COMP1_1X register */
#if (defined MTK_PLT_DENALI) || (defined MTK_PLT_ON_PC)
#define HwdSet1XGainComp1(Data) \
   HwdWrite(HWD_TX_GAIN_COMP1_1X, (Data))
#else
#define HwdSet1XGainComp1(Data) \
   HwdWrite(HWD_JADE_TX_GAIN_COMP1_1X, (Data))
#endif

/* Define Macro that manages the HWD_TX_GAIN_COMP2_1X register */
#if (defined MTK_PLT_DENALI) || (defined MTK_PLT_ON_PC)
#define HwdSet1XGainComp2(Data) \
   HwdWrite(HWD_TX_GAIN_COMP2_1X, (Data))
#else
#define HwdSet1XGainComp2(Data) \
   HwdWrite(HWD_JADE_TX_GAIN_COMP2_1X, (Data))
#endif

/* Define Macro that manages the HWD_TX_AP_IIR2_A1_CP_DO register */
#define HwdSetDOEqualCoeff1(Data) \
   HwdWrite(HWD_TX_AP_IIR2_A1_CP_DO, (Data))

/* Define Macro that manages the HWD_TX_AP_IIR2_A2_CP_DO register */
#define HwdSetDOEqualCoeff2(Data) \
   HwdWrite(HWD_TX_AP_IIR2_A2_CP_DO, (Data))

/* Define Macro that manages the HWD_TX_AP_IIR2_A1_CP_1X register */
#define HwdSet1XEqualCoeff1(Data) \
   HwdWrite(HWD_TX_AP_IIR2_A1_CP_1X, (Data))

/* Define Macro that manages the HWD_TX_AP_IIR2_A2_CP_1X register */
#define HwdSet1XEqualCoeff2(Data) \
   HwdWrite(HWD_TX_AP_IIR2_A2_CP_1X, (Data))

/* Define Macro that manages the HWD_TX_EQUAL_EN_DO register */
#define HwdSetDOEqualEnable() \
   HwdWrite(HWD_TX_EQUAL_EN_DO, 0x1)

#define HwdSetDOEqualDisable() \
   HwdWrite(HWD_TX_EQUAL_EN_DO, 0x0)


/* MXS ABB interrupt sources */
#define HWD_MXS_AUXADC_EOC_INTRPT      (1<<0)
#define HWD_MXS_HEADSET_DET_INTRPT     (1<<1)
#define HWD_MXS_HEADSET_BTN_INTRPT_EN  (1<<2)
#define HWD_MXS_TCH_SCREEN_WAKE        (1<<3)
#define HWD_MXS_TS_CONV_ERR_INTRPT     (1<<4)
#define HWD_MXS_TS_CONV_DONE_INTRPT    (1<<5)
#define HWD_MXS_TS_12p5ms_INTRPT       (1<<6)
#define HWD_MXS_TS_PENDOWN_INTRPT      (1<<7)
#define HWD_MXS_TS_PENUP_INTRPT        (1<<8)

#define HWD_MXS_ALL_INTERRUPTS         (0x1ff)

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* MixedSys operation types */
typedef enum
{
   RX_ADC_INIT,
   RX_ADC_ON,
   RX_ADC_OFF,
   TX_DAC_INIT,
   TX_DAC_ON,
   TX_DAC_OFF,
   SIN_GEN_CONFIG_I,
   SIN_GEN_CONFIG_Q,
   SIN_GEN_DISABLE_I,
   SIN_GEN_DISABLE_Q,
   SIN_GEN_ENABLE,
   SIN_GEN_DISABLE,
   MS_REG_WRITE,
   MS_REG_READ,
   MS_REG_BIT_SET,
   MS_REG_BIT_CLEAR,
   MS_APC_DAC_INIT,
   MS_APC_DAC_ON,
   MS_APC_DAC_OFF,
   MS_APC_DAC_WR_IMMED,
   MS_APC_DAC_WR_DLY
}HwdMsOperT;

/* DFE operation types */
typedef enum
{
   HWD_RX_DFE_SW_OFF,
   HWD_RX_DFE_SW_ON,
   HWD_TX_DFE_SW_OFF,
   HWD_TX_DFE_SW_ON
}HwdDfeOperT;


/* register write for logging in HW simulation */

#ifdef MTK_DEV_HW_SIM
/* Debug Trace */
#define HwdMsDbgTrace(num, ...)  MonTrace(MON_CP_HWD_MS_DBG_TRACE_ID, num, __VA_ARGS__)
#else
/* Debug Trace */
#define HwdMsDbgTrace(num, ...)
#endif /* MTK_DEV_HW_SIM */

#endif
/*----------------------------------------------------------------------------
 Global Variables
----------------------------------------------------------------------------*/
extern uint16 RxsdDcIirCoefIdleDoVal;
extern bool Hwd1xInTraffic;

/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/
extern void HwdMsInit(void);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void HwdMsReadAuxAdcReasults(HwdAdcChanIdT ChanId, uint16 MeasResult);
extern void HwdMsCalInitCal(void);
extern void HwdRxDcBiasCalStart(void);
extern void HwdRxDcBiasMsgDisable(void);
extern void HwdMsDcBiasParmSend(void);
#endif
extern void HwdDspmAliveProcessMsg(void);
#ifndef MTK_PLT_AUDIO
extern void HwdDspvAliveProcessMsg(void);
#endif
extern void HwdDspmSchAlgValueConfig(bool UseDefaults, uint16 *ActSetTimer, uint16 *AverageNum,
	                                     uint16 *PwrRptTimer);
extern void HwdDspmSchAlgValue1Config(bool UseDefaults, uint16 *SysNumFingers, uint16 *MaxFingerBs,
                                        uint16 *SearchTimeMin, uint16 *CohIaqThresh);
extern void HwdConfigureDSPM(void);
extern void HwdMsConfigMainRxDcBias(HwdRxAdcDcBiasRateT Rate);
extern void HwdMsConfigMainRxPowerSetting(HwdRxAdcStagePwrLvlT Setting);
extern void HwdMsConfigMainTxCmv(HwdTxCmvSourceT Source);
extern void HwdMsConfigMainTxIQ(HwdTxIQLevelT Setting);
extern void HwdMsConfigMainTxDlyValue(uint16 TxMdmDelay);
extern void HwdMsConfigMainTxClipperLevel(HwdTxClipperLevelT Setting);
#if ((!defined MTK_CBP) || defined(MTK_PLT_ON_PC))
extern void HwdMsConfigMainPhaseEqualizer(bool enable, uint16 A2, uint16 A1);
#endif
extern void HwdMsConfigDivRxDcBias(HwdRxAdcDcBiasRateT Rate);
extern void HwdMsConfigDivRxPowerSetting(HwdRxAdcStagePwrLvlT Setting);
#if ((!defined MTK_CBP) || defined(MTK_PLT_ON_PC))
extern void HwdMsConfigDivPhaseEqualizer(bool enable, uint16 A2, uint16 A1);
#endif
extern void HwdMsConfigInterfMode(SysAirInterfaceT AirInterfaceType, uint8 RfPath);
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void HwdMsSetDcOffsetParms(SysAirInterfaceT AirInterfaceType, uint8 RfPath, uint8 Mode, bool CallbackReq);
extern void HwdMsConfigMainRxAdcTarget(HwdRxAdcInputLevelT Target);
extern void SwitchDefault1xAfcPdmControl(uint16 Proc);
extern void HwdMsConfigMainIQGainOffset(uint16 I_Offset, uint16 Q_Offset);
extern void HwdMsConfigDivIQGainOffset(uint16 I_Offset, uint16 Q_Offset);
#endif
extern void HwdMsConfigMainIIRSetting(HwdRxFilterModeT Setting);
extern void HwdMsConfigDivIIRSetting(HwdRxFilterModeT Setting);
extern void HwdMsResetModeVariables(void);
#if ((!defined MTK_CBP) || defined(MTK_PLT_ON_PC))
extern void HwdMsRxSourceSelect(SysAirInterfaceT Interface, HwdRfMpaEnumT RfPath, HwdIcPathT ICPath);
extern void HwdMsSetEvdoSchClockFreq(uint16 Divider);

extern void HwdMxsGeneralLisr(void);
extern void HwdMxsSpiReadDoneLisr(void);

extern void HwdMsSetTxDcOffset(SysAirInterfaceT Mode, uint32 Path);
#endif

extern void HwdMsApcDacInit(void);
extern void HwdMsApcDacOn(SysAirInterfaceT Interface);
extern void HwdMsApcDacOff(SysAirInterfaceT Interface);
extern void HwdMsApcDacWriteImmed(uint16 voltageMv);
extern void HwdMsApcDacWriteDelayLoad(uint16 voltageMv, uint8 hslotBoundary);

extern void HwdMsAfcPdmControl(SysAirInterfaceT Interface);
extern bool HwdMsAfcSingleTcxoCtrl(void);

extern void HwdMsProcessActivePilotData(HwdActivePilotDataMsgT *MsgP);
extern void HwdMsClearRunningAverage(void);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void Hwd1xTrafficStart( bool InTraffic );
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* AFE interface for DENALI and afterwards */
extern void HwdMsTxDacEnableSwMode(void);
extern void HwdMsTxDacDisableSwMode(void);
extern uint32 HwdMsSetSineTone(uint32 Frequency, uint8 OnOff, bool TestMode);
extern void HwdMsSetRcCalResult(uint16 rcCalVal);
extern uint16 HwdMsGetRcCalResult(void);
#endif /* MTK_CBP */
extern void HwdMsRestoreTxRegisters(SysAirInterfaceT Interface);
#if (SYS_BOARD >= SB_JADE)
extern void HwdMsRst(void);
#endif

#ifdef MTK_PLT_DENALI
#include "../hwd/hwdmsdenali.h"
#endif /* MTK_PLT_DENALI */

#if (SYS_BOARD >= SB_JADE)
#include "../hwd/hwdmsjade.h"
#endif

#ifdef MTK_DEV_HW_SIM_RF
#include "../hwd/hwdmshwsim.h"
#endif /* MTK_DEV_HW_SIM_RF */

#if (defined MTK_DEV_DUMP_REG)
#ifndef M_HwdMsRegLogRdAll
#define M_HwdMsRegLogRdAll()
#endif
#endif

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_ylxiao_href22060\1 2013-03-26 07:38:04 GMT ylxiao
** HREF#22060, bugfix for DMA**/
/**Log information: \main\Trophy\1 2013-04-03 02:41:06 GMT hzhang
** HREF#22060 to merge code from Tropby 0.3.x**/
/**Log information: \main\Trophy_0.3.X\1 2013-03-26 02:24:09 GMT fwu
** HREF#21981.**/
/**Log information: \main\Trophy\Trophy_fwu_href22082\1 2013-04-03 02:26:28 GMT fwu
** HREF#22082, Modified to support UART1 to be the AT channel between AP and CP.**/
/**Log information: \main\Trophy\2 2013-04-03 02:56:59 GMT hzhang
** HREF#22082 to merge code.**/
