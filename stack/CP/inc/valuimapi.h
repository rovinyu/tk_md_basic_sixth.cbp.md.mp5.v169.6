/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2005-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef VALUIMAPI_H
#define VALUIMAPI_H
/*****************************************************************************
 
  FILE NAME:  valuimapi.h

  DESCRIPTION:

   This file contains all constants and typedefs needed by
   valuimapi.c. It provides the main UIM functions to VAL layer.


*****************************************************************************/
#include "uimapi.h"
#include "valapi.h"
#include "valphbapi.h"
#include "valsmsapi.h"
#ifdef MTK_DEV_C2K_IRAT
#include "valutkapi.h"
#endif

#ifdef  __cplusplus
extern "C" {
#endif

#define VAL_CHV_MAX_INPUT_NUM           3
#define VAL_CHV_UNBLOCK_MAX_INPUT_NUM  10
#define VAL_UIM_MAX_REG_IDS             2
#define VAL_UIM_MAX_MDN_COUNT          10 
#ifdef MTK_CBP
/* according to 3.4.34 of TIA/EIA-95-B of C.S0023 */
#define VAL_MAX_DIGITS_IN_EF_MDN       15
#endif
#define VAL_UIM_MAX_SMSVP_COUNT 4

/* CHV Status read from UIM card */
typedef PACKED_PREFIX struct
{
  CardStatusT          CardStatus;
  ChvStatusT           ChvStatus;
#ifdef MTK_DEV_C2K_IRAT
  bool                 UsimAppExisted;
#endif
  uint8                ChvSum;       /* Sum of CHV attempt */
  uint8                PukSum;       /* Sum of UNBLOCK CHV attempt */
  uint8                Chv1LeftCount;
  uint8                Chv2LeftCount;
  uint8                Puk1LeftCount;
  uint8                Puk2LeftCount;
} PACKED_POSTFIX  ValChvStatusT;

#ifdef MTK_DEV_C2K_IRAT
extern uint8 ValUimInfoReadStatus;

#define VAL_UIM_SP_NAME_READ_MASK   0x01
#define VAL_UIM_MDN_READ_MASK       0x02
#define VAL_UIM_ICCID_READ_MASK     0x04
#define VAL_UIM_NAM_DATA_READ_MASK  0x08
#define VAL_UIM_INFO_ALL_READ_MASK  0x0F

#define UIM_UTK_MENU_SIZE 256

#define VAL_UTK_TIMER_MANAGER_NUM 8

#ifdef __CARRIER_RESTRICTION__
/* the basic unit of categories */
#define VAL_UML_SIZE_OF_CAT_NET     3 /* MCC/MNC 5 digits */
#define VAL_UML_SIZE_OF_CAT_SPN     23 /* MCC/MNC + SPN(20bytes) from MD1 different from MD3 32 bytes */
#define VAL_UML_SIZE_OF_CAT_IMSI_PREFIX 4 /* MCC/MNC + IMSI digits 6 and 7 */
#define VAL_UML_SIZE_OF_CAT_GID1    4 /* MCC/MNC + GID1 */
#define VAL_UML_SIZE_OF_CAT_GID2    4 /* MCC/MNC + GID2 */

/* Define the maximum suppot elements of each category */
#define VAL_UML_MAX_SUPPORT_CAT_NUM     5

/* Define the size of each category */
#define VAL_UML_CFG_CAT_NET_SIZE            (VAL_UML_MAX_SUPPORT_CAT_NUM * VAL_UML_SIZE_OF_CAT_NET)
#define VAL_UML_CFG_CAT_SPN_SIZE            (VAL_UML_MAX_SUPPORT_CAT_NUM * VAL_UML_SIZE_OF_CAT_SPN)
#define VAL_UML_CFG_CAT_INSI_PREFIX_SIZE    (VAL_UML_MAX_SUPPORT_CAT_NUM * VAL_UML_SIZE_OF_CAT_IMSI_PREFIX)
#define VAL_UML_CFG_CAT_GID1_SIZE           (VAL_UML_MAX_SUPPORT_CAT_NUM * VAL_UML_SIZE_OF_CAT_GID1)
#define VAL_UML_CFG_CAT_GID2_SIZE           (VAL_UML_MAX_SUPPORT_CAT_NUM * VAL_UML_SIZE_OF_CAT_GID2)

/* Define the support category size */
#define VAL_UML_SUPPORT_CAT_SIZE    VAL_UML_CAT_SIZE 

#define VAL_UML_PRIORITY_CHECK_WHITE_LIST 1 /* allowed_carriers_prioritized, 0 - Allow All except Black, 1 - Allow White except Black */
#define VAL_UML_MUTIL_SIM_POLICY          1 /* allowed_carriers_prioritized, 0 - Allow All except Black, 1 - Allow White except Black */
#endif

typedef enum
{
  VAL_UTK_CSIM_INIT_COMPLETED = 0x1,
  VAL_UTK_PHB_FILE_COMPLETED = 0x2,
  VAL_UTK_SMS_FILE_COMPLETED = 0x4,
  VAL_UTK_UIM_INFO_FILE_COMPLETED = 0x8,
  // for test
  //VAL_UTK_ALL_FILE_COMPLETED_MASK = 0xf
#if 1  
  VAL_UTK_HLP_FILE_COMPLETD = 0x10,
  VAL_UTK_ALL_FILE_COMPLETED_MASK = 0x1F
#endif  
}ValUimUtkFileMaskTypeT;

typedef enum
{
    VAL_CAT_TYPE_STK,
    VAL_CAT_TYPE_UTK,
    VAL_CAT_TYPE_NUM
} ValCatTypeE;

typedef PACKED_PREFIX struct
{
  ValCatTypeE   CatType;
  uint8         StoredUtkMenu[UIM_UTK_MENU_SIZE];
  uint16        UtkMenuLen;
} PACKED_POSTFIX ValUimUtkInfoT;

enum
{
    UTK_FILE_ADN = 0x6F3A,
    UTK_FILE_FDN = 0x6F3B,  
    UTK_FILE_SMS = 0x6F3C, 
    UTK_FILE_SPN = 0x6F41,
    UTK_FILE_MDN = 0x6F44,
    UTK_FILE_CST = 0x6F32,
    UTK_FILE_EST = 0x6F75,
    UTK_FILE_ICCID = 0x2FE2,
    UTK_FILE_HRPDCAP = 0x6F56,
    UTK_FILE_HRPDUPP = 0x6F57,
    UTK_FILE_ME3GPD = 0x6F48,
    UTK_FILE_3GPDOPM = 0x6F49,
    UTK_FILE_SIPCAP = 0x6F4A,
    UTK_FILE_MIPCAP = 0x6F4B,
    UTK_FILE_SIPUPP = 0x6F4C,
    UTK_FILE_MIPUPP = 0x6F4D,
    UTK_FILE_SIPSP = 0x6F4E,
    UTK_FILE_MIPSP = 0x6F4F,
    UTK_FILE_SIPPAPSS = 0x6F50,
    UTK_FILE_MIPFLAGS = 0x6F78,
    UTK_FILE_C_MIPFLAGS = 0x6F84,
    UTK_FILE_SIPUPPEXT = 0x6F7D,
    UTK_FILE_MIPUPPEXT = 0x6F80,
    UTK_FILE_TCPCONFIG = 0x6F79,
    UTK_FILE_C_TCPCONFIG = 0x6F88,
    UTK_FILE_DGC = 0x6F7A,
    UTK_FILE_C_DGC = 0x6F89,
    UTK_FILE_IPV6CAP = 0x6F77,
    UTK_FILE_C_IPV6CAP = 0x6F87
    /* add other files here */
};

#define VAL_UIM_FILE_LIST_MAX 50

typedef struct
{
  uint8   FileCount;
  uint16  FileId[VAL_UIM_FILE_LIST_MAX];
}ValUimFileListT;

#endif

/* Define CHV type */
typedef enum
{
  VAL_CHV1 = 1,
  VAL_CHV2
} ValChvIdT;

/* Define CHV operation response result */
typedef enum
{
  VAL_CHV_OP_SUCCESS = 0,	
  VAL_CHV_NOT_INITIALIZE, /* no CHV initialized */
  VAL_CHV_OP_FAILURE_AND_PERMIT_ATTEMPT, /*  unsuccessful CHV/UNBLOCK CHV verification, at least one attempt left */
  VAL_CHV_OP_CONTRADICTION_WITH_CHV_STATUS, /* in contradiction with CHV status */
  VAL_CHV_OP_CONTRADICTION_WITH_INVALIDATION_STATE, /* in contradiction with invalidation status */
  VAL_CHV_OP_FAILURE_AND_NO_ATTEMPT, /*unsuccessful CHV/UNBLOCK CHV verification, no attempt left;CHV/UNBLOCK CHV blocked*/
  VAL_CHV_OP_FAILURE /*failure caused by other causes */
} ValChvOpResultT;

#ifdef CHV_DEBUG
/* test message */
typedef PACKED_PREFIX struct
{
  uint8 ChvId; /* Specify the CHV */
  uint8 ChvLen;
  uint8 ChvVal[8]; /* CHV value */	
} PACKED_POSTFIX  ValChvTstReqVerifyMsgT;
/* change CHV  msg */
typedef PACKED_PREFIX struct 
{
  uint8 ChvId;       /* Specify the CHV */
  uint8 OldChvLen;
  uint8 OldChvVal[8];/* Old CHV value */
  uint8 NewChvLen;
  uint8 NewChvVal[8];/* New CHV value */
} PACKED_POSTFIX  ValChvTstReqChangeMsgT;

/* Disable CHV Msg */
typedef PACKED_PREFIX struct 
{
  uint8 ChvLen;
  uint8 Chv1Val[8];	/* CHV1 value */
} PACKED_POSTFIX  ValChvTstReqDisableMsgT;

/* Enable CHV Msg */
typedef PACKED_PREFIX struct 
{
  uint8 ChvLen;
  uint8 Chv1Val[8]; /* CHV1 value */
} PACKED_POSTFIX  ValChvTstReqEnableMsgT;

/* Unblock CHV Msg */
typedef PACKED_PREFIX struct 
{
  uint8 ChvId;        /* Specify the CHV */
  uint8 UblkChvLen;
  uint8 UblkChvVal[8];/* Unblock CHV value */
  uint8 NewChvLen;
  uint8 NewChvVal[8]; /* New CHV value */
} PACKED_POSTFIX  ValChvTstReqUnblockMsgT;

#endif /* CHV_DEBUG */

/*********UIM Short Message Service Parameters structure***********/
typedef struct
{
    uint8 RecLen;
    uint8 RecData[127];
}ValUimSmsParamRecT;

/******************** UIM MDN structures***************************/
/* Val MDN buffer structure(MDN is BCD type) */
typedef struct
{
  uint8 MdnLen;
  uint8 Mdn[8];
} ValUimMdnT;

/* Val MDN structure(MDN is char type) */
typedef struct
{
  uint8 MdnLen;
  uint8 Mdn[NAM_MAX_MDN_DIGITS];
} ValUimMdnRecT;

/****** Val Uim notify register event *****/
typedef enum
{
  VAL_UIM_EVENT_NOTIFY_REGISTER 
} ValUimEventIdT;

typedef PACKED_PREFIX struct
{
  UiccCardStatusType Status;
} PACKED_POSTFIX  ValUimNotifyMsgT;

/* ETS Test Messages */
typedef PACKED_PREFIX struct
{
  uint8 nIndex;
} PACKED_POSTFIX  ValUimGenericMsgT;

typedef PACKED_PREFIX struct
{
  uint8 nIndex;
  uint8 Name[MAXALPHALENGTH];
  uint8 PhNum[21];
} PACKED_POSTFIX  ValUimUpdatePhbRecMsgT;

typedef PACKED_PREFIX struct
{
  uint8 nIndex;
  uint8 nStatus;
  uint8 MsgData[255];
  uint8 MsgLen;
} PACKED_POSTFIX  ValUimUpdateSmsRecMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
} PACKED_POSTFIX  ValChvMsgT;

typedef PACKED_PREFIX struct
{
  bool status;
} PACKED_POSTFIX  ValChvStatusMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
  uint8      ChvId;
  uint8      ChvLen;
  uint8      Chv[8];
} PACKED_POSTFIX  ValChvVerifyMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
  uint8      ChvId;
  uint8      OldChvLen;
  uint8      OldChv[8];
  uint8      NewChvLen;
  uint8      NewChv[8];
} PACKED_POSTFIX  ValChvChangeMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
  uint8      Chv1Len;
  uint8      Chv1[8];
} PACKED_POSTFIX  ValChvEnableDisableMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
  uint8      ChvId;
  uint8      UnblockChvLen;
  uint8      UnblockChv[8];
  uint8      NewChvLen;
  uint8      NewChv[8];
} PACKED_POSTFIX  ValChvUnblockMsgT;

typedef PACKED_PREFIX struct
{
  ExeRspMsgT RspInfo;
} PACKED_POSTFIX  ValUimGetMaxMdnRecsMsgT;

typedef PACKED_PREFIX struct
{
  uint8 numRecs;
} PACKED_POSTFIX  ValUimNumRecsMsgT;

#ifndef __CARRIER_RESTRICTION__
typedef PACKED_PREFIX struct
{
  uint8           Encoding;
  uint8           Language;
  uint16          StringLen;
  char            String[32];
} PACKED_POSTFIX  SPNameT;
#endif

typedef struct
{
  SPNameT SPName;
  bool    IsValidData;
} ValSPNameT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
    UTK_NO_TIMER_STATUS = 0,
    UTK_TIMER_RUNNING,
    UTK_TIMER_NOT_RUNNING,
    UTK_TIMER_STOPPED,
    UTK_TIMER_TIMED_OUT
} UtkTimerStatusT;

typedef struct
{
  uint32          UtkTimerValue;
  UtkTimerStatusT UtkTimerStatus;
  uint32          StartTime;
}UtkTimerManagerT;
#endif


/*get gsm imsi data*/
#define VAL_UIM_GSM_IMSI_LEN 9

typedef struct
{
  ExeRspMsgT RspMsg;
  bool ImsiValid;
  uint8 ImsiData[VAL_UIM_GSM_IMSI_LEN];
}ValUimGsmImsiRspMsgT;

typedef enum
{
  TYPE_RAND = 0,
  TYPE_RANDU =1
}ValRandTypeT;

typedef enum
{
   VAL_UIM_NT_UNKNOWN        = 0,
   VAL_UIM_NT_INTERNATIONAL,
   VAL_UIM_NT_NATIONAL,
   VAL_UIM_NT_NETWORK_SPECIFIC,
   VAL_UIM_NT_SUBSCRIBER,
   VAL_UIM_NT_RESERVED,
   VAL_UIM_NT_ABBREVIATED    = 6
} ValUimNumberTypeT;

typedef enum
{
   VAL_UIM_NP_UNKNOWN = 0,
   VAL_UIM_NP_ISDN_TELEPHONY,
   VAL_UIM_NP_RESERVED2,
   VAL_UIM_NP_DATA,
   VAL_UIM_NP_TELEX,
   VAL_UIM_NP_RESERVED5,
   VAL_UIM_NP_RESERVED6,
   VAL_UIM_NP_RESERVED7,
   VAL_UIM_NP_RESERVED8,
   VAL_UIM_NP_PRIVATE = 9
} ValUimNumberPlanT;

typedef enum
{
  VAL_UIM_READ_BINARY = 0xB0,
  VAL_UIM_READ_RECORD = 0xB2,
  VAL_UIM_UPDATE_BINARY = 0xD6,
  VAL_UIM_UPDATE_RECORD = 0xDC,
  VAL_UIM_STORE_ESN = 0xDE,
  VAL_UIM_STATUS = 0xF2,
  VAL_UIM_ENVELOPE = 0xC2,
  VAL_UIM_TERM_PROFILE = 0x10,
  VAL_UIM_TERM_RESPONSE = 0x14,
  VAL_UIM_SELECT_FILE = 0xA4,
  VAL_UIM_GET_RESPONSE = 0xC0
  /* Reserved */
}ValUimCmdT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
  VAL_UIM_BT_CONNECT = 0,
  VAL_UIM_BT_DISCONNECT,
  VAL_UIM_BT_POWER_ON,
  VAL_UIM_BT_POWER_OFF,
  VAL_UIM_BT_RESET,
  VAL_UIM_BT_APDU,
  VAL_UIM_BT_INVALID_CMD
}ValBTCmdT;
#endif

typedef PACKED_PREFIX struct 
{
    bool            Ack; /* TRUE: Operation is successful, FALSE: File doesn't exist or operation is error */
    uint8           SDNRecCount;		
} PACKED_POSTFIX  ValUimSDNParamsT;

typedef PACKED_PREFIX  struct 
{
    bool            Ack; /* TRUE: Operation is successful, FALSE: File doesn't exist or operation is error */
    bool            IsFree;  
    uint8           RecordIndex;
    uint8           AlphaIdentifier[VAL_PHB_MAX_ALPHA_LENGTH];
    uint8           PhoneNumber[VAL_PHB_MAX_PHONE_LENGTH];
    uint8           TON;
    uint8           NPI;
} PACKED_POSTFIX  ValUimSDNRecT;

typedef PACKED_PREFIX struct
{
	uint8 DisCond;   /*display condiction*/
	uint8 Encoding;   /* character encoding*/
	uint8 LangInd;    /*language indication */
	uint8 Name[32];
} PACKED_POSTFIX ValUimProviderName;

#define VAL_UIM_MAX_ADDRESS_NUM 20
/* Address (8.1) */
typedef struct
{
  ValUimNumberPlanT Npi;
  ValUimNumberTypeT Ton;
  uint8             Len;
  uint8             Number[VAL_UIM_MAX_ADDRESS_NUM];
} ValUimAddressT;

typedef enum
{
  VAL_UIM_SETUP_CALL_ONIDLE,  /* set up call, but only if not currently busy on another call */
  VAL_UIM_SETUP_CALL_ONIDLE_REDIALABLE, /*set up call, but only if not currently busy on another call, with redial*/
  VAL_UIM_SETUP_CALL_HOLDABLE, /*set up call, putting all other calls (if any) on hold */
  VAL_UIM_SETUP_CALL_HOLDABLE_REDIALABLE, /*set up call, putting all other calls (if any) on hold, with redial*/
  VAL_UIM_SETUP_CALL_DISCONNECTABLE, /*set up call, disconnecting all other calls (if any)*/
  VAL_UIM_SETUP_CALL_DISCONNECTABLE_REDIALABLE /*set up call, disconnecting all other calls (if any), with redial*/ 
} ValUimCallTypeT;

typedef struct
{
   ValUimCallTypeT CallType;
   ValUimAddressT  Address;
   uint32          Duration;  /* ms */
}ValUimSetUpCallT;

typedef enum
{
   VAL_UIM_CMD_RST_OK            = 0x00, /*Command performed successfully*/
   VAL_UIM_CMD_RST_ME_UNABLE     = 0x20, /*terminal currently unable to process command*/
   VAL_UIM_CMD_RST_NET_UNABLE,               /*Network currently unable to process command*/
   VAL_UIM_CMD_RST_USER_NOT_ACCEPT,          /*User did not accept the proactive command*/
   VAL_UIM_CMD_RST_USER_CLEAR_CALL,          /*User cleared down call before connection or network release*/
   VAL_UIM_CMD_RST_CMD_BYD_CAP       = 0x30, /*Command beyond terminal's capabilities*/
   VAL_UIM_CMD_RST_CMD_TYPE_NOT_UND,         /*Command type not understood by terminal*/
   VAL_UIM_CMD_RST_CMD_DATA_NOT_UND,         /*Command data not understood by terminal*/
   VAL_UIM_CMD_RST_CMD_NUM_UNK,              /*Command number not known by terminal*/
   VAL_UIM_CMD_RST_REQ_VAL_MISS      = 0x36, /*Error, required values are missing*/
}ValUimCmdGenRstT;

typedef enum
{
  VAL_UIM_CMD_ADD_INF_NO_CAUSE       = 0x00, /*No specific cause can be given*/
  VAL_UIM_CMD_ADD_INF_SCR_BUSY,              /*Screen is busy*/
  VAL_UIM_CMD_ADD_INF_BUSY_CALL,             /*terminal currently busy on call*/
  VAL_UIM_CMD_ADD_INF_NO_SERVICE     = 0x04, /*No service*/
  VAL_UIM_CMD_ADD_INF_ACC_CLASS_BAR  = 0x05, /*Access control class bar*/
  VAL_UIM_CMD_ADD_INF_RADIO_RES_NOT_GRANT=0x06,/*Radio resource not granted*/
  VAL_UIM_CMD_ADD_INF_NOT_SPEECH_CALL,       /*Not in speech call*/
} ValUimCmdAddInfoT;

#ifdef __CARRIER_RESTRICTION__
/*carrier match type*/
typedef enum {
    VAL_UML_CAT_NET,        /* restrict by mcc/mnc */
    VAL_UML_CAT_SPN,        /* mcc/mnc and SPN */
    VAL_UML_CAT_IMSI_PREFIX,    /* mcc/mnc and 67 digits of IMSI */
    VAL_UML_CAT_GID1,       /* mcc/mnc and GID1 */
    VAL_UML_CAT_GID2,       /* mcc/mnc and GID2 */
    VAL_UML_CAT_SIZE
} ValUmlCatT;

typedef enum{
    VAL_UML_NONE,           /* Default state:not received pin, or no need to check uml */
    VAL_UML_PASSED,         /* checked and passed */
    VAL_UML_BUSY,           /* request UIM/UML data and checking */
    VAL_UML_RESTRICTED,     /* restricted in black or not in white */
    VAL_UML_REBOOT          /* locked->unlocked or unlocked->locked need AP to reset card */
} ValUmlStatusT;

typedef enum{
    VAL_UML_MD1_STATUS_NOT_RECEIVED,    /* not received */
    VAL_UML_MD1_STATUS_READY,           /* checked and passed */
    VAL_UML_MD1_STATUS_RESTRICTED,      /* restricted in black or not in white */
    VAL_UML_MD1_STATUS_REBOOT           /* locked->unlocked or unlocked->locked need AP to reset card */
} ValUmlMd1StatusT;

typedef enum{
    VAL_UML_STATUS_SYNC_READY,              /* card ready */
    VAL_UML_STATUS_SYNC_CARD_RESTRICTED,    /* card locked */
    VAL_UML_STATUS_SYNC_CARD_REBOOT         /* status changed */
} ValUmlStatusSyncT;

typedef struct
{
    bool        is_stored;  /* has received from MD1 */
    ValUmlCategoryListT   black_list;
    ValUmlCategoryListT   white_list;
    uint8   allow_all;  /* 0 - Not allow all, 1 - Allow all sim*/
    uint8   carrier_rest_state; /* 0 - Enabled 1 - Disabled */
    uint8   multi_sim_policy;
    uint8   allowed_carriers_prioritized;
}ValUmlContextT;    /* 393 bytes */

typedef struct
{
    bool        received;     /* has read from uim */
    UimUmlDataT UimUmlData;     /* uim data record for uml */
}ValUimUmlDataT;    /* 393 bytes */

typedef struct
{
    bool            checkByMd3;       /* UIM card need check by MD3. If UIM's uml data is received, then MD3 checked. */    
    bool            sentCrrstStatusToMD1;  /* has sent the status to MD1 */
    ValUmlStatusT   ValUmlCurStatus;  /* current status */
    ValUmlStatusT   ValUmlMd3Status;  /* the resule checked by MD3 */
    ValUmlMd1StatusT    ValRecUmlMd1Status[2];     /* status received from MD1, [0] : slot1 [1] : slot2  */
}ValUmlResultsT;
#endif

/*****************************************************************************

  FUNCTION NAME: ValUimInit

  DESCRIPTION:

    Create ValUimEvtLock. 

  PARAMETERS:

    None.

  RETURNED VALUES:

    None

*****************************************************************************/
void ValUimInit( void );

/********************************************************************************
 
  FUNCTION NAME: ValUimMdnInit 

  DESCRIPTION:

    This routine initiates the procedure of reading MDN parameters of the UIM. 
    
  PARAMETERS:
  
    None.
    
  RETURNED VALUES:

    None.

*********************************************************************************/
void ValUimMdnInit( void );

/********************************************************************************
 
  FUNCTION NAME: ValUimGetMaxMdnRecNum 

  DESCRIPTION:

    It's invoked to Get the number of EF MDN record in UIM card.
    
  PARAMETERS:

    None.
    
  RETURNED VALUES:

    Num: the number of MDN record.

*********************************************************************************/
uint8 ValUimGetMaxMdnRecNum( void );

/********************************************************************************
 
  FUNCTION NAME: ValUimGetMdnRec 

  DESCRIPTION:

    It's invoked to Get MDN record.
    
  PARAMETERS:

    Index:      Record index, start from 1.
    MdnRecP:    Returned Mdn pointer.
    
  RETURNED VALUES:

    TRUE: this operation is successful. 
    FALSE: failure.

*********************************************************************************/
bool ValUimGetMdnRec( uint8          Index,
                      ValUimMdnRecT* MdnRecP );

/********************************************************************************
 
  FUNCTION NAME: ValUimUpdateMdnRec 

  DESCRIPTION:

    This routine sends Update Record(6F44) message to the UIM task.
  PARAMETERS:
  
    Index: Record index, start from 1.
    Len: MDN length
    MdnP: Point to MDN buffer.
   
  RETURNED VALUES:

    TRUE: this operation is successful. 
    FALSE: failure.

*********************************************************************************/
bool ValUimUpdateMdnRec( uint8  Index,
                         uint8  Len,
                         uint8* MdnP );

/*********************************************************************************
 
  FUNCTION NAME: ValUimDelMdnRec 

  DESCRIPTION:

    This routine sends Update Record(6F44) message to the UIM task.
    
  PARAMETERS:
  
    Index: Record index, start from 1.
   
  RETURNED VALUES:

    TRUE: this operation is successful. 
    FALSE: failure.

**********************************************************************************/
bool ValUimDelMdnRec( uint8 Index );

/*****************************************************************************

  FUNCTION NAME: ValUimMdnDeliverMsg

  DESCRIPTION:

    Deliver UIM MDN response messages.  It is used by valtask.

  PARAMETERS:

    MsgId: received message id   
    MsgP: message
    MsgSize: message size	

  RETURNED VALUES:

    None

*****************************************************************************/
void ValUimMdnDeliverMsg( uint32 MsgId,
                          void*  MsgP,
                          uint32 MsgSize );

/*****************************************************************************
 
  FUNCTION NAME: ValChvGetStatus 

  DESCRIPTION:

    This routine sends UIM Get Status message to the UIM task, and saves callback 
    function's pointer in order to process CHV state when the response from the
    UIM is received.
    
  PARAMETERS:

    ChvCallbackP: Pointer to callback function

  RETURNED VALUES:

    TRUE: Get Status Msg has been sent.
    FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValChvGetStatus( void(*ChvCallbackP)(ValChvStatusT) );

#ifdef MTK_DEV_C2K_IRAT
/*****************************************************************************
 
  FUNCTION NAME: ValLocalChvVerify 

  DESCRIPTION:

    This routine sends  verify CHV  message to the UIM task
    
  PARAMETERS:
  
    ChvId: CHV1 or CHV2

  RETURNED VALUES:

            none
            
*****************************************************************************/
void ValLocalChvVerify( ValChvIdT ChvId);
#endif
/*****************************************************************************
 
  FUNCTION NAME: ValChvVerify 

  DESCRIPTION:

    This routine sends App verify CHV  message to the UIM task, and saves callback 
    function's pointer in order to process the result when the response from the 
    UIM is received.
    
  PARAMETERS:
  
    ChvId: CHV1 or CHV2
    ChvLen: Length of CHV value
    ChvP: Pointer to CHV value 
    ChvCallbackP: Pointer to callback function

  RETURNED VALUES:

    TRUE: Uim App Verify Msg has been sent.
    FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValChvVerify( ValChvIdT ChvId,
                   uint8     ChvLen,
                   uint8*    ChvP,
                   void      (*ChvCallbackP)(ValChvOpResultT) );

/*****************************************************************************
 
  FUNCTION NAME: ValChvChange 

  DESCRIPTION:

    This routine sends App change CHV  message to the UIM task, and saves callback
    function's pointer in order to process the result when the response from the 
    UIM is received.
    
  PARAMETERS:
  
    ChvId: CHV1 or CHV2
    OldChvLen: Length of old CHV value
    OldChvP: Pointer to old CHV value 
    NewChvLen: Length of new CHV value
    NewChvP: Pointer to new CHV value
    ChvCallbackP: Pointer to callback function

  RETURNED VALUES:

    TRUE: Uim App change Chv Msg has been sent.
    FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValChvChange( ValChvIdT ChvId,
                   uint8     OldChvLen,
                   uint8*    OldChvP,
                   uint8     NewChvLen,
                   uint8*    NewChvP,
                   void      (*ChvCallbackP)(ValChvOpResultT) );

/*****************************************************************************
 
  FUNCTION NAME: ValChvDisable 

  DESCRIPTION:

    This routine sends App disable CHV  message to the UIM task, and saves callback 
    function's pointer in order to process the result when the response from 
    the UIM is received.
    
  PARAMETERS:
  
    Chv1Len: Length of CHV1 value
    Chv1P: Pointer to CHV1 value 
    ChvCallbackP: Pointer to callback function

  RETURNED VALUES:

    TRUE: Uim App Disable Chv Msg has been sent.
    FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValChvDisable( uint8  Chv1Len,
                    uint8* Chv1P,
                    void   (*ChvCallbackP)(ValChvOpResultT) );

/*****************************************************************************
 
  FUNCTION NAME: ValChvEnable 

  DESCRIPTION:

    This routine sends App enable CHV message to the UIM task, and saves callback 
    function's pointer in order to process the result when the response from
    the UIM is received.
    
  PARAMETERS:
  
    Chv1Len: Length of CHV1 value
    Chv1P: Pointer to CHV1 value 
    ChvCallbackP: Pointer to callback function

  RETURNED VALUES:

    TRUE: Uim App Enable Chv Msg has been sent.
    FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValChvEnable( uint8  Chv1Len,
                   uint8* Chv1P,
                   void   (*ChvCallbackP)(ValChvOpResultT) );

/*****************************************************************************
 
  FUNCTION NAME: ChvUnblock 

  DESCRIPTION:

    This routine sends App unblock CHV  message to the UIM task, and saves callback
    function's pointer in order to process the result when the response from
    the UIM is received.
    
  PARAMETERS:
  
    ChvId: CHV1 or CHV2
    UnblockChvLen: Length of UNBLOCK CHV value
    UnblockChvP: Pointer to UNBLOCK CHV value 
    NewChvLen: Length of new CHV value
    NewChvP: Pointer to new CHV value
    ChvCallbackP: Pointer to callback function

  RETURNED VALUES:

    TRUE: Uim App Unblock Chv Msg has been sent.
    FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValChvUnblock( ValChvIdT ChvId,
                    uint8     UnblockChvLen,
                    uint8*    UnblockChvP, 
                    uint8     NewChvLen,
                    uint8*    NewChvP,
                    void      (*ChvCallbackP)(ValChvOpResultT) );

/********************************************************************************
  FUNCTION NAME: ValUimSPNameInit 
  DESCRIPTION:
    This routine initiates the procedure of reading 
    service provider name parameters of the UIM. 
  PARAMETERS:
    UimCallbackP: Pointer to callback function
  RETURNED VALUES:
    TRUE: Uim read sp name Msg has been sent.
    FALSE: error. callback=NULL or other task is doing this operation. 
*********************************************************************************/
bool ValUimSPNameInit(void (*UimCallbackP)(void *));
/*****************************************************************************

  FUNCTION NAME: ValChvDeliverMsg

  DESCRIPTION:

    Deliver CHV response message.  It is used by valtask.

  PARAMETERS:

    MsgId: received message id   
    MsgP: message
    MsgSize: message size	

  RETURNED VALUES:

    None

*****************************************************************************/
void ValChvDeliverMsg( uint32 MsgId,
                       void*  MsgP,
                       uint32 MsgSize );

void ValUimDeliverMsg( uint32 MsgId,
                       void*  MsgP,
                       uint32 MsgSize );

#ifdef __CARRIER_RESTRICTION__
void ValUmlDeliverMsg( uint32 MsgId,
                       void*  MsgP,
                       uint32 MsgSize );
#endif

/*****************************************************************************

  FUNCTION NAME: ValUimNotifyInit

  DESCRIPTION:

    Create ValUimNotifyEventLock.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None

*****************************************************************************/
void ValUimNotifyInit( void );

/*****************************************************************************

  FUNCTION NAME: ValUimRegister

  DESCRIPTION:

    Other task invokes this function to register UIM  message functions
    in Val task.

  PARAMETERS:
    EventFunc: Callback function
	
  RETURNED VALUES:

    TRegister ID
    -1: failed.

*****************************************************************************/
RegIdT ValUimRegister( ValEventFunc EventFunc );

/*****************************************************************************

  FUNCTION NAME: ValUimUnregister

  DESCRIPTION:

    Other task invokes this function to unregister UIM Notify Register message functions

  PARAMETERS:
    RegId: Register ID
	
  RETURNED VALUES:

    None.
    
*****************************************************************************/
void ValUimUnregister( RegIdT RegId );

/* Val Uim Phb */

void ValUimGetPhbRecParams( void );
void ValUimGetPhbRecord( uint8 nIndex );
void ValUimUpdatePhbRecord( uint8  nIndex,
                            uint8* pName,
#ifdef MTK_CBP
                            uint8 name_len,
#endif                                                               
                            uint8* pPhNum,
                            ValUimNumberTypeT TON,
                            ValUimNumberPlanT NPI,
                            bool isAtHcpbw);
void ValUimErasePhbRecord( uint8 nIndex , bool isAtCpbw);

/* Val Uim Sms */
void ValUimGetSmsRecParams( void );
void ValUimGetSmsRecord( uint8 nIndex );
void ValUimUpdateSmsRecord( uint8  nIndex,
                            uint8  nStatus,
                            uint8* pMsgData,
                            uint8  MsgLen );
void ValUimEraseSmsRecord( uint8 nIndex );
bool ValGetPrefLang(UINT8 *pLang, UINT8 *pEncoding);
//don't modify following Function, brew has referred to it
bool ValGetUIMId(UINT32 *pId);

/********************************************************************************
 
  FUNCTION NAME: ValUimSmsVPInit 

  DESCRIPTION:

    This routine initiates the procedure of reading Sms parameters of the UIM. 
    
  PARAMETERS:
  
    None.
    
  RETURNED VALUES:

    None.

*********************************************************************************/
void ValUimSmsVPInit(void);


/********************************************************************************
 
  FUNCTION NAME: ValUimGetSmsVPRecNumProcess 

  DESCRIPTION:

    This routine processes VAL_UIM_GET_SMSVP_REC_NUM_MSG response message. 
    
  PARAMETERS:
  
    RspMsgP:  VAL_UIM_GET_SMSVP_REC_NUM_MSG response message structure pointer.
    
  RETURNED VALUES:

    None.
    
*********************************************************************************/
void ValUimGetSmsVPRecNumProcess(UimRspMsgT* RspMsgP);

/********************************************************************************
 
  FUNCTION NAME: ValUimGetMaxSmsVPRecNum 

  DESCRIPTION:

    It's invoked to Get the number of SMS PARAMETER record in UIM card.
    
  PARAMETERS:

    None.
    
  RETURNED VALUES:

    Num: the number of SMS PARAMETER record.

*********************************************************************************/
uint8 ValUimGetMaxSmsVPRecNum(void);



/********************************************************************************
 
  FUNCTION NAME: ValUimReadSmsVPProcess 

  DESCRIPTION:

    This routine processes VAL_UIM_READ_SMSVP_REC_MSG response message. 
    
  PARAMETERS:
  
    RspMsgP:  VAL_UIM_READ_SMSVP_REC_MSG response message structure pointer.
    
  RETURNED VALUES:

    None.
    
*********************************************************************************/
void ValUimReadSmsVPProcess(UimRspMsgT* RspMsgP);


/********************************************************************************
 
  FUNCTION NAME: ValUimUpdateSMSVPProcess 

  DESCRIPTION:

    This routine processes VAL_UIM_UPDATE_SMSVP_REC_MSG response message. 
    
  PARAMETERS:
  
    RspMsgP:  VAL_UIM_UPDATE_SMSVP_REC_MSG response message structure pointer.
    
  RETURNED VALUES:

    None.
    
*********************************************************************************/
void ValUimUpdateSMSVPProcess(UimRspMsgT* RspMsgP);


/********************************************************************************
 
  FUNCTION NAME: ValUimReadSmsVPRec 

  DESCRIPTION:

    This routine sends Read Record(6F3D) message to the UIM task.
    
  PARAMETERS:
  
    index: Record index, start from 1.

  Result:
    this Read Record operation is successfully deliveried to UIM driver. 

*********************************************************************************/
void ValUimReadSmsVPRec(uint8 index);


/********************************************************************************
 
  FUNCTION NAME: ValUimUpdateSmsVPRec 

  DESCRIPTION:

    This routine sends Update Record(6F3D) message to the UIM task.
    
  PARAMETERS:
  
    index: Record index, start from 1.
    vp: Validity period inputed by client
   
  RETURNED VALUES:

    TRUE: this operation is successful to delivery to UIM driver. 
    FALSE: index is invalide parameter.

*********************************************************************************/
bool ValUimUpdateSmsVPRec(uint8 index,uint8 vp);

//don't modify following Function, brew has referred to it
/********************************************************************************
 
  FUNCTION NAME: ValUimGetMdnRec 

  DESCRIPTION:

    It's invoked to SMS Validity Period .
    
  PARAMETERS:

    index:      Record index, start from 1.
    ptvp:       point to the address that store the Validity Period.
    
  RETURNED VALUES:

    TRUE: this operation is successful. 
    FALSE: failure.

*********************************************************************************/
bool ValUimGetSmsVPRec(uint8 index, uint8* ptvp);

/********************************************************************************
 
  FUNCTION NAME: ValUimPollingReq 

  DESCRIPTION:

    To request Polling interval or Poll off function in uim
    
  PARAMETERS:

    Interval:  Timer interval to send STATUS command for UIM. Interval=0, means Poll off.
    
  RETURNED VALUES:

    None.
    
*********************************************************************************/
void ValUimPollingReq(uint16 Interval);

/********************************************************************************
 
  FUNCTION NAME: ValUimSpecCmd 

  DESCRIPTION:

    It's invoked to send UIM specific command to UIM card, for command which needs to select 
    file id, this command just selects the file in the current DF. 
    .
    
  PARAMETERS:

    Cmd:      Specific Cmd Id.
    FileId:   File ID
    P1:       ref to 1111-810 spec.
    P2:       ref to 1111-810 spec
    P3:       ref to 1111-810 spec
    DataP:    pointer to the data to be send to uim.
    RspInfo:  Response message info.
    Dfid:     DF identifier
    sessionId: UICC session identifier
  RETURNED VALUES:

    TRUE: this operation is successful. 
    FALSE: failure.

*********************************************************************************/
bool ValUimSpecCmd(ValUimCmdT Cmd, uint16 FileId, uint8 P1, 
    uint8 P2, uint8 P3, uint8 *DataP, ExeRspMsgT* RspInfo, uint8 Dfid, uint8 sessionId);

/********************************************************************************
 
  FUNCTION NAME: ValUimRawCmd 

  DESCRIPTION:

    It's invoked to send UIM raw command to UIM card.
    
  PARAMETERS:

    len:      Raw command APDU length.
    CmdApdu:  pointer to command APDU.
    RspInfo:  Response message info.
    
  RETURNED VALUES:

    TRUE: this operation is successful. 
    FALSE: failure.

*********************************************************************************/
bool ValUimRawCmd(uint16 Len, uint8 *CmdApdu,  ExeRspMsgT* RspInfoP);


/********************************************************************************
 
  FUNCTION NAME: ValUimRunCave 

  DESCRIPTION:

    It's invoked to send RUN CAVE command to UIM card.
    
  PARAMETERS:

    rand:      rand value.
    type:       RAND or RANDU.
    digitLen:   digits length.
    digit:      digits, should be digit[6]
    saveRegister: save registers or not.
    esn:        UimId(if UIM_ID is used for auth.),  otherwise, If ME is 
                assigned with MEID,Pseudo-ESN value shall be used. if it's ESN ME 
                and esn is used for auth, esn should be used.
    RspInfo:    response info.
  RETURNED VALUES:
    None.

*********************************************************************************/
void ValUimRunCave(uint32 rand, ValRandTypeT type, uint8 digitLen, uint8 *digit, 
              bool saveRegister, uint32 esn, ExeRspMsgT rspInfo);

/********************************************************************************
 
  FUNCTION NAME: ValUimGenKeyVPM 

  DESCRIPTION:

    It's invoked to send Generate Key/VPM command to UIM card.
    
  PARAMETERS:

    cmeaKey:    the pointer to cemakey[8] array
    vpm:       the pointer to VPM[65] array.
  RETURNED VALUES:
    None.

    Notes: When Generate Key/VPM command is finished, cmeaKey and vpm will be written
           by UIM task.

*********************************************************************************/
void ValUimGenKeyVPM(uint8 *cmeaKey, uint8 *vpm);

/********************************************************************************
 
  FUNCTION NAME: ValUimUpdateSSD 

  DESCRIPTION:

    It's invoked to tell UIM do Update SSD operation.
    
  PARAMETERS:

    randssd:      randssd.
    esn:        UimId(if UIM_ID is used for auth.),  otherwise, If ME is 
                assigned with MEID,Pseudo-ESN value shall be used. if it's ESN ME 
                and esn is used for auth, esn should be used.
    rspInfo:    response info.
  RETURNED VALUES:
    None.

*********************************************************************************/
void ValUimUpdateSSD(uint8 *randssd, uint32 esn, ExeRspMsgT rspInfo);

/********************************************************************************
 
  FUNCTION NAME: ValUimConfirmSSD 

  DESCRIPTION:

    It's invoked to send CONFIRM SSD command to UIM card.
    
  PARAMETERS:

    authBS:      AuthBS.
    esn:        UimId(if UIM_ID is used for auth.),  otherwise, If ME is 
                assigned with MEID,Pseudo-ESN value shall be used. if it's ESN ME 
                and esn is used for auth, esn should be used.
    rspInfo:    response info.
  RETURNED VALUES:
    None.

*********************************************************************************/
void ValUimConfirmSSD(uint32 authBS, ExeRspMsgT rspInfo);

/********************************************************************************
 
  FUNCTION NAME: ValUimMD5Auth 

  DESCRIPTION:

    It's invoked to request UIM MD5 auth.
    
  PARAMETERS:

    chapId:      chapId.
    challengeLen:     challenge length, uim supports challenge length up to 16.
    challenge:   challenge array.
    rspInfo:    response info.
  RETURNED VALUES:
    None.

*********************************************************************************/
void ValUimMD5Auth(uint8 chapId, uint8 challengeLen, uint8 *challenge, ExeRspMsgT rspInfo);

/*****************************************************************************

  FUNCTION NAME: ValUimGetSDNRecParams

  DESCRIPTION:
   To send a message to UIM to read SDN parameters. User can get SDN parameters
   in callback function. 

   PARAMETERS:

   SDNCallbackP: Pointer to callback function
    
  RETURNED VALUES:

   TRUE: message to UIM has been sent.
   FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValUimGetSDNRecParams(void (*SDNCallbackP)(ValUimSDNParamsT*));

/*****************************************************************************

  FUNCTION NAME: ValUimGetSDNRecord

  DESCRIPTION:
   To send a message to UIM to read SDN record. User can get SDN record
   in callback function. 

  PARAMETERS:
    <nIndex> the phone record index
    SDNCallbackP: Pointer to callback function
    
  RETURNED VALUES:

   TRUE: message to UIM has been sent.
   FALSE: error. callback=NULL or other task is doing this operation. 

*****************************************************************************/
bool ValUimGetSDNRecord(uint8 nIndex, void (*SDNCallbackP)(ValUimSDNRecT*));
ValUimProviderName* ValGetProviderName(void);
void ValUimReadyInit(void);
void ValUimStoreEsn(uint64 Esn, uint8 Len, bool MEAndUIMMEIDSupported);
void ValUimGetEsn(void(*GetEsnCallbackP)(uint64));
bool ValGetMEIDUIMSupported(void);
bool ValGetSFUIMIDSupported(void);
uint8 ValUimGetCst(UimECstIndexT index);

/* Used for andriod system */
/*****************************************************************************

  FUNCTION NAME: ValUimGetUtkCmdFlag

  DESCRIPTION:

    Get UTK CMD flag.

  PARAMETERS:

    None.

  RETURNED VALUES:

    bool:  TRUE: USE AT+UTKPD to init UTK.

*****************************************************************************/
bool ValUimGetUtkCmdFlag( void );
/*****************************************************************************

  FUNCTION NAME: ValUimProfileDownload

  DESCRIPTION:

    Call this function to send TERMINAL PROFILE Command to UIM task.

  PARAMETERS:

    None.

  RETURNED VALUES:

    None.

*****************************************************************************/
void ValUimProfileDownload( void );
/*****************************************************************************

  FUNCTION NAME: ValUimTerminalRespnse

  DESCRIPTION:

    Call this function to send TERMINAL RESPONSE Command to UIM task.

  PARAMETERS:

    len.   data buffer length
    data:  data buffer

  RETURNED VALUES:

    None.

*****************************************************************************/
void ValUimTerminalRespnse(uint8 len, uint8 *data);

/*****************************************************************************

  FUNCTION NAME: ValUimEnvelope

  DESCRIPTION:

    Call this function to send ENVELOPE Command to UIM task.

  PARAMETERS:

    len.   data buffer length
    data:  data buffer

  RETURNED VALUES:

    None.

*****************************************************************************/
void ValUimEnvelope(uint8 len, uint8 *data);

/*****************************************************************************

  FUNCTION NAME: ValUimConfirmCall

  DESCRIPTION:

    Call this function to confirm if Set Up Call command needs to be executed.

  PARAMETERS:

    Accept:
    ErrCause: why not accept

  RETURNED VALUES:

    None.

*****************************************************************************/
void ValUimConfirmCall( bool Accept, ValUimCmdGenRstT ErrCause);

/*****************************************************************************

  FUNCTION NAME: ValProProactCmdRsp

  DESCRIPTION:

    Call this to give respnose for proactive cmd.

  PARAMETERS:

    Result.   General result
    AddInfo:  if result is 0x20, 0x21, need fill in this field.

  RETURNED VALUES:

    None.

*****************************************************************************/
void ValProProactCmdRsp( ValUimCmdGenRstT Result,  ValUimCmdAddInfoT AddInfo);

/*****************************************************************************

  FUNCTION NAME: ValUimSmsDownload

  DESCRIPTION:

    To execute UTK SMS PP Download.

  PARAMETERS:

   ApduP : Apdu buffer
    BufferP  : Apdu length 
    TxtMessage: Sms text message buffer

  RETURNED VALUES:

    None.

*****************************************************************************/
void ValUimSmsDownload( uint8*  ApduP,  uint8 Len, ValSmsTxtRecordT* TxtMessage);

#ifdef MTK_DEV_C2K_IRAT
void ValProTimerManagerCmdRsp( ValUimCmdGenRstT Result,  ValUimCmdAddInfoT AddInfo,uint8 DataLen, uint8 *Adddata);
void valUimProvideLocationInfoProcess(void);
void ValUimUtkTimerExpiryMsgPro(ValUtkTimerExpiryMsgT *MsgData);
void ValUimUtkRefreshRspMsg(UimUtkRefreshRspMsgT *MsgData);
#ifdef MTK_DEV_CARD_HOTPLUG
void ValProcessPendingCardPowerAction(void);
#endif
/*****************************************************************************
 
  FUNCTION NAME: ValUimFileChangeCompleted

  DESCRIPTION:

    internal help function: send UIM_FILE_CHANGE_RSP_MSG to UIM task.

  PARAMETERS: ChangeSuccess
 
  RETURNED VALUES: None.
  
*****************************************************************************/
extern void ValUimFileChangeCompleted(ValUimUtkFileMaskTypeT FileMask);
/*****************************************************************************
 
  FUNCTION NAME: ValUimUtkRefreshRequest

  DESCRIPTION:

    internal help function: .

  PARAMETERS: 
 
  RETURNED VALUES: None.
  
*****************************************************************************/

extern void ValUimUtkRefreshRequest(void);
/*****************************************************************************
 
  FUNCTION NAME: ValUimUtkRefreshProcess

  DESCRIPTION:

    internal help function: handle UTK Refresh command

  PARAMETERS: ChangeSuccess
 
  RETURNED VALUES: None.
  
*****************************************************************************/
extern void ValUimUtkRefreshProcess(uint8 *BufDataP,uint8 Len, uint8 *ApIndicator);
extern bool IsPowerCycleTrigByRefresh(void);
extern void SetPowerCycleTrigByRefreshFlag(bool flag);

#ifdef FEATURE_UTK
/*****************************************************************************
 
  FUNCTION NAME: ValUimEhrpdAuthFailure

  DESCRIPTION:

  PARAMETERS: 
 
  RETURNED VALUES: None.
  
*****************************************************************************/
extern void ValUimEhrpdAuthFailure(void);
#endif
#endif
#ifdef MTK_CBP
extern void ValUimSetFdnStatus(bool ActiveStatus,uint8 ChvLen,uint8 *ChvData);
extern void ValUimUpdateFdnRecord( uint8  nIndex,
                            uint8* pName,
                            uint8 name_len,
                            uint8* pPhNum, 
                            ValUimNumberTypeT TON,
                            ValUimNumberPlanT NPI,
                            bool isAtCpbw);
extern void ValUimEraseFdnRecord( uint8 nIndex,bool isAtCpbw);
extern void ValUimGetFdnRecord(uint8 nIndex);
extern uint8 bRecvTerminalRspFromAp;
#endif

#if defined(MTK_DEV_C2K_IRAT) && defined(__CARRIER_RESTRICTION__)
bool ValUmlNeedCheckCarrier(uint8 slot_id);
ValUmlStatusT ValUmlCalFinStatus(uint8 slotId);
void ValUmlSendCarrierStatus(uint8 slot_id);
void ValUmlGetFinStatus(CardStatusT *CardStatus);
#endif

#ifdef  __cplusplus
}
#endif
#endif

/*****************************************************************************
* $Log: valuimapi.h $
* Revision 1.2  2006/02/08 16:55:11  sbenz
* moved non-global function headers to valdefs.h
* Revision 1.1  2005/11/07 14:59:40  wavis
* Initial revision
* Revision 1.1  2005/10/13 14:12:11  dorloff
* Initial revision
* Initial revision
*****************************************************************************/

/**Log information: \main\CBP7FeaturePhone\CBP7FeaturePhone_nicholaszhao_href17384\1 2011-07-08 06:09:12 GMT nicholaszhao
** HREF#17384**/
/**Log information: \main\CBP7FeaturePhone\4 2011-07-12 09:42:17 GMT marszhang
** HREF#17384**/
/**Log information: \main\2 2012-04-06 05:34:48 GMT pzhang
** modify contact uim interface**/
