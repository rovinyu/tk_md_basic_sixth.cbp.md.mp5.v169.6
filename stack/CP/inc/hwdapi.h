/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
* 
* FILE NAME   : hwdapi.h
*
* DESCRIPTION : API definition for hardware drivers.
*
* HISTORY     :
*     See Log at end of file
*
*****************************************************************************/
#ifndef _HWDAPI0_H_
#define _HWDAPI0_H_

#include "sysapi.h"
#include "exeapi.h"
#include "hwddefs.h"       /* Hardware defines                              */
#include "hwdint.h"        /* Interrupt handling RF Driver API              */

#if defined (SIM_MT6280) && defined (MTK_PLT_ON_PC_IT) && !defined (MTK_PLT_ON_PC_CODEGEN)
#include "simul_public.h"
#endif

#if ((defined(MTK_CBP)&& (!defined(MTK_PLT_ON_PC))) && (defined MTK_DEV_HW_SIM))
#include "hwdapisim.h"
#endif

/*----------------------------------------------------------------------------
 Global typedefs
----------------------------------------------------------------------------*/

typedef uint32 HwdRegT;

#ifdef MTK_PLT_ON_PC
// Include the software model
// Note: Included here as the HwdRegT type above needs to be defined.
#include "c2k_dma_model_api.h"
#endif

#endif /* _HWDAPI0_H_ */

/*
 * This section is deliberately outside of the include protection to allow the
 * register trace mode to be selected on file by by file basis.
 */

/*-----------------**
** Register Access **
**-----------------*/

#undef HwdWrite
#ifdef MTK_PLT_ON_PC
#if defined (MTK_PLT_ON_PC_IT) && defined (SIM_MT6280)
   #define HwdWrite(REG, VAL) HW_WRITE((volatile int*)REG,VAL)
#else
#ifdef MTK_DEV_HWD_HW_SIM_PC_UT
   #define HwdWrite(rEG, dATA)  printf("HwdWrite: Addr=0x%x, Data=0x%x\r\n", rEG, dATA)
#else /* MTK_DEV_HWD_HW_SIM_PC_UT */
   #define HwdWrite(Reg, Data) hwd_write((HwdRegT)(Reg), Data)
#endif /* MTK_DEV_HWD_HW_SIM_PC_UT */
#endif
#else
   #define HwdWrite(Reg, Data) \
               *((volatile HwdRegT*) (Reg)) = (Data)
#endif


#undef HwdRead
#if defined MTK_PLT_ON_PC
#if defined (MTK_PLT_ON_PC_IT) && defined (SIM_MT6280)
#define HwdRead(REG) HW_READ((volatile int*)REG)
#else
#ifdef MTK_DEV_HWD_HW_SIM_PC_UT
#define HwdRead(rEG)  (printf("HwdRead: Addr=0x%x\r\n", rEG), 0)
#else /* MTK_DEV_HWD_HW_SIM_PC_UT */
#define HwdRead(Reg) hwd_read((HwdRegT)(Reg))
#endif /* MTK_DEV_HWD_HW_SIM_PC_UT */
#endif
#else
#define HwdRead(Reg) \
            (*((volatile uint16*) (Reg)))
#endif

#undef HwdClearBit32
#ifdef MTK_PLT_ON_PC
#define HwdClearBit32( Reg, BitMask ) hwd_clear_bit(Reg, BitMask)
#else
#define	HwdClearBit32( Reg, BitMask ) \
	*( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) & ~((uint32)(BitMask))
#endif

#undef HwdClearBit16
#ifdef MTK_PLT_ON_PC
#define HwdClearBit16( Reg, BitMask ) hwd_clear_bit(Reg, BitMask)
#else
#define	HwdClearBit16( Reg, BitMask ) \
	*( (volatile uint16 *)(Reg) ) = *((volatile uint16 *)(Reg)) & ~((uint16)(BitMask))
#endif

#undef HwdClearBit8
#ifdef MTK_PLT_ON_PC
#define HwdClearBit8( Reg, BitMask ) hwd_clear_bit(Reg, BitMask)
#else
#define	HwdClearBit8( Reg, BitMask ) \
	*( (volatile uint8 *)(Reg) ) = *((volatile uint8 *)(Reg)) & ~((uint8)(BitMask))
#endif





/* 32-Bit Register MACROS - used by USB */
#undef HwdWrite32
#ifdef MTK_PLT_ON_PC
#define HwdWrite32(Reg, Data) hwd_write_32((HwdRegT)(Reg), Data)
#else
#define HwdWrite32(Reg, Data) \
	HwdWrite(Reg,Data)
#endif

#undef HwdRead32
#if defined MTK_PLT_ON_PC
#if defined (MTK_PLT_ON_PC_IT) && defined (SIM_MT6280)
#define HwdRead32(REG) HW_READ((volatile uint32*)REG)
#else
#define HwdRead32(Reg) hwd_read_32((HwdRegT)(Reg))
#endif
#else
#define HwdRead32(Reg) \
           (*((volatile uint32*) (Reg)))
#endif

#undef HwdSetBit32
#ifdef MTK_PLT_ON_PC
#define HwdSetBit32( Reg, BitMask ) hwd_set_bit_32(Reg, BitMask)
#else
#define	HwdSetBit32( Reg, BitMask )	\
	*( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) | ((uint32)(BitMask))
#endif

#undef HwdResetBit32
#ifdef MTK_PLT_ON_PC
#define HwdResetBit32( Reg, BitMask ) hwd_reset_bit_32(Reg, BitMask)
#else
#define	HwdResetBit32( Reg, BitMask ) \
	*((volatile uint32 *)(Reg)) = *((volatile uint32 *)(Reg)) & ~((uint32)(BitMask))	
#endif

/* 16-Bit Register MACROS  */
#undef HwdWrite16
#ifdef MTK_PLT_ON_PC
#define HwdWrite16(Reg, Data) hwd_write_16(Reg, Data)
#else
#define HwdWrite16(Reg, Data) \
            *((volatile uint16*) (Reg)) = (Data)
#endif

#undef HwdRead16
#ifdef MTK_PLT_ON_PC
#if defined (MTK_PLT_ON_PC_IT) && defined (SIM_MT6280)
#define HwdRead16(rEG) (int16)(HW_READ((volatile uint32*)rEG) & 0xffff)
#else
#define HwdRead16(rEG) hwd_read_16(rEG)
#endif
#else
#define HwdRead16(Reg) \
           (*((volatile uint16*) (Reg)))
#endif

#undef HwdSetBit16
#ifdef MTK_PLT_ON_PC
#define HwdSetBit16( Reg, BitMask ) hwd_set_bit_16(Reg, BitMask)
#else
#define	HwdSetBit16( Reg, BitMask )	\
           *( (volatile uint16 *)(Reg) ) = *((volatile uint16 *)(Reg)) | ((uint16)(BitMask))
#endif

#undef HwdResetBit16
#ifdef MTK_PLT_ON_PC
#define HwdResetBit16( Reg, BitMask ) hwd_reset_bit_16(Reg, BitMask)
#else
#define	HwdResetBit16( Reg, BitMask ) \
           *((volatile uint16 *)(Reg)) = *((volatile uint16 *)(Reg)) & ~((uint16)(BitMask))
#endif


/* 8-Bit Register MACROS - used by EBI */
#undef HwdWrite8
#ifdef MTK_PLT_ON_PC
#define HwdWrite8(Reg, Data) hwd_write_8(Reg, Data)
#else
#define HwdWrite8(Reg, Data) \
	*((volatile uint8*) (Reg)) = (Data)
#endif

#undef HwdRead8
#ifdef MTK_PLT_ON_PC
#define HwdRead8(Reg) hwd_read_8(Reg)
#else
#define HwdRead8(Reg) \
   (*((volatile uint8*) (Reg)))
#endif

#undef HwdSetBit8
#ifdef MTK_PLT_ON_PC
#define HwdSetBit8( Reg, BitMask ) hwd_set_bit_8(Reg, BitMask)
#else
#define	HwdSetBit8( Reg, BitMask )	\
	*( (volatile uint8 *)(Reg) ) = *((volatile uint8 *)(Reg)) | ((uint8)(BitMask))
#endif

#undef HwdResetBit8
#ifdef MTK_PLT_ON_PC
#define HwdResetBit8( Reg, BitMask ) hwd_reset_bit_8(Reg, BitMask)
#else
#define	HwdResetBit8( Reg, BitMask ) \
	*((volatile uint8 *)(Reg)) = *((volatile uint8 *)(Reg)) & ~((uint8)(BitMask))	
#endif

#if ((defined(MTK_CBP)&& (!defined(MTK_PLT_ON_PC))))
#ifdef MTK_DEV_HW_SIM
#define HwdReadSim(Type, Reg)                HwdReadRegSim((HwdRegTypeT) Type, (uint32)Reg)
#define HwdWriteSim(Type, Reg, Data)         HwdWriteRegSim((HwdRegTypeT) Type, (uint32)Reg, (uint16)Data)
#define HwdSetBitSim(Type,Reg, BitMask)      HwdSetRegBitSim((HwdRegTypeT) Type, (uint32)Reg, (uint16)BitMask)
#define HwdClearBitSim(Type, Reg, BitMask)   HwdClrRegBitSim((HwdRegTypeT) Type, (uint32)Reg, (uint16)BitMask)
#else
#define HwdReadSim(Type, Reg)
#define HwdWriteSim(Type, Reg, Data)
#define HwdSetBitSim(Type,Reg, BitMask)
#define HwdClearBitSim(Type, Reg, BitMask)
#endif

#define MtkHwdRead(Reg)                      HwdRead(Reg)
#define MtkHwdWrite(Reg, Data)               HwdWrite(Reg, Data)
#define MtkHwdSetBit(Reg, BitMask)           HwdSetBit16(Reg, BitMask)
#define MtkHwdClearBit(Reg, BitMask)         HwdClearBit16(Reg, BitMask)

#define HwdReadJade(Type, Reg)               HwdRead(Reg)
#define HwdWriteJade(Type,Reg, Data)         HwdWrite(Reg, Data)
#define HwdSetBitJade(Type,Reg, BitMask )    HwdSetBit16(Reg, BitMask)
#define HwdClearBitJade(Type,Reg, BitMask )  HwdClearBit16(Reg, BitMask)
#endif


/* multiple include protection restarts here */   
#ifndef _HWDAPI_H_
#define _HWDAPI_H_

#if defined (MTK_PLT_ON_PC) && defined (MTK_PLT_ON_PC_IT) && defined (SIM_MT6280)
void SysHwdSetBitProtect(uint32 Reg, uint32 BitMask);
/*     *( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) | (uint32)(BitMask);      */

void SysHwdClearBitProtect(uint32 Reg, uint32 BitMask);
/*     *( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) & ~((uint32)(BitMask));    */

void SysHwdSetMaskedBitProtect(uint32 Reg, uint32 BitMask, uint32 mask);
/*     *( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) & ~((uint32)(Mask)) | ((uint32)(BitMask));     */
#define  HwdSetBitProtect(Reg, BitMask)        HwdWrite(Reg, (HwdRead(Reg) | (uint32)BitMask))
#define  HwdClearBitProtect(Reg, BitMask)      HwdWrite(Reg, (HwdRead(Reg) & ~((uint32)BitMask)))
#define  HwdSetMaskedBitProtect(Reg, BitMask, Mask)  HwdWrite(Reg, (HwdRead(Reg) & ~((uint32)(Mask)) | ((uint32)(BitMask))))
#else
void SysHwdSetBitProtect(uint32 Reg, uint32 BitMask);
/*     *( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) | (uint32)(BitMask);      */

void SysHwdClearBitProtect(uint32 Reg, uint32 BitMask);
/*     *( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) & ~((uint32)(BitMask));    */

void SysHwdSetMaskedBitProtect(uint32 Reg, uint32 BitMask, uint32 mask);
/*     *( (volatile uint32 *)(Reg) ) = *((volatile uint32 *)(Reg)) & ~((uint32)(Mask)) | ((uint32)(BitMask));     */

#define  HwdSetBitProtect        SysHwdSetBitProtect
#define  HwdClearBitProtect      SysHwdClearBitProtect
#define  HwdSetMaskedBitProtect  SysHwdSetMaskedBitProtect
#endif

#define  HwdMsSerInit()   
#define  HwdMsRegisterWrite      HwdSetMaskedBitProtect
#define  HwdMsValueRead          HwdRead

extern void HwdAdvancedClkWrite(uint32 reg, uint32 bitMask, uint32 mask);


#ifndef BOOT_BUILDING
#define MASK_16BITS 0xFFFF 
#define HwdAssistMaskWrite16(Reg, BitMask, mask) HwdWrite32(Reg, (((uint32)(BitMask) << 16) | (mask)))
#else  // BOOT_BUILDING
#define MASK_16BITS 0xFFFF 
#define HwdAssistMaskWrite16 SysHwdSetMaskedBitProtect
#endif

#define HwdClkWrite HwdAssistMaskWrite16

#if (SYS_ASIC <= SA_CBP82)
extern void HwdMuxSel0Clear (uint32 RegMask);
extern void HwdMuxSel0Set (uint32 RegMask);
#endif
/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
#define HWD_MAILBOX      EXE_MAILBOX_1_ID    /* Command Mailbox Id          */
#define HWD_CAL_MAILBOX  EXE_MAILBOX_2_ID    /* Calibration Mailbox Id      */

#define HWD_CALIB_INIT_MODE_NVRAM   0     /* use calibration values from NVRAM */
#define HWD_CALIB_INIT_MODE_DEFAULT 1     /* use default coded calibration values  */
/* defines for message interface */
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_DBM_DATA_INIT_MODE_NVRAM        0 /* Use NVRAM values */
#define HWD_DBM_DATA_INIT_MODE_DEFAULT      1 /* Use default values */
#else
#define HWD_CALIB_INIT_MODE_NVRAM   0     /* use calibration values from NVRAM */
#define HWD_CALIB_INIT_MODE_DEFAULT 1     /* use default coded calibration values  */
#endif /* MTK_CBP */
#define HWD_GET_CAL_DATA_TIMEOUT    5000

#define HWD_STARTUP_SIGNAL           EXE_SIGNAL_1
#define HWD_MP3_SPI_SIGNAL           EXE_SIGNAL_2
#define HWD_CMR_CTRL_SIGNAL          EXE_SIGNAL_3

#if defined(MTK_PLT_AUDIO)
#define HWD_SPH_AUDIO_SIGNAL         EXE_SIGNAL_4
#endif // defined(MTK_PLT_AUDIO)

#define HWD_MIDI_EOF_SIGNAL          EXE_SIGNAL_5
#define HWD_SDIO_S2M_RD_RDY_SIGNAL   EXE_SIGNAL_6
#define HWD_AFC_TIMER_STOP_SIGNAL    EXE_SIGNAL_7
#define HWD_MIDI_LOAD_BUF_SIGNAL     EXE_SIGNAL_8

#define HWD_RX_FILT_BW_CAL_DONE_SIGNAL      EXE_SIGNAL_12  /* 0x00010000 */
#define HWD_RX_IQ_GAIN_CAL_DONE_SIGNAL      EXE_SIGNAL_13  /* 0x00020000 */

#define TBD 0xDEAD

#define HWD_TX_RX_FIXED_DLY  1    /* Desired delay between TX and RX under
                                   * normal operation.
                                   */
                                      
/* defines for the calibration parameter messages and calibration parameter usage */
/* note: number of regions in all cases includes one "extra" region to store the rightmost endpoint */
/* These must match the shared definitions for compatibility with the DSPM SW */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_HYST_STATES_TXAGC         SYS_MAX_NUM_HYST_STATES_TXAGC 
#else
#define HWD_NUM_HYST_STATES_TXAGC         HWD_PA_MODE_NUM 
#endif
#define HWD_NUM_HYST_STATES_RXAGC         SYS_MAX_NUM_HYST_STATES_RXAGC

#define HWD_NUM_GAIN_POINTS_TXAGC         SYS_MAX_NUM_GAIN_POINTS_TXAGC
#define HWD_NUM_GAIN_POINTS_RXAGC         SYS_MAX_NUM_GAIN_POINTS_RXAGC

#define HWD_NUM_TEMP_CALIBR_POINTS        8
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_NUM_RX_PATH                   2
#define HWD_RF_RX_GAIN_STEP_MAX_NUM       25
#endif
#define HWD_NUM_BATT_CALIBR_POINTS        8
#define HWD_NUM_BATT_PDM_CALIBR_POINTS    8

#define HWD_NUM_TEMP_POINTS_TXAGC         8
#define HWD_NUM_TEMP_POINTS_RXAGC         8
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_TEMP_POINTS_TX_LIM        8
#endif

#define HWD_NUM_FREQ_CALIBR_POINTS        16
#define HWD_NUM_FREQ_POINTS_TXAGC         16
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_FREQ_POINTS_TX_LIM        16
#endif
#define HWD_NUM_FREQ_POINTS_RXAGC         16
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_RXAGC_GAIN_STEP_NUM            25
#endif

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_BATT_POINTS_TXAGC         8
#define HWD_NUM_BATT_POINTS_TX_LIM        8
#endif

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_GAIN_POINTS_TX_PWR_DET    8
#else
#define HWD_NUM_GAIN_POINTS_TX_PWR_DET    3
#endif

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_TEMP_POINTS_TX_PWR        8
#endif

#define HWD_NUM_FREQ_POINTS_TX_PWR        16

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_MAX_BATT_POINTS_TX_PWR    8
#endif
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_MAX_NUM_DIGITAL_GAIN_STATES   8
#else
#define HWD_MAX_NUM_DIGITAL_GAIN_STATES   HWD_LNA_MODE_NUM
#endif
#define HWD_NUM_DEFAULT_GAIN_POINTS       2    /* Used for software calibration defaults */

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_SUPPORTED_PDM             5    /* CBP6.0 Supports 5 PDMs */
#endif

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
#define HWD_NUM_GAIN_STATE_RANGES		  3
#endif

/* Define HWD I2C Write msg */
#define HWD_I2C_BUF_SIZE_MAX              64

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_MD1_CLOCK_SETLE_TIME_MS      5 /* 5 ms for MD1 clock settle */
#endif


#undef DISABLE_IMD_PSAVE_OPTIMIZATION

/*----------------------------------------------------------------------------
 * Q-value definitions
----------------------------------------------------------------------------*/
/* Amplitude Log2 value * 6.0206 = 20log10(dBm) value */
#define ALOG2_TO_DBM_CONVERSION_FACTOR_Q10      0x1815

/* X(dBm) = 20 log X = (20 log2 X) / (log2 10),
 * therefore log2 X = 1 / (20 log 2) * X(dBm)
 * 1 / (20 log 2) = 0.16609640474436811... */
#define DBM_TO_ALOG2_CONVERSION_FACTOR_Q16      0x2A85

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/*----------------------------------------------------------------------------
 * Compile time macro definitions
----------------------------------------------------------------------------*/
#define COMPILE_ASSERT(cOND)    (sizeof(char[1 - 2*!(cOND)]) - 1)
#define PARAM_NOT_USED(pARAM)   ((pARAM) = (pARAM))
#endif /* MTK_CBP */


/*----------------------------------------------------------------------------
 * GPS definitions
----------------------------------------------------------------------------*/
#define HWD_GPS_PULSE_WIDTH  0x08c8  // active high polarity and equal to 2 chip

#ifdef MTK_DEV_OPTIMIZE_EVL1
#define DEF_CodeInIram(x)      ATTRIB_SECTION("CodeInIram") x
#define DEF_CODE(x)            DEF_CodeInIram(x)
#else
#define DEF_CODE(x)            x
#endif

/*----------------------------------------------------------------------------
     RF type definitions
----------------------------------------------------------------------------*/
/* All Physical layer band, A-E
 */
typedef enum
{
   HWD_RF_BAND_A = 0,
   HWD_RF_BAND_B,
   HWD_RF_BAND_C,
#if(!defined(MTK_PLT_RF_ORIONC) || defined(MTK_DEV_HW_SIM_RF))
   HWD_RF_BAND_D,
   HWD_RF_BAND_E,
#endif
   HWD_RF_BAND_MAX,//it should be updated after asher CL is submitted
   HWD_RF_BAND_UNSUPPORTED = HWD_RF_BAND_MAX,
   HWD_RF_BAND_UNDEFINED = HWD_RF_BAND_MAX
}HwdRfBandT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/**RF frequency high band and low band enum type*/
typedef enum
{
  HWD_RF_FREQ_LOW_BAND,
  HWD_RF_FREQ_HIGH_BAND,
  HWD_RF_FREQ_BAND_MAX
}HwdRfFreqBandT;
#endif

/* This structure describes the support for a particular bandclass.
   The Subclass element is a bitmap field with each bit indicating support
   for a particular subclass. The Supported element indicates in general
   support for a specific band class
*/
typedef PACKED_PREFIX struct
{
   uint32 Subclass;
   HwdRfBandT HwdRfBand;
   bool Supported;
} PACKED_POSTFIX HwdSupportedCDMABandT;

#define HWD_RF_SUB_CLASS_MAX  16

#define HWD_RF_SUB_CLASS_0_SUPPORTED 0x80000000
#define HWD_RF_SUB_CLASS_1_SUPPORTED (1<<30)
#define HWD_RF_SUB_CLASS_2_SUPPORTED (1<<29)
#define HWD_RF_SUB_CLASS_3_SUPPORTED (1<<28)
#define HWD_RF_SUB_CLASS_4_SUPPORTED (1<<27)
#define HWD_RF_SUB_CLASS_5_SUPPORTED (1<<26)
#define HWD_RF_SUB_CLASS_6_SUPPORTED (1<<25)
#define HWD_RF_SUB_CLASS_7_SUPPORTED (1<<24)
#define HWD_RF_SUB_CLASS_8_SUPPORTED (1<<23)
#define HWD_RF_SUB_CLASS_9_SUPPORTED (1<<22)
#define HWD_RF_SUB_CLASS_10_SUPPORTED (1<<21)
#define HWD_RF_SUB_CLASS_11_SUPPORTED (1<<20)
#define HWD_RF_SUB_CLASS_12_SUPPORTED (1<<19)
#define HWD_RF_SUB_CLASS_13_SUPPORTED (1<<18)
#define HWD_RF_SUB_CLASS_14_SUPPORTED (1<<17)
#define HWD_RF_SUB_CLASS_15_SUPPORTED (1<<16)

#define HWD_RF_SUB_CLASS_0_UNSUPPORTED (0<<31)
#define HWD_RF_SUB_CLASS_1_UNSUPPORTED (0<<30)
#define HWD_RF_SUB_CLASS_2_UNSUPPORTED (0<<29)
#define HWD_RF_SUB_CLASS_3_UNSUPPORTED (0<<28)
#define HWD_RF_SUB_CLASS_4_UNSUPPORTED (0<<27)
#define HWD_RF_SUB_CLASS_5_UNSUPPORTED (0<<26)
#define HWD_RF_SUB_CLASS_6_UNSUPPORTED (0<<25)
#define HWD_RF_SUB_CLASS_7_UNSUPPORTED (0<<24)
#define HWD_RF_SUB_CLASS_8_UNSUPPORTED (0<<23)
#define HWD_RF_SUB_CLASS_9_UNSUPPORTED (0<<22)
#define HWD_RF_SUB_CLASS_10_UNSUPPORTED (0<<21)
#define HWD_RF_SUB_CLASS_11_UNSUPPORTED (0<<20)
#define HWD_RF_SUB_CLASS_12_UNSUPPORTED (0<<19)
#define HWD_RF_SUB_CLASS_13_UNSUPPORTED (0<<18)
#define HWD_RF_SUB_CLASS_14_UNSUPPORTED (0<<17)
#define HWD_RF_SUB_CLASS_15_UNSUPPORTED (0<<16)

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_RF_SUB_CLASS_ALL_SUPPORTED  0xffff0000
#define HWD_RF_SUB_CLASS_NONE_SUPPORTED 0x00000000
#endif

/*----------------------------------------------------------------------------
  Global enums -  Message IDs, etc
----------------------------------------------------------------------------*/
typedef enum
{
   /*-----------------------------------------------------------------
    * NOTE: Messages, which IDs start with HWD_TST are for testing only;
    *       they shouldn't be used in normal operation !!!
    *----------------------------------------------------------------*/

   HWD_INIT_MSG,
   HWD_RESET_MSG,
   HWD_CP_DSPM_ALIVE_MSG,
   HWD_CP_DSPV_ALIVE_MSG,

   /*-----------------------------------------------------------------
    * Register & Memory Messages
    *----------------------------------------------------------------*/
   HWD_INIT_NVRAM_ACK_MSG = 0x100,
   HWD_TST_PSRAM_CTRL_MSG,
   HWD_TST_BURST_FLASH_CTRL,

   HWD_TST_GPIO_CTRL_MSG,
   HWD_TST_GPIO_FUNC_SWITCH_MSG,

   HWD_TST_SET_CG_CK_MUX_SET_MSG,

   HWD_TST_SIM_WATCHDOG_MSG,

   HWD_RF_START_CALIBRATION_MSG,
   
   /*-----------------------------------------------------------------
    * C2K speech link
    *----------------------------------------------------------------*/      
   HWD_SPH_READ_DSP_MEM = 0x150,
   HWD_SPH_LBK,
   HWD_SPH_FRC_TONE,
   
   /*-----------------------------------------------------------------
    * Power Savings Messages
    *----------------------------------------------------------------*/
   HWD_PWR_SAVINGS_MODE_CONFIG_MSG = 0x200,
   HWD_TST_PWR_SAVINGS_CONFIG_MSG,
   HWD_TST_PWR_SAVINGS_HW_STATUS_GET_MSG,
   HWD_TST_PWR_SAVINGS_GET_MODE_MSG,

   /*-----------------------------------------------------------------
    * USB ACC Messages
    *----------------------------------------------------------------*/   
   HWD_USB_CABLE_MSG = 0x250,
   HWD_USB_CABLE_CONNECTED,
   HWD_USB_CABLE_REMOVED, 
   HWD_MSC_READ_MSG,
   HWD_MSC_WRITE_MSG = 0x254,
   HWD_USB_STORAGE_ENABLE_MSG,
   HWD_USB_STORAGE_DISABLE_MSG = 0x256,
   HWD_USB_CONFIGED_MSG,

   /*Headset handler*/   
   HWD_HEADSET_MSG = 0x260,
   HWD_VIBRATOR_MSG = 0x261, 
   HWD_HEADSETKEY_MIC_BIAS_MSG,

   /*SD Card Detect MSG*/
   HWD_MMC_SD_CARD_MSG,
   
   HWD_DCBIAS_CONFIG_MSG,


   /*-----------------------------------------------------------------
    * ADC Messages
    *----------------------------------------------------------------*/
   HWD_ADC_MEAS_REQUEST_MSG = 0x300,
   HWD_ADC_MEAS_RESPONSE_MSG,
   HWD_ADC_POLL_START_MSG,
   HWD_ADC_POLL_STOP_MSG,
   HWD_TST_ADC_RF_VERSION_RESPONSE_MSG,
   HWD_TST_AUX_ADC_POLLING,

   HWD_TST_READ_CURRENT_TEMP_MSG,
   HWD_TST_READ_CURRENT_BATT_MSG,
   HWD_TST_READ_CURRENT_TX_POWER_MSG,

   HWD_VAL_READ_CURRENT_TEMP_ACK_MSG,
   HWD_VAL_READ_CURRENT_BATT_ACK_MSG,

   /*-----------------------------------------------------------------
    * PDM/PWM Messages
    *----------------------------------------------------------------*/
   HWD_UPDATE_PDM_DELTAS_MSG_UNUSED = 0x400,
   HWD_TST_PDM_CONFIG_MSG,
   HWD_TST_PDM_GET_VALUE_MSG,

   HWD_TST_PWM_CONFIG_MSG,

   HWD_TST_MXS_ABB_WRITE_MSG,
   HWD_TST_MXS_ABB_READ_MSG,

   /*-----------------------------------------------------------------
    * Camera Messages
    *----------------------------------------------------------------*/
   HWD_CMR_SS_CALLBACK_MSG = 0x500,

   HWD_TST_CMR_SETUP_MSG,
   HWD_TST_CMR_PREVIEW_MSG,
   HWD_TST_CMR_SNAPSHOT_MSG,
   HWD_TST_CMR_DORADO_REG_READ_MSG,
   HWD_TST_CMR_DORADO_REG_WRITE_MSG,
   HWD_TST_CMR_IMAGER_REG_READ_MSG,
   HWD_TST_CMR_IMAGER_REG_WRITE_MSG,
   HWD_TST_CMR_HIF_CMD_MSG,

   /*-----------------------------------------------------------------
    * GPS Messages
    *----------------------------------------------------------------*/
   HWD_GPS_GROUP_DELAY_MSG = 0x600,


   /*-----------------------------------------------------------------
    * RF Messages
    *----------------------------------------------------------------*/
   HWD_PLL_CHANNEL_SET_MSG = 0x700,

   HWD_TST_TX_ON_OFF_MSG,   
   HWD_TST_TX_ON_CONTROL_MSG,
   HWD_TST_TX_ON_READ_MSG,
   HWD_TST_TX_FLTR_TEST_MSG,
   HWD_TST_SET_TX_POWER_LIMITS,

   HWD_TST_RX_ON_OFF_MSG,
   HWD_TST_RF_ON_CONTROL_MSG,
   HWD_TST_RF_ON_READ_MSG,

   HWD_TST_RF_CHANGE_CONSTANTS,
   HWD_TST_GET_RF_CONSTANTS,
   HWD_TST_SET_RXTX_IQ_SWAP,
   HWD_TST_GET_RXTX_IQ_SWAP,

   HWD_TST_RF_GEN_WRITE_MSG,
   HWD_TST_RF_GEN_READ_MSG,
   HWD_TST_PLL_CHANNEL_CONFIG_MSG,
   HWD_TST_PLL_CHANNEL_GET_MSG,
   HWD_TST_PLL_LOCK_DETECT_TEST_MSG,
   HWD_TST_PROC_PLL_REPROGRAM_MSG,

   /* Algorithms test controls */
   HWD_TST_AFC_CONFIG_MSG,
   HWD_TST_AFC_DATA_READ_MSG,
   HWD_TST_RX_AGC_CONFIG_MSG,
   HWD_TST_TX_AGC_CONFIG_MSG,
   HWD_TST_TX_TEST_MODE_MSG,
   HWD_TST_RX_AGC_GAIN_SET_MSG,
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   MTK_HWD_TST_RX_AGC_GAIN_SET_MSG,
   MTK_HWD_TST_AFC_FOE_CONFIG_MSG,
   MTK_HWD_CP_REG_LOG_MSG,
   MTK_HWD_CP_REG_SET_MSG,
   MTK_HWD_CP_REG_SPY_MSG,
   MTK_HWD_DM_DBG_MSG,
   HWD_TST_RF_TEST_MTK_MSG,
   HWD_TST_RF_TEST_CHAN_SETTLE_MSG,
   MTK_HWD_TST_RX_AGC_GAIN_UP_THRESHOLD_MSG,
   HWD_TST_AFC_PARAM_READ_NVRAM_ACK_MSG,
   HWD_TST_AFC_PARAM_WRITE_NVRAM_ACK_MSG,
#if (SYS_BOARD >= SB_JADE)
   HWD_TST_RX_AGC_COMP_NV_READ_NVRAM_ACK_MSG,
   HWD_TST_RX_AGC_COMP_NV_WRITE_NVRAM_ACK_MSG,
   HWD_TST_TX_AGC_COMP_NV_READ_NVRAM_ACK_MSG,
   HWD_TST_TX_AGC_COMP_NV_WRITE_NVRAM_ACK_MSG,
   HWD_TST_COUPLER_LOSS_COMP_NV_READ_NVRAM_ACK_MSG,
   HWD_TST_COUPLER_LOSS_COMP_NV_WRITE_NVRAM_ACK_MSG,
#endif
#endif

    /* Diversity RF control test Handlers */
   HWD_TST_DIV_RX_ON_OFF_MSG,
   HWD_CAL_BAND_A_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_B_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_C_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_D_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_E_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_A_DIV_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_B_DIV_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_C_DIV_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_D_DIV_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_E_DIV_RX_BANK_CAL_MSG,
   HWD_RF_DRIVER_CMD,
   HWD_DATABASE_WRITE_ACK_MSG,

   /* IMD RF control Handlers, Main and DIV                          */
   HWD_CAL_BAND_A_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_A_DIV_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_B_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_B_DIV_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_C_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_C_DIV_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_D_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_D_DIV_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_E_NOIMD_RXAGC_MSG,
   HWD_CAL_BAND_E_DIV_NOIMD_RXAGC_MSG,

   HWD_RF_GET_RX_AGC_DEFAULTS,
   HWD_TXAGC_ALT_THRESHOLDS,
   HWD_GSM_CDMA_INTERFERENCE_SOLUTION_MSG,
   HWD_ACTIVE_PILOT_DATA_MSG,
   HWD_RX_AGC_GAIN_TABLE_SELECT_MSG,
   HWD_RF_GSM_COMP_MSG,
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))    
   MTK_TST_HWD_CP_REG_READ_MSG,  
   HWD_TEMP_MEAS_REQUEST_MSG,
   HWD_TEMP_MEAS_RESPONSE_MSG,
   HWD_DET_TXPWR_REPORT_MSG,
   HWD_TST_SET_SPI_MTK_MSG,
   HWD_TST_GET_SPI_MTK_MSG,
  
#ifdef MTK_DEV_LOGIQ
   HWD_TST_LOG_IQ_MTK_MSG,
#endif
   HWD_TST_GET_TEMPERATURE_ADC_MTK_MSG,
   HWD_TST_TRANSMITTER_CTRL_MTK_MSG,
   HWD_TST_RECEIVER_CTRL_MTK_MSG, 
   HWD_TST_AFC_CFG_MTK_MSG,
   HWD_TST_AFC_PARM_CTRL_MTK_MSG,
   HWD_TST_TX_TONE_CTRL_MTK_MSG,   
   HWD_TST_TX_CDMA_CTRL_MTK_MSG,
   HWD_TST_TX_AGC_CFG_MTK_MSG,
   HWD_TST_TX_POWER_GET_MTK_MSG,
   HWD_TST_RX_AGC_CFG_MTK_MSG,
   HWD_TST_RX_DIG_GAIN_START_MTK_MSG,
   HWD_TST_READ_RX_RF_DCOC_TBL_MTK_MSG,
   HWD_TST_WRITE_RX_RF_DCOC_TBL_MTK_MSG,
   HWD_TST_SET_RX_AGC_DCOC_MTK_MSG,
   HWD_TST_GET_RX_AGC_DCOC_MTK_MSG,
   HWD_TST_READ_RX_DFE_DCOC_TBL_MTK_MSG,
   HWD_TST_WRITE_RX_DFE_DCOC_TBL_MTK_MSG,
   HWD_TST_TEMPERATURE_MEASURE_CTRL_MTK_ETS,
   HWD_TST_FIX_RATI_MTK_MSG,
   HWD_TST_TX_AGC_IMMED_MODE_SET_MTK_MSG,
   HWD_TST_RF_DRIVER_VAL_ETS,
   HWD_TST_BYPASS_DELTA_GAIN_COMP_MTK_MSG,
#if (!defined (MTK_PLT_DENALI)) && (!defined(MTK_PLT_ON_PC))
   HWD_TST_START_FACTORY_POC_MSG,
   HWD_TST_FACTORY_POC_PARAM_RDY_MSG,
   HWD_TST_FACTORY_POC_DONE_MSG,
   HWD_TST_FACTORY_POC_LID_RDY_MSG,
   HWD_TST_FACTORY_POC_RESULT_WRITE_NVRAM_ACK_MSG,
#endif
#if (SYS_BOARD >= SB_JADE)
   HWD_TST_READ_CURRENT_XO_TEMPERATURE_MSG,
   HWD_TST_CONFIGURATION_INFO_JADE_MSG,
   HWD_TST_DET_TX_POWER_GET_MSG,
   HWD_TST_RX_AGC_PATHLOSSCOM_SET_MSG,
   HWD_TST_RX_AGC_PATHLOSSCOM_ADJUST_MSG,
   HWD_TST_RX_AGC_PATHLOSSCOM_RESTORE_MSG,
   HWD_TST_RX_ADC_SATURATION_MSG,
   HWD_TST_RX_AGC_FIX_MSG,
   HWD_TST_RX_AGC_COMP_TBL_CTRL_MSG,
   HWD_TST_TX_AGC_COMP_TBL_CTRL_MSG,
   HWD_TST_COUPLER_LOSS_COMP_TBL_CTRL_MSG,
   HWD_TST_TAS_CTRL_MTK_MSG,
#endif
   HWD_TST_COMPENSATION_ADJUST_MSG,
   HWD_TST_COMPENSATION_RESTORE_MSG,
   HWD_TST_TEMPERATURE_FORCE_UPDATE_MSG,
   HWD_TST_SET_TX_AGC_CL_RANGE_MSG,
#endif

#if (defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC)))
   HWD_TST_FHC_START_MTK_MSG,
#endif

#ifdef MTK_DEV_RF_TEST
   HWD_TST_GET_INITIALIZED_CAL_DATA_MTK_MSG,
#endif
   HWD_TST_DUMP_SRCH_BUFFER_IQ_MSG,
   
   /*-----------------------------------------------------------------
    * Audio Messages
    *----------------------------------------------------------------*/
   /* General Audio */
   HWD_AUDIO_PATH_CFG_RESP_MSG = 0x1000,
   HWD_AUDIO_DAC_SAMPLING_RATE_MSG,     /* Hifi DAC sampling rate sent from DV */
   HWD_AUDIO_VOL_SCAL_FCTR_RESP_MSG,
   HWD_TST_SET_MIC_VOLUME,
   HWD_TST_SET_SPKR_VOLUME,
   HWD_TST_AUDIO_OUTPUT_SEL_MSG,
   HWD_TST_AUDIO_LOOPBACK_CONFIG_MSG,
   HWD_TST_AUDIO_TONE_GEN_MSG,
   HWD_TST_AUDIO_TONE_GEN_CFG_MSG,
   HWD_TST_AUDIO_SIDETONE_MULT_MSG,
   HWD_TST_AUDIO_STEREO_CFG_MSG,
   HWD_TST_EDAI_CONFIGURE_MSG,
   HWD_TST_AUDIO_I2S_CFG_MSG,
   HWD_AUDIO_VOICE_FRM_RESYNC_RESP_MSG,
   HWD_FM_AUDIO_CLEANUP_RESP_MSG,
   HWD_AUDIO_BLUETOOTH_CONFIG_RSP_MSG,
   HWD_I2S_AUDIO_CLEANUP_RESP_MSG,
   HWD_PCM_PROCESS_MODE_RESP_MSG,
   HWD_AUDIO_SET_EXT_DAC_SAMPLE_RATE_MSG,
   HWD_AUDIO_EXT_DAC_ENABLE_MSG,

   /* MIDI RINGER Handler */
   HWD_MIDI_STATUS_MSG = 0x1100,        /* From DSPV to indicate when MIDI ringer off */
   HWD_MIDI_AUDIO_CFG_COMPLETE_MSG,     /* From DSPV after configuring audio */

   HWD_TST_MIDI_PARMS_MSG,              /* Debug message: MIDI test parms */
   HWD_TST_MIDI_CONFIG_MSG,             /* Debug message: Set Midi config */

   /* MULTIMEDIA APPLICATION Handler */
   HWD_MMAPPS_CHAN_DATA_MSG = 0x1200,   /* Application data from IPC */
   HWD_MMAPPS_MODE_RSP_MSG,             /* Application Mode resp from DSPV */
   HWD_MMAPPS_AUDIO_CFG_RESP_MSG,       /* MM-Apps audio config response from DSPV */
   HWD_MMAPPS_AUDIO_PATH_ON_MSG,        /* MM-Apps audio path ON processing is complete */
   HWD_MMAPPS_AUDIO_CLEANUP_RESP_MSG,   /* MM-Apps audio cleanup response from DSPV */
   HWD_MMAPPS_MUSIC_STOP_MSG,           /* Stop music from MMApps code */
   HWD_MMAPPS_APP_STATUS_MSG,           /* Application status from DSPV */
   HWD_MMAPPS_BUFF_RELOAD_MSG,          /* Reload music buffer from MMApps code */
   HWD_MMAPPS_FRAME_RELOAD_RESP_MSG,    /* Frame buffer reloaded by FP task */
   HWD_MMAPPS_DSPV_APP_REQ_MSG,         /* DSPv request to dnld different App */

   HWD_TST_MMAPPS_TEST_START_MSG,       /* Loopback MM-Apps test from ETS */
   HWD_TST_MMAPPS_TEST_STOP_MSG,        /* Loopback MM-apps test stop from ETS */
   HWD_TST_MMAPPS_VOCODER_DNLD_MSG,     /* MM-Apps Vocoder Download request from ETS */

   /* Tone Generator Messages */
   HWD_TONE_GEN_RESP_MSG = 0x1300, // not in use
   HWD_TONE_SSO_DISC_RCVD_MSG, // not is use

   /* External Micronas MP3 Handler */
   HWD_EXT_MP3_AUDIO_CFG_RESP_MSG = 0x1400, /* Ext MP3 audio config response from DSPV */
   HWD_EXT_MP3_AUDIO_CLEANUP_RESP_MSG,      /* Ext MP3 audio cleanup response from DSPV */
   
   /*-----------------------------------------------------------------
    * Calibration Messages
    *----------------------------------------------------------------*/
   HWD_CAL_INITIALIZE_MSG = 0x2000,

#if ((!defined MTK_CBP) || defined (MTK_PLT_ON_PC))
   HWD_TX_DC_BIAS_CAL_MSG,
#endif
   HWD_RX_ANTENNA_SELECT_MSG,

   HWD_TST_RX_ANTENNA_TESTMODE_SET_MSG,
   HWD_AUTO_BATTERY_CAL_START,
   HWD_AUTO_BATTERY_CAL_MSG,

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   /* Segment initialize message */
   HWD_RF_CUST_DATA_INIT_NV_MSG,

   /* Segment set message */
   HWD_RF_CUST_DATA_FLUSH_NV_MSG,

   /* NVRAM data to DSPM messages */
   HWD_CUST_TO_DSPM_MSG,

   /* Other messages related to NVRAM */
   HWD_CAL_INIT_NVRAM_MSG,
   HWD_CAL_RF_DB_EXISTS_RSP_MSG,

   /* Cross-Core data process messages */
   HWD_DRDI_VALID_CHECK_MSG,
   HWD_DRDI_INIT_MSG,
   HWD_DRDI_INIT_DONE_MSG,
   HWD_CAL_PARAM_PROC_MSG,

   /* DRDI messge */
   HWD_DRDI_STATUS_MSG,

   /*** SEGMENTS read/write response messages BEGIN ***/
   HWD_RF_CUST_SEG_MSG_START = 2100,
#undef DBM_RF_CUST_ITEM
#define DBM_RF_CUST_ITEM(nAME, sIZE, dATA, iNPUTdATA, fUNC,bAND) nAME##_MSG,
#include "dbmrfcustid.h"
#undef DBM_RF_CUST_ITEM
#define DBM_RF_CUST_ITEM(nAME, sIZE, dATA, iNPUTdATA, fUNC,bAND) nAME##_WRITE_MSG,
#include "dbmrfcustid.h"
   HWD_RF_CUST_SEG_MSG_END,

   HWD_CAL_SEG_MSG_START = 0x2200,
#undef DBM_RF_CAL_ITEM
#define DBM_RF_CAL_ITEM(nAME, sIZE, dATA, fUNC, bAND) nAME##_MSG,
#include "dbmrfcalid.h"
#undef DBM_RF_CAL_ITEM
#define DBM_RF_CAL_ITEM(nAME, sIZE, dATA, fUNC, bAND) nAME##_WRITE_MSG,
#include "dbmrfcalid.h"
   HWD_CAL_SEG_MSG_END,

#if (SYS_BOARD >= SB_JADE)
   /* MIPI segments messages */
   HWD_MIPI_SEG_MSG_START = 0x2300,
#undef DBM_MIPI_ITEM
#define DBM_MIPI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) nAME##_MSG,
#include "dbmmipiid.h"
#undef DBM_MIPI_ITEM
#define DBM_MIPI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) nAME##_WRITE_MSG,
#include "dbmmipiid.h"
   HWD_MIPI_SEG_MSG_END,

   HWD_POC_SEG_MSG_START = 0x2400,
#undef DBM_POC_ITEM
#define DBM_POC_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) nAME##_MSG,
#include "dbmpocid.h"
#undef DBM_POC_ITEM
#define DBM_POC_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC, bAND) nAME##_WRITE_MSG,
#include "dbmpocid.h"
   HWD_POC_SEG_MSG_END,

   HWD_DRDI_SEG_MSG_START = 0x2410,
#undef DBM_DRDI_ITEM
#define DBM_DRDI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC) nAME##_MSG,
#include "dbmdrdiid.h"
#undef DBM_DRDI_ITEM
#define DBM_DRDI_ITEM(nAME, sIZE, dEFAULTdATA, iNPUTdATA, fUNC) nAME##_WRITE_MSG,
#include "dbmdrdiid.h"
   HWD_DRDI_SEG_MSG_END,
#endif

   /*** SEGMENTS read response messages END ***/

#else /* MTK_CBP */
   HWD_CAL_AFC_PARMS_MSG,
   HWD_CAL_TEMPERATURE_PARMS_MSG,
 #if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_TEMP_OFFSET_PARMS_MSG,
   HWD_CAL_BATTERY_PARMS_MSG,
   HWD_CAL_BATT_PDM_PARMS_MSG,
   HWD_CAL_BANDGAP_PARMS_MSG,
 #endif
   HWD_CAL_GPS_RXAGC_MSG,
   HWD_CAL_GPS_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_INIT_NVRAM_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_NOT_USED_MSG, /*NOT used Message*/
#endif
   HWD_CAL_RF_DB_EXISTS_RSP_MSG,

   HWD_CAL_BAND_A_TXAGC_MSG = 0x2100,
   HWD_CAL_BAND_A_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_TXAGC_TEMP_ADJ_MSG,
 #if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_A_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_A_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_A_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_A_TX_LIM_BATT_ADJ_MSG,
#endif
   HWD_CAL_BAND_A_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_A_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_TX_PWR_DET_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_A_TX_PWR_DET_MAX_BATT_ADJ_MSG,
   HWD_CAL_BAND_A_RXAGC_MSG,
#endif
   HWD_CAL_BAND_A_RXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_A_PDM_DELTA_ADJ_MSG_UNUSED, /*NOT Used*/
   HWD_CAL_BAND_A_IP2_CAL_MSG_UNUSED,
   HWD_CAL_BAND_A_DIV_RXAGC_MSG,
#endif
   HWD_CAL_BAND_A_DIV_RXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_A_DIV_PDM_DELTA_ADJ_MSG_UNUSED,
#endif
   HWD_CAL_BAND_A_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_A_SEC_RXAGC_MSG,
   HWD_CAL_BAND_A_SEC_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_A_SEC_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_NOIMD_SEC_RXAGC_MSG,
#endif
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_A_AUX_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_A_AUX_TXAGC_MSG,
   HWD_CAL_BAND_A_AUX_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_A_AUX_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TX_LIM_BATT_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_A_AUX_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_A_AUX_TX_PWR_DET_MAX_BATT_ADJ_MSG,
 #endif
   HWD_CAL_BAND_A_GPS_CAL_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_A_TXAGC_SPDM_MSG,
   HWD_CAL_BAND_A_AUX_TXAGC_SPDM_MSG,
#endif

   HWD_CAL_BAND_B_TXAGC_MSG = 0x2200,
   HWD_CAL_BAND_B_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_B_TXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_B_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_B_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_B_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_B_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_B_TX_LIM_BATT_ADJ_MSG,
#endif
   HWD_CAL_BAND_B_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_B_TX_PWR_DET_FREQ_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_B_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_B_TX_PWR_DET_MAX_BATT_ADJ_MSG,
   HWD_CAL_BAND_B_RXAGC_MSG,
#endif
   HWD_CAL_BAND_B_RXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_B_PDM_DELTA_ADJ_MSG_UNUSED,
   HWD_CAL_BAND_B_IP2_CAL_MSG_UNUSED,
   HWD_CAL_BAND_B_DIV_RXAGC_MSG,
#endif
   HWD_CAL_BAND_B_DIV_RXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_B_DIV_PDM_DELTA_ADJ_MSG_UNUSED,
#endif
   HWD_CAL_BAND_B_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_B_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_B_SEC_RXAGC_MSG,
   HWD_CAL_BAND_B_SEC_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_B_SEC_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_B_NOIMD_SEC_RXAGC_MSG,
   HWD_CAL_BAND_B_AUX_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_B_AUX_TXAGC_MSG,
   HWD_CAL_BAND_B_AUX_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_B_AUX_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TX_LIM_BATT_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_B_AUX_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_B_AUX_TX_PWR_DET_MAX_BATT_ADJ_MSG,
#endif
   HWD_CAL_BAND_B_GPS_CAL_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_B_TXAGC_SPDM_MSG,
   HWD_CAL_BAND_B_AUX_TXAGC_SPDM_MSG,
#endif

   HWD_CAL_BAND_C_TXAGC_MSG = 0x2300,
   HWD_CAL_BAND_C_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_C_TXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_C_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_C_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_C_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_C_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_C_TX_LIM_BATT_ADJ_MSG,
#endif
   HWD_CAL_BAND_C_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_C_TX_PWR_DET_FREQ_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_C_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_C_TX_PWR_DET_MAX_BATT_ADJ_MSG,
   HWD_CAL_BAND_C_RXAGC_MSG,
#endif
   HWD_CAL_BAND_C_RXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_C_PDM_DELTA_ADJ_MSG_UNUSED,
   HWD_CAL_BAND_C_IP2_CAL_MSG_UNUSED,
   HWD_CAL_BAND_C_DIV_RXAGC_MSG,
#endif
   HWD_CAL_BAND_C_DIV_RXAGC_TEMP_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_C_DIV_PDM_DELTA_ADJ_MSG_UNUSED,
#endif
   HWD_CAL_BAND_C_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_C_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_C_SEC_RXAGC_MSG,
   HWD_CAL_BAND_C_SEC_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_C_SEC_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_C_NOIMD_SEC_RXAGC_MSG,
   HWD_CAL_BAND_C_AUX_TXAGC_MSG,
   HWD_CAL_BAND_C_AUX_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_C_AUX_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_C_AUX_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TX_LIM_BATT_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_C_AUX_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_C_AUX_TX_PWR_DET_MAX_BATT_ADJ_MSG,
#endif
   HWD_CAL_BAND_C_GPS_CAL_MSG,
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_C_TXAGC_SPDM_MSG,
   HWD_CAL_BAND_C_AUX_TXAGC_SPDM_MSG,
#endif
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
   HWD_CAL_BAND_D_TXAGC_MSG = 0x2400,
   HWD_CAL_BAND_D_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_TXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_D_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_D_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_TX_LIM_BATT_ADJ_MSG,
   HWD_CAL_BAND_D_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_D_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_TX_PWR_DET_MAX_BATT_ADJ_MSG,
   HWD_CAL_BAND_D_RXAGC_MSG,
   HWD_CAL_BAND_D_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_PDM_DELTA_ADJ_MSG_UNUSED,
   HWD_CAL_BAND_D_IP2_CAL_MSG_UNUSED,
   HWD_CAL_BAND_D_DIV_RXAGC_MSG,
   HWD_CAL_BAND_D_DIV_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_DIV_PDM_DELTA_ADJ_MSG_UNUSED,
   HWD_CAL_BAND_D_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_SEC_RXAGC_MSG,
   HWD_CAL_BAND_D_SEC_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_SEC_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_NOIMD_SEC_RXAGC_MSG,
   HWD_CAL_BAND_D_AUX_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_D_AUX_TXAGC_MSG,
   HWD_CAL_BAND_D_AUX_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_D_AUX_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TX_LIM_BATT_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_D_AUX_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_D_AUX_TX_PWR_DET_MAX_BATT_ADJ_MSG,
   HWD_CAL_BAND_D_GPS_CAL_MSG,
   HWD_CAL_BAND_D_TXAGC_SPDM_MSG,
   HWD_CAL_BAND_D_AUX_TXAGC_SPDM_MSG,

   HWD_CAL_BAND_E_TXAGC_MSG = 0x2500,
   HWD_CAL_BAND_E_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_TXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_E_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_E_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_TX_LIM_BATT_ADJ_MSG,
   HWD_CAL_BAND_E_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_E_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_TX_PWR_DET_MAX_BATT_ADJ_MSG,
   HWD_CAL_BAND_E_RXAGC_MSG,
   HWD_CAL_BAND_E_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_PDM_DELTA_ADJ_MSG_UNUSED,
   HWD_CAL_BAND_E_IP2_CAL_MSG_UNUSED,
   HWD_CAL_BAND_E_DIV_RXAGC_MSG,
   HWD_CAL_BAND_E_DIV_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_DIV_PDM_DELTA_ADJ_MSG_UNUSED,
   HWD_CAL_BAND_E_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_DIV_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_SEC_RXAGC_MSG,
   HWD_CAL_BAND_E_SEC_RXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_SEC_RXAGC_MULTIGAIN_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_NOIMD_SEC_RXAGC_MSG,
   HWD_CAL_BAND_E_AUX_RX_BANK_CAL_MSG,
   HWD_CAL_BAND_E_AUX_TXAGC_MSG,
   HWD_CAL_BAND_E_AUX_TXAGC_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TXAGC_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TXAGC_BATT_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TXAGC_ALT_THRESH_MSG,
   HWD_CAL_BAND_E_AUX_TX_LIM_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TX_LIM_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TX_LIM_BATT_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TX_PWR_DET_TBL_MSG,
   HWD_CAL_BAND_E_AUX_TX_PWR_DET_FREQ_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TX_PWR_DET_TEMP_ADJ_MSG,
   HWD_CAL_BAND_E_AUX_TX_PWR_DET_MAX_BATT_ADJ_MSG,
   HWD_CAL_BAND_E_GPS_CAL_MSG,
   HWD_CAL_BAND_E_TXAGC_SPDM_MSG,
   HWD_CAL_BAND_E_AUX_TXAGC_SPDM_MSG,
#endif
#endif /* MTK_CBP */

   HWD_RF_VERSION_MSG = 0x2600,
   /* RF Band Class Support Table */
   HWD_SYS_BC_SUPPORT_TABLE_MSG,
   HWD_CAL_DSPM_READY_MSG,


   /*-----------------------------------------------------------------
    * Miscellaneous Driver Control Messages
    *----------------------------------------------------------------*/
   /* External Audio DAC */
   HWD_DAC_INIT_MSG = 0x3000, 
   HWD_DAC_SET_VOL_MSG,
   HWD_DAC_OFF_MSG,
   /* Touch Screen */
   HWD_SCREEN_CALIBRATION_REQ_MSG,
   HWD_SCREEN_CALIBRATION_CNF_MSG,

   /* NAND Flash Controller */
   HWD_NFC_SIMU_OPERATIONS = 0x3100,  
   HWD_NFC_COPY_TO_FLASH,
   HWD_NFC_CONFIG,

   /* MMC Driver */
   HWD_MMC_INIT_MSG = 0x3200,
   HWD_MMC_READ_MSG,
   HWD_MMC_WRITE_MSG,
   HWD_MMC_ERASE_MSG,
   HWD_MMC_CARD_STATUS_MSG,
   HWD_MMC_RESET_CARD_MSG,
   HWD_MMC_DEACT_CARD_MSG,
   HWD_MMC_CARD_CAPACITY_MSG,
   HWD_SDIOM_INIT_MSG,
   HWD_SDIOM_READ_BYTE_MSG,
   HWD_SDIOM_WRITE_BYTE_MSG,
#if defined (MTK_CBP) && !defined (MTK_PLT_ON_PC)
   HWD_TEST_PMIC_READ_MSG = 0x3300,
   HWD_TEST_PMIC_WRITE_MSG,
   HWD_TEST_PMIC_VPA_DELAYLOAD_MSG,
#else
   HWD_TST_I2C_READ_MSG = 0x3300,
   HWD_TST_I2C_WRITE_MSG,
#endif
   /* LCD Driver */
   HWD_LCD_TEST_MSG = 0x3400,      
   HWD_LCD_SINGLE_CMD_MSG,
   HWD_LCD_SET_INTERFACE_MSG,

   /* SPI */
   HWD_SERIALIZER_CONFIG_MSG = 0x3500,
   HWD_SERIALIZER_WRITE_MSG,
   HWD_ETS_SPI_DELAY_WRITE_MSG,
   HWD_ETS_RSSI_CORRECTION_MSG,

   /* USB */
   HWD_USB_DEV_CONTROL_MSG,
   HWD_USB_EP0_IN_EVT_MSG,
   HWD_USB_EP0_IN_DONE_MSG,
   HWD_USB_EP0_OUT_DONE_MSG,

   /* RTC ALARM */
   HWD_RTC_ALARM_MSG,

   /*-----------------------------------------------------------------
    * System time transition handlers
    *----------------------------------------------------------------*/
   HWD_SYS_TRANSIT_1X_TO_DO_MSG = 0x4000,
   HWD_SYS_TRANSIT_DO_TO_1X_MSG,
   HWD_SYS_REGISTER_SYS_TIME_MSG,
   HWD_SYS_SET_FRAMEOFFSET_MSG,
   HWD_SYS_TIME_DO_GET_MSG,
   HWD_SYS_9MHZ_CNT_INIT_MSG,
   HWD_SYS_TIMEDATA_GET_MSG,
   
   HWD_SYS_SET_PARM_MSG,
   HWD_SYS_GET_PARM_MSG,

   /*TS*/
   HWD_TS_EVENT_MSG = 0x4100,
   HWD_TS_TIMER_MSG,

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   HWD_VAL_RX_TX_POWER_DETECTOR_MSG = 0x4200,
   HWD_RMC_RX_AGC_GAIN_CFG_RSP_MSG ,   
   HWD_RCP_TX_CDMA_CTRL_START_RSP_MSG,
   HWD_RCP_TX_CDMA_CTRL_STOP_RSP_MSG,
#endif

#ifdef MTK_CBP
   HWD_VAL_RX_TEST_MODE_MSG,
#endif

   /*-----------------------------------------------------------------------------
    * The remaining message ID's starting at 0xA000 are reserved for customer use
    *-----------------------------------------------------------------------------*/
   HWD_FIRST_CUST_MSG_ID = 0xA000

} HwdCmdIdT;

/* power saving modem modes */
typedef enum 
{
  HWD_PWRSAVING_1X_LOW_PWR,
  HWD_PWRSAVING_DO_LOW_PWR,
  HWD_PWRSAVING_1X_PWRUP,
  HWD_PWRSAVING_PWRUP_QP,
  HWD_PWRSAVING_1X_RX,
  HWD_PWRSAVING_1X_RX_TX,

  HWD_PWRSAVING_DO_PWRUP,
  HWD_PWRSAVING_DO_PN_SEARCH,
  HWD_PWRSAVING_DO_RX,
  HWD_PWRSAVING_DO_RX_TX,

  HWD_PWRSAVING_NUM_OF_STATES
} HwdPwrSaveModesT;


/* clk source options */
typedef enum
{	
   HWD_CLK_OPTION_32K_INT_OSC = 0,
   HWD_CLK_OPTION_32K_BYPASS,
   HWD_CLK_OPTION_32K_DIV_TCXO
}HwdClkOption32KT; 

typedef enum
{
   HWD_ENABLE = 0,
   HWD_DISABLE,
   HWD_LOAD
} HwdCtrlModeT;

/* Clock Control Blocks */
typedef enum 
{
  HWD_BLK_CTRL_DMA_CTRL,
  HWD_BLK_CTRL_DMA_RX,
  HWD_BLK_CTRL_DMA_TX,
  HWD_BLK_CTRL_PPP_HA,
  HWD_BLK_CTRL_RF_CTRL,
  HWD_BLK_CTRL_DLY_CTRL,
  HWD_BLK_CTRL_1X_MDM,
  HWD_BLK_CTRL_RF_SPI,
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
  HWD_BLK_CTRL_BSI,
  HWD_BLK_CTRL_BPI,
  HWD_BLK_CTRL_PA_VDD,
#if (SYS_BOARD >= SB_JADE)
  HWD_BLK_CTRL_MIPI,
#endif
#endif
  HWD_BLK_CTRL_MXS_RX,
  HWD_BLK_CTRL_MXS_TX,
  HWD_BLK_CTRL_MXS_VC,
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
  HWD_BLK_CTRL_MXS,
#endif
  HWD_BLK_CTRL_1X_SYSTIME,
  HWD_BLK_CTRL_DO_SYSTIME,
  HWD_BLK_CTRL_DO_SEARCHER,
  HWD_BLK_CTRL_DO_RX_CTRL,
  HWD_BLK_CTRL_DO_DRC,
  HWD_BLK_CTRL_DO_FINGERS,
  HWD_BLK_CTRL_DO_RX_EQUAL,
  HWD_BLK_CTRL_DO_PREAMB,
  HWD_BLK_CTRL_DO_HARQ,
  HWD_BLK_CTRL_DO_TURBO_DECODE,
  HWD_BLK_CTRL_DO_MCD_CTRL,
  HWD_BLK_CTRL_DO_C2I,
  HWD_BLK_CTRL_DO_TX_CTRL,

  HWD_BLK_CTRL_DMAC_LCD_PSO,

  HWD_BLK_CTRL_NUM_BLOCKS
} HwdBlkControlBlocksT;

/*------------------------------------------------------------------------
* Definition of GPS Clock Edge
*------------------------------------------------------------------------*/
typedef enum
{
   GPS_RF_CLK_EDGE_NONE,
   GPS_RF_CLK_EDGE_2BITS_NEG,
   GPS_RF_CLK_EDGE_2BITS_POS,
   GPS_RF_CLK_EDGE_3BITS_NEG,
   GPS_RF_CLK_EDGE_3BITS_POS,
   GPS_RF_CLK_EDGE_4BITS_NEG,
   GPS_RF_CLK_EDGE_4BITS_POS,
   GPS_RF_CLK_EDGE_MAX
} GpsRfClkEdgeT;

/*-------------------------**
** Global type definitions **
**--------------------------*/

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RespInfo;
   bool        AckRequired;
   uint8       Owner;
} PACKED_POSTFIX HwdResetReqMsgT;

/* Rx power calibration (baseline, without temp and freq channel adjustment) */
typedef PACKED_PREFIX struct
{
   int16 RxAgcDacSetting; /* Rx AGC DAC setting coordinate. Units: DAC steps */
   int16 RxGainDb;        /* Rx gain coordinate. Units: dB with Q=IPC_DB_Q */
} PACKED_POSTFIX  HwdRxPwrCoordinateT;     /* format matches HwdCoordinateT */

/* Tx power calibration (baseline, without temp and freq channel adjustment) */
typedef PACKED_PREFIX struct
{
   int16 TxPwrDbm;        /* Tx power coordinate. Units: dBm with Q=IPC_DB_Q */
   int16 TxAgcDacSetting; /* Tx AGC DAC setting coordinate. Units: DAC steps */
} PACKED_POSTFIX  HwdTxPwrCoordinateT;    /* format matches HwdCoordinateT */

typedef PACKED_PREFIX struct
{
   int32 RfDelay1X;
   int32 RfDelayDO;
   int32 Reserve2;
   int32 Reserve3;

} PACKED_POSTFIX HwdGpsCalBandDataT;



/* HWD_CAL_GPS_CALIB_DB_MSG */
typedef PACKED_PREFIX struct
{
   HwdGpsCalBandDataT GpsCalBand;
   uint16             GpsCalStatus;       /* To check the Global Variable Status         */ 
} PACKED_POSTFIX  HwdGpsCalNvDataT;

/* HWD_CAL_GPS_CHAR_DB_MSG */
typedef PACKED_PREFIX struct
{
   uint8 CNOOffset;  /* CNO offset depends on LNA and other external components */
} PACKED_POSTFIX  HwdCalGpsCharDBT;

/* HWD_CAL_BAND_A_IP2_CAL_MSG, HWD_CAL_BAND_B_IP2_CAL_MSG */
typedef PACKED_PREFIX struct
{
   int16 IMixerLoad;
   int16 QMixerLoad;
} PACKED_POSTFIX  Ip2CalMixerLoadsT;

typedef PACKED_PREFIX struct
{
   Ip2CalMixerLoadsT MixerLoads[5];
} PACKED_POSTFIX  HwdIp2CalibDataT;

/* typedef for HWD_TST_PLL_CHANNEL_CONFIG_MSG */
typedef enum
{
   HWD_PLL_CFG_CSS_UNIT = 1,
   HWD_PLL_CFG_HWD_UNIT = 2,
   HWD_PLL_CFG_BOTH = 3
}HwdCtrlUnitT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT   RspInfo;             /* cmd/rsp info for acknowlegment */
   HwdCtrlModeT CtrlMode;
   HwdCtrlUnitT CtrlUnit;   /* bit-map field */
   SysAirInterfaceT RfMode;
   uint8        MdmMain;      /* Main, Div or Sec */
   uint8        MdmDiv;       /* Main, Div or Sec */
   SysCdmaBandT Band;
   uint16       Channel;
} PACKED_POSTFIX  HwdTstPllChannelMsgT;

/* HWD_AUX_ADC_POLL */
typedef PACKED_PREFIX struct
{
   uint8    Enable;
} PACKED_POSTFIX HwdAuxAdcPollMsgT;

/* define power saving mode set msg command */
#define AUTO_PWR_SAVINGS    0xff
typedef PACKED_PREFIX struct
{
   ExeRspMsgT    RspInfo;             /* cmd/rsp info for acknowlegment */
   uint8         PwrSaveMode;
   uint8         MainRxEn;
   uint8         DivRxEn;
   uint8         SecRxEn;
   uint8         MainTxEn;
   uint8         AuxTxEn;
   uint16        VcPwrSaveMode;
} PACKED_POSTFIX  HwdPwrSaveModeConfigMsgT;

typedef PACKED_PREFIX struct
{
   uint8         PwrSaveMode;
   uint8         MainRxEn;
   uint8         DivRxEn;
   uint8         SecRxEn;
   uint8         MainTxEn;
   uint8         AuxTxEn;
   uint16        VcPwrSaveMode;
} PACKED_POSTFIX  HwdPwrSaveModeConfigRspT;

typedef PACKED_PREFIX struct
{
    bool             Enable;
} PACKED_POSTFIX  HwdSimWatchdogMsgT;

typedef struct
{
   bool VibratorEnable;        /* TRUE if Enable the vibrator */
} HwdVibratorMsgT;

/* HWD_MMAPPS_FRAME_RELOAD_RESP_MSG */
typedef struct
{
    uint32 AppId;              /* Use DspvAppIdT */
    uint16 NumFramesLoaded;
} HwdMMAppsFrameReloadRspMsgT;

/* HWD_ACTIVE_PILOT_DATA_MSG */
typedef PACKED_PREFIX struct
{
   uint8    NumActive;                        /* Number of active set pilot measurements */
   uint16   Strength[SYS_CP_MAX_ACTIVE_LIST_PILOTS];
} PACKED_POSTFIX HwdActivePilotDataMsgT;

typedef enum
{
   HWD_CLK_CP,
   HWD_CLK_AHB,
   HWD_CLK_NUM
}HwdClkTypesT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#if defined(MTK_PLT_DENALI)
/* for FQMTER Function */
typedef enum
{
   MDM_PLLOUT=1,
   CP_PLLOUT,
   DSP_PLLOUT,
   F26M_CLK,
   F32K_CLK, 
   RX1_CLK49_MX,
   CG_GTX1X_DRT_CLK,
   CG_GTX1X_CLK9_CLK,
   CG_GTX1X_CLK19_CLK,
   CG_GTX1X_CLK39_CLK,
   CG_C2KLOG_CLK49_CLK,
   CLK_ARM_PROC_CLK,
   CLK_AMBA_CLK,
   CLK_C2KSPH_HCLK_CLK,
   CLK_C2KSPH_260M_CLK,
   GCLK_PMIC_BCLK_CLK,
   CG_DM_FCLK,
   CG_TD1X_CLK,
   CG_TDDO_CLK,
   CG_GSRCH_CLK,
   CG_IC_CLK,
   CG_SRDO_CLK,
   CG_GRXSPI_CLK,
   CG_GTXSPI_CLK,
   CG_GUIM0_CLK,
   CG_GUIM1_CLK,
   F13M_CLK,   
   CG_CLK32,
   CG_JIT0_CLK,
   CG_JIT1_CLK,     
   FRMTER_MAXCLK=31
 
 }HwdClkFqmtTckSel;
#elif (SYS_BOARD >= SB_JADE)
/* for FQMTER Function */
typedef enum
{
   MDM_PLLOUT=1,
   CP_PLLOUT,
   DSP_PLLOUT,
   F26M_CLK,
   F32K_CLK, 
   CG_GRXDO_CLK9_CLK,  /* Jade added */
   CG_MUX_CLK_CLK,   /* cg_mux_clk from CLK_CTRL3[6:0] */
   CG_GTX1X_CLK9_CLK,
   CG_GRX1X_CLK9_CLK, /* Jade added */
   CG_CLK_104M_FREE_CLK, /* Jade added */
   CG_C2KLOG_CLK39_CLK,   /* Jade added */
   CLK_ARM_PROC_CLK,
   CLK_AMBA_CLK,
   CLK_C2KSPH_HCLK_CLK,
   CLK_C2KSPH_260M_CLK,
   GCLK_PMIC_BCLK_CLK,
   CG_DM_FCLK,
   CG_TD1X_CLK,
   CG_TDDO_CLK,
   CG_GSRCH_CLK,
   CG_IC_CLK,
   CG_SRDO_CLK,
   CG_BSI1_CLK,   /* Jade added */
   CG_MIPI0_CLK,   /* Jade added */
   CG_GUIM0_CLK,
   CG_GUIM1_CLK,
   F13M_CLK,   
   CG_CLK32,
   CG_JIT0_CLK,
   CG_JIT1_CLK,     
   FRMTER_MAXCLK=31
 
 }HwdClkFqmtTckSel;
#endif

typedef enum
{
   F26MCLK = 0,
   F32KCLK = 2, 

 }HwdClkFqmtFckSel;

typedef enum 
{
   PLLCTRL_SWMODE = 0x01,
   PLLCTRL_HWMODE = 0x02
} HwdClkPllCtrlMode;


typedef enum
{
    HWD_SRCSEL_F26M_CLK = 0x00,
    HWD_SRCSEL_CP_260M_CLK = 0x01,
    HWD_SRCSEL_MDM_39M_CLK = 0x02,
} HwdUimClkSrcSel;

typedef enum
{
     HWD_LOWCLK  = 0x00,
     HWD_HIGHCLK = 0x01,
} HwdUimClkFreqSel;

typedef enum
{
     HWD_TxRxSpi_Clk_130M = 0x00,  /* CP260M/2=130M */
     HWD_TxRxSpi_Clk_65M = 0x01,   /* CP260M/4=65M */
     HWD_TxRxSpi_Clk_32M = 0x02,   /* CP260M/8=32M */
     HWD_TxRxSpi_Clk_16M = 0x03,   /* CP260M/16=16M */
} HwdTxRxSPIClkFreqSel;
#endif

typedef enum
{
   RMC_NORMAL_RF_MODE = 0,
   RMC_DIVERSITY_ONLY_MODE
} HwdDivRxTestModeT;

typedef PACKED_PREFIX struct
{
    uint8   Status;
} PACKED_POSTFIX  HwdRfGsmCompensateMsgT;


#ifdef  MTK_DEV_SSDVT
typedef struct 
{
   uint32 reggroups1[37];   /* 0x52013000 - 0x52013090 */
   uint32 reggroups2[20];   /* 0x520130F0 - 0x5201313C */
}  PllMapRegisters;

#endif


/*----------------------------------------------------------------------------
 Global Data
----------------------------------------------------------------------------*/
extern uint16 SbpClockMode;
/*----------------------------------------------------------------------------
 Global Function Prototypes
----------------------------------------------------------------------------*/

/* Initialization */
extern void HwdInit(void);
extern void	HwdTask(uint32 argc, void *argv);

#if (SYS_BOARD >= SB_JADE) && (SYS_BOARD <= SB_EVEREST)
extern void HwdClkSrcInit(bool CoClockEn);
#endif
extern void HwdClkInit(void);
extern void HwdSet32KClock (void);
extern void HwdPowerDown32KClock (void);
extern void HwdSwitch32kClkSrc (HwdClkOption32KT New32kClkSource);
extern void HwdSetClockMuxesForDualTcxo(SysAirInterfaceT Mode, bool LiveSwitch);

extern bool  HwdGetClockSRCMode(void);

#ifndef BOOT_BUILDING
extern void  HwdClkRestoreCpPll(void);
#endif
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
extern void  HwdClkMDMPllInit(void);
extern void  HwdClkDSPPllCtrl(bool Enable, uint8 Mode);
extern void  HwdClkUimClockSet(HwdUimClkSrcSel UimClkSrc, HwdUimClkFreqSel UimClkFreq, uint8 UimCard);
extern void  HwdClkTxSpiClockSet(HwdTxRxSPIClkFreqSel SpiClkFreq);
extern void  HwdClkRxSpiClockSet(HwdTxRxSPIClkFreqSel SpiClkFreq);
#endif

/* watchdog functions */
extern void HwdWatchdogInit(void);
extern void HwdWatchdogSetTime(uint16 TimeMs);
extern void HwdWatchdogKill(void);
extern void HwdSimWatchdogMsg (HwdSimWatchdogMsgT *MsgDataPtr);

/* Power detect enable/disable function */
extern void HwdTxPwrDetectEnable(SysAirInterfaceT ModeLocal, bool Enable);

/* Calibration */
extern void HwdCalResetBandChan(uint8 RfSystem);
extern void HwdCalGetGainStateNumbers(SysCdmaBandT Band, uint16 *PaGains, uint16 *LnaGains);
extern HwdCalGpsCharDBT* HwdGetGPSCharDB( HwdRfBandT RfBand );
extern HwdGpsCalNvDataT* HwdGetGpsCalibDB( HwdRfBandT RfBand );

extern HwdSupportedCDMABandT *HwdRfSupportedBands(void);
extern HwdSupportedCDMABandT *HwdRfDriverSupportedBands(void);

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
extern void HwdBattCheckPwrOnVolt(void);
#endif
extern uint32 HwdClkFrequencyGet(HwdClkTypesT type);
extern 	void HwdGpsSetPulseWdith( void );

/* PWMs */
#ifdef __cplusplus
extern "C" {
#endif
extern void HwdWatchdogKick(void);
#ifdef __cplusplus
}
#endif


extern void Hwd32kDelay(uint32 delay_cnt);

extern GpsRfClkEdgeT HwdGetGpsRFClkEdge( void );


/*****************************************************************************
* $Log: hwdapi.h $
*****************************************************************************/

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:15:28 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:19:48 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
/**Log information: \main\Trophy\Trophy_SO73\1 2013-07-09 02:12:13 GMT jtzhang
** scbp#11737**/
/**Log information: \main\Trophy\2 2013-07-17 08:16:59 GMT jtzhang
** scbp#11737**/
