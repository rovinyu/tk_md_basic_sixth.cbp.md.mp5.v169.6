/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2006-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
/*****************************************************************************
* 
* FILE NAME   : do_rupapi.h
*
* DESCRIPTION : API definition for RUP (Route Update Protocol).
*
* HISTORY     :
*****************************************************************************/
#ifndef _DO_RUPAPI_H_
#define _DO_RUPAPI_H_

/*----------------------------------------------------------------------------
 Include Files
----------------------------------------------------------------------------*/
#include "do_rmcapi.h"
#include "do_rcpapi.h"
/*----------------------------------------------------------------------------
 Global Defines and Macros
----------------------------------------------------------------------------*/
 /*****************************************************************************
 * Defines
 ****************************************************************************/
#define  MAX_SUPPORTED_BAND             3
#define  MAX_SUPPORTED_SUBBAND          8
#define  DO_MSG_ID_LEN                       8
#define  DO_TRANSAC_ID_LEN                   8

#define  DO_SYSTYPE_LEN                8
#define  DO_BANDCLASS_LEN              5
#define  DO_CHAN_LEN                   11

#define  MAX_SRHWIN_OFFSET             8
#define  MAX_SRHWIN_SIZE               17
#define  MAX_TDROP_TIMER               16
#define  MAX_DROPTIMER_NUM             (SYS_MAX_ACTIVE_LIST_PILOTS+SYS_MAX_CANDIDATE_LIST_PILOTS+1)
#define  DO_SYS_TYPE                   0
#define  NULL_PN_PHASE                 0x7FFF
#define RUP_MAX_NLPILOT_NUM                  32

#define  RUP_DEFAULT_DFS_IDLE_INTERVAL     6
#define  RUP_DEFAULT_DFS_CONN_INTERVAL     2
#define  RUP_DEFAULT_DFS_IDLE_NUMCHAN      1
#define  RUP_DEFAULT_DFS_CONN_NUMCHAN      1
#define  RUP_DEFAULT_DFS_IDLE_ECIO_THRESH  48  
#define  RUP_DEFAULT_DFS_CONN_ECIO_THRESH  48
#define  RUP_DFS_SEARCH_ALL_CHANNELS       255
/*****************************************************************************
  * Structure definitions
  ****************************************************************************/

/*----------------------------------------------------------------------------
     Message Formats structure
----------------------------------------------------------------------------*/
/*typedef enum
{
   ACCESS_CHAN,
   TRAFFIC_CHAN,
   BOTH
}RevPhyChanT;*/

/* CDMA Channel Record */
typedef PACKED_PREFIX struct
{
   uint8             SysType;
   uint8             BandClass;
   uint16            Channel;
} PACKED_POSTFIX  ChanRecGT;

/* RUP_SCH_RESULT_RPT_CMD_MSG */
/* interface with Searcher Control*/
/*pilot measurement report structure*/
 typedef PACKED_PREFIX struct
{
   int16             PnPhase;        /*offset, in units of 0.5chips,(-2048~+2048)*/
   int16             Strength;        /*in units of 0.125dB*/  
} PACKED_POSTFIX  RupPilotMeasurGT;
 
/* search result report msg structure*/
 typedef  PACKED_PREFIX struct /*RUP_SRC_SEARCH_RESULT_RPT_CMD_MSG*/
{
   int16             RptSeq;
   uint16            ReferencePN;     /* the earliest pilotPN */
   uint8             NumActive;       /* Number of active set pilot */
   uint8             NumCandidate;    /* Number of candidate set pilot */
   uint8             NumNeighbor;     /* Number of neighbor set pilot */
   uint8             NumRemaining;    /*Number of remaining set pilot*/
 
   RupPilotMeasurGT  Active[SYS_MAX_ACTIVE_LIST_PILOTS];
   RupPilotMeasurGT  Candidate[SYS_MAX_CANDIDATE_LIST_PILOTS];
   RupPilotMeasurGT  Neighbor[SYS_MAX_NEIGHBOR_LIST_PILOTS];
   RupPilotMeasurGT  Remaining[SYS_MAX_REMAINING_LIST_PILOTS];    
} PACKED_POSTFIX RupSearchResultRptMsgT;

typedef  struct
{
   uint8             Count;
   uint16            PilotPN[SYS_MAX_NEIGHBOR_LIST_PILOTS];
   uint8             ChanInc[SYS_MAX_NEIGHBOR_LIST_PILOTS];
   ChanRecGT         Chan[SYS_MAX_NEIGHBOR_LIST_PILOTS];
   uint8             SrhWinSizeInc ;
   uint8             SrhWinSize[SYS_MAX_NEIGHBOR_LIST_PILOTS];
   uint8             SrhWinOffInc;
   uint8             SrhWinOffset[SYS_MAX_NEIGHBOR_LIST_PILOTS];
} NghbrInfoT;

/*Overhead.update Indication*/
 typedef  struct
{
   uint8             SectorId[16];
   uint16            PilotPN;
   uint32            Latitude;
   uint32            Longitude;
   uint16            RouteUpdateRadius;
   NghbrInfoT        Nghbr;  
} RupOmpUpdatedMsgGT;
 
/*RUP_DISP_CONNST_TMSTATE_CMD_MSG*/
typedef struct
{
   bool              state;
} RupIdpConnTmStateMsgGT;

/*Rup.Activate command*/
typedef  struct
{
   ChanRecGT Chan;
   uint16            PilotPN;
} RupAlmpActiveMsgGT;

typedef struct
{
  uint8      Num;
  uint16        PilotPn[SYS_MAX_NEIGHBOR_LIST_PILOTS+SYS_MAX_CANDIDATE_LIST_PILOTS];        
  int16          Strength[SYS_MAX_NEIGHBOR_LIST_PILOTS+SYS_MAX_CANDIDATE_LIST_PILOTS];     /*in units of 0.125db*/
}Rup2ValPnListInfoT;

typedef enum
{
   IDP_DFS_SLOTTED,
   IDP_DFS_NON_SLOTTED,
   IDP_MAX_DFS_START_CAUSE_CODE
} DfsStartCauseCode;

typedef PACKED_PREFIX struct
{
   DfsStartCauseCode Cause;
   UINT8             NumDfsChan;
} PACKED_POSTFIX  RupIdpDfsStartMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 IdleInterval;
   UINT8 ConnInterval;
   UINT8 IdleNumChan;
   UINT8 ConnNumChan;
   UINT16 IdleEcIoThresh;
   UINT16 ConnEcIoThresh;
} PACKED_POSTFIX  RupClcDfsSettingsMsgT;

typedef PACKED_PREFIX struct
{
   ExeRspMsgT  RspInfo; /* Response routing information */
} PACKED_POSTFIX  RupIopGetDfsInfoMsgT;

typedef PACKED_PREFIX struct
{
   UINT8 State;
   UINT8 IdleCause;
   UINT8 Count;
   UINT8 ChanIndex;
   UINT8 NumChan;
   UINT16 Chan[10];
} PACKED_POSTFIX  RupIopGetDfsInfoRspMsgT;

/*----------------------------------------------------------------------------
 Global Functions
----------------------------------------------------------------------------*/
extern uint16 RupGetSearchWinSize (uint8 size);
extern int16 RupGetSearchWinOffset (uint16 size, uint8 offset);
extern void PcpGetAcmRupMsgData(DatapktGT*  pPkt);
extern bool PcpGetAcmRupMsgLen(uint16  BytesAvail, uint16*  pMsgLen);

/* get the pilot strength(signed, in Q3 dB) of current active sector */
extern int16 RupGetCurSectorPilotStrength(void); 
extern bool RupCheckNeighborListSearchDone(void);
extern void RupGetActiveSetInfo(Rup2ValPnListInfoT* pInfo);
extern void RupGetNeighborSetInfo(Rup2ValPnListInfoT* pInfo);

/*****************************************************************************
* End of File
*****************************************************************************/
#endif
