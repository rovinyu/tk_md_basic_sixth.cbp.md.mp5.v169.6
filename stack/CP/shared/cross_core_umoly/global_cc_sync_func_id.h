/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2012
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *   global_cc_sync_func_id.h
 *
 * Project:
 * --------
 *   UMOLY
 *
 * Description:
 * ------------
 *   CC IRQ SYNC function ID definition for MD1 and MD3.
 *
 * Author:
 * -------
 *  Jun-Ying Huang
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision$
 * $Modtime$
 * $Log$
 *
 * 08 14 2015 jun-ying.huang
 * [MOLY00137306] [System service][CC IRQ]Add MD1 and MD3 share memory "PS2MD3_LPM_CNT" and SYNC_FUNC_ID "LPM_CNT" for 32K-less user, Lanslo Yang
 * .
 *
 * 07 08 2015 jun-ying.huang
 * [MOLY00126776] [System service][CC IRQ]CC IRQ HS and Polling Implementation between Pscore and L1core
 * .
 *
 * 07 03 2015 jun-ying.huang
 * [MOLY00125956] [System service][CC IRQ]CC IRQ final version after IT
 * CC IRQ final version after IT
 *
 * 06 20 2015 liang.yan
 * [MOLY00122638] [Change Feature][MT6755][CCIRQ]Modify some ccirq global_file in cross_core_umoly
 * [Change Feature][MT6755][CCIRQ]modify some ccirq global_file in cross_core_umoly
 *
 * 06 18 2015 jun-ying.huang
 * [MOLY00122475] [system service][CC IRQ]Add "Copyright Statement".
 * Add "Copyright Statement" to every CC IRQ file.
 *
 * 06 12 2015 jun-ying.huang
 * [MOLY00120783] [System service]CC IRQ for MD1 and MD3
 * 1.[Jade] CC IRQ Driver migration for multi-core
 * 	-Add MD3 driver interface
 * 	-Add MD3 CC SYS interface
 * 	-Add buffer mode function
 * 	2.[Jade] CC IRQ ILM TX/RX for MD1<->MD3
 * 	-CC IRQ take charge of the transportation of KAL ILM message between MD1 & MD3. KAL ILM function is used by user and check the mod_id, ILM direction.
 * 	3.[Jade] MD1<->MD3 CC IRQ ShareMem Implementation
 * 	4.[Jade] MD1<->MD3 CC IRQ RPC Implementation
 * 	5.[Jade] PScore<->MD3 CC IRQ HS and Polling Implementation
 * 
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/
 
/* for function registered by user to execute before/after sync packet to/from MD3. */
/* CC IRQ SYNC function ID */
#ifndef _GLOBAL_CC_SYNC_FUNC_ID_H_
#define _GLOBAL_CC_SYNC_FUNC_ID_H_


typedef enum {
    CC_IRQ_SYNC_P2MD3_BASE = 0,
    /* ==== Start of PScore to MD3 sync index(PScore ping MD3) ==== */
    CC_IRQ_SYNC_P2MD3_LPM_CNT = CC_IRQ_SYNC_P2MD3_BASE,
    /* ==== End of PScore to MD3 sync index(PScore ping MD3) ==== */
    CC_IRQ_SYNC_P2MD3_END, 
    CC_IRQ_SYNC_MD32P_START = CC_IRQ_SYNC_P2MD3_END,
    /* ==== Start of MD3 to PScore sync index(MD3 ack PScore) ==== */ 
//    CC_IRQ_SYNC_MD32P_RESERVED = CC_IRQ_SYNC_MD32P_START,
    /* ==== End of MD3 to PScore sync index(MD3 ack PScore) ==== */
    CC_IRQ_SYNC_MD32P_END = CC_IRQ_SYNC_MD32P_START /* no user now */
} CC_IRQ_SYNC_FUNC_ID_T;

typedef enum {
    CC_IRQ_SYS_STATE_INIT = 0,
    CC_IRQ_SYS_STATE_RUNNING,
    CC_IRQ_SYS_STATE_ASYNC,    
    CC_IRQ_SYS_STATE_MAX
} CC_IRQ_SYS_STATE_T;

typedef enum{
    CC_IRQ_SYNC_PKT_TYPE_INIT_HS1 = 0x1111,
    CC_IRQ_SYNC_PKT_TYPE_INIT_HS2 = 0x2222,		
    CC_IRQ_SYNC_PKT_TYPE_PING = 0xCCCC,
    CC_IRQ_SYNC_PKT_TYPE_PING_ACK = 0xAAAA,    
    CC_IRQ_SYNC_PKT_MAX
} CC_IRQ_SYNC_PKT_TYPE_T;	

#endif  /* _GLOBAL_CC_SYNC_FUNC_ID_H_ */

/* End of file*/

