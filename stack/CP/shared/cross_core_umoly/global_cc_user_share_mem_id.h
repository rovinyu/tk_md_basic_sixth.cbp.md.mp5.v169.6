/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *   global_cc_user_share_mem_id.h
 *
 * Project:
 * --------
 *   
 *
 * Description:
 * ------------
 *   This file provides the user share memory ENUM for MD1<->MD3.
 *
 * Author:
 * -------
 *   Jun-Ying Huang   (mtk09276)
 *
 *============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision$
 * $Modtime$
 * $Log$
 *
 * 06 20 2015 liang.yan
 * [MOLY00122638] [Change Feature][MT6755][CCIRQ]Modify some ccirq global_file in cross_core_umoly
 * [Change Feature][MT6755][CCIRQ]modify some ccirq global_file in cross_core_umoly
 *
 * 06 18 2015 jun-ying.huang
 * [MOLY00122475] [system service][CC IRQ]Add "Copyright Statement".
 * Add "Copyright Statement" to every CC IRQ file.
 *
 * 06 12 2015 jun-ying.huang
 * [MOLY00120783] [System service]CC IRQ for MD1 and MD3
 * 1.[Jade] CC IRQ Driver migration for multi-core
 * 	-Add MD3 driver interface
 * 	-Add MD3 CC SYS interface
 * 	-Add buffer mode function
 * 	2.[Jade] CC IRQ ILM TX/RX for MD1<->MD3
 * 	-CC IRQ take charge of the transportation of KAL ILM message between MD1 & MD3. KAL ILM function is used by user and check the mod_id, ILM direction.
 * 	3.[Jade] MD1<->MD3 CC IRQ ShareMem Implementation
 * 	4.[Jade] MD1<->MD3 CC IRQ RPC Implementation
 * 	5.[Jade] PScore<->MD3 CC IRQ HS and Polling Implementation
 * 
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *============================================================================
 ****************************************************************************/
#ifndef _GLOBAL_CC_USER_SHM_ID_H_
#define _GLOBAL_CC_USER_SHM_ID_H_

#undef MD1_MD3_USER_SHARE_MEMORY_BEGIN
#define MD1_MD3_USER_SHARE_MEMORY_BEGIN(BEGIN_POS)  typedef enum __cgen_md1_md3_user_sharemem_id{BEGIN_POS = 0,
//#define MD1_MD3_USER_SHARE_MEMORY_END(END_POS) MD1_MD3_SHARE_MEMORY_USER_ID_END=END_POS }MD1_MD3_SHARE_MEMORY_USER;
    
MD1_MD3_USER_SHARE_MEMORY_BEGIN(CC_SHARE_MEM_BEGIN)
#undef MD1_MD3_CC_USER_SHARE_MEM_REG
#define MD1_MD3_CC_USER_SHARE_MEM_REG(UserID, Size) CC_SHARE_MEM_##UserID,
    #include "md1_md3_cc_user_share_mem_config.h"   
#undef MD1_MD3_CC_USER_SHARE_MEM_REG
/* CC_SHARE_MEM_END */
}MD1_MD3_SHARE_MEMORY_USER;  

#undef MD1_MD3_USER_SHARE_MEMORY_BEGIN

#endif /*_GLOBAL_CC_USER_SHM_ID_H_ */

/* End of file */

