/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _DSPMAPI_H_
#define _DSPMAPI_H_
/*****************************************************************************

  FILE NAME: dspmapi.h
 
  DESCRIPTION:

    This file contains CP - DSPM constants/message/structure definitions.

    NOTE: This file is shared between the DSPM and the CP

*****************************************************************************/

#include "sysdefs.h"
#include "dspiparm.h"
#include "dspiapi.h"
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#include "hwdrfapi.h"
#include "hwdmipidefs.h"
#endif
#if defined(MTK_CBP)&&(!defined(MTK_PLT_ON_PC))&&(!defined(MTK_PLT_RF_ORIONC)||defined(MTK_DEV_HW_SIM_RF))
#include "hwdrxdfe.h"
#endif
#define DSPM_COMPATIBILITY_ID       10

/* Q value for DB parms */
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define  IPC_DB_Q                        5
#else
#define  IPC_DB_Q                        6
#endif

/* DspM initialization type */
#define SYS_INIT_COLD 0   /* initialize on powerup */
#define SYS_INIT_WARM 1   /* initialize after runtime */

#define IPC_DSPM_MBOX_SIZE         1268

/*used for AFLT and GPS measure*/
#define TIMING_QUERY_SEQ_GPS 0   
#define TIMING_QUERY_SEQ_AFLT 1  



/*---------------------------------------------------------------------------*
 +-----------------------------------------------------+ HWD_MBM_DAT_TX_BASE (used by CP)
 |                                                     |
 |                                                     |
 | CP - > DM Control Mbox        339                   |    IPC_CTL_MBOX_C2M_SIZE
 |                                                     |
 |                                                     |
 +-----------------------------------------------------+
 | CP - > DM Direct Buf Mbox     429                   |    IPC_DTA_MBOX_C2M_SIZE
 |           Header                   5                |       IPC_DTA_MBOX_C2M_CH0_SIZE
 |           DB 0                    17                |       IPC_DTA_MBOX_C2M_CH1_SIZE
 |           DB 1                    17                |       IPC_DTA_MBOX_C2M_CH2_SIZE
 |           DB 2                   390                |
 +-----------------------------------------------------+ HWD_MBM_DAT_RX_BASE (used by CP)
 |                                                     |
 |                                                     |
 | DM - > CP Control Mbox        334                   |    IPC_CTL_MBOX_M2C_SIZE
 |                                                     |
 |                                                     |
 +-----------------------------------------------------+
 | DM - > CP Direct Buf Mbox     102                   |    IPC_DTA_MBOX_M2C_SIZE
 |           Header                   8                |       IPC_DTA_MBOX_M2C_SIGBUF_SIZE
 |           DB 0                    17                |       IPC_DTA_MBOX_M2C_CH0_SIZE
 |           DB 1                    30                |       IPC_DTA_MBOX_M2C_CH1_SIZE
 |           DB 2                    47                |       IPC_DTA_MBOX_M2C_CH2_SIZE
 +-----------------------------------------------------+
 |                                                     |
 | DM - > CP Fast Mbox            64                   |
 |                                                     | IPC_CTL_MBOX_M2C_DAGC_OFFSET
 |           DAGC (DCR)              16                |    IPC_CTL_MBOX_DSPM_DAGC_SIZE
 |                                                     | IPC_CTL_MBOX_M2C_SMART_ANT_ADDR_OFFSET
 |           Smart Ant               48                |    IPC_CTL_MBOX_M2C_SMART_ANT_SIZE
 +-----------------------------------------------------+
   TOTAL                        1268
 *---------------------------------------------------------------------------*/


/*==================================================================*/
/* DSPM MAILBOXES                                                   */
/*==================================================================*/

/*-------------------------------**
** Data Mailbox Sizes And Macros **
**-------------------------------*/

/* Forward channel signaling buffer size in uint16 */
#define IPC_DTA_MBOX_M2C_SIGBUF_SIZE  8

/* Forward channel modem channel buffer sizes in uint16 */
#define IPC_DTA_MBOX_M2C_CH0_SIZE     17
#define IPC_DTA_MBOX_M2C_CH1_SIZE     30
#define IPC_DTA_MBOX_M2C_CH2_SIZE     47

/* Forward channel data mailbox size in uint16 */
#define IPC_DTA_MBOX_M2C_SIZE (IPC_DTA_MBOX_M2C_SIGBUF_SIZE + IPC_DTA_MBOX_M2C_CH0_SIZE + IPC_DTA_MBOX_M2C_CH1_SIZE + IPC_DTA_MBOX_M2C_CH2_SIZE)
/*                            ( 8 + 17 + 30 + 47 = 102 ) */

/* Reverse channel signaling buffer size in uint16 */
#define IPC_DTA_MBOX_C2M_SIGBUF_SIZE  5

/* Reverse channel modem channel buffer sizes in uint16 */
#define IPC_DTA_MBOX_C2M_CH0_SIZE     17
#define IPC_DTA_MBOX_C2M_CH1_SIZE     17
#define IPC_DTA_MBOX_C2M_CH2_SIZE     390

/* Reverse channel data mailbox size in uint16 */
#define IPC_DTA_MBOX_C2M_SIZE (IPC_DTA_MBOX_C2M_SIGBUF_SIZE + IPC_DTA_MBOX_C2M_CH0_SIZE + IPC_DTA_MBOX_C2M_CH1_SIZE + IPC_DTA_MBOX_C2M_CH2_SIZE)
/*                            ( 5 + 17 + 17 + 390 = 429 ) */

/*-------------------------------------**
** Forward channel data mailbox macros **
**-------------------------------------*/

/* Macro to return the forward channel signaling buffer address */
extern const void *IpcFwdSigBufAddr;
#define IPC_FWD_SIG_BUF_ADDR IpcFwdSigBufAddr;

/*---------------------------------------------------------------**
** Macro to return the modem channel number based on the forward **
** physical channel. The parameter must be of type FwdChnlTypeT  **
**---------------------------------------------------------------*/
extern uint16 IpcFwdPhysChanToMdmChan[];
#define IPC_FWD_PHYS_CHAN_TO_MDM_CHAN(FwdPhysChan) IpcFwdPhysChanToMdmChan[FwdPhysChan];

/*---------------------------------------------------------------**
** Macro to return the modem channel address based on the forward**
** modem channel number. The parameter must be a forward modem   **
** channel number.                                               **
**---------------------------------------------------------------*/
extern void *IpcFwdMdmChanToAddr[];
#define IPC_FWD_MDM_CHAN_TO_ADDR(MdmChan) IpcFwdMdmChanToAddr[MdmChan];

/*-------------------------------------**
** Reverse channel data mailbox macros **
**-------------------------------------*/

/* Macro to return the reverse channel signaling buffer address */
extern const void *IpcRevSigBufAddr;
#define IPC_REV_SIG_BUF_ADDR IpcRevSigBufAddr;

/*---------------------------------------------------------------**
** Macro to return the modem channel number based on the reverse **
** physical channel. The parameter must be of type RChnlTypeT    **
**---------------------------------------------------------------*/
extern uint16 IpcRevPhysChanToMdmChan[];
#define IPC_REV_PHYS_CHAN_TO_MDM_CHAN(RPhysChan) IpcRevPhysChanToMdmChan[RPhysChan]

/*---------------------------------------------------------------**
** Macro to return the modem channel address based on the reverse**
** modem channel number. The parameter must be a reverse modem   **
** channel number.                                               **
**---------------------------------------------------------------*/
extern void *IpcRevMdmChanToAddr[];
#define IPC_REV_MDM_CHAN_TO_ADDR(MdmChan) IpcRevMdmChanToAddr[MdmChan]

/*---------------------------------------------------------------**
** Macro to return the DSPm forward mailbox partition address    **
** The partitions are:                                           **
**  control mailbox partition                                    **
**  dagc mailbox partition                                       **
**  smart mailbox antenna partition                              **
**---------------------------------------------------------------*/
extern void *IpcFwdCtrlMboxAddr[];
#define IPC_FWD_MDM_MBOX_ADDR(MboxPartition) IpcFwdCtrlMboxAddr[MboxPartition]

typedef enum
{
   IPC_FWD_MDM_CTRL,
   IPC_FWD_MDM_DAGC,
   IPC_FWD_MDM_SMART_ANTENNA
}FwdMboxPartitionTypeT;

/*-----------------------------------**
** Control Mailbox Sizes and Offsets **
**-----------------------------------*/

/* Size of dspm rev control mailbox in uint16 */
#define IPC_CTL_MBOX_C2M_SIZE     339

/* Size of dspm fwd control mailbox in uint16 */
#define IPC_CTL_MBOX_M2C_SIZE     334

/* Size of dspm fwd smart antenna mailbox in uint16 */
#define IPC_CTL_MBOX_M2C_SMART_ANT_SIZE    48

/* Size of dspm fwd dagc mailbox in uint16 */
#define IPC_CTL_MBOX_DSPM_DAGC_SIZE             16

/* Total size of dspm fwd control mailboxes */
#define IPC_CTL_MBOX_M2C_TOTAL_SIZE (IPC_CTL_MBOX_M2C_SIZE)
/*                                       ( 300 ) */

/* Define dspm fwd control mailbox offset */
#define IPC_CTL_MBOX_M2C_OFFSET    0
/* Define dspm fwd dagc mailbox offset */
#define IPC_CTL_MBOX_M2C_DAGC_OFFSET (IPC_CTL_MBOX_M2C_OFFSET + \
                                           IPC_CTL_MBOX_M2C_SIZE +   \
                                           IPC_DTA_MBOX_M2C_SIZE)

/* Define dspm fwd smart antenna mailbox offset */
#define IPC_CTL_MBOX_M2C_SMART_ANT_ADDR_OFFSET (IPC_CTL_MBOX_M2C_DAGC_OFFSET + \
                                                     IPC_CTL_MBOX_DSPM_DAGC_SIZE)


/* Maximum dspm fwd control mailbox message size (minus mailbox header and message header sizes) in uint16 */
#define IPC_MSG_DSPM_FWD_MAX_SIZE (IPC_CTL_MBOX_M2C_SIZE - IPC_MBOX_HEADER_SIZE  - IPC_MSG_HEADER_SIZE)
/* Maximum dspm rev control mailbox message size (minus mailbox header and message header sizes) in uint16 */
#define IPC_MSG_DSPM_REV_MAX_SIZE (IPC_CTL_MBOX_C2M_SIZE - IPC_MBOX_HEADER_SIZE  - IPC_MSG_HEADER_SIZE)



/*---------------------------------------------------------------**
** Control Mailbox Message Ids: Control Processor to DSPM        **
**---------------------------------------------------------------*/
typedef enum
{
   /* Forward Task Messages */
   IPC_DSM_FWD_CHNL_CFG_MSG                      = IPC_DSM_FWD_MIN_MSG,
   IPC_DSM_FWD_RDA_H_METRICS_CFG_MSG,
   IPC_DSM_FWD_RDA_P_METRICS_CFG_MSG,
   IPC_DSM_FWD_RESERVE1_MSG,
   IPC_DSM_FWD_CHNL_PC_MSG,
   IPC_DSM_FWD_CHNL_SCRAMBLE_MSG,
   IPC_DSM_FWD_INIT_FWM_PARMS_MSG,
   IPC_DSM_FWD_FNGR_NOISE_NORM_MSG,  /* This is a Parms ReCfg message sent down at MMI power down */
   IPC_DSM_FWD_GPS_FNGR_TT_DISABLE_MSG,


   IPC_DSM_FWD_FPC_PARM_MSG      = 0x140,
   IPC_DSM_FWD_OUTERLOOP_REQ_MSG,
   IPC_DSM_FWD_FPC_ALG_PARM_MSG,

   IPC_DSM_FPDCHS_PARAM_MSG      = 0x180, /* Start of FPDCH message */    
   IPC_DSM_FPDCHS_CQI_ACK_MSG,
   IPC_DSM_FPDCHS_DISABLE_MSG,
   IPC_DSM_FPDCHS_CNTRLHOLD_MSG,
   IPC_DSM_FPDCHS_DBG_MSG,
   IPC_DSM_FWD_RATE_PWR_CHG_MSG,
   IPC_DSM_FPDCHS_SIM_FINGER_CMD,
   IPC_DSM_FPDCHS_SIM_QAMSCALE_CMD,
   IPC_DSM_FPDCHS_SIM_TDITER_CMD,
   IPC_DSM_FPDCHS_SIM_CLR_STATS_CMD,
   IPC_DSM_FPDCHS_SIM_BYPASS_SCH_CMD,
   IPC_DSM_FPDCHS_SIM_BYPASS_REV_CMD,
   IPC_DSM_FPDCHS_SIM_MACID0_DISABLE_CMD,
   IPC_DSM_FPDCHS_SIM_FIX_CQIVAL_CMD,
   IPC_DSM_FPDCHS_SIM_FIX_ACKNAK_CMD,
   IPC_DSM_FPDCHS_SIM_CELL_SWITCH_CMD,
   IPC_DSM_FPDCHS_SIM_CELL_SWITCH_THRESH,
   IPC_DSM_FPDCHS_SIM_PDCH_VALID_CHK_CMD,
   IPC_DSM_FPDCHS_SIM_LWCI_CHK_CMD,
   IPC_DSM_FPDCHS_SIM_LC_DISABLE_CMD,
   IPC_DSM_FPDCHS_SIM_LCPATTERN_CMD,
   IPC_DSM_FPDCHS_SIM_PDCCH_SCRAM_BIT_REV_OFF,

   /* Reverse Task Messages */
   IPC_DSM_REV_CHNL_CFG_MSG                      = IPC_DSM_REV_MIN_MSG,
   IPC_DSM_REV_PREAMBLE_CFG_MSG,
   IPC_DSM_REV_MIXSIG_PARM_MSG,
   IPC_DSM_REV_TX_ACK_NAK_MSG,
   IPC_DSM_REV_TAS_DPDT_MSG,

   /* RF Control Task Messages */
   IPC_DSM_AFC_PARM_MSG                          = IPC_DSM_RFC_MIN_MSG,  /*0x300*/
   IPC_DSM_AFC_CAL_PARM_MSG,      /*0x301*/
   IPC_DSM_AFC_CONFIG_MSG,        /*0x302*/
   IPC_DSM_DCB_PARM_MSG,          /*0x303*/
   IPC_DSM_DCB_CONFIG_MSG,        /*0x304*/
   IPC_DCB_GAINBAL_BITSEL_MSG,    /*0x305*/
   IPC_DSM_RX_CAL_MEASURE_MSG,    /*0x306*/
   IPC_DSM_RX_CAL_OVERRIDE_MSG,   /*0x307*/
   IPC_DSM_RX_AGC_GET_MSG,        /*0x308*/
   IPC_DSM_RXAGC_CONFIG_MSG,      /*0x309*/
   IPC_DSM_RX_CAL_PARM_MSG,       /*0x30A*/
   IPC_DSM_AGC_PARM_MSG,          /*0x30B*/
   IPC_DSM_TX_CAL_PARM_MSG,       /*0x30C*/
   IPC_DSM_TXAGC_PARM_MSG,        /*0x30D*/
   IPC_DSM_TXAGC_CAL_PARM_MSG,    /*0x30E*/
   IPC_DSM_TX_AGC_GET_MSG,        /*0x30F*/
   IPC_DSM_TXAGC_CONFIG_MSG,      /*0x310*/
   IPC_DSM_AFC_GET_PARMS_MSG,     /*0x311*/
   IPC_DSM_RFON_TABLE,            /*0x312*/
   IPC_DSM_TXON_TABLE,            /*0x313*/
   IPC_DSM_RXAGC_SETTLE_TIME,     /*0x314*/
   IPC_DSM_RXAGC_RX_PATH,         /*0x315*/
   IPC_DSM_TXAGC_CR_STATUS,       /*0x316*/
   IPC_DSM_DCB_FAST_DECIM,        /*0x317*/
   IPC_DSM_TXAGC_APT_CTRL,        /*0x318*/
   IPC_DSM_MAG_SMRT_ANT_MODE_MSG, /* Support  Smart Antenna */ /*0x319*/
   IPC_DSM_RX_SD_PARMS_CFG,       /*0x31A*/
   IPC_DSM_RX_DAGC_PARMS_CFG,     /*0x31B*/
   IPC_DSM_RX_DAGC_GAIN_STATE,    /*0x31C*/
   IPC_DSM_DCB_IIR_PARM_MSG,      /*0x31D*/
   IPC_DSM_PDM_CTRL_MSG_MSG,      /*0x31E*/
   IPC_DSM_RSSI_CORRECTION_TABLE_MSG,    /*0x31F*/
   IPC_DSM_TX_DAGC_SPI_GAINS_MSG,        /*0x320*/
   IPC_DSM_TX_DAGC_SPI_MSG,              /*0x321*/
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   IPC_DSM_TXAGC_CALIB_PARM_MSG,         /*0x322*/
   IPC_DSM_TXAGC_TXPGA_TABLE_MSG,        /*0x323*/
   IPC_DSM_TXAGC_COMP_PARM_MSG,          /*0x324*/
   IPC_DSM_TXAGC_CALC_PARM_MSG,          /*0x325*/
   IPC_DSM_TEMP_START_MSG,               /*0x326*/
   IPC_DSM_GSM_STATE_MSG,                /*0x327*/
   IPC_DSM_AFC_CALIB_PARM_MSG,           /*0x328*/
   IPC_DSM_TXON_TABLE_MSG,               /*0x329*/
   IPC_DSM_RXAGC_DCOC_PARMS_MSG,         /*0x32A*/
   IPC_DSM_RXAGC_COMP_PARM_MSG,          /*0x32B*/
   IPC_DSM_RF_TEST_MSG,                  /*0x32C*/
   IPC_DSM_RF_CUST_PARM_MSG,             /*0x32D*/
   IPC_DSM_GSM_BLOCK_TX_STATUS_MSG,      /*0x32E*/
   IPC_DSM_RX_PATH_CTRL_MSG,             /*0x32F*/
#if (!defined(MTK_PLT_RF_ORIONC)||defined(MTK_DEV_HW_SIM_RF))
   IPC_DSM_TX_POC_PARM_MSG,              /*0x330*/
   IPC_DSM_DET_POC_PARM_MSG,             /*0x331*/
#ifdef MTK_DEV_HW_SIM_RF
   IPC_DSM_TXAGC_TXPGA_TABLE_SIM_MSG,
   IPC_DSM_RXAGC_DCOC_PARMS_SIM_MSG,
#endif
#endif
#if (SYS_BOARD >= SB_JADE)
   IPC_DSM_CLOCK_MODE_PARM_MSG,
#endif
#endif
   IPC_DSM_RX_POWER_MODE_CONFIG_MSG, 
#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
   IPC_DSM_TX_POWER_SAR_OFFSET_MSG,
#endif

#ifdef  SYS_OPTION_HPUE_ENABLED
   IPC_DSM_TXON_HPUE_TABLE_MSG                   = IPC_DSM_MSC_MIN_MSG-2,    
#endif

#if defined (IS_C2K_ET_FEATURE_SUPPORT)   
   IPC_DSM_TX_ETM_TABLE_MSG                      = IPC_DSM_MSC_MIN_MSG-1,//IPC_DSM_TX_ETM_TABLE_MSG MSGID should be IPC_DSM_MSC_MIN_MSG-1
#endif   

 

   /* Miscellaneous Task Messages */
   IPC_DSM_MDM_RST_MSG                           = IPC_DSM_MSC_MIN_MSG,
   IPC_DSM_SYNC_SYS_TIME_MSG,
   IPC_DSM_WAKE_ACQ_SYNC_MSG,
   IPC_DSM_WAKE_STATES_MSG,
   IPC_DSM_MSC_RESERVE1_MSG, /* IPC_DSM_FPC_PARM_MSG, */
   IPC_DSM_SLOTTED_MODE_MSG,
   IPC_DSM_SIM_MSG,
   IPC_DSM_MSC_PARM_MSG,
   IPC_DSM_MSC_RESERVE2_MSG, /* IPC_DSM_OUTERLOOP_REQ_MSG,*/
   IPC_DSM_MSC_RESERVE3_MSG, /* IPC_DSM_FPC_ALG_PARM_MSG, */
   IPC_DSM_CAND_FREQ_START_MSG,
   IPC_DSM_RF_RELEASE_REQ_MSG,
   IPC_DSM_NULL_GPM_POWER_SAVE_MSG, /*NULLGPM_POWERSAVING*/
   IPC_DSM_MDM_INIT_REG_MSG,   
   #if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC)) && defined(MTK_DEV_DUMP_REG)
   IPC_DSM_HWD_DBG_MSG, /*Used for DSPM HWD debug--Register Log function */
   IPC_DSP_REG_SET_MSG,
   IPC_DSP_REG_SPY_MSG,  
   IPC_DSP_REG_SPY_BLOCK_MSG,
   #endif
#if defined(SYS_OPTION_PSO_ENABLED) && defined(MTK_CBP)
   IPC_DSM_UNMASK_SYMINT_MSG,
   IPC_DSM_MASK_SYMINT_MSG,
#endif

   /* High Task Messages */
   IPC_DSM_HGH_RESERVE1_MSG                      = IPC_DSM_HGH_MIN_MSG,

   /* Searcher Task Messages */
   IPC_DSM_PILOT_ACQUIRE_MSG                     = IPC_DSM_SCH_MIN_MSG,
   IPC_DSM_SEARCH_PARM_MSG,
   IPC_DSM_SEARCH_WINDOW_SIZE_MSG,
   IPC_DSM_ACQ_PARM_MSG,
   IPC_DSM_ACTIVE_CAND_PILOT_SET_MSG,
   IPC_DSM_NEIGHBOR_PILOT_SET_MSG,
   IPC_DSM_TEST_ALG_VALUE_MSG,
   IPC_DSM_AFLT_PILOT_SET_MSG,
   IPC_DSM_AFLT_PILOT_SET_ABORT_MSG,
   IPC_DSM_AFLT_ALG_VALUE_MSG,
   IPC_DSM_QUERY_TIMING_MSG,
   IPC_DSM_TEST_ALG_VALUE1_MSG,
   IPC_DSM_TEST_ALG_VALUE2_MSG,
   IPC_DSM_SEARCH_WINDOW_SIZE_PARM_MSG,
   IPC_DSM_IC_CONTROL_MSG,    
   IPC_DSM_DIVERSITY_CONTROL_MSG,    
   IPC_DSM_SRCH_SAMPLE_PHASE_CONTROL_MSG,
   IPC_DSM_IC_FILTER_MSG
#ifdef  MTK_CBP_SYNC_OPTIMIZE   
   ,IPC_DSM_MINI_ACQUIRE_MSG
#endif
#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE)
   ,IPC_DSM_ICS_RSSI_SCAN_MSG
#endif

#if defined(MTK_DEV_C2K_SRLTE_L1)&&defined(MTK_PLT_ON_PC)
   ,IPC_L1D_TEST_CASE_MSG
#endif          
} IpcDspmMsgIdT;




/*---------------------------------------------------------------**
** Control Mailbox Message Ids: DSP to Control Processor (_CP_)  **
**---------------------------------------------------------------*/


typedef enum
{

   /* DSPM to CP Messages */
   IPC_CP_RX_POWER_MSG                       =IPC_CP_FROM_DSM_MIN_MSG,
   IPC_CP_DC_BIAS_MSG,
   IPC_CP_FWD_CHNL_DTA_MSG,
   IPC_CP_PILOT_PWR_MSG,
   IPC_CP_QUICK_PAGE_BITS_MSG,
   IPC_CP_SEARCH_RSLT_MSG,
   IPC_CP_SLEEP_RDY_MSG,
   IPC_CP_SPY_ACQ_MSG,
   IPC_CP_SPY_AFC_MSG,
   IPC_CP_SPY_AGC_MSG,
   IPC_CP_SPY_DCB_MSG,
   IPC_CP_SPY_FINGER_ALLOC_MSG,
   IPC_CP_SPY_FINGER_STATE_MSG,
   IPC_CP_SPY_FINGER_TRK_MSG,
   IPC_CP_SPY_FPC_MSG,
   IPC_CP_SPY_PDA_MSG,
   IPC_CP_SPY_PLF_MSG,
   IPC_CP_SPY_RDA_MSG,
   IPC_CP_SPY_RPC_MSG,
   IPC_CP_SPY_SEARCH_MSG,
   IPC_CP_SPY_VIT_MSG,
   IPC_CP_FPC_REP_MSG,
   IPC_CP_FWD_CHNL_PC_RSP_MSG,
   IPC_CP_FWD_CHNL_SCRAMBLE_RSP_MSG,
   IPC_CP_AFLT_MEAS_MSG,
   IPC_CP_TIMING_RSP_MSG,
   IPC_CP_RX_AGC_GET_RSP_MSG,
   IPC_CP_TX_AGC_GET_RSP_MSG,
   IPC_CP_AFC_GET_PARMS_RSP_MSG,
   IPC_CP_TXAGC_CR_STATUS_RSP_MSG,
   IPC_CP_SPAGE_RSP_MSG,
   IPC_CP_MDM_ACK_MSG,
   IPC_CP_NULL_GPM_IND_MSG,/*NULLGPM_POWERSAVING*/
   IPC_CP_MAG_SMRT_ANT_ACK_MSG,
   IPC_CP_RF_RELEASE_RSP_MSG
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   ,IPC_CP_TEMP_REPORT_MSG
#endif
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   ,IPC_CP_DET_TXPWR_REPORT_MSG
#endif
   ,IPC_CP_AFC_REPORT_MSG

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE_L1)
   ,IPC_CP_SEARCH_STATUS_MSG
   ,IPC_CP_SCH_EBNT_MSG
   ,IPC_CP_RSSI_SCAN_REPORT_MSG
#endif
} IpcCpmMsgIdT;


typedef enum
{
   IPC_FWD_FRAME_BLANK       = 0x0,
   IPC_FWD_FRAME_EIGHTH      = 0x1,
   IPC_FWD_FRAME_QUARTER     = 0x2,   /* not used for SSO 3 */
   IPC_FWD_FRAME_HALF        = 0x3,
   IPC_FWD_FRAME_FULL        = 0x4,
   IPC_FWD_FRAME_ERASURE     = 0xE,
   IPC_FWD_FRAME_FULL_LIKELY = 0xF    /* used only for SSO's 1&3 */
} IpcFwdFrameRateT;


typedef enum
{
   IPC_MS_REG_ID_RX, /* Rx Mixed Signal block */
   IPC_MS_REG_ID_TX, /* Rx Mixed Signal block */
   IPC_MS_REG_ID_VC  /* Voice Codec Mixed Signal block */
}IpcMsRegIdT;


/*--------------------------------------------**
** CP to DSPM Forward Task Message Structures **
**--------------------------------------------*/

#define SYS_NUM_FINGERS         8                  /* Number of "normal" Rx fingers. Normal
                                                    * fingers are indexed 0 -> SYS_NUM_FINGERS-1
                                                    */

#define SYS_FINGER_SYSTEM_ID    (SYS_NUM_FINGERS)  /* ID of the Rx/Tx system time finger */
                                                   
#define SYS_MAX_BASE_STATIONS   6                  /* Maximum number of base stations with 
                                                   * which a mobile can be in softhandoff 
                                                   */ 

#define SYS_VOICE_PCKT_DATA_LENGTH 1

#define SYS_LIN_APPROX_SLOPE_Q           7  /* Q of the Slope for the Linear approx of RxGainDb/DAC and DAC/TxPwrDbm */


#define SYS_MAX_CANDIDATE_FREQ_LIST     10 /* ?? guess */
#define SYS_MAX_ACTIVE_LIST_PILOTS       6 /* max number of pilot pn's in acitve list  */
#define SYS_MAX_CANDIDATE_LIST_PILOTS   10 /* max number of pilot pn's in candidate list  */
#ifdef MTK_CBP
#define SYS_MAX_RTT_NEIGHBOR_LIST_PILOTS 44 /* max number of pilot pn's in neighbor list  */
#define SYS_MAX_RTT_AFLT_LIST_PILOTS 128 /* max number of pilot pn's in AFLT list  */
#endif
#define SYS_MAX_NEIGHBOR_LIST_PILOTS    40 /* max number of pilot pn's in neighbor list  */
#ifdef MTK_CBP
#define SYS_MAX_AFLT_LIST_PILOTS         16 /* max number of pilot pn's in neighbor list  */
#else
#define SYS_MAX_AFLT_LIST_PILOTS         5 /* max number of pilot pn's in neighbor list  */
#endif
#define SYS_MAX_PRIORITY_LIST_PILOTS    40 /* max number of pilots in priority list */
#define SYS_MAX_REMAINING_LIST_PILOTS    5 /* max number of pilots in remaining set */
#define L1D_TX_POWER_LEVEL_NUM           5 


/*-------------------------------------------------------------------**
** IpcDsmFwdChnlCfgMsgT                                              **
** This message sets up the channel configuration only. Info specific**
** to basestations comes from the ActiveSetMsg (walsh codes)         **
**                                                                   **
** NOTE: Changes to this list must be accompanied by a corresponding **
**       change to the mapping tables in ipctask.c for both the CP & **
**       DSP code                                                    **
**                                                                   **
**-------------------------------------------------------------------*/
typedef enum
{
   FSYNC,
   FPAGE,
   FBCCH,
   FQPCH,
   FCPCCH,
   FCACH,
   FCCCH,
   FDCCH,
   FFCH,   /* also for traffic channel in IS95 FTCH */
   FSCH,
   F_MON_CHNL,
   FPDCH,
   FWD_MAX_CHNL_TYPS
} FwdChnlTypeT;

#if ((SYS_ASIC >= SA_CBP80) || ((SYS_ASIC == SA_FPGA) && (SYS_VERSION >= SV_REV_C0)))
typedef enum
{
   RC1,
   RC2,
   RC3,
   RC4,
   RC5,
   RC_RESERVED_1,
   RC_RESERVED_2,
   RC8,
   RC_RESERVED_3,
   RC_RESERVED_4,
   RC11,
   RC_MAX_TYPS
} RadioConfigT;
#define  RC1_TO_RC5     5
#define  RC11X          RC_RESERVED_1
#define  RC1_THRU_RC11X (RC11X - RC1 + 1)

#define  RC_NONE        0xff /* this value is used for channel types where no RC value is assigned */
#else
typedef enum
{
   RC1,
   RC2,
   RC3,
   RC4,
   RC5,
   RC_MAX_TYPS,
   RC_NONE        /* this value is used for channel types where no RC value is assigned */
} RadioConfigT;
#endif

typedef enum
{
   F80MS,
   F40MS,
   F26MS,
   F20MS,
   F10MS,
   F5MS,
   F160MS,
   FWD_MAX_FRM_DURATION
} FrameDurationT;

typedef enum {
   DIV_1,
   DIV_2,
   DIV_4,
   DIV_8
} VitScaleT;

typedef enum {
   NO_ENCODE,
   HALF_RT,
   THIRD_RT,
   QUARTER_RT
} RateT;

typedef enum {
   NO_CRC,
   CRC_6_RC2,
   CRC_6,
   CRC_8,
   CRC_10,
   CRC_12,
   CRC_16
} CrcIdxT;

typedef enum {
   LC_MASK_0,
   LC_MASK_1,
   LC_MASK_DONT_CARE
} LcMaskIdxT;

typedef enum {
   SLOT40MS  = 0,
   SLOT80MS  = 1,
   SLOT160MS = 2
} SlotDurationT;

typedef PACKED_PREFIX struct
{
   uint16         ChnlTyp;     /*  FwdChnlTypeT */
   uint16         NumBits[4];  /* Number of data bits (including reserve bit)
                                  NumBits_Full
                                  NumBits_Half
                                  NumBits_Quarter
                                  NumBits_Eighth
                               */
   uint16         FrameDuration;  /* (FrameDurationT) Frame duration */
   uint16         CrcIdx;    /* Index into CRC table
                                [0:3]   CrcIdx_Full
                                [4:7]   CrcIdx_Half
                                [8:11]  CrcIdx_Quarter
                                [12:15] CrcIdx_Eighth
                             */
   uint16         Rate;      /* Convolution code rate */
   uint16         FrepM;     /* Flexible Repetition at output of convolutional code (derepetition per formed by HARX) */
                             /* [0:3]   Full
                                [4:7]   Half
                                [8:11]  Quarter
                                [12:15] Eighth
                             */
   uint16         PuncIdx;   /* Index into puncture table
                                [0:3]   PuncIdx_Full
                                [4:7]   PuncIdx_Half
                                [8:11]  PuncIdx_Quarter
                                [12:15] PuncIdx_Eighth
                             */
   uint16         IntrlvMJ;  /* IntrlvMJ [0:7] M, [8:11] J */
   uint16         RxaSum;    /* Repetition at output of puncture (derepetition performed by HwRxAccel) */
   uint16         LcMaskIdx; /* Index for Long Code Mask */
   uint16         SymbolLen; /* Symbol length (chips/symbol), redundant but saves calculation
                                (Walsh length = SymbolLen * RxaSum) */
   uint16         VitScale;  /* Viterbi branch metric scale */
    int16         MrcScale;  /* Mrc Fixed scaling parameter */
   uint16         RdaDecAllRates; /* 0: standard Rda, 1: Viterbi Decode 4 times + Aposteriori Rda */
   uint16         FourChnlConfig; /* 0:config Harx for 2 channels D0 and D3, 1:config Harx for 4 channels D0,D1,D2 and D3 */
   uint16         TurboMode;      /* 0:viterbi decoder, 1:turbo decoder */
   uint16         SlotDuration;   /* SlotDurationT, used for FBCCH only */
   uint16         DtxEnabled;/* DtxEnabled = 0 for FSYNC,FPAGE,FFCH. DtxEnabled = 1 for FDCCH, FSCH. */
   uint16         FrameIndex;     /* 40ms frame index (0,1,2,3) used for FBCCH only */
#if ((SYS_ASIC >= SA_CBP80) || ((SYS_ASIC == SA_FPGA) && (SYS_VERSION >= SV_REV_C0)))
   uint16         FfchAckMask;    /* Fwd Early Termination Mask, Rev Ack Chnl Mask */ 
   uint16         EarlyTerminationTest; 
   uint16         FwdBlankingDutyCycles; /* Forward blanking duty cycles */
#endif
} PACKED_POSTFIX  FChnlCfgT;

typedef PACKED_PREFIX struct
{
   uint16       Disable;     /* one bit for each of channel: 0-do nothing, 1-disable */
   uint16       Dest;        /* destination of response message, this applies to all */
                             /* all channels */
   uint16       ChnlNum;     /* channel number: 0, 1, 2, 3 */
   uint16       Change;      /* one bit for each of fields below */
   uint16       RC;          /* Radio Config -- will contain enum type RC_1, etc. (RC_1 = 0) */
   uint16       FrameOffset; /* frame offset in x1.25ms */
   uint16       LcMask0[3];
   uint16       LcMask1[3];
   FChnlCfgT    ChnlCfg;     /* walsh code for channel & pilot in ActiveSetMsg */
} PACKED_POSTFIX  IpcDsmFwdChnlCfgMsgT;

/*-----------------------------------------------------------**
** IpcDsmFwdRdaHmetricsCfgMsgT                               **
** for IPC_DSM_FWD_RDA_H_METRICS_CFG_MSG                     **
** This message reconfigures the Viterib Scale and           **
** Rate Determination Apriori parameters in the Fwd unit.    **
**                                                           **
**-----------------------------------------------------------*/
typedef enum {
   FWD_RDA_RT_EIGHTH,
   FWD_RDA_RT_QUARTER,
   FWD_RDA_RT_HALF,
   FWD_RDA_RT_FULL,
   FWD_RDA_RT_MAX
}  FwdRdaRtT;

typedef enum {

   FE,
   HE,
   QE,
   FQ,
   HQ,
   FH,
   FWD_RDA_T_TBL_SZ
} FwdRdaTtblRC1IdxT;

typedef enum {

   HF,
   QF,
   EF,
   QH,
   EH,
   EQ
} FwdRdaTtblRC2to5IdxT;

#if ((SYS_ASIC >= SA_CBP80) || ((SYS_ASIC == SA_FPGA) && (SYS_VERSION >= SV_REV_C0)))
typedef PACKED_PREFIX struct {
   uint16  FwdVitScale[RC1_TO_RC5][FWD_RDA_RT_MAX];
   uint16  FwdRdaTtbl[RC1_TO_RC5][FWD_RDA_T_TBL_SZ];
} PACKED_POSTFIX   IpcDsmFwdRdaHmetricsCfgMsgT;
#else
typedef PACKED_PREFIX struct {
   uint16  FwdVitScale[RC_MAX_TYPS][FWD_RDA_RT_MAX];
   uint16  FwdRdaTtbl[RC_MAX_TYPS][FWD_RDA_T_TBL_SZ];
} PACKED_POSTFIX   IpcDsmFwdRdaHmetricsCfgMsgT;
#endif


/*-----------------------------------------------------------**
** IpcDsmFwdRdaPmetricsCfgMsgT                               **
** for IPC_DSM_FWD_RDA_P_METRICS_CFG_MSG                     **
** This message reconfigures Rate Determination              **
** Static Bits Length and Aposteriori parameters.            **
**                                                           **
**-----------------------------------------------------------*/
#define FWD_RDA_YAMA_SZ      2
#define FWD_VITMET_RC_SZ     3
#define FWD_VITMET_RT_SZ     5
typedef enum {
   T0_HF,
   T0_QF,
   T0_EF,
   T0_QH,
   T0_EH,
   TP_HF,
   T0_EQ,
   TP_QH,
   TP_QF,
   TP_EQ,
   TP_EH,
   TP_EF,
   FWD_RDA_T0_TBL_SZ
} FwdRdaT0tblIdxT;

#if ((SYS_ASIC >= SA_CBP80) || ((SYS_ASIC == SA_FPGA) && (SYS_VERSION >= SV_REV_C0)))
typedef PACKED_PREFIX struct {
   uint16  FwdRdaChkStaticBitsLen[RC1_TO_RC5];
   uint16  FwdRdaYamatbl[FWD_RDA_YAMA_SZ];
   uint16  FwdRdaT0tbl[RC1_TO_RC5][FWD_RDA_T0_TBL_SZ];
   uint16  FwdRdaSerThres[FWD_RDA_YAMA_SZ];
   uint16  FwdVitMetThresTbl[FWD_VITMET_RC_SZ][FWD_VITMET_RT_SZ];
} PACKED_POSTFIX   IpcDsmFwdRdaPmetricsCfgMsgT;
#else
typedef PACKED_PREFIX struct {
   uint16  FwdRdaChkStaticBitsLen[RC_MAX_TYPS];
   uint16  FwdRdaYamatbl[FWD_RDA_YAMA_SZ];
   uint16  FwdRdaT0tbl[RC_MAX_TYPS][FWD_RDA_T0_TBL_SZ];
   uint16  FwdRdaSerThres[FWD_RDA_YAMA_SZ];
   uint16  FwdVitMetThresTbl[FWD_VITMET_RC_SZ][FWD_VITMET_RT_SZ];
} PACKED_POSTFIX   IpcDsmFwdRdaPmetricsCfgMsgT;
#endif


/*-----------------------------------------------------------**
** IpcDsmFwdInitFwmParmsMsgT for                             **
** IPC_DSM_FWD_INIT_FWM_PARMS_MSG                            **
** This message reconfigures Fixed Scaling parameters.       **
** Received symbols are shifted left by the amount indicated **
** in the Fixed Scaling parameter before input to the        **
** Hardware Accelerator.                                     **
**                                                           **
**-----------------------------------------------------------*/
typedef PACKED_PREFIX struct {
   uint16  FwdPdaScaling[SYS_NUM_CHNL];
} PACKED_POSTFIX   IpcDsmFwdInitFwmParmsMsgT;

/* IpcDsmFwdChnlPcMsgT: allows the forward channel puncture state to be set */
typedef PACKED_PREFIX struct
{
   uint16       Change;      /* When non-zero the Enable field will be transferred to
                                the forward channel scramble state.  Otherwise the
                                Enable field is ignored.
                             */
   uint16       Enable;      /* Set to non-zero to enable and zero to disable.
                             */
} PACKED_POSTFIX  IpcDsmFwdChnlPcMsgT;

/* IpcDsmFwdChnlScrambleMsgT: message allows the forward channel scramble state to be set */
typedef PACKED_PREFIX struct
{
   uint16       Change;      /* When non-zero the Enable field will be transferred to
                                the forward channel scramble state.  Otherwise the
                                Enable field is ignored.
                             */
   uint16       Enable;      /* Set to non-zero to enable and zero to disable.
                             */
} PACKED_POSTFIX  IpcDsmFwdChnlScrambleMsgT;

/* IpcDsmPdaParmMsgT post despread agc (soft scaling) parameters */
typedef PACKED_PREFIX struct
{
   uint16       BlkShift[SYS_NUM_CHNL];
   uint16       Alpha;
   uint16       Q1UpdateConst; /* = 18 - log2((1-Alpha)*Fr/Fact_3) */
} PACKED_POSTFIX  IpcDsmPdaParmMsgT;

/* IpcDsmPlfParmMsgT pilot filter parameters */
typedef PACKED_PREFIX struct
{
   uint16       FirLen;        /* filter length */
   uint16       SymbolDelay;   /* filter delay */
} PACKED_POSTFIX  IpcDsmPlfParmMsgT;

/*-------------------------------------------------------------------**
** IpcDsmFwdFngrNoiseNormMsgT                                        **
** for IPC_DSM_FWD_FNGR_NOISE_NORM_MSG                               **
** This message reconfigures the Finger Noise Norm Parms & Table     **
**                                                                   **
**-------------------------------------------------------------------*/
#define FWD_FNGR_NO_NORM_MULT_TBL_SZ  16
/* IpcDsmFwdFngrNoiseNormMsgT finger noise normalization parameters */
typedef PACKED_PREFIX struct
{
   uint16  FngrNoNormEnable;
   int16   IsrA0;
   int16   IsrB0;
   int16   FngrNoMin;
   uint16  FwdFngrNoNormMultTbl[RC3][FWD_FNGR_NO_NORM_MULT_TBL_SZ];
   uint16  D3LmtEnable;    /* 0: Disable, 1: Enable */
   int16   MonFreq;        /* In (n-1) multiples of 20ms */
   uint16  FngrLimit;      /* Max no. of Sch fingers at high data rate */
   uint16  FrmSzThres;     /* NumBits per frame */
   int16   FngrEcIoFactor; /* Finger EcIo Factor, linear number, Q12 */
} PACKED_POSTFIX   IpcDsmFwdFngrNoiseNormMsgT;


/*-------------------------------------------------------------------**
** IpcDsmFPdchsXXXXXXXMsgT                                           **
**-------------------------------------------------------------------*/
typedef PACKED_PREFIX struct {
   uint16  PilotPn;
   uint16  WalshTableIdAndNumPdcch;    /* [15:8] WalshTableId,   [7:0] NumPdcch */
   uint16  ForPdcchWalsh;              /* [15:8] ForPdcchWalsh0,  [7:0]  ForPdcchWalsh1 */
   uint16  MacIdAndRevCqichCover;      /* [15:8]  MacId,   [7:0] RevCqichCover */
   uint16  PdchGroupIndAndPwrCombInd;  /* [2]  PdchGroupInd,   [1] PwrCombInd, [0] PdchGroupIndIncl */
} PACKED_POSTFIX  IpcDsmFPdchsBaseStationCfgT;

typedef PACKED_PREFIX struct {
   uint16 SysTime;    /* system time at the following 20 ms frame boundary, in 20 ms,  */
   uint16 AckDelayAndNumArqChan;       /* [15:8]  AckDelay,   [7:0] NumArqChan */
   uint16 PilotGatingInfo;             /* [15:8]  PilotGatingRate,   [7:0] PilotGatingUseRate */
   uint16 ForPdchRcAndFullCI;          /* [15:8]  ForPdchRc,   [7:0] FullCIFeedbackInd */
   uint16 RevCqichFrameOffset;
   uint16 RevCqichAckReps;             /* [15:8]  RevCqichReps,   [7:0] RevAckchReps */
   uint16 NumSwitchingFrames;          /* [15:8]  NumSoftSwitchingFrames, [7:0] NumSofterSwitchingFrames */
   uint16 NumSwitchingFramesChm;       /* [15:8]  NumSoftSwitchingFramesChm, [7:0] NumSofterSwitchingFramesChm */
   uint16 NumSwitchingSlot;            /* [15:8]  NumSoftSwitchingSlot,   [7:0] NumSofterSwitchingSlot */
   uint16 NumSwitchingSlotChm;         /* [15:8]  NumSoftSwitchingSlotChm,   [7:0] NumSofterSwitchingSlotChm */
   uint16 PdchSwitchingDelay;          /* [15:8]  PdchSoftSwitchingDelay,   [7:0] PdchSofterSwitchingDelay */
   uint16 NumPilots;
   IpcDsmFPdchsBaseStationCfgT BaseStationCfg[7];

} PACKED_POSTFIX  IpcDsmFPdchsParamCfgMsgT;

typedef PACKED_PREFIX struct {
   uint16 Disable;                     /* 1 - Disable. */
} PACKED_POSTFIX  IpcDsmFPdchsDisableMsgT;

typedef PACKED_PREFIX struct {
    uint16 FullCIFeedbackInd;
    uint16 RevCqichAckReps;        /* [15:8]  RevCqichReps,   [7:0] RevAckchReps */
    uint16 NumSwitchingFrames;     /* [15:8]  NumSoftSwitchingFrames, [7:0] NumSofterSwitchingFrames */
    uint16 NumSwitchingFramesChm;  /* [15:8] NumSoftSwitchingFramesChm, [7:0] NumSofterSwitchingFramesChm */
    uint16 RevAckchPwr;       /* Linear value, 0x40 as 0 dB */
    uint16 RevCqichHighPwr;   /* Linear value, 0x40 as 0 dB */
    uint16 RevCqichLowPwr;    /* Linear value, 0x40 as 0 dB */
} PACKED_POSTFIX  IpcDsmFPdchsCqiAckMsgT;

typedef PACKED_PREFIX struct {
    uint16 PilotGatingInfo;       /* [15:8]  PilotGatingRate,   [7:0] PilotGatingUseRate */
    uint16 NumSwitchingFramesChm; /* [15:8]  NumSoftSwitchingFramesChm, [7:0] NumSofterSwitchingFramesChm */
} PACKED_POSTFIX  IpcDsmFPdchsCntrlHoldMsgT;


/*-------------------------------------------------------------------**
** IpcDsmFwdRatePwrChgMsgT                                           **
** for IPC_DSM_FWD_RATE_PWR_CHG_MSG                                  **
** This message does...                                              **
**                                                                   **
**-------------------------------------------------------------------*/

typedef PACKED_PREFIX struct {
   uint16 FullCIFeedbackInd;
   uint16 RevCqichAckReps;          /* [15:8] RevCqichReps, [7:0] RevAckchReps */
   uint16 NumSwitchingFrames;       /* [15:8] NumSoftSwitchingFrames, [7:0] NumSofterSwitchingFrames */
   uint16 NumSwitchingFramesChm;    /* [15:8] NumSoftSwitchingFramesChm, [7:0] NumSofterSwitchingFramesChm */
   uint16 RevAckchPwr;              /* Linear value, 0x40 as 0 dB */
   uint16 RevCqichHighPwr;          /* Linear value, 0x40 as 0 dB */
   uint16 RevCqichLowPwr;           /* Linear value, 0x40 as 0 dB */
} PACKED_POSTFIX  IpcDsmFwdRatePwrChgMsgT;


/*------------------------------------------------**
** End CP to DSPM Forward Task Message Structures **
**------------------------------------------------*/

/*--------------------------------------------**
** CP to DSPM Reverse Task Message Structures **
**--------------------------------------------*/

/* IpcDsmRevChnlCfgMsgT */

/*
**                                                                   **
** NOTE: Changes to this list must be accompanied by a corresponding **
**       change to the mapping tables in ipctask.c for both the CP & **
**       DSP code                                                    **
**                                                                   **
*/
typedef enum
{
   RFCH,     /* variable rate */
   RDCCH,
   RSCH,
   RACCESS,
   RHEACH,
   REACH,
   RCCCH,
   R_MON_CHNL
} RChnlTypeT;

typedef PACKED_PREFIX struct
{
   uint16         ChnlTyp;     /* (RChnlTypeT) channel type */
   uint16         NumBits[4];  /* Number of data bits (including reserve bit)
                                    NumBits_Full
                                    NumBits_Half
                                    NumBits_Quarter
                                    NumBits_Eighth
                               */
   uint16         FrameDuration;  /* (FrameDurationT) Frame duration */
   uint16         CrcIdx;      /* Index into CRC table
                                  [0:3]   CrcIdx_Full
                                  [4:7]   CrcIdx_Half
                                  [8:11]  CrcIdx_Quarter
                                  [12:15] CrcIdx_Eighth
                               */
   uint16         Rate;        /* bit8 - Turbo/Conv, [0:7]=code rate */
   uint16         Rep1;        /* Repetition at output of convolutional code. */
                               /* If normal repitition (see IntrlvMJ[15])
                                   [0:3]   Rep1_Full    M-1  for all channels
                                  for RFCH
                                   [4:7]   Rep1_Half    M-1
                                   [8:11]  Rep1_Quarter M-1
                                   [12:15] Rep1_Eighth  M-1
                                  else
                                    [14:0] - repetition, in flexible rep this is 'M-1'
                               */
   uint16         PuncIdx;     /* Index into puncture table
                                  [0:3]   PuncIdx_Full
                                  [4:7]   PuncIdx_Half
                                  [8:11]  PuncIdx_Quarter
                                  [12:15] PuncIdx_Eighth
                               */
   uint16         IntrlvMJ;    /* [0:7] m, [8:10] J0, [11:13] J1, J=2^J0 + 2^J1 J parameter
                                  [15] - 0=normal, 1=flexible repetition   */
   uint16         ChipOffset;  /* frame offset in chip increments (for ACCESS & EACH) */
                               /* note EACH delay is in increments of 1.25 ms */
} PACKED_POSTFIX  RChnlCfgT;


typedef PACKED_PREFIX struct
{
   uint16         PilotInfo;    /* [0:3] 0-full, 1-1/2, 2-1/4; [4] 1=enable pilot, 0=disable pilot */
                                /* [8] 1 -> subtract mean input power from OpenLoopPwrCorr */
                                /* probably not used. only need to be within 500 ms. */
   uint16         OpenLoopPwrCorr; /* Open Loop Tx power correction from PE in dB Q6 */
} PACKED_POSTFIX  RPilotCfgT;

typedef PACKED_PREFIX struct
{
   uint16         Disable;        /* one bit for each of channel: 0-do nothing, 1-disable */
   uint16         ChnlNum;        /* physical channel number: 0, 1, 2  */
   uint16         Change;         /* one bit for each of fields below */
   uint16         RC;             /* Radio Config -- use RC_NONE enum value for channels for which RC does not apply */
   uint16         LcMask[3];      /* TX long code mask */
   RPilotCfgT     PilotCfg;       /* pilot config */
   RChnlCfgT      ChnlCfg;        /* channel config */
   uint16         ChnlWalsh;      /* [0:1] walsh cover */
   uint16         ChTxPwrToPilot[4];   /* channel tx power relative to pilot in linear Q6*/
                                  /* FCH: Full, Half, Quarter, Eighth rate (if variable rate) */
                                  /* or SCH:  the 3 rates for supplemental channel. */
                                  /* or DCCH: Full rate */
                                  /* ChTxPwrToPilot[0] = 1 when no pilot, else = pwr relative to pilot */
   uint16         RevFchGatPwrCntlStep; /* [0:1] RevPwrCntlDelay; [7] RevFchGatingMode  [8:9:10] PwrCntlStep */
#ifdef MTK_CBP
   uint16         OLPwrBackOff;  /* Open Loop Tx power BackOff for Vzw in dB Q6 */    
#endif
#if ((SYS_ASIC >= SA_CBP80) || ((SYS_ASIC == SA_FPGA) && (SYS_VERSION >= SV_REV_C0)))
   uint16         FfchAckMask;    /* Fwd Early Termination Mask, Rev Ack Chnl Mask */
   uint16         RfchAckMask;    /* Fwd Ack Chnl Mask, Rev Early Termination Mask */
   uint16         AckScale[2];    /* AckScale[0] for ACK 1 channel scale; AckScale[1] for ACK2 channel scale */
   uint16         RevBlankingDutyCycles; /* Reverse blanking duty cycles */
   uint16         Rc8RpcMode;     /* RPC mode for RC8 only */
#endif
} PACKED_POSTFIX  IpcDsmRevChnlCfgMsgT;

/*------------------------------------------------------------------**
** IpcDsmRevChnlDtaMsgT                                             **
** Data actually in shared memory. This is an indicator to tell the **
** dsp that the data is ready.                                      **
**------------------------------------------------------------------*/

typedef enum                      /* Rev fch rate */
{
  R_FULL = 0,
  R_HALF,
  R_QUARTER,
  R_EIGHTH,
  R_ZERO
} RevFundRateTypeT;

typedef PACKED_PREFIX struct
{
   uint16       ChnlDataRdy;  /* bit field indicating channels are ready */
   uint16       FundRate;     /* fund chnl rate if assigned */
   #if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   uint16       VoiceFlag;    /* indicate voice service or not*/
   #endif 
} PACKED_POSTFIX  RevChnlDtaMsgT;

/* IpcDsmRevPreambleCfgMsgT */
typedef PACKED_PREFIX struct
{
   uint16       Enable;               /* enable, disable */
   uint16       MeanOutputPwr;
   uint16       UseCurrMip;           /* flag indicates whether to use current Mean Input Power */
   uint16       Type;                 /* 0-Pilot, 1-use parms below,
                                         2-Rc1&2 Preamble */
   uint16       StartSymbol;          /* [15]-Next20msFrame, [8:0]-SymbolCount */
   uint16       PreambleNumFrac;
   uint16       PreambleFracDuration;
   uint16       PreambleOffDuration;
   uint16       PreambleAddDuration;
} PACKED_POSTFIX  IpcDsmRevPreambleCfgMsgT;
/*
  StartSymbol(0:383)- Is the SymbolCount in 20ms Frames of when the preamble should
  start. When Next20msFrame bit is set the preamble starts at SymbolCount in the
  following 20ms frame.
*/

/* IpcDsmRevMixSigParmMsgT */
typedef PACKED_PREFIX struct
{
   uint16       RevMixSigParm[16];  /* GainComp1,2, GainSel, ClipLvl,ClipMaxthres,ClipMaxSigmaX,ClipSigmaP,KsThres[9] */
} PACKED_POSTFIX  IpcDsmRevMixSigParmMsgT;

typedef PACKED_PREFIX struct
{
   uint16         AckNakControl; /* When the base station sends a Service Option Control Message
                                  * to invoke control action for R-ACKCH1 in a mobile, it shall
                                  * include the Type-specific fields as specified in Table 2.6.4.3-1.
                                  * If this filed is '0', the mobile station is to transmit ACK or NAK
                                  * in accordance with the R-ACKCH1 procedure specified in c.s0002.
                                  * If this filed is '1', the mobile station is to transmit ACK every
                                  * frame at the PCG corresponding to the 1 in the ACK mask.
                                  * If this filed is '2', the mobile station is to transmit NAK every
                                  * frame at the PCG corresponding to the 1 in the ACK mask. */  
} PACKED_POSTFIX IpcDsmTxAckNakMsgT;

/* IpcDsmRevTasDpdtParmMsgT */
typedef PACKED_PREFIX struct
{
   uint16       TasDpdtEn;
   int16        TasDpdtPwr;
} PACKED_POSTFIX  IpcDsmRevTasDpdtParmMsgT;


/*------------------------------------------------**
** End CP to DSPM Reverse Task Message Structures **
**------------------------------------------------*/

/*-----------------------------------------------**
** CP to DSPM RF Control Task Message Structures **
**-----------------------------------------------*/
typedef enum
{
   IPC_AUTOMATIC=0,
   IPC_DISABLE,
   IPC_MANUAL,    /*Fix TxPower and PA info*/
   IPC_FIX_POWER  /*Fix TxPower*/
}ControlModeEnum;

typedef enum
{
  /* CAUTION: Elements in DspmRfcModeEnum and ControlModeEnum must be lined up in the same order */
   RFC_AUTOMATIC=0,  /* IPC_AUTOMATIC */
   RFC_DISABLE,      /* IPC_DISABLE   */
   RFC_MANUAL,       /* IPC_MANUAL    */
   RESUME_AUTOMATIC, /* CDMA          */
   SAVE_DISABLE,
   RESTORE_DISABLE,
   RESTORE_AUTOMATIC,
   GPS_MODE_ENABLE,
   GPS_MODE_DISABLE,
   RFC_INIT_VALUE_SET
} DspmRfcModeEnum;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_RF_TXPGA_GAIN_TABLE_SIZE   64  /**max support 64 stages with MTK solution*/
#define HWD_RF_DET_GAIN_TABLE_SIZE     32  /**max support 32 stages with MTK solution*/
#endif

typedef  PACKED_PREFIX   union {
    uint16      EnumFormat;   /*Of the type ControlModeEnum */
    uint16      WordFormat;
} PACKED_POSTFIX ControlModeT;

/*--------------------------------------------------------------------------------**
** IpcDsmAfcParmMsgT afc parameters                                               **
** Afc only performed on receiving frequency when fingers are assigned. Afc not   **
** performed during candidate freq search. So only one PpmPerHz value is required.**
** This message needs to be sent for each frequency band to change PpmPerHz.      **
**--------------------------------------------------------------------------------*/
typedef PACKED_PREFIX struct    /* FQQ */
{
   uint16       Decim;        /* samples btw phase calc */
   uint16       DecimMsg;     /* #phase sums */
   int16        Shift;
   int16        CorrGain;     /* AR1 filter of phase */
   int16        CorrGainQ;    /*  */
   int16        PdmPpmSlopeQ; /* */
   int16        PpmOfst;
   int16        PpmQ;         /* 11 to accommodate +/-15 ppm*/
   int32        RStatPwrTh;   /* Threshold for disabling VCO updates */
   int16        PpmPerRot;    /* PpmPerRotation = IsrFreq/Decim/NumFgr/CarrierFrq*/
   int16        PpmPerRotQ;   /* max=19200/8/1/196 = 12.2 => Q7 */
   int16        PpmUpdateScaleQ; /* Q for update coef */
   int16        AfcFgrCombInd;/* Bit field indicating which fingers are used for AFC combining LSB=>finger 0*/
} PACKED_POSTFIX IpcDsmAfcParmMsgT;


/*--------------------------------------------------------------------------------**
** IpcDsmAgcParmMsgMsgT AGC parameters                                            **
** This is copied from CBP3.0. Some parameters commented out                      **
** because these are generated by the DSP now using the                           **
** Rx calibration table.                                                          **
**--------------------------------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   int16        HWDacVal;
   int16        Target;
   uint16       PdmMin;
   uint16       PdmMax;
} PACKED_POSTFIX  IpcDsmAgcParmMsgT;

/*------------------------------------------------------------**
** IpcDsmDcBiasParmMsgT DC bias parameters                    **
** Note that this only effects our chip internally so should  **
** not depend on external hw. This should probably report the **
** DC bias to the CP:L1 in case there are external calib that **
** need to be performed.                                      **
**------------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16       Decim;
   uint16       DecimMsg;
   int16        Shift;
} PACKED_POSTFIX  IpcDsmDcBiasParmMsgT;

/* DC Bias Config message */
typedef PACKED_PREFIX struct
{
    ControlModeT CtrlMode;
    int16        DcOffsetI;
    int16        DcOffsetQ;
} PACKED_POSTFIX  IpcDsmDcBiasConfigMsgT;

/* IpcDsmRfConfigMsgT probably only used for debug */
typedef PACKED_PREFIX struct
{
   uint16       Mode;      /* enable/disable/change channel */
   uint16       CdmaBand;  /* band (1st relase always PCS) */
   uint16       FreqChan;  /* frequency channel */
} PACKED_POSTFIX  IpcDsmRfConfigMsgT;

/*--------------------------------------------------------------------------------**
** IpcDsmTxAgcParmMsgT TxAGC parameters                                            **
**--------------------------------------------------------------------------------*/
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
   uint16       OLCorrGainQ;
   uint16       OLMaxSlewQ;
} PACKED_POSTFIX  IpcDsmTxAgcParmMsgT;

typedef PACKED_PREFIX struct
{
   int16        PwrMaxdBQ;
   int16        PwrMindBQ;

   int16        TxAgcCalibrAdjdBQ[SYS_MAX_NUM_HYST_STATES_TXAGC];     /* CPt + CPf + CPv:Calibration Adjustments at nominal tx levels */
   int16        TxAgcMaxCalibrAdjdBQ;    /* CPMAXt + CPMAXf + CPMAXv:Calibration Adjustments at max tx levels  */
} PACKED_POSTFIX  IpcDsmTxAgcPwrMsgT;
#else
typedef PACKED_PREFIX struct
{
   uint16       OLCorrGainQ;    /**Q6*/
   uint16       OLMaxSlewQ;     /**Q6*/
   int16        PwrMaxdBQ;      /**Q6*/
   int16        PwrMindBQ;      /**Q6*/
   int16        PwrDac;         /**Q5*/
} PACKED_POSTFIX  IpcDsmTxAgcCaliMsgT;

typedef PACKED_PREFIX struct
{
   int16 TxPdetCplLossdBQ[HWD_PA_MODE_NUM];    /** PDET coupler loss with freq compensation, dB in Q5*/
   int16 TxAgcCalibrAdjdBQ[HWD_PA_MODE_NUM];   /** txAgc (paGain) total compensation, dB in Q5 */
} PACKED_POSTFIX  IpcDsmTxAgcPwrMsgT;
#endif 

#ifdef __SAR_TX_POWER_BACKOFF_SUPPORT__
typedef PACKED_PREFIX struct
{
   int16       SarPwrOffset;
} PACKED_POSTFIX  IpcDsmTxSarPwrOffsetMsgT;
#endif

/*--------------------------------------------**
** IpcDsmRxCalParmMsgT                        **
** The most often used parms are in DSP.      **
** The slow corrections caused by Temp & Freq **
** are computed in the CP and passed down.    **
**--------------------------------------------*/
#define RX_CAL_PARMS_CHANGE_RF_BAND_BIT     0x0001
#define RX_CAL_PARMS_CHANGE_RX_IQ_SWAP_BIT  0x0002
#define RX_CAL_PARMS_CHANGE_BASE_CH_ADJ_BIT 0x0004
#define RX_CAL_PARMS_CHANGE_THRESH_BIT      0x0008
#define RX_CAL_PARMS_CHANGE_CALIB_LINE_BIT  0x0010

typedef PACKED_PREFIX struct
{
    int16       BoundaryLeftStep;/* left boundary of the region, units DAC Step--RxAgcHWDacVal in Q5 precision */
    int16       SlopeDbPerStep;  /* slope of the linear region, units dBGainQ/DAC step (Q = SYS_LIN_APPROX_SLOPE_Q(=8) + RFC_DB_Q(=6)= 14) */
    int16       InterceptdB;     /* dB Gain intercept of region, units dB, Q=RFC_DB_Q */
} PACKED_POSTFIX  DbToDacStepLinearRegionT;      /* structure for a region of the piecewise linear transfer function */
                                 /* of a dBGain to DAC step conversion (Rx gain) */

/*--------------------------------------------**
** IpcDsmTxCalParmMsgT                        **
** The most often used parms are in DSP.      **
** The slow corrections caused by Temp & Freq **
** are computed in the CP and passed down.    **
**--------------------------------------------*/
#define TX_CAL_PARMS_CHANGE_RF_BAND_BIT     0x0001
#define TX_CAL_PARMS_CHANGE_BASE_CH_ADJ_BIT 0x0002
#define TX_CAL_PARMS_CHANGE_THRESH_BIT      0x0004
#define TX_CAL_PARMS_CHANGE_CALIB_LINE_BIT  0x0008


typedef PACKED_PREFIX struct
{
   int16        BoundaryLeftDbm;  /* left boundary of the region, units dBm, Q=RFC_DB_Q */
   uint16       SlopeStepPerDbm;  /* slope of the linear region, units DAC step/dBmQ (Q = SYS_LIN_APPROX_SLOPE_Q) */
   int16        InterceptStep;    /* DAC Step intercept of region, Q0 */
} PACKED_POSTFIX  DacStepToDbmLinearRegionT;      /* structure for a region of the piecewise linear transfer function */
                                  /* of a DAC step to Dbm conversion (Tx power ) */

typedef PACKED_PREFIX struct
{
    uint16  TxAgcPAGainStates;
    int16   TxAgcHystHighThresh1Dbm; /* thresh to switch the TxAGC 1st hyst state to med gain,TxGain in dBm with Q = RFC_DB_Q*/
    int16   TxAgcHystLowThresh1Dbm;  /* thresh to switch the TxAGC 1st hyst state to low gain,TxGain in dBm with Q = RFC_DB_Q*/
    int16   TxAgcHystHighThresh2Dbm; /* thresh to switch the TxAGC 2nd hyst state to high gain,TxGain in dBm with Q = RFC_DB_Q */
    int16   TxAgcHystLowThresh2Dbm;  /* thresh to switch the TxAGC 2nd hyst state to med gain,TxGain in dBm with Q = RFC_DB_Q*/
} PACKED_POSTFIX  TxAgcThreshT;

/* IPC_DSM_TXAGC_CAL_PARM_MSG Message type */
/* Message structure which supports multi-state Tx AGC adjustments */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
    uint16                      Change;               /* bit field to indicate changes in each of the elements below */
    uint16                      RfBand;               /* RF Band */
    int16                       TxAgcChnlAdjustdB[SYS_MAX_NUM_HYST_STATES_TXAGC];
                                                      /* temp & freq adjustment for base channel */
    TxAgcThreshT                TxAgcThresh;          /* dB Gain Threshold values */
    DacStepToDbmLinearRegionT   TxPwrCalibLine[SYS_MAX_NUM_HYST_STATES_TXAGC][SYS_MAX_NUM_GAIN_POINTS_TXAGC];
} PACKED_POSTFIX  IpcDsmTxCalParmMsgT;
#else
typedef PACKED_PREFIX struct
{
 int16 Start;  /* Tx power for L2M or M2H, unit is 1/32dBm (Q5)*/
 int16 End;    /* Tx power for H2M or M2L, unit is 1/32dBm (Q5)*/
} PACKED_POSTFIX HwdTxPaHystDataDspmT;

typedef PACKED_PREFIX struct 
{
   int16  refPower; 
   int16  paGain; 
   uint16  paMode;
   uint16  paVm0;
   uint16  paVm1;
   uint16 paVdd;
} PACKED_POSTFIX HwdRfPaContextDspmT;

typedef PACKED_PREFIX struct
{
  uint16 change;
  uint16 RfBand;
  uint16 PaSectionNum;
  int16  couplerLoss[HWD_PA_MODE_NUM];
  HwdTxPaHystDataDspmT TxHyst[HWD_PA_MODE_NUM - 1];
  HwdRfPaContextT TxPaContext[SYS_MAX_NUM_GAIN_POINTS_TXAGC];
#ifdef MTK_DEV_RF_CUSTOMIZE
  int16 paPhaseJumpComp[HWD_PA_MODE_NUM];
#endif
  HwdTxPwrBackOffTblT TxPwrBackOffRegion;
} PACKED_POSTFIX IpcDsmTxCalParmMsgT;
#endif

/*----------------------------------**
** IPC_DSM_TXAGC_CONFIG_MSG Message **
**-----------------------------------*/
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef enum {   
    BYPASS_DDPC=0,
    DDPC_OPEN_LOOP_AB=1,
    DDPC_OPEN_LOOP_REL=2,
    DDPC_CLOSE_LOOP_AB=3,
    DDPC_CLOSE_LOOP_REL=4,    
    DDPC_CTRL_INVALID=5,
    DDPC_CAL_CLOSE_LOOP_AB=6,
    DDPC_CAL_OPEN_LOOP_AB=7
}DdpcCtrlType;

typedef PACKED_PREFIX struct
{
    ControlModeT    CtrlMode;
    uint16          Method;
    /**Power : Q6*/
    int16           Power;
    /**DdpcCtrlType*/
    uint16      DdpcCtrl;
    /**THe following paramenters are valid if if CtrlMode.WordFormat == IPC_MANUAL*/
    /*Coupler loss  in dB, Q5*/
    int16       CouplerLoss;
    /* 0 for H, 1 for M, 2 for L*/
    uint16      PaMode;
    /* PA gain in dBm, Q5*/
    int16       PaGain;   
    /* 0/1*/
    uint16      Vm1;   
    /*0/1*/
    uint16      Vm2;
    /* DAC for voltage, in unit of mV*/
    uint16      VoltctrlDac;
    /**1--configure only once ,other value---Config continously*/
    uint16      CfgOnce;    
} PACKED_POSTFIX  IpcDsmTxAgcConfigMsgT;

typedef PACKED_PREFIX struct
{
    /* Power detection result (1/32 dBm) */
    int16        PdetPower;

    /* DDPC output (1/32 dB) */
    int16        DdpcOutput;

    /* GBB0 (1/32 dB) */
    int16        Gbb0;

    /* GBB1 (1/32 dB) */
    int16        Gbb1;

    /* Tx Detection Gain (1/32 dBm) */
    int16        TxDetGain;

    /* Coupler Loss (1/32 dB) */
    int16        CouplerLoss;    
} PACKED_POSTFIX  IpcCpDetTxAgcRptMsgT;

#else 
typedef PACKED_PREFIX struct
{
    ControlModeT    CtrlMode;
    uint16          Method;
    uint16          PdmValue;
    int16           HystState;
    uint16          Power;
} PACKED_POSTFIX  IpcDsmTxAgcConfigMsgT;
#endif 
/* Method */
#define IPC_HW_VALUE    0
#define IPC_DB_GAIN     1

/* IPC_DSM_TX_AGC_GET_MSG Message type */
typedef PACKED_PREFIX struct
{
    uint16    Power;
    uint16    HwValue;
} PACKED_POSTFIX  IpcDsmTxAgcGetMsgT;

/*----------------------------------**
** IPC_DSM_RXAGC_CONFIG_MSG Message **
**-----------------------------------*/
typedef PACKED_PREFIX struct
{
    ControlModeT    CtrlMode;
    uint16          PdmValue;
} PACKED_POSTFIX  IpcDsmRxAgcConfigMsgT;

/* IPC_DSM_AFC_CONFIG_MSG Message type */
typedef PACKED_PREFIX struct
{
    ControlModeT    CtrlMode;
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
    uint16          PdmValue;
#else
    uint16          DcxoFoeHi;
    uint16          DcxoFoeLo;
#endif 
} PACKED_POSTFIX  IpcDsmAfcConfigMsgT;

/* DC Bias Scaling message - IPC_DCB_GAINBAL_BITSEL_MSG */
typedef PACKED_PREFIX struct
{
   uint16 BitSel;
   int16 Gba1I;
   int16 Gba2I;
   int16 Gba1Q;
   int16 Gba2Q;
} PACKED_POSTFIX  IpcDsmDcBiasBitSelMsgT;

/* IPC_DSM_RX_CAL_MEASURE_MSG Message type */
typedef PACKED_PREFIX struct
{
    uint16    CalMode;
    int16     HystState;
    uint16    SettleTime;
} PACKED_POSTFIX  IpcDsmRxCalMeasureMsgT;

/* IPC_DSM_RX_CAL_OVERRIDE_MSG Message type */
typedef PACKED_PREFIX struct
{
    uint16    UseNvramData;
    uint16    Slope;
} PACKED_POSTFIX  IpcDsmRxCalOverrideMsgT;

/* IPC_DSM_RX_AGC_GET_MSG Message type */
typedef PACKED_PREFIX struct
{
    uint16    Power;
    uint16    HwValue;
} PACKED_POSTFIX  IpcDsmRxAgcGetMsgT;

/* IPC_DSM_RFON_TABLE Message type */
typedef PACKED_PREFIX struct
{
    uint16    RxRfonBitMask[3];
} PACKED_POSTFIX  IpcDsmRfonTableMsgT;

typedef enum {
    LOW_TO_MED_TRANS=0,
    MED_TO_LOW_TRANS,
    MED_TO_HIGH_TRANS,
    HIGH_TO_MED_TRANS,
    NUMBER_OF_TRANS
}TransitionType;

/* IPC_DSM_TXON_TABLE Message type */
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
    uint16    TxonBitMask[3];
    uint16    Pa1CmpAddress[NUMBER_OF_TRANS];
    uint16    Pa1CompareValue[NUMBER_OF_TRANS];
    uint16    Pa2CmpAddress[NUMBER_OF_TRANS];
    uint16    Pa2CompareValue[NUMBER_OF_TRANS];
    uint16    PdmCmpAddress[NUMBER_OF_TRANS];
    uint16    PdmCompareValue[NUMBER_OF_TRANS];
    uint16    TxonDlyLdBitMask[NUMBER_OF_TRANS];
    uint16    PdmDlyLdBitMask[NUMBER_OF_TRANS];
} PACKED_POSTFIX  IpcDsmTxonTableMsgT;
#else
typedef PACKED_PREFIX struct
{
    /* MIPI */
    uint32 timCtrl[MIPI_TPC_DATA_MAX_NUM];
    uint32 immCtrl[MIPI_TPC_DATA_MAX_NUM];
    uint32 cw[MIPI_TPC_OCT_NUM][MIPI_TPC_DATA_MAX_NUM];
    uint32 evtOffset;
    uint16 cwNum;
    uint16 bsiName;
} PACKED_POSTFIX  IpcDsmTxonTableMsgT;
#if defined (IS_C2K_ET_FEATURE_SUPPORT)
typedef PACKED_PREFIX struct
{
    
    uint32 timCtrl[MIPI_ETM_TPC_DATA_MAX_NUM];
    uint32 immCtrl[MIPI_ETM_TPC_DATA_MAX_NUM];
    uint32 cw[MIPI_ETM_TPC_DATA_MAX_NUM];
    uint16 cwNum;
    uint32 evtOffset;
    uint16 bsiName;
    uint16 bankId;
}PACKED_POSTFIX IpcDsmTxonEtmTableMsgT;
#endif
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef PACKED_PREFIX struct
{
   uint16 paramValid;
   
   /* Rx diversity enable */
   uint16 rxDiversityEnable;
   /* Rx diversity test enable. The diversity path will be always on if this
      is TRUE. Used by HSC */
   uint16 rxDiversityTestEnable;
   /* PMU VPA control disable/enable */
   uint16 paVddPmuControlEnable;
   /* BATT VPA control disable/enable */
   uint16 paVddBattControlEnable;
   /* DC2DC VPA control disable/enable */
   uint16 paVddDc2DcControlEnable;
   /* HPUE VPA control disable/enable */
   uint16 paVddHpueControlEnable;
   /* Temperature measurement ADC selection: 1: RF internal ADC, 0: external ADC*/
   uint16 customer_TM_enable; 
   /* SAR disable/enable */
   uint16 TPO_enable;
   /* SAR meta disable/enable */
   uint16 TPO_meta_enable;
   /* ETM VPA control disable/enable */
   uint16 paVddEtmControlEnable;   

   int16 paModeDlyOffset;

   uint16 txAgcSettlingTime;

   /* PA control BPI mask */
   uint32 bpiMaskLow32;
   uint32 bpiMaskHigh32;
   uint16 vm0Num;
   uint16 vm1Num;
   uint16 vm0Num2nd;
   uint16 vm1Num2nd;
   uint16 mipiEnable;
} PACKED_POSTFIX IpcDsmCustParmMsgT;

/*IPC_DSM_TXPGA_GAIN_TABLE_MSG*/
typedef PACKED_PREFIX struct
{
  int16  TxPgaGainTable[HWD_RF_TXPGA_GAIN_TABLE_SIZE];
#if (!defined(MTK_PLT_RF_ORIONC)||defined(MTK_DEV_HW_SIM_RF))
  int16  DetGainTable[HWD_RF_DET_GAIN_TABLE_SIZE];
#endif
} PACKED_POSTFIX IpcDsmTxPgaTableMsgT;
#endif
/* IPC_DSM_RXAGC_SETTLE_TIME Message type */
typedef PACKED_PREFIX struct
{
    uint16    RxAgcFastSTime;
    uint16    RxAgcSlowSTime;
    uint16    RxAgcFastDecimMsg;
    uint16    RxAgcSlowDecimMsg;
} PACKED_POSTFIX  IpcDsmRxAgcSettleTimeMsgT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
/* IPC_DSM_RXAGC_RX_PATH message type */
typedef PACKED_PREFIX struct
{
   /** HwdRfMpaEnumT */
   uint16    rfPath;
   /** HwdIcPathT */
   uint16    hwdIcPath; 
} PACKED_POSTFIX  IpcDsmRxAgcRxPathMsgT;
#else
/* IPC_DSM_RXAGC_RX_PATH message type */
typedef PACKED_PREFIX struct
{
   uint16    RxAgcMainDigiGainReg;
   uint16    RxAgcDivDigiGainReg;
} PACKED_POSTFIX  IpcDsmRxAgcRxPathMsgT;
#endif

/* IPC_DSM_TXAGC_APT_CTRL Message type */
typedef PACKED_PREFIX struct
{
   uint16    SPdmAddr;
   DacStepToDbmLinearRegionT   TxPwrCalibLine[SYS_MAX_NUM_HYST_STATES_TXAGC][SYS_MAX_NUM_GAIN_POINTS_TXAGC];
} PACKED_POSTFIX  IpcDsmTxAgcAptControlMsgT;

/* IPC_DSM_TXAGC_RELBS_THRSH Message type */
typedef PACKED_PREFIX struct
{
    int16    ReliableBsThreshPwr;
} PACKED_POSTFIX  IpcDsmTxAgcRelibBsThrshMsgT;

typedef PACKED_PREFIX struct
{
    uint16  DcbFastDecim;
    uint16  DcbFastDecimMsg;
    int16   DcbFastShiftUp;
} PACKED_POSTFIX  IpcDsmDcbFastDecimMsgT;

typedef PACKED_PREFIX struct
{
    uint16  DcbIIRFastTimeConst;
    uint16  DcbIIRSlowTimeConst;
    int16   DcbIIRFastAlgPcgConst;
} PACKED_POSTFIX  IpcDsmDcbIIRParmMsgT;


/* IPC_DSM_IIR_RESET_CTRL */
typedef PACKED_PREFIX struct
{
    uint16  RfcRxIirEnb;
    uint16  RfcRxPdmThreshold; /* in Q3 */
    uint16  RfcRxPdmRecovery; /* in Q3 */
    uint16  RfcRxIQThreshold;
} PACKED_POSTFIX  IpcDsmIirResetCtrlMsgT;

/*---------------------------------------------**
** IPC_DSM_RX_SD_PARMS_CFG: IpcDsmRxSdParmMsgT **
**---------------------------------------------*/
#define RFC_RX_SD_PARMS_SZ           22
typedef enum
{
   ANALOG_RXAGC,              /* RxAGC using PDM control */
   DIGITAL_RXAGC_USING_RFON,  /* Digital stepped-gain RxAGC using RF_ON interface */
   DIGITAL_RXAGC_USING_SPI,   /* Digital stepped-gain RxAGC using SPI interface */
   MAX_RXAGC_ALG_TYPES
} RxAgcAlgTypeT;

typedef PACKED_PREFIX struct
{
  uint16 RxAgcAlgType;  /* Rx AGC control scheme - indentified using RxAgcAlgTypeT enum */
  uint16 CpFiqDly;      /* CP Fast Interrupt Delay, in units of symbol time (52us) */
} PACKED_POSTFIX  IpcDsmRxSdParmMsgT;


/*-------------------------------------------------**
** IPC_DSM_RX_DAGC_PARMS_CFG: IpcDsmRxDAgcParmMsgT **
**-------------------------------------------------*/
#define RFC_RX_DAGC_DGAIN_SZ         11
#define RFC_RX_DAGC_SGAIN_SZ         8
#define RFC_RX_DAGC_SGAIN_XTNS_SZ    (RFC_RX_DAGC_SGAIN_SZ << 1)
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define HWD_RF_RX_GAIN_STEP_MAX_NUM  25
#endif
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct
{
    uint16 Change;               /* bit field to indicate changes in each of the elements below */

    uint16 DivAntenna;           /* Diversity Antenna, FALSE(0) for main and TRUE(1) for Diversity */
    uint16 RfBand;               /* RF Band */
    uint16 RxIQSwap;             /* Flag indicating if Rx IQ needs to be swapped, 0: no swap, 1: swap */
    int16  RxAgcBaseChAdjustdB[RFC_RX_DAGC_SGAIN_SZ];  /* an adjustment (i.e., temperature, channel) for each gain state */
    int16  RxAgcCandChAdjustdB;  /* temp & freq adjustment for candidate channel */
    int16  SGainMax;                              /* Max number of SGain states */
    int16  SGainStep[RFC_RX_DAGC_SGAIN_SZ];       /* SGain steps in Log2, Q8 */
    int16  SGainThres[RFC_RX_DAGC_SGAIN_XTNS_SZ]; /* SGain Thres in dBm,  10Log10 */
    int16  SGainMsc[RFC_RX_DAGC_SGAIN_XTNS_SZ];   /* b[7:0]  = HystDly,
                                                     b[14:8] = NewGainState
                                                     b15     = CmpGe: 1: greater than or equal to , 0: less than
                                                  */
    int16  PwrdBmRef;                             /* Reference power in dBm, 10Log10, Q6*/
    int16  SGainRef;                              /* Gain State 0..7 at Reference power */
    uint16 DGainRef;                              /* DGain at Reference power
                                                     b[15:8] = DigiGain, b[7:0] = DigiBitsel
                                                  */
} PACKED_POSTFIX  IpcDsmRxDAgcParmMsgT;

#else
typedef PACKED_PREFIX struct
{
   int16 LowSwitchThresh;      /* Low Switch Threshold, Units: dB with Q=DSPM_IPC_DB_Q */
   int16 HighSwitchThresh;     /* High Switch Threshold, Units: dB with Q=DSPM_IPC_DB_Q */
} PACKED_POSTFIX  HwdRxDagcSwitchThreshDspmT;
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
typedef struct
{
  uint16 RxFsm;
}IpcDsmRxPathCtrlMsgT;

#if defined(MTK_PLT_RF_ORIONC) && (!defined(MTK_DEV_HW_SIM_RF))
typedef PACKED_PREFIX struct
{
 uint16 change;        
 uint16 RxPath;
 uint16 RfBand;  
 int16  RxAgcBaseChAdjustdB[HWD_LNA_MODE_NUM];
} PACKED_POSTFIX IpcDsmRxDAgcCompMsgT;
#else
typedef PACKED_PREFIX struct
{
 uint16 RxPath;
 uint16 RfBand;
 int16  RxPathLossHpm[HWD_LNA_MODE_NUM];
 int16  RxPathLossLpm[HWD_LNA_MODE_NUM];
} PACKED_POSTFIX IpcDsmRxDAgcCompMsgT;
#endif

typedef PACKED_PREFIX struct
{
 int16 MainRxDcI;
 int16 MainRxDcQ;
 int16 DivRxDcI;
 int16 DivRxDcQ;
} PACKED_POSTFIX HwdRxAgcDcocParmT;

/** RXDC Table context */
typedef PACKED_PREFIX struct
{
  uint16  rxDcocI;  /** Rx DCOC I value */
  uint16  rxDcocQ;  /** Rx DCOC Q value */
} PACKED_POSTFIX HwdRxDcocT;

typedef PACKED_PREFIX struct
{
 uint16        rfBand;
 uint16        rxPath;
#if (!defined(MTK_PLT_RF_ORIONC)||defined(MTK_DEV_HW_SIM_RF))
 uint16        pwrMode;
#endif
 HwdRxDcocT    rxDcocTable[HWD_RF_RX_GAIN_STEP_MAX_NUM];
#if (!defined(MTK_PLT_RF_ORIONC)||defined(MTK_DEV_HW_SIM_RF))
 HwdRxDcocT    rxDfeDcoTbl[HWD_RF_RX_GAIN_STEP_MAX_NUM];
#endif
} PACKED_POSTFIX IpcDsmRxAgcDcocParmMsgT;

#endif
/*----------------------------------------------------------**
** IPC_DSM_TX_DAGC_SPI_GAINS_MSG: IpcDsmTxDagcSpiConfigMsgT **
**-----------------------------------------------------------*/
#define  RFC_RXTX_DAGC_CP_SPI_CTRL   0
#define  RFC_RXTX_DAGC_DM_SPI_CTRL   1

#define  RFC_TX_DAGC_MAX_REGS_NUM    8

typedef PACKED_PREFIX struct
{
   uint16  Data[RFC_TX_DAGC_MAX_REGS_NUM*2];    /* reg settings for each gain state (up to 3, 2 16-bit words per setting) */
} PACKED_POSTFIX IpcDsmTxGainRegT;

typedef PACKED_PREFIX struct
{
   uint16  GainSwitchCtrl;   /* default is CP control, so if this message is not sent - will work as before */
   uint16  SpiBlk;
   uint16  NumRegs;     /* number of registers to be written for every gain switch - up to 4 */
   uint16  RegSize;
   IpcDsmTxGainRegT  Regs[SYS_MAX_NUM_HYST_STATES_TXAGC];   /* Up to 4 registers can be programmed for each gain */
   uint16  DelayArr[NUMBER_OF_TRANS];
} PACKED_POSTFIX  IpcDsmTxDagcSpiConfigMsgT;

/*----------------------------------------------------------**
** IPC_DSM_TX_DAGC_SPI_MSG: IpcDsmTxDagcFullSpiCfgMsgT **
**-----------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16  TxAgcCtrl;        /*  default is CP control, so if this message is not sent - will work as before */
   uint16  TotalBitsInReg;   /* Number of bits in the register containing Tx Agc value */
   uint16  BitsPerSetting;   /* Number of bits for AGC control */
   uint16  StartBit;         /* Place of Agc word inside the register */
   uint16  SpiBlk;
   uint32  DefRegister[2];
   
} PACKED_POSTFIX  IpcDsmTxDagcFullSpiCfgMsgT;

/*-------------------------------------------------------**
** IPC_DSM_RX_DAGC_GAIN_STATE: IpcDsmRxDAgcGainStateMsgT **
**-------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
    uint16 RfPath;
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
    uint16 gsmBlock;
    /** The current Rx power in dBm (Q6)*/
    int16  rxPowerDbm; /* Q6 */
    /**The required DCOC that convert to RXADC, the unit is mv . Q4 value */
    int16  reqDcocI;
    /**The required DCOC that convert to RXADC, the unit is mv . Q4 value */
    int16  reqDcocQ;
    uint16 getDcTable;
    uint16 band;
    uint16 path;
#else
    uint16 SGainState;
#endif
    uint16 CtrlMode;
} PACKED_POSTFIX  IpcDsmRxDAgcGainStateMsgT;

#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
/*-------------------------------------------------------**
** IPC_DSM_PDM_CTRL_MSG_MSG: IpcRfcPdmCtrlMsgT           **
**-------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16      AfcPdmAddr;       /* if = 0, then don't change */
   uint16      TxAgcPdmAddr;     /* if = 0, then don't change */
} PACKED_POSTFIX IpcRfcPdmCtrlMsgT;
#endif

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#ifdef MTK_DEV_RF_TEST
/*-------------------------------------------------------**
** IPC_DSM_RF_TEST_MSG: IpcRfcPdmCtrlMsgT           **
**-------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16      tstNum;      /* RF Test case number */
   uint16      param;       /* RF Test case parameters */
   uint16      param2;
} PACKED_POSTFIX IpcHwdRfTstMsgT;
#endif /* MTK_DEV_RF_TEST */
#endif

/*--------------------------------------------------------**
** IPC_DSM_RSSI_CORRECTION_TABLE_MSG: RssiCorrectionDataT **
**--------------------------------------------------------*/
#define MAX_RSSI_CORRECTIONS         13
typedef PACKED_PREFIX struct
{
   int16  offset;                /* Log2 Q7 */
   int16  slope;                 /* log2 Q7 */
} PACKED_POSTFIX RssiCorrectionDataT;


/*---------------------------------------------------**
** End CP to DSPM RF Control Task Message Structures **
**---------------------------------------------------*/

/*---------------------------------------------------**
** CP to DSPM Miscellaneous Task Message Structures  **
**---------------------------------------------------*/

/* IpcDsmMdmRstMsgT modem reset - reset various modem functions */
typedef enum {
  FWD_INIT_BN=0,
  MSC_INIT_BN=1,
  REV_INIT_BN=2,
  RFC_INIT_BN=3,
  SCH_INIT_BN=4
#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE_L1)
  ,SRLTE_INIT_BN=5
#endif
 } IpcDsmMdmRstMsgResetBnT;

/* This message requires an ack */
typedef PACKED_PREFIX struct
{
   uint16      SeqNum;
   uint16      Reset;  /* bit field: with IpcDsmMdmRstMsgResetBnT */

   uint16      Type;   /* 0 - cold, 1 - warm */
#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE_L1)
   uint16      SrlteEnable;
#endif
#if defined (MTK_CBP)
   uint16      VoiceGatingEn;
#endif
} PACKED_POSTFIX  IpcDsmMdmRstMsgT;

typedef PACKED_PREFIX struct
{
   uint16      MsgId;
   uint16      SeqNum;
} PACKED_POSTFIX  IpcDsmMdmAckMsgT;

/* IpcDsmSyncSysTimeMsgT */
typedef PACKED_PREFIX struct
{
   uint16       Immediate;   /* 1 => sync operation should take place as soon as possible */
                             /* 0 => allow for receive delay in Timing Change sync case */
   uint16       PilotPn;     /* Tc/2 resolution */
   uint16       LCState[3];
} PACKED_POSTFIX  IpcDsmSyncSysTimeMsgT;


/* IpcDsmWakeAcqSyncMsgT */
typedef PACKED_PREFIX struct
{
   uint16       CdmaBand;  /* band (1st relase always PCS) */
   uint16       FreqChan;  /* frequency channel */
   uint16       PNOffset;  /* base PN offset*/
                           /* allow hard handoff to be performed on wakeup*/
} PACKED_POSTFIX  BaseChnlT;

typedef PACKED_PREFIX struct
{
   uint16       Change;    /* one bit for each of fields below */
   BaseChnlT    BaseChnl;
   uint16       WinSize;    /* search window size */
   uint32       SysCtrInit;
   uint16       LCState[3];
} PACKED_POSTFIX  IpcDsmWakeAcqSyncMsgT;

/*-------------------------------------------------------------------------**
** IpcDsmWakeStatesMsgT                                                    **
** Notes:                                                                  **
**   The dsp will program the LC and PN states immediately. There may      **
**   be slight timing differences due to time tracking (1/8 chips in 80ms).**
**   The search window at wakeup will take care of this.                   **
**-------------------------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16       PNOffset;  /* offset of PN state at wakeup from current */
                           /* DSP uses this to calculate the PN state */
                           /* to initialize the PN states upon wakeup */
   uint16       LCState[3];
} PACKED_POSTFIX  IpcDsmWakeStatesMsgT;

/*-------------------------------------------------------------**
** IpcDsmFPCParmMsgT forward power control parameters          **
**  should be all parms from Protocol Stack API related to FPC **
**  from L1D_TRAFFIC_CHAN_START_MSG, L1D_SERVICE_CONFIG_NN_MSG **
**  This is completely performed in the DSP                    **
**-------------------------------------------------------------*/
#define IPC_FWD_RATE_16X             0
#define IPC_FWD_RATE_8X              1
#define IPC_FWD_RATE_4X              2
#define IPC_FWD_RATE_2X              3
#define IPC_FWD_RATE_1X              4
#define IPC_FWD_RATE_05X             5
#define IPC_FWD_RATE_025X            6
#define IPC_FWD_RATE_0125X           7
/* IpcDsmFpcParmMsg.ChChanged definition */
#define IPC_FPC_FCH_INCL             0x0001
#define IPC_FPC_DCCH_INCL            0x0002
#define IPC_FPC_SCH_INCL             0x0004
#define IPC_FPC_MODE_INCL            0x0010
#define IPC_FPC_PRI_CHAN_INCL        0x0020
#define IPC_FPC_PRI_THRESH_INCL      0x0040
#define IPC_FPC_SEC_THRESH_INCL      0x0080
#define IPC_FPC_SUBCHAN_GAIN_INCL    0x0100
#define IPC_FPC_SCH_RATE_INCL        0x0200
#define IPC_FPC_CAL_INCL             0x0400
#define IPC_FPC_SCH_INIT_SETPT_OP_INCL 0x0800
typedef PACKED_PREFIX struct
{
   uint16  ChnlNum;   /* physical channel number 0..3 */
   int16   InitSetPt; /* initial setpoint,  in 0.125 dB unit */
                      /* 0xffff means ignore */
   uint16  Fer;       /* Frame Error Rate, in 0.1% unit (range 2-300) */
   int16   MinSetPt;  /* minimum setpoint, in 0.125 dB unit. */
                      /* 0xffff means set min setpoint to current set point    */
   int16   MaxSetPt;  /* maximum setpoint, in 0.125 dB unit. */
                      /* 0xffff means set max setpoint to current set point */
} PACKED_POSTFIX FpcParmsT;

typedef PACKED_PREFIX struct
{
   uint16 RC;         /* RC_1 to RC_5 */
   uint16 ChIncl;     /* bit0=1: FCH included; */
                      /* bit1=1: DCCH included; */
                      /* bit2=1: SCH included. */
   uint16 ChChanged;  /* bitmap, same as above */
   uint16 PriChan;    /* 0=FCH,1=DCCH, */
   uint16 FpcMode;    /* FPC_MODE, value (000,...110) */
   uint16 SchRate;    /* index:0 highest rate (4 chip), 7 lowest (1/8 rate) */
   FpcParmsT  FpcFch;
   FpcParmsT  FpcDcch;
   FpcParmsT  FpcSch;
   uint16  SetPtThres;     /* in 0.125 dB unit. If the change in setpoint goes */
                           /* beyond this threshold, report setpoint to BS */
   uint16  SetPtThresSch;  /* in 0.125 dB unit. If the change in SCH */
                           /* setpoint goes beyond this threshold, report setpoint to BS. */
   uint16 FpcSubchanGain;  /* FPC_SUBCHAN_GAIN set by BS in dB , Q8 (>=0, default=0)*/
   int16  ChCalFactor[8];  /* unit dB in Q8 format */
                           /* ChCalFactor[0] = cal for FSCH rate other than full rate */
                           /* ChCalFactor[1] = cal for FSCH full rate */
                           /* ChCalFactor[2] = cal for FSCH full rate */
                           /* ChCalFactor[3] = cal for FDCCH full rate */
                           /* ChCalFactor[4] = cal for FCH full rate */
                           /* ChCalFactor[5] = cal for FCH half rate */
                           /* ChCalFactor[6] = cal for FCH quarter rate */
                           /* ChCalFactor[7] = cal for FCH eighth rate */
} PACKED_POSTFIX  IpcDsmFpcParmMsgT;

typedef PACKED_PREFIX struct
{
  uint16   OLCoarseK1;            /* FPC outer loop coarse adj K1*1000 Q0 */
  uint16   OLCoarseK2;            /* FPC outer loop coarse adj K1 Q14 */
  uint16   OLFineC1;              /* FPC outer loop fine adj C1 Q14 */
  uint16   OLFineC2;              /* FPC outer loop fine adj C2 Q14 */
  int16    ZmetricIIRAlpha[2];    /* FPC inner loop Z0 & Z1 metric filter [0]-FCH, [1]-SCH, alpha in Q14 */
} PACKED_POSTFIX  IpcDsmFpcAlgParmMsgT;

typedef PACKED_PREFIX struct
{
   uint16  FchSetPtIncl;   /* 1 = FPC_FCH_CURR_SETPT included */
   int16   FchCurrSetPt;   /* FPC_FCH_CURR_SETPT, in 0.125 dB unit */
   uint16  DcchSetPtIncl;  /* 1 = FPC_DCCH_CURR_SETPT included */
   int16   DcchCurrSetPt;  /* FPC_DCCH_CURR_SETPT, in 0.125 dB unit */
   uint16  SchSetPtIncl;   /* 1 = FPC_SCH_CURR_SETPT included */
   int16   SchCurrSetPt;   /* FPC_SCH_CURR_SETPT, in 0.125dB unit */
} PACKED_POSTFIX IpcCpFpcRepMsgT;

/* IpcDsmRPCParmMsgT */
typedef PACKED_PREFIX struct
{
   uint16       Mode;    /* 0-disable/1-enable/2...-debug modes */
   uint16       PwrCntlStep;
   uint16       PwrCntlDelay;
} PACKED_POSTFIX  IpcDsmRPCParmMsgT;

/* IpcDsmSlottedModeMsgT */
typedef PACKED_PREFIX struct
{
   uint16       Enable;    /* enable, disable */
} PACKED_POSTFIX  IpcDsmSlottedModeMsgT;

/* IpcDsmQuickPageMsgT */
typedef PACKED_PREFIX struct
{
   uint16       Enable;
   uint16       BitPos;    /* symbol location of first bit */
} PACKED_POSTFIX  IpcDsmQuickPageMsgT;

/* IpcDsmTrkParmMsgT time track parameters */
typedef PACKED_PREFIX struct
{
   uint16       Decim;
   int16        Shift;
   uint16       A0;
   uint16       A1;
   uint16       DecimMsg;
   uint16       UnlockCnt;
} PACKED_POSTFIX  IpcDsmTrkParmMsgT;

/*------------------------------------------------------**
** End CP to DSPM Miscellaneous Task Message Structures **
**------------------------------------------------------*/

/*---------------------------------------------**
** CP to DSPM Searcher Task Message Structures **
**---------------------------------------------*/

/* IpcDsmTestAlgValueMsgT */
typedef PACKED_PREFIX struct
{
   uint16   Searcher;               /* Searcher Ec/Io normalization factor */
   uint16   Finger;                 /* Finger Ec/Io normalization factor */
} PACKED_POSTFIX NormFactorT;

typedef PACKED_PREFIX struct
{
   uint16   AverageNum;             /* Number of Active set searcher to complete (with averaging)
                                       before making allocation decisions
                                    */
   uint16   Interval;               /* Number of Active set symbols searcher to complete (time lapse)
                                       before making allocation decisions
                                    */
   uint16   EcIoAbsThresh;          /* Minimum Ec/Io level at which to allocate a finger. Any
                                       searcher or finger result which is less than this value
                                       will not be/remain allocated
                                    */
   uint16   MinOffsetDist;          /* Minimum offet distance for allocation. Fingers will not
                                       be allocated less than this distance from each other (uints
                                       Tc/8 to allow flexibility if this vlaue changes)
                                    */
   int16   FngrEcIoFiltConst;      /* Finger filtered power filter time constant */
   int16   FngrEcIoInstConst;      /* Finger Instant power filter time constant */
   uint16   SideLobeDist;           /* Distance from a higher power entry within which an allocation
                                       phase is considered to potentially be a filter response sidelobe
                                    */
   uint16   EcIoRelThresh;          /* Threshold relative to the power of a higher powered entry below
                                       which  a finger will not be allocated if it has an offset less than
                                       SideLobeDist from the higher powered entry
                                    */
} PACKED_POSTFIX FingerAllocT;

typedef PACKED_PREFIX struct
{
   uint16   FastSystemTimeTrackNum; /* Number of initial fast system time tracking when
                                       the first finger is allocated
                                    */
   uint16   FastTimeTrackNum;       /* Number of initial fast time tracking when
                                       a new finger is allocated */
   uint16   FgrAllocOffsetDistQ3;   /* Number of chips a lower power finger is allowed to
                                       approach a higher power finger */
   uint16   TrkSysTimeMinFrames;    /* minimum number frames between track of sys time */
   uint16   TrkMinPwr;              /* minimum time tracking powr */
   int16    TimeTrackCoefR1;        /* early late gate time tracking constant */
   uint16   CompPn;                 /* Tc/2, Search PN compensation to align with Finger */
   int16    TimingErrComp;          /* Freq Err correction comp from Timing Err */
} PACKED_POSTFIX TimeTrackT;

typedef PACKED_PREFIX struct
{
   int16   ActSetTimer;            /* Timer value for which Active Set must be searched upon expired */
   int16   PwrRptTimer;            /* Power Measurement Report Timer */
} PACKED_POSTFIX ListProcT;

typedef PACKED_PREFIX struct
{
   uint16   UsablePathCntr;             /* Usable Path Spy Cntr */
   uint16   FingerStatusCntr;           /* FingerStatus Spy Cntr */
} PACKED_POSTFIX SchSpyT;

typedef PACKED_PREFIX struct
{
   NormFactorT    NormFactor;       /* Variables related to Ec/Io power normalization */
   FingerAllocT   FingerAlloc;      /* Variables related to finger allocation */
   TimeTrackT     TimeTrack;        /* Variables related to time tracking */
   ListProcT      ListProc;
   SchSpyT        Spy;              /* Variables control spy output rate */
} PACKED_POSTFIX  IpcDsmTestAlgValueMsgT;

/* IpcDsmTestAlgValue1MsgT, in addition to TestAlgValue */
typedef PACKED_PREFIX struct
{
    uint16 FastPwrRptTimer;
    uint16 QpchNeighborFastPwrRptTimer;
    uint16 SearchTimeMin;
    uint16 EarliestFngrEcIoAbsThresh;
    uint16 NoiseFloor;
    uint16 CohIaqThresh;
    uint16 SysNumFingers;
    uint16 SideLobeDist0;
    uint16 SideLobeDist1;
    uint16 SideLobeDist2;
    uint16 SideLobeWin;
    uint16 FgrAllocMPathThresh;
    uint16 FgrAllocSoftHOThresh;
    uint16 MaxFingerBs;
    uint16 MinAcqWinSize;
    int16  FgrAllocDropTimer;
    uint16 FgrAllocDropThresh;
    uint16 DecimationCntr;
    int16  FgrOffsetQ3;
    uint16 SchPwrRptCombThresh;
    uint16 Sch3RayAddFloorThresh;
    uint16 Sch3RayFloor;
    uint16 NCohIaqThresh;                  
    uint16 IaqMinPnDist;                  
    int16 QpchSnrThresh;             /* Q6 */
    int16 QpchSnrAdj;                /* Q0 */
    int16 QpchT1LowSnrSlope;         /* Q0 */
    int16 QpchT1LowSnrIntercept;     /* Q0 */
    int16 QpchT2LowSnrSlope;         /* Q0 */
    int16 QpchT2LowSnrIntercept;     /* Q0 */
    int16 QpchT1HighSnrSlope;        /* Q0 */
    int16 QpchT1HighSnrIntercept;    /* Q0 */
    int16 QpchT3Slope;               /* Q0 */
    int16 QpchT3Intercept;           /* Q0 */
    int16 QpchT3LowLimit;            /* Q0 */
    uint16 MinPwrEarlst;             /* the least strength of a path for finding the earliest one. Used to recenter around each active and candidate pilot */ 
    int16 QpchSnrMarginal;           /* Q8 */
} PACKED_POSTFIX  IpcDsmTestAlgValue1MsgT;


/* IpcDsmPilotAcquireMsgT */
typedef PACKED_PREFIX struct
{
   uint16       CdmaBand;  /* band (1st relase always PCS) */
   uint16       FreqChan;  /* frequency channel */
   uint16       NumCycles; /* number of cycles before abort */
} PACKED_POSTFIX  IpcDsmPilotAcquireMsgT;

#ifdef  MTK_CBP_SYNC_OPTIMIZE   
/* IpcDsmMiniAcquireMsgT */
typedef PACKED_PREFIX struct
{
   uint16       PilotPn;  /* PilotPn */
   uint16       BypassSyncFlag;  /* BypassSyncFlag */
} PACKED_POSTFIX  IpcDsmMiniAcquireMsgT;
#endif

/* IpcDsmSearchParmMsgT */
typedef PACKED_PREFIX struct
{
   uint16       DwellLen1;
   uint16       DwellLen2;
   uint16       Thresh1;
   uint16       QuickSrchThresh1;       /* Dwell 1 threshold for faster search time */
   uint16       QuickSrchThreshSort;    /* Sort Threshold for faster search time, single buffer */
   uint16       QuickSrchNumNeighborThresh;   /* Force quick search in QPCH mode if the number of neighbors exceeds this*/
} PACKED_POSTFIX  IpcDsmSearchParmMsgT;

/* IpcDsmSearchWinSizeMsgT */
typedef PACKED_PREFIX struct
{
   uint16       ActWinSize;
   uint16       NeighWinSize;
   uint16       RemainWinSize;
} PACKED_POSTFIX  IpcDsmSearchWinSizeMsgT;


typedef PACKED_PREFIX struct
{
   uint16       WinSizeThresh;           /* window size in chip */
   uint16       MaxWinSizeThreshSort;    /* threshsort used for this window size */
} PACKED_POSTFIX  IpcDsmSearchWinSizeParmMsgT;


/* IpcDsmAcqParmMsgT acquisition parameters */
typedef PACKED_PREFIX struct
{
   uint16       DwellLen1;     /* dwell length & thresh for 2 dwell search */
   uint16       DwellPCnt1;
   uint16       Thresh1;
   uint16       DwellLen2;
   uint16       DwellPCnt2;
   uint16       SortListSz;    /* sort list size */
   uint16       SingleBufThreshSort;  /* sort threshold for single buffer */
   uint16       DoubleBufThreshSort;  /* sort threshold for double buffer */
   uint16       VerifWin;      /* verif window */
   uint32       ThreshVerif;   /* verif threshold */
} PACKED_POSTFIX  IpcDsmAcqParmMsgT;




/*-------------------------------------------------------------------------------**
** IpcDsmActiveCandPilotSetMsgT                                                  **
** This message updates the DSP information about the active and candidate       **
** set basestations.                                                             **
** The dsp performs finger allocation after receiving this message.              **
**                                                                               **
** PilotWalsh                                                                    **
** [11:13] 3b - WalshLen (len = 64*2^(WalshLen))                                 **
** [9:10]  2b - QOF                                                              **
** [0:8]   9b - Walsh Id                                                         **
**-------------------------------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16       PilotPN;                  /* 64 Tc resolution */
   uint16       PilotPhase;               /* Chip resolution */
   uint16       PilotWalsh;               /* WalshLen, QOF & Walsh for pilot */
   uint16       PwrCombInd;
   uint16       ChnlWalsh[SYS_NUM_CHNL];  /* QOF & Walsh for channel         */
                                          /* [15]    1b - 1-Channel Disabled  */
                                          /* [9:10]  2b - QOF                 */
                                          /* [0:8]   9b - Walsh Id            */
} PACKED_POSTFIX  IpcActiveConfigT;

typedef PACKED_PREFIX struct {
   uint16       PilotPN;    /* 64 Tc resolution */
   uint16       PilotPhase; /* Chip resolution */
   uint16       PilotWalsh; /* WalshLen, QOF & Walsh for pilot */
} PACKED_POSTFIX  IpcCandConfigT;

typedef PACKED_PREFIX struct
{
   uint16           NumPilots;
   IpcActiveConfigT Data[SYS_MAX_ACTIVE_LIST_PILOTS];
} PACKED_POSTFIX  IpcActiveListT;

typedef PACKED_PREFIX struct
{
   uint16           NumPilots;
   IpcCandConfigT   Data[SYS_MAX_CANDIDATE_LIST_PILOTS];
} PACKED_POSTFIX  IpcCandListT;

typedef PACKED_PREFIX struct
{
   uint16         SeqNum;     /* Sequence number for co-ordination with Search Result Messages */
   uint16         NextFrame;  /* bit 0  0=immediate, 1=apply in frame(20ms) */
#ifdef MTK_PLT_ON_PC  
   /**Add the Band and Freq chan for PC project; Only used NWSIM project.*/
   uint16       CdmaBand;  /* band (1st relase always PCS) */
   uint16       FreqChan;  /* frequency channel */
#endif   
   IpcActiveListT Active;     /* active set list  */
   IpcCandListT   Candidate;  /* candidate set list  */
} PACKED_POSTFIX  IpcDsmActiveCandPilotSetMsgT;



/*---------------------------------------------------------------------------**
** IpcDsmNeighborPilotSetMsgT                                                **
** This message updates the DSP information about base stations in the       **
** neighbor list.                                                            **
**---------------------------------------------------------------------------*/


typedef PACKED_PREFIX struct
{
   uint16       PilotPN;           /* 64 Tc resolution */
   uint16       PilotWalsh;        /* WalshLen, QOF & Walsh for pilot */
   uint16       NeighborWinData;   /* bits 0-9 Neighbor Window Size, bits 10-12 Neighbor Search Offset */
} PACKED_POSTFIX  IpcNghbrConfigT;


typedef PACKED_PREFIX struct
{
   uint16           NumPilots;
#ifdef MTK_CBP
   IpcNghbrConfigT  Data[SYS_MAX_RTT_NEIGHBOR_LIST_PILOTS];
#else
   IpcNghbrConfigT  Data[SYS_MAX_NEIGHBOR_LIST_PILOTS];
#endif
} PACKED_POSTFIX  IpcNghbrListT;


typedef PACKED_PREFIX struct
{
   uint16           SeqNum;        /* Sequence number for co-ordination with Search Result Messages */
   IpcNghbrListT    Neighbor;      /* active set list  */
   uint16           PilotInc;      /* increment for remaing set  */
} PACKED_POSTFIX  IpcDsmNeighborPilotSetMsgT;



/*--------------------------------------------------------------------**
** IpcDsmPriorityPilotSetMsgT                                         **
** This search preempts the normal search                             **
** The CP uses this to perform priority search                        **
** and candidate frequency search                                     **
** The choice of 3 sets is arbitrary. This lets the CP set the search **
** window for the 3 sets.                                             **
**--------------------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16       NumPilots;
   uint16       PilotPN[SYS_MAX_PRIORITY_LIST_PILOTS];    /* 64 Tc resolution */
   uint16       PilotWalsh[SYS_MAX_PRIORITY_LIST_PILOTS]; /* QOF & Walsh for auxiliary pilot channel */
} PACKED_POSTFIX  IpcPilotListT;

typedef PACKED_PREFIX struct
{
   int16          StartSymbol;    /* symbol to start search, 0xffff = immediate */
   uint16         CdmaBand;
   uint16         FreqChan;
   uint16         NumPilotsA;
   uint16         NumPilotsB;
   uint16         NumPilotsC;
   uint16         WinSizeA;
   uint16         WinSizeB;
   uint16         WinSizeC;
   IpcPilotListT  Pilots;   /* pilot list */
} PACKED_POSTFIX  IpcDsmPriorityPilotSetMsgT;

typedef PACKED_PREFIX struct
{
   uint16         QpchWalshCh;       /* Walsh Code */
   uint16         QpchRate;          /* 0 = 4800, 1 = 9600 */
   uint16         QpchSymbolBit;     /* Symbol Location */
} PACKED_POSTFIX  IpcDsmQpchMsgT;

typedef PACKED_PREFIX struct
{
  uint16 Data[40];
} PACKED_POSTFIX  IpcDsmSimMsgT;

typedef PACKED_PREFIX struct
{
  int16 RxTxOffsetQ3;  /* required (Rx_time - Tx_time) in 1/8 chip
                          to account for the filter delay
                        */
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
   /* Rx group delay offset constant in 1/8 chip, that is used when rx path is swaped between LPM and HPM */
  int16 rxGlbDlyOffset;
#endif

} PACKED_POSTFIX  IpcDsmMscParmMsgT;


/*---------------------------------------------------------------------------**
  IPC_DSM_AFLT_PILOT_SET_MSG
  IpcDsmAfltPilotSetMsgT
  This message provides the DSP the AFLT neighbor list (not regular neighbor
  list) received in the Position Determination BS Almanac message, and requests
  DSP to do pilot strength, and phase measurements on given neighbor list.
**---------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
  /* individual PILOT_PN from BS ALMANAC             */
  /* first one will always be REF_PN from BS ALMANAC */
  uint16  PilotPN;
  /* individual TIME_CORRECTION from BS ALMANAC                 */
  /* first one will always be TIME_CRRCTION_REF from BS ALMANAC */
   int16 TimeCorrect;
} PACKED_POSTFIX  IpcAfltConfigT;


typedef PACKED_PREFIX struct
{
   uint16          NumPilots;
   IpcAfltConfigT  Data[SYS_MAX_AFLT_LIST_PILOTS];
} PACKED_POSTFIX  IpcAfltListT;


typedef PACKED_PREFIX struct
{
   uint16         SeqNum;
   uint16         SrchWin;
   IpcAfltListT     Aflt;
} PACKED_POSTFIX  IpcDsmAfltPilotSetMsgT;


/*---------------------------------------------------------------------------**
  IPC_DSM_AFLT_PILOT_SET_ABORT_MSG
  IpcDsmAfltPilotSetAbortMsgT
  This message requests DSP to abort the AFLT search measurements for the
  given sequence number.
**---------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
    uint16           SeqNum;
} PACKED_POSTFIX  IpcDsmAfltPilotSetAbortMsgT;

/*---------------------------------------------------------------------------**
   IpcSchMsgAfltAlgValueT
   for IPC_DSM_AFLT_ALG_VALUE_MSG
   This message reconfigures AFLT search parameters.
**---------------------------------------------------------------------------*/
typedef PACKED_PREFIX struct
{
   uint16 Msets;              /* valid values are [1..8] */
   uint16 DwellLen1;          /* (DwellLen1+1)*16 + (DwellLen2+1)*16 */
   uint16 DwellLen2;          /* DwellLen1 = 0x0f; DwellLen2 = 0x2f for single buffer */
                              /* DwellLen1 = 0x3f; DwellLen2 = 0x3f for double buffer */
   uint16 CoarseAverageNum;
   uint16 FineAverageNum;
   uint16 CoarseMaskRelShft;  /* Shift down of detected peak power for finding earliest path */
   uint16 CoarseThreshAbs;    /* Absolute power threshold for finding earliest path */
   uint16 FineWinSize;        /* one sided window in Tc/2 */
    int16 Fine1to2ChipMask;   /* Q15 relative mask */ 
    int16 Fine2to3ChipMask;   /* Q15 relative mask */ 
    int16 FineOtherMask;      /* Q15 relative mask */ 
   uint16 FineThreshAbs;      /* Absolute power threshold for finding earliest path */
   uint16 SpyPilotPN;         /* display coarse and fine search result of this PilotPN in Spy, default: 0 */
   uint16 NormFactor;         /* AFLT Ec/Io normalization factor */   
} PACKED_POSTFIX  IpcSchMsgAfltAlgValueT;


/*---------------------------------------------------------------------------**
  IPC_DSM_QUERY_TIMING_MSG
  IpcDsmQueryTimingMsgT
  This message requests DSP to send up system and finger timing information.
**---------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
    uint16           SeqNum;
} PACKED_POSTFIX  IpcDsmQueryTimingMsgT;

/*--------------------------------------------------------------------**
** IpcDsmCandFreqStartMsgT                                            **
** This message contains the necessary information used to initiate   **
** candidate frequency search.                                        **
**--------------------------------------------------------------------*/
typedef enum /* Enum matches L1dCfSrchModeT in <l1dapi.h> */
{
   IPC_CF_SEARCH_CDMA,
   IPC_CF_SEARCH_ANALOG
} IpcCfSrchModeT;

typedef PACKED_PREFIX struct
{
   uint16         CfSrchMode;
   /* Absolute time in symbol units: [0, 383] */
   uint16         TuneCfSmbNum;
   uint16         CfPllSettleSmbNum;
   uint16         TuneSfSmbNum;
   uint16         SfPllSettleSmbNum;
   uint16         DwellLen1;
   uint16         DwellLen2;
   uint16         Thresh1;
   uint16         NormFactor;
} PACKED_POSTFIX  IpcDsmCandFreqStartMsgT;


/* IPC_DSM_TEST_ALG_VALUE2_MSG Message type */
typedef PACKED_PREFIX struct
{
   uint16    VcxoCompEnable;  /* Sloppy VCXO Compensation Mode 1: enabled */
                              /*                               0: disabled */
   uint16    AvePilotPwrReportFlg; /* 1- QPCH Average pilot power reporting */
                                   /* 0- Non-average power */
} PACKED_POSTFIX  IpcDsmTestAlgValue2MsgT;

/* IPC_DSM_DIVERSITY_CONTROL_MSG Message type */
typedef PACKED_PREFIX struct
{
   uint16    DivDisable;  /* 0: Diversity ON, 1: Diversity OFF */
   uint16    AntPilotPwrCombAlg; /* AntPilotPwrCombAlg - 0: AVERAGE, 1:SUM, 2:MAX, 3:Others */
} PACKED_POSTFIX  IpcDsmDiversityControlMsgT;

/*-------------------------------------------------**
** End CP to DSPM Searcher Task Message Structures **
**-------------------------------------------------*/


/*--------------------------------------**
** DSPM to CP Message Data Structures   **
**--------------------------------------*/

/* IpcCpSearchResultMsgT (from engRelayPilotMeasureReport) */
typedef PACKED_PREFIX struct
{
   uint16       PnPhase;        /* pilot pn relative to zero offset pilot */
   uint16       Strength;       /* pilot power in Q16 linear units -- Bit 15 is Stale Bit 1=Stale */
} PACKED_POSTFIX  PilotMeasurementT;

typedef PACKED_PREFIX struct
{
   uint16              SeqNum;       /* Sequence nuumber for co-ordination with list messages*/
   uint16              NumActive;    /* number of pilots in active list */
   uint16              NumCandidate; /* number of pilots in candidate list */
   uint16              NumNeighbor;  /* number of pilots in neighbor list */
   uint16              NumRemaining; /* number of pilots in remaining list */
   PilotMeasurementT   ActiveList[SYS_MAX_ACTIVE_LIST_PILOTS];
   PilotMeasurementT   CandidateList[SYS_MAX_CANDIDATE_LIST_PILOTS];
 #ifdef MTK_CBP
   PilotMeasurementT   NeighborList[SYS_MAX_RTT_NEIGHBOR_LIST_PILOTS];
#else
   PilotMeasurementT   NeighborList[SYS_MAX_NEIGHBOR_LIST_PILOTS];
#endif
   PilotMeasurementT   RemainingList[SYS_MAX_REMAINING_LIST_PILOTS];
   uint16              EarliestPilotPN;    /* Earliest finger's pilot pn 64Tc */
   int16               SysTimeOffsetQ3;    /* total system time offset in Tc/8 */
#ifdef MTK_CBP_SYNC_OPTIMIZE
   int16               RxPowerdBm ;  /* immediate value used for miniacq fail to judge */
#endif
} PACKED_POSTFIX  IpcCpSearchResultMsgT;


/*---------------------------------------------------------------------------**
  IPC_CP_AFLT_MEAS_MSG
  IpcCpAfltMeasurementsMsgT
  This message is returned from the after the CP requests DSP to do pilot
  strength, and phase measurements on given AFLT neighbor list.
**---------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
   /* PnPhaseREF_PN is PnPhase returned by DSP of  REF_PN received in BS ALMANAC */
   /* ((PilotPN * 64 * 8) + (PnPhase - PnPhaseREF_PN) ) * 2 = PILOT_PN_PHASE     */
   /*  in 1/16 chip unit sent in PROVIDE PILOT PHASE MEASUREMENTS                */
   uint16       PilotPN;
   /* correction relative to PilotPN in 1/8 chip                  */
   int16        PnPhaseOffset;
   /* | -2 * 10 log10 pilot strength | = PILOT_STRENGTH           */
   /*  sent in PROVIDE PILOT PHASE MEASUREMENTS                   */
   /* Pilot strength in Q16 linear units (Bits 14-0)              */
   /* Stale (Bit 15) 1=Stale                                      */
   uint16       Strength;
} PACKED_POSTFIX  AfltMeasurementT;

typedef PACKED_PREFIX struct
{
   uint16              SeqNum;
   /* REF_PN sent in PROVIDE PILOT PHASE MEASUREMENTS                */
   /* REF_PN's pilot strength which is found in                      */
   /*  either in AFLT pilot list or ACTIVE pilot list = REF_STRENGTH */
   /*  sent in PROVIDE PILOT PHASE MEASUREMENTS                      */
   uint16              ReferencePN;
   /* SUPERFRAME = SYSTEM TIME / 4                                     */
   /* ( ( TimingCnt / (4 * 384) ) / 80mS ) + SUPERFRAME = TIME_REF_MS  */
   /*  sent in PROVIDE PILOT PHASE MEASUREMENTS                        */
   /* symbol number of last searcher buffer capture (0 to 0x5FF)       */
   uint16              TimingCnt;
   uint16              NumAflt;
   AfltMeasurementT    AfltList[SYS_MAX_AFLT_LIST_PILOTS];
} PACKED_POSTFIX  IpcCpAfltMeasurementsMsgT;


/*---------------------------------------------------------------------------**
  IPC_CP_TIMING_RSP_MSG
  IpcCpTimingRspMsgT
  This message is a response to IPC_DSM_QUERY_TIMING_MSG.
  It returns the DSM system and finger timing information to CP.
**---------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct
{
   uint16 PilotPN;            /* Base Station Pilot PN */
   int16  OffsetQ3;           /* Finger Offset from Base in Tc/8 */
   uint16 Power1[3];          /* power of short term finger pilot filter: ontime, early, late */
   uint16 Power2;             /* power of long term finger pilot filter */
} PACKED_POSTFIX  FingerTmT;

typedef PACKED_PREFIX struct
{
   int16      SysTimeOffsetQ3;/* cumulative sum of system time adjustments in Tc/8 */
   FingerTmT  FingerTm[SYS_NUM_FINGERS];
   uint16     FingerStat;     /* [11:8] latest #, [7:4] strongest #, [3:0] earliest # */
   uint16     SeqNum;
} PACKED_POSTFIX  IpcCpTimingRspMsgT;


/* IpcCpFwdChnlDtaMsgT */
typedef PACKED_PREFIX struct
{
   uint16       ChnlRdy;      /* bit field indicating which channels are ready */
   uint16       ChnlDataQlty; /* bit field indicating channels are good (CRC or rate decn) */
   uint16       FundRate;     /* Low 4 bitfund chnl rate if assigned, for MTK_CBP  high 12 bit is for Eb/Nt*/
   uint16       ChnlType;     /* channel type, FwdChnlTypeT, 4 bits per channel */
                              /* [0:3] channel 0 type */
                              /* [4:7] channel 1 type */
                              /* [8:11] channel 2 type */
                              /* [12:15] channel 3 type */
   uint16       ChnlSize[4];  /* channel size, 1 word per channel */
                              /* ChnlSize[0]: channel 0 size, in bits */
                              /* ChnlSize[1]: channel 1 size, in bits */
                              /* ChnlSize[2]: channel 2 size, in bits */
                              /* ChnlSize[3]: channel 3 size, in bits */
} PACKED_POSTFIX  IpcCpFwdChnlDtaMsgT;

/* IpcCpFwdChnlPcRspMsgT */
typedef PACKED_PREFIX struct
{
   uint16 Enabled;             /* Indicates the current value of the forward channel puncture state.
                               */
} PACKED_POSTFIX  IpcCpFwdChnlPcRspMsgT;


/* IpcCpFwdChnlScrambleRspMsgT */
typedef PACKED_PREFIX struct
{
   uint16 Enabled;             /* Indicates the current value of the forward channel scramble state
                               */
} PACKED_POSTFIX  IpcCpFwdChnlScrambleRspMsgT;

/* IpcCpRxPowerMsgT */
typedef enum
{
   IPC_SERVING_FREQ_RX_PWR,
   IPC_CANDIDATE_FREQ_RX_PWR
} IpcRxPwrReportT;

typedef PACKED_PREFIX struct
{
   int16   RxPowerdBm;                           /* RxPwr/Received Power     Q=7        */
   int16   DivRxPowerdBm;                        /* Div RxPwr/Received Power Q=7        */
   uint16  RxPwrReportType;
   int16   TxPowerdBm;                           /* TxPwr/Transmit Power     Q=6, dec   */
   uint16  RxGainState;
   uint16  DivRxGainState;
   uint16  TxGainState;                          /* GainState/Tx Gain State  Q=0, dec   */
   int16   TxAgcPdmRep;                          /* TxPdm/Transmit PDM Value Q=0, hex   */
   int16   TxClosedLoopAdj;                      /* CLAdj                    Q=6        */
   int16   TxPowerMax;                           /* MaxPwr                   Q=6        */     
   int16   TxPowerMin;                           /* MinPwr                   Q=6        */
   int16   TxRevKsErr;                           /* RevKsErr                 Q=6        */      
   int16   RxGainIntegral;                       /* RxDAgc value: HwVal      Q=0, hex   */
   int16   RxGainCoarse;                         /* RxDAgc value: Bitsel     Q=0, dec   */  
   int16   RxGainFine;                           /* RxDAgc value: Gain       Q=0, dec   */
   int16   AfcFreqErr;                           /* Afc freq error */
   uint16  AfcPdmVal;                            /* Afc Pdm Value */
   uint16  FwdPilotEbNtMain;                     /* Q8 */
   uint16  FwdPilotEbNtDiv;                      /* Q8 */
   uint16  FwdFChEbNtMain;                       /* Q8 */
   uint16  FwdFChEbNtDiv;                        /* Q8 */   
   uint32  FwdPilotEcIoMain;                     /* Q16 linear */
   uint32  FwdPilotEcIoDiv;                      /* Q16 linear */
   uint16  rxTotalActiveTime;
   uint16  txTotalActiveTime[L1D_TX_POWER_LEVEL_NUM];

   
#if 0
   uint16  FwdPilotEbNtMainPCG0;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG1;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG2;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG3;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG4;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG5;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG6;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG7;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG8;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG9;                 /* Q8 */
   uint16  FwdPilotEbNtMainPCG10;                /* Q8 */
   uint16  FwdPilotEbNtMainPCG11;                /* Q8 */
   uint16  FwdPilotEbNtMainPCG12;                /* Q8 */
   uint16  FwdPilotEbNtMainPCG13;                /* Q8 */
   uint16  FwdPilotEbNtMainPCG14;                /* Q8 */
   uint16  FwdPilotEbNtMainPCG15;                /* Q8 */
#endif   
} PACKED_POSTFIX  IpcCpRxPowerMsgT;


/* IpcCpQuickPageBitsMsgT  */
typedef PACKED_PREFIX struct
{
   uint16       Bits;   /* bit0, bit1 */
   int16        SBit0;  /* scalar bit0 */
   int16        SBit1;  /* scalar bit1 */
} PACKED_POSTFIX  IpcCpQuickPageBitsMsgT;


/*---------------------------*
 | Slotted Paging interface  |
 |       CP -> DSPM          |
 *---------------------------*/

typedef enum
{
   INIT_CMD,                 /* Receive Initialization parameters */
   STOP_CMD,                 /* Prepare for sleep */
   DECODE_PCH_CONT_CMD,      /* Decode the Paging CHannel Continuously until commanded to stop (sleep) */
   DECODE_PCH_NO_ACQ_CMD,    /* This requires QPCH support. The assumption is that the QPCH has already acquired the
                              pilot not longer than 100mS, so there is no need to re-acquire the pilot to decode the
                              paging channel */
   QPCH_1_CMD,               /* Decode the Quick Paging CHannel bit, and also conduct a neighbor search */
   QPCH_2_CMD,               /* The first indicator was indeterminate, so Decode the Quick Paging CHannel again, but
                              do not conduct a neighbor search. */
   NGHBR_SRCH_ONLY_CMD,      /* Conduct a Candidate Frequency Check */
   QUERY_LONG_CODE_CMD,      /* Initially the CP needs the long code to calculate the long code for a future time when
                              we will hopefully wake up prior to! */
   DLY_TMR_EXPRD,            /* Signaled in MscIsr.asm, and sent by MscTask.c, but put here so that all messages sent
                              to the MscPage sub-unit are unique. */
   SRCH_BUFF_FULL,
   SRCH_CMPLT,
   SRCH_ACT_LIST_CMPLT,
   SRCH_NGHBR_LIST_CMPLT,
   SRCH_QPCH_DECODE_CMPLT,
   SRCH_CCI_DECODE_CMPLT,
   CCI_CMD,
   DECODE_FCCCH_CONT_CMD,    /* Decode the FCCCH continuously until commanded to stop (sleep) */
   DECODE_FCCCH_NO_ACQ_CMD,  /* This requires QPCH support. The assumption is that the QPCH has already acquired the
                              pilot not longer than 100mS, so there is no need to re-acquire the pilot to decode the
                              paging channel */
   RF_SHUTOFF_CMD
#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE_L1)
   ,
   ICS_NCOH_CMD,
   ICS_FDS_CMD,
   ICS_COH_CMD,
   SYNC_START_CMD,
   PCH_START_CMD,
   FCH_START_CMD,
   DFS_CONT_CMD,
   RF_LOST_CMD,
   UPDATE_LONG_CODE_CMD,
#ifdef MTK_CBP_SYNC_OPTIMIZE
   ICS_MINIACQ_CMD,
#endif
   ICS_RSSI_SCAN,
   SPAGE_CMD_NUM
#endif
}IpcDsmMscSpageCmdIdT;


#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE_L1)
/** The Enum of the 1xRTT Channel Type. */
typedef enum
{
    XL1_CHAN_START = 0,
    /** ICS Pilot NCOH Channel. */
    XL1_ICS_NCOH,
    /** ICS Pilot FDS Channel. */
    XL1_ICS_FDS,
    /** ICS Pilot COH Channel. */
    XL1_ICS_COH,
    /** ICS Sync Acq Channel. */
    XL1_ICS_SYNC_CHAN,
    /** Slotted Paging */
    XL1_PAGING,
    /** Slotted QPCH */
    XL1_QPCH,
    /** Non-Slotted PCh and Slotted PCH_Lost_Det Channel. */
    XL1_PCH_CONT,
    /** FCH channel. */
    XL1_FCH,
    /**  Inter Freq Search. */
    XL1_INTER_FREQ,
    /**  AFLT Meas. */
    XL1_AFLT,    
#ifdef MTK_CBP_SYNC_OPTIMIZE
	/** ICS Pilot mini acq channel. */
	XL1_ICS_MINI_ACQ,
#endif
    XL1_ICS_RSSI_SCAN,
	/** number of channel type. */
    XL1_CHAN_NUM
}MscSpageRfCtrlChannelTypeT;
#endif

typedef enum
{
   MSC_MODE_CDMA,
   MSC_MODE_GPS
} ModeT;                    /* For GPS: Indicate CDMA or GPS mode in IpcDsmMscSpageInitCmdMsgT */

typedef PACKED_PREFIX struct
{
   uint16                  MsgCmd;          /* INIT_CMD */
   uint16                  RxAgcDly;        /* Automatic Gain Control steady state delay. Asynchronously timed with the CP */
   uint16                  Mode;            /* MSC_MODE_CDMA: Init to Normal CDMA mode or MSC_MODE_GPS: GPS mode */
} PACKED_POSTFIX  IpcDsmMscSpageInitCmdMsgT;


typedef PACKED_PREFIX struct
{
   uint16                  MsgCmd;        /* STOP_CMD */
   uint16                  SysCntInit1;   /* Resync high order value for system counter, set before sleep, in TC/8  */
   uint16                  SysCntInit0;   /* Resync low order value for system counter , set before sleep  in TC/8  */
   uint16                  SysTimeInTC;   /* System Time in TC. */
   uint16                  SendSrchResults;
} PACKED_POSTFIX  IpcDsmMscSpageStopCmdMsgT;


typedef PACKED_PREFIX struct
{                                           /* DECODE_PCH_NO_ACQ_CMD or  */
   uint16                  MsgCmd;          /* DECODE_PCH_CONT_CMD       */
   uint16                  AgcActSymbTime;  /* (Absolute 80mS) symbol time when Agc shall be started (52uS ticks)*/
   uint16                  LongCode2;       /* current long code to be programmed at the superframe boundary */
   uint16                  LongCode1;       /* is received upon wakeup from sleep and programmed into the    */
   uint16                  LongCode0;       /* DSPM timer control registers */
   uint16                  Next20msLongCode2; /* Next20ms long code to be programmed at the superframe boundary */
   uint16                  Next20msLongCode1; /* is received upon wakeup from sleep and programmed into the    */
   uint16                  Next20msLongCode0; /* DSPM timer control registers */
#ifndef MTK_CBP
   int16                   TimeCorrection;  /* Mini-acq correction to be applied, tc/2 */
#endif
   uint16                  SymbArrival;     /* If DSPM receives message after SymbArrival, use the Next20msLongCode */
} PACKED_POSTFIX  IpcDsmMscSpageDecodePchMsgT;

typedef PACKED_PREFIX struct
{                                            /* QPCH_1_CMD or */
   uint16                  MsgCmd;          /* QPCH_2_CMD    */
   uint16                  AgcActSymbTime;  /* (Absolute 80mS) symbol time when Agc shall be started (52uS ticks)*/
   uint16                  QpageCh;         /* needs to be converted to Walsh Code */
   uint16                  QpchRate;        /* 0 =4800, 1=9600 */
   uint16                  QPRatio;         /* Qpch to Pilot Power Ratio in Q14 format */
#ifndef MTK_CBP
   int16                   TimeCorrection;  /* Mini-acq correction to be applied, tc/2 */
#endif
} PACKED_POSTFIX  IpcDsmMscSpageQpchCmdMsgT;

#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE_L1)
typedef PACKED_PREFIX struct
{
   uint16   MsgCmd;  /* new added members in MscSpageRfCtrlChannelTypeT */
   uint16   CtrlWord;/* AgcActSymbTime for start cmd,  MscSpageRfCtrlChannelType for RF_LOST*/
   uint16   FrameSize;/* frame size */
} PACKED_POSTFIX  IpcDsmMscSpageRfCtrlCmdMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MsgCmd;            /* UPDATE_LONG_CODE_CMD */
   uint16   LongCode2;         /* current long code to be programmed at the superframe boundary */
   uint16   LongCode1;         /* is received upon wakeup from sleep and programmed into the    */
   uint16   LongCode0;         /* DSPM timer control registers */
   uint16   Next20msLongCode2; /* Next20ms long code to be programmed at the superframe boundary */
   uint16   Next20msLongCode1; /* is received upon wakeup from sleep and programmed into the    */
   uint16   Next20msLongCode0; /* DSPM timer control registers */
   uint16   SymbArrival;       /* If DSPM receives message after SymbArrival, use the Next20msLongCode */
} PACKED_POSTFIX  IpcDsmMscSpageUpdateLcCmdMsgT;
#endif

typedef PACKED_PREFIX struct
{
   uint16                  MsgCmd;          /* CCI_CMD    */
   uint16                  ActSymbTime;     /* (Absolute 80mS) symbol time when Agc shall be started (52uS ticks)*/
   uint16                  CCICh;           /* needs to be converted to Walsh Code */
   uint16                  CCIRate;         /* 0 =4800, 1=9600 */
   int16                   TimeCorrection;  /* Mini-acq correction to be applied, tc/2 */   
} PACKED_POSTFIX  IpcDsmMscSpageCCICmdMsgT;

typedef PACKED_PREFIX struct
{
    uint16                  MsgCmd;
} PACKED_POSTFIX  IpcDsmMscSpageQueryLongCodeMsgT;

typedef PACKED_PREFIX struct
{
    uint16                  MsgCmd;
} PACKED_POSTFIX  IpcDsmMscSpageSignalMsgT;

typedef PACKED_PREFIX struct
{
    uint16                  MsgCmd;           /* RF_SHUTOFF_CMD */
} PACKED_POSTFIX  IpcDsmMscSpageRfShutoffMsgT;

typedef enum
{
    RF_RELEASE,               
    RF_PREEMPT              
} IpcDsmMscRfReleaseReqMsgCmdT;

/* IPC_DSM_RF_RELEASE_REQ_MSG */
typedef PACKED_PREFIX struct
{
   uint16                   MsgCmd;    /* MsgCmd = RF_RELEASE or RF_PREEMPT */
   uint16                   Req;
} PACKED_POSTFIX  IpcDsmMscRfReleaseReqMsgT;


/*---------------------------*
 | Slotted Paging interface  |
 |       DSPM -> CP          |
 *---------------------------*/
typedef enum
{
    QPCH_IND,                 /* Quick Paging Result Indicator */
    STOP_CMD_ACK,             /* After preparing for sleep, acknowledge the request for sleep */
    SYS_TIME_ERR_AFT_ACQ,     /* Return the differnce in system timing from the calculated and the finger's acquired time */
    LONG_CODE_STATE,
    PWR_OFF_MIX_SIG,
    RF_SHUTOFF_RSP
#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE_L1)
    ,RF_RESUME_SYS_TIME_ERR_AFT_ACQ
#endif
} IpcCpSpageRspnsIdMsgT;

typedef PACKED_PREFIX struct
{
   uint16  MsgResponse;      /* Response identifier = QPCH_IND */
   int16   QpchInd;          /* OFF = 0, ON = 1, Erasure = -1, Q0*/
   int16   QpchMetric;       /* X1 if first indicator, X3 if second indicator, Q0 */
   int16   PilotEnergy;      /* Usable path combined Pilot Energy, Q0 */
   uint16  Ec_Io;            /* Mini-Acq Pilot Ec/Io */
   uint16  NumPath;          /* Number of usable path */
   uint16  Qpch_Pilot_PN;    /* Pilot PN Offset */
} PACKED_POSTFIX  IpcCpSpageQpchIndMsgT;

typedef PACKED_PREFIX struct
{
   uint16  MsgResponse;      /* Response identifier = STOP_CMD_ACK */
} PACKED_POSTFIX  IpcCpSpageStopCmdAckMsgT;

typedef PACKED_PREFIX struct
{
   uint16                 MsgResponse;      /* Response identifier = SYS_TIME_ERR_AFT_ACQ */
   int16                  SysClkDevCntTC;   /* Actual Received Time counts in 1.2288Mchp/sec 14:0 bits*/
   int16                  SysClkDevCntTC8;  /* Actual Received Time counts in 9.8304MHz  2:0 bits*/
   uint16                 Ec_Io;            /* Mini-Acq Pilot Ec/Io */
} PACKED_POSTFIX  IpcCpSpageSysTimeDevMsgT;

typedef PACKED_PREFIX struct
{
   uint16                 MsgResponse;      /* Response identifier = LONG_CODE_STATE */
   uint16                 LongCode2;        /* long code bits 41:32                  */
   uint16                 LongCode1;        /* long code bits 31:16                  */
   uint16                 LongCode0;        /* long code bits 15:00                  */
} PACKED_POSTFIX  IpcCpSpageLongCodeStateMsgT;

typedef PACKED_PREFIX struct
{
   uint16                 MsgResponse;      /* Response identifier =  RF_SHUTOFF_RSP  */
} PACKED_POSTFIX  IpcCpSpageRfShutoffRspMsgT;


/*---------------------------*
 |   RF Release/Preemption   | 
 |       DSPM -> CP          |
 *---------------------------*/
/* IPC_CP_RF_RELEASE_RSP_MSG */
typedef PACKED_PREFIX struct
{
   uint16      Status;
   uint16      MsgCmd;
   uint16      Req;
} PACKED_POSTFIX  IpcCpMscRfReleaseRspMsgT;

#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE_L1)
typedef PACKED_PREFIX struct
{
   uint16     SearchStatus;       /* Sequence nuumber for co-ordination with list messages*/   
} PACKED_POSTFIX  IpcCpSearchStatusMsgT;

typedef PACKED_PREFIX struct
{
   int16     SchEbNt;       /* EbNt of Sync Channel*/   
} PACKED_POSTFIX  IpcCpSchEbNtMsgT;
#endif
/*--------------------------------------------------------------------------------**
** IpcDsmAfcCalibrParmMsgT afc parameters                                         **
**--------------------------------------------------------------------------------*/
#if !defined (MTK_CBP)||defined (MTK_PLT_ON_PC)
typedef PACKED_PREFIX struct    /* FQQ */
{
   int16        PdmPpmSlope;  /* conversion factor, Ppm frequency error => PDM setting */
   int16        PdmOfst;
   uint16       PdmMinValue;
   uint16       PdmMaxValue;
} PACKED_POSTFIX IpcDsmAfcCalibrParmMsgT;
#else
typedef PACKED_PREFIX struct    /* FQQ */
{
   uint16 InitDacValue;
   uint16 AfcSlopeInv;
   uint16 centerFreq;
} PACKED_POSTFIX IpcDsmAfcCalibrParmMsgT;
#endif
#if  SYS_OPTION_NULL_GPM
/*--------------------------------------------------------------------------------**
** NULLGPM_POWERSAVING                                                           **
**--------------------------------------------------------------------------------*/
/*--------------------------------*
 |  NullGpm PowerSave CP - > DSPM |
 |  IPC_CP_IP2_CAL_REPORT_MSG     |
 *--------------------------------*/
typedef PACKED_PREFIX struct
{
    uint16 Switch;       /*0: off the null GPM power save; 1: on */
} PACKED_POSTFIX IpcDsmNullGpmPowerSaveMsgT;
/*--------------------------------*
 |  NullGpm PowerSave DSPM - > CP |
 |  IPC_CP_IP2_CAL_REPORT_MSG     |
 *--------------------------------*/
typedef PACKED_PREFIX struct
{
    uint16 NullGpmHalfFrame0[2];     
    uint16 NullGpmHalfFrame1[2];  
} PACKED_POSTFIX IpcDsmNullGpmIndMsgT;
#endif

/*--------------------------------------------------------------------------------**
** Interface to Interference Canceller (IC) unit                                  **
**--------------------------------------------------------------------------------*/
/* Defines for RF IC coefficient Table */
#define HWD_MAX_IC_COEF  41
typedef enum
{
    NORMAL_IC,               
    FNGR_SRCH_DLY_BUF_OUT_IC,
    FNGR_IC_DLY_BUF_OUT_IC,
    BYPASS_IC
#if MTK_CBP_SYNC_OPTIMIZE
	, NUM_IC
#endif   
} IpcIcEnableModeT;

typedef PACKED_PREFIX struct
{
   uint16    IcDisable; /* 0: IC Enable, 1: IC Disable */
   uint16    IcEnableMode;
} PACKED_POSTFIX  IpcIcControlMsgT;

typedef PACKED_PREFIX struct
{
   int16    RfIcControlTbl[HWD_MAX_IC_COEF];
} PACKED_POSTFIX  IpcIcFilterMsgT;

#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))

typedef PACKED_PREFIX struct
{
    /* dummy  for temp meas start*/
    uint16 dummy ; 
} PACKED_POSTFIX  IpcTempStartMsgT;

typedef PACKED_PREFIX struct
{
     /* ADC output  for temp result*/
    uint16 TempResult ; 
} PACKED_POSTFIX  IpcTempReportMsgT;


typedef PACKED_PREFIX struct
{
    /* Tx status*/
    uint16 TxStatus ; 
} PACKED_POSTFIX  IpcGsmBlockTxStatusMsgT;

typedef PACKED_PREFIX struct
{
    /*uint is 2Hz*/
    int16  Foe2Hz; 
    /*Indicate coarse or fine AFC*/
    uint16 IsCoarseAfc;
} PACKED_POSTFIX  IpcAfcReportMsgT;

#endif
#if defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC)) && defined(MTK_DEV_DUMP_REG)
typedef PACKED_PREFIX struct
{
    uint16   regTypeBitMap;
} PACKED_POSTFIX IpcDspmHwdDbgT;

typedef PACKED_PREFIX struct
{
    uint16 addr;
    uint16 data;
} PACKED_POSTFIX IpcDspmRegSetT;

typedef PACKED_PREFIX struct
{
    uint16 regNum;
    uint16 addr[8];
} PACKED_POSTFIX IpcDspmRegSpyT;

typedef PACKED_PREFIX struct
{
    uint16 typeAll;
    uint16 startAddr;
    uint16 regNum;
} PACKED_POSTFIX IpcDspmRegSpyBlockT;

#endif

#if defined(MTK_CBP)&&(!defined(MTK_PLT_ON_PC))&&(!defined(MTK_PLT_RF_ORIONC)||defined(MTK_DEV_HW_SIM_RF))
/*----------------------------------**
** IPC_DSM_POC_TX_PARM_MSG Message **
**-----------------------------------*/
#define RF_TX_PGA_SLICE_NUM_MAX    (10)
typedef struct
{
  /* TX IQDC, config in TXDFE */
  HwdIqDcCalibParmT txIqDcTbl[RF_TX_PGA_SLICE_NUM_MAX];

  /* TX PGA phase error*/
  int16 txPgaPhaseErr;

  /* TX RC LPF Cal result*/
  uint16 txRcLpfCsel;
	
}IpcDsmTxPocParmMsgT;

/*----------------------------------**
** IPC_DSM_POC_DET_DFE_PARM_MSG Message **
**-----------------------------------*/
#define RF_DET_FE_GAIN_STEPS_MAX    (10)
#define RF_DET_AGC_TABLE_SIZE_MAX   (20)
typedef struct
{
	/* DET IQ imbalance with Tx path forward signal, config in TXDFE  */
	HwdDetIqCalParmT detIqFwd[RF_DET_FE_GAIN_STEPS_MAX];
	/* DET fine DC with Tx path forward signal, config in TXDFE */
	HwdDetDcCalParmT detDcFwd[RF_DET_AGC_TABLE_SIZE_MAX];
	/* DET IQ imbalance with Tx path reverse signal, config in TXDFE  */
	HwdDetIqCalParmT detIqRev[RF_DET_FE_GAIN_STEPS_MAX];
	/* DET fine DC with Tx path reverse signal, config in TXDFE */
	HwdDetDcCalParmT detDcRev[RF_DET_AGC_TABLE_SIZE_MAX];
}IpcDsmDetPocParmMsgT;

#endif

#if (SYS_BOARD >= SB_JADE)&&(!defined(MTK_PLT_ON_PC))
typedef enum
{
   IPC_COCLOCK_ON,
   IPC_COCLOCK_OFF
}IpcColckModeT;

/* IPC_DSM_CLOCK_MODE_CONFIG_MSG Message type */
typedef PACKED_PREFIX struct
{
    uint16          SbpClockMode; 
} PACKED_POSTFIX IpcDsmClockModeConfigMsgT;
#endif
#if (SYS_BOARD >= SB_JADE)
/* IPC_DSM_RX_POWER_MODE_CONFIG_MSG Message type */
typedef PACKED_PREFIX struct
{
    uint16          IsLowPowrMode;
} PACKED_POSTFIX IpcDsmRxPowrModeConfigMsgT;
#endif

#if defined(MTK_DEV_C2K_IRAT) && defined(MTK_DEV_C2K_SRLTE)
typedef PACKED struct
{
   uint16     Channel;       /* Sequence nuumber for co-ordination with list messages*/  
   int16      RxPowerDbmQ6;
} IpcCpRssiScanReportMsgT;
#endif

#endif
/*****************************************************************************
* End of File
*****************************************************************************/

/**Log information: \main\Trophy\Trophy_ylxiao_href22033\1 2013-03-18 14:18:03 GMT ylxiao
** HREF#22033, merge 4.6.0**/
/**Log information: \main\Trophy\1 2013-03-19 05:22:35 GMT hzhang
** HREF#22033 to merge 0.4.6 code from SD.**/
