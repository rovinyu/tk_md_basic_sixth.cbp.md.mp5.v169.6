/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.  
* 
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
* 
* Copyright (c) 2002-2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _DSPVAPI_H_
#define _DSPVAPI_H_
/*****************************************************************************

  FILE NAME: dspvapi.h
 
  DESCRIPTION:

    This file contains CP - DSPV constants/message/structure definitions.

    NOTE: This file is shared between the DSPV and the CP

*****************************************************************************/

#include "sysdefs.h"
#include "dspiparm.h"
#include "dspiapi.h"

#define DSPV_COMPATIBILITY_ID 21                  

/*==================================================================*/
/* DSPV MAILBOXES                                                   */
/*==================================================================*/

/*---------------------------------------------------------------------------*
 +-----------------------------------------------------+ HWD_MBV_DAT_TX_BASE (used by CP)
 |                                                     |
 |                                                     |
 | CP - > DV Control Mbox        316                   |    IPC_CTL_MBOX_C2V_SIZE
 |                                                     |
 |                                                     |
 +-----------------------------------------------------+
 |                                                     |
 | CP - > DV Fast Mbox             4                   |
 |                                                     |
 +-----------------------------------------------------+
 |                                                     |
 | Not Used                                            |
 |                                                     |
 +-----------------------------------------------------+ HWD_MBV_DAT_RX_BASE (used by CP)=HWD_MBV_DAT_TX_BASE+0x180
 |                                                     |
 |                                                     |
 | DV - > CP Control Mbox        260                   |    IPC_CTL_MBOX_V2C_SIZE
 |                                                     |
 |                                                     |
 +-----------------------------------------------------+ HWD_MBV_FIQ_DAT_RX_BASE (used by CP)
 |                                                     |
 | DV - > CP Fast Mbox             4                   |
 |                                                     |
 +-----------------------------------------------------+
   TOTAL                         584
 *---------------------------------------------------------------------------*/


/* Size of dspv rev control mailbox in uint16 */
#define IPC_CTL_MBOX_C2V_SIZE    316

/* Size of dspv rev control mailbox in uint16 */
#define IPC_CTL_MBOX_V2C_SIZE    260

/* Offset of dspv fwd control mailbox in uint16 */
#define IPC_CTL_MBOX_V2C_OFFSET  0x180

/* Maximum dspv fwd control mailbox message size (minus mailbox header and message header sizes) in uint16 */
#define IPC_MSG_V2C_MAX_SIZE (IPC_CTL_MBOX_V2C_SIZE - IPC_MBOX_HEADER_SIZE  - IPC_MSG_HEADER_SIZE)
/* Maximum dspv rev control mailbox message size (minus mailbox header and message header sizes) in uint16 */
#define IPC_MSG_C2V_MAX_SIZE (IPC_CTL_MBOX_C2V_SIZE - IPC_MBOX_HEADER_SIZE  - IPC_MSG_HEADER_SIZE)

#define PCM_DATA_FRAME_SIZE_WORDS 160

/*---------------------------------------------------------------**
** Control Mailbox Message Ids: Control Processor to DSPV        **
**---------------------------------------------------------------*/
typedef enum
{

   /* Ringer Task */
   IPC_DSV_RNGR_INIT_MSG                         =IPC_DSV_RNGR_MIN_MSG,
   IPC_DSV_RNGR_MODE_MSG,
   IPC_DSV_RNGR_CONFIG_MSG,
   IPC_DSV_RESERVED_30,
   IPC_DSV_RESERVED_31,

   /* Application Task message */
   IPC_DSV_APP_MODE_MSG                          =IPC_DSV_APP_MIN_MSG,
   IPC_DSV_APP_CONFIGURE_APPCHANNELS_RSP_MSG,
   IPC_DSV_APP_IMAGE_SIZE_RSP_MSG,
   IPC_DSV_APP_VOL_SCAL_FCTR_MSG,
   IPC_DSV_APP_INFO_QUERY_MSG,
   IPC_DSV_SEND_RNGR_VOL_SCAL_FCTR_MSG,
   IPC_DSV_SEND_RNGR_LDSPKR_VOL_SCAL_FCTR_MSG,
   IPC_DSV_I2S_DELAY_MODE_MSG,
   IPC_DSV_SET_MIC_VOL_SCAL_FCTR_MSG,
   IPC_DSV_SEND_SPKR_VOL_SCAL_FCTR_MSG,
   IPC_DSV_SEND_AUDIO_SIDETONE_SCAL_FCTR_MSG,
   IPC_DSV_APP_HIFI_DAC_SAMPLING_RATE_RSP_MSG,
   IPC_DSV_VOLUME_SCAL_FCTR_MSG,
   IPC_DSV_APP_AUDIO_PATH_CFG_MSG,
   IPC_DSV_APP_DEBUG_DNLD_MSG,
   IPC_DSV_APP_BLUETOOTH_CONFIG_MSG,
   IPC_DSV_APP_BITSTREAM_RESIDUAL_MSG,
   IPC_DSV_APP_MIXER_DATA_MSG,
   IPC_DSV_APP_MIXER_CTRL_MSG,

   /* Microphone Path (encoder) */
   IPC_DSV_SET_MIC_FIF_MODE_MSG                  =IPC_DSV_VAP_MPP_MIN_MSG,
   IPC_DSV_SEND_MIC_FIF_CFG_PARAMS_MSG,
   IPC_DSV_SET_MIC_ANS_MODE_MSG,
   IPC_DSV_SEND_MIC_ANS_CFG_PARAMS_MSG,
   IPC_DSV_SET_MIC_BIF_MODE_MSG,
   IPC_DSV_SEND_MIC_BIF_CFG_PARAMS_MSG,
   IPC_DSV_RESERVED_32,
   IPC_DSV_RESERVED_1,
   IPC_DSV_RESERVED_2,
   IPC_DSV_SEND_MPP_TONE_GEN_PARAMS_MSG,
   IPC_DSV_SET_MIC_VOICE_ENC_MAX_RATE_MSG,
   IPC_DSV_SET_MIC_VOICE_ENC_MIN_RATE_MSG,
   IPC_DSV_RESERVED_3,
   IPC_DSV_SEND_MIC_SIM_MSG_MSG,
   IPC_DSV_RESERVED_4,
   IPC_DSV_RESERVED_5,
   IPC_DSV_SET_REV_CH_SSO_CONFIG_MSG,
   IPC_DSV_SET_MIC_AMP_REV_VOL_SCAL_FCTR_MSG,
   IPC_DSV_SEND_MIC_PC_DATA_MSG,

   /* Speaker Path (decoder) */
   IPC_DSV_SEND_SPKR_VOICE_FWD_CH_PCKT_DATA_MSG  =IPC_DSV_VAP_SPP_MIN_MSG,
   IPC_DSV_RESERVED_6,
   IPC_DSV_RESERVED_7,
   IPC_DSV_SEND_SPKR_VOICE_RECORD_PB_PCKT_DATA_MSG,
   IPC_DSV_SEND_SPP_TONE_GEN_PARAMS_MSG,
   IPC_DSV_RESERVED_8,
   IPC_DSV_SET_SPKR_FIF_MODE_MSG,
   IPC_DSV_SEND_SPKR_FIF_CFG_PARAMS_MSG,
   IPC_DSV_RESERVED_33,
   IPC_DSV_SEND_FWD_CH_SSO_CONFIGURATION_MSG,
   IPC_DSV_RESERVED_25,
   IPC_DSV_SEND_SPKR_AMP_FWD_VOL_SCAL_FCTR_MSG,
   IPC_DSV_SEND_SPP_TONE_GEN_MSG,
   IPC_DSV_SET_PCM_IF_API_MODE_MSG,
   IPC_DSV_SPKR_PATH_CP2DSPV_PCM_DATA_MSG,
   IPC_DSV_LPBK_CP2DSPV_SEND_SPKR_PCM_DATA_MSG,
   IPC_DSV_SEND_ABE_MODE_MSG,

   /* General Audio Path (both microphone and speaker) */
   IPC_DSV_SET_AUDIO_AIR_INTERFACE_MODE_MSG      =IPC_DSV_VAP_AUDIO_MIN_MSG,
   IPC_DSV_SEND_AUDIO_SSO_CONNECT_MSG,
   IPC_DSV_SEND_AUDIO_SSO_DISCONNECT_MSG,
   IPC_DSV_SEND_AUDIO_SSO_CTRL_PARAMS_MSG,
   IPC_DSV_RESERVED_34,
   IPC_DSV_SET_AUDIO_LOOPBACK_MODE_MSG,
   IPC_DSV_SEND_AUDIO_CONFIG_QUERY_MSG,
   IPC_DSV_SEND_AUDIO_SPCH_ROUTING_MSG,
   IPC_DSV_SET_AUDIO_AEC_MODE_MSG,
   IPC_DSV_SEND_AUDIO_AEC_MAIN_CFG_PARAMS_MSG,
   IPC_DSV_SET_AUDIO_RECORD_PLAYBACK_MODE_MSG,
   IPC_DSV_SEND_SPKR_SIM_MSG_MSG,

   IPC_DSV_RESERVED_40,
   IPC_DSV_RESERVED_41,
   IPC_DSV_SET_AUDIO_TTY_MODE_MSG,
   IPC_DSV_SEND_AUDIO_TTY_CFG_PARAMS_MSG,
   IPC_DSV_SEND_AUDIO_VAP_CFG_PARAMS_MSG,
   IPC_DSV_SEND_AUDIO_ACP_CFG_PARAMS_MSG,
   IPC_DSV_SET_AUDIO_ACP_MODE_MSG,
   IPC_DSV_RESERVED_14,
   IPC_DSV_RESERVED_15,
   IPC_DSV_RESERVED_23,
   IPC_DSV_RESERVED_24,
   IPC_DSV_SEND_AUDIO_PATH_CFG_MSG,
   IPC_DSV_VOICE_FRAME_RESYNC_MSG,
   IPC_DSV_RESERVED_17,
   IPC_DSV_SEND_AUDIO_AEC_AUX_CFG_PARAMS_MSG,
   IPC_DSV_SEND_AUDIO_PC_MODE_MSG,
   IPC_DSV_VAP_AMR_CONFIG_MSG,
   IPC_DSV_SEND_AUDIO_CHAN_QLTY_MSG,

   /* AMPS Related IPC message IDs */
   IPC_DSV_AMP_SEND_REV_CH_SIM_MSG            =IPC_DSV_AMP_REV_CH_MIN_MSG,
   IPC_DSV_AMP_SEND_WBD_RECC_MSG,
   IPC_DSV_AMP_SEND_WBD_RVC_MSG,
   IPC_DSV_AMP_RECC_ABORT_MSG,
   IPC_DSV_AMP_REV_CH_PARAMS_CONFIG_MSG,       /* Configuration message */
   IPC_DSV_AMP_COMPRS_MODE_MSG,
   IPC_DSV_AMP_TRANSMIT_FIXED_FREQ_MSG,
   IPC_DSV_AMP_ST_MODE_MSG,
   IPC_DSV_AMP_REV_CH_CTRL_CONFIG_MSG,
   IPC_DSV_AMP_GET_COMP_EXP_STATUS_MSG,

   IPC_DSV_AMP_SEND_FWD_CH_SIM_MSG            =IPC_DSV_AMP_FWD_CH_MIN_MSG,
   IPC_DSV_AMP_MODE_MSG,
   IPC_DSV_AMP_BUSY_IDLE_PROCESS_MSG,
   IPC_DSV_AMP_GET_FM_SIGNAL_STRENGTH_MSG,
   IPC_DSV_AMP_FWD_IQ_DC_COMP_MSG,
   IPC_DSV_AMP_RX_AGC_CTRL_MSG,
   IPC_DSV_AMP_AFC_CTRL_MSG,
   IPC_DSV_AMP_FWD_CH_COUNTER_MSG,
   IPC_DSV_AMP_FWD_CH_PARAMS_CONFIG_MSG,       /* Configuration message */
   IPC_DSV_AMP_FWD_CH_AFC_CALIB_MSG,
   IPC_DSV_RESERVED_11,
   IPC_DSV_AMP_EXPANDER_MODE_MSG,
   IPC_DSV_AMP_HPF_MODE_MSG,
   IPC_DSV_AMP_SAT_MODE_MSG,
   IPC_DSV_AMP_SET_WS_COUNTER_MSG,
   IPC_DSV_AMP_GET_WS_COUNTER_MSG,
   IPC_DSV_AMP_FWD_LPF_MODE_MSG,
   IPC_DSV_AMP_SET_PDM_MSG,
   IPC_DSV_AMP_FWD_CH_CTRL_CONFIG_MSG,
   IPC_DSV_AMP_FWD_CH_AGC_CALIB_MSG,
   IPC_DSV_RESERVED_12,
   IPC_DSV_RESERVED_13,
   IPC_DSV_AMP_FWD_BUSY_IDLE_STATUS_MSG
} IpcDspvMsgIdT;




#define  IPC_ACF_LEN_MAX            17



/*---------------------------------------------------------------**
** Control Mailbox Message Ids: DSP to Control Processor (_CP_)  **
**---------------------------------------------------------------*/


typedef enum
{
   IPC_CP_HW_MBOX_CFG_PARAMS_MSG             =IPC_CP_FROM_DSV_MIN_MSG,
   IPC_CP_SPCH_DATA_REV_CH_PACKET_MSG,
   IPC_CP_AUDIO_SSO_CONNECT_RSP_MSG,
   IPC_CP_AUDIO_SSO_DISCONNECT_RSP_MSG,
   IPC_CP_SEND_RECORD_PLAYBACK_DATA_MSG,
   IPC_CP_SEND_AUDIO_PATH_CFG_RSP_MSG,
   IPC_CP_AMP_MODE_RSP_MSG,
   IPC_CP_RESERVED_42,
   IPC_CP_AMP_SAT_DET_RSP_MSG,
   IPC_CP_AMP_FWD_CH_DATA_RSP_MSG,
   IPC_CP_AMP_BUSY_IDLE_CHANGE_RSP_MSG,
   IPC_CP_AMP_SIGNAL_STRENGTH_RSP_MSG,
   IPC_CP_SEND_AUDIO_CONFIG_QUERY_RSP_MSG,
   IPC_CP_AMP_WORD_SYNC_STATUS_RSP_MSG,
   IPC_CP_AMP_COMP_EXP_STATUS_RSP_MSG,
   IPC_CP_AMP_AFC_STATUS_RSP_MSG,
   IPC_CP_AMP_FWD_BUSY_IDLE_STATUS_RSP_MSG,
   IPC_CP_DV_RNGR_STATUS_MSG,     /* DV Signal for passing IpcDsvRingerStatusMsgT to CP */
   IPC_CP_APP_MODE_RSP_MSG,
   IPC_CP_APP_CONFIGURE_APPCHANNELS_MSG,
   IPC_CP_APP_IMAGE_SIZE_MSG,
   IPC_CP_APP_STATUS_MSG,
   IPC_CP_APP_INFO_QUERY_RSP_MSG,
   IPC_CP_SEND_SPP_TONE_GEN_RSP_MSG,
   IPC_CP_SEND_SPKR_PC_DATA_MSG,
   IPC_CP_APP_HIFI_DAC_SAMPLING_RATE_MSG,
   IPC_CP_VOLUME_SCAL_FCTR_RSP_MSG,
   IPC_CP_APP_AUDIO_PATH_CFG_RSP_MSG,
   IPC_CP_VOICE_FRAME_RESYNC_RSP_MSG,
   IPC_CP_AUDIO_RECORD_PLAYBACK_MODE_RSP_MSG,
   IPC_CP_APP_BLUETOOTH_CONFIG_RSP_MSG,
   IPC_CP_APP_BITSTREAM_RESIDUAL_RSP_MSG,
   IPC_CP_SPKR_PATH_DSPV2CP_PCM_DATA_MSG,
   IPC_CP_SET_PCM_IF_API_MODE_RSP_MSG,
   IPC_CP_SPKR_PATH_CP2DSPV_PCM_DATA_RSP_MSG,
   IPC_CP_LPBK_DSPV2CP_SEND_MIC_PCM_DATA_MSG,
   IPC_CP_MIXER_DATA_REQ_MSG,
   IPC_CP_DSPV_APP_REQ_MSG
} IpcCpvMsgIdT;

typedef enum
{
   IPC_SPCH_SRVC_OPTION_NULL   = 0,       /* Invalid service option  */
   IPC_SPCH_SRVC_OPTION1       = 1,       /* SO1.   QCELP8. not supported  */
   IPC_SPCH_SRVC_OPTION3       = 3,       /* SO3.   EVRC-A */
   IPC_SPCH_SRVC_OPTION17      = 17,      /* SO17.  QCELP13 */
   IPC_SPCH_SRVC_OPTION68      = 68,      /* SO68.  EVRC-B */
   IPC_SPCH_SRVC_OPTION73      = 73,      /* SO73.  EVRC-NB */
   IPC_SPCH_SRVC_OPTION73WB    = 173,     /* SO73.  EVRC-WB */
   IPC_SPCH_SRVC_OPTION_AMR    = 100,     /* WCDMA. AMR */
   IPC_SPCH_SRVC_OPTION_AMRWB  = 101,     /* WCDMA. AMR-WB */
   IPC_SPCH_SRVC_OPTION_GSMFR  = 256,     /* GSM.   GSMFR */
   IPC_SPCH_SRVC_OPTION_GSMEFR = 257,     /* GSM    GSMEFR */
   IPC_SPCH_SRVC_OPTION_GSMHR  = 258      /* GSM.   GSMHR */  
#ifdef MTK_CBP_ENCRYPT_VOICE
   ,     /* GSM.   GSMHR */
   IPC_SPCH_SRVC_OPTION32944  =32944     /*SO0x80b0 Voice Encryption*/
#endif
} IpcSpchSrvcOptT;

typedef enum 
{
   IPC_SPCH_BLANK        = 0x0,
   IPC_SPCH_EIGHTH       = 0x1,
   IPC_SPCH_QUARTER      = 0x2,   /* not used for SSO 3 */
   IPC_SPCH_HALF         = 0x3,
   IPC_SPCH_FULL         = 0x4,
#ifdef MTK_CBP_ENCRYPT_VOICE
   IPC_SPCH_ENCRYPT_BLK  = 0x5,   /*used only for SO32944*/
   IPC_SPCH_ENCRYPT_FULL = 0X6,  /*used only for SO32944*/
#endif
   IPC_SPCH_NON_CRITICAL = 0xC,
   IPC_SPCH_ERASURE      = 0xE,
   IPC_SPCH_FULL_LIKELY  = 0xF    /* used only for SSO's 1&3 */
} IpcSpchSrvcOptRateT;

/*---------------------------------------------------------------------**
** Control Mailbox Message Stuctures: Control Processor to DSP (_DSP_) **
**---------------------------------------------------------------------*/

/*--------------------------------------**
** DSPV to CP Message Data Structures   **
**--------------------------------------*/

/*----------------------------**
** Voice And Audio Processing **
**----------------------------*/

#define  IPC_SPCH_PCKT_LEN_MAX      31
#define  IPC_RS_PCKT_LEN_MAX        5

typedef enum
{
   IPC_FIF_MODE_OFF = 0,
   IPC_FIF_MODE_ON  = 1
} IpcFrontendFilterModeT;

typedef enum
{
   IPC_NOISE_SUPPRESS_OFF = 0,
   IPC_NOISE_SUPPRESS_ON  = 1
} IpcNoiseSuppressModeT;

typedef enum
{
   IPC_BIF_MODE_OFF = 0,
   IPC_BIF_MODE_ON  = 1
} IpcBackendFilterModeT;

typedef enum
{
   /* Q8 format, 0.75db per step */
   IPC_VOLSCALE_MUTE   = 0x0000, /* fully mute */
   IPC_VOLSCALE_0_00DB = 0x0100, /* 0db, DEFAULT */
   IPC_VOLSCALE_0_75DB = 0x00EB, /* 0.75db */
   IPC_VOLSCALE_1_50DB = 0x00D7, /* 1.50db */
   IPC_VOLSCALE_2_25DB = 0x00C6  /* 2.25db */
} IpcVolScaleFactorT;

typedef enum
{
   IPC_AUDIO_AGC_OFF = 0,
   IPC_AUDIO_AGC_ON  = 1
} IpcAudioAgcModeT;

typedef enum
{
   IPC_AUDIO_ACP_COMP_OFF = 0,
   IPC_AUDIO_ACP_COMP_ON  = 1
} IpcAudioAcpCompModeT;

typedef enum
{
   IPC_AUDIO_ACP_ASVC_OFF = 0,
   IPC_AUDIO_ACP_ASVC_ON  = 1
} IpcAudioAcpAsvcModeT;


#define IPC_FRAC_PITCH_OFF 0
#define IPC_FRAC_PITCH_ON  1

#define IPC_LPC_IR_MAX_LEN 20
#define IPC_LPC_IR_MIN_LEN 0

#define  IPC_LPC_IR_OPT_EN_OFF FALSE
#define  IPC_LPC_IR_OPT_EN_ON  TRUE

#define  IPC_FCB_SUB_SAMP_EN_OFF  FALSE
#define  IPC_FCB_SUB_SAMP_EN_ON   TRUE

typedef enum
{
   IPC_PITCH_PRE_FLTR_OFF = 0,
   IPC_PITCH_PRE_FLTR_ON  = 1
} IpcPitchPreFltrModeT;

typedef enum
{
   IPC_SYN_POST_FLTR_OFF = 0,
   IPC_SYN_POST_FLTR_ON  = 1
} IpcPostSynFltrModeT;

typedef enum
{
   IPC_AIR_INTERFACE_MODE_CDMA = 0,
   IPC_AIR_INTERFACE_MODE_AMPS = 1
} IpcAirInterfaceModeT;

#define  IPC_SSO_CTRL_WRD_SIZ_MAX   1
#define  IPC_SSO_CTRL_RRM_MSK       0x00E0
#define  IPC_SSO_CTRL_RRM_SHFT      5
#define  IPC_SSO_CTRL_RSVD_MSK      0x001C
#define  IPC_SSO_CTRL_RSVD_SHFT     2
#define  IPC_SSO_CTRL_M2M_MSK       0x0002
#define  IPC_SSO_CTRL_M2M_SHFT      1
#define  IPC_SSO_CTRL_INIT_MSK      0x0001
#define  IPC_SSO_CTRL_INIT_SHFT     0

typedef enum 
{
   IPC_RS1_RRM_LEVEL0 = 0,    /* 9.3 kbps average encoding rate */
   IPC_RS1_RRM_LEVEL1 = 1,    /* 8.5 kbps average encoding rate */
   IPC_RS1_RRM_LEVEL2 = 2,    /* 7.5 kbps average encoding rate */
   IPC_RS1_RRM_LEVEL3 = 3,    /* 7.0 kbps average encoding rate */
   IPC_RS1_RRM_LEVEL4 = 4,    /* 6.6 kbps average encoding rate */
   IPC_RS1_RRM_LEVEL5 = 5,    /* 6.2 kbps average encoding rate */
   IPC_RS1_RRM_LEVEL6 = 6,    /* 5.8 kbps average encoding rate */
   IPC_RS1_RRM_LEVEL7 = 7     /* 4.8 kbps average encoding rate */
} IpcRs1RrmLevelT;

typedef enum 
{
   IPC_RS2_RRM_LEVEL0 = 0,    /* 14.4 kbps average encoding rate */
   IPC_RS2_RRM_LEVEL1 = 1,    /* 12.2 kbps average encoding rate */
   IPC_RS2_RRM_LEVEL2 = 2,    /* 11.2 kbps average encoding rate */
   IPC_RS2_RRM_LEVEL3 = 3,    /*  9.0 kbps average encoding rate */
   IPC_RS2_RRM_LEVEL4 = 4     /*  7.2 kbps average encoding rate */
} IpcRs2RrmLevelT;

typedef enum 
{
   IPC_M2M_PROC_OFF = 0,
   IPC_M2M_PROC_ON  = 1
} IpcM2mModeT;

typedef enum 
{
   IPC_INIT_CODEC_OFF = 0,
   IPC_INIT_CODEC_ON  = 1
} IpcInitCodecModeT;

typedef enum
{
    IPC_DISABLE_LPBK               = 0, /* all loopback modes disabled */
    IPC_MIXED_SIGNAL_HW_LPBK       = 1, /* mixed-signal hw loopback mode enabled */
    IPC_DIGITAL_HW_LPBK            = 2, /* digital hw loopback mode enabled */
    IPC_SAMPLE_ISR_LPBK            = 3, /* sample isr loopback mode enabled */
    IPC_SSO_LPBK                   = 4, /* speech service option (vocoder) loopback mode enabled */
    IPC_GSM_TEST_LPBK              = 5, /* GSM test loopback. DSPv don't send packet to CP */
    IPC_GSM_MODEM_LPBK             = 6, /* GSM loopback from DSPg */
    IPC_MULTIPLEX_SUBLAYER_LPBK    = 7, /* multiplex sublayer loopback mode enabled */
    IPC_MPP_SPP_LPBK               = 8, /* Mpp to Spp voice loopback without any audio processing */
    IPC_RESERVED_LPBK              = 9, /* Reserved loopback */
    IPC_PCM_DELAY_LPBK             = 10 /* PCM delay loopback */
} IpcAudioLoopBackModeT;

typedef enum
{
   IPC_HANDSFREE_OFF = 0,
   IPC_HANDSFREE_ON  = 1
} IpcHandsFreeModeT;

typedef enum 
{
   IPC_ECHO_CANCEL_OFF     = 0,
   IPC_ECHO_CANCEL_ON      = 1

} IpcEchoCancelModeT;


typedef enum
{
   IPC_RECORD_PLAYBACK_DISABLED    = 0,
   IPC_VOICE_RECORD_MODE           = 1,
   IPC_VOICE_PLAYBACK_MODE         = 2,
   IPC_VOICE_RECORD_PLAYBACK_MODE  = 3
} IpcVoiceRecordPlaybackModeT;

typedef enum
{
   IPC_VOICE_PACKETS_ONLY      = 0,
   IPC_VOICE_PCM_ONLY          = 1,
   IPC_VOICE_PACKETS_PCM_BOTH  = 2
} IpcVoiceRecPlayDataTypeT;

typedef enum
{
   IPC_VOICE_REC_SEL_PATH_REV       = 0,
   IPC_VOICE_REC_SEL_PATH_FWD       = 1,
   IPC_VOICE_REC_SEL_PATH_BOTH      = 2
} IpcVoiceRecSelPathModeT;

typedef enum
{
   IPC_VOICE_MUTE_AT_PLAYBACK        = 0,
   IPC_VOICE_MIX_SPKR_AT_PLAYBACK    = 1,
   IPC_VOICE_MIX_MIC_AT_PLAYBACK     = 2
} IpcVoiceRecPlaybackVoiceMixT;

typedef enum
{
   IPC_TTY_OFF = 0,
   IPC_TTY_ON  = 1
} IpcTtyModeT;

/*------------------------**
** Monitor and Diagnostic **
**------------------------*/
/* Mailbox size msg */
typedef PACKED_PREFIX struct
{
   uint16       ProcessorId;
   uint16       DspHwMboxUplinkSize;    
   uint16       DspHwMboxDownlinkSize;    
} PACKED_POSTFIX  IpcCpHWMboxCfgParamsMsgT;

/* This is the dynamic mailbox test message */
typedef PACKED_PREFIX struct
{
   uint16       ProcessorId;
   uint16       DspHwMboxUplinkSize;    
   uint16       DspHwMboxDownlinkSize;    
} PACKED_POSTFIX  IpcDspMboxSizeMsgT;

/*** ======================== ***/
/*** Microphone Path Messages ***/
/*** ======================== ***/

typedef PACKED_PREFIX struct
{
   uint16   MicFifMode;             /* front-end IIR filter mode switch */
} PACKED_POSTFIX  IpcDsvSetMicFifModeMsgT;

#define IPC_NUM_MIC_FIF_BQ_COEFFS  18
typedef PACKED_PREFIX struct
{
   uint16   MicFifNumBqSects;             /* number of biquad filter sections */
   uint16   MicFifBqCoefs[IPC_NUM_MIC_FIF_BQ_COEFFS];  /* filter coefficients */
} PACKED_POSTFIX  IpcDsvSendMicFifCfgParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MicAnsMode;             /* acoustic noise suppression mode switch */
} PACKED_POSTFIX  IpcDsvSetMicAnsModeMsgT;


typedef PACKED_PREFIX struct
{
   uint16   AnsVmThres;
   uint16   AnsTceThres;
   uint16   AnsSpecDevThres;
   uint16   AnsCntThres;
   uint16   AnsHysCntThres;
   uint16   AnsGainMin;
   uint16   AnsGainFactor;
} PACKED_POSTFIX  IpcDsvSendMicAnsCfgParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MicBifMode;             /* back-end IIR filter mode switch */
} PACKED_POSTFIX  IpcDsvSetMicBifModeMsgT;

#define IPC_NUM_MIC_BIF_BQ_COEFFS  18
typedef PACKED_PREFIX struct
{
   uint16   MicBifNumBqSects;             /* number of biquad filter sections */
   uint16   MicBifBqCoefs[IPC_NUM_MIC_BIF_BQ_COEFFS];  /* filter coefficients */
} PACKED_POSTFIX  IpcDsvSendMicBifCfgParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MicVolScalFctr;         /* software volume scale factor */
} PACKED_POSTFIX  IpcDsvSendMicVolScalFctrMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AmpsRevVolScalFctr;         /* software volume scale factor */
} PACKED_POSTFIX  IpcDsvSendMicAmpsRevVolScalFctrMsgT;


typedef PACKED_PREFIX struct
{
   uint16   MicAgcMode;             /* microphone AGC mode switch */
} PACKED_POSTFIX  IpcDsvSetMicAudioAgcModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   tbd;
} PACKED_POSTFIX  IpcDsvSendMicAudioAgcCfgParamsMsgT;

/* OBSOLETE MSG INTERFACE - Can delete once CP software has been updated */
typedef PACKED_PREFIX struct
{
   int16    PoleCoefLo;                /* low frequency tone pole coefficient */
   int16    VolLo;                     /* low frequency tone volume coefficient */
   int16    PoleCoefHi;                /* high frequency tone pole coefficient */
   int16    VolHi;                     /* high frequency tone volume coefficient */
   uint16   SeqOnTime;                 /* tone sample sequence on time (in samples) */
   uint16   MuteVoice;                 /* tone sequence with/without voice control switch */
   uint16   ContSeq;                   /* tone sequence on time control switch */
} PACKED_POSTFIX  IpcDsvSendMicPlmnToneGenParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MicVoiceEncMaxRate;     /* maximum voice encode rate for connected SSO */
} PACKED_POSTFIX  IpcDsvSetMicVoiceEncMaxRateMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MicVoiceEncMinRate;     /* minimum voice encode rate for connected SSO */
} PACKED_POSTFIX  IpcDsvSetMicVoiceEncMinRateMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MicVoiceEncFracPitchMode;     /* voice encode fractional pitch search mode switch */
} PACKED_POSTFIX  IpcDsvSetMicVoiceEncFracPitchModeMsgT;

typedef uint16 IpcLpcIROptEnT;
typedef uint16 IpcLpcIRMaxLenT;
typedef uint16 IpcLpcIRMinLenT;
typedef uint16 IpcLpcIRPctPwrT;
typedef uint16 IpcFcbSubSampEnT;
typedef uint16 IpcFcbSubSampParmT;
typedef uint16 IpcFracPitchModeT;

typedef PACKED_PREFIX struct
{
    IpcLpcIROptEnT  LpcIROptEn;
    IpcLpcIRMaxLenT LpcIRMaxLen;
    IpcLpcIRMinLenT LpcIRMinLen;
    IpcLpcIRPctPwrT LpcIRPctPwr;
} PACKED_POSTFIX  IpcDsvSetLpcIROptMsgT;

typedef PACKED_PREFIX struct
{
    IpcFcbSubSampEnT FcbSubSampEn;
    IpcFcbSubSampParmT  FcbSubSampParm;

} PACKED_POSTFIX  IpcDsvSetFcbSubSampMsgT;

typedef PACKED_PREFIX struct 
{
   uint16 MicSimMsgId;              /* identification number of simulated message */
   PACKED_PREFIX union 
   {
      IpcDsvSetMicFifModeMsgT                 SetMicFifModeMsg;
      IpcDsvSendMicFifCfgParamsMsgT           SendMicFifCfgParamsMsg;
      IpcDsvSetMicAnsModeMsgT                 SetMicAnsModeMsg;
      IpcDsvSendMicAnsCfgParamsMsgT           SendMicAnsCfgParamsMsg;
      IpcDsvSetMicBifModeMsgT                 SetMicBifModeMsg;
      IpcDsvSendMicBifCfgParamsMsgT           SendMicBifCfgParamsMsg;
      IpcDsvSendMicVolScalFctrMsgT            SendMicVolScalFctrMsg;
      IpcDsvSendMicVolScalFctrMsgT            SendMicAmpsRevVolScalFctrMsg;
      IpcDsvSetMicAudioAgcModeMsgT            SetMicAudioAgcModeMsg;
      IpcDsvSendMicAudioAgcCfgParamsMsgT      SendMicAudioAgcCfgParamsMsg;
      IpcDsvSendMicPlmnToneGenParamsMsgT      SendMicPlmnToneGenParamsMsg;
      IpcDsvSetMicVoiceEncMaxRateMsgT         SetMicVoiceEncMaxRateMsg;
      IpcDsvSetMicVoiceEncMinRateMsgT         SetMicVoiceEncMinRateMsg;
      IpcDsvSetMicVoiceEncFracPitchModeMsgT   SetMicVoiceEncFracPitchModeMsg;
   } PACKED_POSTFIX  Msg;
} PACKED_POSTFIX  IpcDsvSendMicSimMsgMsgT;

/*** ===================== ***/
/*** Speaker Path Messages ***/
/*** ===================== ***/

typedef enum
{
   IPC_DSV_AUDIO_CFG_MONO_HIFI_DAC   = 0x0001,   /* Hifi DAC mono output */
   IPC_DSV_AUDIO_CFG_STEREO_HIFI_DAC = 0x0002,   /* Hifi DAC stereo output */
   IPC_DSV_AUDIO_CFG_EDAI            = 0x0004,   /* EDAI output */
   IPC_DSV_AUDIO_CFG_I2S             = 0x0008,   /* I2S input & output mode */
   IPC_DSV_AUDIO_CFG_BT              = 0x0010,   /* Bluetooth, uses Ext. BT */
   IPC_DSV_AUDIO_CFG_VADC_I2S        = 0x0020    /* ADC MIC input, I2S output */
} IpcDsvAudioCfgEnum;

/* Configuration of right/left channels when outputting MONO source to stereo output */
typedef enum
{  
   IPC_DSV_NON_INVERTED_STEREO = 0,  /* Do not invert right/left channels */
   IPC_DSV_INVERTED_STEREO,          /* Invert right/left channels        */
   IPC_DSV_STEREO_TONE               /* Audio tone gen testing only: output each of 
                                        the 2 test frequencies on different channels */
} IpcDsvStereoConfigEnum;

/* Configuration of I2S mode */
typedef enum
{  
   I2S_SLAVE = 0,         
   I2S_MASTER                    
} IpcDsvI2sModeEnum;

/* Configuration of I2S delay */
typedef enum
{  
   I2S_NO_DELAY = 0,              /* I2S_DATA has no delay from I2S_SYNC */         
   I2S_DELAY                      /* I2S_DATA has 1 clock delay from I2S_SYNC */
} IpcDsvI2sDelayEnum;

typedef enum
{                                       /* Refer to CBP80_HLD_8.0_D.PDF */
   I2S_STANDARD = 0,                    /* Section 3.4.1 */         
   PCM_0_NORMAL,                        /* Section 3.4.2.1.1 */
   PCM_0_DELAYED_DATA,                  /* Section 3.4.2.1.2 */
   PCM_0_INVERTED_WS,                   /* Section 3.4.2.1.3 */
   PCM_0_DELAYED_DATA_INVERTED_WS,      /* Section 3.4.2.1.4 */  /* Section 3.4.2.1.5 does not exist */
   PCM_1_DELAYED_DATA,                  /* Section 3.4.2.1.6 */
   PCM_1_NON_DELAYED_DATA,              /* Section 3.4.2.1.7 */
   PCM_1_NON_DELAYED_DATA_INVERTED_SCK, /* Section 3.4.2.1.8 */ 
   PCM_1_DELAYED_DATA_INVERTED_SCK,     /* Section 3.4.2.1.9 */ 
} IpcDsvI2SRegConfigEnum;

typedef enum
{  
   PCM_SAMPLE_RATE_16KSPS = 0,       /* default sample rate used for 16ksps WB processing */  
   PCM_SAMPLE_RATE_8KSPS  = 1        /* alternative sample rate used for 8ksps NB processing */            
} IpcDsvPcmSampleRate;

typedef PACKED_PREFIX struct
{
   uint16   SpkrPath;             /* Use IpcDsvAudioCfgEnum */
   uint16   InvertedStereo;       /* Use IpcDsvStereoConfigEnum */
   uint16   I2sMode;              /* Use IpcDsvI2sModeEnum */
   uint16   I2sDelay;             /* Use IpcDsvI2sDelayEnum */
   uint16   I2sRegConfig;         /* Use IpcDsvI2SRegConfigEnum. use it for I2S/PCM mode selection. */
   uint16   PcmSampleRate;        /* Use IpcDsvPcmSampleRate. use it for pcm sample rate selection. 16ksps vs 8ksps. 16ksps is default. */
} PACKED_POSTFIX  IpcDsvSendAudioPathCfgMsgT;

typedef   enum
{
   SSO_STATUS_SUCCESS           =0,
   SSO_STATUS_FAIL              =1
}AudioSsoStatusT ;

typedef PACKED_PREFIX struct
{
   uint16 Status;                     
} PACKED_POSTFIX  IpcCpAudioSsoConnectRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 Status;                     
} PACKED_POSTFIX  IpcCpAudioSsoDisConnectRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16    SpkrPath;
   uint16    Status;                     
} PACKED_POSTFIX  IpcCpAudioPathCfgRspMsgT;

/* DSP I2S delay mode Message */
typedef PACKED_PREFIX struct
{
   uint16       I2sDelayMode;
} PACKED_POSTFIX  IpcCpDsvI2sDelayModeMsgT;

typedef enum
{
   IPC_DSP_I2S_NO_DELAY,
   IPC_DSP_I2S_1_DELAY
} IpcCpDsvI2sDelayModeT;

/* DSP AMR & AMRWB configuration Message */
typedef PACKED_PREFIX struct
{
    uint16 AmrFrameType;
    uint16 AmrDtx;
    uint16 AmrVadOption;
} PACKED_POSTFIX  IpcDsvSendAmrConfigMsgT;

/*** ============================= ***/
/*** Polyphonic Ringer Definitions ***/
/*** ============================= ***/

/*
 *    Ringer shared memory data format.
 */
typedef struct        /* 16 16-bit words total size */
{
   uint16 State;      /* Use VoiceStateT enum */

   struct
   {
       uint16 AmPmMultLevels;  /* KSL(15:14)|DAM(13)|DVB(12)|AM(7)|VIB(6)|EGT(5)|KSR(4)|MULT(3:0) */
       uint16 EnvlpRates;      /* WaveForm(14:12)|RR(11:8)|AR(7:4)|DR(3:0) */
       uint16 BlockFnum;       /* Block(14:12)|FNUM(9:0)   */
       int16  TotalLevel;      /* Envelop of total level   */
       int16  SustainLevel;    /* Envelop of sustain level */
   } Opt[2];

   uint16 KOnNtsFbAlgSelect;   /* KOn(9)|NTS(8)|FB(7:5)|Algo(4:0) */
   uint16 RChlLChlLevels;      /* 0:255 represents ampl of 0 to 1 */
   uint16 Alpha;               /* Alpha(15:0) Make 16 bits for the highest resolution
                                  so we can play with it */
   uint16 PitchBendQ;          /* Pitch Bend Q14 */
   uint16 ReleaseEnvLevel;     /* Reporting envelop of voice during Release only */
} IpcDsvRngrSharedMemT;

/* Ringer Voice States */

#define IPC_RINGER_UNALLOC_VOICE_STATE      0   /* Empty voice bin -- Set by DSV or CP */
#define IPC_RINGER_START_VOICE_STATE        1   /* Start note, init w/ voice info -- set by CP */
#define IPC_RINGER_PROCESS_VOICE_STATE      2   /* Voice Processing -- set by DSV */
#define IPC_RINGER_RELEASE_VOICE_STATE      3   /* Key off: release and start fade -- set by DSV or CP */
#define IPC_RINGER_DEALLOCATE_VOICE_STATE   4   /* Bad note Frequency, do not process -- set by DSV */
#define IPC_RINGER_LOCK_VOICE_STATE         5   /* Don't use this bin for new voices; this is a
                                                 * temporary lock-out state when DSPV short on MIPS 
                                                 * set by CP */


/**************************
    IPC_DSV_RNGR_INIT_MSG
***************************/

/* RngrCtrlReg bit definitions */
/* --------------------------- */

/* Bit [2:0] SampleRate */
   /* to be defined */

/* Bit [3] RingerEnable */
#define IPC_RNGR_ENABLED              (0<<3)
#define IPC_RNGR_DISABLED             (1<<3)

/* Bit [4] I2SEnable */
#define IPC_RNGR_I2S_DISABLED         (0<<4)
#define IPC_RNGR_I2S_ENABLED          (1<<4)

/* Bit [5] I2S Input Sync Select */
#define IPC_RNGR_I2S_SYNCSEL_DISABLED (0<<5)
#define IPC_RNGR_I2S_SYNCSEL_ENABLED  (1<<5)

/* Bit [6] PDM0Enable */
#define IPC_RNGR_PDM0_DISABLED        (0<<6)
#define IPC_RNGR_PDM0_ENABLED         (1<<6)

/* Bit [7] SID delay mode */
#define IPC_I2S_SID_NO_DELAY          (0<<7)
#define IPC_I2S_SID_DELAY             (1<<7)

/* Bit [8] BufferSelectLeft */
#define IPC_RNGR_BUFFSEL_RT           (0<<8)
#define IPC_RNGR_BUFFSEL_LFT          (1<<8)

/* Bit [9] SODDelay */
#define IPC_RNGR_I2S_SOD_NO_DELAY     (0<<9)
#define IPC_RNGR_I2S_SOD_DELAY        (1<<9)

/* Bit [10] SOIInvert */
#define IPC_RNGR_I2S_SOI_NO_INV       (0<<10)
#define IPC_RNGR_I2S_SOI_INV          (1<<10)

/* Bit [11] Mono */
#define IPC_RNGR_MONO_DISABLED        (0<<11)
#define IPC_RNGR_MONO_ENABLED         (1<<11)

/* Bit [12] PdmDiffOrSE */
#define IPC_RNGR_PDM_DIFFSE_DISABLED  (0<<12)
#define IPC_RNGR_PDM_DIFFSE_ENABLED   (1<<12)

/* Bit [13] SMPRepeatEnable */
#define IPC_RNGR_CLKSRC_HIFI_DISABLED (0<<13)
#define IPC_RNGR_CLKSRC_HIFI_ENABLED  (1<<13)

/* Bit [14] MaxSmpCntrlRngr */
#define IPC_RNGR_MIN_SMP_RINGER       (0<<14)
#define IPC_RNGR_MAX_SMP_RINGER       (1<<14)

/* Bit [15] FifoPtrReset */
#define IPC_RNGR_WFIFO_PTR_NO_RST     (0<<15)
#define IPC_RNGR_WFIFO_PTR_RST        (1<<15)

typedef enum
{
   IPC_DSV_NO_DTMF       = 0x0000,
   IPC_DSV_DTMF_SPKR     = 0x0001,      /* bit 0 */
   IPC_DSV_DTMF_MIC      = 0x0002,      /* bit 1 */
   IPC_DSV_DTMF_MIC_SPKR = 0x0003       /* bit 0 AND bit 1 (DSPV uses bit fields) */
} IpcDsvDtmfPathEnum;

typedef PACKED_PREFIX struct
{
    uint16 MaxNumVoices;            /* 1:64 */
    uint16 SampleRate;              /* kHz */
    uint16 RngrCtrlReg;             /*  SmpMaxCtrl(14)|PwmSmpMode(13)|PwmDiffSE(12)|Mono(11)|
                                        LfRtInver(10)|DataDelay(9)|BuffSel(8)|PdmEn1(7)|PdmEn0(6)
                                        PwmEn(5)|I2SEn(4)|RingerEnable(3)|SampleRate(2:0) */ 
    uint16 DtmfPath;                /* DTMF Path: Use IpcDsvDtmfPathEnum */
    uint16 InvertedStereo;          /* Use IpcDsvStereoConfigEnum */
} PACKED_POSTFIX  IpcDsvRngrInitMsgT;


/*   IPC_DSV_RNGR_CONFIG_MSG */
typedef PACKED_PREFIX struct
{
    uint16 BlockSzMs;               /*  milliSeconds                        */
    uint16 RampTime;                /*  Ramp Time for PWM                   */
    uint16 CntrlField;               /*  CntrlField: ON=1, OFF=0             */
                                    /*   Interpolation filter        [0]    */
} PACKED_POSTFIX  IpcDsvRngrConfigMsgT;             /*   WaveTable Accelerator       [1]    */
                                    /*   Noise Shaping Filter        [2]    */
                                    /*   Dither Generator            [3]    */
                                    /*   10/11bit resolution PDM/PWM [4]    */
                                    /*   HwTest--write out data      [5]    */

/* Ringer Control Fields Definition */
#define IPC_RNGR_INTRP_FLTR_BN          0
#define IPC_RNGR_WAVTBL_ACCELR_BN       1
#define IPC_RNGR_NOISE_SHAPE_FLTR_BN    2
#define IPC_RNGR_DITHER_FLTR_BN         3
#define IPC_RNGR_PDM_HIGHBIT_RESL_BN    4 
#define IPC_RNGR_HW_TEST_BN             5  

#define IPC_RNGR_INTRP_FLTR         (1<<IPC_RNGR_INTRP_FLTR_BN)
#define IPC_RNGR_WAVTBL_ACCELR      (1<<IPC_RNGR_WAVTBL_ACCELR_BN)
#define IPC_RNGR_NOISE_SHAPE_FLTR   (1<<IPC_RNGR_NOISE_SHAPE_FLTR_BN)
#define IPC_RNGR_DITHER_FLTR        (1<<IPC_RNGR_DITHER_FLTR_BN)
#define IPC_RNGR_PDM_HIGHBIT_RESL   (1<<IPC_RNGR_PDM_HIGHBIT_RESL_BN)  
                                            /* 10-bit resolution if OFF */
                                            /* 11-bit resol (HIGH) if ON */
#define IPC_RNGR_HW_TEST            (1<<IPC_RNGR_HW_TEST_BN)  

/*   IPC_DSV_SEND_RNGR_VOL_SCAL_FCTR_MSG */
typedef PACKED_PREFIX struct
{
   uint16   RngrVolScalFctr;        /* software volume scale factor */
} PACKED_POSTFIX  IpcDsvSendRngrVolScalFctrMsgT;

/*  IPC_DSV_APP_VOL_SCAL_FCTR_MSG */
typedef PACKED_PREFIX struct
{
   uint16   VolScalFctr;        /* software volume scale factor */
} PACKED_POSTFIX  IpcDsvSendAppVolScalFctrMsgT;

typedef enum
{
   IPC_DV_RNGR_ON = 0,
   IPC_DV_RNGR_OFF,
   IPC_DV_RNGR_RELEASE,
   IPC_DV_RNGR_ABORT
} IpcDsvRngrCntrlModeEnum;

/*   IPC_DSV_RNGR_MODE_MSG */
typedef PACKED_PREFIX struct
{
    uint16 RngrCtrlMode;   /* bit field: of type IpcDsvRngrCntrlModeEnum */
} PACKED_POSTFIX  IpcDsvRngrModeMsgT;

/*   IPC_CP_DV_RNGR_STATUS_MSG */
typedef PACKED_PREFIX struct
{
    uint16 RngrStatus;     /* bit field: of type IpcDsvRngrCntrlModeEnum: 0:on,1=Off */
} PACKED_POSTFIX  IpcDsvRngrStatusMsgT;

/********* End of POLYPHONIC RINGER definitions ************/

typedef PACKED_PREFIX struct
{
   uint16   SpkrVoiceFwdChPcktRate;    /* forward channel voice packet rate */
   uint16   SpkrVoiceFwdChPcktSize;    /* forward channel voice packet size (in x16 words) */
   uint16   SpkrVoiceFwdChPcktData[1]; /* forward channel voice packet data */
} PACKED_POSTFIX  IpcDsvSendSpkrVoiceFwdChPcktDataMsgT;

typedef PACKED_PREFIX struct
{
   uint16   SpkrVoiceDecPitchPreFltrMode;  /* voice decode pitch pre-filter mode switch */
} PACKED_POSTFIX  IpcDsvSetSpkrVoiceDecPitchPreFltrModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   SpkrVoiceDecSynPostFltrMode;  /* voice decode synthesis post-filter mode switch */
} PACKED_POSTFIX  IpcDsvSetSpkrVoiceDecSynPostFltrModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   SpkrVoicePlayPcktRate;    /* voice record playback packet rate */
   uint16   SpkrVoicePlayPcktSize;    /* voice record playback packet size (in x16 words) */
   uint16   SpkrVoicePlayPcktData[1]; /* voice record playback packet data */
} PACKED_POSTFIX  IpcDsvSendSpkrVoicePlayPcktDataMsgT;

/* OBSOLETE MSG INTERFACE - Can delete once CP software has been updated */
typedef PACKED_PREFIX struct
{
   int16    PoleCoefLo;             /* low frequency tone pole coefficient */
   int16    VolLo;                  /* low frequency tone volume coefficient */
   int16    PoleCoefHi;             /* high frequency tone pole coefficient */
   int16    VolHi;                  /* high frequency tone volume coefficient */
   uint16   SeqOnTime;              /* tone sample sequence on time (in samples) */
   uint16   MuteVoice;              /* tone sequence with/without voice control switch */
   uint16   ContSeq;                /* tone sequence on time control switch */
} PACKED_POSTFIX  IpcDsvSendSpkrPlmnToneGenParamsMsgT;

/* OBSOLETE MSG INTERFACE - Can delete once CP software has been updated */
typedef PACKED_PREFIX struct
{
   int16    PoleCoefLo;             /* low frequency tone pole coefficient */
   int16    VolLo;                  /* low frequency tone volume coefficient */
   int16    PoleCoefHi;             /* high frequency tone pole coefficient */
   int16    VolHi;                  /* high frequency tone volume coefficient */
   uint16   SeqOnTime;              /* tone sample sequence on time (in samples) */
   uint16   MuteVoice;              /* tone sequence with/without voice control switch */
   uint16   ContSeq;                /* tone sequence on time control switch */
} PACKED_POSTFIX  IpcDsvSendSpkrRngrToneGenParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   SpkrFifMode;            /* front-end IIR mode switch */
} PACKED_POSTFIX  IpcDsvSetSpkrFifModeMsgT;

#define IPC_NUM_SPKR_FIF_BQ_COEFFS  18
typedef PACKED_PREFIX struct
{
   uint16   SpkrFifNumBqSects;              /* number of biquad filter sections */
   uint16   SpkrFifBqCoefs[IPC_NUM_SPKR_FIF_BQ_COEFFS];  /* filter coefficients */
} PACKED_POSTFIX  IpcDsvSendSpkrFifCfgParamsMsgT;


typedef PACKED_PREFIX struct
{
   uint16   AmpsFwdVolScalFctr;        /* software volume scale factor */
} PACKED_POSTFIX  IpcDsvSendSpkrAmpsFwdVolScalFctrMsgT;

/* This message sends all audio volume scale factor messages in one.
 * Each field in the message is identical to the original volume message indicated.
 */
typedef PACKED_PREFIX struct
{
   uint16   SpkrVolScalFctr;   
   uint16   SidetoneScalFctr;  
   uint16   RngrVolScalFctr;   
   uint16   AppVolScalFctr;    

   uint16   MicVolScalFctr;    
   uint16   DacPgaVal;              /* DacPga setting of voice DAC */
   
   uint16   ToneVolScalFctr;   
   
} PACKED_POSTFIX  IpcDsvVolumeScalFctrMsgT;


/*** =========================== ***/
/*** General Audio Path Messages ***/
/*** =========================== ***/

typedef PACKED_PREFIX struct
{
   uint16   AudioAirInterfaceMode;  /* air interface mode switch */
} PACKED_POSTFIX  IpcDsvSetAudioAirInterfaceModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioSpchSrvcOpt;          /* speech service option connection type */
   uint16   AudioSpchSrvcOptMaxRate;   /* maximum voice encode rate */
} PACKED_POSTFIX  IpcDsvSendAudioSsoConnectMsgT;

typedef PACKED_PREFIX struct
{
   uint16   EbNt;                       /* Eb/Nt for FCH */
} PACKED_POSTFIX  IpcDsvSendAudioChanQltyMsgT;

typedef PACKED_PREFIX struct
{
   uint16   RoutingDisabled;           /* TRUE if speech routing disabled */
} PACKED_POSTFIX  IpcDsvSendAudioSpchRoutingMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioSsoCtrlRrmLevel;      /* rate reduced mode bit-field */
   uint16   AudioSsoCtrlM2mMode;       /* mobile-to-mobile mode bit-field */
   uint16   AudioSsoCtrlInitCodecMode; /* initialize codec mode bit-field */
} PACKED_POSTFIX  IpcDsvSendSsoCtrlParamsMsgT;

#define IPC_NUM_TONE_GEN 6

typedef PACKED_PREFIX struct
{
   uint16   ToneGenId;     /* Tone generator to which the structure applies */
   int16    PoleCoef;      /* tone pole coefficient */
   int16    VolCoef;       /* tone volume coefficient */
   uint32   SeqOnTime;     /* Long timer for tones    */
} PACKED_POSTFIX  IpcDsvToneCfgT;

typedef PACKED_PREFIX struct
{
   uint16         MuteVoice;     /* tone sequence with/without voice control switch */
   uint16         NumTones;      /* Number of Tones to be activated  */
   IpcDsvToneCfgT ToneParms[1];    
} PACKED_POSTFIX  IpcDsvToneGenParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioSideToneScalFctr;     
   uint16   SpkrBufSoftLimitScalFctr;      
} PACKED_POSTFIX  IpcDsvSendAudioIsrScalFctrMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioLoopbackMode;      /* audio path loop-back mode switch */
} PACKED_POSTFIX  IpcDsvSetAudioLoopbackModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioHandsFreeMode;     /* hands-free audio mode switch */
} PACKED_POSTFIX  IpcDsvSetAudioHandsFreeModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   tbd;
} PACKED_POSTFIX  IpcDsvSendAudioHandsFreeCfgParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioAecMode;           /* acoustic echo cancellation mode switch */
} PACKED_POSTFIX  IpcDsvSetAudioAecModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16 phone_mode;                                    // HANDSET,HEADSET,LOUDSPEAKER
   uint16 NumMic;                                        // 1: single-mic, 2:dual-mic
   uint16 DacPgaVal;                                     // DacPga mixed signal gain setting.
   uint16 AecClipSatThres;                               // Soft clipping saturator threshold. Q15
    int16 BNE_base;                                      // Background Noise Estimation base. Q0
   uint16 Mu_max;                                        // Max Step Size
   uint16 Mu_min;                                        // Min Step Size
   uint16 Converge_prd;                                  // Filter Converge Hangover Period
   uint16 Rin2SinDelay;                                  // delay between Rin and Sin. set to 0 for test. need to re-confiugre in real phone.
   uint16 Res_spkr_vm_Thres;                             // far-end voice metric threshold
   uint16 Res_spkr_ch_engy_Thres;                        // far-end channel energy threshold
   uint16 Res_mic_vm_Thres;                              // near-end voice metric threshold
   uint16 Res_mic_ch_engy_Thres;                         // near-end channel energy threshold
   uint16 dt_ratio;                                      // Double talk ratio of res_energy/rep_energy. Q10
   uint16 converge_thre;                                 // Converence threshold of near_end_energy/res_energy. Q15
   uint16 fre_bin_start;                                 // start bin for DTD detection.
   uint16 fre_bin_end;                                   // end bin for DTD detection.
   uint16 MaxCoupling;                                   // maximum acoutic coupling factor. Q15.  
   uint16 StepShift;                                     // NLMS filter step adjust in double-talk period. Q0.
   uint16 BinEnergyAlpha;                                // Bin energy estimate smoothing factor Q15
   uint16 beta;                                          // forgeting factor. range of [0.x ~ 0.x] is acceptable.
    int16 eta;                                           // adaptation step size. range of [ 0.1 ~ 0.5 ] is acceptable
    int16 eta2;                                          // adaptation step size. range of [ 0.1 ~ 0.5 ] is acceptable
   uint16 Delay;                                         // for better adpatation of secondary microphone adaptation filter 
   uint16 MaxHGain;                                      // MaxHgain in single-talk period
   uint16 MaxHGain_dt;                                   // MaxHgain in double-talk period
   uint16 BSSDtRatio;                                    // Double-talk ratio in RIC
   uint16 StepFacMax;                                    // Step Smoothing factor in single-talk period
   uint16 StepFacMin;                                    // Step Smoothing factor in double-talk period
   uint16 FilterQ;                                       // Adaptive filter Q factor. Handset: Q15; Loudspk: Q10
   uint16 WeightedSIRmax;
   uint16 WeightedSIRmin;
   uint16 BandGainWeightMax;
   uint16 BandGainWeightMin;
   uint16 SIR_Thres;                                     // LPF triggering threshold for loud speaker mode
   uint16 LPF_cutoff_band;                               // LPF cut-off band index for loud speaker mode
   uint16 P_fe;                                          // RIC extra gain adjustment for Far-End mode 
   uint16 P_dt;                                          // RIC extra gain adjustment for Double-Talk mode for low frequency band
   uint16 P_dt_high;                                     // RIC extra gain adjustment for Double-Talk mode for high frequency band
   uint16 dt_th;                                         // RIC dobule talk decision threshold
   uint16 Aggressive_Echo_Supr;							 // Boolean control for aggressive echo suppression and low NE quality
} PACKED_POSTFIX  IpcDsvSendAudioAecMainCfgParamsMsgT;

typedef PACKED_PREFIX struct 
{
   int16    Dummy;
} PACKED_POSTFIX  IpcDsvSendAudioAecAuxCfgParamsMsgT;  

typedef PACKED_PREFIX struct
{
   int16    BlockSize;               /* -- ACP configuration parameters-- */        
   int16    BlockNum;
   int16    RmsQ;
   int16    RmsScalFctr;
   int16    GammaHi;
   int16    GammaLo;
   int16    BetaHi;
   int16    BetaLo;
   int16    BetaLoQ;
   int16    AttackTime;
   int16    ReleaseTime;

   uint16   AsvcMinGain;             /* -- Mpp Backgroun noise level estimatimation -- */ 
   uint16   AsvcMaxState;
   uint16   AsvcNoiseStep;
   uint16   AsvcGainStep;
   uint16   AsvcNoiseThres;
   uint16   AsvcHysterisis;
   int16    AsvcNoiseScalFctr;
   uint16   AsvcVmThres;
   uint16   AsvcTceThres;
   uint16   AsvcSpecDevThres;
   uint16   AsvcCntThres;
   uint16   AsvcHysCntThres;

   int16    AsvcSpkrWeight;
   int16    AsvcScalFctrQ;
   int16    AsvcSmoothFctr;
   int16    AsvcMaxSpkrGain;         
   uint16   FramePwrStep;            /* -- Spp frame power estimatimation -- */
   uint16   FramePwrThres;
   uint16   FramePwrHyst;
   uint16   SppGainStep;
   uint16   SppGainMaxState;
   uint16   SppFrmPwrBufLen;
   uint16   SppFrameCntThres;
   uint16   GainUpdateThres;
   uint16   GainUpdateSwch;
   uint16   AcpToneThres;               /* --- Acp Acoustic Shock tone threshold and suppress target */
   uint16   AcpToneSuppress;
   uint16   LcpTarget_Threshold;	    /* --- LCP (Limit Cycle Protection) parameters used in SmartAscScalSampBuf */
   uint16   LcpMargin;
   uint16   Acp2kToneThres;
} PACKED_POSTFIX  IpcDsvSendAudioAcpCfgParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioAcpCompMode;
   uint16   AudioAcpAsvcMppMode;
   uint16   AudioAcpAsvcSppMode;
} PACKED_POSTFIX  IpcDsvSetAudioAcpModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16  AudioRecordPlaybackMode;        /* Use IpcVoiceRecordPlaybackModeT     voice record playback mode */
   uint16  AudioRecordPlaybackVocoder;     /* Use IpcSpchSrvcOptT                 voice record playback vocoder type */
   uint16  AudioRecordPlaybackDataType;    /* LSB : Use IpcVoiceRecPlayDataTypeT  voice record playback data type */
                                           /* MSB : Use EdaiCompndT               When LSB is PCM, MSB determine between LinearPCM, Mu-lawPCM or A-lawPCM */
   uint16  AudioRecordPlaybackMaxRate;     /* Use IpcSpchSrvcOptRateT             voice record playback maximum encode rate */
   uint16  AudioRecordPlaybackMinRate;     /* Use IpcSpchSrvcOptRateT             voice record playback minimum encode rate */
   uint16  AudioRecordPathSel;             /* Use IpcVoiceRecSelPathModeT         voice record select between rev ch and fwd ch in AMPS mode*/
   uint16  AudioPlaybackVoiceMix;          /* Use IpcVoiceRecPlaybackVoiceMixT    select voice mix or playback pcm only */
} PACKED_POSTFIX  IpcDsvSetAudioRecordPlaybackModeMsgT;

typedef PACKED_PREFIX struct                      /* Response indicating the record/playback action completed */
{
   uint16  Mode;                           /* Use IpcVoiceRecordPlaybackModeT */
} PACKED_POSTFIX IpcDsvSetAudioRecordPlaybackModeRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AudioTtyMode;           /* TTY mode switch */
} PACKED_POSTFIX  IpcDsvSetAudioTtyModeMsgT;

typedef PACKED_PREFIX struct
{
   int16 TtyLogicEngyTh;
    int16   TtySilnceEngyTh;
   int16 TtyOnsetWin;
   int16 TtyEarlyOnsetThres;
   int16 TtyDetctThres;                   
   int16 TtyFrmSyncMemTh;                 
   int16 TtyFrmSyncStartTh;               
   int16 TtyFrmSyncDataTh;                   
   int16 TtyFrmSyncStopTh;                   
   int16 TtyFrmSyncSilnceTh;              
   int16 TtyXmtTime;                      
   int16 TtyMaxHngOvrTime;                
   int16 TtySilnceTh;
   int16 TtyToneGenGain;
} PACKED_POSTFIX  IpcDsvSendAudioTtyCfgParamsMsgT;


typedef PACKED_PREFIX struct 
{
   uint16 SpeakerSimMsgId;          /* identification number of simulated message */
   PACKED_PREFIX union 
   {
      IpcDsvSendSpkrVoiceFwdChPcktDataMsgT       SendSpkrVoiceFwdChPcktDataMsg;
      IpcDsvSetSpkrVoiceDecPitchPreFltrModeMsgT  SetSpkrVoiceDecPitchPreFltrModeMsg;
      IpcDsvSetSpkrVoiceDecSynPostFltrModeMsgT   SetSpkrVoiceDecSynPostFltrModeMsg;
      IpcDsvSendSpkrVoicePlayPcktDataMsgT        SendSpkrVoiceRecPlayPcktDataMsg;
      IpcDsvSendSpkrPlmnToneGenParamsMsgT        SendSpkrPlmnToneGenParamsMsg;
      IpcDsvSendSpkrRngrToneGenParamsMsgT        SendRngrPlmnToneGenParamsMsg;
      IpcDsvSetSpkrFifModeMsgT                   SetSpkrFifModeMsg;
      IpcDsvSendSpkrFifCfgParamsMsgT             SendSpkrFifCfgParamsMsg;
      IpcDsvVolumeScalFctrMsgT                   SendVolumeScalFctrMsg;
      IpcDsvSetAudioAirInterfaceModeMsgT         SetAudioAirInterfaceModeMsg;
      IpcDsvSendAudioSsoConnectMsgT              SendAudioSsoConnectMsg;
      IpcDsvSendAudioSpchRoutingMsgT             SendAudioSpchRoutingMsg;
      IpcDsvSendSsoCtrlParamsMsgT                SendSsoCtrlParamsMsg;
      IpcDsvSendAudioIsrScalFctrMsgT             SendAudioIsrScalFctrMsg;
      IpcDsvSetAudioLoopbackModeMsgT             SetAudioLoopbackModeMsg;
      IpcDsvSetAudioHandsFreeModeMsgT            SetAudioHandsFreeModeMsg;
      IpcDsvSendAudioHandsFreeCfgParamsMsgT      SendAudioHandsFreeCfgParamsMsg;
      IpcDsvSetAudioAecModeMsgT                  SetAudioAecModeMsg;
      IpcDsvSendAudioAecMainCfgParamsMsgT        SendAudioAecMainCfgParamsMsg;
      IpcDsvSendAudioAecAuxCfgParamsMsgT         SendAudioAecAuxCfgParamsMsg;
      IpcDsvSetAudioRecordPlaybackModeMsgT       SetAudioVoiceRecModeMsg;
      IpcDsvSetAudioTtyModeMsgT                  SetAudioTtyModeMsg;
      IpcDsvSendAudioTtyCfgParamsMsgT            SendAudioTtyCfgParamsMsg;
   } PACKED_POSTFIX  Msg;
} PACKED_POSTFIX  IpcDsvSendSpkrSimMsgMsgT;


/*******************************************************
** Message Stuctures: DSP to Control Processor (_CP_) **
*******************************************************/

/*----------------------------**
** Voice And Audio Processing **
**----------------------------*/

typedef PACKED_PREFIX struct
{
   uint16 MppSpchRate;                 /* Encoder rate decision indicator */
   uint16 NumMppSpchData;              /* Number of Tx Speech Data */
   uint16 MppSpchData[1];              /* Starting address of Tx Speech Data */
} PACKED_POSTFIX  IpcCpSpchDataRevChPacketMsgT;

typedef PACKED_PREFIX struct
{ 
   uint16 MppSpchDataType;             /* Record data type */
   uint16 MppSpchRate;                 /* Encoder rate decision indicator */
   uint16 NumMppSpchData;              /* Number of Tx Speech Data */
   uint16 MppSpchData[1];              /* Starting address of Tx Speech Data */
} PACKED_POSTFIX  IpcCpSpchDataRecordPacketMsgT;

typedef PACKED_PREFIX struct
{
   int32   BwExpandAcf[IPC_ACF_LEN_MAX];  /* bandwidth expanded autocorrelation coefficients, Rw */
} PACKED_POSTFIX  IpcCpSpchDataMppAgcMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MppParmType;                  /* encoder diagnostic parameter type */
   uint16   MppParmLen;                   /* encoder diagnostic parameter length */
   uint16   MppParms[1];                  /* encoder diagnostic parameters */
   uint16   SppParmType;                  /* decoder diagnostic parameter type */
   uint16   SppParmLen;                   /* decoder diagnostic parameter length */
   uint16   SppParms[1];                  /* decoder diagnostic parameters */
} PACKED_POSTFIX  IpcCpSpchDataDiagMsgT;

typedef PACKED_PREFIX struct
{
   uint16 ModuleId;                    /* Audio module ids */
   uint16 NumCfgData;                  /* Number of configurable parameters Data */
   uint16 AudioCfgData[1];             /* Audio Config Data */
} PACKED_POSTFIX  IpcCpAudioCfgParamsMsgT;

/***********************************************************
** Message Field Defines: Control Processor to DSP (_DSV_) **
************************************************************/
/*-------------------------------------------------**
** AMPS related message data structures CP -> DSPV **
**-------------------------------------------------*/
typedef   enum
{
   NORMAL_MODE          =0,
   CONTINUOUS_TEST_MODE =1
} ReccTstModeT ;

typedef PACKED_PREFIX struct
{
   uint16 Mode;
   uint16 Dcc;
   uint16 MsgLen;
   uint16 Data[1];
} PACKED_POSTFIX  IpcDsvSendAmpWbdReccMessageMsgT;

typedef PACKED_PREFIX struct
{
   uint16 MsgLen;
   uint16 Data[1];
} PACKED_POSTFIX  IpcDsvSendAmpWbdRvcMessageMsgT ;

/* Voice mode: Rvc Mode message type structure: */
typedef PACKED_PREFIX struct
{
   uint16 StOn;
} PACKED_POSTFIX  IpcDsvSetAmpStControlMsgT ;

typedef PACKED_PREFIX struct
{ 
   uint16   RevChAbortOn;
} PACKED_POSTFIX  IpcDsvSetAmpRevChAbortMsgT; 

typedef enum
{
   IPC_AMP_COMPRESSOR_ON        =1,
   IPC_AMP_COMPRESSOR_OFF       =0 
} CompressorCtrlT;

typedef PACKED_PREFIX struct
{
   uint16 CompOn;
} PACKED_POSTFIX  IpcDsvSetAmpCompControlMsgT ;


typedef enum
{
   REV_CH_POLARITY_NORMAL =0,
   REV_CH_POLARITY_INVERT =1  
}RevChPolarityT ;

typedef enum
{
  REV_CH_MODULATION_BASEBAND_IQ = 0,
  REV_CH_MODULATION_VCO         = 1,
  REV_CH_MODULATION_DIF_MODE    = 2
} RevChModulationT;



/*------------------------------------------------------------------------------*/

/* Structure for Amps parameters configuration message. All the voice path (RevCh and FwdCh
   voice, Sat , and St parameters are defined in this data structure */



typedef PACKED_PREFIX struct
{
   int16      Ciscale;     
   int16      Piscale;     
   int16      STscale;     
   int16      SATscale;     
   int16      DevLimFltrscale;     
   int16      DevLimThresh;
   uint16     wbdscaladj;
} PACKED_POSTFIX  IpcAmpRevChScalCtlT ;    /* Amps parameters scale and control configuration data */

/*
typedef PACKED_PREFIX struct
{

   int16      InterpCoefs[8];
   int16      PostDLCoefs[7];
   int16      PreEmphCoefs[55];
   int16      CompCoefs[2]; 
} PACKED_POSTFIX  IpcAmpRevChFiltCoefsT ;  */  /* Amps parameters filter coefficients configuration data */

typedef PACKED_PREFIX struct
{
   IpcAmpRevChScalCtlT     ScalCtlData;     
/*   int16                   ManchTable[32];
   IpcAmpRevChFiltCoefsT   FiltCoefs;     */
} PACKED_POSTFIX  IpcDsvSetAmpRevChParamsConfigMsgT ;    /* Amps parameters configuration data type */

typedef PACKED_PREFIX struct
{
   int16                   RevChPolarity;
   int16                   RevChModulationType;
} PACKED_POSTFIX  IpcDsvSetAmpRevChCtrlConfigMsgT ;    /* Amps control configuration data type */

typedef PACKED_PREFIX struct
{
   uint16  DcOffsetMode;
   uint16  DcOffsetVal;
} PACKED_POSTFIX  IpcDsvSetAmpFixedFreqMsgT;

typedef PACKED_PREFIX struct
{
   uint16  Status;
} PACKED_POSTFIX  IpcDsvGetAmpCompExpStatusMsgT;

/* structure for Reverse Channel simulation message */
typedef PACKED_PREFIX struct 
{
   uint16 RevChSimMsgId;
   PACKED_PREFIX union 
   {
      IpcDsvSendAmpWbdReccMessageMsgT             SendAmpWbdReccMessageMsg;
      IpcDsvSendAmpWbdRvcMessageMsgT              SendAmpWbdRvcMessageMsg;
      IpcDsvSetAmpRevChAbortMsgT                  SetAmpRevChAbortMsg;
      IpcDsvSetAmpRevChParamsConfigMsgT           SetAmpRevChParamsConfigMsg;
      IpcDsvSetAmpCompControlMsgT                 SetAmpCompControlMsg;
      IpcDsvSetAmpStControlMsgT                   SetAmpStControlMsg;
      IpcDsvSetAmpFixedFreqMsgT                   SendAmpFixedFreqMsg;
      IpcDsvSetAmpRevChCtrlConfigMsgT             SetAmpRevChCtrlConfigMsg;
      IpcDsvGetAmpCompExpStatusMsgT               GetAmpCompExpStatusMsg;
   } PACKED_POSTFIX  Msg;
} PACKED_POSTFIX  IpcDsvAmpSendRevChSimMsgMsgT;

/* Rx message structuctures */

typedef enum
{
   FOCC_DEMOD_OFF = 0,
   FOCC_DEMOD_ON
} WideBandDataDemodControlT;

typedef enum
{
   POWER_SAVING_MODE_OFF = 0,
   POWER_SAVING_MODE_ON
} PowerSavingModeControlT;

typedef enum
{
   STREAM_A = 0,
   STREAM_B
} WideBandDataStreamT;

typedef enum
{
   IPC_AMP_MODE_DISABLED = 0,
   IPC_AMP_MODE_FOCC     = 1,
   IPC_AMP_MODE_VOICE    = 2,
   IPC_AMP_MODE_FOCC_RECC,
   IPC_AMP_MODE_VOICE_RVC,
   IPC_AMP_MODE_VOICE_FVC
} IpcAmpModeT;

typedef enum
{
   IPC_AMP_IDLE_MODE_DISABLE = 0,
   IPC_AMP_IDLE_MODE_ENABLE  = 0xC000
} IpcAmpIdleModeControlT;

/* Voice mode/FOCC Mode message type structure: */
typedef PACKED_PREFIX struct
{
   uint16 Mode;
   uint16 FoccStream;
   uint16 FoccType;
} PACKED_POSTFIX  IpcDsvSetAmpModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16 Mode;
} PACKED_POSTFIX  IpcCpSetAmpModeRspMsgT;


typedef PACKED_PREFIX struct
{
   uint16  BusyIdleProcessOn;
} PACKED_POSTFIX  IpcDsvSetAmpBusyIdleProcessMsgT;


typedef PACKED_PREFIX struct
{
   uint16 FrameCounts;
} PACKED_POSTFIX  IpcDsvAmpGetFmSignalStrengthMsgT;


typedef PACKED_PREFIX struct
{
   uint16  DcBiasCalcMode;
   uint16  FrameCnt;
   int16   FreqOffset;   
} PACKED_POSTFIX  IpcDsvSetAmpDcOffsetCompMsgT;

typedef enum
{
   IPC_AMP_DC_CALC_DISABLED    =0,
   IPC_AMP_DC_AVER_COMP        =1,
   IPC_AMP_DC_AVER_ARC_COMP    =2,
   IPC_AMP_DC_AVER_ONLY        =3,
   IPC_AMP_DC_AVER_ARC_ONLY    =4,
   IPC_AMP_DC_COMP_ONLY        =5
} IpcDsvAmpDcBiasCalcModeT;

typedef PACKED_PREFIX struct
{
   uint16  RxAgcMode;
   uint16  AssertPdmTarget;
   uint16  PdmVal;
   uint16  TargetMode;
} PACKED_POSTFIX  IpcDsvSetAmpRxAgcCtrlMsgT;

typedef enum
{
   IPC_AMP_ASSERT_NOTHING          =0,
   IPC_AMP_ASSERT_PDM_ONLY         =1,
   IPC_AMP_ASSERT_TARGET_ONLY      =2,
   IPC_AMP_ASSERT_PDM_TARGET_BOTH  =3
} IpcDsvAmpRxAgcAssertPdmTargetT;

typedef enum
{
   IPC_AMP_RX_AGC_LO_TARGET   =0,
   IPC_AMP_RX_AGC_ME_TARGET   =1,
   IPC_AMP_RX_AGC_HI_TARGET   =2,
   IPC_AMP_RX_AGC_NUM_TARGETS =3
} IpcDsvAmpRxAgcTargetModeT;

typedef PACKED_PREFIX struct
{
   uint16  AfcMode;
   uint16  AssertPdm;
   uint16  PdmVal;
} PACKED_POSTFIX  IpcDsvSetAmpAfcCtrlMsgT;

typedef PACKED_PREFIX struct
{
   uint16  WordSyncCounter;
} PACKED_POSTFIX  IpcDsvSetAmpFwdChCounterMsgT;


/* Expander message */
typedef enum
{
   IPC_AMP_EXPANDER_ON        =1,
   IPC_AMP_EXPANDER_OFF       =0 
 } ExpanderCtrlT;

typedef PACKED_PREFIX struct
{
   uint16            ExpanderCtrl;
} PACKED_POSTFIX  IpcDsvSetAmpExpControlMsgT ;

/* Hpf message */
typedef enum
{
   IPC_AMP_HPF_ON             =1,
   IPC_AMP_HPF_OFF            =0 
 } HpfCtrlT;

typedef PACKED_PREFIX struct
{
   uint16            HpfCtrl;
} PACKED_POSTFIX  IpcDsvSetAmpHpfControlMsgT ;

/* SAT control message */
typedef enum
{                                /* Table Form:   Det    AddSat   Gen/Trans        */
   IPC_AMP_SAT_DET_TRANS   =7,   /*               1       1       1   =     7      */
   IPC_AMP_SAT_DET_GEN     =6,   /*               1       1       0   =     6      */
   IPC_AMP_SAT_GEN_ONLY    =2,   /*               0       1       0   =     2      */
   IPC_AMP_SAT_ALL_OFF     =0    /*               0       0       0   =     0      */
} SatModeT ;

typedef PACKED_PREFIX struct
{
   uint16 SatMode;
} PACKED_POSTFIX  IpcDsvSetAmpSatControlMsgT ;

/* SAT Raw Data Control message */
typedef enum
{
   IPC_AMP_SAT_RAW_DATA_ON    =1,
   IPC_AMP_SAT_RAW_DATA_OFF   =0 
 } SatRawDataCtrlT;

typedef     enum
{
   FWD_CH_DC_BIAS_ENABLE    =0,   
   FWD_CH_DC_BIAS_DISABLE   =1
 } FwdChDcBiasT ;

typedef enum
{
   FWD_CH_POLARITY_NORMAL = 0,
   FWD_CH_POLARITY_INVERT = 1  
}FwdChPolarityT;

typedef PACKED_PREFIX struct
{
   uint16      Discriminatorscale;
    
   uint16      WbdInpScale;
   uint16      FoccDotWsThd;
   uint16      FoccDotWsThdAdj;
   uint16      FvcDotThd;
   uint16      FvcDotCntrInc;
   uint16      FvcDotCntrDec;
   uint16      FvcDotCntrThd;

   uint16      Satiscale;
   
   int16       AfcTarget;
   int16       VoiceAfcCn;        
   int16       FoccAfcCn;

   int16       RxAgcTargetLo;
   int16       RxAgcTargetMe;
   int16       RxAgcCorrectFctr;
   int16       VoiceAfcSlope;
   int16       FoccAfcSlope;   

   uint16      DcThreshold;
   uint16      SatDetectScale;
   uint16      PwrChangeHystMed2;
   uint16      PwrChangeHystHi;
   uint16      AfcOffPwrThresh;
   int16       AfcCntrlGain;
   int16       FoccLostSyncCntrThd;
   uint16      FoccGoodFrmThd;
   uint16      IdleDspUpTime;
   uint16      IdleRfFastUpTime;
   uint16      IdleRfSlowUpTime;
   uint16      FvcSyncDetLim;
   uint32      BchErrPat;
   uint16      CenterFreq;
   uint16      PdmPerUnitQ;
   uint16      FoccSlowRFSlpNum;
   uint16      SatWeight;
   uint16      ExpCorrectThresh;
   uint16      ExpCorrectFctr;
   uint16      AverOnThresh;
   uint16      MinPdmVal;
   uint16      DcCompNormShift;
   int16       RxAgcSlopeQAdjFctr;
   uint16      DcOffsetReportMode;
   uint16      DcOffsetThresh1;
   uint16      DcOffsetThresh2;
   uint16      DcCalSettleFrmCnt;

} PACKED_POSTFIX  IpcAmpFwdChScalCtlT;    /* Amps parameters configuration data type */

/*
typedef  PACKED_PREFIX struct
{ 
   int16     LpfDeEmphCoefs[12];
   int16     ExpCoefs[4];
   int16     HpfCoefs[8];
   int16     DcfCoefs[8];
   int16     Lpf5thOrderDemodCoefs[16];
   int16     SatBpfLpfCoefs[16];
} PACKED_POSTFIX  IpcAmpFwdChFiltCoefsT;
*/

typedef PACKED_PREFIX struct
{
   IpcAmpFwdChScalCtlT     ScalCtlData;
/*   IpcAmpFwdChFiltCoefsT   FiltCoefs;  */      
} PACKED_POSTFIX  IpcDsvSetAmpFwdChParamsConfigMsgT ;    /* Amps parameters configuration data type */

typedef PACKED_PREFIX struct
{
   uint16      FwdChPolarity;
   uint16      FoccStreamAB;
   uint16      IdleModeStatusInit;
   uint16      Eoscale;     
   uint16      RxAgcAverRate;
   uint16      DcArcLoopGain;

} PACKED_POSTFIX  IpcDsvSetAmpFwdChCtrlConfigMsgT ;    /* Amps control configuration data type */


typedef PACKED_PREFIX struct
{
   int16  AgcStepPerDb[SYS_MAX_NUM_HYST_STATES_RXAGC];
   int16  NumberOfTargetStates;
} PACKED_POSTFIX  IpcDsvSetAmpFwdChAgcCalibParamsMsgT;

typedef PACKED_PREFIX struct
{
   uint16  WsCounter;
} PACKED_POSTFIX  IpcDsvSetAmpWordSyncCounterMsgT;

typedef PACKED_PREFIX struct
{
   uint16  Status;
} PACKED_POSTFIX  IpcDsvGetAmpWordSyncStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint16  LpfMode;
} PACKED_POSTFIX  IpcDsvSetAmpFwdLpfModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16  IpcPdmTarget;
   uint16  IpcPdmValue;
} PACKED_POSTFIX  IpcDsvSetAmpPdmValueMsgT;


typedef PACKED_PREFIX struct
{
   uint16  Status;
} PACKED_POSTFIX  IpcDsvGetAmpAfcStatusMsgT;

typedef PACKED_PREFIX struct
{
   uint16  LpfMode;
} PACKED_POSTFIX  IpcDsvSetAmpFwdFeFirLpfModeMsgT;

/*------------------------------------------------------------------------------*/
/* structure for Rx simulation message */
/*------------------------------------------------------------------------------*/

typedef PACKED_PREFIX struct 
{
   uint16 FwdChSimMsgId;
   PACKED_PREFIX union 
   {
      IpcDsvSetAmpModeMsgT                    SetAmpModeMsg;
      IpcDsvSetAmpBusyIdleProcessMsgT         SetAmpBusyIdleProcessModeMsg;
      IpcDsvAmpGetFmSignalStrengthMsgT        GetAmpFmSignalStrengthMsg;
      IpcDsvSetAmpDcOffsetCompMsgT            SetAmpDcOffsetCompMsg;
      IpcDsvSetAmpRxAgcCtrlMsgT               SetAmpRxAgcCtrlMsg;
      IpcDsvSetAmpAfcCtrlMsgT                 SetAmpAfcCtrlMsg;
      IpcDsvSetAmpFwdChCounterMsgT            SetAmpFwdChCounterMsg; 
      IpcDsvSetAmpFwdChParamsConfigMsgT       SetAmpFwdChParamsConfigMsg;
      IpcDsvSetAmpExpControlMsgT              SetAmpExpControlMsg;
      IpcDsvSetAmpHpfControlMsgT              SetAmpHpfControlMsg;
      IpcDsvSetAmpSatControlMsgT              SetAmpSatControlMsg;
      IpcDsvSetAmpWordSyncCounterMsgT         SetAmpWordSyncCounterMsg;
      IpcDsvGetAmpWordSyncStatusMsgT          GetAmpWordSyncStatusMsg;
      IpcDsvSetAmpFwdLpfModeMsgT              SetAmpFwdLpfModeMsg;
      IpcDsvSetAmpPdmValueMsgT                SetAmpPdmValueMsg;
      IpcDsvSetAmpFwdChCtrlConfigMsgT         SetAmpFwdChCtrlConfigMsg;
      IpcDsvSetAmpFwdChAgcCalibParamsMsgT     SetAmpFwdChAgcCalibParamsMsg;
      IpcDsvGetAmpAfcStatusMsgT               GetAmpAfcStatusMsg;
      IpcDsvSetAmpFwdFeFirLpfModeMsgT         SetAmpFwdFeFirLpfModeMsg;

   } PACKED_POSTFIX  Msg;
} PACKED_POSTFIX  IpcDsvAmpSendFwdChSimMsgMsgT ;


/*-------------------------------------------------**
** AMPS related message data structures DSPV -> CP **
**-------------------------------------------------*/
typedef enum
{
   FOCC_DATA = 0,
   FVC_DATA
} IpcCpAmpFwdChDataT;

typedef PACKED_PREFIX struct
{
   uint16   FwdChDataType;
   uint16   FwdChDemodBit27To16;
   uint16   FwdChDemodBit15To0;
   uint16   FwdControlChNumWords;
   uint16   FwdChBchErr;
   uint16   FwdChSyncLock;
} PACKED_POSTFIX  IpcCpAmpFwdChDataRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 SatDet;
} PACKED_POSTFIX  IpcCpAmpSatDetRspMsgT;



typedef enum
{
   AMPS_BIS_IDLE = 0,
   AMPS_BIS_BUSY
} BusyIdleT;

typedef PACKED_PREFIX struct
{
   uint16 BusyIdleStatus;
} PACKED_POSTFIX  IpcCpAmpBusyIdleRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 IpcNumGoodSync;
   uint16 IpcNumBadSync;
} PACKED_POSTFIX  IpcCpAmpWordSyncRspMsgT;


/* FAST MBOX: CP -> DSPV:
 * List of commands for the DSPV to use to manage the Idle Mode Power Saving */
/* These codes are to be used to drive the operation of the FIQ */
typedef enum
{
   NULL_FUNCTION = 0,
   ALL_RF_ON,
   FAST_OFF,
   FAST_ON,
   SLOW_OFF,
   SLOW_ON,
   NUM_IDLE_MODE_ENTRIES
} IpcCpAmpFwdChIdleModeRspT;

typedef PACKED_PREFIX struct
{
   uint16 RawRssiVal;       /* Raw Rssi 16 bit unsigned */
   uint16 PdmVal;           /* Pdm value */
   uint16 AgcTarget;
} PACKED_POSTFIX  IpcCpFmSignalStrengthRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 IpcTimer20ms;
   uint16 IpcTimer125us;
   uint16 IpcSampleOffset;
} PACKED_POSTFIX  IpcCpAmpFrameSyncDetectRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16  CompMode;
   uint16  ExpMode;
} PACKED_POSTFIX  IpcCpAmpCompExpStatusRspMsgT;

typedef PACKED_PREFIX struct
{
   int16  AfcPdmVal;
   int16  FreqErr;
} PACKED_POSTFIX  IpcCpAmpAfcStatusRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 NumBusyIdleBits;
   uint16 NumBusy;
   uint16 NumIdle;
} PACKED_POSTFIX  IpcCpAmpFwdBusyIdleStatusRspMsgT;

typedef PACKED_PREFIX struct
{
   uint16 NumBusyIdleBits;
} PACKED_POSTFIX  IpcDsvAmpFwdBusyIdleStatusMsgT;

typedef PACKED_PREFIX struct
{
    IpcLpcIROptEnT          LpcIROptEn;
    IpcLpcIRMaxLenT         LpcIRMaxLen;
    IpcLpcIRMinLenT         LpcIRMinLen;
    IpcLpcIRPctPwrT         LpcIRPctPwr;

    IpcFcbSubSampEnT        FcbSubSampEn;
    IpcFcbSubSampParmT      FcbSubSampParm;
    IpcFracPitchModeT       MicVoiceEncFracPitchMode;
    uint16                  V13ScaleFactor;                                    
} PACKED_POSTFIX  IpcDsvSendRevChSsoConfigurationMsgT; 

typedef PACKED_PREFIX struct
{
   uint16   SpkrVoiceDecPitchPreFltrMode;  /* voice decode pitch pre-filter mode switch */
   uint16   SpkrVoiceDecSynPostFltrMode;  /* voice decode synthesis post-filter mode switch */
   uint16   FrameErasureFcbExicitionMode;
} PACKED_POSTFIX  IpcDsvSendFwdChSsoConfigurationMsgT;


typedef PACKED_PREFIX struct
{
   uint16 ModuleId;
} PACKED_POSTFIX  IpcDsvVapAudioCfgQueryMsgT;

typedef PACKED_PREFIX struct
{
   uint16 CurrApp;
} PACKED_POSTFIX IpcCpAppInfoQueryRspMsgT;


/* ===== DSP Tone structures and definitions ===== */

#define IPC_TONE_CONTINUOUS  0xFFFF

typedef enum
{
    IPC_TONE_MODE_STOP = 0,   /* Stop tone */
    IPC_TONE_MODE_START,      /* Start new tone */
    IPC_TONE_MODE_ABORT       /* Stop tone without ramping down */
} IpcToneModeT;

typedef enum
{
    IPC_TONE_STATUS_SUCCESS,  /* Successfully performed requested action */
    IPC_TONE_STATUS_FAILED    /* Failed to perform requested action */
} IpcToneStatusT;

typedef enum
{
    IPC_TONE_PHASE_BEGIN,
    IPC_TONE_PHASE_MIDDLE,
    IPC_TONE_PHASE_END,
    IPC_TONE_PHASE_ALL        /* Single tone, include both ramp up and down */
} IpcTonePhaseT;

/* IPC_DSV_SEND_SPP_TONE_GEN_MSG */
typedef PACKED_PREFIX struct
{
   uint16   ToneId;           /* Tone id (not used by DSPV, returned in response msg)    */
   uint16   TonePath;         /* Tone path is Mic, Spkr or both (use IpcDsvDtmfPathEnum) */
   uint16   Mode;             /* Use IpcToneModeT */
   uint16   Duration;         /* # of 20-msec frames for tone playback; use
                                 IPC_TONE_CONTINUOUS for continuous tone (about 22 mins) */
   int16    Freq[2];          /* Up to 2 tone frequencies in Hz; 0 indicates no tone     */
   uint16   TonePhase;        /* Phase of tone, use IpcTonePhaseT */

} PACKED_POSTFIX  IpcDsvSendSppToneGenMsgT;

/* IPC_CP_SEND_SPP_TONE_GEN_RSP_MSG */
typedef PACKED_PREFIX struct
{
   uint16   ToneId;           /* Tone id */
   uint16   Mode;             /* Use IpcToneModeT */
   uint16   Status;           /* Use IpcToneStatusT */
} PACKED_POSTFIX  IpcCpSendSppToneGenRspMsgT;


/* ===== DSP Multimedia Applications structures ===== */


#define  IPC_NUM_DSPAPP_CHANNELS        1                       /* Total number of app channels available on this HW */
#define  IPC_APPCHAN0_MAX_WORDS      2048                       /* MAX words for App Channel 0 */
#define  IPC_MAX_NUM_APP_MSGS           6                       /* MAX number of messages sent on App Chan 0 */
#define  IPC_SBC_ENC_SHRD_MEM_OFFSET    IPC_APPCHAN0_MAX_WORDS  /* Shared mem offset for SBC Encode data. used only in CP */

/* When AppServiceId = IPC_APP_CHAN_SERVICE_CODE_DNLD_REQ/RSP, the DspAppMessageP->CmdHdr.AppId
indicates whether we're doing DSPV or DSPM dynamic code download */
typedef enum
{
   IPC_APP_CHAN_CODE_DOWNLOAD_SOURCE_DSPV = 1,
   IPC_APP_CHAN_CODE_DOWNLOAD_SOURCE_DSPM
}IpcDspAppChannelCodeDownloadSourceT;


typedef enum 
{
   /* App Channel Service requests from DSP to CP */
   IPC_APP_CHAN_SERVICE_CODE_DNLD_REQ = 1,
   IPC_APP_CHAN_SERVICE_APP_DATA_REQ,

   /* App Channel Service responses from CP to DSP */
   IPC_APP_CHAN_SERVICE_CODE_DNLD_RSP = 0x1001,
   IPC_APP_CHAN_SERVICE_APP_DATA_RSP
}IpcDspAppChannelServiceT;


typedef enum 
{
   IPC_APP_CHAN_OK = 0,
   IPC_APP_CHAN_ERR = -2000,
   IPC_APP_CHAN_ERR_INVALID_CHAN_IDX = -2001,
   IPC_APP_CHAN_ERR_INVALID_CHAN_STATE = -2002,
   IPC_APP_CHAN_ERR_INVALID_MSG_IDX = -2003,
   IPC_APP_CHAN_ERR_MSG_NOT_FOUND = -2004,
   IPC_APP_CHAN_ERR_NO_MEMORY_TO_ADD_MSG = -2005,
   IPC_APP_CHAN_ERR_FREAD = -2006,
   IPC_APP_CHAN_ERR_FWRITE = -2007,
   IPC_APP_CHAN_ERR_FSEEK = -2008,
    IPC_APP_CHAN_ERR_INVALID_FILE_ID = -2009,
    IPC_APP_CHAN_ERR_INVALID_APP_ID = -2010,
    IPC_APP_CHAN_ERR_APP_NOT_FOUND = -2011
} IpcDspAppChanErrCodesT;




typedef PACKED_PREFIX struct
{
    uint16    NumAppChannelsUsed;
    uint16    ChanWordSize[1];  /* Size of each of NumAppChannelsUsed channels */
} PACKED_POSTFIX  IpcCpConfigureAppchannelsMsgT; 


typedef enum {
   /* App messages from DSP to CP */
   IPC_APP_CP_REQ_FREAD = 1,
   IPC_APP_CP_REQ_FWRITE,         /* shared memory loopback */
   IPC_APP_CP_REQ_FCLOSE,         /* NOT USED */
   IPC_APP_CP_REQ_FILEINFO,       /* NOT USED */
   IPC_APP_CP_REQ_DISPLAY_PIXELS,
   IPC_APP_CP_MP3_STATUS,         /* Status specific to MP3 decoder App */
   IPC_APP_CP_APP_STATUS,         /* General APP status, used for all Apps */
   IPC_APP_CP_SET_COLOR_MAPPING,
   IPC_APP_CP_SOURCE_IMAGE_SIZE,
   IPC_APP_CP_IMAGE_SCALING,
   IPC_APP_CP_REQ_DISPLAY_PIXELS_SAME_COLOR,
   IPC_APP_CP_AAC_STATUS,         /* Status specific to AAC decoder App */
   IPC_APP_CP_MUSIC_SIDE_INFO,    /* Music side info for CP to cache */
   IPC_APP_CP_REQ_FRAME_DATA,     /* Request for next frame of data  */
   IPC_APP_CP_WMA_STATUS,         /* Status specific to WMA decoder App */
   IPC_APP_CP_WAV_STATUS,         /* Status specific to WAV decoder App */   
   IPC_APP_CP_ENC_WAV_STATUS,     /* Status specific to WAV encoder App */   
   IPC_APP_CP_ENC_AAC_STATUS,     /* Status specific to AAC encoder App */   

   /* App messages from CP to DSP */
   IPC_APP_DSP_RSP_FREAD = 0x1001,
   IPC_APP_DSP_RSP_FWRITE,
   IPC_APP_DSP_RSP_FCLOSE,
   IPC_APP_DSP_RSP_FILEINFO,
   IPC_APP_DSP_RSP_FRAME_DATA     /* Response for frame of data  */

}IpcDspAppMsgIdT;

typedef enum
{
   IPC_GRAPHICS_FORMAT_BMP = 1,
   IPC_GRAPHICS_FORMAT_WBMP,
   IPC_GRAPHICS_FORMAT_GIF,
   IPC_GRAPHICS_FORMAT_TIF,
   IPC_GRAPHICS_FORMAT_JPEG
}IpcDspGraphicsFormatT;

/* ======================================================== */
/* The following structures are sent through the AppChannel */
/* ======================================================== */

/* Request structures: from DSP to CP */
/* ---------------------------------- */

/* IPC_APP_CP_REQ_FREAD message structure */
typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
   uint32      AbsFileOffsetBytes;
   uint16      NumBytesToRead;
   uint16      RefNum;  /* DSP-assigned request id. */
} PACKED_POSTFIX  IpcCpFreadRequestT;

/* IPC_APP_CP_REQ_FWRITE message structure */
typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
   uint32      AbsFileOffsetBytes;
   uint16      NumBytesToWrite;
   uint16      RefNum;  /* DSP-assigned request id. */
} PACKED_POSTFIX  IpcCpFwriteRequestT;

/* IPC_APP_CP_REQ_FCLOSE message structure */
typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
} PACKED_POSTFIX  IpcCpFcloseRequestT;

/* IPC_APP_CP_REQ_FILEINFO message structure */
typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
} PACKED_POSTFIX  IpcCpFileInfoRequestT;


/* IPC_APP_CP_REQ_DISPLAY_PIXELS message structure */
typedef PACKED_PREFIX struct {
   /* 0-based coordinate of the top left corner, relative */
   /* to the display area size specified in an earlier    */
   /* IpcCpDisplayImageSize message.                      */
   uint16      Xleft;
   uint16      Ytop;

   /* Width and height of this block. */
   uint16      Xpixels;
   uint16      Ypixels;


   /* The data will consist of N 24-bit RGB triplets, packaged */
   /* into two 16-bit words, for a total size of 2N words,     */
   /* where N=Xpixels*Ypixels.                                  */
} PACKED_POSTFIX  IpcCpDisplayPixelBlock;



typedef PACKED_PREFIX struct {
   /* Band frequency range. */
   uint16      BandMinHz;
   uint16      BandMaxHz;

   /* Some relative strength number */
   uint16      BandAttenuation; 
} PACKED_POSTFIX  IpcCpEqualizerBandT;


/* IPC_APP_CP_MP3_STATUS message structure */
typedef enum
{
    IPC_MP3_OK,
    IPC_MP3_FRAME_SYNC_NOT_FOUND,
    IPC_MP3_LOSS_OF_FRAME_SYNC,
    IPC_MP3_DATA_OUT_OF_SEQUENCE,
    IPC_MP3_RAMPING_DOWN,
    IPC_MP3_TERMINATED,
    IPC_MP3_INSUFFICIENT_MAIN_DATA,
    IPC_MP3_LAYER_NOT_SUPPORTED /* check for ironic layer 1 stream */
} IpcMp3StatusCodesT; 

typedef PACKED_PREFIX struct {
   uint16      FileId;       /* CP identifier of this file */
   uint16      Mp3Status;    /* Use IpcMp3StatusCodesT */

   /* Playback status information for CP */
   uint16      AvgBytesPerSecond;
   uint32      FirstFrameOffsetBytes;
   uint32      BitRate;
   uint16      SamplesPerSecond;
   uint16      NumEqualizerBands;

   /* The data will consist of N IpcCpEqualizerBandT structures */
   /* where N=NumEqualizerBands.                                */
} PACKED_POSTFIX  IpcCpMP3PlaybackStatusT;

/* IPC_APP_CP_AAC_STATUS message structure */
typedef enum 
{
    IPC_AAC_OK = 0,
    IPC_AAC_TERMINATED,

    /* Following codes are fatal; DSPV must send IPC_CP_APP_STATUS_MSG for CP to stop app */
    IPC_AAC_CODEC_ERR,
    IPC_AAC_PARSER_ERR
} IpcAACStatusCodesT;

/* IPC_APP_CP_WAV_STATUS message structure */
typedef enum   
{
    IPC_WAV_OK = 0,
    IPC_WAV_TERMINATED,

    /* Following codes are fatal; DSPV must send IPC_CP_APP_STATUS_MSG for CP to stop app */
    IPC_WAV_CODEC_ERR,
    IPC_WAV_PARSER_ERR
} IpcWAVStatusCodesT; /* free to add or remove */

/* IPC_APP_CP_ENC_WAV_STATUS message structure */
typedef enum   
{
    IPC_ENC_WAV_OK = 0,
    IPC_ENC_WAV_TERMINATED,
    IPC_ENC_WAV_CODEC_ERR,
    IPC_ENC_WAV_PARSER_ERR
} IpcAppEncWAVStatusT; 

/* IPC_APP_CP_ENC_AAC_STATUS message structure */
typedef enum   
{
    IPC_ENC_AAC_OK = 0,
    IPC_ENC_AAC_TERMINATED,
    IPC_ENC_AAC_CODEC_ERR,
    IPC_ENC_AAC_PARSER_ERR
} IpcAppEncAACStatusT; 

typedef enum
{
    IPC_AAC_MP4_OK = 0,
    IPC_AAC_MP4_EOF,
    IPC_AAC_MP4_HASROOTOD,

    IPC_AAC_MP4_FILENOTFOUND = -1,
    IPC_AAC_MP4_BADPARAM = -2,
    IPC_AAC_MP4_NOMEMORY = -3,
    IPC_AAC_MP4_IO_ERR = -4,
    IPC_AAC_MP4_NOLARGEATOMSUPPORTED = -5,
    IPC_AAC_MP4_BADDATA = -6,
    IPC_AAC_MP4_VERSIONNOTSUPPORTED = -7,
    IPC_AAC_MP4_INVALIDMEDIA = -8,

    IPC_AAC_MP4_DATAENTRYTYPENOTSUPPORTED = -100,
    IPC_AAC_MP4_NOQTATOM = -500,
    IPC_AAC_MP4_NOTIMPLEMENTED = -1000

} IpcAacMp4SubStatusT;

/* IPC_APP_CP_WMA_STATUS message structure */
typedef enum 
{
    IPC_WMA_OK = 0,
    IPC_WMA_TERMINATED,

    /* Following codes are fatal; DSPV must send IPC_CP_WMA_STATUS_MSG for CP to stop app */
    IPC_WMA_CODEC_ERR,
    IPC_WMA_PARSER_ERR
} IpcWMAStatusCodesT;

/* Audio Object Type for AAC MP4 */
typedef enum
{
    IPC_AOT_NULL_OBJECT      = 0,
    IPC_AOT_AAC_MAIN         = 1,
    IPC_AOT_AAC_LC           = 2,
    IPC_AOT_AAC_SSR          = 3,
    IPC_AOT_AAC_LTP          = 4,
    IPC_AOT_SBR              = 5,
    IPC_AOT_AAC_SCAL         = 6,
    IPC_AOT_TWIN_VQ          = 7,
    IPC_AOT_CELP             = 8,
    IPC_AOT_HVXC             = 9,
    IPC_AOT_RSVD_10          = 10, /* (reserved)                                */
    IPC_AOT_RSVD_11          = 11, /* (reserved)                                */

    /* not supported */
    IPC_AOT_TTSI             = 12, /* TTSI Object                               */
    IPC_AOT_MAIN_SYNTH       = 13, /* Main Synthetic Object                     */
    IPC_AOT_WAV_TAB_SYNTH    = 14, /* Wavetable Synthesis Object                */
    IPC_AOT_GEN_MIDI         = 15, /* General MIDI Object                       */
    IPC_AOT_ALG_SYNTH_AUD_FX = 16, /* Algorithmic Synthesis and Audio FX Object */
    IPC_AOT_ER_AAC_LC        = 17, /* Error Resilient(ER) AAC LC Object         */
    IPC_AOT_RSVD_18          = 18, /* (reserved)                                */
    IPC_AOT_ER_AAC_LTP       = 19, /* Error Resilient(ER) AAC LTP Object        */
    IPC_AOT_ER_AAC_SCAL      = 20, /* Error Resilient(ER) AAC Scalable Object   */
    IPC_AOT_ER_TWIN_VQ       = 21, /* Error Resilient(ER) TwinVQ Object         */
    IPC_AOT_ER_BSAC          = 22, /* Error Resilient(ER) BSAC Object           */
    IPC_AOT_ER_AAC_LD        = 23, /* Error Resilient(ER) AAC LD Object         */
    IPC_AOT_ER_CELP          = 24, /* Error Resilient(ER) CELP Object           */
    IPC_AOT_ER_HVXC          = 25, /* Error Resilient(ER) HVXC Object           */
    IPC_AOT_ER_HILN          = 26, /* Error Resilient(ER) HILN Object           */
    IPC_AOT_ER_PARA          = 27, /* Error Resilient(ER) Parametric Object     */
    IPC_AOT_RSVD_28          = 28, /* (reserved)                                */
    IPC_AOT_RSVD_29          = 29, /* (reserved)                                */
    IPC_AOT_RSVD_30          = 30, /* (reserved)                                */
    IPC_AOT_RSVD_31          = 31  /* (reserved)                                */

} IpcAacAudioObjectTypeT;

typedef enum
{
    IPC_AAC_CONFIG_UNINIT,
    IPC_AAC_CONFIG_MONO,
    IPC_AAC_CONFIG_STEREO
} IpcAacMonoStereoConfigT;

typedef PACKED_PREFIX struct {
    uint16    FileId;           /* CP identifier of this file  */
    uint16    AACStatus;        /* Use IpcAACStatusCodesT      */
    int16     Mp4SubStatus;     /* Use IpcAacMp4SubStatusT     */

    uint16    AudioObjectType;  /* Use IpcAacAudioObjectTypeT  */
    uint16    ChannelConfig;    /* Use IpcAacMonoStereoConfigT */
    uint32    BitRate;
    uint16    SamplingRate;
    uint16    ExtSamplingRate;  /* for SBR */

} PACKED_POSTFIX  IpcCpAACCodecStatusT;

typedef PACKED_PREFIX struct {
    uint16    FileId;           /* CP identifier of this file  */
    uint16    WMAStatus;        /* Use IpcWMAStatusCodesT      */
    uint16    ChannelConfig;    /* Use IpcAacMonoStereoConfigT */
    uint32    BitRate;
    uint16    SamplingRate;
} PACKED_POSTFIX  IpcCpWMACodecStatusT;

typedef PACKED_PREFIX struct {
    uint16    FileId;           /* CP identifier of this file  */
    uint16    WAVStatus;        /* Use IpcWAVStatusCodesT      */
    uint16    ChannelConfig;    /* Use IpcAacMonoStereoConfigT */
    uint32    BitRate;
    uint16    SamplingRate;
} PACKED_POSTFIX  IpcCpWAVCodecStatusT;  

typedef PACKED_PREFIX struct {
   uint16   InputFileId;        /* associated file id       */
   uint16   AppChannelConfig;   /* use IpcDsvAppChConfigT   */
   uint16   AppSampleRate;      /* use IpcDsvAppSampleRateT */
   uint16   AppEncWAVStatus;    /* use IpcAppEncWAVStatusT  */
} PACKED_POSTFIX  IpcCpEncWAVStatusT;  

typedef PACKED_PREFIX struct {
   uint16   InputFileId;        /* associated file id       */
   uint16   AppChannelConfig;   /* use IpcDsvAppChConfigT   */
   uint16   AppSampleRate;      /* use IpcDsvAppSampleRateT */
   uint16   AppEncAACStatus;    /* use IpcAppEncAACStatusT  */
} PACKED_POSTFIX  IpcCpEncAACStatusT;  

/* IPC_APP_CP_APP_STATUS message structure */
typedef enum
{
    IPC_APP_CHAN_NOT_ENOUGH_DATA = 1
} IpcApplicationStatusT; 

typedef PACKED_PREFIX struct
{
    uint16  Status;   /* General APP Status (IpcApplicationStatusT) */
} PACKED_POSTFIX  IpcCpApplicationStatusT;

/* IPC_APP_CP_MUSIC_SIDE_INFO message structure */
typedef PACKED_PREFIX struct
{
    uint16 FileId;             /* CP identifier of this file */
    uint16 InfoIdentifier;     /* DSPV identifier for type of Side Info (not used by CP) */
    uint32 StartOffsetBytes;   /* Absolute file start offset for the side info */
    uint32 SizeBytes;          /* Size in bytes of the side info */
} PACKED_POSTFIX  IpcCpMusicSideInfoT;

/* IPC_APP_CP_REQ_FRAME_DATA message structure */
typedef PACKED_PREFIX struct
{
    uint16 FileId;  /* CP identifier of this file */
    uint16 RefNum;  /* DSP-assigned request ID    */
} PACKED_POSTFIX  IpcCpFrameDataReqT;

/* IPC_CP_DSPM_GET_PARMS_RSP_MSG message structure */
typedef PACKED_PREFIX struct
{
   uint16  AfcVal;
} PACKED_POSTFIX  IpcCpDspmAfcParmsRspMsgT;


/* Response structures: from CP to DSP */
/* ----------------------------------- */

typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
   uint32      AbsFileOffsetBytes;
   uint16      NumBytesToRead;
   int16       NumBytesReadOrError;
   uint16      RefNum;  /* DSP-assigned request id. */
   /* Data bytes follow here */
} PACKED_POSTFIX  IpcDspFreadResponseT;

typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
   uint32      AbsFileOffsetBytes;
   uint16      NumBytesToWrite;
   int16       NumBytesWrittenOrError;
   uint16      RefNum;  /* DSP-assigned request id. */
} PACKED_POSTFIX  IpcDspFwriteResponseT;


typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
   int16       ResultOrError;
} PACKED_POSTFIX  IpcDspFcloseResponseT;

typedef PACKED_PREFIX struct {
   uint16      FileId;  /* CP identifier of this file */
   uint32      FileLengthBytes;
   uint32      CurAbsByteOffset;
   /* 
      Can add other info if needed: date/time of creation, of last modification, 
   etc.
   */

} PACKED_POSTFIX  IpcDspFileInfoResponseT;

typedef PACKED_PREFIX struct {
   uint16      FileId;              /* CP identifier of this file */
   uint16      SamplingRate;        /* Sampling rate */
   int16       NumBytesReadOrError; /* If error, use IpcDspAppChanErrCodesT enum */
   uint16      RefNum;              /* DSP-assigned request ID */

   /* Data bytes follow here */
} PACKED_POSTFIX  IpcDspFrameDataRespT;

/* Bit mask values for setting image transformation options. */
typedef enum
{
    IPC_IMG_TRANSFORM_SCALE = 0x0001, 				 /* 0 = clip, 1 = scale */
    IPC_IMG_TRANSFORM_PRESERVE_ASPECT_RATIO = 0x0002 /* Matters only in scale mode */
} IpcImageTransformationOptionsT;

typedef PACKED_PREFIX struct {
   /* DSP will tell CP how big the displayed image will be (after clipping or scaling).
   	This size will be less than or equal to DestWidthPixels x DestHeightPixels
	specified by the IpcDspImageTransformT message to DSP
	*/
   uint16      NumXpixelsToBeUsed;		
   uint16      NumYpixelsToBeUsed;

	/* DSP will tell CP how much of compression (fraction < 1) or expansion */
	/* will be performed on each dimension.								    */
   uint16      XscaleNumerator;		
   uint16      XscaleDenominator;		
   uint16      YscaleNumerator;		
   uint16      YscaleDenominator;		

} PACKED_POSTFIX  IpcCpDisplayImageSizeT;


/* IPC_APP_CP_SET_COLOR_MAPPING message structure */
typedef PACKED_PREFIX struct {
   uint16      NumBitsPerPixel;	/* 1,2,4, or 8 */

   /* The data is a mapping table from index to RGB, where   */
   /* index varies from 0 to M-1, where M=2^NumBitsPerPixel. */
   /* The data will consist of M RGB16 values (i.e. M words) */
} PACKED_POSTFIX  IpcCpColorMappingRequestT;


typedef PACKED_PREFIX struct {
   /* DSP will tell CP how big the source image is. */
   /* Full size of the original image without clipping or scaling. */
   uint16      NumOrigImageXpixels;
   uint16      NumOrigImageYpixels;
   uint16      NumOrigImageBitsPerPixel;
   /* Typically 1-24, ranging from monochrome to 24-bit RGB */
} PACKED_POSTFIX  IpcCpSourceImagePropertiesT;

/* IPC_APP_CP_REQ_DISPLAY_PIXELS message structure */
typedef PACKED_PREFIX struct {
   /* 0-based coordinate of the top left corner, relative */
   /* to the display area size specified in an earlier    */
   /* IpcCpDisplayImageSize message.                      */
   uint16      Xleft;
   uint16      Ytop;

   /* Width and height of this block. */
   uint16      Xpixels;
   uint16      Ypixels;

   /* The data will consist of N=Xpixels*Ypixels pixel     */
   /* descriptors. In the default 16-bit RGB case, the	   */
   /* data will consist of N 16-bit RGB triplets for a     */
   /* total size of N words. 							   */

   /* In case an IpcCpColorMappingRequestT has established */
   /* a mapping from K bits to RGB, the data here will be  */
   /* made up of K-bit entries stuffed into 16-bit words.  */
   /* We will constrain K to be a divisor of 8 (1,2,4,8).  */
   /* Hence, given that pixels are transmitted in row-major*/
   /* order, the total number of words here will be        */
   /*    Ypixels * CEIL (Xpixels * K / 16)				   */

} PACKED_POSTFIX  IpcCpDisplayPixelBlockT;

typedef  PACKED_PREFIX   struct
{
   uint16   InputFileId;
   uint16   BitmapType;		/* To differentiate between various bitmap formats */

   /* Specify the source image rectangle to display.            */
   /* Use X,Y=(0,0) and DX,DY=(0,0) to display the WHOLE IMAGE. */

   uint16   SourceImageStartLeftX;
   uint16   SourceImageStartTopY;
   uint16   SourceImageDX;
   uint16   SourceImageDY;


	/*  Size of the destination rectangle to map the source image to. 
		The coordinates in the returned pixel data from DSP to CP 
		will be relative to this destination rectangle, 0-based.
	 */
   uint16   DestWidthPixels;
   uint16   DestHeightPixels;

   uint16   TransformationOptions;	 /* IpcImageTransformationOptionsT */

   uint16   ConversionPreferences; 	/* IpcDspImageConversionPreferenceT -->
   									  How CP prefers to receive pixel data: 
   									  with/without fixed color blocks, 
   									  use/not use palettes, etc. */
} PACKED_POSTFIX  IpcDsvStartBmpAppT;


/* This is a dummy message to make sure that the size of the IpcDspAppCmdArgsT 
 * structure is a multiple of 2 words; this is required so that the first DATA field is 
 * always aligned on a 32-bit boundary. The number of words in the "Dummy" field must
 * be EVEN, and must equal or exceed the largest sub-structure in the union.
 */
typedef PACKED_PREFIX struct {
    uint16 Dummy [10];
} PACKED_POSTFIX  IpcAppMaxCmdArgsT;


typedef PACKED_PREFIX union
{
  IpcCpFreadRequestT             FreadReq;
  IpcDspFreadResponseT           FreadRsp;

  IpcCpFwriteRequestT            FwriteReq;
  IpcDspFwriteResponseT          FwriteRsp;

  IpcCpFcloseRequestT            FcloseReq;
  IpcDspFcloseResponseT          FcloseRsp;

  IpcCpFileInfoRequestT          FileInfoReq;
  IpcDspFileInfoResponseT        FileInfoRsp;

  IpcCpSourceImagePropertiesT    SourceImageProperties;
  IpcCpDisplayImageSizeT         DisplaySizeUsedByImage;

  IpcCpColorMappingRequestT      ColorMappingReq;
  IpcCpDisplayPixelBlockT        DisplayPixelsReq;

  IpcCpApplicationStatusT        ApplicationStatus;
  IpcCpMP3PlaybackStatusT        MP3PlaybackStatus;

  IpcCpAACCodecStatusT           AACCodecStatus;  /* added for AAC */
  IpcCpWAVCodecStatusT           WAVCodecStatus;  /* added for WAV */   
  IpcCpWMACodecStatusT           WMACodecStatus;  /* added for WMA */   

  IpcCpMusicSideInfoT            MusicSideInfo;

  IpcCpFrameDataReqT             FrameDataReq;
  IpcDspFrameDataRespT           FrameDataRsp;

  IpcAppMaxCmdArgsT              MaxCmdArgs;      /* Dummy structure used for alignment */

} PACKED_POSTFIX  IpcDspAppCmdArgsT;

/* It is assumed that every message consists of 3 parts:
   the first 3 words are the same for every message: Msg Id, App Id, Data Size;
    the next structure differs for different command, but is part of the same union type, 
    and therefore has the same size in every message;
    and finally the third section contains 0 or more data words that may be needed to 
    specify the message (e.g., an FWRITE message carries data to be written)
 */

typedef PACKED_PREFIX struct 
{
    PACKED_PREFIX struct
    {
      uint16             MsgId;        /* use IpcDspAppMsgIdT */
      uint16             AppId;       /* Which application (IpcDspAppIdT) */
      uint16             NumDataWords; /* K>=0 : size of data following the command */
      IpcDspAppCmdArgsT  CmdArgs;
    } PACKED_POSTFIX  CmdHdr;

    uint16 Data[1];    /* K (0 or more) 16-bit-words */

} PACKED_POSTFIX  IpcDspAppMessageT;


typedef PACKED_PREFIX struct
{
   uint16            NumAppMsgs;  /* 1 or more IpcDspAppMessageT structures */
   IpcDspAppMessageT AppMsg [1];  /* Message array of size 'NumAppMsgs'     */

} PACKED_POSTFIX  IpcDspAppChanDataT;

typedef PACKED_PREFIX struct
{
    uint16  AppServiceId;   /* use IpcDspAppChannelServiceT */
    uint16  TotalNumBytes;  /* Total number of bytes used in Shared Memory 
                        including THESE first 6 header bytes */

    IpcDspAppChanDataT AppData;  /* Application channel data */

} PACKED_POSTFIX  IpcDspAppChannelT;

#ifdef MTK_CBP
typedef PACKED_PREFIX struct
{
    uint8    rateReduction;      /*rate reduction of current call,0 if no call*/
} PACKED_POSTFIX  IpcValRateReductionMsgT;
#endif


/* ============================================================= */
/* The following structures are sent through the IPC Mailbox I/F */
/* ============================================================= */

typedef enum
{
    IPC_APP_NULL     = 0x0000,   
    IPC_APP_TEST     = 0x0001,           /* SHARED MEMORY TEST                       */
    IPC_APP_MIDI     = 0x0002,           /* MIDI synthesizer                         */  
    IPC_APP_MP3      = 0x0004,           /* MP3 decoder                              */
    IPC_APP_AAC      = 0x0008,           /* AAC decoder                              */
    IPC_APP_WMA      = 0x0010,           /* WMA decoder                              */
    IPC_APP_WAV      = 0x0020,           /* WAV decoder                              */
    IPC_APP_SO3      = 0x0040,           /* EVRC-A                                   */
    IPC_APP_SO17     = 0x0080,           /* QCELP13K                                 */
    IPC_APP_GSMFR    = 0x0100,           /* GSM FR                                   */
    IPC_APP_GSMHR    = 0x0110,           /* GSM HR                                   */
    IPC_APP_GSMEFR   = 0x0200,           /* GSM EFR                                  */
    IPC_APP_SO68     = 0x0400,           /* EVRC-B                                   */
    IPC_APP_ENC_WAV  = 0x0800,           /* WAV encoder                              */
    IPC_APP_ENC_AAC  = 0x0810,           /* AAC encoder. APP ID is not bit-map       */
    IPC_APP_TONE     = 0x1000,           /* Tone Generation App.                     */
    IPC_APP_AMR      = 0x2000,           /* AMR NB                                   */
    IPC_APP_AMRWB    = 0x2100,           /* AMR WB                                   */
    IPC_APP_SO73     = 0x4000,           /* EVRC-NW. support only EVRC-NW NB vocoder */
    IPC_APP_SO73WB   = 0x8000            /* EVRC-NW. support only EVRC-NW WB vocoder */
} IpcDspAppIdT;

typedef enum
{
    IPC_IMG_CONV_PREF_ALLOW_FIXED_BLOCKS  = 0x0001, /* 0 means do not use fixed color blocks */
    IPC_IMG_CONV_PREF_PALETTE_IF_POSSIBLE = 0x0002 /* 0 means do not use palettes, just RGBs */
} IpcDspImageConversionPreferenceT;

/*
  App mode commands from CP to DSP
  All apps must support at least START and STOP
 */
typedef enum
{
    IPC_APPMODE_START = 1,
    IPC_APPMODE_STOP
} IpcDspAppModeT;

/*
  App mode responses from DSP to CP
 */
typedef enum
{
   IPC_APPMODE_RSP_STARTED = 1,
   IPC_APPMODE_RSP_STOPPED,
   IPC_APPMODE_RSP_APPLICATION_NOT_FOUND,
   IPC_APPMODE_RSP_UNABLE_TO_START,
   IPC_APPMODE_RSP_UNABLE_TO_STOP
} IpcCpAppModeRspT;

typedef enum
{
    IPC_APP_CH_CONFIG_MONO,
    IPC_APP_CH_CONFIG_STEREO
} IpcDsvAppChConfigT;

 typedef enum 
{
   SAMPLE_RATE_8KHZ,
   SAMPLE_RATE_11P025KHZ,
   SAMPLE_RATE_12KHZ,
   SAMPLE_RATE_16KHZ,
   SAMPLE_RATE_22P05KHZ,
   SAMPLE_RATE_24KHZ,
   SAMPLE_RATE_32KHZ,
   SAMPLE_RATE_44P1KHZ,
   SAMPLE_RATE_48KHZ,
} IpcDsvAppSampleRateT;

typedef enum 
{
   HIGH_BIT_RATE_HIGH_QUALITY,
   MID_BIT_RATE_MID_QUALITY,
   LOW_BIT_RATE_LOW_QUALITY,
} IpcDsvAppBitRateT;

typedef  PACKED_PREFIX   struct
{
   uint16   InputFileId;
   uint16   OutputFileId;
   uint16   ByteTransferSize; /* How many bytes read/written each time */
} PACKED_POSTFIX  IpcDsvStartTestAppT;

typedef  PACKED_PREFIX   struct
{
   uint16   InputFileId;
   uint32   InitialFileOffsetBytes;
   uint16   UseFrameRequests;      /* TRUE if DSPv to request frames, FALSE for bitstream */
} PACKED_POSTFIX  IpcDsvStartMP3AppT;

typedef  PACKED_PREFIX   struct
{
   uint16   InputFileId;
   uint32   InitialFileOffsetBytes;
} PACKED_POSTFIX  IpcDsvStartAACAppT;

typedef  PACKED_PREFIX   struct
{
   uint16   InputFileId;
   uint32   InitialFileOffsetBytes;
} PACKED_POSTFIX  IpcDsvStartWMAAppT;

typedef  PACKED_PREFIX   struct        
{
   uint16   InputFileId;
   uint32   InitialFileOffsetBytes;
} PACKED_POSTFIX  IpcDsvStartWAVAppT;

typedef  PACKED_PREFIX   struct        
{
   uint16   InputFileId;        /* associated file id       */
   uint16   AppChannelConfig;   /* use IpcDsvAppChConfigT   */
   uint16   AppSampleRate;      /* use IpcDsvAppSampleRateT */
} PACKED_POSTFIX  IpcDsvStartEncWAVAppT;

typedef  PACKED_PREFIX   struct        
{
   uint16   InputFileId;        /* associated file id       */
   uint16   AppChannelConfig;   /* use IpcDsvAppChConfigT   */
   uint16   AppSampleRate;      /* use IpcDsvAppSampleRateT */
   uint16   AppBitRate;         /* use IpcDsvAppBitRateT. High quality high bit rate , Mid quality mid bit rate, Low quality low bit rate*/ 
} PACKED_POSTFIX  IpcDsvStartEncAACAppT;

typedef  PACKED_PREFIX   struct
{
   uint16   InputFileId;
   uint16   MaxAvailableWidthPixels;
   uint16   MaxAvailableHeightPixels;
} PACKED_POSTFIX  IpcDsvStartJPEGAppT;

typedef PACKED_PREFIX struct
{
   uint16   InputFileId;
} PACKED_POSTFIX  IpcDsvStartVocoderAppT;

typedef PACKED_PREFIX struct
{
   uint16   InputFileId;
} PACKED_POSTFIX  IpcDsvStartMidiAppT;

typedef PACKED_PREFIX union
{
   IpcDsvStartTestAppT     StartTestApp;
   IpcDsvStartMP3AppT      StartMP3App;
   IpcDsvStartAACAppT      StartAACApp;
   IpcDsvStartJPEGAppT     StartJPEGApp;
   IpcDsvStartBmpAppT      StartBmpApp;
   IpcDsvStartVocoderAppT  StartVocoderApp;
   IpcDsvStartMidiAppT     StartMidiApp;
   IpcDsvStartWMAAppT      StartWMAApp;
   IpcDsvStartWAVAppT      StartWAVApp; 
   IpcDsvStartEncWAVAppT   StartEncWAVApp; 
   IpcDsvStartEncAACAppT   StartEncAACApp; 
} PACKED_POSTFIX  IpcDsvAppParamsT;

/* App mode messaging is done through regular IPC mailbox mechanism */

/* IPC_DSV_APP_MODE_MSG message structure */
typedef PACKED_PREFIX struct 
{
   uint16            AppId;               /* Which application (IpcDspAppIdT)  */
   uint16            AppMode;             /* Start, stop, etc. (IpcDspAppModeT)*/

   /* 
      App-specific data, 0 or more 16-bit words. Depends on the state of application
   (i.e. resuming an app may involve sending different data than starting an app)

   For example, to start MP3 only one 16-bit word is needed -- input file id.

   When resuming an app, CP will send back the data that the DSP passed to CP
   when it was suspended.
   */
                         
   IpcDsvAppParamsT  AppParams;  
} PACKED_POSTFIX  IpcDsvSendAppModeMsgT;

/* IPC_CP_APP_MODE_RSP_MSG message structure */
typedef PACKED_PREFIX struct 
{
   uint16         AppId;               /* Which application (IpcDspAppIdT) */
   uint16         AppModeRsp;       /* Response code (IpcCpAppModeRspT) */

   /*
   DSP may use this area to pass back to CP any app-specific data.
   For example, 
   may be used to save app state at SUSPEND instant (not interpreted by CP) or
   to pass back some status information at the START (interpreted by CP).
    */
   uint16         AppDataNumWords;                          
   uint16         AppData[1]; 
} PACKED_POSTFIX  IpcCpAppModeRspMsgT;

/* IPC_CP_APP_STATUS_MSG message structure */
typedef enum
{
    IPC_APP_CHAN_NOT_READY_ERR = 1,
    IPC_APP_END_OF_FILE,
    IPC_APP_CODEC_FAILURE,
    IPC_APP_PARSER_FAILURE,
    IPC_APP_FILE_SYNC_ERR
} IpcAppStatusT;

typedef PACKED_PREFIX struct 
{
   uint16           AppId;        /* Which application (IpcDspAppIdT)   */
   uint16           AppStatus;    /* Application status (IpcAppStatusT) */
   uint16           FatalErrFlag; /* TRUE if this is a fatal error      */
   uint16           ErrCount;     /* # times this error occurred        */
} PACKED_POSTFIX  IpcCpAppStatusMsgT;






/* Image display related messages (for JPEG and possibly other */
/* imaging applications).                             */

/* Regular IPC message. */
typedef PACKED_PREFIX struct {
   /* DSP will tell CP how many pixels will be occupied */
   /* by an image (based on the initial DSP message     */
   /* that specified the available screen real estate). */
   /* Obviously the width and height here must not      */
   /* exceed the available area.                        */
   /* CP will decide how to position the image within   */
   /* the available space (e.g. center it in a larger   */
   /* rectangle).                             */
   uint16      NumXpixelsToBeUsed;
   uint16      NumYpixelsToBeUsed;
} PACKED_POSTFIX  IpcCpDisplayImageSize;


/* Regular IPC message. */
typedef PACKED_PREFIX struct {
   /* Do we need it at all??????? Probably not! */

   uint16      NumXpixelsToBeUsed;
   uint16      NumYpixelsToBeUsed;
} PACKED_POSTFIX  IpcDspDisplayImageSizeResponseT;

typedef struct
{
    uint16 DftSize;
    uint16 DftStage;
    uint16 *Fft1LupP;
    uint16 *PhsTblP;
    uint16 *BitRevTabP;	
} DftCfgParmsT;

typedef PACKED_PREFIX struct
{
   int16    Indx;              
   int16    BufIndxStep;      
   int16    DftBufSize;       
   int16    BufOfst;          
   int16    LastStep;          
   int16    LastExp;           
   int16    AdjExp;            
   int16    ChanPtr;           
   int16    StepSzeExp;        
   int16    CorrIndx;          
   int16    PartCntlPtr;       
   int16    ErrPtr;            
   int16    TmpBufSize;       
   int16    RndValue;         
   int16    TmpQ;              
   int16    *CoeffPtr;         
   int16    DC_Value;          
   int16    FO_Value;          
   int16    PartNum;           
   int16    ConstrPos;        
} PACKED_POSTFIX  IpcDsvAudioAecTempParamsT;


/* IPC_DSV_SEND_AUDIO_PC_MODE_MSG. CP->DSPv PC data mode control message */
typedef enum
{
    IPC_AUDIO_PC_MODE_DISABLE                 = 0,                 
    IPC_AUDIO_PC_MODE_ENABLE                  = 1,            /* PC_MIC data will be used in microphone path. DSPv will send PC_SPKR data to CP */
    IPC_AUDIO_PC_MIC_PCM_DATA_MIX             = 2,            /* PC_MIC pcm data will be mixed with voice ADC MIC pcm data */
    IPC_AUDIO_PC_SPKR_PCM_DATA_MIX            = 3,            /* PC_MIC pcm data will be mixed with decoded spkr pcm data */
    IPC_AUDIO_PC_BOTH_MIC_SPKR_PCM_DATA_MIX   = 4             /* PC_MIC pcm data will be mixed with both ADC MIC pcm date and decoded spkr pcm data */
} IpcDsvAudioPcModeT;

typedef PACKED_PREFIX struct
{
   uint16 PcModeCtrl;
} PACKED_POSTFIX  IpcDsvAudioSendPcModeMsgT;


/* IPC_DSV_SEND_MIC_PC_DATA_MSG. CP->DSPv PCM data for microphone path */
typedef PACKED_PREFIX struct
{
   uint16   MicPathPcmData[PCM_DATA_FRAME_SIZE_WORDS]; 
} PACKED_POSTFIX  IpcDsvSendMicPcDataMsgT;


/* IPC_CP_SEND_SPKR_PC_DATA_MSG. DSPv->CP PCM data for speaker path */
typedef PACKED_PREFIX struct
{
   uint16   SpkrPathPcmData[PCM_DATA_FRAME_SIZE_WORDS]; 
} PACKED_POSTFIX  IpcCpSendSpkrPcDataMsgT;

/* IPC_CP_APP_HIFI_DAC_SAMPLING_RATE_MSG & IPC_DSV_APP_HIFI_DAC_SAMPLING_RATE_RSP_MSG */
typedef PACKED_PREFIX struct
{
   uint16 HifiDacFs;       /* actual sampling rate such as 8000, 11025, 12000, 16000, 22050, .... or 48000  */
} PACKED_POSTFIX  IpcCpAppHifiDacSamplingRateMsgT;

typedef enum 
{
   IPC_BLUETOOTH_DISCONNECT     = 0,
   IPC_BLUETOOTH_CONNECT        = 1

} IpcBluetoothConfigModeT;

typedef PACKED_PREFIX struct
{
   uint16 BluetoothCfgMode;    /* use IpcBluetoothConfigModeT */
} PACKED_POSTFIX  IpcDsvBluetoothConfigMsgT;

#define MAX_RESIDUAL_DATA_WORDS  16
typedef PACKED_PREFIX struct
{
   uint16   BitstreamFrameNumber;   /* Start from 1. Incremented by 1 when CP send frame bitstream  */
   uint16   BitstreamResSize;       /* BitstreamResSize < 17 */
   uint16   BitstreamResData[MAX_RESIDUAL_DATA_WORDS]; 
} PACKED_POSTFIX  IpcDsvBitstreamResMsgT;

typedef enum
{
   APP_MIXER_STOP   = 0,
   APP_MIXER_START  = 1
} IpcAppMixerModeT;

typedef PACKED_PREFIX struct
{
   uint16   AppMixerMode;       /* IpcAppMixerModeT */   
   uint16   AppChannelConfig;   /* use IpcDsvAppChConfigT   */
   uint16   AppSampleRate;      /* use IpcDsvAppSampleRateT */
} PACKED_POSTFIX  IpcDsvAppMixerCtrlMsgT;

/* PCM IF API related define */
typedef enum
{
   PCM_IF_API_OFF = 0,
   PCM_IF_API_ON  = 1
} IpcPcmIfApiModeT;

typedef PACKED_PREFIX struct
{
   uint16   PcmIfApiMode;            /* IpcPcmIfApiModeT */
} PACKED_POSTFIX  IpcDsvSetPcmIfApiModeMsgT;

typedef PACKED_PREFIX struct
{
   uint16   Data[PCM_DATA_FRAME_SIZE_WORDS]; 
} PACKED_POSTFIX  IpcSendCp2DspvPcmDataMsgT;

typedef PACKED_PREFIX struct
{
   uint16   Data[PCM_DATA_FRAME_SIZE_WORDS]; 
} PACKED_POSTFIX  IpcSendDspv2CpPcmDataMsgT;

typedef PACKED_PREFIX struct
{
   uint16   MixerDataFrameNum; 
   uint16   MixerDataFrameSize; 
} PACKED_POSTFIX  IpcSendDspv2CpMixerDataReqMsgT;

typedef PACKED_PREFIX struct
{
   uint16   AppId;       /* Use IpcDspAppIdT */
} PACKED_POSTFIX  IpcSendDspv2CpAppReqMsgT;

/* ABE API related define */
typedef enum
{
   ABE_OFF = 0,
   ABE_ON  = 1
} IpcAbeModeT;

typedef PACKED_PREFIX struct
{
   uint16   AbeMode;            /* IpcAbeModeT */
} PACKED_POSTFIX  IpcSendCp2DspvAbeModeMsgT;

/* DSPv -> CP Fast Mailbox definitions (only 1st and 4th words used)
 * List of commands for the CP to use to drive the operation of the FIQ.
 * The Command ID is written as the 1st word of the TX Fmbox HWD memory.
 * These must match the bit definitions for the DSPv to CP direction.
 */
#define CP2DV_FMB_CMDID_RESET     0x0000  /* Reset command ID at power up  */
#define CP2DV_FMB_APP_DATA_RDY    0x0001  /* Shared memory data is available */
#define CP2DV_FMB_MIDI_DATA_RDY   0x0002  /* Shared memory data is available */
#define CP2DV_FMB_AV_SYNC_ACK     0x0004  /* AV SYNC FIQ rcvd by CP    */
#define CP2DV_FMB_SBC_ENC_ACK     0x0008  /* SBC Encode FIQ rcvd by CP */
 
/* DSPv -> CP Fast Mailbox definitions (only 1st and 4th words used)
 *    1st word - FIQ interrupt command id (must match CP to DSPV definitions):
 */
#define DV2CP_FIQ_CMDID_RESET     0x0000  /* Reset command ID at power up  */
#define DV2CP_FIQ_CMDID_APP       0x0001  /* Multi-media App  */
#define DV2CP_FIQ_CMDID_MIDI      0x0002  /* MIDI FIQ */
#define DV2CP_FIQ_CMDID_AV_SYNC   0x0004  /* Audio-video sync */
#define DV2CP_FIQ_CMDID_SBC_ENC   0x0008  /* SBC encoder pcm data */

/*    4th word - DSPv Sleep or Busy */
#define DV2CP_FIQ_DSPV_SLEEP          0   /* DSPV in light sleep */
#define DV2CP_FIQ_DSPV_BUSY_IPCISR    1   /* DSPV busy processing IPC interrupt */
#define DV2CP_FIQ_DSPV_BUSY_CODECISR  2   /* DSPV busy processing CODEC ISR */
#define DV2CP_FIQ_DSPV_BUSY_DTSISR    3   /* DSPV busy processing DTS timer interrupt */


/*****************************************************************************
* End of File
*****************************************************************************/
#endif
/**Log information: \main\Trophy_SO73\1 2013-07-09 02:14:02 GMT jtzhang
** scbp#11737**/
/**Log information: \main\Trophy\1 2013-07-17 08:21:56 GMT jtzhang
** scbp#11737**/
