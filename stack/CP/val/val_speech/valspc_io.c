/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * spc_io.c
 *
 * Project:
 * --------
 * MAUI
 *
 * Description:
 * ------------
 * IO interface between AP and MD speech driver
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:   1.85  $
 * $Modtime:   Jul 27 2005 09:18:28  $
 * $Log:   //mtkvs01/vmdata/Maui_sw/archives/mcu/l1audio/l1sp.c-arc  $
 *
 * 10 21 2015 miyavi.tsai
 * [SIXTH00004987] Add 10ms delay for MD Speech first ccci message timing issue
 * add 10ms delay for MD first ccci message timing issue
 *
 * 07 23 2015 ys.hsieh
 * [SIXTH00003582] [JADE][MT6755][SPE] New SPE architecture porting
 * Unshelved from pending changelist '33721':
 * 	
 * 	re spe
 *
 * 07 17 2015 ys.hsieh
 * [SIXTH00003601] ?��??????,Modem assert
 * 	[Denali][VM] vm output buffer size large than MD to AP buffer size, blocking issue
 *
 * 07 13 2015 ys.hsieh
 * [SIXTH00003548] CCIF flight mode
 * CCIF flight mode for SRLET
 *
 * 06 17 2015 ys.hsieh
 * [SIXTH00003262] [Jade][Pre-BringUp] [C2K] [CCCI IT][MD3 Regression]4G??????speechlogmtc?????????EE(??crash)resetmodemNEEE(??crash)(1/5)
 * [Jade][Pre-BringUp] CCIF limit size 3456 byte
 *
 * 04 13 2015 ys.hsieh
 * [SIXTH00002536] [Denali-1][MT6735][Stage1][CSFB DSDS][Free test] When flight mode on / off UE happened "After AP send EPOF, MD didn't go to sleep in 4 seconds" exception
 * CBP c2k speech EPOF flight mode
 *
 * 09 29 2014 sheila.chen
 * [MOLY00078029] [K2] [HAC]
 * HAC
 *
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/
#if 0 

#include "kal_public_defs.h"
#include "kal_public_api.h"
#include "kal_general_types.h"
#include "kal_trace.h"

#include "ccci.h"

#include "drv_sap.h"
#include "audio_msgid.h"

#include "audio_def.h"
#include "audio_enum.h"
#include "l1sp_trc.h"
#include "l1audio.h"
#include "spc_io.h"
#include "spc_drv.h"
#endif 

#include "cpbuf.h"
#include "iopapi.h"
#include "sysapi.h"
#include "monapi.h"
#include "monids.h"

#include "hwdaudioservice.h"
#include "valspc_io.h"
#include "valaudio_enum.h"
#include "valapi.h"
#include "valspc_drv.h"
#include "valspherr.h"
#include "exeapi.h"
#include "valapi.h"

// TODO: c2k
// done [sharebuffer]
// done [spc]
// done [val self notify (mail box)]
// done [logging]
// [interrupt disable]
// [trace]
// [init skip]
// [ASSERT]

#define true         (bool)(1==1)
#define false        (bool)(1==0)


#if 0 // TODO: c2k temp
// #define __CCCI_VIA_SDIO__ i.e. __SDIO_DEVICE_CONNECTIVITY__
extern uint32 SaveAndSetIRQMask( void );
extern void   RestoreIRQMask( uint32 );
extern uint32 L1I_GetTimeStamp( void );
#endif // c2k temp

// ----------------------------------------------------------------------------
// Definition using in SPC IO
// ----------------------------------------------------------------------------
//#define __CCCI_OVER_SDIO_SUPPORT__


// #if defined(__SDIO_DEVICE_CONNECTIVITY__) || defined(__CCCI_OVER_SDIO_SUPPORT__)
#define SPCIO_CCCI_STREAM_BUF_RESERVED_SIZE 16 //unit is byte
#define SPCIO_SHARE_BUF_SIZE 16384
//#endif //__SDIO_DEVICE_CONNECTIVITY__

#define SPCIO_CCCI_MSG_QUEUE_SIZE 32

typedef enum {
    SPCIO_QUEUE_STATUS_INIT = 0,
    SPCIO_QUEUE_STATUS_READY,
    
} SPCIO_QUEUE_STATUS_T;

// ----------------------------------------------------------------------------
// Structure using in SPC IO
// ----------------------------------------------------------------------------
typedef struct _SPCIO_MESSAGE_T_
{
    SPC_MSG_ID_T message_id; // messaage using in AUDL
    uint32 message_data; // to put CCCI_data
    uint32 message_dataRev; // to put CCCI_Reserved
    
    uint32 putTime;
    uint32 getTime;
} SPCIO_MESSAGE_T;

typedef struct _SPCIO_T_
{
   RINGBUFFER_T(SPCIO_MESSAGE_T, SPCIO_CCCI_MSG_QUEUE_SIZE)   ccciq;

    //ilm_struct *pIlm; 

    //share buffer related
    uint32 apShareBufSize; // The size of AP share buffer, in size of Byte
   uint32 mdShareBufSize; // The size of MD share buffer, in size of Byte (MD to AP)
   uint32 apShareBufWrite;
    uint32 mdShareBufWrite;
   uint32 apShareBufRead;
    uint32 mdShareBufRead;
    uint8 *pApShareBuf;     // The base of AP share buffer
    uint8 *pMdShareBuf;     // The base of MD share buffer (MD to AP)
    uint8 *pMdHighPriBuf; 

    SPCIO_QUEUE_STATUS_T ccciqStatue; // to keep the queue status, make em_data_request is first process in the queue
    
   bool isTaskInitialize;
   bool isIopTaskInitialize;
} SPCIO_T;

// ----------------------------------------------------------------------------
// Global varialbe
// ----------------------------------------------------------------------------


SPCIO_T gSpcIo;
extern _SP_ENC SP_ENC; 


// done TODO: c2k [sharebuffer]
// [REMIND] Please use gSpcIo pointer intead of following buffer directly.
// __attribute__  ((section ("NONCACHEDZI"))) kal_uint8 gSpcIo_MdShareBuffer[SPCIO_SHARE_BUF_SIZE];
// __attribute__  ((section ("NONCACHEDZI"))) kal_uint8 gSpcIo_ApShareBuffer[SPCIO_SHARE_BUF_SIZE];
__attribute__((section ("NoCachedData"))) uint8 gSpcIo_MdShareBuffer[SPCIO_SHARE_BUF_SIZE];
__attribute__((section ("NoCachedData"))) uint8 gSpcIo_ApShareBuffer[SPCIO_SHARE_BUF_SIZE];

__attribute__((section ("NoCachedData"))) uint8 gSpcIo_MDHighPrioBuffer[SPCIO_SHARE_BUF_SIZE];


// end TODO: c2k [sharebuffer]
ExeSemaphoreT ENCSem;


// ----------------------------------------------------------------------------
// Logging
// ----------------------------------------------------------------------------

/**
    @prMsg: [input] pointer to structure SPCIO_MESSAGE_T. 
    @state: [input] logging state. 0 for receive, 1 for begin process, 2 for finish process
*/
static void _spcIO_MsgLog(SPCIO_MESSAGE_T *prMsg, uint16 state)
{
   
   //kal_uint32 msgTypeAndFunc = SPCIO_CCCI_MSG_ORG_TYPE_AND_FUNC(prMsg->message_data);
    
   ASSERT( prMsg != NULL, VALSPH_ERR_FORCE_ASSERT, 0);

    // AUDL thread msg logging
    if(prMsg->message_id != SPC_ID_AUDIO_A2M_CCCI) {
        /*
        if(state == 2) {
         if(!kal_if_hisr())
            kal_trace(TRACE_INFO, SPCIO_FINISH_MODEM_INTERNAL, prMsg->message_id, prMsg->message_id);
         else
            kal_dev_trace(TRACE_INFO, SPCIO_FINISH_MODEM_INTERNAL, prMsg->message_id, prMsg->message_id);
      } else if(state == 1) {
         if(!kal_if_hisr())
            kal_trace(TRACE_INFO, SPCIO_PROCESS_MODEM_INTERNAL, prMsg->message_id, prMsg->message_id);
         else
            kal_dev_trace(TRACE_INFO, SPCIO_PROCESS_MODEM_INTERNAL, prMsg->message_id, prMsg->message_id);
      } else if(state == 0) {
         if(!kal_if_hisr())
            kal_trace(TRACE_INFO, SPCIO_RECEIVE_MODEM_INTERNAL, prMsg->message_id, prMsg->message_id);
         else
            kal_dev_trace(TRACE_INFO, SPCIO_RECEIVE_MODEM_INTERNAL, prMsg->message_id, prMsg->message_id);
      }*/
      MonTrace(MON_CP_VAL_SPH_SPCIO_QUEUE_TRACE_ID, 5, state, prMsg->message_id, prMsg->message_id, 
      prMsg->message_data,
      prMsg->message_dataRev);
        return;
    }

    // CCCI message logging  ========================================
    // original version for MAUI branch (like MT6573, MT6575, MT6577)
#if 0 //def SPC_MSG_ORG_VER     
    if(!(SPCIO_CCCI_MSG_IS_EXTEND(prMsg->message_data))) {  
        AUD_CCCI_MSG_TYPE_T eMsgType;
       eMsgType = (AUD_CCCI_MSG_TYPE_T)(SPCIO_CCCI_MSG_ORG_TYPE(prMsg->message_data));
       
       switch (eMsgType)
       {
          case MSG_TYPE_VOLUME_CTRL:
          case MSG_TYPE_SET_DEVICE:
          case MSG_TYPE_SPEECH_CTRL:
          case MSG_TYPE_DATA_TRANSFER_CTRL:
          case MSG_TYPE_OTHER_API_HANDLER:
          case MSG_TYPE_ACK_FROM_AP:
          {
             kal_uint16 eFunc;
             eFunc = (SPCIO_CCCI_MSG_ORG_FUNC1(prMsg->message_data));
             //eFunc = (SPCIO_CCCI_MSG_ORG_FUNC(prMsg->message_data));
             
                if(state == 2)
             {
                if(!kal_if_hisr())
                   kal_trace(TRACE_INFO, SPCIO_ORG_FINISH_COMMAND, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eFunc, eFunc + eMsgType * 0x10);
                else
                   kal_dev_trace(TRACE_INFO, SPCIO_ORG_FINISH_COMMAND, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eFunc, eFunc + eMsgType * 0x10);
             } else if(state == 1)
             {
                if(!kal_if_hisr())
                   kal_trace(TRACE_INFO, SPCIO_ORG_PROCESS_COMMAND, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eFunc, eFunc + eMsgType * 0x10);
                else
                   kal_dev_trace(TRACE_INFO, SPCIO_ORG_PROCESS_COMMAND, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eFunc, eFunc + eMsgType * 0x10);
             } else if(state == 0)
             {
                if(!kal_if_hisr())
                   kal_trace(TRACE_INFO, SPCIO_ORG_RECEIVE_COMMAND, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eFunc, eFunc + eMsgType * 0x10);
                else
                   kal_dev_trace(TRACE_INFO, SPCIO_ORG_RECEIVE_COMMAND, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eFunc, eFunc + eMsgType * 0x10);
             }
             
             break;
          }
          
          case MSG_TYPE_NOTIFICATION:
            case MSG_TYPE_MISC_CTRL:
          {
             kal_uint16 eNotify;
             kal_uint32 offset, length;
             // offset   = (prMsg->message_data<<6)>>19;
             // length   = (prMsg->message_data<<19)>>19;
             offset = SPCIO_CCCI_MSG_ORG_PARAM2_OFFSET(prMsg->message_data);
                length = SPCIO_CCCI_MSG_ORG_PARAM2_LENGTH(prMsg->message_data);
             eNotify  =  SPCIO_CCCI_MSG_ORG_TYPE_AND_FUNC2(prMsg->message_data);
             //eNotify  =  SPCIO_CCCI_MSG_ORG_FUNC(prMsg->message_data);         
                
                if(state == 2)
             {
                if(!kal_if_hisr())
                   kal_trace(TRACE_INFO, SPCIO_ORG_FINISH_DATA_NOTIFY, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eNotify, eNotify, offset, length);
                else
                   kal_dev_trace(TRACE_INFO, SPCIO_ORG_FINISH_DATA_NOTIFY, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eNotify, eNotify, offset, length);
                } else if(state == 1)
             {
                if(!kal_if_hisr())
                   kal_trace(TRACE_INFO, SPCIO_ORG_PROCESS_DATA_NOTIFY, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eNotify, eNotify, offset, length);
                else
                   kal_dev_trace(TRACE_INFO, SPCIO_ORG_PROCESS_DATA_NOTIFY, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eNotify, eNotify, offset, length);
             } else if(state == 0)
             {
                if(!kal_if_hisr())
                   kal_trace(TRACE_INFO, SPCIO_ORG_RECEIVE_DATA_NOTIFY, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eNotify, eNotify, offset, length);
                else
                   kal_dev_trace(TRACE_INFO, SPCIO_ORG_RECEIVE_DATA_NOTIFY, prMsg->message_data, prMsg->message_id, prMsg->message_id, eMsgType, eMsgType, eNotify, eNotify, offset, length);
             }
          }
          
          break;
          
        default:
            break;
        }
        
        return;
     }
#else   
    // new version for MOLY branch (Chip like MT6280)
    {
    uint32 cmd = SPCIO_CCCI_MSG_CMD(prMsg->message_data);
    uint16 data16 = SPCIO_CCCI_MSG_DATA16(prMsg->message_data);

    MonTrace(MON_CP_VAL_SPH_CCCI_QUEUE_TRACE_ID, 8, 
        state, prMsg->message_data, 
        prMsg->message_id, prMsg->message_id, 
        cmd, cmd, 
        data16, prMsg->message_dataRev);
    /*
    if(state == 2)  {
       if(!kal_if_hisr())
          kal_trace(TRACE_INFO, SPCIO_FINISH_MSG, prMsg->message_data, prMsg->message_id, prMsg->message_id, cmd, cmd, data16, prMsg->message_dataRev);
       else
          kal_dev_trace(TRACE_INFO, SPCIO_FINISH_MSG, prMsg->message_data, prMsg->message_id, prMsg->message_id, cmd, cmd, data16, prMsg->message_dataRev);
    
    } else if(state == 1) {
        if(!kal_if_hisr())
          kal_trace(TRACE_INFO, SPCIO_PROCESS_MSG, prMsg->message_data, prMsg->message_id, prMsg->message_id, cmd, cmd, data16, prMsg->message_dataRev);
       else
          kal_dev_trace(TRACE_INFO, SPCIO_PROCESS_MSG, prMsg->message_data, prMsg->message_id, prMsg->message_id, cmd, cmd, data16, prMsg->message_dataRev);
    } else if(state == 0) {
       if(!kal_if_hisr())
          kal_trace(TRACE_INFO, SPCIO_RECEIVE_MSG, prMsg->message_data, prMsg->message_id, prMsg->message_id, cmd, cmd, data16, prMsg->message_dataRev);
       else
          kal_dev_trace(TRACE_INFO, SPCIO_RECEIVE_MSG, prMsg->message_data, prMsg->message_id, prMsg->message_id, cmd, cmd, data16, prMsg->message_dataRev);
    }
    */
    
    }
#endif //#ifdef SPC_MSG_ORG_VER 
}

// ----------------------------------------------------------------------------
// Software Queue Related
// ----------------------------------------------------------------------------

// extern ilm_struct *allocate_ilm(module_type module_id);

/**
    @prSmpMsg:
    @comeFrom: 0 for trigger from CCCI_HISR, 1 for trigger from AUDL. 2 for trigger from HISR(you can add more in the future)
*/
void SpcIO_MsgQueuePut(SPC_MSG_ID_T msgId, uint32 msgData, uint32 msgDataRev, uint8 callerFrom)
{    
    // uint32 u4SavedMask;
    uint32 isEmpty;
    SPCIO_MESSAGE_T queueMsg;

    queueMsg.message_id = msgId;
    queueMsg.message_data = msgData;
    queueMsg.message_dataRev = msgDataRev;
    
    // done TODO: c2k [logging]
    _spcIO_MsgLog(&queueMsg, 0);

    // Not Full
    if(gSpcIo.ccciq.write >= gSpcIo.ccciq.read) {
        ASSERT( (gSpcIo.ccciq.write - gSpcIo.ccciq.read) < SPCIO_CCCI_MSG_QUEUE_SIZE, VALSPH_ERR_FORCE_ASSERT, 0 );
    } else {
        ASSERT( (gSpcIo.ccciq.write + (0xffffffff - gSpcIo.ccciq.read) +1 ) < SPCIO_CCCI_MSG_QUEUE_SIZE, VALSPH_ERR_FORCE_ASSERT, 0 );
    }

    // disable inturrupt to prevent race condition, 
    // checking queue status and put data into sw queue
    SysIntDisable(SYS_IRQ_INT); //u4SavedMask = SaveAndSetIRQMask();

    isEmpty = RB_EMPTY(gSpcIo.ccciq);   
    gSpcIo.ccciq.queue[gSpcIo.ccciq.write & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].message_id = msgId;
    gSpcIo.ccciq.queue[gSpcIo.ccciq.write & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].message_data = msgData;
    gSpcIo.ccciq.queue[gSpcIo.ccciq.write & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].message_dataRev = msgDataRev;
    gSpcIo.ccciq.queue[gSpcIo.ccciq.write & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].putTime = SysTimeGetFine().MostSignificant32Bits;// L1I_GetTimeStamp();
    gSpcIo.ccciq.write++;

    SysIntEnable(SYS_IRQ_INT); // RestoreIRQMask(u4SavedMask);

   //prevent Modem side from processing A2M message before L1AudioMain is initialized, 
   //and just put A2M message into queue to wait the initilization of L1AudioMain      
    if(gSpcIo.isTaskInitialize != false){
        // sned a taskMsg to AUDL to process when sw queue is empty then begin process. 
        if(isEmpty) {
            //done  TODO: c2k [val self notify (mail box)]          
            ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_CCCI_QUEUE_PROC_MSG, NULL, 0);
            // msg_send4(MOD_L1SP, MOD_MED, AUDIO_SAP, MSG_ID_AUDIO_A2M_CCCI);
        }
    }
    
}



void spcIO_MsgQueueGet(SPCIO_MESSAGE_T * prSmpMsg)
{
    // uint32 u4SavedMask;

    ASSERT( gSpcIo.ccciq.read != gSpcIo.ccciq.write, VALSPH_ERR_FORCE_ASSERT, 0 );
    ASSERT( prSmpMsg != NULL, VALSPH_ERR_FORCE_ASSERT, 0);

    SysIntDisable(SYS_IRQ_INT); // u4SavedMask = SaveAndSetIRQMask();

    prSmpMsg->message_id = gSpcIo.ccciq.queue[gSpcIo.ccciq.read & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].message_id;
    prSmpMsg->message_data = gSpcIo.ccciq.queue[gSpcIo.ccciq.read & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].message_data;
    prSmpMsg->message_dataRev = gSpcIo.ccciq.queue[gSpcIo.ccciq.read & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].message_dataRev;
    gSpcIo.ccciq.queue[gSpcIo.ccciq.read & (SPCIO_CCCI_MSG_QUEUE_SIZE-1)].getTime = SysTimeGetFine().MostSignificant32Bits; // L1I_GetTimeStamp();
    gSpcIo.ccciq.read++;

    SysIntEnable(SYS_IRQ_INT); // RestoreIRQMask(u4SavedMask);

    // done TODO: c2k [logging]
    _spcIO_MsgLog(prSmpMsg, 1);  

}

// ----------------------------------------------------------------------------
// Function related to CCCI
// ----------------------------------------------------------------------------
typedef enum {
    SPCIO_CCCI_GET_STATE_IDLE,
    SPCIO_CCCI_GET_STATE_GETTING,
    SPCIO_CCCI_GET_STATE_DONE
}SPCIO_CCCI_GET_STATE_T;

#define SPCIO_CCCI_DATA_TEMP_STORE_BUF_SIZE 4096
typedef struct _SPCIO_CCCI_DATA_TEMP_STORE_T_
{
    SPCIO_CCCI_GET_STATE_T getCcciMsgStatus;
    uint32 ccciHeadWPos; // ccciHeader size is 4*4=16 byte
    SPCIO_CCCI_BUFF_T ccciHeadForData; 
    
    uint32 wPos;
    uint32 dataLen; 
    uint8  dataBuf[SPCIO_CCCI_DATA_TEMP_STORE_BUF_SIZE];
    
    bool isDataBufInUse;

} SPCIO_CCCI_DATA_TEMP_STORE_T_;
SPCIO_CCCI_DATA_TEMP_STORE_T_ gSpcIoCcciDataTemp; 



void SpcIO_A2M_hisr(SPCIO_CCCI_BUFF_T *bufp)
{

    uint32 ccciMsg;

    /* confirm the channel buffer type, and get the mailbox id */
    if (SPCIO_IS_MAIL_BOX(bufp)) {

        ccciMsg = SPCIO_MAILBOX_ID(bufp); // CCCI_MAILBOX_ID(bufp);

        SpcIO_MsgQueuePut(SPC_ID_AUDIO_A2M_CCCI, ccciMsg, (bufp->reserved), 0);
        
    } else {
    
        // if data send via ccci stream, the following checking needs to remove.
#if 0 // !(defined(__SDIO_DEVICE_CONNECTIVITY__) || defined(__CCCI_OVER_SDIO_SUPPORT__))
        ASSERT(0);
#else   // CCCI via SDIO
        // process the data then put to internal share buffer. 
        uint32 segment = 0;
        uint32 offset;
        
        uint32 dataLen;
        uint8 *data;
		uint16 cmd, msgData16;
	    uint16 msgData32; 

        // status check. Do NOT process before task init is done. 
        if(true != gSpcIo.isTaskInitialize) {
            return;
        }           
        
        data =  (void *)SPCIO_STREAM_ADDR(bufp); // instead use CCCI mesage 
        dataLen = SPCIO_STREAM_LEN(bufp);
        // TODO: c2k [trace]        
        // kal_trace(TRACE_INFO, SPCIO_A2M_HISR, 1, data, dataLen);

        /* REMIND: for origial CCCI API get restult usage 
        if(dataLen>16) {
            dataLen = (dataLen-16);
            data = ((kal_uint8 *)bufp)+16;
        } else {
            dataLen = 0;
            data = NULL;
        } 
        */ 
        
        //do memory copy, and send message to queue     
        // TODO: c2k [trace]
        // kal_trace(TRACE_INFO, SPCIO_A2M_HISR , 2, data, dataLen);
        
        offset = gSpcIo.apShareBufWrite;
        
        if(dataLen>gSpcIo.apShareBufSize){ // illegle
            ASSERT(0, VALSPH_ERR_FORCE_ASSERT, 0);
        } else {
            segment = gSpcIo.apShareBufSize - offset;
            if(segment>=dataLen){
                segment = dataLen;
                SysMemcpy( (gSpcIo.pApShareBuf)+offset, data, segment*sizeof(uint8));
                gSpcIo.apShareBufWrite += segment;

                if(gSpcIo.apShareBufWrite>=gSpcIo.apShareBufSize){
                    gSpcIo.apShareBufWrite = 0;
                }
            }else {
                SysMemcpy( (gSpcIo.pApShareBuf)+offset, data, segment*sizeof(uint8));
                SysMemcpy( gSpcIo.pApShareBuf,  data+segment, (dataLen-segment)*sizeof(uint8));
                gSpcIo.apShareBufWrite = dataLen-segment;
            }
            
#if 0 // def SPC_MSG_ORG_VER
            if(!SPCIO_CCCI_MSG_CMD_IS_EXTEND_M2A(bufp->reserved)){
                ccciMsg = SPCIO_CCCI_MSG_ORG_CONSTRUCT_DATA_WO_SHIFT(bufp->reserved, offset, dataLen);
                SpcIO_MsgQueuePut(SPC_ID_AUDIO_A2M_CCCI, ccciMsg, 0, 0);

                return;
            } else 
#endif // SPC_MSG_ORG_VER
            {
                ccciMsg = SPCIO_CCCI_MSG_CONSTRUCT_DATA_CMD_WO_SHIFT(bufp->reserved, dataLen);
				if(SP_ENC.Is_Voice_Encryption)
			    {
			  
			  		cmd = SPCIO_CCCI_MSG_CMD(ccciMsg);
	          		msgData16 = SPCIO_CCCI_MSG_DATA16(ccciMsg);
	          		msgData32 = offset;
			  		if(cmd == MSG_A2M_SPC_UL_ENC)
			  		{
                  		Spc_GetVoiceEncryptionHandler((kal_uint16)msgData32, msgData16);				
                  		return;
			  		}
					else if(cmd == MSG_A2M_SPC_DL_DEC)
					{
						Spc_GetVoiceDecryptionHandler((kal_uint16)msgData32, msgData16);
						return;
					}					
				}
                SpcIO_MsgQueuePut(SPC_ID_AUDIO_A2M_CCCI, ccciMsg, offset, 0);
            }

        }
        
        
#endif //__SDIO_DEVICE_CONNECTIVITY__ || defined(__CCCI_OVER_SDIO_SUPPORT__)) for CCCI API usage
    }

}

void SpcIo_ProcessIopSignal(void)
{
    IopDataCpBuffDescT dataCpBuffDesc;
    IopDataChRetStatus iopReadStatus;

    MonTrace(MON_CP_VAL_SPH_SPCIO_IOP_REVDATA_PROC_TRACE_ID, 7, -1, -1, 
        gSpcIoCcciDataTemp.getCcciMsgStatus, gSpcIoCcciDataTemp.ccciHeadWPos,
        gSpcIoCcciDataTemp.isDataBufInUse, gSpcIoCcciDataTemp.wPos, gSpcIoCcciDataTemp.dataLen);
    do {
        uint16 procLen=0;
        iopReadStatus = IopRead ( &dataCpBuffDesc ,IopDataChannelVoice, 0 );

        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, iopReadStatus, 77, 777, 7777);

        if((iopReadStatus != IopDataChRetOK) && (iopReadStatus != IopDataChRetOK_Empty)){
            MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPCIO_IO_DATA_ERROR, iopReadStatus, MON_CONTINUE);
            break;
        }
        
        MonTrace(MON_CP_VAL_SPH_SPCIO_IOP_REVDATA_PROC_TRACE_ID, 7, 
                procLen, dataCpBuffDesc.dataLen, 
                gSpcIoCcciDataTemp.getCcciMsgStatus, gSpcIoCcciDataTemp.ccciHeadWPos,
                gSpcIoCcciDataTemp.isDataBufInUse, gSpcIoCcciDataTemp.wPos, gSpcIoCcciDataTemp.dataLen);
                
        while(procLen<dataCpBuffDesc.dataLen) {
            
            uint16 leftDataLenInIopBuf;
            uint16 copyLenInByte; 

            MonTrace(MON_CP_VAL_SPH_SPCIO_IOP_REVDATA_PROC_TRACE_ID, 7, 
                procLen, dataCpBuffDesc.dataLen, 
                gSpcIoCcciDataTemp.getCcciMsgStatus, gSpcIoCcciDataTemp.ccciHeadWPos,
                gSpcIoCcciDataTemp.isDataBufInUse, gSpcIoCcciDataTemp.wPos, gSpcIoCcciDataTemp.dataLen);
            
            if(SPCIO_CCCI_GET_STATE_IDLE == gSpcIoCcciDataTemp.getCcciMsgStatus) {
                
                gSpcIoCcciDataTemp.ccciHeadWPos = 0;
                
                SysMemcpy(&(gSpcIoCcciDataTemp.ccciHeadForData), ((uint8 *)(dataCpBuffDesc.bufPtr->dataPtr))+procLen, 
                    sizeof(uint8));
                procLen += (sizeof(uint8));
                gSpcIoCcciDataTemp.ccciHeadWPos += (sizeof(uint8));
                gSpcIoCcciDataTemp.getCcciMsgStatus = SPCIO_CCCI_GET_STATE_GETTING;
                continue;
            } else if (SPCIO_CCCI_GET_STATE_GETTING == gSpcIoCcciDataTemp.getCcciMsgStatus) {
                SysMemcpy(((uint8 *)(&(gSpcIoCcciDataTemp.ccciHeadForData))+gSpcIoCcciDataTemp.ccciHeadWPos), 
                    ((uint8 *)(dataCpBuffDesc.bufPtr->dataPtr))+procLen, 
                    sizeof(uint8));
                procLen += (sizeof(uint8));
                gSpcIoCcciDataTemp.ccciHeadWPos+= (sizeof(uint8));
                
                if(gSpcIoCcciDataTemp.ccciHeadWPos == sizeof(SPCIO_CCCI_BUFF_T) ){                  
                    gSpcIoCcciDataTemp.getCcciMsgStatus = SPCIO_CCCI_GET_STATE_DONE;
                } else {
                    continue;
                }
            } /* else { // SPCIO_CCCI_GET_STATE_DONE == gSpcIoCcciDataTemp.getCcciMsgStatus
                // do nothing just get data. 
            } */


            // header process 
            if( false == gSpcIoCcciDataTemp.isDataBufInUse) { 

                /*
                SysMemcpy(&(gSpcIoCcciDataTemp.ccciHeadForData), 
                    ((uint8 *)(dataCpBuffDesc.bufPtr->dataPtr))+procLen, 
                    sizeof(SPCIO_CCCI_BUFF_T));
                procLen += (sizeof(SPCIO_CCCI_BUFF_T)); 
                */
    
                if(0xFFFFFFFF == gSpcIoCcciDataTemp.ccciHeadForData.data[0] ) { // mailbox //SPCIO_IS_MAIL_BOX(&ccciBuf))
                    // put to queue
                    SpcIO_A2M_hisr(&(gSpcIoCcciDataTemp.ccciHeadForData));
                    
                    gSpcIoCcciDataTemp.getCcciMsgStatus = SPCIO_CCCI_GET_STATE_IDLE;
                    gSpcIoCcciDataTemp.ccciHeadWPos = 0;
                    
                    continue;                   
                } else { // stream 
                    //  begin use temp buffer 
                    gSpcIoCcciDataTemp.wPos = 0;
                    gSpcIoCcciDataTemp.dataLen = gSpcIoCcciDataTemp.ccciHeadForData.data[1];
                    gSpcIoCcciDataTemp.isDataBufInUse = true;                                       
                    gSpcIoCcciDataTemp.ccciHeadForData.data[0] = (uint32)(&(gSpcIoCcciDataTemp.dataBuf));
                }
            } 

            // process IOP buffer left data
            leftDataLenInIopBuf = dataCpBuffDesc.dataLen-procLen;
			copyLenInByte = (leftDataLenInIopBuf >= (gSpcIoCcciDataTemp.dataLen-gSpcIoCcciDataTemp.wPos))? (gSpcIoCcciDataTemp.dataLen-gSpcIoCcciDataTemp.wPos): leftDataLenInIopBuf;
            
            SysMemcpy(gSpcIoCcciDataTemp.dataBuf + gSpcIoCcciDataTemp.wPos, 
                ((uint8 *)(dataCpBuffDesc.bufPtr->dataPtr))+procLen, 
                copyLenInByte); 
            procLen += (copyLenInByte);
            gSpcIoCcciDataTemp.wPos += (copyLenInByte);
            if (gSpcIoCcciDataTemp.wPos <= SPCIO_CCCI_DATA_TEMP_STORE_BUF_SIZE){
                
                ASSERT(gSpcIoCcciDataTemp.wPos <= SPCIO_CCCI_DATA_TEMP_STORE_BUF_SIZE, VALSPH_ERR_FORCE_ASSERT, 0);            
            } else {
                
            }
                
            // put to queue when stream data is all get. 
            if(gSpcIoCcciDataTemp.wPos == gSpcIoCcciDataTemp.dataLen ) {
                
                SpcIO_A2M_hisr(&(gSpcIoCcciDataTemp.ccciHeadForData));

                // reset 
                gSpcIoCcciDataTemp.isDataBufInUse = false;
                
                gSpcIoCcciDataTemp.getCcciMsgStatus = SPCIO_CCCI_GET_STATE_IDLE;
                gSpcIoCcciDataTemp.ccciHeadWPos = 0;
            } else if ( gSpcIoCcciDataTemp.wPos > gSpcIoCcciDataTemp.dataLen) {
                
                ASSERT(0, VALSPH_ERR_FORCE_ASSERT, 0); // should not be here. Meanwhile have bug! 
            }
        }
        
        CpBufFree(dataCpBuffDesc.bufPtr);


    } while((iopReadStatus != IopDataChRetErr) && (iopReadStatus != IopDataChRetOK_Empty));

    MonTrace(MON_CP_VAL_SPH_SPCIO_IOP_REVDATA_PROC_TRACE_ID, 7, -1, -2, 
        gSpcIoCcciDataTemp.getCcciMsgStatus, gSpcIoCcciDataTemp.ccciHeadWPos,
        gSpcIoCcciDataTemp.isDataBufInUse, gSpcIoCcciDataTemp.wPos, gSpcIoCcciDataTemp.dataLen);
}

/*
void SpcIO_M2A_hisr(SPCIO_CCCI_BUFF_T *bufp)
{
 
}
*/

int32 spcio_mailbox_write_with_reserved(uint32 channel, uint32 id,uint32 reserved)
{
    IopDataCpBuffDescT CpBuffDesc;
    CpBufferT* pCpBuf;
    IopDataChRetStatus wResult; 
    
    uint32 msg[4];

    MonTrace(MON_CP_VAL_SPH_SPCIO_MAILBOX_TRACE_ID, 5, 0, channel, id, reserved, -1);
    
    msg[0] = 0xffffffff, 
    msg[1] = id;
    msg[2] = channel; 
    msg[3] = reserved;

    pCpBuf = CpBufGet(4*sizeof(uint32), CPBUF_FWD);
    if(NULL == pCpBuf) {
        MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPCIO_CONNOT_GET_SEND_BUF, 0, MON_CONTINUE);
        return -1 ;
    }


    SysMemcpy ((uint8 *) pCpBuf->dataPtr,
               (uint8 *) msg,
            (uint16)  4*sizeof(uint32));

    // set CpBuffDesc 
    CpBuffDesc.bufPtr = pCpBuf; 
   CpBuffDesc.dataLen= 4*sizeof(uint32);
   CpBuffDesc.offset= 0;
   CpBuffDesc.nRLPFlow = 0 ;
   CpBuffDesc.streamType = 0;
   
   wResult = IopWrite(&CpBuffDesc, IopDataChannelVoice, 0);

    MonTrace(MON_CP_VAL_SPH_SPCIO_MAILBOX_TRACE_ID, 5, 0, pCpBuf->dataPtr[2], pCpBuf->dataPtr[1], pCpBuf->dataPtr[3], wResult);

    return wResult; 
    
}

#define SPCIO_CCCI_MAX_SEND_SIZE_IN_BYTE 3400 // 16kB for SDIO, 3400 for CCIF

int32 ccci_stream_write_with_reserved(	uint32 channel, uint32 addr, uint32 len, uint32 reserved)
{
	IopDataCpBuffDescT CpBuffDesc;
	CpBufferT* pCpBuf;	
	IopDataChRetStatus wResult; 

	uint32 msg[4];
	uint32 procLen = 0; 
	MonTrace(MON_CP_VAL_SPH_SPCIO_STREAM_TRACE_ID, 6, 0, channel, addr, len, reserved, -1);
#if 0	
	msg[0] = 0x0, 
	msg[1] = len;
	msg[2] = channel; 
    msg[3] = reserved;
        
    ASSERT((len+16)<SPCIO_CCCI_MAX_SEND_SIZE_IN_BYTE, VALSPH_ERR_SPCIO_STREAM_DATA_OVERFLOW, len);
    
    pCpBuf = CpBufGet(len+16, CPBUF_REV);
    if(NULL == pCpBuf) {
        MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPCIO_CONNOT_GET_SEND_BUF, 0, MON_CONTINUE);
        return -1 ;
    } 
    
    SysMemcpy ((uint8 *) pCpBuf->dataPtr,
               (uint8 *) msg,
            (uint16)  4*sizeof(uint32));
    SysMemcpy (((uint8 *) pCpBuf->dataPtr) + 16,
               (uint8 *) addr,
            (uint16)  len);
    
    // set CpBuffDesc 
    CpBuffDesc.bufPtr = pCpBuf;
    CpBuffDesc.dataLen= len+16; // ccci structure size is 16 bytes
    CpBuffDesc.offset= 0;
    CpBuffDesc.nRLPFlow = 0 ;
    CpBuffDesc.streamType = 0;
    
    wResult = IopWrite(&CpBuffDesc, IopDataChannelVoice, 0);
    
    MonTrace(MON_CP_VAL_SPH_SPCIO_STREAM_TRACE_ID, 6, 1, pCpBuf->dataPtr[2], pCpBuf, CpBuffDesc.dataLen, pCpBuf->dataPtr[3], wResult);  

	return wResult; 
#endif		
#if 1 // for future is we need to cut data to several times
	
	
	msg[0] = 0x0, 
	msg[1] = len;
	msg[2] = channel; 
    msg[3] = reserved;
    
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 6, 246246, msg[0], msg[1], msg[2], msg[3], 642642);
    
    pCpBuf = CpBufGet(SPCIO_CCCI_MAX_SEND_SIZE_IN_BYTE, CPBUF_REV);
    if(NULL == pCpBuf) {
        MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPCIO_CONNOT_GET_SEND_BUF, 0, MON_CONTINUE);
        return -1 ;
    } 
    ExeSemaphoreGet(&ENCSem, EXE_TIMEOUT_FALSE);
    SysMemcpy ((uint8 *) pCpBuf->dataPtr,
               (uint8 *) msg,
            (uint16)  4*sizeof(uint32));
    // set CpBuffDesc 
    CpBuffDesc.bufPtr = pCpBuf;
    CpBuffDesc.dataLen= 16; // ccci structure size is 16 bytes
    CpBuffDesc.offset= 0;
    CpBuffDesc.nRLPFlow = 0 ;
    CpBuffDesc.streamType = 0;

    wResult = IopWrite(&CpBuffDesc, IopDataChannelVoice, 0);

    while(procLen < len) {
        uint32 sendLen = len-procLen; 
        if (sendLen > SPCIO_CCCI_MAX_SEND_SIZE_IN_BYTE) {
            sendLen = SPCIO_CCCI_MAX_SEND_SIZE_IN_BYTE; 
		}

		pCpBuf = CpBufGet(SPCIO_CCCI_MAX_SEND_SIZE_IN_BYTE, CPBUF_REV);
		if(NULL == pCpBuf) {
			MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPCIO_CONNECT_GET_SEND_BUF, 0, MON_CONTINUE);
			return -1 ;
		} 
    MonTrace(MON_CP_VAL_SPH_SPCIO_STREAM_TRACE_ID, 6, 0, channel, addr, sendLen, reserved, 1);

		SysMemcpy ((uint8 *) pCpBuf->dataPtr,
	           		((uint8 *) addr)+procLen,
                    (uint16)  sendLen);
        // set CpBuffDesc 
        CpBuffDesc.bufPtr = pCpBuf;
        CpBuffDesc.dataLen= sendLen; // ccci structure size is 16 bytes
        CpBuffDesc.offset= 0;
        CpBuffDesc.nRLPFlow = 0 ;
        CpBuffDesc.streamType = 0;
        
        wResult = IopWrite(&CpBuffDesc, IopDataChannelVoice, 0);

        if(IopDataChRetOK == wResult) {
            procLen += sendLen; 
            
        } else {
            MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPCIO_SENDDATA_FAIL, 0, MON_CONTINUE);
        }
    }   
	ExeSemaphoreRelease(&ENCSem);
    return wResult; 
#endif  
}


// Mail box
bool spcIO_sendMsgViaCCCI(uint32 msg, uint32 revMsg)
{
    uint32 ret, i = 0;

    // TODO: c2k [trace]
   // kal_trace(TRACE_INFO, SPCIO_SEND_M2A_MESSAGE, msg, revMsg);

    SpcIO_MsgQueuePut(SPC_ID_AUDIO_M2A_CCCI, msg, revMsg, 1); 
    
    // ret = ccci_mailbox_write(CCCI_PCM_CHANNEL, msg);   
    ret = spcio_mailbox_write_with_reserved(SPCIO_PCM_CHANNEL_MSG, msg, revMsg);   

   while( ret != SPCIO_SUCCESS ){
        // TODO: c2k [trace]
      // kal_trace(TRACE_INFO, SPCIO_SEND_M2A_MESSAGE_FAIL, msg, ret);

      if(i == 3) {
            // TODO: c2k [trace]
            // kal_trace(TRACE_INFO, SPCIO_SEND_M2A_MESSAGE_FAIL_END, msg, ret);
            break;
      }
      i++;
      ExeTaskWait(1); // kal_sleep_task(1);

      ret = spcio_mailbox_write_with_reserved(SPCIO_PCM_CHANNEL_MSG, msg, revMsg);
   }

   return (bool)(ret == SPCIO_SUCCESS);
}


// Stream
/**
    @msg: CCCI message what to put in 
*/
bool spcIO_sendDataViaCCCI(uint32 msg, void *buf, uint32 bufLength)
{

    uint32 ret, i = 0;
    
   // TODO: c2k [trace]
   // kal_trace(TRACE_INFO, SPCIO_SEND_M2A_DATA, msg);

    SpcIO_MsgQueuePut(SPC_ID_AUDIO_M2A_CCCI, msg, bufLength, 1); 

    ret = ccci_stream_write_with_reserved(SPCIO_PCM_CHANNEL_DATA, (uint32)buf, bufLength, msg);
    
   while( ret != SPCIO_SUCCESS ){
      // TODO: c2k [trace]
       // kal_trace(TRACE_INFO, SPCIO_SEND_M2A_DATA_FAIL, msg, ret);

      if(i == 3) {
            // TODO: c2k [trace]
    // kal_trace(TRACE_INFO, SPCIO_SEND_M2A_DATA_FAIL_END, msg, ret);
            break;
      }
      i++;
      ExeTaskWait(1); // kal_sleep_task(1);
      ret = ccci_stream_write_with_reserved(SPCIO_PCM_CHANNEL_DATA,(uint32)buf, bufLength, msg);
   }

   return (bool)(ret == SPCIO_SUCCESS);

}

// ----------------------------------------------------------------------------
// Share Buffer(CCCI over CHIF) / Stream Temp Buffer(CCCI over SDIO) Related
// ----------------------------------------------------------------------------


AUD_CCCI_MSG_T spcIO_msgMapper(SPCIO_MSG_FROM_SPC spcIoMsg)
{
    AUD_CCCI_MSG_T ccciMsgCmd = 0; 
    switch(spcIoMsg){

        case SPCIO_MSG_FROM_SPC_SPH_ON_ACK: ccciMsgCmd = MSG_M2A_SPH_ON_ACK; break; 
        case SPCIO_MSG_FROM_SPC_SPH_OFF_ACK: ccciMsgCmd = MSG_M2A_SPH_OFF_ACK; break; 
        case SPCIO_MSG_FROM_SPC_SET_SPH_MODE_ACK: ccciMsgCmd = MSG_M2A_SET_SPH_MODE_ACK; break;
        case SPCIO_MSG_FROM_SPC_CTRL_SPH_ENH_ACK: ccciMsgCmd = MSG_M2A_CTRL_SPH_ENH_ACK; break;
        case SPCIO_MSG_FROM_SPC_CONFIG_SPH_ENH_ACK: ccciMsgCmd = MSG_M2A_CONFIG_SPH_ENH_ACK; break;     
        case SPCIO_MSG_FROM_SPC_SET_ACOUSTIC_LOOPBACK_ACK: ccciMsgCmd = MSG_M2A_SET_ACOUSTIC_LOOPBACK_ACK; break;
        /*
        // phase out from MT6589
        case SPCIO_MSG_FROM_SPC_QUERY_MD_CAPABILITY_ACK: ccciMsgCmd = MSG_M2A_QUERY_MD_CAPABILITY_ACK; break; 
        case SPCIO_MSG_FROM_SPC_SET_MD_CAPABILITY_ACK: ccciMsgCmd = MSG_M2A_SET_MD_CAPABILITY_ACK; break; 
        */
        case SPCIO_MSG_FROM_SPC_PRINT_SPH_COEFF_ACK: ccciMsgCmd = MSG_M2A_PRINT_SPH_COEFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_SPH_ON_FOR_HOLD_CALL_ACK: ccciMsgCmd = MSG_M2A_SPH_ON_FOR_HOLD_CALL_ACK; break;
        case SPCIO_MSG_FROM_SPC_SPH_ON_FOR_DACA_ACK: ccciMsgCmd = MSG_M2A_SPH_ON_FOR_DACA_ACK; break;
        case SPCIO_MSG_FROM_SPC_SPH_ROUTER_ON_ACK: ccciMsgCmd = MSG_M2A_SPH_ROUTER_ON_ACK; break;
		case SPCIO_MSG_FROM_SPC_SET_VOICE_ENCRYPTION_ACK: ccciMsgCmd = MSG_M2A_SET_VOICE_ENCRYPTION_ACK; break;
            
        case SPCIO_MSG_FROM_SPC_PNW_ON_ACK: ccciMsgCmd = MSG_M2A_PNW_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_PNW_OFF_ACK: ccciMsgCmd = MSG_M2A_PNW_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_RECORD_ON_ACK: ccciMsgCmd = MSG_M2A_RECORD_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_RECORD_OFF_ACK: ccciMsgCmd = MSG_M2A_RECORD_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_DMNR_RECPLAY_ON_ACK: ccciMsgCmd = MSG_M2A_DMNR_RECPLAY_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_DMNR_RECPLAY_OFF_ACK: ccciMsgCmd = MSG_M2A_DMNR_RECPLAY_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_DMNR_REC_ONLY_ON_ACK: ccciMsgCmd = MSG_M2A_DMNR_REC_ONLY_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_DMNR_REC_ONLY_OFF_ACK: ccciMsgCmd = MSG_M2A_DMNR_REC_ONLY_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_PCM_REC_ON_ACK: ccciMsgCmd = MSG_M2A_PCM_REC_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_PCM_REC_OFF_ACK: ccciMsgCmd = MSG_M2A_PCM_REC_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_VM_REC_ON_ACK: ccciMsgCmd = MSG_M2A_VM_REC_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_VM_REC_OFF_ACK: ccciMsgCmd = MSG_M2A_VM_REC_OFF_ACK; break;

        case SPCIO_MSG_FROM_SPC_RECORD_RAW_PCM_ON_ACK: ccciMsgCmd = MSG_M2A_RECORD_RAW_PCM_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_RECORD_RAW_PCM_OFF_ACK: ccciMsgCmd = MSG_M2A_RECORD_RAW_PCM_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_CTM_ON_ACK: ccciMsgCmd = MSG_M2A_CTM_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_CTM_OFF_ACK: ccciMsgCmd = MSG_M2A_CTM_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_CTM_DUMP_DEBUG_FILE_ACK: ccciMsgCmd = MSG_M2A_CTM_DUMP_DEBUG_FILE_ACK; break;
        case SPCIO_MSG_FROM_SPC_BGSND_ON_ACK: ccciMsgCmd = MSG_M2A_BGSND_ON_ACK; break;
        case SPCIO_MSG_FROM_SPC_BGSND_OFF_ACK: ccciMsgCmd = MSG_M2A_BGSND_OFF_ACK; break;
        case SPCIO_MSG_FROM_SPC_BGSND_CONFIG_ACK: ccciMsgCmd = MSG_M2A_BGSND_CONFIG_ACK; break;


        case SPCIO_MSG_FROM_SPC_PNW_DLDATA_REQUEST: ccciMsgCmd = MSG_M2A_PNW_DL_DATA_REQUEST; break;
        case SPCIO_MSG_FROM_SPC_BGS_DATA_REQUEST: ccciMsgCmd = MSG_M2A_BGSND_DATA_REQUEST; break;
        case SPCIO_MSG_FROM_SPC_CTM_DATA_REQUEST: ccciMsgCmd = MSG_M2A_CTM_DATA_REQUEST; break;
        case SPCIO_MSG_FROM_SPC_DACA_ULDATA_REQUEST: ccciMsgCmd = MSG_M2A_DACA_UL_DATA_REQUEST; break;
		case SPCIO_MSG_FROM_SPC_MSD_DATA_ACK: ccciMsgCmd = MSG_M2A_ECALL_MSD_ACK; break;
		case SPCIO_MSG_FROM_SPC_UL_ENC_REQUEST: ccciMsgCmd = MSG_M2A_ENC_UL_DATA_REQUEST; break;
		case SPCIO_MSG_FROM_SPC_DL_DEC_REQUEST: ccciMsgCmd = MSG_M2A_DEC_DL_DATA_REQUEST; break;

        case SPCIO_MSG_FROM_SPC_PNW_ULDATA_NOTIFY: ccciMsgCmd = MSG_M2A_PNW_UL_DATA_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_REC_DATA_NOTIFY: ccciMsgCmd = MSG_M2A_REC_DATA_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_CTM_DEBUG_DATA_NOTIFY: ccciMsgCmd = MSG_M2A_CTM_DEBUG_DATA_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_PCM_REC_DATA_NOTIFY: ccciMsgCmd = MSG_M2A_PCM_REC_DATA_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_VM_REC_DATA_NOTIFY: ccciMsgCmd = MSG_M2A_VM_REC_DATA_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_DACA_DLDATA_NOTIFY: ccciMsgCmd = MSG_A2M_DACA_DL_DATA_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_RAW_PCM_REC_DATA_NOTIFY: ccciMsgCmd = MSG_M2A_RAW_PCM_REC_DATA_NOTIFY; break;
		case SPCIO_MSG_FROM_SPC_CUST_DUMP_NOTIFY: ccciMsgCmd = MSG_M2A_CUST_DUMP_NOTIFY; break;

        case SPCIO_MSG_FROM_SPC_EM_DATA_REQUEST: ccciMsgCmd = MSG_M2A_EM_DATA_REQUEST; break;
        case SPCIO_MSG_FROM_SPC_EM_INCALL_ACK: ccciMsgCmd = MSG_M2A_EM_INCALL_ACK; break;
        case SPCIO_MSG_FROM_SPC_EM_DMNR_ACK: ccciMsgCmd = MSG_M2A_EM_DMNR_ACK; break;
        case SPCIO_MSG_FROM_SPC_EM_WB_ACK: ccciMsgCmd = MSG_M2A_EM_WB_ACK; break;
        case SPCIO_MSG_FROM_SPC_EM_MAGICON_ACK: ccciMsgCmd = MSG_M2A_EM_MAGICON_ACK; break;     
        case SPCIO_MSG_FROM_SPC_EM_HAC_ACK: ccciMsgCmd = MSG_M2A_EM_HAC_ACK; break;             
        case SPCIO_MSG_FROM_SPC_NETWORK_STATUS_NOTIFY:  ccciMsgCmd = MSG_M2A_NETWORK_STATUS_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_RF_INFO_NOTIFY:  ccciMsgCmd = MSG_M2A_RF_INFO_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_EPOF_NOTIFY:  ccciMsgCmd = MSG_M2A_EPOF_NOTIFY; break;
        case SPCIO_MSG_FROM_SPC_DYNAMIC_PAR_ACK: ccciMsgCmd = MSG_M2A_DYNAMIC_Par; break; 

        case SPCIO_MSG_FROM_SPC_VIBSPK_PARAMETER_ACK: ccciMsgCmd = MSG_M2A_VIBSPK_PARAMETER_ACK; break;
        case SPCIO_MSG_FROM_SPC_NXP_SMARTPA_PARAMETER_ACK: ccciMsgCmd = MSG_M2A_NXP_SMARTPA_PARAMETER_ACK; break;

		case SPCIO_MSG_FROM_SPC_NW_CODEC_INFO_NOTIFY: ccciMsgCmd = MSG_M2A_NW_CODEC_INFO_NOTIFY; break;
		
    default: 
            ASSERT(0, VALSPH_ERR_FORCE_ASSERT, 0);  

    }
    return ccciMsgCmd;
}



uint16 SpcIo_GetDataFromAp(const uint16 offset, const uint16 length, void *buf)
{
    uint16 procLen;
    uint16 curOffset;
    
    curOffset = offset;
    procLen = length;
    if((curOffset + procLen) > gSpcIo.apShareBufSize) {
        uint32 segment = gSpcIo.apShareBufSize - curOffset;
        uint8 *pCurrent = buf;
        SysMemcpy(pCurrent, gSpcIo.pApShareBuf+curOffset, sizeof(uint8)*segment);
        pCurrent += segment;
        
        segment = procLen - segment;
        SysMemcpy(pCurrent, gSpcIo.pApShareBuf, sizeof(uint8)*segment);

        // move offset pointer
        curOffset = segment;
    } else {
        SysMemcpy(buf, gSpcIo.pApShareBuf+curOffset, sizeof(uint8)*procLen);

        // move offset pointer
        curOffset = offset + procLen ;
    }

    // move read pointer
    //gSpcIo.apShareBufRead = curOffset;

    // provide current position
    return curOffset;
        
}

/**
    @offset: [Input] 
    @length: [Input] unit int byte
    @headerLen: [Input] unit int byte

    @header: [Output]
    @buf: [Output]
    
*/
void SpcIO_GetDataFromAp_inOneTime(uint16 offset, uint16 length, int16 headerLen,
    void *header, void *buf)
{
    int16 procLen;
    uint16 curOffset;

    // length checking: total processing length should be longer then header
    // TODO: c2k [trace]
    // kal_trace(TRACE_INFO, SPCIO_GETDATAFROMAP_INONETIME_1, length, headerLen);   
    // TODO: c2k [ASSERT] ASSERT(length >= headerLen);  
    
    //header
    curOffset = SpcIo_GetDataFromAp(offset, headerLen, header);

    // TODO: c2k [trace]
    // kal_trace(TRACE_INFO, SPCIO_GETDATAFROMAP_INONETIME_2, *((kal_uint16 *)header), *((kal_uint16 *)header+1), *((kal_uint16 *)header+2));
        
    //data
    procLen = length - headerLen;
    if(procLen >0) { // prevent empty data
        SpcIo_GetDataFromAp(curOffset, procLen, buf);
    } else {
        buf = NULL;
    }
}

void spcIO_copyDataToMdShareBuf(const uint8 *pSrc, const int16 srcLen)
{
    uint8 *pSrcCur; 
    uint16 curOffset = gSpcIo.mdShareBufWrite;
    
    pSrcCur = (uint8 *)pSrc;
    
    if((curOffset + srcLen) > gSpcIo.mdShareBufSize) {
        // cut to segment 
        int16 segment = gSpcIo.mdShareBufSize -  curOffset;

        SysMemcpy( (uint8 *)(gSpcIo.pMdShareBuf + curOffset), pSrcCur, sizeof(uint8)*segment);
        pSrcCur = pSrcCur + segment; 

        segment = srcLen - segment;
        SysMemcpy(gSpcIo.pMdShareBuf, pSrcCur, sizeof(uint8)*segment);

        // move offset pointer
        curOffset = segment;
    } else {
        SysMemcpy((uint8 *)(gSpcIo.pMdShareBuf+curOffset), pSrcCur, sizeof(uint8)*srcLen);

        // move offset pointer
        curOffset = curOffset + srcLen ;
    }

    gSpcIo.mdShareBufWrite = curOffset;

    }
/**
    if the result is fail, log will leave inside
    @headerBuf: 
    @headerLen:
    @srcBuf:
    @srcLen:
    @msg:

    @return: successful send or not! 
*/


#define MAX_CCCI_BUFFER      (3000)

#define MAX_CCCI_AVAILABLE_BUF (MAX_CCCI_BUFFER - sizeof(spcExtendedBufInfo) - SPCIO_CCCI_STREAM_BUF_RESERVED_SIZE)


bool SpcIO_WriteDataToAp(void *headerBuf, int16 headerLen, 
    void *srcBuf1, int16 srcLen1, void *srcBuf2, int16 srcLen2, 
    SPCIO_MSG_FROM_SPC spcIoMsg)
{
    uint16 length;
    
    bool sendResult = false;
    AUD_CCCI_MSG_T msg;



#if 1 // defined(__SDIO_DEVICE_CONNECTIVITY__)  
    uint8 *sendData;
    uint16 ccciStreamSize; 

    if(spcIoMsg == SPCIO_MSG_FROM_SPC_UL_ENC_REQUEST || spcIoMsg == SPCIO_MSG_FROM_SPC_DL_DEC_REQUEST )	
    {
       sendData = gSpcIo.pMdHighPriBuf;
    }
    else
    {
       sendData = gSpcIo.pMdShareBuf;
    }
    length = headerLen + srcLen1 + srcLen2; 
    ccciStreamSize = length; // + SPCIO_CCCI_STREAM_BUF_RESERVED_SIZE;
	ASSERT(ccciStreamSize<SPCIO_SHARE_BUF_SIZE, VALSPH_ERR_FORCE_ASSERT, 0);	   
    
    SysMemcpy((uint8 *)(sendData) , headerBuf, headerLen);
    SysMemcpy((uint8 *)(sendData+ headerLen), srcBuf1, srcLen1);
    if(0 != srcLen2) {
        SysMemcpy((uint8 *)(sendData+ srcLen1+ headerLen), srcBuf2, srcLen2);
    }
        
    // mapper 
    msg = spcIO_msgMapper(spcIoMsg);
        
    //send via CCCI stream
    {   
        sendResult = spcIO_sendDataViaCCCI(SPCIO_CCCI_MSG_CONSTRCUT_DATA_CMD(msg, length), 
            sendData, ccciStreamSize);
    }
    
#elif defined(__CCCI_OVER_SDIO_SUPPORT__) // CCCI over SDIO has the limitation of maximun 5k throughput. Oversized data are sent consecutively by multiple times. By the modified data header, AP can indicate which consecutively data belong to data which should be carried by one time.

    kal_uint8 *sendData, *buf1, *buf2;
    kal_uint16 ccciStreamSize; 
    kal_uint16 capacity, t1, t2;    
    kal_int16 r_srcLen1 = srcLen1, r_srcLen2 = srcLen2; //remaining

    spcExtendedBufInfo einfo;
        
   sendData = gSpcIo.pMdShareBuf;

   if(0 < headerLen){
      einfo.syncWord = 0x1234;
      einfo.type     = ((spcBufInfo *)headerBuf)->type;//info.type;
      einfo.length   = 0;
      einfo.curIdx   = 0;
      einfo.totalIdx = (srcLen1 + srcLen2+(MAX_CCCI_AVAILABLE_BUF-1))/MAX_CCCI_AVAILABLE_BUF;
      headerLen     = sizeof(spcExtendedBufInfo);
      kal_trace(TRACE_INFO, SPCIO_SDIO_DEBUG1, 1, einfo.totalIdx, srcLen1, srcLen2);
   }else{ // without padding any header, just to check size < MAX_CCCI_AVAILABLE_BUF, which means curIdx equals 1.
      einfo.curIdx   = 0;
      einfo.totalIdx = 1;      
      kal_trace(TRACE_INFO, SPCIO_SDIO_DEBUG1, 2, einfo.totalIdx, srcLen1, srcLen2);
   }    
   
   kal_trace(TRACE_INFO, SPCIO_SDIO_WRITE2AP_ENTER);
   while(0 < r_srcLen1 + r_srcLen2){
      kal_trace(TRACE_INFO, SPCIO_SDIO_WRITE2AP_INFO1, r_srcLen1, r_srcLen2, einfo.curIdx);
        
      buf1 = (kal_uint8 *)srcBuf1+(srcLen1-r_srcLen1);
      buf2 = (kal_uint8 *)srcBuf2+(srcLen2-r_srcLen2); 
      capacity = MAX_CCCI_AVAILABLE_BUF;       
      
      t1 = r_srcLen1;
      if(r_srcLen1 > capacity){
         t1 = capacity;
      }    
      //copy buf1 with t1 size, then consume capacity with t1 size     
      r_srcLen1  -= t1;
      capacity -= t1;
       
      t2 = r_srcLen2;
      if(r_srcLen2 > capacity){
         t2 = capacity;
      }    
      //copy buf1 with t2 size, then consume capacity with t2 size     
      r_srcLen2  -= t2;
      capacity   -= t2;
      
      //assemble the content of buffer
      
      if(0 < headerLen){
         einfo.curIdx++;           
         einfo.length = t1 + t2;        
         kal_mem_cpy((kal_uint8 *)(sendData + SPCIO_CCCI_STREAM_BUF_RESERVED_SIZE ), &einfo, headerLen);
      }else{
         einfo.curIdx++;    
         ASSERT(einfo.curIdx==1); //Data with no header happens only when PCM4WAY debug log in CTM, which is expected not to exceed the throughput limitation of CCCI over SDIO
      }
      
      kal_mem_cpy((kal_uint8 *)(sendData + SPCIO_CCCI_STREAM_BUF_RESERVED_SIZE + headerLen)      , buf1, t1);
      kal_mem_cpy((kal_uint8 *)(sendData + SPCIO_CCCI_STREAM_BUF_RESERVED_SIZE + headerLen + t1) , buf2, t2);      
      
      length = headerLen + t1 + t2; 
      kal_trace(TRACE_INFO, SPCIO_SDIO_WRITE2AP_INFO2, headerLen, t1, t2, length);

      ccciStreamSize = length + SPCIO_CCCI_STREAM_BUF_RESERVED_SIZE;        
      ASSERT(ccciStreamSize <= MAX_CCCI_BUFFER);
      
      // mapper 
      msg = spcIO_msgMapper(spcIoMsg);
        
      //send via CCCI stream
   #ifdef SPC_MSG_ORG_VER   
      if(!SPCIO_CCCI_MSG_CMD_IS_EXTEND_M2A(msg)){
        sendResult = spcIO_sendDataViaCCCI(SPCIO_CCCI_MSG_ORG_CONSTRUCT_DATA(msg, 0, length),
            sendData, ccciStreamSize);
        
      } else 
   #endif // SPC_MSG_ORG_VER    
      { 
        sendResult = spcIO_sendDataViaCCCI(SPCIO_CCCI_MSG_CONSTRCUT_DATA_CMD(msg, length), 
            sendData, ccciStreamSize);
      }     
   }
   kal_trace(TRACE_INFO, SPCIO_SDIO_WRITE2AP_LEAVE);

#else //via CHIF
    kal_uint16 offset;
    //keep starting address before put data into share buffer
    length = headerLen + srcLen1 + srcLen2;
    offset = gSpcIo.mdShareBufWrite;
    
    // put to share buffer
    if((0 != headerLen) && (NULL != headerBuf)) {
        spcIO_copyDataToMdShareBuf(headerBuf, headerLen);
    }
    spcIO_copyDataToMdShareBuf(srcBuf1, srcLen1);   
    if(0 != srcLen2) {
        spcIO_copyDataToMdShareBuf(srcBuf2, srcLen2);   
    }
    
    // mapper 
    msg = spcIO_msgMapper(spcIoMsg);
        
    // send data notification message
#ifdef SPC_MSG_ORG_VER      
    if(!SPCIO_CCCI_MSG_CMD_IS_EXTEND_M2A(msg)){
        sendResult = spcIO_sendMsgViaCCCI(SPCIO_CCCI_MSG_ORG_CONSTRUCT_DATA(msg, offset, length), 0);
    } else 
#endif //SPC_MSG_ORG_VER
    {
        sendResult = spcIO_sendMsgViaCCCI(SPCIO_CCCI_MSG_CONSTRCUT_DATA_CMD(msg, length), offset);
    }

#endif // __SDIO_DEVICE_CONNECTIVITY__

    return sendResult;

}



bool SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC spcIoMsg, uint32 data1, uint32 reserve)
{
    uint32 msgContent;
    AUD_CCCI_MSG_T ccciMsgCmd; 

    // mapper 
    ccciMsgCmd = spcIO_msgMapper(spcIoMsg);
    

    msgContent = SPCIO_CCCI_MSG_CONSTRCUT_CMD(ccciMsgCmd, data1);
    return spcIO_sendMsgViaCCCI(msgContent, reserve);

}

// ----------------------------------------------------------------------------
// AUDL running functions Related
// ----------------------------------------------------------------------------


// void SpcIO_Msg_Handler_inAudL(ilm_struct *ilm_ptr)
void SpcIO_Msg_Handler_inAudL(void)
{

        while (!RB_EMPTY(gSpcIo.ccciq)) 
    {
        SPCIO_MESSAGE_T msg; 
        
        spcIO_MsgQueueGet(&msg);
        if(0 != get_spcGetEpofTimes())
        {
            if(SPC_ID_AUDIO_A2M_CCCI == msg.message_id)
            {
                uint16 cmd = SPCIO_CCCI_MSG_CMD(msg.message_data);
                if(MSG_A2M_EPOF_ACK == cmd)
                {
                    //Notify L4C: speech driver enter EPOF done
                    ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_EPOF_DONE_MSG, NULL, 0);
                    //kal_prompt_trace(MOD_L1SP, "SPC [EPOF]SpcIO_Msg_Handler_inAudL notify L4C done");
                    MonTrace(MON_CP_VAL_SPH_L4C_EPOF_TRACE_ID, 1,1);
                    _spcIO_MsgLog(&msg, 2);
                    continue;
                }
                else
                {
                    //When enter EPOF, ignore all command
                    //kal_prompt_trace(MOD_L1SP, "SPC [EPOF]SpcIO_Msg_Handler_inAudL drop SPC_ID_AUDIO_A2M_CCCI cmd=%x", cmd);
                    MonTrace(MON_CP_VAL_SPH_AP_EPOF_TRACE_ID, 1,0);
                    continue;
                }
            }
            else
            {
                //When enter EPOF, ignore all command
                //kal_prompt_trace(MOD_L1SP, "SPC [EPOF]SpcIO_Msg_Handler_inAudL drop message=%x", msg.message_id);
                MonTrace(MON_CP_VAL_SPH_AP_EPOF_TRACE_ID, 1,1);
                continue;
            }
        }
        
            switch (msg.message_id)
          {
                case SPC_ID_AUDIO_A2M_CCCI:
                    if(SPCIO_QUEUE_STATUS_READY == gSpcIo.ccciqStatue) {
                        
                        spc_A2M_MsgHandler(msg.message_data, msg.message_dataRev);
                        
                    } else {
                        // leave a log
                        // TODO: c2k [trace]
                        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 5555, 78, 5555);
                        // kal_trace(TRACE_INFO, SPCIO_INVALID_MSG_BEFORE_QUEUE_READY, msg.message_id, msg.message_data, msg.message_dataRev);
                    }
                    break;              
            
                case SPC_ID_AUDIO_M2A_CCCI:
                    //directly send when call spcIO_sendMsgViaCCCI() or spcIO_sendDataViaCCCI()
                    break;
             
                case SPC_ID_AUDIO_STRM_PNW_UL_DATA_NOTIFY: 
                    //handle the data to pcmNway data to AP                 
#if defined(__ENABLE_SPEECH_DVT__)
                    spc_pcmNWay_sendUlData(msg.message_data);                   
#else // defined(__ENABLE_SPEECH_DVT__)
                    spc_pcmNWay_sendUlData();                   
#endif // defined(__ENABLE_SPEECH_DVT__)
                    break;

                case SPC_ID_AUDIO_STRM_PNW_DL_DATA_REQUEST:
#if defined(__ENABLE_SPEECH_DVT__)
                    spc_pcmNWay_requestDlData(msg.message_data);
#else // defined(__ENABLE_SPEECH_DVT__)
                    spc_pcmNWay_requestDlData();
#endif // defined(__ENABLE_SPEECH_DVT__)
                    break;
          
                case SPC_ID_AUDIO_STRM_BGSND_DATA_REQUEST:
                    spc_bgSnd_requestData();
                break;
        
               case SPC_ID_SPEECH_CUSTOM_DATA_REQUEST:
                    // Boot UP to request EM data                   
                SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_EM_DATA_REQUEST, 0, 0);
                    gSpcIo.ccciqStatue = SPCIO_QUEUE_STATUS_READY;                
                    break;

                case SPC_ID_AUDIO_STRM_PCMREC_DATA_NOTIFY: //pcm record
                    spc_record_sendPcmData();
                    break;

                case SPC_ID_AUDIO_STRM_RAWPCMREC_DATA_NOTIFY: // raw pcm record
                    spc_record_sendRawPcmData();
                    break;
                    
                case SPC_ID_AUDIO_STRM_VMREC_DATA_NOTIFY: // vm record
                    spc_record_sendVmData();
                    break;
#if 0 // temp for c2k                   
#ifdef __CTM_SUPPORT__  
                case SPC_ID_AUDIO_STRM_CTM_DUMP_DATA_NOTIFY:
                    spc_ctm_sendDumpDebugData();
                    break;
#endif

                case SPC_ID_AUDIO_STRM_DACA_DL_DATA_NOTIFY:
                    spc_daca_sendDlData();
                    break;

                case SPC_ID_AUDIO_STRM_DACA_UL_DATA_REQUEST:
                    spc_daca_requestUlData();                   
                    break;
#endif // c2k temp  
                case SPC_ID_AUDIO_CONTROL_BGSND_CLOSE: // close background sound and send back the message
                    spc_BgSndClose();
                    break;
#if 0 // temp for c2k                   
                case SPC_ID_NETWORK_STATUS_NOTIFY: 
                    spc_notify_network_status();
                    break;                      
#endif // c2k temp          
                default:
                    // done TODO: c2k [fault message]
                    MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPCIO_UNKNOWN_CMD, msg.message_id, MON_CONTINUE);
                // kal_trace(TRACE_INFO, SPCIO_INVALID_MSG, msg.message_id, msg.message_data, msg.message_dataRev);
          }

            // done TODO: c2k [logging]
            _spcIO_MsgLog(&msg, 2); 
        }

    
}

void SpcIO_CheckSpeechInitDone(void) // Only Full load need to check both HwdSph & ValSph are ini. 
{
#ifdef VALSPC_CHIP_BACK_PHONECALL_USE // org def SPH_CHIP_BACK_PHONECALL_USE
        // do not send notify to ap     
        gSpcIo.ccciqStatue = SPCIO_QUEUE_STATUS_READY;
#else 

#ifdef SYS_OPTION_IOP_CCIF
    if(true == gSpcIo.isIopTaskInitialize) {
#endif
        //final step to notify AP 
        if( true == L1Audio_IsTaskInitilized() && true == gSpcIo.isTaskInitialize   ) {     
			ExeTaskWait(ExeCalMsec(10)); // For MD Speech frst meg and MD CCCI stats=ready timing issue
            SpcIO_MsgQueuePut(SPC_ID_SPEECH_CUSTOM_DATA_REQUEST, 0, 0, 0);
            ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_CCCI_QUEUE_PROC_MSG, NULL, 0);
        }
#ifdef SYS_OPTION_IOP_CCIF
        else {
            // leave a log
            // TODO: c2k [trace]
            MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 5339, 78, 5339);
            // kal_trace(TRACE_INFO, SPCIO_INVALID_MSG_BEFORE_QUEUE_READY, msg.message_id, msg.message_data, msg.message_dataRev);
        }
    }  
#endif   
    
#endif      
}

#ifdef SYS_OPTION_IOP_CCIF
void SpcIO_CCCIInitDone(void)
{
    gSpcIo.isIopTaskInitialize = true;
    ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_CCCIPART_INIT_DONE_MSG, NULL, 0);
}
#endif
// ============================================================================
/**
    This function only used in full load. 
*/
void SpcIO_Init(void)
{
    // do initail when AUDL first running. 
    // gSpcIo.pIlm = allocate_ilm(MOD_L1SP);

    gSpcIo.pApShareBuf = gSpcIo_ApShareBuffer;
    gSpcIo.apShareBufSize = SPCIO_SHARE_BUF_SIZE;

    gSpcIo.pMdShareBuf = gSpcIo_MdShareBuffer;
	gSpcIo.pMdHighPriBuf = gSpcIo_MDHighPrioBuffer;
    gSpcIo.mdShareBufSize = SPCIO_SHARE_BUF_SIZE;

    gSpcIo.mdShareBufWrite = 0;
    gSpcIo.apShareBufWrite = 0;

    gSpcIo.ccciqStatue = SPCIO_QUEUE_STATUS_INIT;

    // done TODO: c2k [spc]
    Spc_Init();
	//init the semphore at init, semphore count = 1;
    ExeSemaphoreCreate(&ENCSem, 1);

    // Move the EM data notify Message after both HwdSph and ValSph are init. 

    // send self a message to check the status of HwdSph and ValSph.
    ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_VALPART_INIT_DONE_MSG, NULL, 0);

    // final step
    gSpcIo.isTaskInitialize = true;
}


void ValPrintMessageQueueTest(void)
{
    extern SPCIO_T gSpcIo;
    int i;

    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 9999, gSpcIo.ccciq.write, gSpcIo.ccciq.read, 9999 );
    for(i=0; i<32; i++) {
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 8, 
            9999, 0,
            gSpcIo.ccciq.queue[i].message_id,
            gSpcIo.ccciq.queue[i].message_data,
            gSpcIo.ccciq.queue[i].message_dataRev,
            gSpcIo.ccciq.queue[i].putTime,
            gSpcIo.ccciq.queue[i].getTime,
            9999);
    }
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 9999, 9999, 9999 );

    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 8989, gSpcIo.apShareBufRead, gSpcIo.apShareBufWrite);
    for(i=0; i<SPCIO_SHARE_BUF_SIZE; ) {
        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 9, 8989, 
            gSpcIo.pApShareBuf[i],
            gSpcIo.pApShareBuf[i+1],
            gSpcIo.pApShareBuf[i+2],
            gSpcIo.pApShareBuf[i+3],
            gSpcIo.pApShareBuf[i+4],
            gSpcIo.pApShareBuf[i+5],
            gSpcIo.pApShareBuf[i+6],
            gSpcIo.pApShareBuf[i+7]
            );
        i+=8;
    }
    
    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 8989, 8989, 8989 );
}


