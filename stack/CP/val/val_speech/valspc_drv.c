/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * spc_drv.c
 *
 * Project:
 * --------
 * MAUI
 *
 * Description:
 * ------------
 * MD speech control  
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:   1.85  $
 * $Modtime:   Jul 27 2005 09:18:28  $
 * $Log:   //mtkvs01/vmdata/Maui_sw/archives/mcu/l1audio/l1sp.c-arc  $
 *
 * 10 13 2015 ting-ni.chen
 * [SIXTH00004825] [GINR6753_65C_L1][Z160][modem]使用过程中弹出modem重启提示
 * acoustic loopback
 *
 * 09 30 2015 miyavi.tsai
 * [SIXTH00004533] Add audio dynamic param support and modify SPE control flow
 * for audio tuning tool modify
 *
 * 09 21 2015 miyavi.tsai
 * [SIXTH00004533] Add audio dynamic param support and modify SPE control flow
 * NEW SPE control flow
 *
 * 07 15 2015 ys.hsieh
 * [SIXTH00003582] [JADE][MT6755][SPE] New SPE architecture porting
 * 0x2f79 cmd bypass srlte
 *
 * 04 13 2015 ys.hsieh
 * [SIXTH00002536] [Denali-1][MT6735][Stage1][CSFB DSDS][Free test] When flight mode on / off UE happened "After AP send EPOF, MD didn't go to sleep in 4 seconds" exception
 * CBP c2k speech EPOF flight mode
 *
 * 10 22 2014 fu-shing.ju
 * [MOLY00078649] Phone Call Recording Quality Enhancement.
 * Phone Call Recording Quality Enhancement.
 * 
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#include "sysapi.h"
#include "monapi.h"
#include "monids.h"
#include "valspherr.h"

#include "valspc_drv.h"
#include "valaudio_em_struct.h"
#include "valspc_io.h"
#include "valaudio_enum.h"
#include "valsph_coeff_default.h"

#include "hwdsph.h"
#include "hwdsp_enhance.h"
#include "hwdafe.h"
#include "hwdsidetone.h"
#include "hwdbgSnd.h"
#include "hwdvm.h"
#include "hwdaudioservice.h"
#include "hwdrawpcmrec.h"
#include "hwdpcmrec.h"

#include "hwdpcm4way.h" 
#include "hwdbtsco_drv.h"
#include "hwdctm_drv.h"

#include "valapi.h"
#include "exeapi.h"

// TODO: c2k 
// done [sidetone]
// [ASSERT]
// [trace]
// done [init skip]
// done [em init]
// [vm]

#define ACLOOPBACK_USING_P2W 1

#define ACLOOPBACK_FRAME_BUF_NO (64) // REMIND: should be pow of 2 
#define ACLOOPBACK_NB_FRAME_SIZE   (160)
#define ACLOOPBACK_DELAY 12 // unit is 20ms 
#define ACLOOPBACK_SILENCE_FRAME_NO 5

#if ACLOOPBACK_USING_P2W
static uint16 gSpc_acLoopback_PCM_BUF[ACLOOPBACK_FRAME_BUF_NO][ACLOOPBACK_NB_FRAME_SIZE];
#else
// static uint16 gSpc_acLoopback_PCM_UL_BUF[ACLOOPBACK_FRAME_BUF_NO][ACLOOPBACK_NB_FRAME_SIZE];
static uint16 gSpc_acLoopback_PCM_UL_BUF[ACLOOPBACK_NB_FRAME_SIZE];
static uint16 gSpc_acLoopback_PCM_DL_BUF[ACLOOPBACK_FRAME_BUF_NO][ACLOOPBACK_NB_FRAME_SIZE];
#endif 


#if defined(__ENABLE_SPEECH_DVT__)
extern void Spc_PCMNWay_DVT_Test(kal_uint8 uParam);
extern void Spc_PCMNWay_DVT_Test_by_Rec_Button(kal_uint8 uParam, uint8 format, uint8 sampling_rate_idx, uint8 ch_num_idx);
#endif // defined(__ENABLE_SPEECH_DVT__)

#define true         (bool)(1==1)
#define false        (bool)(1==0)

extern uint32	HwdGetSysMode(void); // factory mode test

typedef enum {
	SPC_APP_SPEECH_USAGE = 0,	
	SPC_APP_PCMNWAY_USAGE,
	SPC_APP_RECORD_USAGE, // either "SPC_APP_RECORD_USAGE" or "SPC_APP_PCM_REC_USAGE & SPC_APP_VM_REC_USAGE" usage
	SPC_APP_BGSND_USAGE,	
	
	SPC_APP_CTM_USAGE,
	SPC_APP_ACLOOPBACK_USAGE,
	
	SPC_APP_PCM_REC_USAGE, // 6, add from MT6582 chip
	SPC_APP_VM_REC_USAGE,
	
	SPC_APP_HOLD_USAGE, // cannot mix with other speech application
	SPC_APP_HOSTED_USAGE,	
	SPC_APP_PCMROUTER_USAGE, 

	//We can't enable all record function, only following selections are acceptable:
	//1. SPC_APP_RECORD_USAGE
	//2. SPC_APP_PCM_REC_USAGE
	//3. SPC_APP_PCM_REC_USAGE     + SPC_APP_VM_REC_USAGE
	//4. SPC_APP_RAW_PCM_REC_USAGE
	//5. SPC_APP_RAW_PCM_REC_USAGE + SPC_APP_VM_REC_USAGE
	SPC_APP_RAW_PCM_REC_USAGE, //11, add from MT6752 chip
	
	SPC_APP_MAX = 16 // due to spcAppUsage is uint16. so only 16 application can existing.
} MSG_SPC_APP_T;

typedef enum{
	SPC_EM_INIT_COMMON = 0,
	SPC_EM_INIT_NB,
	SPC_EM_INIT_WB,
	SPC_EM_INIT_DMNR,
	SPC_EM_INIT_LSPK_DMNR,

	SPC_EM_INIT_MAX = 16 // due to spcEmInit is unit16, so only 16 em data message can existing. 
} SPC_EM_INIT_T;

typedef enum{
	SPC_DROP_MUTE_UL = 1,
	SPC_DROP_MUTE_DL, 
	SPC_DROP_MUTE_UL_ENH_RESULT,
	SPC_DROP_MUTE_UL_SOURCE,
} SPC_DROP_MUTE_POS_T;
typedef struct _SPC_T_
{
	uint16           spcGetEpofTimes;
	uint16           spFeatureList;
	uint16           spcAppUsage; //Please reference MSG_SPC_APP_T for bit definition

	uint16           spcEmInit; 

	// DSP mute 
	bool             spcMuteUl;
	bool             spcMuteDl;

	bool             spcMuteUlEnhResult;
	bool             spcMuteUlSource;
} _SPC_T;

#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 




typedef struct spcDynParHeaderStruct 
{
   uint16 SphParserVer; 
   uint16 NumLayer;
   uint16 NumEachLayer;
   uint16 Param_Header_Layer1;
   uint16 Param_Header_Layer2;
   uint16 Param_Header_Layer3;
   uint16 Param_Header_Layer4;
   uint16 SphUnitMagiNum;
} spcDynParHeader;
uint16 Data_Header;


typedef struct spcDynParNBDataStruct 
{
   unsigned short speech_Mode_para[NUM_MODE_PARAS];
   unsigned short speech_NB_FIR_IN_para[NUM_FIR_COEFFS];
   unsigned short speech_NB_FIR_OUT_para[NUM_FIR_COEFFS];
} spcDynParNBData;

typedef struct spcDynParWBDataStruct 
{
   
   unsigned short speech_Mode_para[NUM_MODE_PARAS];
   unsigned short speech_WB_FIR_IN_para[NUM_WB_FIR_COEFFS];
   unsigned short speech_WB_FIR_OUT_para[NUM_WB_FIR_COEFFS];
} spcDynParWBData;

typedef struct spcMagiClarityDataStruct 
{   
   short MagiClarity_Para[32];  
} spcMagiClarityData;

typedef struct spcCommonDataStruct 
{   

   unsigned short Common_Para[NUM_COMMON_PARAS];
   unsigned short Debug_info_para[16];
  
} spcCommonData;

typedef struct spcDynParEchoRefForUsbAudioStruct 
{
   
   	bool isEchoRefForUsbAudioOn;
	short chip_delay_with_switch;
	short mic_index; // bit wise definition ordered from main mic to reference mic. Only one bit is set!! bit 0: o17, bit 1: o18, bit 2: o23, bit 3: o24, bit 4: o25, 
	
} spcDynParEchoRefForUsbAudio_T;


int16 DMNR_NB_REC[NUM_DMNR_PARAM];
int16 DMNR_NB_LoudSpk[NUM_DMNR_PARAM];
int16 DMNR_WB_REC[NUM_WB_DMNR_PARAM];
int16 DMNR_WB_LoudSpk[NUM_WB_DMNR_PARAM];
//DMNR header
#define Header_DMNR_NB_REC 0x0011
#define Header_DMNR_NB_LSP 0x0012
#define Header_DMNR_WB_REC 0x0021
#define Header_DMNR_WB_LSP 0x0022


// define magic number 
#define Vol_de_speech_unit 0xaa01// volume dependent speech unit 
#define Vol_in_general_unit 0xaa02//volume independent general unot
#define Vol_in_DMNR 0xaa03 //DMNR
#define Vol_in_MagiClarity 0xaa04 //MagiClarity
#define Vol_in_echoRefforUsbAudio 0xaa06 // USB audio echo reference path 

//define data header - voice band 
#define NB_Par 0x10
#define WB_Par 0x20
#define SWB_Par 0x30
//define param header- voice band 
#define VoiceBand_NB (1 << 1) 
#define VoiceBand_WB (1 << 2) 
#define VoiceBand_SWB 0x04
#define VoiceBand_FB 0x08



//define data header -network 

#define GSM_NETWORK 0x01
#define WCDMA_NETWORK 0x02
#define CDMA_NETWORK 0x03
#define VOLTE_NETWORK 0x04
#define C2K_NETWORK 0x05
#define DONT_CARE_NETWORK_V0 0x0F
#define DONT_CARE_NETWORK_V1 0x0FFF

unsigned short Temp_speech_NB_Mode_para[TOTAL_NETWORK_NUMBER][NUM_MODE_PARAS];
unsigned short Temp_speech_NB_FIR_IN_para[TOTAL_NETWORK_NUMBER][NUM_FIR_COEFFS];
unsigned short Temp_speech_NB_FIR_OUT_para[TOTAL_NETWORK_NUMBER][NUM_FIR_COEFFS];

unsigned short Temp_speech_WB_Mode_para[TOTAL_NETWORK_NUMBER][NUM_MODE_PARAS];
unsigned short Temp_speech_WB_FIR_IN_para[TOTAL_NETWORK_NUMBER][NUM_WB_FIR_COEFFS];
unsigned short Temp_speech_WB_FIR_OUT_para[TOTAL_NETWORK_NUMBER][NUM_WB_FIR_COEFFS];


#endif



// ----------------------------------------------------------------------------
// Global Variable
// ----------------------------------------------------------------------------

static _SPC_T gSpc;
_SP_ENC SP_ENC; 

__attribute__((aligned (4)))static uint8 emDataBuf[4096];



// ----------------------------------------------------------------------------
// Macro function
// ----------------------------------------------------------------------------

#define IS_SPC_APP_USED(app)     ((gSpc.spcAppUsage & (1 << (app))) != 0)
#define SET_SPC_APP_USAGE(app)   (gSpc.spcAppUsage |= (1 << (app)))
#define CLR_SPC_APP_USAGE(app)   (gSpc.spcAppUsage &= (~(1 << (app))))


#define IS_SPC_EM_INIT(emPara)   ((gSpc.spcEmInit& (1 << (emPara))) != 0)
#define SET_SPC_EM_INIT(emPara)    (gSpc.spcEmInit |= (1 << (emPara)))

// done TODO: c2k [init skip]
#if ((defined(MT6280) || defined(MT6290)) && (defined(__SMART_PHONE_MODEM__))) || defined(VALSPC_CHIP_BACK_PHONECALL_USE) || defined(MTK_PLT_MODEM_ONLY)
// for super dongle project on data card chip, MD reset scenario is useless
// So the protection is unnecessary and un-sync with AP side, so the request is always done
#define IS_SPC_ID_SPEECH_CUSTOM_DATA_REQUEST_DONE (true)
#else
#define IS_SPC_ID_SPEECH_CUSTOM_DATA_REQUEST_DONE (IS_SPC_EM_INIT(SPC_EM_INIT_COMMON)) // using comm parameter data notify as EM data request ack
#endif

// ----------------------------------------------------------------------------
// Internal Init Related
// ----------------------------------------------------------------------------


const unsigned short Speech_Normal_Mode_Para[16] = DEFAULT_SPEECH_NORMAL_MODE_PARA;
const unsigned short Speech_Earphone_Mode_Para[16] = DEFAULT_SPEECH_EARPHONE_MODE_PARA;
const unsigned short Speech_LoudSpk_Mode_Para[16] = DEFAULT_SPEECH_LOUDSPK_MODE_PARA;
const unsigned short Speech_BT_Earphone_Mode_Para[16] = DEFAULT_SPEECH_BT_EARPHONE_MODE_PARA;
const unsigned short Speech_BT_Cordless_Mode_Para[16] = DEFAULT_SPEECH_BT_CORDLESS_MODE_PARA;
const unsigned short Speech_CARKIT_Mode_Para[16] = DEFAULT_SPEECH_CARKIT_MODE_PARA;
const unsigned short Speech_AUX1_Mode_Para[16] = DEFAULT_SPEECH_AUX1_MODE_PARA;
const unsigned short Speech_AUX2_Mode_Para[16] = DEFAULT_SPEECH_AUX2_MODE_PARA;

const unsigned short WB_Speech_Normal_Mode_Para[16] = DEFAULT_WB_SPEECH_NORMAL_MODE_PARA;
const unsigned short WB_Speech_Earphone_Mode_Para[16] = DEFAULT_WB_SPEECH_EARPHONE_MODE_PARA;
const unsigned short WB_Speech_LoudSpk_Mode_Para[16] = DEFAULT_WB_SPEECH_LOUDSPK_MODE_PARA;
const unsigned short WB_Speech_BT_Earphone_Mode_Para[16] = DEFAULT_WB_SPEECH_BT_EARPHONE_MODE_PARA;
const unsigned short WB_Speech_BT_Cordless_Mode_Para[16] = DEFAULT_WB_SPEECH_BT_CORDLESS_MODE_PARA;
const unsigned short WB_Speech_CARKIT_Mode_Para[16] = DEFAULT_WB_SPEECH_CARKIT_MODE_PARA;
const unsigned short WB_Speech_AUX1_Mode_Para[16] = DEFAULT_WB_SPEECH_AUX1_MODE_PARA;
const unsigned short WB_Speech_AUX2_Mode_Para[16] = DEFAULT_WB_SPEECH_AUX2_MODE_PARA;

// #if defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)
// #ifdef __AMRWB_LINK_SUPPORT__
const int16 WB_DMNR_Para[76] = DEFAULT_WB_DMNR_PARAM;
const int16 LSpk_WB_DMNR_Para[76] = DEFAULT_LSPK_WB_DMNR_PARAM; 
// #endif
const int16 DMNR_Para[44] = DEFAULT_DMNR_PARAM;
const int16 LSpk_DMNR_Para[44] = DEFAULT_LSPK_DMNR_PARAM; 
// #endif

const signed short Speech_Input_FIR_Coeff[6][45] = SPEECH_INPUT_FIR_COEFF_DEFAULT;
const signed short Speech_Output_FIR_Coeff[6][45] = SPEECH_OUTPUT_FIR_COEFF_DEFAULT;

const signed short WB_Speech_Input_FIR_Coeff[6][90] = WB_SPEECH_INPUT_FIR_COEFF_DEFAULT;
const signed short WB_Speech_Output_FIR_Coeff[6][90] = WB_SPEECH_OUTPUT_FIR_COEFF_DEFAULT;

/**

*/
#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
void spc_EmInit(kal_bool isNeedBasic, kal_bool isNeedNb,kal_bool isNeedWb, kal_bool isNeedDmnr, kal_bool isNeedLspkDmnr)
{
	//kal_trace(TRACE_INFO, SPC_EM_DEFAULT_INIT, isNeedBasic, isNeedWb, isNeedDmnr, isNeedLspkDmnr);
	
	// ====== parameters from EM =======	
	if(isNeedBasic){
		// common parameter
		
		uint16 commonP[] = { 0, 55997, 31000, 10752,     32769,     0,     0,     0,     0,     0, 
							      0, 0};

		L1SP_LoadCommonSpeechPara(commonP);
		
	
		
	}
	if(isNeedNb){
		unsigned short default_speech_mode_para[TOTAL_NETWORK_NUMBER][NUM_MODE_PARAS];
		signed short     default_Speech_Input_FIR_Coeff[TOTAL_NETWORK_NUMBER][NUM_FIR_COEFFS];
		signed short     default_Speech_Output_FIR_Coeff[TOTAL_NETWORK_NUMBER][NUM_FIR_COEFFS];
		// mode parameter  , default use normal mode,GSM,NB 
		
	    memcpy(default_speech_mode_para[0], Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		memcpy(default_speech_mode_para[1], Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		memcpy(default_speech_mode_para[2], Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		memcpy(default_speech_mode_para[3], Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		l1sp_setAllSpeechModePara((uint16 *)default_speech_mode_para, TOTAL_NETWORK_NUMBER * NUM_MODE_PARAS);
	   // SPE_LoadSpeechPara(NULL, gSpe.sph_ModePara[0], NULL, NULL); // l1sp.sph_v_para no one use it.	
	   
	
		// FIR , default use normal mode,GSM,NB 
		memcpy(default_Speech_Input_FIR_Coeff[0], Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_Speech_Input_FIR_Coeff[1], Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_Speech_Input_FIR_Coeff[2], Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_Speech_Input_FIR_Coeff[3], Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		l1sp_setAllSpeechFirCoeff_InputOnly((int16 *)default_Speech_Input_FIR_Coeff, TOTAL_NETWORK_NUMBER*NUM_FIR_COEFFS);
        memcpy(default_Speech_Output_FIR_Coeff[0], Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_Speech_Output_FIR_Coeff[1], Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_Speech_Output_FIR_Coeff[2], Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_Speech_Output_FIR_Coeff[3], Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		l1sp_setAllSpeechFirCoeff_OutputOnly((int16 *)default_Speech_Output_FIR_Coeff, TOTAL_NETWORK_NUMBER*NUM_FIR_COEFFS);
		//AM_WriteFirCoeffs( gSpe.sph_InFirCoeff[0], gSpe.sph_OutFirCoeff[0] );
		
	}

	if(isNeedWb){
		
		// WB FIR
#if 1//#ifdef __AMRWB_LINK_SUPPORT__	
		// in customer folder's Audcoeff.c
		unsigned short default_WB_speech_mode_para[TOTAL_NETWORK_NUMBER][NUM_MODE_PARAS];
		signed short     default_WB_Speech_Input_FIR_Coeff[TOTAL_NETWORK_NUMBER][NUM_FIR_COEFFS];
		signed short     default_WB_Speech_Output_FIR_Coeff[TOTAL_NETWORK_NUMBER][NUM_FIR_COEFFS];
		
		
        // WB mode parameter  , default use normal mode,GSM,NB 
		
	    memcpy(default_WB_speech_mode_para[0], WB_Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		memcpy(default_WB_speech_mode_para[1], WB_Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		memcpy(default_WB_speech_mode_para[2], WB_Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		memcpy(default_WB_speech_mode_para[3], WB_Speech_Normal_Mode_Para, NUM_MODE_PARAS*sizeof(uint16)) ;
		l1sp_setAllWbSpeechModePara((uint16 *)default_WB_speech_mode_para, TOTAL_NETWORK_NUMBER * NUM_MODE_PARAS);
	    //SPE_LoadSpeechPara(NULL, NULL, NULL,gSpe.sph_WbModePara[0] );

		
		//extern const signed short WB_Speech_Input_FIR_Coeff[6][90];
		//extern const signed short WB_Speech_Output_FIR_Coeff[6][90];
		
		

		// WB FIR , default use normal mode,GSM,NB 
		memcpy(default_WB_Speech_Input_FIR_Coeff[0], WB_Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_WB_Speech_Input_FIR_Coeff[1], WB_Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_WB_Speech_Input_FIR_Coeff[2], WB_Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_WB_Speech_Input_FIR_Coeff[3], WB_Speech_Input_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		l1sp_setAllWbSpeechFirCoeff_InputOnly((int16 *)default_WB_Speech_Input_FIR_Coeff, TOTAL_NETWORK_NUMBER*NUM_WB_FIR_COEFFS);
        memcpy(default_WB_Speech_Output_FIR_Coeff[0], WB_Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_WB_Speech_Output_FIR_Coeff[1], WB_Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_WB_Speech_Output_FIR_Coeff[2], WB_Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		memcpy(default_WB_Speech_Output_FIR_Coeff[3], WB_Speech_Output_FIR_Coeff[0], NUM_FIR_COEFFS*sizeof(uint16)) ;
		l1sp_setAllWbSpeechFirCoeff_OutputOnly((int16 *)default_WB_Speech_Output_FIR_Coeff, TOTAL_NETWORK_NUMBER*NUM_WB_FIR_COEFFS);
		//AM_WriteWbFirCoeffs(gSpe.sph_WbInFirCoeff[0],gSpe.sph_WbOutFirCoeff[0]);
		
#endif // __AMRWB_LINK_SUPPORT__
	}

	if(isNeedDmnr){
		
#if defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)
#if 1//#ifdef __AMRWB_LINK_SUPPORT__
		L1SP_SetWbDMNRPara(WB_DMNR_Para);
#endif
		L1SP_SetDMNRPara(DMNR_Para);
#endif //  defined(__DUAL_MIC_SUPPORT__)|| defined(__SMART_PHONE_MODEM__)
		
	}

	if(isNeedLspkDmnr){ // load param when begin run
#if defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)
#if 1//#ifdef __AMRWB_LINK_SUPPORT__
		L1SP_SetLSpkWbDMNRPara(LSpk_WB_DMNR_Para);
#endif
		L1SP_SetLSpkDMNRPara(LSpk_DMNR_Para);
#endif //  defined(__DUAL_MIC_SUPPORT__)|| defined(__SMART_PHONE_MODEM__)		
	}
	
	// loading!!
	// if(isNeedBasic || isNeedWb) {
	// 	L1SP_LoadSpeechPara();	
	// }
}

#else

void spc_EmInit(kal_bool isNeedBasic, kal_bool isNeedNb,kal_bool isNeedWb, kal_bool isNeedDmnr, kal_bool isNeedLspkDmnr)
{
	// kal_trace(TRACE_INFO, SPC_EM_DEFAULT_INIT, isNeedBasic, isNeedWb, isNeedDmnr, isNeedLspkDmnr);
	MonTrace(MON_CP_VAL_SPH_EM_USE_DEFAULT_TRACE_ID, 4, isNeedBasic, isNeedWb, isNeedDmnr, isNeedLspkDmnr);
	
	// ====== parameters from EM =======	
	if(isNeedBasic){
		AUDIO_CUSTOM_PARAM_STRUCT *audio_par;			// common parameter
		uint16 commonP[] = { 0, 55997, 31000, 10752,     32769,     0,     0,     0,     0,     0, 
							      0, 0};
		
		L1SP_LoadCommonSpeechPara(commonP);
	
		// mode parameter		
		audio_par = (AUDIO_CUSTOM_PARAM_STRUCT *)(&emDataBuf); //buffer reuse
	   memcpy(&(audio_par->speech_mode_para[0]), Speech_Normal_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_para[1]), Speech_Earphone_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_para[2]), Speech_LoudSpk_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_para[3]), Speech_BT_Earphone_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_para[4]), Speech_BT_Cordless_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_para[5]), Speech_CARKIT_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_para[6]), Speech_AUX1_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_para[7]), Speech_AUX2_Mode_Para, 16*sizeof(uint16)) ;	
		l1sp_setAllSpeechModePara((uint16 *)(audio_par->speech_mode_para), 8 * 16);
	
	
		// FIR
		l1sp_setAllSpeechFirCoeff_InputOnly((int16 *)Speech_Input_FIR_Coeff, 6*45);
		l1sp_setAllSpeechFirCoeff_OutputOnly((int16 *)Speech_Output_FIR_Coeff, 6*45);
	}

	if(isNeedWb){
		// WB FIR
// #ifdef __AMRWB_LINK_SUPPORT__	
		// in customer folder's Audcoeff.c
		
		AUDIO_CUSTOM_WB_PARAM_STRUCT *audio_par;	
		audio_par = (AUDIO_CUSTOM_WB_PARAM_STRUCT *)(&emDataBuf); //buffer reuse
	   memcpy(&(audio_par->speech_mode_wb_para[0]), WB_Speech_Normal_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_wb_para[1]), WB_Speech_Earphone_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_wb_para[2]), WB_Speech_LoudSpk_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_wb_para[3]), WB_Speech_BT_Earphone_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_wb_para[4]), WB_Speech_BT_Cordless_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_wb_para[5]), WB_Speech_CARKIT_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_wb_para[6]), WB_Speech_AUX1_Mode_Para, 16*sizeof(uint16)) ;
		memcpy(&(audio_par->speech_mode_wb_para[7]), WB_Speech_AUX2_Mode_Para, 16*sizeof(uint16)) ;	
		l1sp_setAllWbSpeechModePara((uint16 *)(audio_par->speech_mode_wb_para), 8 * 16);
		
		l1sp_setAllWbSpeechFirCoeff_InputOnly((int16 *)WB_Speech_Input_FIR_Coeff, 6*90);
		l1sp_setAllWbSpeechFirCoeff_OutputOnly((int16 *)WB_Speech_Output_FIR_Coeff, 6*90);
// #endif // __AMRWB_LINK_SUPPORT__
	}

	if(isNeedDmnr){
		
// #if defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)
// #ifdef __AMRWB_LINK_SUPPORT__
		L1SP_SetWbDMNRPara(WB_DMNR_Para);
// #endif
		L1SP_SetDMNRPara(DMNR_Para);
// #endif //  defined(__DUAL_MIC_SUPPORT__)|| defined(__SMART_PHONE_MODEM__)
		
	}

	if(isNeedLspkDmnr){ // load param when begin run
// #if defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)
// #ifdef __AMRWB_LINK_SUPPORT__
		L1SP_SetLSpkWbDMNRPara(LSpk_WB_DMNR_Para);
// #endif
		L1SP_SetLSpkDMNRPara(LSpk_DMNR_Para);
// #endif //  defined(__DUAL_MIC_SUPPORT__)|| defined(__SMART_PHONE_MODEM__)		
	}
	
}
#endif
typedef enum
{
	SPC_APP_ON_CHECK_CASES_B4_EM_REQ_DONE,
	SPC_APP_ON_CHECK_CASES_DUPLCATE_MAIN_APP,
} SPC_APP_ON_CHECK_CASES_T;
/**
	@app: app to be enable
	@return: check result, true for pass, false for fail
*/
bool spc_mainAppOnCheck(MSG_SPC_APP_T app, bool byPassEmDataCheck)
{
	if((!IS_SPC_ID_SPEECH_CUSTOM_DATA_REQUEST_DONE) && (false == byPassEmDataCheck)){ // prevent speech on before EM data sending
		// done TODO: c2k [trace] kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, app, app, 1);
		MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, app, SPC_APP_ON_CHECK_CASES_B4_EM_REQ_DONE);
		return false;
	}

	// other related check
	if(SPC_APP_SPEECH_USAGE != app) {
		if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) {
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, app, SPC_APP_ON_CHECK_CASES_DUPLCATE_MAIN_APP, SPC_APP_SPEECH_USAGE, true);
			// done TODO: c2k [trace] kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR_CHECK, app, SPC_APP_SPEECH_USAGE, SPC_APP_SPEECH_USAGE, gSpc.spcAppUsage);
			ASSERT(0, VALSPH_ERR_SPC_DUPLICATE_MAIN_APP_TURN_ON, VALSPH_ERR_SPC_DUPLICATE_MAIN_APP_TURN_ON);
			// done TODO: c2k [ASSERT] 	ASSERT(0);
			return false;
		}
	}

	if(SPC_APP_ACLOOPBACK_USAGE != app) {
		if(IS_SPC_APP_USED(SPC_APP_ACLOOPBACK_USAGE)) {
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, app, SPC_APP_ON_CHECK_CASES_DUPLCATE_MAIN_APP, SPC_APP_ACLOOPBACK_USAGE, true);
			//done TODO: c2k [trace]  kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR_CHECK, app, SPC_APP_ACLOOPBACK_USAGE, SPC_APP_ACLOOPBACK_USAGE, gSpc.spcAppUsage);
			ASSERT(0, VALSPH_ERR_SPC_DUPLICATE_MAIN_APP_TURN_ON, SPC_APP_ACLOOPBACK_USAGE);
			// done TODO: c2k [ASSERT]  ASSERT(0);
			return false;
		}
	}
	
	

	if(SPC_APP_HOLD_USAGE != app) {
		if(IS_SPC_APP_USED(SPC_APP_HOLD_USAGE)) {
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, app, SPC_APP_ON_CHECK_CASES_DUPLCATE_MAIN_APP, SPC_APP_HOLD_USAGE, true);
			// done TODO: c2k [trace]  kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR_CHECK, app, SPC_APP_HOLD_USAGE, SPC_APP_HOLD_USAGE, gSpc.spcAppUsage);
			ASSERT(0, VALSPH_ERR_SPC_DUPLICATE_MAIN_APP_TURN_ON, SPC_APP_HOLD_USAGE);
			// done TODO: c2k [ASSERT]  ASSERT(0);

			return false;
		}
	}

	if(SPC_APP_HOSTED_USAGE != app) {
		if(IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE)) {
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, app, SPC_APP_ON_CHECK_CASES_DUPLCATE_MAIN_APP, SPC_APP_HOSTED_USAGE, true);
			// done TODO: c2k [trace]  kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR_CHECK, app, SPC_APP_HOSTED_USAGE, SPC_APP_HOSTED_USAGE, gSpc.spcAppUsage);
			ASSERT(0, VALSPH_ERR_SPC_DUPLICATE_MAIN_APP_TURN_ON, SPC_APP_HOSTED_USAGE);
			// done TODO: c2k [ASSERT]  ASSERT(0);

			return false;
		}
	}

	if(SPC_APP_PCMROUTER_USAGE != app) {
		if(IS_SPC_APP_USED(SPC_APP_PCMROUTER_USAGE)) {
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, app, SPC_APP_ON_CHECK_CASES_DUPLCATE_MAIN_APP, SPC_APP_PCMROUTER_USAGE, true);
			// done TODO: c2k [trace]  kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR_CHECK, app, SPC_APP_PCMROUTER_USAGE, SPC_APP_PCMROUTER_USAGE, gSpc.spcAppUsage);
			ASSERT(0, VALSPH_ERR_SPC_DUPLICATE_MAIN_APP_TURN_ON, SPC_APP_PCMROUTER_USAGE);
			// done TODO: c2k [ASSERT]  ASSERT(0);

			return false;
		}
	}

	return true;
}


// ----------------------------------------------------------------------------
// Codec Related
// ----------------------------------------------------------------------------

// AP system property max length

#define SPC_PROP_CODEC_LEN 92 

typedef struct {
	char codecInfo[SPC_PROP_CODEC_LEN];
	char codecOp[SPC_PROP_CODEC_LEN];
} SPC_CODEC_INFO_STRUCT;

typedef struct {
	uint16 curSo;
	uint16 curRate;
	SPC_CODEC_INFO_STRUCT info;
} SPC_CODEC_INFO_ALL;
static SPC_CODEC_INFO_ALL gSpc_codec_info; 

#include "hwdlinkapi.h"
#include "string.h"

void spc_codecInfoInit(void)
{
	gSpc_codec_info.curSo = 0xffff;
	// gSpc_codec_info.curRate is skip, due to LINK didnot update every time
	memset(&(gSpc_codec_info.info), 0, sizeof(SPC_CODEC_INFO_STRUCT));
}


/**
	Allow re-send codec info before got a ap-ack 
*/
void spc_codecInfoNotify(bool isSoUpdate, bool isRateUpdate, uint16 newSo, uint16 newRate)
{
	bool isNeedNotifySo = false;
	bool isNeedNotifyRate = false;
	spcBufInfo info;

	MonTrace(MON_CP_VAL_SPH_CODEC_NOTIFY_TRACE_ID, 4, isSoUpdate, isRateUpdate, newSo, newRate);	
	
	if(false == IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)){
		// just leave log and return	
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_CODEC_NOTIFY_WO_CALL, ((newSo<<16)|newRate), MON_CONTINUE);
		return; 
	} 
	
	if(isSoUpdate) {
		isNeedNotifySo = (newSo != gSpc_codec_info.curSo);
		gSpc_codec_info.curSo = newSo; 
	} 	
	
	if(isRateUpdate) {
		isNeedNotifyRate = (newRate != gSpc_codec_info.curRate);
		gSpc_codec_info.curRate = newRate;
	}
	
	if ((true==isNeedNotifySo) || (true==isNeedNotifyRate)) { // TODO: add Op
		memset(&(gSpc_codec_info.info), 0, sizeof(SPC_CODEC_INFO_STRUCT));
		
		switch(gSpc_codec_info.curSo) {
			case C2K_COD_QCELP8K: // should not be here
				stpcpy(gSpc_codec_info.info.codecInfo, "codec=S01;");
				stpcpy(gSpc_codec_info.info.codecOp, "OM=N;");
				break;
			case C2K_COD_QCELP13K:
				stpcpy(gSpc_codec_info.info.codecInfo, "codec=S017;");
				stpcpy(gSpc_codec_info.info.codecOp, "OM=N;");
				break;
			case C2K_COD_EVRCA:
				stpcpy(gSpc_codec_info.info.codecInfo, "codec=S03;");
				stpcpy(gSpc_codec_info.info.codecOp, "OM=N;");
				break;
			case C2K_COD_EVRCB:
				stpcpy(gSpc_codec_info.info.codecInfo, "codec=S068;");
				stpcpy(gSpc_codec_info.info.codecOp, "OM=N;");
				break;
			case C2K_COD_EVRCNW_NB:
				stpcpy(gSpc_codec_info.info.codecInfo, "codec=S073;");
				stpcpy(gSpc_codec_info.info.codecOp, "OM=N;");
				break;
			case C2K_COD_EVRCNW_WB:
				stpcpy(gSpc_codec_info.info.codecInfo, "codec=S073;");
				stpcpy(gSpc_codec_info.info.codecOp, "OM=Y;");
				break;
			default:
				stpcpy(gSpc_codec_info.info.codecInfo, "codec=N/A;");
				stpcpy(gSpc_codec_info.info.codecOp, "OM=N;");
				break;
		}

		// rate reduction
		switch(gSpc_codec_info.curRate){
			case 0:	strcat(gSpc_codec_info.info.codecInfo, "rr=0;"); break;
			case 1:	strcat(gSpc_codec_info.info.codecInfo, "rr=1;"); break;
			case 2:	strcat(gSpc_codec_info.info.codecInfo, "rr=2;"); break;
			case 3:	strcat(gSpc_codec_info.info.codecInfo, "rr=3;"); break;
			case 4:	strcat(gSpc_codec_info.info.codecInfo, "rr=4;"); break;
			case 5:	strcat(gSpc_codec_info.info.codecInfo, "rr=5;"); break;
			case 6:	strcat(gSpc_codec_info.info.codecInfo, "rr=6;"); break;
			case 7:	strcat(gSpc_codec_info.info.codecInfo, "rr=7;"); break;
			default: break;
		}
		
		if (MonSpyInquire(MON_CP_VALSPH_CODECINFO_SPY_ID)) {
				MonSpy(MON_CP_VALSPH_CODECINFO_SPY_ID, (uint8 *)&gSpc_codec_info, sizeof(SPC_CODEC_INFO_ALL));
		}
		
		info.syncWord = 0x2A2A;
		info.type = AUD_CCCI_STRMBUF_TYPE_NW_CODEC_INFO;
		info.length = sizeof(SPC_CODEC_INFO_STRUCT); // currently, fix the buffer size in WB 
		
		SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
				 &(gSpc_codec_info.info), sizeof(SPC_CODEC_INFO_STRUCT), NULL, 0,
				 SPCIO_MSG_FROM_SPC_NW_CODEC_INFO_NOTIFY);
	}else {
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_CODEC_NOTIFY_DUMMY, ((newSo<<16)|newRate), MON_CONTINUE);
	}
	
}

void spc_ReceiveNwCodecInfoReadDone(void)
{
	l1sp_send_codec_status_notify(gSpc_codec_info.curSo);
}

void SPC_ServiceOptionNotify(void *data)
{
	uint16 newSo = *((uint16 *)data);

#if !defined(MTK_PLT_MODEM_ONLY)
	spc_codecInfoNotify(true, false, newSo, 0);
#endif // MTK_PLT_MODEM_ONLY

	l1sp_CodecStatusNotification(newSo);
}

void SPC_RateReduceNotify(void *data)
{

#if !defined(MTK_PLT_MODEM_ONLY)
	uint16 newRR = *((uint16 *)data);

	spc_codecInfoNotify(false, true, 0, newRR);
#endif // MTK_PLT_MODEM_ONLY

}


// ----------------------------------------------------------------------------
// Phone Call Related
// ----------------------------------------------------------------------------
#define SPC_CUST_ENH 0
#if SPC_CUST_ENH
#include "hwdaudioservice.h"
#define PROCESS_BLK_SIZE 320
static uint16 gSpc_DV_PCM_UL1_BUF[PROCESS_BLK_SIZE];
static uint16 gSpc_DV_PCM_UL2_BUF[PROCESS_BLK_SIZE];
static uint16 gSpc_DV_PCM_IN_BUF[PROCESS_BLK_SIZE];
static uint16 gSpc_DV_PCM_DL_BUF[PROCESS_BLK_SIZE];
static uint16 gSpc_DV_PCM_OUT_BUF[PROCESS_BLK_SIZE];
static uint16 DV_dl_audioid; 
static uint16 DV_ul_audioid;

void DV_init(void)
{
	// Do DV initialize
    return;
}

void DV_Release(void)
{
    // Do DV destroy
}

void DV_UL_process()
{
	// do speech enahncment for uplink
	// gSpc_DV_PCM_IN_BUF = ulProcess(gSpc_DV_PCM_UL1_BUF, gSpc_DV_PCM_UL2_BUF); 
}

void DV_DL_process()
{
	// do speech enahncment for downlink
	// gSpc_DV_PCM_OUT_BUF = dlProcess(gSpc_DV_PCM_DL_BUF); 
	memcpy(gSpc_DV_PCM_OUT_BUF, gSpc_DV_PCM_DL_BUF, 640);

	spc_customDump(320, gSpc_DV_PCM_OUT_BUF);
}

void DV_PCM4WAY_DL_HisrHdl()
{

    PCM4WAY_GetFromSD(gSpc_DV_PCM_DL_BUF);

    PCM4WAY_PutToSpk(gSpc_DV_PCM_OUT_BUF);  

	L1Audio_SetEvent(DV_dl_audioid, NULL);
	
}

void DV_PCM4WAY_UL_HisrHdl()
{

	
    PcmEx_GetFromMic1(gSpc_DV_PCM_UL1_BUF);
    PcmEx_GetFromMic2(gSpc_DV_PCM_UL2_BUF);

	
    
    PCM4WAY_PutToSE(gSpc_DV_PCM_IN_BUF); 
	
	L1Audio_SetEvent(DV_ul_audioid, NULL);
}
#endif // SPC_CUST_ENH

void Spc_SpeechOn( uint8 RAT_Mode )
{
	if(false == spc_mainAppOnCheck(SPC_APP_SPEECH_USAGE, false))
		return;

	// new added for re-entry
	if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)){
		// just leave log and return	
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_SPEECH_USAGE, MON_CONTINUE);
		return; 
	} 
	
	// TODO: c2k [ASSERT]	ASSERT(RAT_Mode <= RAT_3G324M_MODE);

#ifdef CONNECT_WM8904
	Config_WM8904(MIC_8K_LONGSYNC_PHONE_CALL); //Config_WM8904(-1); MIC_8K_LONGSYNC_PHONE_CALL == 2
#endif	

	MonTrace(MON_CP_VAL_SPH_PROMPT_TRACE_ID, 3, 124, RAT_Mode, 124);

	// checking speech useage parameter. if Not, use the default value. 	
	spc_EmInit(!IS_SPC_EM_INIT(SPC_EM_INIT_COMMON),!IS_SPC_EM_INIT(SPC_EM_INIT_NB), 
		!IS_SPC_EM_INIT(SPC_EM_INIT_WB),!IS_SPC_EM_INIT(SPC_EM_INIT_DMNR),
		!IS_SPC_EM_INIT(SPC_EM_INIT_LSPK_DMNR));
	

	// rate setting
#if defined(__UMTS_RAT__)
	if( RAT_Mode != RAT_3G324M_MODE )
		RAT_Mode = RAT_3G_MODE; // Force initial 3G
#else
		RAT_Mode = RAT_2G_MODE;
#endif

	MonTrace(MON_CP_VAL_SPH_PROMPT_TRACE_ID, 3, 122, RAT_Mode, 122);
   // TODO: c2k [trace] kal_trace( TRACE_INFO, SPC_SPEECH_ON, RAT_Mode);

	// set sidetone solution ver before application start
	SIDETONE_SetSolutionVer(SMART_PHONE_SIDETONE_SOLUTION_VER);
    if(SP_ENC.Is_Voice_Encryption)
    {
    	ResetEncInfo();
    }
  	// reset codec before speechOn.
	spc_codecInfoInit();
	
	L1SP_Speech_On( RAT_Mode );

	SET_SPC_APP_USAGE(SPC_APP_SPEECH_USAGE);

	// enable sidetone update
#ifdef VALSPC_CHIP_BACK_PHONECALL_USE
#else	
	#if !defined(__OPEN_DSP_SPEECH_SUPPORT__)
		//SIDETONE_UpdateStart();
	#endif		
#endif

#if SPC_CUST_ENH	
    DV_init();
    Del_PcmEx_Start(DV_PCM4WAY_DL_HisrHdl, NULL, //DV_PCM4WAY_UL_HisrHdl, 
			NULL, //USE_D2M_PATH + USE_M2D_PATH,
			NULL, //USE_D2M_PATH + USE_M2D_PATH,
			NULL, //USE_D2M_PATH + USE_M2D_PATH,
			NULL, //USE_D2M_PATH + USE_M2D_PATH,
			USE_D2M_PATH + USE_M2D_PATH + DATA_SELECT_AFTER_ENH);
#endif	

}

void Spc_SpeechOff( void )
{   
	//[trace] kal_trace( TRACE_INFO, SPC_SPEECH_OFF);

	ASSERT(0==IS_SPC_APP_USED(SPC_APP_PCMNWAY_USAGE), VALSPH_ERR_SPC_CLOSE_MAIN_APP_B4_SUB_APP_CLOSED, SPC_APP_PCMNWAY_USAGE);
	ASSERT(0==IS_SPC_APP_USED(SPC_APP_RECORD_USAGE), VALSPH_ERR_SPC_CLOSE_MAIN_APP_B4_SUB_APP_CLOSED, SPC_APP_RECORD_USAGE);
	ASSERT(0==IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE), VALSPH_ERR_SPC_CLOSE_MAIN_APP_B4_SUB_APP_CLOSED, SPC_APP_PCM_REC_USAGE);
	ASSERT(0==IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE), VALSPH_ERR_SPC_CLOSE_MAIN_APP_B4_SUB_APP_CLOSED, SPC_APP_VM_REC_USAGE);
	ASSERT(0==IS_SPC_APP_USED(SPC_APP_BGSND_USAGE), VALSPH_ERR_SPC_CLOSE_MAIN_APP_B4_SUB_APP_CLOSED, SPC_APP_BGSND_USAGE);
	ASSERT(0==IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE), VALSPH_ERR_SPC_CLOSE_MAIN_APP_B4_SUB_APP_CLOSED, SPC_APP_RAW_PCM_REC_USAGE);

	if(!IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)){
		// just leave log and return
		// done TODO: c2k [trace] kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_SPEECH_USAGE, SPC_APP_SPEECH_USAGE, 0);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNUSED_APP_OFF, SPC_APP_SPEECH_USAGE, MON_CONTINUE);
		return; 
	} 
	
#if SPC_CUST_ENH	
		PcmEx_Stop();
		DV_Release();
#endif 

	// disalbe sidetone update
#ifdef VALSPC_CHIP_BACK_PHONECALL_USE
#else
	//SIDETONE_UpdateStop();
#endif

	// close speech 
	L1SP_Speech_Off();

	CLR_SPC_APP_USAGE(SPC_APP_SPEECH_USAGE);

	// done TODO: c2k [sidetone]
	// reset sieton solution ver after application stop
	SIDETONE_ResetSolutionVer();

}

void Spc_SetSpeechMode_Adaptation( uint8 mode )
{
   int16 totalModeNum;

   MonTrace(MON_CP_VAL_SPH_SPE_SET_SPEECHMODE_ADAPT_TRACE_ID, 1, mode);

	//[REMIND] For MT6589, idle recording function are implement in AP side, 
	// so record mode information is not existing in MD side
   totalModeNum = (SPH_MODE_UNDEFINED-1); // + l1sp_getNumOfRecordMode();
   if (mode >= totalModeNum) {
      // ASSERT(0);
      MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_SET_INCORRECT_SPH_MODE, mode, MON_HALT);		
   }

   // [REMIND] Because Smart phone only has 8+1 speech mode, which is different from l1audio.h's definition. 
   //As the result, we need to mapping the mode into the correct one. 
   L1SP_SetSpeechEnhanceAndFir(mode, SPH_ENH_AND_FIR_UPDATE_TYPE_ALL);

}

typedef enum {
	SPC_EM_PARAM_TYPE_COMMON,
	SPC_EM_PARAM_TYPE_MODE_PARAM,
	SPC_EM_PARAM_TYPE_WB_MODE_PARAM,
	
} SpcEmParamTypeT;

void spc_DebugPrint(void)
{
	uint16 *m_para; 
	
	m_para = Sp_GetCommonSpeechPara(); // size NUM_COMMON_PARAS
	MonTrace(MON_CP_VAL_SPH_EM_DEBUG_PRINT_TRACE_ID, 8, 
		SPC_EM_PARAM_TYPE_COMMON, 0,
		m_para[0], m_para[1], m_para[2], m_para[3], m_para[4], m_para[5] );
	MonTrace(MON_CP_VAL_SPH_EM_DEBUG_PRINT_TRACE_ID, 8, 
		SPC_EM_PARAM_TYPE_COMMON, 6,
		m_para[6], m_para[7], m_para[8], m_para[9], m_para[10], m_para[11]);
	
	m_para = Sp_GetSpeechPara(); // size NUM_MODE_PARAS
	MonTrace(MON_CP_VAL_SPH_EM_DEBUG_PRINT_TRACE_ID, 10, 
		SPC_EM_PARAM_TYPE_MODE_PARAM, 0,
		m_para[0], m_para[1], m_para[2], m_para[3], m_para[4], m_para[5], m_para[6], m_para[7]);
	MonTrace(MON_CP_VAL_SPH_EM_DEBUG_PRINT_TRACE_ID, 10, 
		SPC_EM_PARAM_TYPE_MODE_PARAM, 8,
		m_para[8], m_para[9], m_para[10], m_para[11], m_para[12], m_para[13], m_para[14], m_para[15]);
	
	m_para = Sp_GetWbSpeechPara(); // size NUM_MODE_PARAS
	MonTrace(MON_CP_VAL_SPH_EM_DEBUG_PRINT_TRACE_ID, 10, 
		SPC_EM_PARAM_TYPE_WB_MODE_PARAM, 0,
		m_para[0], m_para[1], m_para[2], m_para[3], m_para[4], m_para[5], m_para[6], m_para[7]);
	MonTrace(MON_CP_VAL_SPH_EM_DEBUG_PRINT_TRACE_ID, 10, 
		SPC_EM_PARAM_TYPE_WB_MODE_PARAM, 8,	
		m_para[8], m_para[9], m_para[10], m_para[11], m_para[12], m_para[13], m_para[14], m_para[15]);
		
}

#if 0 // temp for c2k
//AUDL task
void spc_notify_rf_info(void)
{
	uint16 rf_info = 0x0;
	uint16 rf_2g, rf_3g, rf_4g;	
//at default there exists 2G in the system
   rf_2g = L1D_GetRF(MML1_RF_2G);
#if defined( __UMTS_RAT__ ) 
#if defined( __UMTS_TDD128_MODE__ )
   rf_3g = L1D_GetRF(MML1_RF_3G_TDD);	
#else
   rf_3g = L1D_GetRF(MML1_RF_3G_FDD);	
#endif   
#else   
   rf_3g = 0xF;	 
#endif
#if defined( __VOLTE_SUPPORT__ ) 
   rf_4g = L1D_GetRF(MML1_RF_LTE);	
#else
   rf_4g = 0xF;	
#endif
   rf_info = 0xF000;	
   rf_info = rf_2g | (rf_3g<<4) | (rf_4g<<8);
	kal_trace(TRACE_INFO, SPC_NOTIFY_RF_INFO, rf_info, rf_2g, rf_3g, rf_4g);
	SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_RF_INFO_NOTIFY, (kal_uint16)rf_info, 0);
}

//AUDL task
void spc_notify_network_status(void)
{
	kal_bool isWB = AM_IsSpeechWB();
	uint32 RAT = AM_GetNetworkRate();
	
	uint16 tmp = (kal_uint16)((isWB & 0x1) << 3 | (RAT & 0x7));
	kal_trace(TRACE_INFO, SPC_NOTIFY_NETWORK_STATUS, tmp & 0xFF, isWB, RAT);
	SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_NETWORK_STATUS_NOTIFY, (kal_uint16)tmp, 0);
}

//AUDIO task
void spc_send_network_status_notify(void)
{
	 SpcIO_MsgQueuePut(SPC_ID_NETWORK_STATUS_NOTIFY, 0, 0, 2);
}

#endif // temp for c2k 

/**
	@isGen: true for trun on, false for turn off
*/
void spc_SpeechOnForCallHold(bool isGen)
{

	
	if(isGen){ // turn on

		if(false == spc_mainAppOnCheck(SPC_APP_HOLD_USAGE, false))
			return;

		// turn on speech and mute
		SIDETONE_SetSolutionVer(SMART_PHONE_SIDETONE_SOLUTION_VER);
		L1SP_Speech_On(RAT_3G_MODE);
		SP_MuteUlFromDiffPos(true, SP_MIC_MUTE_POS_FROM_SPC);//L1SP_MuteMicrophone(true);

		SET_SPC_APP_USAGE(SPC_APP_HOLD_USAGE);
	} else { // turn off
	
		if(!IS_SPC_APP_USED(SPC_APP_HOLD_USAGE)) { // sid generation not in use
			// just leave the log and return
			//done TODO: c2k [trace] kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_HOLD_USAGE, SPC_APP_HOLD_USAGE, 0);			
			MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNUSED_APP_OFF, SPC_APP_HOLD_USAGE, MON_CONTINUE);
			return;
		}

		// turn off 
		SP_MuteUlFromDiffPos(false, SP_MIC_MUTE_POS_FROM_SPC); // L1SP_MuteMicrophone(false);
		L1SP_Speech_Off();
		SIDETONE_ResetSolutionVer();
		CLR_SPC_APP_USAGE(SPC_APP_HOLD_USAGE);
	}
	
}

// ----------------------------------------------------------------------------
// Loopback
// ----------------------------------------------------------------------------


// static uint16 gSpc_acLoopback_PCM_UL_BUF[ACLOOPBACK_FRAME_BUF_NO][ACLOOPBACK_NB_FRAME_SIZE];


	// uint32 UL_tmp_w;
	// uint32 UL_tmp_r;





	/*	P2W */
	
	/* P4W UL with delay
	if( (gSpc_acLoopback.UL_tmp_w - gSpc_acLoopback.UL_tmp_r) < ACLOOPBACK_FRAME_BUF_NO ){        
      PCM4WAY_GetFromMic((uint16*)gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)]);
      kal_dev_trace( TRACE_INFO,SPC_AC_LOOPBACK_FROM_MIC, 
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][0],
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][1],
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][2],
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][3]);         
      (gSpc_acLoopback.UL_tmp_w)++;            
   }else{
      kal_dev_trace( TRACE_INFO, SPC_AC_LOOPBACK_SKIP_MIC); 
   }

   if( (gSpc_acLoopback.UL_tmp_w - gSpc_acLoopback.UL_tmp_r) >= gSpc_acLoopback.delay ){
      PCM4WAY_PutToSE(gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)]);   
      kal_dev_trace( TRACE_INFO, SPC_AC_LOOPBACK_TO_SE, 
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][0],
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][1],
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][2],
                       gSpc_acLoopback_PCM_UL_BUF[gSpc_acLoopback.UL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][3]);               
      (gSpc_acLoopback.UL_tmp_r)++;
   }else{
      kal_dev_trace( TRACE_INFO, SPC_AC_LOOPBACK_FILL_SE); 
      PCM4WAY_FillSE(0);
   }
   */

	// P4W UL without delay	
	
   

	/* P2W */
	


   


		  
		  
	


		// gSpc_acLoopback.DL_tmp_w, gSpc_acLoopback.DL_tmp_r, gSpc_acLoopback.UL_tmp_w, gSpc_acLoopback.UL_tmp_r);
	

	
 



/**
	@uParam:
		Bit 0: 1==Enable/0==Disable
		Bit 1: Pre-condiftion is speech mode equals to SPH_MODE_BT_EARPHONE(3) or SPH_MODE_BT_EARPHONE_WO_NREC(4) or SPH_MODE_BT_CCARITE(5). 
		         1== BT Loopback with BT codec / 0 == BT Loopback without BT Codec. 
		         Only support (MT6572/MT6582/MT6592/MT6571/MT6572)
		Bit 2: delay setting for normal loopback, i.e. speech mode is not BT cases. 0==Use modem default delay value/ 1== use AP gives delay value in msgId32 bit[0:7] 
	@extraParam:
		Bit[7:0]: Delay time in uint8. Unit is 20ms. Take effect when msgId16 bit[2] == 1. For example: when bit[7:0] = 0xf, then the delay time is 15*20 == 300 ms.
*/


		// check status
		/*
		if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)){
			// just leave log and return
			kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_ACLOOPBACK_USAGE, SPC_APP_ACLOOPBACK_USAGE, 1);
			return; 
		}
		*/

			// just leave log and return

		/*
		if(IS_SPC_APP_USED(SPC_APP_HOLD_USAGE)){
			kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_ACLOOPBACK_USAGE, SPC_APP_ACLOOPBACK_USAGE, 3);		

			// [TODO] REMOVE
			ASSERT(0);
			
			return;
		}
		*/

		// special case for BT loopback 

		// setup 
		// gSpc_acLoopback.UL_tmp_w = 0;
		// gSpc_acLoopback.UL_tmp_r = 0;

		//clean memory
      // memset(gSpc_acLoopback_PCM_UL_BUF, 0, sizeof(uint16)*ACLOOPBACK_FRAME_BUF_NO*ACLOOPBACK_NB_FRAME_SIZE);

		/* P2W */
		// enable

		


			// just leave log and return


		
		/* P2W */ 
		// SetSpeechEnhancement(false);
	
      // Extended_PCM4WAY_Stop(P4W_APP_TYPE_UNDER_CALL);
		
		
      
	

// ----------------------------------------------------------------------------
// Speech on for Hosted Dongle
// ----------------------------------------------------------------------------


	


/**
	MD -> AP, Run under AUDL/MED
*/

	

	
		//clean up the read waiting flag when send fail to prevent blocking. 

	


/**
	MD -> AP, Run under AUDL/MED
*/

	







	//header checking
   
	


   // speaker buffer comes from SD

   // Transfer to AUDL
   // Send notification
	



   // mic buffer put to SE. 


   // Transfer to AUDL
   // Send notification
	

/**
	@RAT_Mode: 0 for 2G, 1 for 3G, 2 for 3G324
	@enable: true for daca speech on, false for daca speech off
*/
	
		// status checking
		/*
		if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) {
			kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_HOSTED_USAGE, SPC_APP_HOSTED_USAGE, 1);

			ASSERT(0);

			return;
		} else if (IS_SPC_APP_USED(SPC_APP_HOLD_USAGE)){
			kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_HOSTED_USAGE, SPC_APP_HOSTED_USAGE, 2);

			ASSERT(0);

			return;
		}
		*/
			

		// disable speech enhancment, due to path is too long, enhancment is meaningless

		// clean up setting 
		
		// turn on 

		

		// status checking
			// just leave log and return
	
		// turn off

		// speech enhancment back to normal 

// ----------------------------------------------------------------------------
// DSP PCM ROUTER
// ----------------------------------------------------------------------------

		// enhacement check 
		// status checking
		/*
		if(!IS_SPC_ID_SPEECH_CUSTOM_DATA_REQUEST_DONE) {
			kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_ON_OFF_BEHAVIOR, SPC_APP_PCMROUTER_USAGE, SPC_APP_PCMROUTER_USAGE, 1, SPC_APP_MAX);
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, SPC_APP_PCMROUTER_USAGE, SPC_APP_ON_CHECK_CASES_B4_EM_REQ_DONE);
		}
		*/
		

		//set solution ver before application start.
		
		// application enable
		

		// status checking
			

		

		// application disable		

		// reset the solution ver after application stop.
	

// ----------------------------------------------------------------------------
// RECORD
// ----------------------------------------------------------------------------

typedef enum
{
	SPC_REC_FORMAT_UNDEF = 0,
	SPC_REC_FORMAT_PCM,
	SPC_REC_FORMAT_VM,
} SPC_REC_FORMAT;

typedef enum
{
	SPC_REC_SAMPLE_RATE_IDX_8K = 0,
	SPC_REC_SAMPLE_RATE_IDX_16K,
	
} SPC_REC_SAMPLE_RATE_IDX;

/*
typedef struct 
{
	uint8    format; //0 undefined, 1: pcm, 2:VM, ref to SPC_REC_FORMAT
	uint8    samplingRateIdx; //0: 8k, 1: 16k, ref to SPC_REC_SAMPLE_RATE_IDX, used in SPC_REC_FORMAT_PCM
	uint8    channelNum; //0: undefined; 1: mono (1ch); 2: stereo (2ch), used in SPC_REC_FORMAT_PCM

	bool     isRecordDataWaiting;
	
} _SPC_RECORD_T;

static _SPC_RECORD_T gSpc_Record;
*/

typedef struct 
{
	uint8    samplingRateIdx; //0: 8k, 1: 16k, ref to SPC_REC_SAMPLE_RATE_IDX, used in SPC_REC_FORMAT_PCM
	uint8    channelNum; //0: undefined; 1: mono (1ch); 2: stereo (2ch), used in SPC_REC_FORMAT_PCM

	bool     isRecordDataWaiting;
	
} _SPC_PCM_RECORD_T;

typedef struct 
{
	bool     isRecordDataWaiting;
	
} _SPC_VM_RECORD_T;

typedef struct 
{
	bool     isRecordDataWaiting;
} _SPC_RAW_PCM_RECORD_T;

static _SPC_PCM_RECORD_T gSpc_PcmRecord;
static _SPC_VM_RECORD_T gSpc_VmRecord;
static _SPC_RAW_PCM_RECORD_T gSpc_RawPcmRecord;
static SPC_REC_FORMAT gSpc_Record_format; // only use under 
/*
static kal_uint8 tempMicDataCnt = 0;
static kal_uint16 tempMicData[320]
	// = { 0x4808,0x85, 0x156,0, 0x4,0, 0x146,0x9000, 
	= { 
	0xfff4,0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 
	0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 
	0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 
	0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a78, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa576, 0xfff3, 0x5a79, 0x7fff, 0x5a8b, 
   0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000c, 0xa588, 
   0x8001, 0xa576, 0xfff4, 0x5a79, 0x7fff, 0x5a8b, 0x000d, 0xa587, 0x8001, 0xa575, 	

	0xfff4,0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 
	0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 
	0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 
	0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a78, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa576, 0xfff3, 0x5a79, 0x7fff, 0x5a8b, 
   0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000c, 0xa588, 
   0x8001, 0xa576, 0xfff4, 0x5a79, 0x7fff, 0x5a8b, 0x000d, 0xa587, 0x8001, 0xa575, 

	0xfff4,0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 
	0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 
	0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 
	0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a78, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa576, 0xfff3, 0x5a79, 0x7fff, 0x5a8b, 
   0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000c, 0xa588, 
   0x8001, 0xa576, 0xfff4, 0x5a79, 0x7fff, 0x5a8b, 0x000d, 0xa587, 0x8001, 0xa575, 
   
	0xfff4,0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 
	0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 
	0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 
	0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a78, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa576, 0xfff3, 0x5a79, 0x7fff, 0x5a8b, 
   0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000c, 0xa588, 
   0x8001, 0xa576, 0xfff4, 0x5a79, 0x7fff, 0x5a8b, 0x000d, 0xa587, 0x8001, 0xa575 	

};
*/


void spc_record_sendMicDataDone(void)
{
	
	if(!IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)){		
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 2);
		return;
	}

	if(SPC_REC_FORMAT_PCM == gSpc_Record_format){
		gSpc_PcmRecord.isRecordDataWaiting = false;
	} else if(SPC_REC_FORMAT_VM == gSpc_Record_format){
		gSpc_VmRecord.isRecordDataWaiting = false;
	} else {
		ASSERT(0, VALSPH_ERR_FORCE_ASSERT, SPC_APP_RECORD_USAGE); // un-know format
	}
}

void spc_pcmRec_sendMicDataDone(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE)){		
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCM_REC_USAGE, SPC_APP_PCM_REC_USAGE, 1);
		return;
	}

	gSpc_PcmRecord.isRecordDataWaiting = false;
}



/**
	run under AUDL/MED, when process message "SPC_ID_AUDIO_STRM_PCMREC_DATA_NOTIFY"
*/
void spc_record_sendPcmData(void)
{
	uint32 add1, add2;
	uint16 len1, len2; 
	spcBufInfo info;
	bool sendResult = true; 

	if((!IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)) && 
		(!IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE))){ // prevent pcm data sending to AP after record off
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 4);
		return;
	}

	// get data, length is word!!
	PcmRec_GetMicDataBufs(&add1, &len1, &add2, &len2);

	// kal_trace(TRACE_INFO, PCMREC_GET_DATA_BUFS, add1, len1, add2, len2);
	
	// change the len to unit of byte 
	len1 <<=1; 
	len2 <<=1; 
	
	if(len1 == 0){
		gSpc_PcmRecord.isRecordDataWaiting = false;
		//kal_trace(TRACE_INFO, PCMREC_GET_EMPTY_DATA);
		
	} else {
		info.syncWord = 0x2A2A;
		info.type = AUD_CCCI_STRMBUF_TYPE_PCM_TYPE; 
		info.length = len1 + len2;

		if(IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE)){ // new from MT6582
			sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
				 (void *)add1, len1, (void *)add2, len2,
				 SPCIO_MSG_FROM_SPC_PCM_REC_DATA_NOTIFY);
		} else { // original
			sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
				 (void *)add1, len1, (void *)add2, len2,
				 SPCIO_MSG_FROM_SPC_REC_DATA_NOTIFY);
		}		
		
		
		/* Testing code 
		info.syncWord = 0x2A2A;
		info.type = AUD_CCCI_STRMBUF_TYPE_PCM_TYPE; 
		info.length = 320;
		
		kal_prompt_trace(MOD_L1SP, "prints: %x, %d, %x, %d", add1, len1, add2, len2);
		
		if(IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE)){ // new from MT6582
			SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
				 tempMicData, 160*sizeof(kal_uint16), NULL, 0,
				 SPCIO_MSG_FROM_SPC_PCM_REC_DATA_NOTIFY);
		} else { // original
			sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
				 tempMicData, 160*sizeof(kal_uint16), NULL, 0,
				 SPCIO_MSG_FROM_SPC_REC_DATA_NOTIFY);
		}
		End Testing code */

		if(sendResult == false) {
			gSpc_PcmRecord.isRecordDataWaiting = false;
			//kal_trace( TRACE_INFO, SPC_APP_DATA_SEND_FAIL, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 0);
		}
	}
}

void spc_RawPcmRec_sendDataDone(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE)){		
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RAW_PCM_REC_USAGE, SPC_APP_RAW_PCM_REC_USAGE, 1);
		return;
	}

	gSpc_RawPcmRecord.isRecordDataWaiting = false;
}

/**
	run under AUDL/MED, when process message "SPC_ID_AUDIO_STRM_RAWPCMREC_DATA_NOTIFY"
*/
void spc_record_sendRawPcmData(void)
{
	uint32 add1, add2;
	uint16 len1, len2; 
	spcBufInfo info;
	bool sendResult = true; 

	if( (!IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE)) ){ // prevent pcm data sending to AP after record off
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 4);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_RAW_PCM_REC_USAGE, MON_CONTINUE);
		
		return;
	}

	RawPcmRec_GetMicDataBufs(&add1, &len1, &add2, &len2);

	//kal_trace(TRACE_INFO, PCMREC_GET_DATA_BUFS, add1, len1, add2, len2);
	
	// change the len to unit of byte 
	len1 <<=1; 
	len2 <<=1; 
	
	if(len1 == 0){
		gSpc_RawPcmRecord.isRecordDataWaiting = false;
		//kal_trace(TRACE_INFO, PCMREC_GET_EMPTY_DATA);
	} else {
		info.syncWord = 0x2A2A;
		info.type = AUD_CCCI_STRMBUF_TYPE_RAW_PCM_TYPE; 
		info.length = len1 + len2;

        sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
             (void *)add1, len1, (void *)add2, len2,
             SPCIO_MSG_FROM_SPC_RAW_PCM_REC_DATA_NOTIFY);
		
		if(sendResult == false) {
            gSpc_RawPcmRecord.isRecordDataWaiting = false;
			//kal_trace( TRACE_INFO, SPC_APP_DATA_SEND_FAIL, SPC_APP_RAW_PCM_REC_USAGE, SPC_APP_RAW_PCM_REC_USAGE, 0);
		}
	}
}


/**
	Send notification to AUDL/MED, prepare to get the recording data
*/
void spc_pcmRec_handler(void)
{
	if( gSpc_PcmRecord.isRecordDataWaiting == true){
		//kal_dev_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 0);
		return;
	}
	
   // Send notification to AUDL to begin data transfer
	SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_PCMREC_DATA_NOTIFY, 0, 0, 2);
	
   gSpc_PcmRecord.isRecordDataWaiting = true;
	
	return;
}

void spc_pcmRec_On(uint8 sampleRateIdx, uint8 channelNumIdx)
{
	gSpc_PcmRecord.samplingRateIdx = sampleRateIdx;
	gSpc_PcmRecord.channelNum = channelNumIdx+1; 

	// PCM Record  
	gSpc_PcmRecord.isRecordDataWaiting = false;

	SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_PCMRECORD, true);

	PCMREC_Start(spc_pcmRec_handler, gSpc_PcmRecord.channelNum, (PCM_REC_SAMPLE_RATE_IDX)sampleRateIdx );	
}

void spc_pcmRec_Off(void)
{
	PCMREC_Stop();

	SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_PCMRECORD, false);

	//clean up all inforamtions
	gSpc_PcmRecord.samplingRateIdx = PCM_REC_SAMPLE_RATE_IDX_UNDEF;
	gSpc_PcmRecord.channelNum = 0;
}


/**
	Send notification to AUDL/MED, prepare to get the recording data
*/
void spc_RawPcmRec_handler(void)
{
	if( gSpc_RawPcmRecord.isRecordDataWaiting == true){
		//kal_dev_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_RAW_PCM_REC_USAGE, SPC_APP_RAW_PCM_REC_USAGE, 0);
		return;
	}
	
	// Send notification to AUDL to begin data transfer
	SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_RAWPCMREC_DATA_NOTIFY, 0, 0, 2);
	
	gSpc_RawPcmRecord.isRecordDataWaiting = true;
	
	return;
}

void spc_RawPcmRec_On(kal_uint8 ULChannelNumIdx)
{
	// RAW PCM Record  
	gSpc_RawPcmRecord.isRecordDataWaiting = false;

	SP_SetForcedUnMuteULController(L1SP_FORCEDUNMUTE_RAWPCMRECORD, true);


	RAWPCMREC_Start(spc_RawPcmRec_handler, ULChannelNumIdx);	
}

void spc_RawPcmRec_Off(void)
{
	RAWPCMREC_Stop();

	SP_SetForcedUnMuteULController(L1SP_FORCEDUNMUTE_RAWPCMRECORD, false);

}

bool spc_RawPcmRec_getDataWaiting(void)
{
    return(gSpc_RawPcmRecord.isRecordDataWaiting);
}



void spc_vmRec_sendMicDataDone(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE)){		
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_VM_REC_USAGE, SPC_APP_VM_REC_USAGE, 1);
		return;
	}

	gSpc_VmRecord.isRecordDataWaiting = false;
}


/**
	run under AUDL/MED, when process message "SPC_ID_AUDIO_STRM_VMREC_DATA_NOTIFY"
*/
void spc_record_sendVmData(void)
{
	
	uint32 add1, add2;
	uint16 len1, len2; 
	uint16 totalLenInWord;
	spcBufInfo info;
	bool sendResult = true; 

	if((!IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)) && (!IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE))){ // prevent sening vm data to AP after record off
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_VM_INT_WITHOUT_ON, gSpc.spcAppUsage, MON_CONTINUE);
		return;
	}
	
	// get data, length is word!!
	VmRec_GetReadBufs(&add1, &len1, &add2, &len2);
	totalLenInWord = len1+len2;

	// MonTrace(MON_CP_HWD_SPH_VM_DATA_PROC_TRACE_ID, 5, VMREC_DATA_PROC_POS_SEND2AP, add1, len1, add2, len2); // length unit is word 
	
	// change the len to unit of byte 
	len1 <<= 1;
	len2 <<= 1;
	
	if(len1 == 0){
		gSpc_VmRecord.isRecordDataWaiting = false;		
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_VM_NO_DATA, len1, MON_CONTINUE);
		
	} else {
		info.syncWord = 0x2A2A;
		info.type = AUD_CCCI_STRMBUF_TYPE_VM_TYPE; 
		info.length = len1 + len2; 

		if(IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE)){
			sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
				(void *)add1, len1, (void *)add2, len2,
				SPCIO_MSG_FROM_SPC_VM_REC_DATA_NOTIFY);
		} else { // original version
			sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
				 (void *)add1, len1, (void *)add2, len2,
				 SPCIO_MSG_FROM_SPC_REC_DATA_NOTIFY);
		}

		VmRec_ReadDataDone(totalLenInWord);
		
		if(sendResult == false) {
			gSpc_VmRecord.isRecordDataWaiting = false;
			MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_VM_SEND_DATA_FAIL, totalLenInWord, MON_CONTINUE);
		}
	}
}

void spc_vmRec_handler(void)
{
	if( gSpc_VmRecord.isRecordDataWaiting == true){
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_VM_DROP_DATA, gSpc_VmRecord.isRecordDataWaiting, MON_CONTINUE);
		return;
	}

	SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_VMREC_DATA_NOTIFY, 0, 0, 2);
	
   gSpc_VmRecord.isRecordDataWaiting = true;
	
	return;
}

void spc_vmRec_On(void)
{
	gSpc_VmRecord.isRecordDataWaiting = false;

	VMREC_Start(spc_vmRec_handler, 0, false);
}

void spc_vmRec_Off(void)
{
	VMREC_Stop(false);
}

// ---- Entry Points and Checking -----

/**
	@return: true for pass checking, flase for fail checking
*/
bool spc_recordStartCheck(void)
{
	if(!IS_SPC_ID_SPEECH_CUSTOM_DATA_REQUEST_DONE){ // prevent record on before EM data sending
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 0);
		MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, SPC_APP_RECORD_USAGE, SPC_APP_ON_CHECK_CASES_B4_EM_REQ_DONE);
		return false;
	}

	if(IS_SPC_APP_USED(SPC_APP_HOLD_USAGE)){ // under SID generation state, record function is not provided
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR_CHECK, SPC_APP_RECORD_USAGE, SPC_APP_HOLD_USAGE, SPC_APP_HOLD_USAGE, gSpc.spcAppUsage);		

		ASSERT(0, VALSPH_ERR_SPC_OPEN_SUB_APP_UNDER_CALL_HOLD, SPC_APP_RECORD_USAGE);		
		return false;
	}

	// system is under idle state, we do not provide record function. 
	if((!IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) 
		&& (!IS_SPC_APP_USED(SPC_APP_ACLOOPBACK_USAGE))
		&& (!IS_SPC_APP_USED(SPC_APP_CTM_USAGE))
		&& (!IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE))
		&& (!IS_SPC_APP_USED(SPC_APP_PCMROUTER_USAGE))
		){ 
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 3);				
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_OPEN_SUB_APP_WO_MAIN_APP, SPC_APP_RECORD_USAGE, MON_CONTINUE);		

		return false;
	}

	// defualt
	return true;
}



/**
	Open PCM record. This can concurrency with VM record (spc_VmRecordStart)
	Only support speech on record!! (Speech on for daca is NOT support)
	
	@sampleRateIdx: only use when PCM record. 0: 8k, 1: 16k, ref to SPC_REC_SAMPLE_RATE_IDX, used in SPC_REC_FORMAT_PCM
	@channelNumIdx: only use when PCM record. 0: 1 channel; 1: 2 channel. 
*/
void spc_PcmRecordStart(uint8 sampleRateIdx, uint8 channelNumIdx)
{
	if(true!= spc_recordStartCheck()){
		return;
	}

	if(IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE)) // PCM record is not allow on DACA
	{
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCM_REC_USAGE, SPC_APP_PCM_REC_USAGE, 4);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_HOSTED_USAGE, MON_CONTINUE);
		ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_PCM_REC_USAGE);		
		return;
	}

	// prevent re-entry
	if(IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE)){ 
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCM_REC_USAGE, SPC_APP_PCM_REC_USAGE, 2);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_PCM_REC_USAGE, MON_CONTINUE);
		return;
	}

	// It is illegle to use different formats to open record, so force assert.
	if((IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)) || (IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE))){ 
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCM_REC_USAGE, SPC_APP_PCM_REC_USAGE, 3);
		ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_RECORD_USAGE);
		return;
	}
	
	spc_pcmRec_On(sampleRateIdx, channelNumIdx);
	SET_SPC_APP_USAGE(SPC_APP_PCM_REC_USAGE);
}


void spc_PcmRecordStop(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE)){
		// just leave log and return
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCM_REC_USAGE, SPC_APP_PCM_REC_USAGE, 0);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNUSED_APP_OFF, SPC_APP_PCM_REC_USAGE, MON_CONTINUE);
		return; 
	}
	
	spc_pcmRec_Off();
	CLR_SPC_APP_USAGE(SPC_APP_PCM_REC_USAGE);
}

/**
	Open vm logging to AP side. This can concurrency with PCM record (spc_PcmRecordStart)
*/
void spc_VmRecordStart(void)
{
	if(true!= spc_recordStartCheck()){
		return;
	}

	// prevent re-entry
	if(IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE)){ 
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_VM_REC_USAGE, MON_CONTINUE);
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_VM_REC_USAGE, SPC_APP_VM_REC_USAGE, 2);
		return;
	}

	// It is illegle to use different formats to open record, so force assert.
	if(IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)){ 
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_VM_REC_USAGE, SPC_APP_VM_REC_USAGE, 3);
		ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_VM_REC_USAGE);
		return;
	}
	
	spc_vmRec_On();

	SET_SPC_APP_USAGE(SPC_APP_VM_REC_USAGE);
}

/**
	Pair with spc_VmRecordStart(), to close vm logging.
*/
void spc_VmRecordStop(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE)){
		// just leave log and return
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_VM_REC_USAGE, SPC_APP_VM_REC_USAGE, 1);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNUSED_APP_OFF, SPC_APP_VM_REC_USAGE, MON_CONTINUE);
		
		return; 
	} 
	
	spc_vmRec_Off();
	CLR_SPC_APP_USAGE(SPC_APP_VM_REC_USAGE);
}


/**
	Open RAW PCM record. This can concurrency with VM record (spc_VmRecordStart)
	Only support speech on record!! (Speech on for daca is NOT support)
*/
void spc_RawPcmRecordStart(uint8 ULChannelNumIdx)
{
	if(true != spc_recordStartCheck()){
		return;
	}

	if(IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE)) // PCM record is not allow on DACA
	{
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_HOSTED_USAGE, MON_CONTINUE);
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RAW_PCM_REC_USAGE, SPC_APP_RAW_PCM_REC_USAGE, 4);
		ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_RAW_PCM_REC_USAGE);
		return;
	}

	// prevent re-entry
	if(IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE)){ 
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_RAW_PCM_REC_USAGE, MON_CONTINUE);
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RAW_PCM_REC_USAGE, SPC_APP_RAW_PCM_REC_USAGE, 2);
		return;
	}

	// It is illegle to use different formats to open record, so force assert.
	if((IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)) || (IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE))){ 
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RAW_PCM_REC_USAGE, SPC_APP_RAW_PCM_REC_USAGE, 3);
		ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_RAW_PCM_REC_USAGE);
		return;
	}
    SP_SetForcedUnMuteULController(L1SP_FORCEDUNMUTE_RAWPCMRECORD, true);
	spc_RawPcmRec_On(ULChannelNumIdx);
	SET_SPC_APP_USAGE(SPC_APP_RAW_PCM_REC_USAGE);
}


void spc_RawPcmRecordStop(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE)){
		// just leave log and return
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RAW_PCM_REC_USAGE, SPC_APP_RAW_PCM_REC_USAGE, 0);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNUSED_APP_OFF, SPC_APP_RAW_PCM_REC_USAGE, MON_CONTINUE);
		return; 
	}
	
	spc_RawPcmRec_Off();
    SP_SetForcedUnMuteULController(L1SP_FORCEDUNMUTE_RAWPCMRECORD, false);

	CLR_SPC_APP_USAGE(SPC_APP_RAW_PCM_REC_USAGE);
}


/**
	Make VM record and PCM record same API entry but different format
	Only support speech on record!! (Speech on for daca is NOT support)
	@format: 0: PCM, 1: VM
	@sampleRateIdx: only use when PCM record. 0: 8k, 1: 16k, ref to SPC_REC_SAMPLE_RATE_IDX, used in SPC_REC_FORMAT_PCM
	@channelNumIdx: only use when PCM record. 0: 1 channel; 1: 2 channel. 
*/
void spc_RecordStart(uint8 format, uint8 sampleRateIdx, uint8 channelNumIdx)
{

	if(true!= spc_recordStartCheck()){ 
		return;
	}

	if(IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE)){
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_HOSTED_USAGE, MON_CONTINUE);
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 9);
		return;
	}
	
	// prevent re-entry
	if(IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)){ 
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 7);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_PCM_REC_USAGE, MON_CONTINUE);
		return;
	}

	// It is illegle to use different formats to open record, so force assert.
	if((IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE)) || (IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE)) || (IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE))){ 
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 8);
		ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_PCM_REC_USAGE);	
		return;
	}
	
#ifdef CONNECT_WM8904
  		Config_WM8904(2); //Config_WM8904(-1); MIC_8K_LONGSYNC_PHONE_CALL == 2
#endif  
	switch(format){
		case 0: // PCM
		{
			/*
			gSpc_Record.format = SPC_REC_FORMAT_PCM;
			gSpc_Record.samplingRateIdx = sampleRateIdx;
			gSpc_Record.channelNum = channelNumIdx+1; 
			
			// PCM Record  
         gSpc_Record.isRecordDataWaiting = false;

			PCMREC_Start(spc_pcmRec_handler, gSpc_Record.channelNum, (PCM_REC_SAMPLE_RATE_IDX)sampleRateIdx );			
			*/
			gSpc_Record_format = SPC_REC_FORMAT_PCM;
			spc_pcmRec_On(sampleRateIdx, channelNumIdx);
		}
			break;
		case 1: //VM			
			/*
			gSpc_Record.format = SPC_REC_FORMAT_VM;
			gSpc_Record.samplingRateIdx = PCM_REC_SAMPLE_RATE_IDX_UNDEF;
			gSpc_Record.channelNum = 0;

			gSpc_Record.isRecordDataWaiting = false;

			VMREC_Start(spc_vmRec_handler, 0, false);
			*/
			gSpc_Record_format = SPC_REC_FORMAT_VM;
			spc_vmRec_On();
			break;
		/*
		case 2: //Phase out DMNR calibration
			break;
		
#if defined(__CTM_SUPPORT__)
		case 3: //CTM debug
			break;
#endif
		*/
		default: // illegle format. 
			ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_RECORD_USAGE);
	}

	SET_SPC_APP_USAGE(SPC_APP_RECORD_USAGE);
}

/**
	Pair with spc_RecordStart()
*/
void spc_RecordStop()
{
	if(!IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)){
		// just leave log and return
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_RECORD_USAGE, SPC_APP_RECORD_USAGE, 1);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_RECORD_USAGE, MON_CONTINUE);
		return; 
	} 
	
	switch(gSpc_Record_format){
		case SPC_REC_FORMAT_PCM:
			/*
			PCMREC_Stop();

			//clean up all inforamtions
			gSpc_Record.format = SPC_REC_FORMAT_UNDEF;
			gSpc_Record.samplingRateIdx = PCM_REC_SAMPLE_RATE_IDX_UNDEF;
			gSpc_Record.channelNum = 0;
			*/
			spc_pcmRec_Off();
			break;
		case SPC_REC_FORMAT_VM:
			/*
			VMREC_Stop(false);

			gSpc_Record.format = SPC_REC_FORMAT_UNDEF;
			gSpc_Record.samplingRateIdx = PCM_REC_SAMPLE_RATE_IDX_UNDEF;
			gSpc_Record.channelNum = 0;
			*/
			spc_vmRec_Off();
			break;
		default:
			ASSERT(0, VALSPH_ERR_SPC_SUBAPP_RECORD_CONTROL_ILLEGLE, SPC_APP_RECORD_USAGE);
	}

	gSpc_Record_format = SPC_REC_FORMAT_UNDEF;
	CLR_SPC_APP_USAGE(SPC_APP_RECORD_USAGE);

}


// ----------------------------------------------------------------------------
// PCMNWAY
// ----------------------------------------------------------------------------
#define SPC_PCMNWAY_MAX_SIZE_OF_SE_BUF  320 // unit is 16bit(word)
#define SPC_PCMNWAY_MAX_SIZE_OF_SPK_BUF 320 // unit is 16bit(word), max = 16*20
#define SPC_PCMNWAY_MAX_SIZE_OF_MIC_BUF 642 // unit is 16bit(word), max = 320*2 +2(agc use)
#define SPC_PCMNWAY_MAX_SIZE_OF_SD_BUF  320 // unit is 16bit(word)
	
	
#define SPC_PNW_MSG_BUFFER_SE  1 // Bit 0
#define SPC_PNW_MSG_BUFFER_SPK 2 // Bit 1
#define SPC_PNW_MSG_BUFFER_MIC 4 // Bit 2
#define SPC_PNW_MSG_BUFFER_SD  8 // Bit 3
	
#define SPC_PNW_MSG_BIT_BAND   4
	
	typedef enum
	{
	
		SPC_PCMNWAY_APP_TYPE_NONE = 0,
		SPC_PCMNWAY_APP_TYPE_DMNR_CAL_PLAY_REC,
		SPC_PCMNWAY_APP_TYPE_DMNR_CAL_REC_ONLY,
		
		SPC_PCMNWAY_APP_TYPE_UNSET = 0xffff,	
	} SPC_PCMNWAY_APP_TYPE_T;
	
	typedef enum {
	  SPC_PCMNWAY_FORMAT_P2W_NORMAL = 0,
	  SPC_PCMNWAY_FORMAT_P2W_VOIP,
	  SPC_PCMNWAY_FORMAT_P2W_CAL,		   //calibration 
	  SPC_PCMNWAY_FORMAT_P2W_WB_CAL,	   //wb calibration
	  SPC_PCMNWAY_FORMAT_P2W_WB_NORMAL, 	//normal usage for WB p2w
	
	  SPC_PCMNWAY_FORMAT_UNSET = 0xffff,
	}SPC_PCMNWAY_Format;
	
	
	typedef struct _SPC_PCMNWAY_T_
	{
		kal_uint8	 pnwBufInfo; //ref to SPC_PNW_MSG_BUFFER_x
		kal_uint8	 pnwBand; //0: for narrow band, 1: for wide band
		// kal_uint16	pnwAppType;
	
		kal_bool	 pnwIsMicBufWaiting;
		kal_bool	 pnwIsSpkBufWaiting;
		
		kal_uint32	 micBufSize; // unit is byte (8bit)
		kal_uint32	 spkBufSize; // unit is byte (8bit)
		kal_uint32	 seBufSize;
		kal_uint32	 sdBufSize;
	
		
	}_SPC_PCMNWAY_T;
	
	static _SPC_PCMNWAY_T gSpc_Pcmnway;
	static kal_uint16 gSpc_Pcmnway_seBuf[SPC_PCMNWAY_MAX_SIZE_OF_SE_BUF];
	static kal_uint16 gSpc_Pcmnway_spkBuf[SPC_PCMNWAY_MAX_SIZE_OF_SPK_BUF]; 
	static kal_uint16 gSpc_Pcmnway_micBuf[SPC_PCMNWAY_MAX_SIZE_OF_MIC_BUF]; 
	static kal_uint16 gSpc_Pcmnway_sdBuf[SPC_PCMNWAY_MAX_SIZE_OF_SD_BUF];

/*
__attribute__  ((section ("NONCACHEDZI"))) static kal_uint16 tempUlData[179]
	// = { 0x4808,0x85, 0x156,0, 0x4,0, 0x146,0x9000, 
	= { 0,0, 0,0, 0,0, 0,0, 
	1, 2, 3, 4, 5, 6, 7, 8,
	0x2A2A, 0x2, 0x140, 
	0xfff4,0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 
	0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 
	0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 
	0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a78, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa576, 0xfff3, 0x5a79, 0x7fff, 0x5a8b, 
   0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000c, 0xa588, 
   0x8001, 0xa576, 0xfff4, 0x5a79, 0x7fff, 0x5a8b, 0x000d, 0xa587, 0x8001, 0xa575, 	

	0xfff4,0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 
	0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 
	0x8001, 0xa575, 0xfff4, 0x5a7a, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 
	0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a78, 
	0x7fff, 0x5a8a, 0x000d, 0xa587, 0x8001, 0xa576, 0xfff3, 0x5a79, 0x7fff, 0x5a8b, 
   0x000d, 0xa587, 0x8001, 0xa575, 0xfff4, 0x5a79, 0x7fff, 0x5a8a, 0x000c, 0xa588, 
   0x8001, 0xa576, 0xfff4, 0x5a79, 0x7fff, 0x5a8b, 0x000d, 0xa587, 0x8001, 0xa575 	
};
*/

/**
	MD -> AP, Run under AUDL/MED
*/

#if 0 // defined(__ENABLE_SPEECH_DVT__)
		void spc_pcmNWay_sendUlData(uint16 bufSize)
#else // defined(__ENABLE_SPEECH_DVT__)
		void spc_pcmNWay_sendUlData(void)
#endif // defined(__ENABLE_SPEECH_DVT__)
		{
			kal_bool sendResultMic = KAL_TRUE;
			kal_bool sendResultSd = KAL_TRUE;
			spcBufInfo info;
		
			if(!IS_SPC_APP_USED(SPC_APP_PCMNWAY_USAGE)){ // prevent data sending after PCMNWAY is off
				//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 4);
				return;
			}
			
			if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_MIC) {
				info.syncWord = 0x2A2A;
				info.type = AUD_CCCI_STRMBUF_TYPE_PCM_GetFromMic; 
#if 0 //defined(__ENABLE_SPEECH_DVT__)
				info.length = bufSize; // gSpc_Pcmnway.micBufSize; FIXME: wideband
#else // defined(__ENABLE_SPEECH_DVT__)
				info.length = gSpc_Pcmnway.micBufSize;
#endif // defined(__ENABLE_SPEECH_DVT__)
		
				// for debug	
				/*
				sendResultMic = spcIO_sendDataViaCCCI(
					SPCIO_CCCI_MSG_CONSTRCUT_DATA_CMD(MSG_M2A_DATA_NOTIFY_PNW_ULREAD, 326), 
					&(tempUlData[8]), 342);
				*/
		
#if 0 //defined(__ENABLE_SPEECH_DVT__)
				if (P2W_APP_TYPE_UNDER_CALL == gSpc_Pcmnway.pnwAppType)
				{
					sendResultMic = (SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
							gSpc_Pcmnway_micBuf, bufSize, NULL, 0,
							SPCIO_MSG_FROM_SPC_PNW_ULDATA_NOTIFY));
				}
				else
#endif // defined(__ENABLE_SPEECH_DVT__)
				{
					sendResultMic = (SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
						gSpc_Pcmnway_micBuf, gSpc_Pcmnway.micBufSize,  NULL, 0,
						SPCIO_MSG_FROM_SPC_PNW_ULDATA_NOTIFY));
				}
		
			}
			
			if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_SD) {
				info.syncWord = 0x2A2A;
				info.type = AUD_CCCI_STRMBUF_TYPE_PCM_GetfromSD; 
				info.length = gSpc_Pcmnway.sdBufSize;
		
				sendResultSd = (SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
						gSpc_Pcmnway_sdBuf, gSpc_Pcmnway.sdBufSize, NULL, 0,
						SPCIO_MSG_FROM_SPC_PNW_ULDATA_NOTIFY)); 	
			}
			
			if((KAL_TRUE !=  sendResultMic) || (KAL_TRUE !=  sendResultSd)) {
					//clean up the read waiting flag when send fail to prevent blocking. 
					gSpc_Pcmnway.pnwIsMicBufWaiting = KAL_FALSE;
					//kal_trace( TRACE_INFO, SPC_APP_DATA_SEND_FAIL, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 0);
			}	
		}
		
		void spc_pcmNWay_sendUlDataDone(void)
		{
			
			if(!IS_SPC_APP_USED(SPC_APP_PCMNWAY_USAGE)){		
				//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 1);
				return;
			}
			gSpc_Pcmnway.pnwIsMicBufWaiting = KAL_FALSE;
		
		}	
		
		/**
			MD -> AP
		*/
#if defined(__ENABLE_SPEECH_DVT__)
		void spc_pcmNWay_requestDlData(uint16 bufLength)
#else // defined(__ENABLE_SPEECH_DVT__)
		void spc_pcmNWay_requestDlData(void)
#endif // defined(__ENABLE_SPEECH_DVT__)
		{
			kal_int32 sendResult;
		
			if(!IS_SPC_APP_USED(SPC_APP_PCMNWAY_USAGE)){ // prevent data sending after PCMNWAY is off
				//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 5);
				return;
			}
		
#if 0 // defined(__ENABLE_SPEECH_DVT__)
			if (P2W_APP_TYPE_UNDER_CALL == gSpc_Pcmnway.pnwAppType)
			{
				sendResult = SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_PNW_DLDATA_REQUEST, bufLength, 0);
			}
			else
#endif // defined(__ENABLE_SPEECH_DVT__)
			{
				sendResult = SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_PNW_DLDATA_REQUEST, gSpc_Pcmnway.spkBufSize, 0);
			}
			// SPCIO_CCCI_MSG_ORG_CONSTRCUT_CMD(MSG_M2A_DATA_REQUEST_PNW_DLDATA, gSpc_Pcmnway.spkBufSize));
		
			if(sendResult<0){ //clean waiting flag of speaker to prevent message blocking
				gSpc_Pcmnway.pnwIsSpkBufWaiting = KAL_FALSE;
				//kal_trace( TRACE_INFO, SPC_APP_DATA_SEND_FAIL, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 1);
			}
		}
		
		void spc_pcmNWay_writeDlDataDone(kal_uint16 offset, kal_int16 length)
		{
		
			spcBufInfo info;	
			kal_uint16 curOffSet;
		
			if(!IS_SPC_APP_USED(SPC_APP_PCMNWAY_USAGE)){		
				//kal_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 5);
				return;
			}
		
			ASSERT(length>=sizeof(spcBufInfo),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, length);
			curOffSet = SpcIo_GetDataFromAp(offset, sizeof(spcBufInfo), &info);
		
		
			//header checking
		   ASSERT(info.syncWord == 0xA2A2, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord);
		   ASSERT( (info.type == AUD_CCCI_STRMBUF_TYPE_PCM_FillSE) || (info.type == AUD_CCCI_STRMBUF_TYPE_PCM_FillSpk), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type );
		   ASSERT(info.length == (length - sizeof(spcBufInfo)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);
	
			if(info.length >0){
				if (info.type == AUD_CCCI_STRMBUF_TYPE_PCM_FillSE) { //SE
					ASSERT(info.length == gSpc_Pcmnway.seBufSize, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);
					SpcIo_GetDataFromAp(curOffSet, gSpc_Pcmnway.seBufSize, gSpc_Pcmnway_seBuf);
				} else { //SPK
#if 0 //defined(__ENABLE_SPEECH_DVT__)
					// ASSERT(info.length == gSpc_Pcmnway.spkBufSize);
					SpcIo_GetDataFromAp(curOffSet, info.length, gSpc_Pcmnway_spkBuf);
#else // defined(__ENABLE_SPEECH_DVT__)
					ASSERT(info.length == gSpc_Pcmnway.spkBufSize, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);
					SpcIo_GetDataFromAp(curOffSet, gSpc_Pcmnway.spkBufSize, gSpc_Pcmnway_spkBuf);
#endif // defined(__ENABLE_SPEECH_DVT__)
				}
			}
			
			gSpc_Pcmnway.pnwIsSpkBufWaiting = KAL_FALSE;
		}
		
		
		void spc_pcm4way_handler( void )
		{
			// spc_pcm4wayDl_handler();
			if( gSpc_Pcmnway.pnwIsSpkBufWaiting == KAL_TRUE){
				//kal_dev_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 4);
				return;
			}		
		
			// SE buffer
			if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_SE) {
			  gSpc_Pcmnway.seBufSize = ((PCM4WAY_PutToSE((const uint16*)gSpc_Pcmnway_seBuf)) << 1);
		   }
			
		   // Speaker buffer
		   if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_SPK) {
			  gSpc_Pcmnway.spkBufSize = ((PCM4WAY_PutToSpk((const uint16*)gSpc_Pcmnway_spkBuf)) << 1);
		   }
		
		   // Transfer to AUDL
		   // Send notification
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_PNW_DL_DATA_REQUEST, 0, 0, 2);
			
		   gSpc_Pcmnway.pnwIsSpkBufWaiting = KAL_TRUE;
		
			// --------------------------------------------------------------
			// spc_pcm4wayUl_handler();
				if( gSpc_Pcmnway.pnwIsMicBufWaiting == KAL_TRUE){
				//kal_dev_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 3);
				return;
			}		
		
			
		   // Microphone buffer
		   if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_MIC) {
			  gSpc_Pcmnway.micBufSize = ((PCM4WAY_GetFromMic((uint16*)gSpc_Pcmnway_micBuf))<<1);
		   }
		
			// SD buffer
			if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_SD) {
			  gSpc_Pcmnway.sdBufSize = ((PCM4WAY_GetFromSD((uint16*)gSpc_Pcmnway_sdBuf))<<1);
		   }
			
		   // Transfer to AUDL
		   // Send notification
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_PNW_UL_DATA_NOTIFY, 0, 0, 2);
			
		   gSpc_Pcmnway.pnwIsMicBufWaiting = KAL_TRUE;
		}
		
		void spc_pcm2wayUl_handler( void )
		{
#if defined(__ENABLE_SPEECH_DVT__)
			kal_uint16 bufLen; 
#endif // defined(__ENABLE_SPEECH_DVT__)
		
			if( gSpc_Pcmnway.pnwIsMicBufWaiting == KAL_TRUE){
				//kal_dev_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 1);
				return;
			}		
		
		   // Microphone buffer
		   if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_MIC) {
#if 0 //defined(__ENABLE_SPEECH_DVT__)
			  bufLen = PCM2WAY_GetFromMic((uint16*)gSpc_Pcmnway_micBuf);
#else // defined(__ENABLE_SPEECH_DVT__)
			  gSpc_Pcmnway.micBufSize = ((PCM2WAY_GetFromMic((uint16*)gSpc_Pcmnway_micBuf))<<1);
#endif // defined(__ENABLE_SPEECH_DVT__)
		   }
		
		   // Transfer to AUDL
		   // Send notification
#if 0 // defined(__ENABLE_SPEECH_DVT__)
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_PNW_UL_DATA_NOTIFY, (bufLen<<1), 0, 2);
#else // defined(__ENABLE_SPEECH_DVT__)
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_PNW_UL_DATA_NOTIFY, 0, 0, 2);
#endif // defined(__ENABLE_SPEECH_DVT__)
			
		   gSpc_Pcmnway.pnwIsMicBufWaiting = KAL_TRUE;
		}
		
		void spc_pcm2wayDl_handler( void )
		{
#if 0 //defined(__ENABLE_SPEECH_DVT__)
			kal_uint16 bufLen; 
#endif // defined(__ENABLE_SPEECH_DVT__)
		
			if( gSpc_Pcmnway.pnwIsSpkBufWaiting == KAL_TRUE){
				//kal_dev_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 2);
				return;
			}		
		
		   // Speaker buffer
		   if (gSpc_Pcmnway.pnwBufInfo & SPC_PNW_MSG_BUFFER_SPK) {
#if 0 //defined(__ENABLE_SPEECH_DVT__)
			  bufLen = PCM2WAY_PutToSpk((const uint16*)gSpc_Pcmnway_spkBuf);
#else // defined(__ENABLE_SPEECH_DVT__)
			  gSpc_Pcmnway.spkBufSize = ((PCM2WAY_PutToSpk((const uint16*)gSpc_Pcmnway_spkBuf))<<1);
#endif // defined(__ENABLE_SPEECH_DVT__)
		   }
		
		
		   // Transfer to AUDL
		   // Send notification
#if 0 // defined(__ENABLE_SPEECH_DVT__)
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_PNW_DL_DATA_REQUEST, (bufLen<<1), 0, 2);
#else // defined(__ENABLE_SPEECH_DVT__)
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_PNW_DL_DATA_REQUEST, 0, 0, 2);
#endif // defined(__ENABLE_SPEECH_DVT__)
			
		   gSpc_Pcmnway.pnwIsSpkBufWaiting = KAL_TRUE;
		}
		
		
		void spc_pcm2way_handler( void )
		{
			spc_pcm2wayDl_handler();
			spc_pcm2wayUl_handler();
		}
		
		
		/**
			@param: 
				[0:3] pcmnway buffer infor, please ref	SPC_PNW_MSG_BUFFER_x
				[4]: band information: 0 narrow band, 1 wide band, 
			@format: please reference SPC_PCMNWAY_Format. If its value is 0xFFFF(undefine), we decide it by band information. 
			@appType: application type using in PCMNWAY.  If its value is 0xFFFF(undefine), we will choose by ourself
		*/
		void spc_PcmNWayStart(kal_uint32 param, SPC_PCMNWAY_Format format, SPC_PCMNWAY_APP_TYPE_T appType)
		{
			uint32 cfgUL1 , cfgUL2, cfgUL3, cfgUL4, cfgDL;
			
			if(IS_SPC_APP_USED(SPC_APP_HOLD_USAGE) || IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE)){
				//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 3);									
				ASSERT(0, VALSPH_ERR_SPC_OPEN_SUB_APP_UNDER_CALL_HOLD, SPC_APP_PCMNWAY_USAGE);
				return;
			}
		
		
			gSpc_Pcmnway.pnwBufInfo = (param & 0xf); // i.e.  param & (SPC_PNW_MSG_BUFFER_SE|SPC_PNW_MSG_BUFFER_SPK|SPC_PNW_MSG_BUFFER_MIC|SPC_PNW_MSG_BUFFER_SD)
			gSpc_Pcmnway.pnwBand = ((param >> SPC_PNW_MSG_BIT_BAND) & 0x1);
		
			ASSERT(gSpc_Pcmnway.pnwBufInfo != 0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, SPC_APP_PCMNWAY_USAGE);

			
			// buffer rest						 
			memset( (kal_uint8 *)gSpc_Pcmnway_seBuf, 0, SPC_PCMNWAY_MAX_SIZE_OF_SE_BUF*sizeof(uint16) );
			memset( (kal_uint8 *)gSpc_Pcmnway_spkBuf, 0, SPC_PCMNWAY_MAX_SIZE_OF_SPK_BUF*sizeof(uint16) );
			memset( (kal_uint8 *)gSpc_Pcmnway_micBuf, 0, SPC_PCMNWAY_MAX_SIZE_OF_MIC_BUF*sizeof(uint16) );
			memset( (kal_uint8 *)gSpc_Pcmnway_sdBuf, 0, SPC_PCMNWAY_MAX_SIZE_OF_SD_BUF*sizeof(uint16) );
			gSpc_Pcmnway.micBufSize = 0;
			gSpc_Pcmnway.spkBufSize = 0;
			gSpc_Pcmnway.seBufSize = 0;
			gSpc_Pcmnway.sdBufSize = 0;
					 
			// reset wating 
			gSpc_Pcmnway.pnwIsMicBufWaiting = KAL_FALSE;
			gSpc_Pcmnway.pnwIsSpkBufWaiting = KAL_FALSE;
		
		
			// UL1 path
			cfgUL1 =DATA_SELECT_AFTER_ENH;
			if(SPC_PNW_MSG_BUFFER_MIC & gSpc_Pcmnway.pnwBufInfo ) {
				cfgUL1 |= USE_D2M_PATH;
			}
			if(SPC_PNW_MSG_BUFFER_SE & gSpc_Pcmnway.pnwBufInfo ) {
				cfgUL1 |= USE_M2D_PATH;
			}
		
			//UL2 path
			cfgUL2=DATA_SELECT_AFTER_ENH;
			//UL3 path
			cfgUL3=DATA_SELECT_AFTER_ENH;
			//UL4 path
			cfgUL4=DATA_SELECT_AFTER_ENH;
			
			// DL path
			cfgDL = 0;
			if(SPC_PNW_MSG_BUFFER_SD & gSpc_Pcmnway.pnwBufInfo ) {
				cfgDL |= USE_D2M_PATH;
			}
			if(SPC_PNW_MSG_BUFFER_SPK & gSpc_Pcmnway.pnwBufInfo ) {
				cfgDL |= USE_M2D_PATH;
			}
			
			if (IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) { // in call
				if( (SPC_PNW_MSG_BUFFER_SPK|SPC_PNW_MSG_BUFFER_MIC) == gSpc_Pcmnway.pnwBufInfo)  { // 2way
					Del_PcmEx_Start(spc_pcm2wayDl_handler, spc_pcm2wayUl_handler, 
						cfgUL1, cfgUL2, cfgUL3, cfgUL4, cfgDL);
				} else {
					Del_PcmEx_Start(spc_pcm4way_handler, NULL, 
						cfgUL1, cfgUL2, cfgUL3, cfgUL4, cfgDL);
				}
				
				
			} else {	// idle
			
				PCMEX_BAND band = PCMEX_BAND_UNSET;
				PCMEX_IDLE_ENH_SETTING enhSetting = PCMEX_IDLE_ENH_SETTING_WITH; // default with
		
				band = ((gSpc_Pcmnway.pnwBufInfo == 0)? PCMEX_BAND_NB : PCMEX_BAND_WB);
				if (SPC_PCMNWAY_APP_TYPE_DMNR_CAL_PLAY_REC == appType) {			
					enhSetting = PCMEX_IDLE_ENH_SETTING_DMNR_CAL;
				} else if (SPC_PCMNWAY_APP_TYPE_DMNR_CAL_REC_ONLY == appType) {
					enhSetting = PCMEX_IDLE_ENH_SETTING_DMNR_REC_ONLY_CAL;
				}
				
				if( (SPC_PNW_MSG_BUFFER_SPK|SPC_PNW_MSG_BUFFER_MIC) == gSpc_Pcmnway.pnwBufInfo)  { // 2way
					Idle_PcmEx_Start(spc_pcm2wayDl_handler, spc_pcm2wayUl_handler, 
						cfgUL1, cfgUL2, cfgUL3, cfgUL4, cfgDL, 
						band, enhSetting);
				} else {
					Idle_PcmEx_Start(spc_pcm4way_handler, NULL, 
						cfgUL1, cfgUL2, cfgUL3, cfgUL4, cfgDL,
						band, enhSetting);
				}		
		
			}
		
#if 0 
		// rewrite
		
			if( (SPC_PNW_MSG_BUFFER_SPK|SPC_PNW_MSG_BUFFER_MIC) == gSpc_Pcmnway.pnwBufInfo) { // PCM2WAY
#if defined(__ENABLE_SPEECH_DVT__)
				if (IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE))
				{	// under call, buffer side define
					gSpc_Pcmnway.pnwBand = 0xff; 
		
					gSpc_Pcmnway.micBufSize = 0;
					gSpc_Pcmnway.spkBufSize = 0;
					gSpc_Pcmnway.seBufSize = 0;
					gSpc_Pcmnway.sdBufSize = 0;
					
					if(P2W_APP_TYPE_UNDEFINE == appType) {
						gSpc_Pcmnway.pnwAppType = P2W_APP_TYPE_UNDER_CALL; 
					} else {
						ASSERT(0);
					}
				}
				else
#endif // defined(__ENABLE_SPEECH_DVT__)
				{
					kal_uint32 tempU2MicBufSize, tempU2SpkBufSize;
					P2W_Format p2wFormat;
		
					if(0xFFFF == format) { // normal PCMNWAY 
						if(0 == gSpc_Pcmnway.pnwBand) { // NB
							p2wFormat = P2W_FORMAT_NORMAL;
						} else { // WB
							p2wFormat = P2W_FORMAT_WB_NORMAL;
						}
					} else { // specified, mostly for calibration 
						p2wFormat = (P2W_Format) format;
					}
		
					// TODO:settings
					// PCM2Way_SetFormat(p2wFormat);
					PCM2WAY_QueryBufSize(p2wFormat, &tempU2MicBufSize, &tempU2SpkBufSize);
					
					gSpc_Pcmnway.micBufSize = tempU2MicBufSize << 1;
					gSpc_Pcmnway.spkBufSize = tempU2SpkBufSize << 1;
					gSpc_Pcmnway.seBufSize = 0;
					gSpc_Pcmnway.sdBufSize = 0;
		
					if (P2W_APP_TYPE_UNDEFINE == appType)
					{
#if defined(__ENABLE_SPEECH_DVT__)
						// No P2W_APP_TYPE_UNDER_CALL
#else // defined(__ENABLE_SPEECH_DVT__)
						if (IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE))
						{
							gSpc_Pcmnway.pnwAppType = P2W_APP_TYPE_UNDER_CALL; 
						}
						else
#endif // defined(__ENABLE_SPEECH_DVT__)
						{
							gSpc_Pcmnway.pnwAppType = P2W_APP_TYPE_VOIP; 
						}
					}
					else
					{
						gSpc_Pcmnway.pnwAppType = appType;
					}
				}
				
				// run		
				//PCM2WAY_Start(spc_pcm2wayUl_handler, appType);
				// PCM2WAY_Start(spc_pcm2way_handler, gSpc_Pcmnway.pnwAppType); 
				// TODO:
				/*
				Extended_PCM2WAY_Start(spc_pcm2wayDl_handler, spc_pcm2wayUl_handler, gSpc_Pcmnway.pnwAppType, 
								  PNW_ENBLE + USE_D2M_PATH + DATA_SELECT_AFTER_ENH + MCU_SET_P2W_ON,
								  PNW_ENBLE + USE_M2D_PATH + MCU_SET_P2W_ON);
				  */
				
			} else { // always use PCM4WAY
				kal_uint32 tempBufSize;
				
				if(0 != gSpc_Pcmnway.pnwBand) { // WB
					tempBufSize = 640;
				} else {
					tempBufSize = 320; // unit is byte
				}
				
				gSpc_Pcmnway.micBufSize = tempBufSize;
				gSpc_Pcmnway.spkBufSize = tempBufSize;
				gSpc_Pcmnway.seBufSize = tempBufSize;
				gSpc_Pcmnway.sdBufSize = tempBufSize;
		
				if(P4W_APP_TYPE_UNDEFINE == appType) {
					if( IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE) ) {
						gSpc_Pcmnway.pnwAppType = P4W_APP_TYPE_UNDER_CALL; 
					} else {
						gSpc_Pcmnway.pnwAppType = P4W_APP_TYPE_WITHOUT_CALL; // this may be replaced by  P2W_APP_TYPE_WITHOUT_CALL
					}
				} else {
					gSpc_Pcmnway.pnwAppType = appType;
				}		
					
				//run
				PCM4WAY_Start(spc_pcm4way_handler, gSpc_Pcmnway.pnwAppType);
			}
#endif 
		
			// record the spc application usage status
			SET_SPC_APP_USAGE(SPC_APP_PCMNWAY_USAGE);
						
		}
		
		void spc_PcmNWayStop()
		{
			
			if(!IS_SPC_APP_USED(SPC_APP_PCMNWAY_USAGE)){
				// just leave log and return
				//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_PCMNWAY_USAGE, SPC_APP_PCMNWAY_USAGE, 0);
				return; 
			} 
		
			PcmEx_Stop();
			/*
			ASSERT((gSpc_Pcmnway.pnwAppType) != 0xFF);	
		   if( (SPC_PNW_MSG_BUFFER_SPK|SPC_PNW_MSG_BUFFER_MIC) == gSpc_Pcmnway.pnwBufInfo ){ // PCM2WAY 	  
			  PCM2WAY_Stop(gSpc_Pcmnway.pnwAppType);	  
			  // Extended_PCM2WAY_Stop(gSpc_Pcmnway.pnwAppType);
		   } else {
			  PCM4WAY_Stop(gSpc_Pcmnway.pnwAppType);
		   } 
			
			gSpc_Pcmnway.pnwAppType = 0xFF;
			*/
			
			// clear buffer
			gSpc_Pcmnway.pnwBufInfo = 0;
			gSpc_Pcmnway.pnwBand = 0;
			gSpc_Pcmnway.micBufSize = 0;
			gSpc_Pcmnway.spkBufSize = 0;
			gSpc_Pcmnway.seBufSize = 0;
			gSpc_Pcmnway.sdBufSize = 0;
		
			CLR_SPC_APP_USAGE(SPC_APP_PCMNWAY_USAGE);
		
		}


// ----------------------------------------------------------------------------
// Background Sound
// ----------------------------------------------------------------------------
typedef struct _SPC_BGSND_T_
{
	uint8 ulGainLevel;
	uint8 dlGainLevel;

	bool isDataWaiting;
	uint32 id;
}_SPC_BGSND_T;

static _SPC_BGSND_T gSpc_bgSnd;

typedef struct 
{
	uint32 delay;

#if ACLOOPBACK_USING_P2W	
	uint32 tmp_w;
	uint32 tmp_r;
#else	
	// uint32 UL_tmp_w;
	// uint32 UL_tmp_r;
	uint32 DL_tmp_w;
	uint32 DL_tmp_r;
#endif

} _SPC_ACOUSTIC_LOOPBACK_T;

static _SPC_ACOUSTIC_LOOPBACK_T gSpc_acLoopback;

#if ACLOOPBACK_USING_P2W
void AcousticLoopback_PCM2WAY_DL_HisrHdl()
#else
void AcousticLoopback_PCM4WAY_DL_HisrHdl()
#endif
{
#if ACLOOPBACK_USING_P2W
	/* P2W */
	if( (gSpc_acLoopback.tmp_w - gSpc_acLoopback.tmp_r) >= gSpc_acLoopback.delay){		
      PCM2WAY_PutToSpk(gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)]);  
      MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5,
             gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][0],
             gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][1],
             gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][2],
             gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][3],0xdddddddd);  

      (gSpc_acLoopback.tmp_r)++;   
	}else{

      // TODO: MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, SPC_AC_LOOPBACK_FILL_SPK);
            MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2,0,0xdddddddd);  
      PCM2WAY_FillSpk(0);   
   }
	
	 
#else


   if( (gSpc_acLoopback.DL_tmp_w - gSpc_acLoopback.DL_tmp_r) < ACLOOPBACK_FRAME_BUF_NO ){        
      PCM4WAY_GetFromSD((uint16*)gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)]);
	  MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4,
	  				   gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][0],
                       gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][1],
                       gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][2],
                       gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][3]);

      (gSpc_acLoopback.DL_tmp_w)++;            
   }else{
      // TODO: MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, SPC_AC_LOOPBACK_SKIP_SD);
   }
   

	if( (gSpc_acLoopback.DL_tmp_w - gSpc_acLoopback.DL_tmp_r) >= gSpc_acLoopback.delay){	  			  
		  PCM4WAY_PutToSpk(gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)]);  
		  MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 
					  gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][0],
		              gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][1],
					  gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][2],
					  gSpc_acLoopback_PCM_DL_BUF[gSpc_acLoopback.DL_tmp_r & (ACLOOPBACK_FRAME_BUF_NO - 1)][3]);
		  
		  (gSpc_acLoopback.DL_tmp_r)++;	  
		  
	}else{

	      // TODO: MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, SPC_AC_LOOPBACK_FILL_SPK);
		  PCM4WAY_FillSpk(0);
	  }  


	    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 
					  gSpc_acLoopback.DL_tmp_w, 
					  gSpc_acLoopback.DL_tmp_r);
		  
	
#endif

	
 


}


#if ACLOOPBACK_USING_P2W
void AcousticLoopback_PCM2WAY_UL_HisrHdl()
#else
void AcousticLoopback_PCM4WAY_UL_HisrHdl()
#endif
{

#if ACLOOPBACK_USING_P2W

	/*	P2W */
   if( (gSpc_acLoopback.tmp_w - gSpc_acLoopback.tmp_r) < ACLOOPBACK_FRAME_BUF_NO ){        
      PCM2WAY_GetFromMic((uint16*)gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)]);

      MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5, 
       		  gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][0],
              gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][1],
              gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][2],
              gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)][3],0xcccccccc);
	  
			//Fill Silence Pattern
			if(gSpc_acLoopback.tmp_w < ACLOOPBACK_SILENCE_FRAME_NO){
			  memset(gSpc_acLoopback_PCM_BUF[gSpc_acLoopback.tmp_w & (ACLOOPBACK_FRAME_BUF_NO - 1)], 0, ACLOOPBACK_NB_FRAME_SIZE * sizeof(uint16));
			}

      (gSpc_acLoopback.tmp_w)++;            
   }else{

   	  // TODO: MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 1, SPC_AC_LOOPBACK_SKIP_MIC);
   	   MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 0,0xcccccccc);

   }
	
#else
	// P4W UL without delay	
	PCM4WAY_GetFromMic(gSpc_acLoopback_PCM_UL_BUF);

   	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 
                    gSpc_acLoopback_PCM_UL_BUF[0],
                    gSpc_acLoopback_PCM_UL_BUF[1],
                    gSpc_acLoopback_PCM_UL_BUF[2],
                    gSpc_acLoopback_PCM_UL_BUF[3]); 
	
	PCM4WAY_PutToSE(gSpc_acLoopback_PCM_UL_BUF); 

	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4,
                    gSpc_acLoopback_PCM_UL_BUF[0],
                    gSpc_acLoopback_PCM_UL_BUF[1],
                    gSpc_acLoopback_PCM_UL_BUF[2],
                    gSpc_acLoopback_PCM_UL_BUF[3]);  
   
#endif
}

/**
	Run under MED/AUDL Thread
*/
void spc_bgSnd_writeDataDone(kal_uint16 offset, kal_uint16 length)
{
	spcBufInfo info;

	if(!IS_SPC_APP_USED(SPC_APP_BGSND_USAGE)){ // prevent data sending after background sound is off
		// kal_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 1);
		return;
   }

	// kal_trace(TRACE_INFO, SPC_BGSND_WRITEDATADONE_ENTER); 	
	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), &info, emDataBuf);

	//header check
	ASSERT(info.syncWord == 0xA2A2, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
    ASSERT(info.type == AUD_CCCI_STRMBUF_TYPE_BGS_TYPE, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type);
    ASSERT(info.length == (length-6), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);

	if(info.length>0){
		BGSND_WriteData(emDataBuf, (int32) info.length);
		// EXT_BGSND_WriteSrcBuffer(gSpc_bgSnd.id, emDataBuf, (kal_int32) info.length, BGSND_DL_PROCESS);
		// EXT_BGSND_WriteSrcBuffer(gSpc_bgSnd.id, emDataBuf, (kal_int32) info.length, BGSND_UL_PROCESS);
	}
	
	gSpc_bgSnd.isDataWaiting = KAL_FALSE;
	
   // kal_trace(TRACE_INFO, SPC_BGSND_WRITEDATADONE_LEAVE); 
}

/**
	Run under MED/AUDL Thread
*/
// #define MIN(a, b) ((a) < (b) ? (a) : (b))
void spc_bgSnd_requestData(void)
{
	int32 bLen;
   // kal_trace(TRACE_INFO, SPC_BGSND_REQUESTDATA_ENTER);          
   if(!IS_SPC_APP_USED(SPC_APP_BGSND_USAGE)){ // prevent data sending after background sound is off
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 1);
		return;
   }
   
	//if(gSpc_bgSnd.isDataWaiting){
		// kal_trace( TRACE_INFO, SPC_M2A_DROP, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 0);
		//return;
	//}
   // bLen = MIN( EXT_BGSND_GetFreeSpace(gSpc_bgSnd.id, BGSND_UL_PROCESS), EXT_BGSND_GetFreeSpace(gSpc_bgSnd.id, BGSND_DL_PROCESS));

	bLen = BGSND_GetFreeSpace(); //unit is 16bit in BGSND_GetFreeSpace() return
   // bLen &= ~0x1; 

	// kal_trace(TRACE_INFO, SPC_BGSND_REQUESTDATA_INFO, bLen, BGSND_RB_DEFAULT_THRESHOLD); 
   
	if( bLen >= BGSND_RB_DEFAULT_THRESHOLD ) {

		// send data request to AP
		SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_BGS_DATA_REQUEST, (bLen<<1) , 0);
		gSpc_bgSnd.isDataWaiting = KAL_TRUE;
		
	}				
	// kal_trace(TRACE_INFO, SPC_BGSND_REQUESTDATA_LEAVE); 
	
}

/**
	Call back handler from HISR
*/
void spc_bgSnd_handler(void)
{
#if 0
   kal_trace(TRACE_INFO, SPC_BGSND_DLHDR_ENTER); 
   EXT_BGSND_WriteExtBuffer(gSpc_bgSnd.id, 1, BGSND_DL_PROCESS);
   EXT_BGSND_WriteExtBuffer(gSpc_bgSnd.id, 1, BGSND_UL_PROCESS);
   
	if(KAL_FALSE == gSpc_bgSnd.isDataWaiting) {
	   int bLen = MIN( EXT_BGSND_GetFreeSpace(gSpc_bgSnd.id, BGSND_DL_PROCESS), EXT_BGSND_GetFreeSpace(gSpc_bgSnd.id, BGSND_DL_PROCESS));
		if( (bLen >= BGSND_RB_DEFAULT_THRESHOLD) && (EXT_BGSND_GetStatus(gSpc_bgSnd.id, BGSND_DL_PROCESS) == EXT_SRC_STATE_RUN) ) {
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_BGSND_DATA_REQUEST, 0, 0, 2);
		}
	}
	kal_trace(TRACE_INFO, SPC_BGSND_DLHDR_LEAVE); 
#endif 
	if(FALSE == gSpc_bgSnd.isDataWaiting) {
		if( BGSND_GetFreeSpace()>= BGSND_RB_DEFAULT_THRESHOLD) {
			SpcIO_MsgQueuePut(SPC_ID_AUDIO_STRM_BGSND_DATA_REQUEST, 0, 0, 2);
			gSpc_bgSnd.isDataWaiting = KAL_TRUE;
		}
	}
}

/**
	Call back handler from AUDIO task when bgSnd is off
*/
void spc_bgSnd_closeHandler(void)
{

	SpcIO_MsgQueuePut(SPC_ID_AUDIO_CONTROL_BGSND_CLOSE, 0, 0, 2);
}

void spc_BgSndStart(void)
{   
#ifdef VALSPC_CHIP_BACK_PHONECALL_USE
	return;
#else 

	if(IS_SPC_APP_USED(SPC_APP_BGSND_USAGE)){ // application re-entry
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 5);				
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_APP_RETRY, SPC_APP_BGSND_USAGE, MON_CONTINUE);
		return;
	}

	if(!IS_SPC_ID_SPEECH_CUSTOM_DATA_REQUEST_DONE){ // prevent background sound on before EM data sending
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 4);
		MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, SPC_APP_BGSND_USAGE, SPC_APP_ON_CHECK_CASES_B4_EM_REQ_DONE);
		return;
	}

	if((!IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) && (!IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE)) && (!IS_SPC_APP_USED(SPC_APP_PCMROUTER_USAGE))){ // bg sound on without any speech
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 3);		
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_OPEN_SUB_APP_WO_MAIN_APP, SPC_APP_BGSND_USAGE, MON_CONTINUE);		
		return;
	}

	if(IS_SPC_APP_USED(SPC_APP_HOLD_USAGE)){
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 2);		
		ASSERT(0, VALSPH_ERR_SPC_OPEN_SUB_APP_UNDER_CALL_HOLD, SPC_APP_BGSND_USAGE);
		
		return;
	}

	gSpc_bgSnd.isDataWaiting = KAL_FALSE;
	BGSND_Start(spc_bgSnd_handler, spc_bgSnd_closeHandler, gSpc_bgSnd.dlGainLevel, gSpc_bgSnd.ulGainLevel);
	
	SET_SPC_APP_USAGE(SPC_APP_BGSND_USAGE);
#endif	

}

void spc_BgSndStop(void)
{
	
	if(!IS_SPC_APP_USED(SPC_APP_BGSND_USAGE)){
		// just leave log and return, 
		// kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_BGSND_USAGE, SPC_APP_BGSND_USAGE, 0);
		MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNUSED_APP_OFF, SPC_APP_BGSND_USAGE, MON_CONTINUE);
		
		SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_BGSND_OFF_ACK, 0, 0);
		return; 
	} 

	// to avoid to many data request 
	// gSpc_bgSnd.isDataWaiting = KAL_TRUE; 	
   // kal_trace(TRACE_INFO, SPC_BGSNDSTOP_ENTER); 	
	BGSND_Stop(); // EXT_BGSND_Flush(gSpc_bgSnd.id);
	// kal_trace(TRACE_INFO, SPC_BGSNDSTOP_LEAVE); 	
}


/**
	@uParam:
		Bit 0: 1==Enable/0==Disable
		Bit 1: Pre-condiftion is speech mode equals to SPH_MODE_BT_EARPHONE(3) or SPH_MODE_BT_CCARITE(5). 1== BT Loopback with BT codec / 0 == BT Loopback without BT Codec. Only support (MT6572/MT6582/MT6592/MT6571)
		Bit 2: delay setting for normal loopback, i.e. speech mode is not BT cases. 0==Use modem default delay value/ 1== use AP gives delay value in msgId32 bit[0:7] 
	@extraParam:
		Bit[7:0]: Delay time in uint8. Unit is 20ms. Take effect when msgId16 bit[2] == 1. For example: when bit[7:0] = 0xf, then the delay time is 15*20 == 300 ms.
*/
void Spc_AcousticLoopback(uint8 uParam, uint32 extraParam)
{
    kal_bool enable = uParam & 0x1; // bit(0): on/off   bit(1): disable/enable BT SW codec 
    	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, uParam, extraParam,0xaaaaaa);

	if(enable){

		// check status
		/*
		if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)){
			// just leave log and return
			kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_ACLOOPBACK_USAGE, SPC_APP_ACLOOPBACK_USAGE, 1);
			return; 
		}
		*/
		if(false == spc_mainAppOnCheck(SPC_APP_ACLOOPBACK_USAGE, true))
			return;

		if(IS_SPC_APP_USED(SPC_APP_PCMNWAY_USAGE)){
			// just leave log and return
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, SPC_APP_ACLOOPBACK_USAGE, gSpc.spcAppUsage);
			return; 
		}
		

		/*
		if(IS_SPC_APP_USED(SPC_APP_HOLD_USAGE)){
			kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_ACLOOPBACK_USAGE, SPC_APP_ACLOOPBACK_USAGE, 3);		

			// [TODO] REMOVE
			ASSERT(0);
			
			return;
		}
		*/

		// special case for BT loopback 
#if defined(__CVSD_CODEC_SUPPORT__) 
		if( L1SP_GetSpeechMode() == SPH_MODE_BT_EARPHONE || L1SP_GetSpeechMode() == SPH_MODE_BT_CARKIT )
		{  // for SW BT platform, BT loopback does not related to speech and DSP.
		   bool is_WB = AFE_GetVoice8KMode() == 1 ? KAL_TRUE : KAL_FALSE;
		   SET_SPC_APP_USAGE(SPC_APP_ACLOOPBACK_USAGE);
		   BT_SCO_Loopback_ON( (uParam& 0x2)>>1 ,is_WB);
		   return; // REMIND!!!
		}
#endif

		// setup 
		gSpc_acLoopback.delay = ACLOOPBACK_DELAY;
		if((uParam & 0x4) !=0) { // using bit [2] for check delay
			uint8 givenDelay = 	(extraParam & 0xFF); //use msg32  bit [7:0] for delay setting	
			ASSERT(givenDelay<= ACLOOPBACK_FRAME_BUF_NO, VALSPH_ERR_FORCE_ASSERT, 0);
			gSpc_acLoopback.delay = givenDelay;
		}
#if ACLOOPBACK_USING_P2W		
        gSpc_acLoopback.tmp_w = 0;
		gSpc_acLoopback.tmp_r = 0;
#else		
		// gSpc_acLoopback.UL_tmp_w = 0;
		// gSpc_acLoopback.UL_tmp_r = 0;
		gSpc_acLoopback.DL_tmp_w = 0;
		gSpc_acLoopback.DL_tmp_r = 0;
#endif

		//clean memory
#if ACLOOPBACK_USING_P2W		
      memset(gSpc_acLoopback_PCM_BUF, 0, sizeof(uint16)*ACLOOPBACK_FRAME_BUF_NO*ACLOOPBACK_NB_FRAME_SIZE);
#else
      // memset(gSpc_acLoopback_PCM_UL_BUF, 0, sizeof(uint16)*ACLOOPBACK_FRAME_BUF_NO*ACLOOPBACK_NB_FRAME_SIZE);
      memset(gSpc_acLoopback_PCM_UL_BUF, 0, sizeof(uint16)*ACLOOPBACK_NB_FRAME_SIZE);
      memset(gSpc_acLoopback_PCM_DL_BUF, 0, sizeof(uint16)*ACLOOPBACK_FRAME_BUF_NO*ACLOOPBACK_NB_FRAME_SIZE);
#endif

	  SIDETONE_SetSolutionVer(SMART_PHONE_SIDETONE_SOLUTION_VER);
#if ACLOOPBACK_USING_P2W		
		/* P2W */
 
        SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_ACOUSTICLOOPBACK, true);
		SP_MuteUlFromDiffPos(false, SP_MIC_MUTE_POS_FROM_ALL); 
		L1SP_MuteSpeaker(false);
		Idle_PcmEx_Start(AcousticLoopback_PCM2WAY_DL_HisrHdl, AcousticLoopback_PCM2WAY_UL_HisrHdl, 
				USE_D2M_PATH + DATA_SELECT_AFTER_ENH, // ul1
				USE_D2M_PATH + DATA_SELECT_AFTER_ENH, // ul2
				USE_D2M_PATH + DATA_SELECT_AFTER_ENH, // ul3
				USE_D2M_PATH + DATA_SELECT_AFTER_ENH, // ul4
				USE_M2D_PATH,
				PCMEX_BAND_NB, PCMEX_IDLE_ENH_SETTING_WITH);
#else
		// enable
		L1SP_Speech_On(RAT_2G_MODE);
		L1SP_SpeechLoopBackEnable(KAL_TRUE);
		SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_ACOUSTICLOOPBACK, true);

		Del_PcmEx_Start(AcousticLoopback_PCM4WAY_DL_HisrHdl, AcousticLoopback_PCM4WAY_UL_HisrHdl, 
				USE_D2M_PATH + USE_M2D_PATH + DATA_SELECT_AFTER_ENH,
				USE_D2M_PATH + USE_M2D_PATH + DATA_SELECT_AFTER_ENH,
				USE_D2M_PATH + USE_M2D_PATH + DATA_SELECT_AFTER_ENH,
				USE_D2M_PATH + USE_M2D_PATH + DATA_SELECT_AFTER_ENH,
				USE_D2M_PATH + USE_M2D_PATH);
#endif
		
		SET_SPC_APP_USAGE(SPC_APP_ACLOOPBACK_USAGE);

   } else {

		if(!IS_SPC_APP_USED(SPC_APP_ACLOOPBACK_USAGE)){
			// just leave log and return			
			MonTrace(MON_CP_VAL_SPH_ILLEGLE_APP_TRACE_ID, 2, SPC_APP_ACLOOPBACK_USAGE, 0);
			return; 
		}

#if defined(__CVSD_CODEC_SUPPORT__) 
		if( BT_SCO_IS_SPEECH_ON() )
		{  // for SW BT platform, BT loopback does not related to speech and DSP.
		   BT_SCO_Loopback_OFF();
		   CLR_SPC_APP_USAGE(SPC_APP_ACLOOPBACK_USAGE);
		   return;
		}
#endif

		
#if ACLOOPBACK_USING_P2W	
		/* P2W */ 
		// SetSpeechEnhancement(false);
		PcmEx_Stop();
		SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_ACOUSTICLOOPBACK, false);
		SP_MuteUlFromDiffPos(false, SP_MIC_MUTE_POS_FROM_ALL); 
  	    L1SP_MuteSpeaker(false);
#else
	
      // Extended_PCM4WAY_Stop(P4W_APP_TYPE_UNDER_CALL);
	  PcmEx_Stop();
      SP_SetForcedUnMuteController(L1SP_FORCEDUNMUTE_ACOUSTICLOOPBACK, false);
      L1SP_SpeechLoopBackEnable(KAL_FALSE);     
      L1SP_Speech_Off();      	
		
#endif
		
	  CLR_SPC_APP_USAGE(SPC_APP_ACLOOPBACK_USAGE);
      
   }
	
	return; 
}

void spc_BgSndForceClose(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_BGSND_USAGE)){ // due to AP close process may interrupt by EPOF command, so the checked is need	
		// just leave log and return, 
			
		SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_BGSND_OFF_ACK, 0, 0);
		return; 
	}
	BGSND_Close();
	CLR_SPC_APP_USAGE(SPC_APP_BGSND_USAGE);	
}

void spc_BgSndClose(void)
{
	spc_BgSndForceClose();	
	SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_BGSND_OFF_ACK, 0, 0);
}
/**
	@ulGainLevel: 0~7 levels, mapping to 0~32767 
	@dlGainLevel: 0~7 levels, mapping to 0~32767
*/
void spc_BgSndConfig(kal_uint8 ulGainLevel, kal_uint8 dlGainLevel)
{
   // kal_trace(TRACE_INFO, SPC_BGSNDCONFIG_ENTER); 
	gSpc_bgSnd.ulGainLevel = ulGainLevel;
	gSpc_bgSnd.dlGainLevel = dlGainLevel;

	// when application is running, update hte gain directly
	if(IS_SPC_APP_USED(SPC_APP_BGSND_USAGE)){
		BGSND_ConfigULMixer(KAL_TRUE,  gSpc_bgSnd.ulGainLevel);
		BGSND_ConfigDLMixer(KAL_TRUE,  gSpc_bgSnd.dlGainLevel);		
	}
	// kal_trace(TRACE_INFO, SPC_BGSNDCONFIG_LEAVE); 
}

// ----------------------------------------------------------------------------
// Parameter Setting like EM
// ----------------------------------------------------------------------------

void spc_ReceiveEMParameter_common(const uint16 offset, const uint16 length)
{
	spcBufInfo info;
	AUDIO_CUSTOM_PARAM_STRUCT *audio_par;
	
	audio_par = (AUDIO_CUSTOM_PARAM_STRUCT *)(&emDataBuf); //buffer reuse
	
	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), 
		&info, audio_par);

	//header checking
   ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
   ASSERT(((info.type & 0xF) == AUD_CCCI_STRMBUF_TYPE_EM_PARAM), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type); //just use the bit[0:3] to inidicate the type. 
   ASSERT((info.length == (length-6)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);
	
	// common parameter
#if defined(__ENABLE_SPEECH_DVT__)
	audio_par->speech_common_para[0] = 6;
#endif // defined(__ENABLE_SPEECH_DVT__)
	L1SP_LoadCommonSpeechPara(audio_par->speech_common_para);
	// mode parameter
   l1sp_setAllSpeechModePara((uint16 *)(audio_par->speech_mode_para), 8 * 16);

	// [History] useless
   // memcpy(SPH_VOL_PAR , audio_par->speech_volume_para, sizeof(uint16) * 4);

	// set Debug Info
	L1Audio_SetDebugInfo(audio_par->debug_info);
	VMREC_ConfigEpl();
	
	// FIR
   l1sp_setAllSpeechFirCoeff_InputOnly((int16 *)(audio_par->sph_in_fir), 6 * 45);
   l1sp_setAllSpeechFirCoeff_OutputOnly((int16 *)(audio_par->sph_out_fir), 6 * 45);
	
	SET_SPC_EM_INIT(SPC_EM_INIT_COMMON);
	
	// loading!!
	L1SP_LoadSpeechPara();
}

void spc_ReceiveDmnrParameter(const uint16 offset, const uint16 length)
{
	spcBufInfo info;
	AUDIO_CUSTOM_DMNR_PARAM_STRUCT *dmnrPar;

	dmnrPar = (AUDIO_CUSTOM_DMNR_PARAM_STRUCT *)(&emDataBuf);

	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), &info, dmnrPar);

	//header checking
	ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
   ASSERT(((info.type & 0xF) == AUD_CCCI_STRMBUF_TYPE_EM_PARAM), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type); //just use the bit[0:3] to inidicate the type. 
   ASSERT((info.length == (length-6)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);
	
// #if defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)	
	L1SP_SetDMNRPara(dmnrPar->DMNR_para);
	
// #if defined(__AMRWB_LINK_SUPPORT__)
	L1SP_SetWbDMNRPara(dmnrPar->WB_DMNR_para);
// #endif // defined(__AMRWB_LINK_SUPPORT__)
// #endif // __DUAL_MIC_SUPPORT__ || defined(__SMART_PHONE_MODEM__)	

	SET_SPC_EM_INIT(SPC_EM_INIT_DMNR);

	if(info.length>(NUM_DMNR_PARAM + NUM_WB_DMNR_PARAM)) { // [NEW] calibration data for loud speaker mode DMNR
	
// #if defined(__DUAL_MIC_SUPPORT__) || defined(__SMART_PHONE_MODEM__)		
		L1SP_SetLSpkDMNRPara(dmnrPar->LSpk_DMNR_para);
// #if defined(__AMRWB_LINK_SUPPORT__)		
		L1SP_SetLSpkWbDMNRPara(dmnrPar->LSpk_WB_DMNR_para);
// #endif // defined(__AMRWB_LINK_SUPPORT__)
// #endif // __DUAL_MIC_SUPPORT__ || defined(__SMART_PHONE_MODEM__)	

		SET_SPC_EM_INIT(SPC_EM_INIT_LSPK_DMNR);
	}
}

void spc_ReceiveEMParameter_wb(const uint16 offset, const uint16 length)
{
	spcBufInfo info;
	AUDIO_CUSTOM_WB_PARAM_STRUCT *wbPar;
	
	wbPar = (AUDIO_CUSTOM_WB_PARAM_STRUCT *)(&emDataBuf); //buffer reuse
	
	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), &info, wbPar);

	//header checking
   ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
	ASSERT(((info.type & 0xF) == AUD_CCCI_STRMBUF_TYPE_EM_PARAM), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type); //just use the bit[0:3] to inidicate the type. 
	ASSERT((info.length == (length-6)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);	  


	l1sp_setAllWbSpeechModePara((uint16 *)wbPar->speech_mode_wb_para, 8 * 16 );
	l1sp_setAllWbSpeechFirCoeff_InputOnly((int16 *)(wbPar->sph_wb_in_fir),  6 * 90);
	l1sp_setAllWbSpeechFirCoeff_OutputOnly((int16 *)(wbPar->sph_wb_out_fir), 6 * 90);


	SET_SPC_EM_INIT(SPC_EM_INIT_WB);
}


void spc_ReceiveMagiConferenceParameter(const uint16 offset, const uint16 length)
{
	spcBufInfo info;
	AUDIO_CUSTOM_MAGI_CONFERENCE_STRUCT *magiCon; //buffer reuse
	
	magiCon = (AUDIO_CUSTOM_MAGI_CONFERENCE_STRUCT *)(&emDataBuf); //buffer reuse

	//get data
	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), &info, magiCon);
	
	//header checking
   ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
	ASSERT((info.type== AUD_CCCI_STRMBUF_TYPE_MAGI_CONFERENCE_PARAM), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type); 
	ASSERT((info.length == (length-6)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);	  

	// copy coefficient
	spe_setMagiConParam(magiCon->sph_voice_tracking_mode_para, magiCon->sph_voice_tracking_mode_wb_para);

}


void spc_ReceiveHacParameter(const uint16 offset, const uint16 length)
{
	spcBufInfo info;
	AUDIO_CUSTOM_HAC_PARAM_STRUCT *hacParam; //buffer reuse
	
	hacParam = (AUDIO_CUSTOM_HAC_PARAM_STRUCT *)(&emDataBuf); //buffer reuse

	//get data
	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), &info, hacParam);
	
	//header checking
	ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
	ASSERT((info.type== AUD_CCCI_STRMBUF_TYPE_HAC_PARAM), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type); 
	ASSERT((info.length == (length-6)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);	 

	spe_setHacSpeechModePara(hacParam->speech_hac_mode_nb_para, 16);
	spe_setHacSpeechFirCoeff_InputOnly(hacParam->sph_hac_in_fir, 45);
	spe_setHacSpeechFirCoeff_OutputOnly(hacParam->sph_hac_out_fir, 45);

	spe_setHacWbSpeechModePara(hacParam->speech_hac_mode_wb_para, 16);
	spe_setHacWbSpeechFirCoeff_InputOnly(hacParam->sph_hac_wb_in_fir, 90);
	spe_setHacWbSpeechFirCoeff_OutputOnly(hacParam->sph_hac_wb_out_fir, 90);

	spe_setHacModeNeeded(true);
}	


void spc_ReceiveViberationSpkParameter(const uint16 offset, const uint16 length)
{
	spcBufInfo info;
	AUDIO_VIBSPK_PARAMETER_STRUCT *vibSpkParam; //buffer reuse
	
	vibSpkParam = (AUDIO_VIBSPK_PARAMETER_STRUCT *)(&emDataBuf); //buffer reuse

	//get data
	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), &info, vibSpkParam);
	
	//header checking
	ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
	ASSERT((info.type== AUD_CCCI_STRMBUF_TYPE_VIBSPK_PARAM), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type); 
	ASSERT((info.length == (length-6)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);	 

	// copy coefficient
	spe_setNotchFilterParam(vibSpkParam->notch_filter_para, vibSpkParam->is2In1Spk);

	
}

void spc_ReceiveNxpSmartPaParameter(const uint16 offset, const uint16 length)
{
	spcBufInfo info;
	AUDIO_NXP_SMARTPA_PARAMETER_STRUCT *nxpPaParam; //buffer reuse
	
	nxpPaParam = (AUDIO_NXP_SMARTPA_PARAMETER_STRUCT *)(&emDataBuf); //buffer reuse

	//get data
	SpcIO_GetDataFromAp_inOneTime(offset, length, sizeof(spcBufInfo), &info, nxpPaParam);
	
	//header checking
	ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.syncWord );
	ASSERT((info.type== AUD_CCCI_STRMBUF_TYPE_NXP_SMARTPA_PARAM), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE, info.type); 
	ASSERT((info.length == (length-6)), VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, info.length);	 

	SPE_SetEchoRefInfo(nxpPaParam->isNxpFeatureOptOn, nxpPaParam->mic_index, nxpPaParam->chip_delay_with_switch, SPH_MODE_LOUDSPK);

}	

#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 

void spc_ReceiveDynamicParameter(const uint16 offset, const uint16 length)
{
	spcDynParHeader Header;
    kal_uint16 curOffset,i,Network,k;
	kal_uint32 DynamicParCount=0;
	spcDynParNBData NBParData;
	spcDynParWBData WBParData;
	uint16 SpeechIndex; // bit1: Volume index, bit0: Mode 
	uint8 Mode;



    // Header Check 
    spcBufInfo info;
    curOffset = SpcIo_GetDataFromAp(offset, sizeof(spcBufInfo), &info);
    ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD,info.syncWord);
    ASSERT(((info.type) == AUD_CCCI_STRMBUF_TYPE_DYNAMCI_PAR_TYPE),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE,info.type); //just use the bit[0:3] to inidicate the type. 
    ASSERT((info.length == (length-6)),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD,info.length);

	

	
	curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(spcDynParHeader), &Header);
 	
	switch(Header.SphUnitMagiNum){
		case Vol_de_speech_unit:
		{

			//Param_Header_Layer1; Network
            //Param_Header_Layer2; VoiceBand
			MonTrace(MON_CP_VAL_SPH_SPE_RECDYNAMIC_PAR_TRACE_ID, 1, 0);

			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 14,01);
			if(Header.NumLayer == 0x2)
			{
				DynamicParCount = (((Header.NumEachLayer)&0x00F0)>>4) * ((Header.NumEachLayer)&0x000F);
			}
			else if(Header.NumLayer == 0x1)
			{
				DynamicParCount = ((Header.NumEachLayer)&0x000F);
			}
			else
			{			
                //Not suppot over 2 Layer
				ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Header.NumLayer);
			}
			
		    curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(SpeechIndex), &SpeechIndex);
			Mode = (SpeechIndex&0x0F);
			SpeechIndex = (((SpeechIndex)&0xF0)>>4);
     		MonTrace(MON_CP_VAL_SPH_SPE_RECDYNAMIC_MODE_TRACE_ID, 2, Mode, SpeechIndex);
	        MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 02,Mode,SpeechIndex);
            if(Header.Param_Header_Layer3 == 0x10) // Version 1.0.
			{    
            	for(i = 0 ; i < DynamicParCount ; i ++)
				{
					curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(Data_Header), &Data_Header);	
					if(((Data_Header&0xF000)>>8)==NB_Par)
					{
					
						curOffset =  SpcIo_GetDataFromAp(curOffset, sizeof(spcDynParNBData), &NBParData);
						
						Network = (Data_Header&0x0FFF); // [bit0~bit11] bit0:GSM bit1:WCDMA bit2:CDMA bit3:VOLTE bit4:C2K
						if((Network)==DONT_CARE_NETWORK_V1)
					    {
							for(k = 0; k < TOTAL_NETWORK_NUMBER; k++)
							{
					   			memcpy(Temp_speech_NB_Mode_para[k], NBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_NB_FIR_IN_para[k], NBParData.speech_NB_FIR_IN_para, NUM_FIR_COEFFS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_NB_FIR_OUT_para[k], NBParData.speech_NB_FIR_OUT_para, NUM_FIR_COEFFS*sizeof(uint16)) ;	
							}
						}					
						else 
						{
							for(k = 0; k < TOTAL_NETWORK_NUMBER; k++)
							{
								if(((Network>>k)&0x1))
								{
									memcpy(Temp_speech_NB_Mode_para[k], NBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;					
		            	        	memcpy(Temp_speech_NB_FIR_IN_para[k], NBParData.speech_NB_FIR_IN_para, NUM_FIR_COEFFS*sizeof(uint16)) ;
						        	memcpy(Temp_speech_NB_FIR_OUT_para[k], NBParData.speech_NB_FIR_OUT_para, NUM_FIR_COEFFS*sizeof(uint16)) ;			                              
								}
							}
						}
									
				    	SET_SPC_EM_INIT(SPC_EM_INIT_NB);
					}
					else if((((Data_Header&0xF000)>>8)==WB_Par))
					{
						curOffset =  SpcIo_GetDataFromAp(curOffset, sizeof(spcDynParWBData), &WBParData); 		
					    
						Network = (Data_Header&0x0FFF); // [bit0~bit11] bit0:GSM bit1:WCDMA bit2:CDMA bit3:VOLTE bit4:C2K
						if((Network)==DONT_CARE_NETWORK_V1)
						{
							for(k = 0; k < TOTAL_NETWORK_NUMBER; k++)
							{
					   			memcpy(Temp_speech_WB_Mode_para[k], WBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_WB_FIR_IN_para[k], WBParData.speech_WB_FIR_IN_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_WB_FIR_OUT_para[k], WBParData.speech_WB_FIR_OUT_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;	
							}
						}
						else 
						{
							for(k = 0; k < TOTAL_NETWORK_NUMBER; k++)
							{
								if(((Network>>k)&0x1))
								{
									memcpy(Temp_speech_WB_Mode_para[k], WBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;	
		            	        	memcpy(Temp_speech_WB_FIR_IN_para[k], WBParData.speech_WB_FIR_IN_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;
						        	memcpy(Temp_speech_WB_FIR_OUT_para[k], WBParData.speech_WB_FIR_OUT_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;	
								}
							}
						}	
						SET_SPC_EM_INIT(SPC_EM_INIT_WB);
					}
					else
					{
			    	    //Not suppot bandover
						ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Data_Header);
					}
      			}
            }
			else
			{
				for(i = 0 ; i < DynamicParCount ; i ++)
				{
					curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(Data_Header), &Data_Header);	
					if(((Data_Header&0xF0)==NB_Par))
					{	
						curOffset =  SpcIo_GetDataFromAp(curOffset, sizeof(spcDynParNBData), &NBParData);
						Network = (Data_Header&0x0F);
						if(Network == GSM_NETWORK||Network == WCDMA_NETWORK||Network == CDMA_NETWORK||Network == VOLTE_NETWORK||Network ==C2K_NETWORK)
						{
				    		memcpy(Temp_speech_NB_Mode_para[Network-1], NBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;					
		            		memcpy(Temp_speech_NB_FIR_IN_para[Network-1], NBParData.speech_NB_FIR_IN_para, NUM_FIR_COEFFS*sizeof(uint16)) ;
							memcpy(Temp_speech_NB_FIR_OUT_para[Network-1], NBParData.speech_NB_FIR_OUT_para, NUM_FIR_COEFFS*sizeof(uint16)) ;			
						}
						else if(Network==DONT_CARE_NETWORK_V0)
						{
							for(k = 0; k < TOTAL_NETWORK_NUMBER; k++)
							{
					   			memcpy(Temp_speech_NB_Mode_para[k], NBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_NB_FIR_IN_para[k], NBParData.speech_NB_FIR_IN_para, NUM_FIR_COEFFS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_NB_FIR_OUT_para[k], NBParData.speech_NB_FIR_OUT_para, NUM_FIR_COEFFS*sizeof(uint16)) ;	
							}
						}
						else
						{
					    // not support the network info
					   	ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Network);

						}
				    	SET_SPC_EM_INIT(SPC_EM_INIT_NB);
					}
					else if(((Data_Header&0xF0)==WB_Par))
					{
						curOffset =  SpcIo_GetDataFromAp(curOffset, sizeof(spcDynParWBData), &WBParData); 
						Network = (Data_Header&0x0F);
					    if(Network == GSM_NETWORK||Network == WCDMA_NETWORK||Network == CDMA_NETWORK||Network == VOLTE_NETWORK||Network ==C2K_NETWORK)
						{
							memcpy(Temp_speech_WB_Mode_para[Network-1], WBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;	
		            		memcpy(Temp_speech_WB_FIR_IN_para[Network-1], WBParData.speech_WB_FIR_IN_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;
							memcpy(Temp_speech_WB_FIR_OUT_para[Network-1], WBParData.speech_WB_FIR_OUT_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;	
						}
					else if(Network==DONT_CARE_NETWORK_V0)
						{
							for(k = 0; k < TOTAL_NETWORK_NUMBER; k++)
							{
					   			memcpy(Temp_speech_WB_Mode_para[k], WBParData.speech_Mode_para, NUM_MODE_PARAS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_WB_FIR_IN_para[k], WBParData.speech_WB_FIR_IN_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;	
					   			memcpy(Temp_speech_WB_FIR_OUT_para[k], WBParData.speech_WB_FIR_OUT_para, NUM_WB_FIR_COEFFS*sizeof(uint16)) ;	
							}
						}
						else
						{
				    		// not support the network info
				    		
					    	ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Network);
						}	
						SET_SPC_EM_INIT(SPC_EM_INIT_WB);
					}
					else
					{
			    	//Not suppot bandover
					    ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Data_Header);
					}
     		 	}
			}
				
            l1sp_setAllSpeechModePara((uint16 *) Temp_speech_NB_Mode_para, TOTAL_NETWORK_NUMBER * NUM_MODE_PARAS);
	 		l1sp_setAllSpeechFirCoeff_InputOnly((int16 *)Temp_speech_NB_FIR_IN_para, TOTAL_NETWORK_NUMBER * NUM_FIR_COEFFS);
	 		l1sp_setAllSpeechFirCoeff_OutputOnly((int16 *)Temp_speech_NB_FIR_OUT_para, TOTAL_NETWORK_NUMBER * NUM_FIR_COEFFS);	
	 		l1sp_setAllWbSpeechModePara((uint16 *)Temp_speech_WB_Mode_para, TOTAL_NETWORK_NUMBER * NUM_MODE_PARAS);
	 		l1sp_setAllWbSpeechFirCoeff_InputOnly((int16 *)Temp_speech_WB_FIR_IN_para, TOTAL_NETWORK_NUMBER * NUM_WB_FIR_COEFFS);
			l1sp_setAllWbSpeechFirCoeff_OutputOnly((int16 *)Temp_speech_WB_FIR_OUT_para, TOTAL_NETWORK_NUMBER * NUM_WB_FIR_COEFFS);	
			    	//Not suppot bandover
#if defined(__SPEECH_WW_OPERATOR_SCORE_ENHANCE__) 
			spe_setMagiCon();	
			if(Mode!=0xF) 
			{		
				if(Mode == SPH_MODE_HAC)
				{	
					spe_setHacModeNeeded(true);
				}
				else if(Mode == SPH_MODE_AUX1) //Magi con
				{
					Mode = SPH_MODE_LOUDSPK;				
				}
				Spc_SetSpeechMode_Adaptation( (kal_uint8) Mode );
			}
			else
			{	
			
				SetSpeechEnhancement(true);

			}
      
#else
		   
			
			spe_DisableMagiCon();
			if(Mode!=0xF) 
			{
				
				if(Mode == SPH_MODE_HAC)
				{	
					spe_setHacModeNeeded(true);
				}
				else if(Mode == SPH_MODE_EARPHONE)
				{
					spe_setMagiCon();				
				}
				else if(Mode == SPH_MODE_AUX1) //Magi con
				{
					Mode = SPH_MODE_LOUDSPK;
					spe_setMagiCon();

				}
				Spc_SetSpeechMode_Adaptation( (kal_uint8) Mode );
			}
			else
			{	
			
				SetSpeechEnhancement(true);

			}
#endif			
		}
		break;

		case Vol_in_general_unit: 

		{
			spcCommonData CommonParData;
			//   Param_Header_Layer1: Common -bit0:Common
			MonTrace(MON_CP_VAL_SPH_SPE_RECDYNAMIC_PAR_TRACE_ID, 1, 1);				
			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 14,02);
			
			curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(Data_Header), &Data_Header);	
			
			if((Data_Header&0x0F) == 0xF)
			{
				curOffset =  SpcIo_GetDataFromAp(curOffset, sizeof(spcCommonData), &CommonParData);
				L1SP_LoadCommonSpeechPara(CommonParData.Common_Para);
				L1Audio_SetDebugInfo(CommonParData.Debug_info_para);
				VMREC_ConfigEpl();
				SET_SPC_EM_INIT(SPC_EM_INIT_COMMON);
			}
			else
			{
			    ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Data_Header);
			}
			SetSpeechEnhancement(false);
			SetSpeechEnhancement(true);
						
		}
		break;

		case Vol_in_DMNR: 

		{
			//spcDMNRData DMNRParData;
		

            //spcDMNRData *dmnrPar;

			//dmnrPar = (AUDIO_CUSTOM_DMNR_PARAM_STRUCT *)(&emDataBuf);
			MonTrace(MON_CP_VAL_SPH_SPE_RECDYNAMIC_PAR_TRACE_ID, 1, 2);			
			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 14,03);
			DynamicParCount = (((Header.NumEachLayer)&0xF0)>>4) * ((Header.NumEachLayer)&0x0F);
			

			for(i=0;i<DynamicParCount;i++)
			{
				curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(Data_Header), &Data_Header);	
				if(Data_Header==Header_DMNR_NB_REC)
				{
				    curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(DMNR_NB_REC), &DMNR_NB_REC);	
					L1SP_SetDMNRPara(DMNR_NB_REC);

				}
				else if(Data_Header == Header_DMNR_NB_LSP)
				{
				    curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(DMNR_NB_LoudSpk), &DMNR_NB_LoudSpk);	
					L1SP_SetLSpkDMNRPara(DMNR_NB_LoudSpk);

				}							
				else if(Data_Header == Header_DMNR_WB_REC)
				{
				    curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(DMNR_WB_REC), &DMNR_WB_REC);	
					L1SP_SetWbDMNRPara(DMNR_WB_REC);

				}
				else if(Data_Header == Header_DMNR_WB_LSP)
				{
				    curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(DMNR_WB_LoudSpk), &DMNR_WB_LoudSpk);	
					L1SP_SetLSpkWbDMNRPara(DMNR_WB_LoudSpk);

				}
				else
				{
				   ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Data_Header);
					

				}
				
			
			}
			
			SetSpeechEnhancement(false);
			SetSpeechEnhancement(true);


			
			
		}
		break;
		
		case Vol_in_MagiClarity:
		{
			
			//   Param_Header_Layer1: Common -bit0:Common
			//kal_prompt_trace(MOD_L1SP, "[SPE]spc_ReceiveDynamicParameter,magic num: Vol_in_MagiClarity");	
			MonTrace(MON_CP_VAL_SPH_SPE_RECDYNAMIC_PAR_TRACE_ID, 1, 3);
			spcMagiClarityData MagiClarityData;
			curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(Data_Header), &Data_Header);	
			
			if((Data_Header&0x0F) == 0xF)
			{
				curOffset =  SpcIo_GetDataFromAp(curOffset, sizeof(spcMagiClarityData), &MagiClarityData);
				L1SP_MagiClarityData(MagiClarityData.MagiClarity_Para);
				
				
				
			}
			else
			{
			    ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Data_Header);
			}		
			
						
		}
		break;
		case Vol_in_echoRefforUsbAudio: 
		{
			spcDynParEchoRefForUsbAudio_T *usbAudioParam; 
			MonTrace(MON_CP_VAL_SPH_SPE_RECDYNAMIC_PAR_TRACE_ID, 1, 5);
			
			curOffset = SpcIo_GetDataFromAp(curOffset, sizeof(Data_Header), &Data_Header);	

			usbAudioParam = (spcDynParEchoRefForUsbAudio_T *)(&emDataBuf); //buffer reuse

			if((Data_Header&0x0F) == 0xF)
			{
				curOffset =  SpcIo_GetDataFromAp(curOffset, sizeof(spcDynParEchoRefForUsbAudio_T), usbAudioParam);
				SPE_SetEchoRefInfo(usbAudioParam->isEchoRefForUsbAudioOn, usbAudioParam->mic_index, usbAudioParam->chip_delay_with_switch, SPH_MODE_USB);
				
			}
			else
			{
			    ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Data_Header); // Not support the Header
			}
		}
		break; 
		default:
			
			 ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, Header.SphUnitMagiNum); //NOT SUPPORT THE MAGIC NUM
			break;
		}
	
    
		

}





// ref
#endif

// ----------------------------------------------------------------------------
// Volume Setting 
// ----------------------------------------------------------------------------
/**
	@digiGain: unit is 0.25 dB
*/
void spc_gain_DlDigiGainSet(uint16 digiGain)
{
#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
	ASSERT(0, 0, 0);
#endif	
	l1sp_digiOnly_SetOutputVolume((int16)digiGain);
}

/**
	@refDigiGain: unit is 0.25 dB
*/
void spc_gain_DlEnhRefDigiGainSet(uint16 refDigiGain)
{
#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
	ASSERT(0, 0, 0);
#endif		
	l1sp_digiOnly_SetEnhRefOutputVolume((int16)refDigiGain);
}

/**
	@digiGain: unit is 0.25 dB, but for DSP, only 1 db step range. mean while if you send 1.25 dB and 1.5 dB, they are the same
*/
void spc_gain_UlDigiGainSet(uint16 digiGain)
{
#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
	ASSERT(0, 0, 0);
#endif		
	l1sp_digiOnly_SetMicrophoneVolume((int16)digiGain);
}

/**
	@on: 1 for turn on mute, 0 for turn off mute
*/
void spc_gain_UlMuteSet(bool on)
{
	if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE) || IS_SPC_APP_USED(SPC_APP_PCMROUTER_USAGE)) {
		gSpc.spcMuteUl = on;
		SP_MuteUlFromDiffPos(on, SP_MIC_MUTE_POS_FROM_SPC); // L1SP_MuteMicrophone(on);
	} else {
		MonTrace(MON_CP_VAL_SPH_SPC_DROP_MUTE_TRACE_ID, 1, SPC_DROP_MUTE_UL);
		// kal_trace(TRACE_STATE, SPC_SET_DROP_UL_MUTE);
	}
}

void spc_gain_DlMuteSet(bool on)
{
	if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE) || IS_SPC_APP_USED(SPC_APP_PCMROUTER_USAGE)) {
		gSpc.spcMuteDl = on;
		L1SP_MuteSpeaker(on);		
	} else {
		MonTrace(MON_CP_VAL_SPH_SPC_DROP_MUTE_TRACE_ID, 1, SPC_DROP_MUTE_DL);
		// kal_trace(TRACE_STATE, SPC_SET_DROP_DL_MUTE);
	}
}


void spc_gain_UlEnhResultMuteSet(bool on)
{
#if defined(__OPEN_DSP_SPEECH_SUPPORT__)
	ASSERT(0, 0, 0);
#endif	
	
	if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) {
		gSpc.spcMuteUlEnhResult = on;
		SP_MuteUlEnhResult(on);	
	} else {
		MonTrace(MON_CP_VAL_SPH_SPC_DROP_MUTE_TRACE_ID, 1, SPC_DROP_MUTE_UL_ENH_RESULT);
		// kal_trace(TRACE_STATE, SPC_SET_DROP_UL_ENH_RESULT_MUTE);
	}
}

void spc_gain_UlSourceMuteSet(bool on)
{
	if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) {
		gSpc.spcMuteUlSource = on;
		SP_MuteUlSource(on);	
	} else {
		MonTrace(MON_CP_VAL_SPH_SPC_DROP_MUTE_TRACE_ID, 1, SPC_DROP_MUTE_UL_SOURCE);
		// kal_trace(TRACE_STATE, SPC_SET_DROP_UL_SOURCE_MUTE);
	}
}

// ----------------------------------------------------------------------------
// Device Related Setting 
// ----------------------------------------------------------------------------

void spc_dev_SamplingRateSet(uint16 sampleRate)
{
	switch(sampleRate){

		case 8000:
			AFE_SetVoice8KMode(0);
			break;
		case 16000:
			AFE_SetVoice8KMode(1);
			break;
		default:			
			MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_SET_INCORRECT_SAMPLE_RATE, sampleRate, MON_CONTINUE);
			// ASSERT(0);
	} 
}

// ----------------------------------------------------------------------------
// AUDL running functions Related
// ----------------------------------------------------------------------------

void spc_A2M_MsgHandler(uint32 ccciMsg, uint32 ccciResv)
{
	uint16 cmd, msgData16;
	uint16 msgData32; 

	cmd = SPCIO_CCCI_MSG_CMD(ccciMsg);
	msgData16 = SPCIO_CCCI_MSG_DATA16(ccciMsg);
	msgData32 = ccciResv;

	switch(cmd){	

		case MSG_A2M_SPH_DL_DIGIT_VOLUME: 
			spc_gain_DlDigiGainSet(msgData16);
			break;
		case MSG_A2M_SPH_UL_DIGIT_VOLUME: 				
			spc_gain_UlDigiGainSet(msgData16);
			break;
			
		case MSG_A2M_MUTE_SPH_UL: 
			spc_gain_UlMuteSet( (bool)(msgData16&1) );
			break;
		case MSG_A2M_MUTE_SPH_DL: 
			spc_gain_DlMuteSet( (bool)(msgData16&1) );
			break;
		case MSG_A2M_SIDETONE_VOLUME: 
			SIDETONE_SetExtStVolume(msgData16);
			break;

		case MSG_A2M_SPH_DL_ENH_REF_DIGIT_VOLUME:
			spc_gain_DlEnhRefDigiGainSet(msgData16);
			break;
		case MSG_A2M_SIDETONE_CONFIG:
			// TODO:
			break;
			
		case MSG_A2M_MUTE_SPH_UL_ENH_RESULT:
			spc_gain_UlEnhResultMuteSet((bool)(msgData16&1));
			break;
		case MSG_A2M_MUTE_SPH_UL_SOURCE:
			spc_gain_UlSourceMuteSet((bool)(msgData16&1));
			break;
			
		// --------- [0x10] device related -----------
		case MSG_A2M_SET_SAMPLE_RATE: 
			spc_dev_SamplingRateSet(msgData16);
			break;
		case MSG_A2M_SET_DUAL_MIC: break;

		// --------- [0x20] speech control -----------
		case MSG_A2M_SPH_ON: 
		{	
			Spc_SpeechOn((uint8) msgData16);
         SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SPH_ON_ACK, msgData16, 0);
      }
			break;
		case MSG_A2M_SPH_OFF: 
		{
			Spc_SpeechOff();
         SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SPH_OFF_ACK, 0, 0);
		}
			break;

 			
		case MSG_A2M_SET_SPH_MODE: 
		{
			Spc_SetSpeechMode_Adaptation( (uint8) msgData16 );
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SET_SPH_MODE_ACK, msgData16, 0);
		}
			break;
	
		case MSG_A2M_CTRL_SPH_ENH: 
		{
			// add condition checking when turn speech enhancment on/off
			if(IS_SPC_APP_USED(SPC_APP_HOSTED_USAGE)) {
				// kal_trace(TRACE_INFO, SPC_DROP_SET, ccciMsg, cmd, cmd, msgData16, msgData32);
				MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_TURN_SPE_ON_UNDER_HOST_USAGE, 0, MON_CONTINUE);
			} else {
				L1SP_EnableSpeechEnhancement((msgData16!=0));
			}
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_CTRL_SPH_ENH_ACK, msgData16, 0);
		}		
			break; 
				
		case MSG_A2M_CONFIG_SPH_ENH: 
		{
			/*
			@msgData16: as following define. mask define is sync with AP using SPH_ENH_MASK_XX in media.h
			This is the power on/off setting of enhancement. Most of the case, it should be totally on. 
			#define SPH_ENH_MASK_ES    (1<<0)
			#define SPH_ENH_MASK_AEC   (1<<1)
			#define SPH_ENH_MASK_EES   (1<<2)
			#define SPH_ENH_MASK_ULNR  (1<<3)
			#define SPH_ENH_MASK_DLNR  (1<<4)
			#define SPH_ENH_MASK_TDDNC (1<<5)
			#define SPH_ENH_MASK_DMNR  (1<<6)
			#define SPH_ENH_MASK_SIDETONE (1 << 7)

			@msgData32[15:0]: as following define. This is the dynamic switch to decided the enhancment output. 
			typedef enum{
		                                        // ==> SAL_ENH_DYNAMIC_MUTE_UL, bit 0. Please DO NOT use it
				SPE_DYNAMIC_MASK_DMNR = 0x1, // ==> SAL_ENH_DYNAMIC_DMNR_MUX, bit 1
				SPE_DYNAMIC_MASK_VCE  = 0x2, // ==> SAL_ENH_DYNAMIC_VCE_MUX, bit 2
				SPE_DYNAMIC_MASK_BWE  = 0x4, // ==> SAL_ENH_DYNAMIC_BWE_MUX, bit 3
				SPE_DYNAMIC_MASK_DLNR  = 0x8, // ==> SAL_ENH_DYNAMIC_DLNR_MUX, bit 4  
				SPE_DYNAMIC_MASK_ULNR  = 0x10, // ==> SAL_ENH_DYNAMIC_DLNR_MUX, bit 5 
				SPE_DYNAMIC_MASK_LSPK_DMNR  = 0x20, // ==> SAL_ENH_DYNAMIC_SIDEKEYCTRL_DGAIN_MUX, bit 6
				SPE_DYNAMIC_MASK_SIDEKEYCTRL_DGAIN = 0x40, // ==> SAL_ENH_DYNAMIC_SIDEKEYCTRL_DGAIN_MUX, bit 7
				SPE_DYNAMIC_MASK_DLNR_INIT_CTRL = 0x80, // ==> SAL_ENH_DYNAMIC_DL_NR_INIT_CTRL_MUX, bit 8
				SPE_DYNAMIC_MASK_AEC = 0x100, // ==> SAL_ENH_DYNAMIC_AEC_MUX, bit 9
			}SPE_SUB_MASK_T;
			*/
			spe_updateSpeUsrMaskWithWholeValue(msgData16); 
			spe_updateSpeUsrSubMaskWithWholeValue((uint16)(msgData32&0xffff));			
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_CONFIG_SPH_ENH_ACK, msgData16, 0);
		}
			break; 

		case MSG_A2M_SET_ACOUSTIC_LOOPBACK: 
		{
#if defined(__ENABLE_SPEECH_DVT__)
            Spc_PCMNWay_DVT_Test((uint8) msgData16 );
#else // defined(__ENABLE_SPEECH_DVT__)
			Spc_AcousticLoopback((uint8) msgData16, msgData32 );
#endif // defined(__ENABLE_SPEECH_DVT__)
            SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SET_ACOUSTIC_LOOPBACK_ACK, msgData16, msgData32);
        } 
			break;	
			
		case MSG_A2M_PRINT_SPH_COEFF: 
		{
			spc_DebugPrint();
			// ack is: SPCIO_MSG_FROM_SPC_PRINT_SPH_COEFF_ACK --> MSG_M2A_PRINT_SPH_COEFF_ACK
		}	
			break;		
		case MSG_A2M_SPH_ON_FOR_HOLD_CALL: 
		{
			// [REMIND] No application can be on under this application
			spc_SpeechOnForCallHold((msgData16 == 1)); // bit 0, 0 for off, 1 for on
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SPH_ON_FOR_HOLD_CALL_ACK, 0, 0);
		}
			break;
#if 0 // temp for c2k			
		case MSG_A2M_SPH_ON_FOR_DACA:
		{
			Spc_SpeechOnForDaca((kal_uint8) msgData16, (kal_bool)(msgData16 >> 8));
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SPH_ON_FOR_DACA_ACK, msgData16, 0);				
		}
			break;

		case MSG_A2M_SPH_ROUTER_ON:
		{
			// TODO: PCM router 
			Spc_SpeechRouterOn((msgData16&1)==1);
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SPH_ROUTER_ON_ACK, msgData16, 0);							
		}
			break;
#endif 
		case MSG_A2M_SET_VOICE_ENCRYPTION:
		{
			Spc_VoiceEncryptionSwitch((bool)(msgData16&1));
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_SET_VOICE_ENCRYPTION_ACK, msgData16, 0);	
		}
		break;

		// --------- [0x30] speech control -----------
		case MSG_A2M_PNW_ON:  // normal use always narrow band/decided by network
		{
			spc_PcmNWayStart(msgData16, SPC_PCMNWAY_FORMAT_UNSET, SPC_PCMNWAY_APP_TYPE_UNSET);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_PNW_ON_ACK, msgData16, 0);
				// SPCIO_CCCI_MSG_CONSTRCUT_CMD(MSG_M2A_PNW_ON_ACK, msgData16));
		}
			break;
		case MSG_A2M_PNW_OFF: 
		{
			spc_PcmNWayStop();
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_PNW_OFF_ACK, msgData16, 0);
				// SPCIO_CCCI_MSG_CONSTRCUT_CMD(MSG_M2A_PNW_OFF_ACK, msgData16));				
		}
			break;
 			
		case MSG_A2M_RECORD_ON: 
		{
			// TODO: phase out
#if defined(__ENABLE_SPEECH_DVT__)
            Spc_PCMNWay_DVT_Test_by_Rec_Button(1, (kal_uint8)(msgData16&0xF), (kal_uint8)((msgData16>>4)&0xF), (kal_uint8)((msgData16>>8)&0xF));
#else // defined(__ENABLE_SPEECH_DVT__)
			// spc_RecordStart((kal_uint8)(msgData16&0xF), (kal_uint8)((msgData16>>4)&0xF), (kal_uint8)((msgData16>>8)&0xF));
#endif // defined(__ENABLE_SPEECH_DVT__)
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_RECORD_ON_ACK, msgData16, 0);
				// SPCIO_CCCI_MSG_CONSTRCUT_CMD(MSG_M2A_RECORD_ON_ACK, msgData16));
		}
			break;
		case MSG_A2M_RECORD_OFF: 
		{
			// TODO: phase out
			
#if defined(__ENABLE_SPEECH_DVT__)
			Spc_PCMNWay_DVT_Test_by_Rec_Button(0, 0, 0, 0);
#else // defined(__ENABLE_SPEECH_DVT__)
			// spc_RecordStop();
#endif // defined(__ENABLE_SPEECH_DVT__)
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_RECORD_OFF_ACK, msgData16, 0);
				// SPCIO_CCCI_MSG_CONSTRCUT_CMD(MSG_M2A_RECORD_OFF_ACK, msgData16));
		}				
			break;
		case MSG_A2M_DMNR_RECPLAY_ON: 
		{
			// TODO: phase out 
			// spc_PcmNWayStart((SPC_PNW_MSG_BUFFER_SPK|SPC_PNW_MSG_BUFFER_MIC), 
			// 		(SPC_PCMNWAY_Format)msgData16, SPC_PCMNWAY_APP_TYPE_DMNR_CAL_PLAY_REC);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_DMNR_RECPLAY_ON_ACK, msgData16, 0);
				// SPCIO_CCCI_MSG_CONSTRCUT_CMD(MSG_M2A_DMNR_RECPLAY_ON_ACK, msgData16));
		}
			break;
		case MSG_A2M_DMNR_RECPLAY_OFF: 
		{
			// TODO: phase out
			// spc_PcmNWayStop();
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_DMNR_RECPLAY_OFF_ACK, msgData16, 0);
				
		}
			break;

		case MSG_A2M_DMNR_REC_ONLY_ON: 
		{
			// spc_PcmNWayStart((SPC_PNW_MSG_BUFFER_SPK|SPC_PNW_MSG_BUFFER_MIC), 
			// 	(SPC_PCMNWAY_Format)msgData16, SPC_PCMNWAY_APP_TYPE_DMNR_CAL_REC_ONLY);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_DMNR_REC_ONLY_ON_ACK, msgData16, 0);
				
		}
			break;
		case MSG_A2M_DMNR_REC_ONLY_OFF: 
		{
			// spc_PcmNWayStop();
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_DMNR_REC_ONLY_OFF_ACK, msgData16, 0);
				
		}
			break;
			

		case MSG_A2M_PCM_REC_ON:
		{
			spc_PcmRecordStart((uint8)(msgData16&0xF), (uint8)((msgData16>>4)&0xF));
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_PCM_REC_ON_ACK, msgData16, 0);
		}
			break;
		case MSG_A2M_PCM_REC_OFF:
		{
			spc_PcmRecordStop();
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_PCM_REC_OFF_ACK, msgData16, 0);
		}
			break;
					
		case MSG_A2M_VM_REC_ON:
		{
			spc_VmRecordStart();
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_VM_REC_ON_ACK, msgData16, 0);
		}
			break;			
		case MSG_A2M_VM_REC_OFF:
		{
			spc_VmRecordStop();
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_VM_REC_OFF_ACK, msgData16, 0);
		}
			break;
			
        case MSG_A2M_RECORD_RAW_PCM_ON:
		{
			spc_RawPcmRecordStart((uint8)(msgData16&0xF));
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_RECORD_RAW_PCM_ON_ACK, msgData16, 0);
		}
		break;
        case MSG_A2M_RECORD_RAW_PCM_OFF:
		{
			spc_RawPcmRecordStop();
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_RECORD_RAW_PCM_OFF_ACK, msgData16, 0);
		}
		break;

		// --------- [0x40] other control function -----------
		
		case MSG_A2M_CTM_ON: 
			{
			spc_CtmStart(msgData16);
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_CTM_ON_ACK, msgData16, 0);
		}
			break;
		case MSG_A2M_CTM_OFF: 
		{
			spc_CtmStop();
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_CTM_OFF_ACK, msgData16, 0);
		 }
			break;
		//case MSG_A2M_CTM_DUMP_DEBUG_FILE: 
			//spc_CtmDebugConfig(msgData16!=0);
			//break; // ack is SPCIO_MSG_FROM_SPC_CTM_DUMP_DEBUG_FILE_ACK-- > MSG_M2A_CTM_DUMP_DEBUG_FILE_ACK
		


		case MSG_A2M_BGSND_ON:
			spc_BgSndStart();
			SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_BGSND_ON_ACK, msgData16, 0);
			break;
		case MSG_A2M_BGSND_OFF: 
			spc_BgSndStop();
			// SpcIO_SendMsgToAp( SPCIO_MSG_FROM_SPC_BGSND_OFF_ACK, msgData16, 0); 
			break;
		case MSG_A2M_BGSND_CONFIG: 
			// msgData16, [15:8] ulGainLevel from 0 ~ 255, [7:0] dlGainLevel from 0 ~ 255
			// we mapping the value into 0~7 levels. so right shift 5 
			// spc_BgSndConfig((msgData16>>8)>>5, (msgData16&0xff)>>5);
			spc_BgSndConfig((msgData16>>13), ((msgData16&0xff)>>5));
			break; // SPCIO_MSG_FROM_SPC_BGSND_CONFIG_ACK-- > MSG_M2A_BGSND_CONFIG_ACK

		// --------- [0x50] Recevie DATA notify  -----------
		case MSG_A2M_PNW_DLDATA_NOTIFY: 
			spc_pcmNWay_writeDlDataDone((kal_uint16)msgData32, msgData16);
			break;

		case MSG_A2M_BGSND_DATA_NOTIFY: 
			spc_bgSnd_writeDataDone( (kal_uint16)msgData32, msgData16);
			break;
#if 0 // temp for c2k			
		case MSG_A2M_CTM_DATA_NOTIFY: break;
		case MSG_A2M_DACA_UL_DATA_NOTIFY:
			spc_daca_writeUlDataDone((kal_uint16)msgData32, msgData16);
			break;
#endif
       case MSG_A2M_SPC_UL_ENC: 
				// need run in SpcIO_A2M_hisr
			break;	
			case MSG_A2M_SPC_DL_DEC: 
				// need run in SpcIO_A2M_hisr
			break;

		// --------- [0x60] Send DATA Ack  ---------------
		case MSG_A2M_PNW_ULDATA_READ_ACK: 
			spc_pcmNWay_sendUlDataDone();
			break;
#if 0 // temp for c2k				
		case MSG_A2M_REC_DATA_READ_ACK: 
			spc_record_sendMicDataDone();
			break;

#ifdef __CTM_SUPPORT__				
		case MSG_A2M_CTM_DEBUG_DATA_READ_ACK:
			spc_ctm_sendDumpDebugDataDone();
			break;
#endif
#endif			
		case MSG_A2M_PCM_REC_DATA_READ_ACK:
			spc_pcmRec_sendMicDataDone();
			break;

		case MSG_A2M_VM_REC_DATA_READ_ACK:
			spc_vmRec_sendMicDataDone();
			break;
#if 0 // temp for c2k					
		case MSG_A2M_DACA_DL_DATA_READ_ACK:
			spc_daca_sendDlDataDone();
			break;
#endif // temp for c2k 			
		case MSG_A2M_RAW_PCM_REC_DATA_READ_ACK:
         spc_RawPcmRec_sendDataDone();
         break;
				
		

		// --------- [0x70] EM related --------------------
		case MSG_A2M_EM_INCALL: 
			#if (defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)||defined(__OPEN_DSP_SPEECH_SUPPORT__)) 
			ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, MSG_A2M_EM_INCALL);
			#else
			spc_ReceiveEMParameter_common( (uint16)msgData32, msgData16 );
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_EM_INCALL_ACK, 0, 0 );
			#endif
			break;
		case MSG_A2M_EM_DMNR: 
			#if (defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)||defined(__OPEN_DSP_SPEECH_SUPPORT__))
			ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, MSG_A2M_EM_INCALL);
			#else
			spc_ReceiveDmnrParameter((uint16)msgData32, msgData16);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_EM_DMNR_ACK, 0, 0 );
			#endif
			break;
		case MSG_A2M_EM_WB: 
			#if (defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)||defined(__OPEN_DSP_SPEECH_SUPPORT__)) 
			ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, MSG_A2M_EM_INCALL);
			#else
			spc_ReceiveEMParameter_wb((uint16)msgData32, msgData16);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_EM_WB_ACK, 0, 0 );
			#endif
			break;
			
		case MSG_A2M_EM_MAGICON:
			#if (defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)||defined(__OPEN_DSP_SPEECH_SUPPORT__))
			ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, MSG_A2M_EM_INCALL);
			#else
			spc_ReceiveMagiConferenceParameter((kal_uint16)msgData32, msgData16);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_EM_MAGICON_ACK, 0, 0 );
			#endif
			break;
#if 0 // temp for c2k 			
		case MSG_A2M_NETWORK_STATUS_ACK:        
	      break;		
		case MSG_A2M_QUERY_RF_INFO:
		   spc_notify_rf_info();	
         break;
#endif // temp for c2k 									
		case MSG_A2M_EM_HAC: 
			#if (defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT)||defined(__OPEN_DSP_SPEECH_SUPPORT__))
			ASSERT(0, VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD, MSG_A2M_EM_INCALL);
			#else
			spc_ReceiveHacParameter((kal_uint16)msgData32, msgData16);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_EM_HAC_ACK, 0, 0 );
			#endif
			break;
		case MSG_A2M_DYNAMIC_PAR:
			#if defined(MTK_AUDIO_HIERARCHICAL_PARAM_SUPPORT) 
			spc_ReceiveDynamicParameter((kal_uint16)msgData32, msgData16);
			#endif
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_DYNAMIC_PAR_ACK, 0, 0 );
			break;
		// --------- [0x80] New feature data related --------------------
		
		case MSG_A2M_VIBSPK_PARAMETER:
			spc_ReceiveViberationSpkParameter((kal_uint16)msgData32, msgData16);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_VIBSPK_PARAMETER_ACK, 0, 0 );
			break;

		case MSG_A2M_NXP_SMARTPA_PARAMETER: 
			spc_ReceiveNxpSmartPaParameter((kal_uint16)msgData32, msgData16);
			SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_NXP_SMARTPA_PARAMETER_ACK, 0, 0 );
			break;	

		// --------- [0x90] New feature data related --------------------
		case MSG_A2M_NW_CODEC_INFO_READ_ACK: 
			spc_ReceiveNwCodecInfoReadDone();
			break;

		default: 
			MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNKNOWN_CMD, ccciMsg, MON_CONTINUE);
			MonFault(MON_VALSPH_FAULT_UNIT, VALSPH_ERR_SPC_UNKNOWN_CMD, msgData32, MON_CONTINUE);
			// kal_trace(TRACE_INFO, SPCIO_INVALID_MSG, SPC_ID_AUDIO_A2M_CCCI, ccciMsg, ccciResv);
		
	}
}


// ----------------------------------------------------------------------------
// Init Related
// ----------------------------------------------------------------------------

// extern unsigned short L1D_Audio_ChkDspInitDone();
/**
	Function owner is EL1. 
	@return: true all rat init done, else is false
*/
// extern kal_bool MML1_RF_CHECK_RF_INIT_STATUS(void);

void Spc_Init(void)
{	
	/*
	kal_int16 tick=0; 

	while( 0 == L1D_Audio_ChkDspInitDone()){
		kal_sleep_task(1);
	}
	*/

	// for MT6752 MD1, due to RF K needs 1.5 second, please wait for it. 
#if (defined(MT6752) && defined (__MD1__)) || defined(MT6595)	
	while( false == MML1_RF_CHECK_RF_INIT_STATUS()) {
		kal_prompt_trace(MOD_L1SP, "AudioTask Wait EL1 RF Init for %d tick(s). ", tick);
		kal_sleep_task(1);
		tick++;
	}
#endif	
	// ====== Initial needed by other application =======
	//background sound
	gSpc_bgSnd.ulGainLevel = 5;
	gSpc_bgSnd.dlGainLevel = 5;

	// ENC
	SP_ENC.Is_Voice_Encryption = false;
	SP_ENC.Spc_C2K_UL_Dbg_Count = 0;
	SP_ENC.Spc_C2K_DL_Dbg_Count = 0;
	SP_ENC.UL_C2K_after_Enc_buffer_write_index = 0;
    SP_ENC.UL_C2K_after_Enc_buffer_read_index = 0;
    SP_ENC.UL_C2K_after_Enc_buffer_count = 0;
    SP_ENC.DL_C2K_after_Dec_buffer_write_index = 0;
    SP_ENC.DL_C2K_after_Dec_buffer_read_index = 0 ;
    SP_ENC.DL_C2K_after_Dec_buffer_count = 0;
	SP_ENC.UL_C2K_Delay_Count = 0;
    SP_ENC.DL_C2K_Delay_Count = 0;
	(void)(gSpc_bgSnd);
	
	// ----------------------
#if SPC_CUST_ENH	
	// add IDs for DV process
	DV_dl_audioid = L1Audio_GetAudioID();
	L1Audio_SetEventHandler(DV_dl_audioid, (L1Audio_EventHandler)DV_DL_process);  
	DV_ul_audioid = L1Audio_GetAudioID();
	L1Audio_SetEventHandler(DV_ul_audioid, (L1Audio_EventHandler)DV_UL_process);  
#endif 	
}
void Spc_ForceEndAllApp(void) // AP send EPOF to MD L4C. L4C will invoke this function
{
	uint32 is_factory_mode = HwdGetSysMode(); //0 normal, 1 meta 2 factory  
	(gSpc.spcGetEpofTimes)++;

	if(is_factory_mode == 0)
	{
		SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC_EPOF_NOTIFY, (uint16)0, 0);
		MonTrace(MON_CP_VAL_SPH_L4C_EPOF_TRACE_ID, 1,0);		
	}
#if defined(__VOLTE_SUPPORT__) 

	// internal tone stop
	TONE_Stop(DSP_TONE);
	TONE_Stop(MCU_TONE); // do care about this 
	KT_Stop(DSP_TONE);
	KT_Stop(MCU_TONE); // do care about this
#endif

	if(IS_SPC_APP_USED(SPC_APP_RECORD_USAGE)) {
		spc_RecordStop();
	}

	if(IS_SPC_APP_USED(SPC_APP_BGSND_USAGE)) {
		
		if((BGSND_STATE_RUN == BGSND_GetStatus()) || (BGSND_STATE_STOP == BGSND_GetStatus())){
			// force close without ack
			spc_BgSndForceClose(); 
			
		} else if (BGSND_STATE_CLOSE == BGSND_GetStatus()) {
			// AP has send close request, as the result we need to send ack to AP  
			spc_BgSndClose();  
		} // else is idle
		
	}

	if(IS_SPC_APP_USED(SPC_APP_PCM_REC_USAGE)) {
		spc_PcmRecordStop();
	}
	
	if(IS_SPC_APP_USED(SPC_APP_VM_REC_USAGE)) {
		spc_VmRecordStop();
	}

	if(IS_SPC_APP_USED(SPC_APP_RAW_PCM_REC_USAGE)) {
		spc_RawPcmRecordStop();
	}


	// main app 
	if(IS_SPC_APP_USED(SPC_APP_SPEECH_USAGE)) {
		Spc_SpeechOff();
	}		
	
	if(is_factory_mode == 2)
	{
		//Notify L4C: speech driver enter EPOF done
		ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_EPOF_DONE_MSG, NULL, 0);		
		MonTrace(MON_CP_VAL_SPH_L4C_EPOF_TRACE_ID, 2,0);
	}
	
}

uint16 get_spcGetEpofTimes(void)
{
	return(gSpc.spcGetEpofTimes);
}

void spc_CtmStart(L1Ctm_Interface mode)
{
	if(!IS_SPC_ID_SPEECH_CUSTOM_DATA_REQUEST_DONE){ // prevent ctm on before EM data sending
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_CTM_USAGE, SPC_APP_CTM_USAGE, 3);
		return;
	}
	

	//gSpc_ctm.isDumpDataWaiting = false;
	//memset(gSpc_ctm.dumpDebugBuf, 0, sizeof(uint16)*SPC_CTM_DUMP_DEBUG_BUF_SIZE);
	//gSpc_ctm.dumpDebugBufPtr=0;
	
  L1Ctm_Open(mode);
	SET_SPC_APP_USAGE(SPC_APP_CTM_USAGE);
}

void spc_CtmStop(void)
{
	if(!IS_SPC_APP_USED(SPC_APP_CTM_USAGE)){
		// just leave log and return
		//kal_trace( TRACE_INFO, SPC_ILLEGAL_SPC_APP_BEHAVIOR, SPC_APP_CTM_USAGE, SPC_APP_CTM_USAGE, 0);
		return; 
	} 
	
	L1Ctm_Close();

	CLR_SPC_APP_USAGE(SPC_APP_CTM_USAGE);
}
// ============================================================================
#define ILM_SPC_CUST_DUMP_BUF_LEN 2560
typedef struct{
   // LOCAL_PARA_HDR
   //raw information from eMAC
   uint16 dumpLen;
   uint16 dumpBuf[ILM_SPC_CUST_DUMP_BUF_LEN];
}ilm_spc_cust_dump_t;

void spc_customDump(uint16 bufLen, uint16 *dumpBuf)
{

	ilm_spc_cust_dump_t *ilm;
	uint16 len = bufLen; 
	uint16 idx = 0;

	static int32 i=0; 
	
	if ( 0 == bufLen) 
		return;
	
	while(len > ILM_SPC_CUST_DUMP_BUF_LEN) {
		ilm = (ilm_spc_cust_dump_t *)ExeMsgBufferGet( sizeof(ilm_spc_cust_dump_t));
		ilm->dumpLen = ILM_SPC_CUST_DUMP_BUF_LEN; 
		memcpy((ilm->dumpBuf), &(dumpBuf[idx]), sizeof(uint16)*ILM_SPC_CUST_DUMP_BUF_LEN); 
		ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_AUDIO_CUST_DUMP_REQ, ilm, sizeof(ilm_spc_cust_dump_t));
		idx = idx + ILM_SPC_CUST_DUMP_BUF_LEN; 
		len = len - ILM_SPC_CUST_DUMP_BUF_LEN;

		i++;
	}

	
	ilm = (ilm_spc_cust_dump_t *)ExeMsgBufferGet( sizeof(ilm_spc_cust_dump_t));
	ilm->dumpLen = len; 
	memcpy((ilm->dumpBuf), &(dumpBuf[idx]), sizeof(uint16)*len); 
	ExeMsgSend(EXE_VAL_ID, VAL_MAILBOX, VAL_SPH_AUDIO_CUST_DUMP_REQ, ilm, sizeof(ilm_spc_cust_dump_t));
	
	i++;

	MonTrace(MON_CP_VAL_SPH_PROMPT_TRACE_ID, 2, 0x1234, 2);
}

void spc_sendCustomDump(void *ilm)
{
	uint16 bufLen; 
	uint16 *dumpBuf;
	uint16 lenInByte;
	spcBufInfo info;
	bool sendResult = true; 

	// static int32 j=0; 

	bufLen = ((ilm_spc_cust_dump_t *)(ilm))->dumpLen;
	dumpBuf =  ((ilm_spc_cust_dump_t *)(ilm))->dumpBuf;

	if(NULL == dumpBuf || 0 == bufLen)
		return;

	lenInByte = (bufLen <<1);

#if 0	
	{
	uint16 vocLen = lenInByte;
	kal_uint8 *vocBuf;
	
	vocBuf = (kal_uint8 *)(dumpBuf); 
	while(vocLen > 1000)
	{
		tst_vc_response(TVCI_VM_LOGGING, (const kal_uint8*)vocBuf, 1000);
		vocBuf += 1000;
		vocLen -= 1000;
		j++;
	}
	   tst_vc_response(TVCI_VM_LOGGING, (const kal_uint8*)vocBuf, vocLen);
	   j++;
	}
	kal_prompt_trace(MOD_L1SP, "spc_customDump' j=%d", j);
#endif 	


	info.syncWord = 0x2A2A;
	info.type = AUD_CCCI_STRMBUF_TYPE_CUST_DUMP; 
	info.length = lenInByte;

    sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
             (void *)(dumpBuf), lenInByte, NULL, 0,
             SPCIO_MSG_FROM_SPC_CUST_DUMP_NOTIFY);

	if(false == sendResult) {
		// leave log here
		MonTrace(MON_CP_VAL_SPH_PROMPT_TRACE_ID, 2, 0x1234,3);
	}
	
}

void Spc_VoiceEncryptionSwitch(bool enable)
{
    if(enable)
    {
    	SP_ENC.Is_Voice_Encryption = true;	
    }
	else
	{		
		SP_ENC.Is_Voice_Encryption = false;

		//RESET ENC INFO 
		SP_ENC.Spc_C2K_UL_Dbg_Count = 0;
		SP_ENC.Spc_C2K_DL_Dbg_Count = 0;
		SP_ENC.UL_C2K_after_Enc_buffer_write_index = 0;
        SP_ENC.UL_C2K_after_Enc_buffer_read_index = 0;
        SP_ENC.UL_C2K_after_Enc_buffer_count = 0;
        SP_ENC.DL_C2K_after_Dec_buffer_write_index = 0;
        SP_ENC.DL_C2K_after_Dec_buffer_read_index = 0 ;
        SP_ENC.DL_C2K_after_Dec_buffer_count = 0;
		SP_ENC.UL_C2K_Delay_Count = 0;
        SP_ENC.DL_C2K_Delay_Count = 0;
		
	}
	//kal_trace( TRACE_FUNC,SPC_ENC_Switch,SP_ENC.Is_Voice_Encryption);	
	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 0x3520,0x0301,enable);

}

void Spc_SetVoiceEncryptionHandler(SPC_NETWOEK_TYPE network)
{
	switch(network)
    {
    	case Set_C2K_Encrypt:
    	{
			spc_SetC2KULDataEnc();
         	break;
      	}
		
      	default:
         	break;
   	}
}

void Spc_SetVoiceDecryptionHandler(SPC_NETWOEK_TYPE network)
{
	switch(network)
    {
    	case Set_C2K_Decrypt:
    	{
			spc_SetC2KDLDataDec();		
         	break;
      	}
		
      	default:
         	break;
   	}
}

uint16 dbg_count = 0;
uint16 dbg_DL_count = 0;

void spc_SetC2KULDataEnc(void)
{
	spcBufInfo info;
	bool sendResult = true; 

	if( SP_ENC.Is_Voice_Encryption == false ){ 	
		return;
	}
	info.syncWord = 0x2A2A;
	info.type = AUD_CCCI_STRMBUF_TYPE_UL_ENC; 
	info.length = (SP_ENC.UL_C2K_befor_Enc_buffer[2]+3)*2;
	
		 
	 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 10, 0x3520,0x0302,SP_ENC.UL_C2K_befor_Enc_buffer[0],SP_ENC.UL_C2K_befor_Enc_buffer[1],SP_ENC.UL_C2K_befor_Enc_buffer[2],SP_ENC.UL_C2K_befor_Enc_buffer[3],SP_ENC.UL_C2K_befor_Enc_buffer[4],SP_ENC.UL_C2K_befor_Enc_buffer[5],SP_ENC.UL_C2K_befor_Enc_buffer[6],SP_ENC.UL_C2K_befor_Enc_buffer[7]);
	 
	sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
			  SP_ENC.UL_C2K_befor_Enc_buffer, info.length , NULL, 0,
			  SPCIO_MSG_FROM_SPC_UL_ENC_REQUEST);
	
	
	if(sendResult == false) {
		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 0x3520,0x0303,0x9999);
		//kal_trace( TRACE_FUNC,SPC_ENC_SEND_DATA_TO_AP_FAIL);
	}
	
}
void spc_SetC2KDLDataDec(void)
{
	 
	spcBufInfo info;
	bool sendResult = true; 
	
    
	if( SP_ENC.Is_Voice_Encryption == false ){ 	
		return;
	}
    
	info.syncWord = 0x2A2A;
	info.type = AUD_CCCI_STRMBUF_TYPE_DL_DEC; 
	info.length = (SP_ENC.DL_C2K_befor_Dec_buffer[2]+3)*2;

        
				
   MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 10 ,0x3520,0x0304,SP_ENC.DL_C2K_befor_Dec_buffer[0],SP_ENC.DL_C2K_befor_Dec_buffer[1],SP_ENC.DL_C2K_befor_Dec_buffer[2],SP_ENC.DL_C2K_befor_Dec_buffer[3],SP_ENC.DL_C2K_befor_Dec_buffer[4],SP_ENC.DL_C2K_befor_Dec_buffer[5],SP_ENC.DL_C2K_befor_Dec_buffer[6],SP_ENC.DL_C2K_befor_Dec_buffer[7]);
   sendResult = SpcIO_WriteDataToAp(&info, sizeof(spcBufInfo), 
             SP_ENC.DL_C2K_befor_Dec_buffer, info.length , NULL, 0,
             SPCIO_MSG_FROM_SPC_DL_DEC_REQUEST);	
	if(sendResult == false) {
		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 0x3520,0x0305,0x9999);
		//kal_trace( TRACE_FUNC,SPC_ENC_SEND_DATA_TO_AP_FAIL);
	}
	

}



void Spc_GetVoiceEncryptionHandler(kal_uint16 offset, kal_uint16 length)
{
	kal_uint16 curOffSet;
	spcBufInfo info;
	spcC2KEnCInfo EncInfo;
	
	if( SP_ENC.Is_Voice_Encryption == false ){	
		return;
	}
    
    curOffSet = SpcIo_GetDataFromAp(offset, sizeof(spcBufInfo), &info);
    ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD,info.syncWord);
    ASSERT(((info.type) == AUD_CCCI_STRMBUF_TYPE_UL_ENC),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE,info.type); //just use the bit[0:3] to inidicate the type. 
    ASSERT((info.length == (length-6)),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD,info.length);
	curOffSet = SpcIo_GetDataFromAp(curOffSet, sizeof(spcC2KEnCInfo), &EncInfo);
	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 0x3520,0x0306,EncInfo.network,EncInfo.count);
   
	switch(EncInfo.network)
    {
     	case Set_Network_C2K:
		{


			if( SP_ENC.UL_C2K_after_Enc_buffer_count == MAX_UL_C2K_ENC_BUFFER_NUM )
   			{
   				SP_ENC.UL_C2K_after_Enc_buffer_read_index++;          //drop oldest frame
     			SP_ENC.UL_C2K_after_Enc_buffer_count--;
      	
    			if( SP_ENC.UL_C2K_after_Enc_buffer_read_index == MAX_UL_C2K_ENC_BUFFER_NUM){
      			SP_ENC.UL_C2K_after_Enc_buffer_read_index = 0;
    			}
				
				MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 0x3520,0x0307);
   			}
			
			SpcIo_GetDataFromAp(curOffSet, (info.length-2), SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index]);
	 		MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 10, 0x3520,0x0308,SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][0],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][1],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][2],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][3],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][4],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][5],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][6],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_write_index][7]);
            SP_ENC.UL_C2K_after_Enc_buffer_write_index++;
			if(SP_ENC.UL_C2K_after_Enc_buffer_write_index== MAX_UL_C2K_ENC_BUFFER_NUM)
			{
				SP_ENC.UL_C2K_after_Enc_buffer_write_index = 0;
			}
			SP_ENC.UL_C2K_after_Enc_buffer_count++;
			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5, 0x3520,0x0309,SP_ENC.UL_C2K_after_Enc_buffer_count,SP_ENC.UL_C2K_after_Enc_buffer_read_index,SP_ENC.UL_C2K_after_Enc_buffer_write_index);
			break;
		}
		
		default:			
			ASSERT(EncInfo.network, 0, 0);			
       		break;
     
	}
	

}

void Spc_GetVoiceDecryptionHandler(kal_uint16 offset, kal_uint16 length)
{
	kal_uint16 curOffSet;
	spcBufInfo info;
	spcC2KEnCInfo DecInfo;
	if( SP_ENC.Is_Voice_Encryption == false ){	
		return;
	}
	curOffSet = SpcIo_GetDataFromAp(offset, sizeof(spcBufInfo), &info);
    ASSERT((info.syncWord == 0xA2A2),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD,info.syncWord);
    ASSERT(((info.type) == AUD_CCCI_STRMBUF_TYPE_DL_DEC),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_TYPE,info.type); //just use the bit[0:3] to inidicate the type. 
    ASSERT((info.length == (length-6)),VALSPH_ERR_SPC_DATA_HEADER_INCORRECT_SYNC_WORD,info.length);
	curOffSet = SpcIo_GetDataFromAp(curOffSet, sizeof(spcC2KEnCInfo), &DecInfo);
	MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 0x3520,0x030a,DecInfo.network,DecInfo.count);

	switch(DecInfo.network)
    {
     	case Set_Network_C2K:
		{

			if( SP_ENC.DL_C2K_after_Dec_buffer_count == MAX_DL_C2K_ENC_BUFFER_NUM )
   			{
   				SP_ENC.DL_C2K_after_Dec_buffer_read_index++;          //drop oldest frame
     			SP_ENC.DL_C2K_after_Dec_buffer_count--;
      	
    			if( SP_ENC.DL_C2K_after_Dec_buffer_read_index == MAX_DL_C2K_ENC_BUFFER_NUM){
      			SP_ENC.DL_C2K_after_Dec_buffer_read_index = 0;
    			}
				MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 2, 0x3520,0x0377);
   			}
			
			SpcIo_GetDataFromAp(curOffSet, (info.length-2), SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_write_index]);	
			MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 7, 0x3520,0x030b,SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_write_index][0],SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_write_index][1],SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_write_index][2],SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_write_index][3],SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_write_index][4]);
            SP_ENC.DL_C2K_after_Dec_buffer_write_index++;
			if(SP_ENC.DL_C2K_after_Dec_buffer_write_index== MAX_DL_C2K_ENC_BUFFER_NUM)
			{
				SP_ENC.DL_C2K_after_Dec_buffer_write_index = 0;
			}
			SP_ENC.DL_C2K_after_Dec_buffer_count++;
            MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5, 0x03520,0x030c,SP_ENC.DL_C2K_after_Dec_buffer_read_index,SP_ENC.DL_C2K_after_Dec_buffer_write_index);
			break;
		}
		
		default:			
			ASSERT(DecInfo.network, 0, 0);			
       		break;
     
	}
	


}


// run in IPC Task start
void PutC2KULOriData(uint16 *OriDataBuf,uint8 Len)
{

	int32 I;
	SP_ENC.UL_C2K_befor_Enc_buffer[0] = Set_Network_C2K;
	SP_ENC.UL_C2K_befor_Enc_buffer[1] = SP_ENC.Spc_C2K_UL_Dbg_Count;
	SP_ENC.Spc_C2K_UL_Dbg_Count =SP_ENC.Spc_C2K_UL_Dbg_Count+1;

 	  
   	if(SP_ENC.Spc_C2K_UL_Dbg_Count == 0xFF)
	{
	   SP_ENC.Spc_C2K_UL_Dbg_Count = 0;
	}
	SP_ENC.UL_C2K_befor_Enc_buffer[2] = Len; //length

	 for(I=0;I<Len;I++)
	 {
		SP_ENC.UL_C2K_befor_Enc_buffer[3+I]= OriDataBuf[I];
	 }

     Spc_SetVoiceEncryptionHandler(Set_C2K_Encrypt);
}

void PutC2KDLOriData(uint16 *OriDataBuf,uint8 Len)
{
	int32 I;
	SP_ENC.DL_C2K_befor_Dec_buffer[0] = Set_Network_C2K;
	SP_ENC.DL_C2K_befor_Dec_buffer[1] = SP_ENC.Spc_C2K_DL_Dbg_Count;
	SP_ENC.Spc_C2K_DL_Dbg_Count =SP_ENC.Spc_C2K_DL_Dbg_Count+1;

 	  
   	if(SP_ENC.Spc_C2K_DL_Dbg_Count == 0xFF)
	{
	   SP_ENC.Spc_C2K_DL_Dbg_Count = 0;
	}
	SP_ENC.DL_C2K_befor_Dec_buffer[2] = Len; //length

	 for(I=0;I<Len;I++)
	 {
		SP_ENC.DL_C2K_befor_Dec_buffer[3+I]= OriDataBuf[I];
	 }

     Spc_SetVoiceDecryptionHandler(Set_C2K_Decrypt);

}
//uint16 *EncDataBufTemp ;

bool GetC2KULEncData(uint16 *EncDataBuf ,uint8 *Len)
{
	 bool NO_Data;
	 uint16 ULDataLen;
	 uint16 I;
	
	 if(SP_ENC.UL_C2K_Delay_Count < 2)
	 {
	    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 0x03520,0x030d,SP_ENC.UL_C2K_Delay_Count);
	 	SP_ENC.UL_C2K_Delay_Count++;
		*Len = 0;
		NO_Data = false;
		

	 }
	 else if(SP_ENC.UL_C2K_Delay_Count==2 && SP_ENC.UL_C2K_after_Enc_buffer_count == 0)
	 {
	    *Len = 0;
	 	NO_Data = false;
	 }
	 else
	 {
	      ULDataLen = SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][0];
	      *Len = ULDataLen; 
           
           for( I = 0;I < ULDataLen;I++ ) 
           {
         		EncDataBuf[I] = SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][1+I];
           }
		
		 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 10, 0x03520,0x030e,EncDataBuf[0],EncDataBuf[1],EncDataBuf[2],EncDataBuf[3],EncDataBuf[4],EncDataBuf[5],EncDataBuf[6],EncDataBuf[7]);
		 
		 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 10, 0x03520,0x030f,SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][0],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][1],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][2],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][3],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][4],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][5],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][6],SP_ENC.UL_C2K_after_Enc_buffer[SP_ENC.UL_C2K_after_Enc_buffer_read_index][7]);

		if(SP_ENC.UL_C2K_after_Enc_buffer_count != 0)
        {
			SP_ENC.UL_C2K_after_Enc_buffer_count--;
        }
	    SP_ENC.UL_C2K_after_Enc_buffer_read_index++;
        if(SP_ENC.UL_C2K_after_Enc_buffer_read_index == MAX_UL_C2K_ENC_BUFFER_NUM )
        {
           	SP_ENC.UL_C2K_after_Enc_buffer_read_index = 0;
        }
		
		NO_Data = true;
	 }
	 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5, 0x3520,0x0310,SP_ENC.UL_C2K_after_Enc_buffer_count,SP_ENC.UL_C2K_after_Enc_buffer_read_index,SP_ENC.UL_C2K_after_Enc_buffer_write_index);
	 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 0x3520,0x0311,NO_Data,*Len);
	 return NO_Data;

	 

}

bool GetC2KDLDecData(uint16 *DecDataBuf ,uint8 *Len)
{
	 bool NO_Data;
	 uint16 DLDataLen;
	 uint16 I;
	 if(SP_ENC.DL_C2K_Delay_Count < 2)
	 {
	    MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 3, 0x3520,0x0312,SP_ENC.DL_C2K_Delay_Count);
	 	SP_ENC.DL_C2K_Delay_Count++;
		*Len = 0;
		NO_Data = false;
		

	 }
	 else if(SP_ENC.DL_C2K_Delay_Count==2 && SP_ENC.DL_C2K_after_Dec_buffer_count == 0)
	 {
	    *Len = 0;
	 	NO_Data = false;
	 }
	 else
	 {                       
	      DLDataLen = SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_read_index][0];
	     
	     *Len = DLDataLen; 
	 	
           for( I = 0;I < DLDataLen;I++ ) 
           {
         		DecDataBuf[I] = SP_ENC.DL_C2K_after_DEC_buffer[SP_ENC.DL_C2K_after_Dec_buffer_read_index][1+I];	
           }
		
		 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 7, 0x03520,0x0313,DecDataBuf[0],DecDataBuf[1],DecDataBuf[2],DecDataBuf[3],DecDataBuf[4]);
		if(SP_ENC.DL_C2K_after_Dec_buffer_count != 0)
        {
			SP_ENC.DL_C2K_after_Dec_buffer_count--;
        }
	    SP_ENC.DL_C2K_after_Dec_buffer_read_index++;
        if(SP_ENC.DL_C2K_after_Dec_buffer_read_index == MAX_DL_C2K_ENC_BUFFER_NUM )
        {
           	SP_ENC.DL_C2K_after_Dec_buffer_read_index = 0;
        }
		
		NO_Data = true;
	 }
	 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 5, 0x03520,0x0314,SP_ENC.DL_C2K_after_Dec_buffer_count,SP_ENC.DL_C2K_after_Dec_buffer_read_index,SP_ENC.DL_C2K_after_Dec_buffer_write_index);
	 MonTrace(MON_CP_HWD_SPH_PROMPT_TRACE_ID, 4, 0x03520,0x0315,NO_Data,*Len);
	 return NO_Data;

	 

}

void ResetEncInfo(void)
{
	SP_ENC.Spc_C2K_UL_Dbg_Count = 0;
	SP_ENC.Spc_C2K_DL_Dbg_Count = 0;
	SP_ENC.UL_C2K_after_Enc_buffer_write_index = 0;
    SP_ENC.UL_C2K_after_Enc_buffer_read_index = 0;
    SP_ENC.UL_C2K_after_Enc_buffer_count = 0;
    SP_ENC.DL_C2K_after_Dec_buffer_write_index = 0;
    SP_ENC.DL_C2K_after_Dec_buffer_read_index = 0 ;
    SP_ENC.DL_C2K_after_Dec_buffer_count = 0;
	SP_ENC.UL_C2K_Delay_Count = 0;
    SP_ENC.DL_C2K_Delay_Count = 0;
}

bool GetVoiceEncSwitch(void)
{

	return SP_ENC.Is_Voice_Encryption;
	
}

// run in IPC Task end
