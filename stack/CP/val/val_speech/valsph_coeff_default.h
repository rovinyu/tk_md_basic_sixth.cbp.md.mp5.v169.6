/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#ifndef _VAL_SPH_COEFF_DEFAULT_H_
#define _VAL_SPH_COEFF_DEFAULT_H_

#define BT_COMP_FILTER (0 << 15)
#define BT_SYNC_DELAY  86


// ------------- NB  ------------
// following constant inside ./custom/driver/common/aud_common.c
#if 0 // defined(__DUAL_MIC_SUPPORT__)
#define DEFAULT_SPEECH_NORMAL_MODE_PARA \
{ \
    96,  253, 16388,    31, 57351,    31,   400,     0, \
    80,  4325,   99,     0, 20488,   371,    23,  8192  \
}
#else

#define DEFAULT_SPEECH_NORMAL_MODE_PARA \
{ \
    128,  224, 5256,    31, 57351,    31,   400,     0, \
    80,  4325,   99,     0, 8200,     0,     0,  8192  \
}

#endif

#define DEFAULT_SPEECH_EARPHONE_MODE_PARA \
{ \
     0,   189, 10756,    31, 57351,    31,   400,     0, \
    80,  4325,    99,     0, 20488,     0,     0,     0  \
}

#define DEFAULT_SPEECH_BT_EARPHONE_MODE_PARA \
{ \
     0,   253, 10756,    31, 53255, 32799,   400,     0, \
    80,  4325,    99,     0, 20488 | BT_COMP_FILTER,  0,     0, BT_SYNC_DELAY  \
}

#define DEFAULT_SPEECH_LOUDSPK_MODE_PARA \
{ \
    96,   224,  5256,    31, 57351, 16407,   400,   132, \
    80,  4325,    99,     0, 20488,     0,     0,     0  \
}

#define DEFAULT_SPEECH_CARKIT_MODE_PARA \
{ \
    96,   224,  5256,    31, 57351, 16407,   400,   132, \
    80,  4325,    99,     0, 20488,     0,     0,     0  \
}

#define DEFAULT_SPEECH_BT_CORDLESS_MODE_PARA \
{ \
    0,      0,     0,     0,       0,     0,     0,     0, \
    0,      0,     0,     0,       0,     0,     0,     0  \
}

#define DEFAULT_SPEECH_AUX1_MODE_PARA \
{ \
    0,      0,     0,     0,       0,     0,     0,     0, \
    0,      0,     0,     0,       0,     0,     0,     0  \
}

#define DEFAULT_SPEECH_AUX2_MODE_PARA \
{ \
    0,      0,     0,     0,       0,     0,     0,     0, \
    0,      0,     0,     0,       0,     0,     0,     0  \
}

// --------- WB --------------

#if 0 // defined(__DUAL_MIC_SUPPORT__)
#define DEFAULT_WB_SPEECH_NORMAL_MODE_PARA \
{ \
    96,   253, 16388,    31, 57607,    31,   400,     0, \
    80,  4325,    99,     0, 16392,     0,     0,  8192  \
}
#else
#define DEFAULT_WB_SPEECH_NORMAL_MODE_PARA \
{ \
    128,   224, 5256,    31, 57607,    31,   400,     0, \
    80,  4325,    99,     0, 8200,     0,     0,  8192  \
}
#endif

#define DEFAULT_WB_SPEECH_EARPHONE_MODE_PARA \
{ \
     0,   189, 10756,    31, 57607,    31,   400,     0, \
    80,  4325,    99,     0, 16392,     0,     0,     0  \
}

#define DEFAULT_WB_SPEECH_BT_EARPHONE_MODE_PARA \
{ \
     0,   253, 10756,    31, 53511, 32799,   400,     0, \
    80,  4325,    99,     0, 16392 | BT_COMP_FILTER,  0,     0, BT_SYNC_DELAY  \
}

#define DEFAULT_WB_SPEECH_LOUDSPK_MODE_PARA \
{ \
    96,   224,  5256,    31, 57607, 16407,   400,   132, \
    80,  4325,    99,     0, 16392,     0,     0,     0  \
}

#define DEFAULT_WB_SPEECH_CARKIT_MODE_PARA \
{ \
    96,   224,  5256,    31, 57607, 16407,   400,   132, \
    80,  4325,    99,     0, 16392,     0,     0,     0  \
}

#define DEFAULT_WB_SPEECH_BT_CORDLESS_MODE_PARA \
{ \
    0,      0,     0,     0,       0,     0,     0,     0, \
    0,      0,     0,     0,       0,     0,     0,     0  \
}

#define DEFAULT_WB_SPEECH_AUX1_MODE_PARA \
{ \
    96,    224,  5256,    30, 57351,     0,   400,     0,  \
    4112, 4325,    11,     0,     0,     0,     0,     0   \
}

#define DEFAULT_WB_SPEECH_AUX2_MODE_PARA \
{ \
    0,      0,     0,     0,       0,     0,     0,     0, \
    0,      0,     0,     0,       0,     0,     0,     0  \
}

// ============================================================================
// DMNR
// ============================================================================

#define DEFAULT_DMNR_PARAM \
{ \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	68, 0, 0, 0 \
}

#define DEFAULT_WB_DMNR_PARAM \
{ \
	460, 470, 480, 490, 500, \
	510, 520, 530, 540, 550, \
	560, 570, 580, 590, 600, \
	610, 620, 630, 640, 650, \
	660, 670, 680, 690, 700, \
	710, 720, 730, 740, 750, \
	760, 770, 780, 790, 800, \
	810, 820, 830, 840, 850, \
	860, 870, 880, 890, 900, \
	910, 920, 930, 940, 950, \
	960, 970, 980, 990, 1000, \
	1010, 1020, 1030, 1040, 1050, \
	1060, 1070, 1080, 1090, 1200, \
	1210, 1220, 1230, 1240, 1250, \
	1260, 1270, 1280, 1290, 1300, \
	1310		\
}

#define DEFAULT_LSPK_DMNR_PARAM \
{ \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	0, 0, 0, 0, 0, \
	68, 0, 0, 0 \
}

#define DEFAULT_LSPK_WB_DMNR_PARAM \
{ \
	460, 470, 480, 490, 500, \
	510, 520, 530, 540, 550, \
	560, 570, 580, 590, 600, \
	610, 620, 630, 640, 650, \
	660, 670, 680, 690, 700, \
	710, 720, 730, 740, 750, \
	760, 770, 780, 790, 800, \
	810, 820, 830, 840, 850, \
	860, 870, 880, 890, 900, \
	910, 920, 930, 940, 950, \
	960, 970, 980, 990, 1000, \
	1010, 1020, 1030, 1040, 1050, \
	1060, 1070, 1080, 1090, 1200, \
	1210, 1220, 1230, 1240, 1250, \
	1260, 1270, 1280, 1290, 1300, \
	1310		\
}

// ============================================================================
// FIR
// ============================================================================

/******************** INPUT FIR ********************************/


#define SPEECH_INPUT_FIR_COEFF_NORMAL_DEFAULT \
   { /* 0: Input FIR coefficients for 2G/3G Normal mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_INPUT_FIR_COEFF_HEADSET_DEFAULT \
   { /* 1: Input FIR coefficients for 2G/3G/VoIP Headset mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_INPUT_FIR_COEFF_HANDFREE_DEFAULT \
   { /* 2: Input FIR coefficients for 2G/3G Handfree mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_INPUT_FIR_COEFF_BT_DEFAULT \
   { /* 3: Input FIR coefficients for 2G/3G/VoIP BT mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_INPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT \
   { /* 4: Input FIR coefficients for VoIP Normal mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_INPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
   { /* 5: Input FIR coefficients for VoIP Handfree mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_INPUT_FIR_COEFF_DEFAULT \
{ \
    SPEECH_INPUT_FIR_COEFF_NORMAL_DEFAULT, \
    SPEECH_INPUT_FIR_COEFF_HEADSET_DEFAULT, \
    SPEECH_INPUT_FIR_COEFF_HANDFREE_DEFAULT, \
    SPEECH_INPUT_FIR_COEFF_BT_DEFAULT, \
    SPEECH_INPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT, \
    SPEECH_INPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
}

/******************** OUTPUT FIR ********************************/

#define SPEECH_OUTPUT_FIR_COEFF_NORMAL_DEFAULT \
   { /* 0: Output FIR coefficients for 2G/3G Normal mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_OUTPUT_FIR_COEFF_HEADSET_DEFAULT \
   { /* 1: Output FIR coefficients for 2G/3G/VoIP Headset mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_OUTPUT_FIR_COEFF_HANDFREE_DEFAULT \
   { /* 2: Output FIR coefficients for 2G/3G Handfree mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_OUTPUT_FIR_COEFF_BT_DEFAULT \
   { /* 3: Output FIR coefficients for 2G/3G/VoIP BT mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_OUTPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT \
   { /* 4: Output FIR coefficients for VoIP Normal mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_OUTPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
   { /* 5: Output FIR coefficients for VoIP Handfree mode */ \
    32767,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0  \
   }

#define SPEECH_OUTPUT_FIR_COEFF_DEFAULT \
{ \
    SPEECH_OUTPUT_FIR_COEFF_NORMAL_DEFAULT, \
    SPEECH_OUTPUT_FIR_COEFF_HEADSET_DEFAULT, \
    SPEECH_OUTPUT_FIR_COEFF_HANDFREE_DEFAULT, \
    SPEECH_OUTPUT_FIR_COEFF_BT_DEFAULT, \
    SPEECH_OUTPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT, \
    SPEECH_OUTPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
}

/******************** WB INPUT FIR ********************************/

// #ifdef __AMRWB_LINK_SUPPORT__

#define WB_SPEECH_INPUT_FIR_COEFF_NORMAL_DEFAULT \
   { /* 0: Input FIR coefficients for 2G/3G Normal mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_INPUT_FIR_COEFF_HEADSET_DEFAULT \
   { /* 1: Input FIR coefficients for 2G/3G/VoIP Headset mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_INPUT_FIR_COEFF_HANDFREE_DEFAULT \
   { /* 2: Input FIR coefficients for 2G/3G Handfree mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_INPUT_FIR_COEFF_BT_DEFAULT \
   { /* 3: Input FIR coefficients for 2G/3G/VoIP BT mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_INPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT \
   { /* 4: Input FIR coefficients for VoIP Normal mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_INPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
   { /* 5: Input FIR coefficients for VoIP Handfree mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_INPUT_FIR_COEFF_DEFAULT \
{ \
    WB_SPEECH_INPUT_FIR_COEFF_NORMAL_DEFAULT, \
    WB_SPEECH_INPUT_FIR_COEFF_HEADSET_DEFAULT, \
    WB_SPEECH_INPUT_FIR_COEFF_HANDFREE_DEFAULT, \
    WB_SPEECH_INPUT_FIR_COEFF_BT_DEFAULT, \
    WB_SPEECH_INPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT, \
    WB_SPEECH_INPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
}

// #endif //__AMRWB_LINK_SUPPORT__

/******************** WB OUTPUT FIR ********************************/

// #ifdef __AMRWB_LINK_SUPPORT__

#define WB_SPEECH_OUTPUT_FIR_COEFF_NORMAL_DEFAULT \
   { /* 0: Output FIR coefficients for 2G/3G Normal mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_OUTPUT_FIR_COEFF_HEADSET_DEFAULT \
   { /* 1: Output FIR coefficients for 2G/3G/VoIP Headset mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_OUTPUT_FIR_COEFF_HANDFREE_DEFAULT \
   { /* 2: Output FIR coefficients for 2G/3G Handfree mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_OUTPUT_FIR_COEFF_BT_DEFAULT \
   { /* 3: Output FIR coefficients for 2G/3G/VoIP BT mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_OUTPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT \
   { /* 4: Output FIR coefficients for VoIP Normal mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_OUTPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
   { /* 5: Output FIR coefficients for VoIP Handfree mode */ \
    32767,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0, \
        0,     0,     0,     0,     0,     0,     0,     0,     0,     0 \
   }

#define WB_SPEECH_OUTPUT_FIR_COEFF_DEFAULT \
{ \
    WB_SPEECH_OUTPUT_FIR_COEFF_NORMAL_DEFAULT, \
    WB_SPEECH_OUTPUT_FIR_COEFF_HEADSET_DEFAULT, \
    WB_SPEECH_OUTPUT_FIR_COEFF_HANDFREE_DEFAULT, \
    WB_SPEECH_OUTPUT_FIR_COEFF_BT_DEFAULT, \
    WB_SPEECH_OUTPUT_FIR_COEFF_VOIP_NORMAL_DEFAULT, \
    WB_SPEECH_OUTPUT_FIR_COEFF_VOIP_HANDFREE_DEFAULT \
}

// #endif //__AMRWB_LINK_SUPPORT__

#endif // _VAL_SPH_COEFF_DEFAULT_H_
