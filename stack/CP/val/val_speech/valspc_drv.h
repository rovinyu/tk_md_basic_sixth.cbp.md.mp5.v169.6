/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * spc_drv.h
 *
 * Project:
 * --------
 * MAUI
 *
 * Description:
 * ------------
 * MD speech control interfaces 
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:   1.85  $
 * $Modtime:   Jul 27 2005 09:18:28  $
 * $Log:   //mtkvs01/vmdata/Maui_sw/archives/mcu/l1audio/l1sp.c-arc  $
 *
 * 10 13 2015 ting-ni.chen
 * [SIXTH00004825] [GINR6753_65C_L1][Z160][modem]使用过程中弹出modem重启提示
 * acoustic loopback
 *
 * 04 13 2015 ys.hsieh
 * [SIXTH00002536] [Denali-1][MT6735][Stage1][CSFB DSDS][Free test] When flight mode on / off UE happened "After AP send EPOF, MD didn't go to sleep in 4 seconds" exception
 * CBP c2k speech EPOF flight mode
 *
 * 10 22 2014 fu-shing.ju
 * [MOLY00078649] Phone Call Recording Quality Enhancement.
 * Phone Call Recording Quality Enhancement.
 *
 * 09 24 2013 thomas.chen
 * [MOLY00038854] CCCI over SDIO has the limitation of maximun 5k throughput
 * .
 *
 * 07 22 2013 sheila.chen
 * [MOLY00028293] [MT6280] Super Dongle Integration
 * super dongle merge back
 * 1. pcm start volume
 * 2. fix src coefficient by DSP's request
 * 3. fix bgSnd timer name
 * 4. WB parameter position adding (80) [Rimind to ask 90]
 * 5. fix build warning
 * 6. when bt cordless mode fors
 * 7. add daca interface
 * 8. fix spc log typo
 *
 * 04 29 2013 scholar.chang
 * [MOLY00020926] [Speech]DVT test code check-in
 * .
 *
 * 12 03 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Add CTM debug log
 *
 * 11 20 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * 1. Idel VM with EPL bug fix
 * 
 * 2. Warnming Remove
 * 
 * 3. Dynamic feed in the enhancment parameter
 * 
 * 09 18 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Add default value for speech parameters. Add option for phone call
 * 
 * 09 10 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * Phase-in VM/VOC basic
 * 
 * 09 04 2012 sheila.chen
 * [MOLY00000112] [MT6583] Pre-integration
 * fix warning
 *
 * 07 16 2012 sheila.chen
 * NULL
 * phase in 75+80.
 * 
 * 
 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/

#ifndef _SPC_DRV_H
#define _SPC_DRV_H

#include "hwdctm_drv.h"
// #include "sp_drv.h"

typedef struct spcBufInfoStruct 
{
   uint16 syncWord;
   uint16 type;
   uint16 length;
} spcBufInfo;
typedef struct spcC2KEncInfoStruct 
{
   uint16 network;
   uint16 count;
} spcC2KEnCInfo;

typedef struct spcExtendedBufInfoStruct 
{
   uint16 syncWord;
   uint16 type;
   uint16 length;
   uint16 curIdx;
   uint16 totalIdx;      
} spcExtendedBufInfo;
typedef enum {
	Set_2G_Encrypt = 0, 
	Set_3G_Encrypt, 
	Set_4G_AMR_Encrypt,  
	Set_4G_EVS_Encrypt,  
	Set_4G_G_Encrypt,  
	Set_C2K_Encrypt, 
	Set_2G_Decrypt, 
	Set_3G_Decrypt, 
	Set_4G_AMR_Decrypt, 
	Set_4G_EVS_Decrypt, 
	Set_4G_G_Decrypt, 
	Set_C2K_Decrypt,
} SPC_NETWOEK_TYPE;
typedef enum {
	Set_Network_2G = 0, 
	Set_Network_3G, 
	Set_Network_4G_AMR,  
	Set_Network_4G_G,  
	Set_Network_4G_EVS,  
	Set_Network_C2K,
} SPC_NETWOEK_INFO;

#define UL_C2K_ENC_MAX_DATA_SIZE  24 //  22+2(network info+len)
#define DL_C2K_DEC_MAX_DATA_SIZE  24 // 22+2 (network info+len)
#define MAX_UL_C2K_ENC_BUFFER_NUM  2
#define MAX_DL_C2K_ENC_BUFFER_NUM  2
typedef struct SP_ENC_Struct{
   bool     Is_Voice_Encryption;
   uint16    UL_C2K_befor_Enc_buffer[UL_C2K_ENC_MAX_DATA_SIZE];
   uint16    DL_C2K_befor_Dec_buffer[DL_C2K_DEC_MAX_DATA_SIZE];
   uint16    UL_C2K_after_Enc_buffer[MAX_UL_C2K_ENC_BUFFER_NUM][UL_C2K_ENC_MAX_DATA_SIZE];
   uint16    DL_C2K_after_DEC_buffer[MAX_DL_C2K_ENC_BUFFER_NUM][DL_C2K_DEC_MAX_DATA_SIZE];

   uint8    UL_C2K_after_Enc_buffer_write_index;
   uint8    UL_C2K_after_Enc_buffer_read_index;
   uint8    UL_C2K_after_Enc_buffer_count;
   uint8    DL_C2K_after_Dec_buffer_write_index;
   uint8    DL_C2K_after_Dec_buffer_read_index;
   uint8    DL_C2K_after_Dec_buffer_count;
   uint8    Spc_C2K_UL_Dbg_Count;
   uint8    Spc_C2K_DL_Dbg_Count;
   uint8    UL_C2K_Delay_Count;
   uint8    DL_C2K_Delay_Count;
   
} _SP_ENC; // voice encryption structure

void Spc_SpeechOn( uint8 RAT_Mode );
void Spc_SpeechOff( void );

// ----------------------------------------------------------------------------
// Datacard
// ----------------------------------------------------------------------------
void spc_daca_sendDlData(void);
void spc_daca_requestUlData(void);

// ----------------------------------------------------------------------------
// PCMNWAY
// ----------------------------------------------------------------------------
#if defined(__ENABLE_SPEECH_DVT__)
void spc_pcmNWay_sendUlData(uint16 bufLen);
void spc_pcmNWay_requestDlData(uint16 bufLen);
#else // defined(__ENABLE_SPEECH_DVT__)
void spc_pcmNWay_sendUlData(void);
void spc_pcmNWay_requestDlData(void);
#endif // defined(__ENABLE_SPEECH_DVT__)

// ----------------------------------------------------------------------------
// Background Sound
// ----------------------------------------------------------------------------
void spc_bgSnd_requestData(void);
void spc_BgSndClose(void);

// ----------------------------------------------------------------------------
// Recording
// ----------------------------------------------------------------------------
void spc_record_sendPcmData(void);
void spc_record_sendVmData(void);
void spc_record_sendRawPcmData(void);


// ----------------------------------------------------------------------------
// CTM
// ----------------------------------------------------------------------------
void spc_ctm_sendDumpDebugData(void);

// ----------------------------------------------------------------------------
// Codec Info 
// ----------------------------------------------------------------------------
void SPC_ServiceOptionNotify(void *data);
void SPC_RateReduceNotify(void *data);



// ----------------------------------------------------------------------------
// AUDL running functions Related
// ----------------------------------------------------------------------------
void Spc_Init(void); 
void spc_A2M_MsgHandler(uint32 ccciMsg, uint32 ccciResv);
void spc_AP_Command_Handler(uint32 ccciMsg, uint32 ccciMsgRev); //#ifdef SPC_MSG_ORG_VER	
void Spc_ForceEndAllApp(void);
uint16 get_spcGetEpofTimes(void);

void Spc_AcousticLoopback(uint8 uParam, uint32 extraParam);
void spc_CtmStart(L1Ctm_Interface mode);
void spc_CtmStop(void);
void spc_customDump(uint16 bufLen, uint16 *dumpBuf);
void spc_sendCustomDump(void *ilm);
// ----------------------------------------------------------------------------
// Voice Encryption
// ----------------------------------------------------------------------------

void Spc_VoiceEncryptionSwitch(bool enable);
void Spc_SetVoiceEncryptionHandler(SPC_NETWOEK_TYPE network);
void Spc_SetVoiceDecryptionHandler(SPC_NETWOEK_TYPE network);
void Spc_GetVoiceEncryptionHandler(kal_uint16 offset, kal_uint16 length);
void Spc_GetVoiceDecryptionHandler(kal_uint16 offset, kal_uint16 length);
void spc_Set_C2KULDataEnc(void);
void spc_SetC2KDLDataDec(void); 
void PutC2KULOriData(uint16 *OriDataBuf,uint8 Len);
void PutC2KDLOriData(uint16 *OriDataBuf,uint8 Len);
bool GetC2KULEncData(uint16 *EncDataBuf , uint8 *Len);
bool GetC2KDLDecData(uint16 *DecDataBuf ,uint8 *Len);
bool GetVoiceEncSwitch(void);
void ResetEncInfo(void);
void spc_SetC2KULDataEnc(void);
#endif // _SPC_DRV_H
