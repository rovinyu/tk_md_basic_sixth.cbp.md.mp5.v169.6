/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2005
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*******************************************************************************
 *
 * Filename:
 * ---------
 * spc_io.h
 *
 * Project:
 * --------
 * MAUI
 *
 * Description:
 * ------------
 * IO interface between AP and MD speech driver, and AUDL basic function structure
 *
 * Author:
 * -------
 * 
 *
 *==============================================================================
 *             HISTORY
 * Below this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *------------------------------------------------------------------------------
 * $Revision:   1.85  $
 * $Modtime:   Jul 27 2005 09:18:28  $
 * $Log:   //mtkvs01/vmdata/Maui_sw/archives/mcu/l1audio/l1sp.c-arc  $
 *
 * 07 15 2015 ys.hsieh
 * [SIXTH00003582] [JADE][MT6755][SPE] New SPE architecture porting
 * 0x2f79 cmd bypass srlte
 *
 * 04 13 2015 ys.hsieh
 * [SIXTH00002536] [Denali-1][MT6735][Stage1][CSFB DSDS][Free test] When flight mode on / off UE happened "After AP send EPOF, MD didn't go to sleep in 4 seconds" exception
 * CBP c2k speech EPOF flight mode
 *
 * 09 29 2014 sheila.chen
 * [MOLY00078029] [K2] [HAC]
 * HAC
 *

 *
 *------------------------------------------------------------------------------
 * Upper this line, this part is controlled by PVCS VM. DO NOT MODIFY!!
 *==============================================================================
 *******************************************************************************/
#ifndef _SPC_IO_H_
#define _SPC_IO_H_


#include "valaudio_enum.h"
#include "valspherr.h"

typedef enum {
	
	SPCIO_MSG_FROM_SPC_SPH_ON_ACK = 0x20, 
	SPCIO_MSG_FROM_SPC_SPH_OFF_ACK, 
	SPCIO_MSG_FROM_SPC_SET_SPH_MODE_ACK,
	SPCIO_MSG_FROM_SPC_CTRL_SPH_ENH_ACK,
	SPCIO_MSG_FROM_SPC_CONFIG_SPH_ENH_ACK,
	SPCIO_MSG_FROM_SPC_SET_ACOUSTIC_LOOPBACK_ACK,
	SPCIO_MSG_FROM_SPC_QUERY_MD_CAPABILITY_ACK, 
	SPCIO_MSG_FROM_SPC_SET_MD_CAPABILITY_ACK, 
	SPCIO_MSG_FROM_SPC_PRINT_SPH_COEFF_ACK,
	SPCIO_MSG_FROM_SPC_SPH_ON_FOR_HOLD_CALL_ACK,
	SPCIO_MSG_FROM_SPC_SPH_ON_FOR_DACA_ACK,
	SPCIO_MSG_FROM_SPC_SPH_ROUTER_ON_ACK,
	SPCIO_MSG_FROM_SPC_SET_VOICE_ENCRYPTION_ACK,

	SPCIO_MSG_FROM_SPC_PNW_ON_ACK = 0x30,
	SPCIO_MSG_FROM_SPC_PNW_OFF_ACK,
	SPCIO_MSG_FROM_SPC_RECORD_ON_ACK,
	SPCIO_MSG_FROM_SPC_RECORD_OFF_ACK,
	SPCIO_MSG_FROM_SPC_DMNR_RECPLAY_ON_ACK,
	SPCIO_MSG_FROM_SPC_DMNR_RECPLAY_OFF_ACK,
	SPCIO_MSG_FROM_SPC_DMNR_REC_ONLY_ON_ACK,
	SPCIO_MSG_FROM_SPC_DMNR_REC_ONLY_OFF_ACK,
	SPCIO_MSG_FROM_SPC_PCM_REC_ON_ACK,
	SPCIO_MSG_FROM_SPC_PCM_REC_OFF_ACK,
	SPCIO_MSG_FROM_SPC_VM_REC_ON_ACK,
	SPCIO_MSG_FROM_SPC_VM_REC_OFF_ACK,
	SPCIO_MSG_FROM_SPC_RECORD_RAW_PCM_ON_ACK,
	SPCIO_MSG_FROM_SPC_RECORD_RAW_PCM_OFF_ACK,
	
	SPCIO_MSG_FROM_SPC_CTM_ON_ACK = 0x40,
	SPCIO_MSG_FROM_SPC_CTM_OFF_ACK,
	SPCIO_MSG_FROM_SPC_CTM_DUMP_DEBUG_FILE_ACK,
	SPCIO_MSG_FROM_SPC_BGSND_ON_ACK,
	SPCIO_MSG_FROM_SPC_BGSND_OFF_ACK,
	SPCIO_MSG_FROM_SPC_BGSND_CONFIG_ACK,

	SPCIO_MSG_FROM_SPC_PNW_DLDATA_REQUEST = 0x50,
	SPCIO_MSG_FROM_SPC_BGS_DATA_REQUEST,
	SPCIO_MSG_FROM_SPC_CTM_DATA_REQUEST,
	SPCIO_MSG_FROM_SPC_DACA_ULDATA_REQUEST,
	SPCIO_MSG_FROM_SPC_MSD_DATA_ACK,
	SPCIO_MSG_FROM_SPC_UL_ENC_REQUEST,
	SPCIO_MSG_FROM_SPC_DL_DEC_REQUEST,
	
	SPCIO_MSG_FROM_SPC_PNW_ULDATA_NOTIFY = 0x60,
	SPCIO_MSG_FROM_SPC_REC_DATA_NOTIFY,
	SPCIO_MSG_FROM_SPC_CTM_DEBUG_DATA_NOTIFY,
	SPCIO_MSG_FROM_SPC_PCM_REC_DATA_NOTIFY,
	SPCIO_MSG_FROM_SPC_VM_REC_DATA_NOTIFY,
	SPCIO_MSG_FROM_SPC_DACA_DLDATA_NOTIFY,
	SPCIO_MSG_FROM_SPC_RAW_PCM_REC_DATA_NOTIFY, //0x60 + 0x6
	SPCIO_MSG_FROM_SPC_CUST_DUMP_NOTIFY,

	SPCIO_MSG_FROM_SPC_EM_DATA_REQUEST = 0x70,
	SPCIO_MSG_FROM_SPC_EM_INCALL_ACK,
	SPCIO_MSG_FROM_SPC_EM_DMNR_ACK,
	SPCIO_MSG_FROM_SPC_EM_WB_ACK,  
	SPCIO_MSG_FROM_SPC_EM_MAGICON_ACK,  
	SPCIO_MSG_FROM_SPC_NETWORK_STATUS_NOTIFY,
	SPCIO_MSG_FROM_SPC_RF_INFO_NOTIFY,
	SPCIO_MSG_FROM_SPC_EM_HAC_ACK,  
	SPCIO_MSG_FROM_SPC_EPOF_NOTIFY,
	SPCIO_MSG_FROM_SPC_DYNAMIC_PAR_ACK,
	
	SPCIO_MSG_FROM_SPC_VIBSPK_PARAMETER_ACK = 0x80,
	SPCIO_MSG_FROM_SPC_NXP_SMARTPA_PARAMETER_ACK,

	SPCIO_MSG_FROM_SPC_NW_CODEC_INFO_NOTIFY = 0x90,
}SPCIO_MSG_FROM_SPC;

// ----------------------------------------------------------------------------
// Macro function
// ----------------------------------------------------------------------------

// extend version
#define SPCIO_CCCI_MSG_CMD(msg) ((msg) >> 16)
#define SPCIO_CCCI_MSG_DATA16(msg) ((msg) & 0xffffU)
#define SPCIO_CCCI_MSG_CONSTRCUT_CMD(msg, data16) ( ((msg)<<16) | (0xffffU&(data16)) )
#define SPCIO_CCCI_MSG_CONSTRCUT_DATA_CMD(msg, leng) SPCIO_CCCI_MSG_CONSTRCUT_CMD(msg, leng)
#define SPCIO_CCCI_MSG_CONSTRUCT_DATA_CMD_WO_SHIFT(typefuncData, length) ( ( (typefuncData)&0xFFFF0000U) | ((length)&0xffffU) )


// ----------------------------------------------------------------------------
// function for AUDL thread putting
// ----------------------------------------------------------------------------
/**
	@prSmpMsg:
	@comeFrom: 0 for trigger from CCCI_HISR, 1 for trigger from AUDL. (you can add more in the future
*/
void SpcIO_MsgQueuePut(SPC_MSG_ID_T msgId, uint32 msgData, uint32 msgDataRev, uint8 callerFrom);
// ----------------------------------------------------------------------------
// function for msg send/receive
// ----------------------------------------------------------------------------
uint16 SpcIo_GetDataFromAp(const uint16 offset, const uint16 length, void *buf);
void SpcIO_GetDataFromAp_inOneTime(uint16 offset, uint16 length, int16 headerLen,
	void *header, void *buf);
bool SpcIO_WriteDataToAp(void *headerBuf, int16 headerLen, 
	void *srcBuf1, int16 srcLen1, void *srcBuf2, int16 srcLen2, SPCIO_MSG_FROM_SPC spcIoMsg);
bool SpcIO_SendMsgToAp(SPCIO_MSG_FROM_SPC spcIoMsg, uint32 data1, uint32 reserve);


// ==========================================================
// ==========================================================
// ==========================================================

/*
============================================================================================================
------------------------------------------------------------------------------------------------------------
||                       Ring buffer handling
------------------------------------------------------------------------------------------------------------
============================================================================================================
*/
/*
   Example: define a ring buffer of 8 integers (size must be a power of two)
   , put the value 3 and get it again:
   
   RINGBUFFER_T(int,8) myBuffer;
   RB_PUT( myBuffer, 3 );

   int value;
   RB_GET( myBuffer, value );
*/


#ifndef RINGBUFFER_T
#define RINGBUFFER_T(elemtype,size) \
   struct \
   { \
      volatile uint32 write; /* must be unsigned */ \
      volatile uint32 read;  /* must be unsigned */ \
      elemtype volatile queue[size]; \
   }

#define RB_SIZE( rb ) \
   ( sizeof( rb.queue ) / sizeof( *rb.queue ) )

#define RB_MASK( rb ) \
   ( RB_SIZE( rb ) - 1 )

#define RB_INIT( rb ) \
   ( rb.read = rb.write = 0 )

#define RB_COUNT( rb ) \
   ( rb.write - rb.read )

#define RB_FULL( rb ) \
   ( RB_COUNT( rb ) >= RB_SIZE( rb ) )

#define RB_EMPTY( rb ) \
   ( rb.write == rb.read )

#define RB_PUT( rb, value ) \
{ \
   ASSERT( !RB_FULL( rb ) , VALSPH_ERR_FORCE_ASSERT, 0); \
   rb.queue[ rb.write & RB_MASK( rb ) ] = value; \
   ++rb.write; \
}

#define RB_GET( rb, value ) \
{ \
   ASSERT( !RB_EMPTY( rb ) , VALSPH_ERR_FORCE_ASSERT, 0); \
   value = rb.queue[ rb.read & RB_MASK( rb ) ]; \
   ++rb.read; \
}

#define RB_PEEK( rb, value ) \
{ \
   ASSERT( !RB_EMPTY( rb ) , VALSPH_ERR_FORCE_ASSERT, 0); \
   value = rb.queue[ rb.read & RB_MASK( rb ) ]; \
}

#define RB_PEEK_THROUGH( rb, idx, value ) \
{ \
   ASSERT( (rb.read+idx) < rb.write , VALSPH_ERR_FORCE_ASSERT, 0); \
   value = rb.queue[ (rb.read+idx) & RB_MASK( rb ) ]; \
}

#define RB_CONSUME( rb ) \
{ \
   ASSERT( !RB_EMPTY( rb ) , VALSPH_ERR_FORCE_ASSERT, 0); \
   ++rb.read; \
}

#define RB_UNPUT( rb, value ) \
{ \
   ASSERT( !RB_EMPTY( rb ) , VALSPH_ERR_FORCE_ASSERT, 0); \
   --rb.write; \
   value = rb.queue[ rb.write & RB_MASK( rb ) ]; \
}

#define RB_UNGET( rb, value ) \
{ \
   ASSERT( !RB_FULL( rb ) , VALSPH_ERR_FORCE_ASSERT, 0); \
   --rb.read; \
   rb.queue[ rb.read & RB_MASK( rb ) ] = value; \
}

#endif /* RINGBUFFER_T */

// ----------------------------------------------------------------------------
// Macro or Definition for SDIO CCCI
// ----------------------------------------------------------------------------
#if defined(MTK_PLT_AUDIO) 
typedef struct
{
    uint32 data[2];
    uint32 channel;
    uint32 reserved;
} SPCIO_CCCI_BUFF_T;

#define SPCIO_IS_MAIL_BOX(buff)   ((((SPCIO_CCCI_BUFF_T *)(buff))->data[0] == 0xFFFFFFFF)? 1: 0)
#define SPCIO_MAILBOX_ID(buff)   (((SPCIO_CCCI_BUFF_T *)(buff))->data[1])
#define SPCIO_STREAM_ADDR(buff) (((SPCIO_CCCI_BUFF_T *)(buff))->data[0])
#define SPCIO_STREAM_LEN(buff) (((SPCIO_CCCI_BUFF_T *)(buff))->data[1])
#define SPCIO_PCM_CHANNEL_MSG 0x22224444
#define SPCIO_PCM_CHANNEL_DATA 0x33336666
#define SPCIO_SUCCESS IopDataChRetOK

void SpcIo_ProcessIopSignal(void);
void SpcIO_Init(void);
void SpcIO_CheckSpeechInitDone(void);
void SpcIO_Msg_Handler_inAudL(void);
#ifdef SYS_OPTION_IOP_CCIF
void SpcIO_CCCIInitDone(void);
#endif

#endif // defined(MTK_PLT_AUDIO) 

#if 0 //defined(MTK_CBP) && (!defined(MTK_PLT_ON_PC))
#define VALSPC_CHIP_BACK_PHONECALL_USE
#endif
#endif // _SPC_IO_H_

