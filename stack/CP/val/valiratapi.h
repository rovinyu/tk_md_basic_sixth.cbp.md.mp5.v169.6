/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2016
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

#include "iratapi.h"
/*************************************************************
*
* This Software is the property of VIA Telecom, Inc. and may only be used pursuant to a license from VIA Telecom, Inc.
*
* Any unauthorized use inconsistent with the terms of such license is strictly prohibited.
*
* Copyright (c) 2010 VIA Telecom, Inc.  All rights reserved.
*
*************************************************************/
#ifndef _VALIRATAPI_H_
#define _VALIRATAPI_H_  1
#ifdef __cplusplus
extern "C" {
#endif


#ifndef MTK_DEV_C2K_IRAT
#ifndef _C2K_IRAT_MSG_STRUCT_H
#define LOCAL_PARA_HDR
#include "cross_core/c2k_irat_msg_struct.h"
#endif
#endif

#include "../shared/cross_core_umoly/c2k_irat_msg_struct.h"
#include "../shared/cross_core_umoly/c2k_sms_struct.h"
#include "../shared/cross_core_umoly/vdm_val_struct.h"

#ifdef MTK_CBP
#include "../shared/cross_core_umoly/c2k_csm_enums.h"
#include "../shared/cross_core_umoly/c2k_csm_struct.h"
#endif


/*****************************************************************************

  FILE NAME: valiratapi.h

  DESCRIPTION:

    This file contains IRAT API definition. Any change to this file
    shall be alerted to both VIA and SS.

*****************************************************************************/

#ifdef MTK_CBP //MTK_DEV_C2K_IRAT
#include "uimapi.h"

#include "iratapi.h"
#include "do_ims.h"

/* to enable CGI report fucntion */
#define IRAT_SUPPORT_CGI 1

#ifndef GEN_FOR_PC
#ifdef MTK_DEV_C2K_IRAT

#define BURST_TYPE_SMS                      0x03

#define C2K_XCAL_SMS_DEFAULT_MSG_NUMBER     1

#define C2K_XCAL_SMS_DEFAULT_NUM_MSGS       1

#define C2K_XCAL_DEFAULT_REF_COUNT          1

#define C2K_XCAL_DEFAULT_LP_RESERVED        0


/*Note: GEN_FOR_PC is used to avoid gen_nvram_database error, has been confirm with Richard Cui.*/

/* A copy from MD1.
** If merge to 93 in the future, we should remove this definition.*/
typedef enum
{
    EM_OFF = 0,
    EM_ON  = 1,
    EM_NC  = 2 /* No change */
} em_info_status_enum;

/* EM_IDs of C2K_XCAL, it's a subset of MD1 EM IDs.
** If merge to 93 in the future, we should remove this definition.*/
typedef enum
{
    /* C2K XCAL related EMs, maximum 30 EMs supported. */
    EM_C2K_XCAL_INFO_BEGIN = 980,
    EM_C2K_XCAL_OTA_EVENT_INFO = EM_C2K_XCAL_INFO_BEGIN,
    EM_C2K_XCAL_OTA_FDSCH_INFO = 981,
    EM_C2K_XCAL_OTA_RDSCH_INFO = 982,
    /* Note: wew C2K EMs should be added before  EM_C2K_XCAL_INFO_LAST, */
    EM_C2K_XCAL_INFO_LAST = EM_C2K_XCAL_OTA_RDSCH_INFO,
    EM_C2K_XCAL_INFO_END = EM_C2K_XCAL_INFO_BEGIN + 29,
    INVALID_EM_INFO = 0x7fffffff //end tag force this enum 4 bytes, for alignment purpose. Don't remove
} em_info_enum;


#include "../shared/cross_core_umoly/c2k_xcal_struct.h"


typedef struct
{
   kal_uint8	ref_count;
   kal_uint8    lp_reserved;
   kal_uint16	msg_len;
   c2k_xcal_ota_msg_type_enum event;
} em_c2k_xcal_ota_event_info_ind_struct_with_header;

/* Data burst message of  F-DSCH and R-DSCH.*/
typedef struct
{
  kal_uint8	    ref_count;
  kal_uint8     lp_reserved;
  kal_uint16	msg_len;
  kal_uint8     msg_number;
  kal_uint8     burst_type;
  kal_uint8     num_msgs;
  kal_uint8     num_fields;
  kal_uint8     chari[C2K_DATA_BURST_MAX_FIELDS];
} em_c2k_xcal_ota_fdsch_info_ind_struct_with_header,
  em_c2k_xcal_ota_rdsch_info_ind_struct_with_header;


void c2k_xcal_report_ota_msg_event(c2k_xcal_ota_msg_type_enum event);
void c2k_xcal_report_sms_ota_fdsch(em_c2k_xcal_ota_fdsch_info_ind_struct_with_header* sms_fdsch);
void c2k_xcal_report_sms_ota_rdsch(em_c2k_xcal_ota_rdsch_info_ind_struct_with_header* sms_rdsch);
#endif
#endif



/*----------------------------------------------------------------------------
 Global Types
----------------------------------------------------------------------------*/
typedef irat_priority_class_enum IratPriorityClassT;
enum
{
    IRAT_1XRTT = 0,
    IRAT_HRPD,
    IRAT_NULL
};
typedef uint8 IratCdmaTypeT;


/* CDMA IRAT status */

typedef irat_ps_type_enum IratPSTypeT;

typedef enum
{
   REG_AUTOMATIC = 0, /* normal */
   REG_1X_SYSLOST, /* CDMA system lost */
   REG_DO_SYSLOST, /* CDMA system lost */
   REG_WRONG, /* cdm comes at wrong state. can not be executed */
   REG_PS_REL, /* PS released by netwrok */
   REG_AN_REJ,  /* network rejected AT's registration */
   REG_PS_CHG,  /* network supported PS type changed */
   REG_PRI_CHG,  /* priority of PS network changed */
   REG_CMD_CNF,  /* IRAT registration cmd confirmation/response */
   REG_1X_RECOVERY, /* 1X recovered from system lost */
   REG_DO_RECOVERY, /* DO recovered from system lost */
   REG_HO_1X_REQ, /* 1X signal condition is better than DO, data should go to 1X now */
   REG_HO_EHRPD_REQ, /* DO signal is good enough while data is on 1X, data should go to EHPRD now */
   REG_EHRPD_RETRY_REQ, /* DO signal is not too bad, retry on DO system after a while is recommended */
   REG_1X_CONNECTION, /* reg failed due to 1X is in connection */
   REG_DO_IDLE, /* DO CP is in IDLE state */
   REG_DO_CONN, /* DO CP is in CONNECTION state */
   REG_1X_ACQUIRED,
   REG_DO_ACQUIRED,
   REG_1X_CELL_CHG,
   REG_DO_CELL_CHG,
   REG_NO_ANTENNA_AT_CDMA,  /* CBP give away antenna upon request per antanna sharing mechanism. */
   REG_ECGREG, /* css send ecgreg to AP, notify the event to psdm also */
#ifdef MTK_CBP
   REG_CAUSE_NUMBER
#endif
}IratNotifyCauseT;

enum
{
   MEAS_SUCCESS= 0, /* meas completed */
   MEAS_1X_ONCALL, /* meas failed by 1X is on call */
   MEAS_ACQ_FAIL, /* meas failed by can not acquire system from the specified channels */
   MEAS_EMPTY_PNLIST, /*meas failed by the PN list of all channels specified are empty */
   MEAS_BAND_NOT_SUPPORT, /*meas failed by all the bands specified are not support */
   MEAS_FAIL, /* meas failed for other reasons */
   MEAS_NO_ENOUGH_TIME, /* meas failed due to "no enough time" */
   MEAS_ALREADY_DONE  /* all channel were measured in current TMeas period */
};
typedef uint8 IratMeasResultT;

#ifdef MTK_DEV_C2K_IRAT
typedef enum
{
    OP_HYBRID,
    OP_1X_ONLY,
    OP_DO_ONLY,
#ifdef MTK_CBP
    OP_INVALID
#endif
}OperationModeT;
#endif

enum
{
   RAT_CDMA1X = 0,
   RAT_GERAN,
   RAT_CDMAHRPD,
   RAT_UTRAN,
   RAT_EUTRAN
};
typedef uint8 RATIdT;

/* from CS0016-D Table 3.5.13.4-1 */
enum
{
   RAT_CDMA2KA1 = 0,
   RAT_CDMA2K1X = 3,
   RAT_CDMA2KHRPD
};
typedef uint8 MSPLSysTypeT;

/* if priclass value is from default MSPL entry,  the value sent to IRAT must with b7b6=01 */
#define DEFAULT_MSPL_PRICLASS 0x40
#define DFT_MSPL_PRICLASS_MASK 0xc0

typedef enum
{
   DATA_ACTIVE = 0, /* PS is active on data path */
   DATA_DORMANT,    /* ppp link still up, no TCH */
   DATA_IDLE,       /* ppp inactivity expired and released, but PS still on CDMA side. Can reestablish PPP link if needed */
   DATA_SUSPEND,    /* Suspend (just like dormant, but cannot reactivate. A transient state only. */
   DATA_DISABLED,   /* no PS on CDMA side, will wait for AP/LTE to send trigger to setup PS on CDMA */
   DATA_UNAVIABLE_1X_TRAFFIC, /* DATA service not available because of 1X is on voice call */
   DATA_RELEASED /* PPP link released */
}IratCdmaDataStateT;



enum
{
   RF_DOSLP = 0, /* DO enters sleep state and not use RF */
   RF_RTNRF, /* slave returns RF to master */
   RF_DOREL /* DO release Rf due to DISABLE,NO SERVICE */
};
typedef uint8 RfIndReasonT;

#define MAX_SIB8_REC_NUM 16
typedef PACKED_PREFIX struct
{
  IratCdmaTypeT Type;        /* CDMA2000 network  type */
  uint8 Band;            /* CDMA bandclass */
  uint16 Channel;        /* CDMA channel number */
  uint8 NumPn;           /* number of PN included */
  uint16 Pn[MAX_SIB8_REC_NUM]; /* CDMA pilot PN list*/
} PACKED_POSTFIX  IratReSelectionCellListT;

typedef PACKED_PREFIX struct
{
#ifdef IRAT_SUPPORT_CGI
  uint16 CellToReportCGI;  /* cell which need acquire CGI. 0xffff means not specified */
#endif
  uint8 Band;            /* CDMA bandclass */
  uint16 Channel;        /* CDMA channel number */
  uint8 CellReselPri;    /* cell reselection priority of the band. 0xff means not provided */
  uint8 NumPn;           /* number of PN included */
  uint16 Pn[MAX_SIB8_REC_NUM]; /* CDMA pilot PN list*/
} PACKED_POSTFIX  IratSIB8MeasCellListT;

typedef PACKED_PREFIX struct
{
  IratCdmaTypeT Type;        /* CDMA2000 network  type */
  uint8 SearchWinSize;   /* CDMA pilot search window size */
  uint8 ListSize;        /* entry number of cell info list */
  IratSIB8MeasCellListT  CellList[1];  /* cell info list */
} PACKED_POSTFIX  IratSIB8MeasInfoT;

typedef struct
{
  uint16 Pn;         /* pilot PN */
  int16 Phase;       /* CDMA pilot phase, in chips */
  int8 Strength;     /* CDMA pilot strength in -0.5 dB format */
} IratCdmaPilotMeasResultT;

typedef struct
{
#ifdef IRAT_SUPPORT_CGI
  uint8 CgiAvl;  /* 0 - CGI is invalid.  1 - CGI is valid */
  uint8 Cgi[16];  /* EVDO cell sectorID */
#endif
  uint8 Band;            /* CDMA bandclass */
  uint16 Channel;        /* CDMA channel number */
  uint8 NumPn;           /* number of PN included */
  IratCdmaPilotMeasResultT Pilot[MAX_SIB8_REC_NUM]; /* CDMA pilot measure result */
} IratSIB8MeasResultRecT;

typedef struct
{
  IratMeasResultT Result;
  uint8 ListSize;    /* entry number of list */
  IratSIB8MeasResultRecT  CellList[1];  /* CDMA pilot measure result list */
} IratSIB8MeasResultDataT;

typedef PACKED_PREFIX struct
{
  IratCdmaTypeT Type;      /* CDMA2000 network type */

#ifdef IRAT_SUPPORT_CGI
  uint16 CellToReportCGI;  /* cell which need acquire CGI. 0xffff means not specified */
#endif

  uint8 Band;          /* CDMA bandclass */
  uint16 Channel;      /* CDMA channel number */
  uint8 SearchWinSize; /* CDMA pilot search window size */
  uint8 NumPn;         /* number of PN included */
  uint16 Pn[1];        /* CDMA pilot PN list */
} PACKED_POSTFIX  IratMeasInfoT;

typedef struct
{
#ifdef IRAT_SUPPORT_CGI
  uint8 CgiAvl;  /* 0 - can't acquire CGI.  1 - CGI available */
  uint8 Cgi[16];  /* EVDO cell sectorID */
#endif
  IratMeasResultT Result;
  uint8 NumPn;       /* number of PN included */
  IratCdmaPilotMeasResultT Pilot[1]; /* CDMA pilot measure result */
} IratMeasResultDataT;

#define MAX_PLMN_NUM 9
typedef struct /* 3GPP2 C.S0087 v2.0 7.1 */
{
  uint16 Earfcn;       /* EUTRA channel number */
  uint8 EarfcnPri;     /* priority of EARFCN of the neighbir EUTRA system. 0xff means not provided */
  uint8 ThreshX;       /* Min required signal threshold used for selecting an EARFCN */
  uint8 RxLevMinEutra; /* Min ref power level to select EUTRA */
  uint8 RxLevMinEutraOffset; /* Offset to min ref power level to select EUTRA. 0xff means not provided */
  uint8 PeMax; /* Max TX power level UE may use when transmitting up-link of EUTRA */

  uint8 MeasBandWidth; /* measurement bandwidth Nrb value, Table 7.1-1 */
  uint8 NumPlmn; /* number of PLMN on this EARFCN channel */
  uint32 PlmnId[MAX_PLMN_NUM];  /*PLMN ID of the neighbor EUTRA system. 0xffffffff means not provided */
} IratEUTRANNeighborT;

#define MAX_EUTRA_NUM 7
typedef struct /* 3GPP2 C.S0087 7.1 */
{
  uint8 MaxReselectionTimer;   /* upper time limit of random reselection timer. 0xff means not provided */
  uint8 MinMeasurementBackoff; /* lower limit of measurement backoff timer. 0xff means not provided*/
  uint8 MaxMeasurementBackoff; /* upper limit of measurement backoff timer. 0xff means not provided*/

  uint8 ListSize;               /* entry number of list */
  IratEUTRANNeighborT Eutra[MAX_EUTRA_NUM]; /* measure result list */
} IratEUTRANMeasureInfoT;

typedef struct /* refer to 3GPP2 C.S0087 7.1 */
{
  IratCdmaTypeT Type;        /* CDMA2000 network type */
  uint8 ServingPriority;     /* priority of current serving CDMA channel. 0xff means not provided */
  int8 ThreshServing;        /* CDMA serving pilot strength threshold trigger point for EUTRAN neighbor
                                selection. 0xff means not provided  */
  uint8 Band;                /* CDMA bandclass */
  uint16 Channel;            /* CDMA channel number */
  IratCdmaPilotMeasResultT Pilot; /* current CDMA pilot measure result */
} IratCDMASysInfoT;

typedef struct
{
  IratCdmaTypeT Type;        /* CDMA2000 network type */
  uint8 Band;                /* CDMA bandclass */
  uint16 Channel;            /* CDMA channel number */
  IratCdmaPilotMeasResultT Pilot; /* CDMA pilot measure result */
} IratCDMAMeasResultT;

typedef enum
{
  /*Network Generates Error codes*/
  IratGeneralError,
  IratUnauthorizedAPN,
  IratPDNLimitExceeded,
  IratNoPGWAvailable,
  IratPGWUnreachable,
  IratPGWReject,
  IratInsufficientParameters,
  IratResourceUnavailable,
  IratAdminProhibited,
  IratPDNIDAlreadyInUse,
  IratSubScriptionLimitation,
  IratPDNConnAlreadyExistForPDN,
  IratEmergencyNotSupported,
  IratReconnectNotAllowed,

  /*VIA Internal Generates Error codes*/
  IratEAPAKAfailure = 0x80,
  IratRetryTmrThrottling,
  IratNetworkNoResponse,
  IratPDNAttachAbort,
  IratPDNLimitExceededInUESide,
  IratPDNIDAlreadyInUseInUESide,
  IratInvalidPDNAttachReq,
  IratPdnRecFailure,
  IratMainConnSetupFailure,
  IratBearerResourceUnAvailable,
  IratOpAbortByUser,
  Irat1xDataConnected,
  IratEAPAKAReject,
  IratLCPNegotiateFailure,
  IratTchSetupFailure,
  IratNwNoRspInLCP,
  IratNwNoRspInAuth,
#ifdef MTK_DEV_C2K_SRLTE
  IratRsvasSuspended,
  IratAbortByGMSS,
#endif
  IratNoError = 0xff
} IratPdnCfgErrorCodeT;


/*----------------------------------------------------------------------------
 CDMA internal interface defined by VIA
----------------------------------------------------------------------------*/
typedef enum
{
    MMC_CDMA_REGISTRATION_HYBRID_REQ = 0xD100,  /*53504*/
    IRAT_GMSS_CSS_MCC_SEARCH_REQ,               /*53505*/
    IRAT_GMSS_CSS_1X_ACQUISITION_REQ,           /*53506*/
    IRAT_GMSS_CSS_DO_ACQUISITION_REQ,           /*53507*/
    IRAT_GMSS_CSS_SYS_ACQUISITION_REQ,          /*53508*/
    IRAT_GMSS_CSS_CS_REG_REQ,                   /*53509*/
    IRAT_GMSS_CSS_PS_REG_REQ,                   /*53510*/
    IRAT_CPSDM_L2C_RAT_CHANGE_REQ,              /*53511*/
    IRAT_GMSS_CSS_C2L_RAT_CHANGE_REQ,           /*53512*/
    IRAT_CPSDM_CSS_RAT_CHANGE_RSP,              /*53513*/
    IRAT_GMSS_CSS_DEACTIVATE_REQ,               /*53514*/
    MMC_CDMA_TRIGGER_PS_REG_FREQ_REQ,           /*53515*/
    MMC_CDMA_TRIGGER_PS_REG_CELLIDS_REQ,        /*53516*/
    IRAT_CPSDM_CSS_DATA_CONNECTION_REQ,         /*53517*/
    MMC_CDMA_MEASURE_IRAT_REQ,                  /*53518*/
    MMC_CDMA_MEASURE_SIB8_REQ,                  /*53519*/
    IRAT_CPSDM_CSS_EHRPD_DEFAULT_BEARER_REQ,    /*53520*/
    MMC_CDMA_EHRPD_DEDICATED_BEARER_REQ,        /*53521*/
    IRAT_CPSDM_CSS_EHRPD_DETACH_REQ,            /*53522*/
    IRAT_CPSDM_CSS_EHRPD_BEARER_DISCONNECT_REQ, /*53523*/
    MMC_CDMA_STOP_CDMA_MEAS_REQ,                /*53524*/
    MMC_CDMA_CDMA_SERVING_MEAS_REQ,             /*53525*/
    MMC_CDMA_CDMA_TX_POWER_REQ,                 /*53526*/
    MMC_CDMA_EHRPD_SET_IPV6_ADDR,               /*53527*/
    MMC_CDMA_EHRPD_CLEAR_IPV6_ADDR,             /*53528*/
    MMC_CDMA_DISABLE_1X_ONLY_REQ,               /*53529*/
    MMC_CDMA_DO_REQUEST_RF_RSP,                 /*53530*/
    MMC_CDMA_LTE_REQUEST_RF_REQ,                /*53531*/
    MMC_CDMA_LTE_RELEASE_RF_IND,                /*53532*/
    MMC_CDMA_DO_MEAS_WAKEUP_REQ,                /*53533*/
    MMC_CDMA_SIB8_DO_WINDOW_MEAS_REQ,           /*53534*/
    MMC_CDMA_POWER_UP_REQ,                      /*53535*/
    MMC_CDMA_POWER_DOWN_REQ,                    /*53536*/
    IRAT_GMSS_CSS_PS_DEREG_REQ,                 /*53537*/
    IRAT_GMSS_CSS_ACTIVATE_SIM_INFO_REQ,        /*53538*/
    IRAT_RSVAC_CSS_FREQ_SCAN_PREEMPT_IND,       /*53539*/
    IRAT_RSVAC_CSS_FREQ_SCAN_ACCEPT_IND,        /*53540*/
    IRAT_CSS_CPSDM_RESEL_REQ,                   /*53541*/
    IRAT_CSS_CPSDM_REDIRECT_REQ,                /*53542*/
    IRAT_CSS_CPSDM_RAT_CHANGE_CMP_REQ,          /*53543*/
    IRAT_CSS_CPSDM_RESEL_TO_LTE_RSP,            /*53544*/
    /* Add the IRATM/PSW->CSS msg here to use the cmd cache for irat.*/
    IRAT_CSS_IRATM_C2L_RESEL_IND,               /*53545*/
    IRAT_CSS_PSW_RSVAS_SUSPEND_REQ,             /*53546*/
    IRAT_CSS_PSW_RSVAS_DO_VRTL_SUSP_REQ,        /*53547*/
    IRAT_CSS_PSW_RSVAS_RESUME_REQ,              /*53548*/
    IRAT_CSS_PSW_RSVAS_DO_RESUME_REQ,           /*53549*/
    IRAT_CSS_PSW_RSVAS_VRTL_RESUME_REQ,         /*53550*/
    IRAT_CSS_PSW_RSVAS_CS_SERV_START_IND,       /*53551*/
    IRAT_CSS_PSW_RSVAS_CS_SERV_FINISH_IND,      /*53552*/
    IRAT_GMSS_CSS_SIM_PLMN_INFO_IND,            /*53553*/
    IRAT_MRS_CAS_OCCUPY_LLA_REQ,                /*53554*/
    IRAT_MRS_CAS_RELEASE_LLA_REQ,               /*53555*/
    IRAT_MRS_CAS_GET_LLA_OCCUPY_RAT_REQ,        /*53556*/
    IRAT_MRS_CAS_LOWER_LAYER_AVAILABILITY_UPDATE_IND,/*53557*/
    IRAT_RSVAC_CSS_FREQ_SCAN_START_REQ,         /*53558*/
    IRAT_RSVAC_CSS_FREQ_SCAN_MODIFY_REQ,        /*53559*/
    IRAT_RSVAC_CSS_FREQ_SCAN_STOP_REQ,          /*53560*/
    IRAT_CSS_PSW_RSVAS_EVENT_REPORT_IND,        /*53561*/
    IRAT_EAS_CSS_SYS_SNIFFER_REQ,               /*53562*/
    IRAT_GMSS_CSS_MMSS_LIST_SYNC_REQ,           /*53563*/
    IRAT_GMSS_CSS_3GPP_NORMAL_SERVICE_REQ,      /*53564*/
    IRAT_GMSS_CSS_SET_RAT_MODE_REQ,             /*53565*/
    IRAT_GMSS_CSS_EMC_SESSION_START_REQ,        /*53566*/
    IRAT_GMSS_CSS_EMC_SESSION_STOP_REQ,         /*53567*/
    IRAT_GMSS_CSS_EMC_LOCAL_DETACH_REQ,         /*53568*/
    IRAT_CSS_VAL_IRAT_POWER_CTRL_REQ,           /*53569*/
    IRAT_GMSS_CSS_MSPL_SEARCH_EXHAUSTED,        /*53570*/
    IRAT_GMSS_CSS_CS_RESUME_REQ,                /*53571*/
    IRAT_VAL_CSS_LTE_DISABLE_REQ,               /*53572*/
    IRAT_PSW_CSS_EMC_SESSION_START_REQ,         /*53573*/
    IRAT_PSW_CSS_EMC_SESSION_STOP_REQ,          /*53574*/
    IRAT_CSS_PSW_RSVAS_CS_OCCUPY_IND,           /*53575*/
    IRAT_NULL_CMD                               /*53576*/
} IRATMsgT;

typedef enum
{
    NOT_CACHABLE,
    WRONG_MODE,
    WRONG_RESUME,
    WRONG_SUSPEND,
    WRONG_PWR_DOWN
} IRATCmdRejCodeT;

#define IRAT_CSS_IRAT_MSG  100
#define IRAT_PDN_ADD_LEN   5
#define IRAT_MAX_SUPPORTED_IPFLOW  2
#define IRAT_MAX_APN_LEN  101


typedef struct{
    uint16  Cmd;
    uint16  ParamBlklength;
    uint8    ParamBlk[1];
} IRAT2CDMAMsgT;

typedef  PACKED_PREFIX struct
{
  bool    bIpV4;
  uint8   PrecedenceIndex; /*evaluation precedence index*/
  uint16  DstPortNum;      /*For Forward flow, it's DstPortNum*/
  uint8   ProNumOrNHdr;
  uint32  Spi;             /*ipsec security parameter index*/
  uint8   TrafficClass;    /*for ipv4, it's tos*/
  uint8   TrafficMask;
  uint32  FlowLabel;       /*valid for IPv6 only*/
} PACKED_POSTFIX  Irat_QoMPktFilterContentT;


#define IRAT_LENGTH_LOCAL_IP_ADDR 5
#define IRAT_LENGTH_PCSCF6_ADDR 4
#define IRAT_LENGTH_IP6_INTERFACE 8
#define IRAT_MAX_ADDR_NUM 2

#if 0
typedef struct
{
  uint8          PdnId;
  uint8          BearerId;      /*use 0xf0|PdnId to indicate default bearer*/
  uint8          Status;        /*0-Succ, 1-Fail, 2-duplicate, 3-PDN resync*/
  uint8          AddrType;      /*1-IPv4,2-IPv6, 3-IPv6andIPv4(X.S0057)*/
  uint32         LocalIPAddr[IRAT_LENGTH_LOCAL_IP_ADDR];/*the last byte is Ipv4 addr*/
  uint8          Pcscf6Num;
  uint32         PCSCF6Addr[IRAT_MAX_ADDR_NUM][IRAT_LENGTH_PCSCF6_ADDR];
  uint8          DNS6Num;
  uint32         DNS6Addr[IRAT_MAX_ADDR_NUM][IRAT_LENGTH_PCSCF6_ADDR];
  uint8          Pcscf4Num;
  uint32         PCSCF4Addr[IRAT_MAX_ADDR_NUM];
  uint8          DNS4Num;
  uint32         DNS4Addr[IRAT_MAX_ADDR_NUM];

  uint32         RouteAddr;
  uint8          IP6Interfaceid[IRAT_LENGTH_IP6_INTERFACE]; /*IPv6 InterfaceID*/
  uint8          SelBearerCtrlMode;
  IratPdnCfgErrorCodeT ErrCode;
  uint8          ResContainerLen; /*length of Operation reserved PCO container FF00H, 0 means not available*/
  uint8          ResContainer[255]; /*content of operation reserved PCO container FF00H*/
#ifdef SPRINT_EXTENSIONS
  UINT32      DNSPriAddr;
  UINT32      DNSSecAddr;
#endif
  uint16         IPv4MTU;
}Irat_NetwkConnRspMsgT;
#else

typedef AppImsNetwkConnRspMsgT Irat_NetwkConnRspMsgT;

#endif

typedef struct
{
  uint8           Status; /*0-Succ, 1-Fail*/
  uint8           PdnId;
  uint8           BearerId;
  uint8           ErrCode;
} Irat_EpsBearerSetupRspT;

typedef struct
{
   uint8          PdnId;
   uint8          BearerId;
   uint8          Num;  /*number of Tft*/
   Irat_QoMPktFilterContentT   Tft[IRAT_MAX_SUPPORTED_IPFLOW];
} Irat_BearerSetupIndT;

typedef struct
{
  uint8       Status; /*0-Succ, 1-Fail*/
  uint8       PdnId;
  uint8       BearerId;
} Irat_EpsBearerDisconnRspT;

typedef struct
{
  uint8  PdnId;
  uint8  BearerId;
  uint8  Reason; /* 0-Network intiated; 1-AT initiated */
} Irat_EpsBearerDisconnIndT;

typedef struct
{
  uint8          status;
  uint32         LocalIPAddr;
  uint32         RemoteIPAddr;
  uint32         PriDNSAddr;
  uint32         SecDNSAddr;
} Irat_PsConnectionCnfT;

typedef struct
{
  uint8 subnetID[16];    /* DO system ID */
  IratPriorityClassT priClass; /* priority class of DO system */
  uint8 Band;            /* CDMA bandclass */
  uint16 Channel;        /* CDMA channel number */
} Irat_DOSysInfoT;

typedef struct
{
  uint16 sid;    /* 1x system ID */
  uint16 nid;    /* network id of 1x system */
  IratPriorityClassT priClass; /* priority class of 1X system */
  uint8 Band;            /* CDMA bandclass */
  uint16 Channel;        /* CDMA channel number */
} Irat_1XSysInfoT;


/***************************************************************************
                    VAL IRAT event definitions
****************************************************************************/
typedef enum
{
    VAL_IRAT_EVT_REG_1X_ONLY_CNF_MSG,
    VAL_IRAT_EVT_REG_HYBRID_CNF_MSG,
    VAL_IRAT_EVT_NETWK_STATE_CHANGE_NOTIFY_MSG,
    VAL_IRAT_EVT_TRIGGER_PS_REG_RSP_MSG,
    VAL_IRAT_EVT_RESEL_CNF_MSG,
    VAL_IRAT_EVT_REDIRECT_CNF_MSG,                            /* 5 */
    VAL_IRAT_EVT_RAT_CHANGE_CMP_CNF_MSG,
    VAL_IRAT_EVT_RESEL_TO_LTE_IND_MSG,
    VAL_IRAT_EVT_TRIGGER_PS_REG_FREQ_RSP_MSG,
    VAL_IRAT_EVT_TRIGGER_PS_REG_CELLIDS_RSP_MSG,
    VAL_IRAT_EVT_MCC_SEARCH_RSP_MSG,                           /* 10 */
    VAL_IRAT_EVT_PS_STATUS_CHANGE_NOTIFY_MSG,
    VAL_IRAT_EVT_PS_RAT_CHANGE_IND_MSG,
    VAL_IRAT_EVT_LTE_MEASURE_INFO_IND_MSG,
    VAL_IRAT_EVT_CDMA_NO_SERVICE_RSP_MSG,
    VAL_IRAT_EVT_CDMA_DATA_STATE_IND_MSG,                      /* 15 */
    VAL_IRAT_EVT_CDMA_SERV_MEASURE_RSP_MSG,
    VAL_IRAT_EVT_CDMA_TX_POWER_RSP_MSG,
    VAL_IRAT_EVT_CDMA_SCAN_STATUS_IND_MSG,
    VAL_IRAT_EVT_CMD_REJECT_IND_MSG,
    VAL_IRAT_EVT_MEASURE_RESULT_IND_MSG,                       /* 20 */
    VAL_IRAT_EVT_MEASURE_SIB8_RESULT_IND_MSG,
    VAL_IRAT_EVT_EHRPD_DETACH_CNF_MSG,
    VAL_IRAT_EVT_EHRPD_DEDICATED_BEARER_CNF_MSG,
    VAL_IRAT_EVT_EHRPD_DEDICATED_BEARER_IND_MSG,
    VAL_IRAT_EVT_EHRPD_BEARER_DISC_CNF_MSG,                    /* 25 */
    VAL_IRAT_EVT_EHRPD_BEARER_DISC_IND_MSG,
    VAL_IRAT_EVT_EHRPD_DEFAULT_BEAER_CNF_MSG,
    VAL_IRAT_EVT_EHRPD_DEFAULT_BEARER_DISC_IND_MSG,
    VAL_IRAT_EVT_PS_CONNECTION_CNF_MSG,
    VAL_IRAT_EVT_DO_ACQUISITION_RSP_MSG,                      /* 30 */
    VAL_IRAT_EVT_DO_RELEASE_RF_IND_MSG,
    VAL_IRAT_EVT_DO_REQUEST_RF_RSP_MSG,
    VAL_IRAT_EVT_DO_NORF_EARLY_WAKEUP_RSP_MSG,
    VAL_IRAT_EVT_SIB8_DO_WINDOW_MEAS_RSP_MSG,

    VAL_IRAT_EVT_C2K_LTE_RAT_CHANGE_CNF_MSG,                  /* 35 */
    VAL_IRAT_EVT_LTE_C2K_RAT_CHANGE_REQ_MSG,
    VAL_IRAT_EVT_PPP_MTU_NTF_MSG,

    VAL_IRAT_EVT_DEACTIVE_NOTIFY_MSG,
    VAL_IRAT_EVT_3GPP_NORMAL_SERVICE_NOTIFY_MSG,
    VAL_IRAT_EVT_RAT_MODE_NOTIFY_MSG,                         /* 40 */
    VAL_IRAT_EVT_LTE_C2K_IA_PDN_QUERY_REQ_MSG,
    VAL_DANDCN_SEND_DCN_REQ,
    VAL_DANDCN_SEND_START_RSP,
    VAL_DANDCN_IMS_REG_STATUS_IND,
    VAL_IRAT_EVT_ABORT_C2L_RESEL_IND_MSG,                     /* 45 */
#ifdef MTK_DEV_C2K_SRLTE
    VAL_IRAT_EVT_ABORT_IND_MSG,
#endif
    VAL_IRAT_EVT_LTE_C2K_PDN_TRANSFER_REQ_MSG,
    VAL_IRAT_EVT_C2K_LTE_PDN_TRANSFER_CNF_MSG,
    VAL_IRAT_EVT_LOCAL_DEATCH_MSG,

    VAL_IRAT_EVT_MAX
} ValIratEventIdT;

typedef struct{
    uint16  Cmd;
    uint16  ParamBlklength;
    uint8    ParamBlk[1];
} CDMA2IRATMsgT;


/* event data structure for VAL_IRAT_EVT_REG_HYBRID_CNF_MSG */

/* event data structure for VAL_IRAT_EVT_REG_1X_ONLY_CNF_MSG */

/* event data structure for VAL_IRAT_EVT_NETWK_STATE_CHANGE_NOTIFY_MSG */
typedef struct
{
   uint8 SearchResult;
   uint16 Mcc;
   MSPLSysTypeT SysType;
   uint16 Sid;
   uint16 Nid;
   uint8 *SubnetId;
}IratEventMccSearchRspT;

/* event data structure for VAL_IRAT_EVT_PS_STATUS_CHANGE_NOTIFY_MSG */
typedef struct
{
   bool        registered;
   IratPSTypeT PsType;
}IratPsStatusChangeNotifyT;


/* event data structure for VAL_IRAT_EVT_PS_RAT_CHANGE_IND_MSG */
typedef struct
{
    irat_type_enum type;
    bool           success;
}IratPsRatChangeIndT;

typedef struct
{
    irat_type_enum type;
    kal_bool       is_irat_success;
    IratPdnCfgErrorCodeT code;
} IratPsRatChangeRspT;

/* event data structure for VAL_IRAT_EVT_NETWK_STATE_CHANGE_NOTIFY_MSG */
typedef struct
{
   IratPSTypeT PsType;
   IratNotifyCauseT Cause;
}IratEventNetwkStateChangeNotifyT;

/* event data structure for VAL_IRAT_EVT_TRIGGER_PS_REG_RSP_MSG/VAL_IRAT_EVT_TRIGGER_PS_REG_FREQ_RSP_MSG/VAL_IRAT_EVT_TRIGGER_PS_REG_CELLIDS_RSP_MSG */
typedef struct
{
   IratPSTypeT     PsType;
   c2k_irat_result_enum   FailCause;
}IratEventTriggerPsRegRspT;

/* event data structure for VAL_IRAT_EVT_TRIGGER_PS_REG_FREQ_RSP_MSG/VAL_IRAT_EVT_TRIGGER_PS_REG_CELLIDS_RSP_MSG */

/* event data structure for VAL_IRAT_EVT_CDMA_DATA_STATE_IND_MSG */
typedef struct
{
   IratCdmaDataStateT state;
   IratPSTypeT PsType;
   IratPSTypeT CurPsType;
}IratEventDataStateIndT;

/* event data structure for VAL_IRAT_EVT_LTE_MEASURE_INFO_IND_MSG */
typedef struct
{
   IratEUTRANMeasureInfoT LteInfo;
   IratCDMASysInfoT       CDMAInfo;
}IratEventLteMeasInfoT;

/* event data structure for VAL_IRAT_EVT_EHRPD_DEFAULT_BEARER_DISC_IND_MSG */
typedef struct
{
   uint8 PdnId;
   uint8 BearerId;
}IratEventEhrpdDfltBearerDiscIndT;

/* event data structure for VAL_IRAT_EVT_CDMA_SERV_MEASURE_RSP_MSG */
typedef struct
{
   IratCDMAMeasResultT CdmaMeasResult;
   IratPriorityClassT  SysPri;
}IratEventCdmaServMeasRspT;

/* event data structure for VAL_IRAT_EVT_CDMA_SERV_MEASURE_RSP_MSG */
typedef struct
{
   IratCdmaTypeT Type;
   uint8 Band;
   uint16 Channel;
   IratPriorityClassT  SysPri;
}IratEventCdmaCdmaMeasureResultT;

/* event data structure for VAL_IRAT_EVT_EHRPD_DETACH_CNF_MSG */
typedef struct
{
   uint8 status;
}IratEventEhrpdDetachCnfT;

/* event data structure for VAL_IRAT_EVT_EHRPD_DEFAULT_BEARER_DISC_IND_MSG */
typedef struct
{
   uint8 PdnId;
   uint8 BearerId;
}IratEventEhrpdDefaultBearerDiscIndT;

/* event data structure for VAL_IRAT_EVT_CDMA_SCAN_STATUS_IND_MSG */
typedef struct
{
   uint8 Scan1X;
   uint8 ScanDO;
}IratEventCdmaScanStatusIndT;

/* event data structure for VAL_IRAT_EVT_CMD_REJECT_IND_MSG */
typedef struct
{
  IRATMsgT IratCdmId;
  IRATCmdRejCodeT Reason;
}IratEventCmdRejectIndT;

/* event data structure for VAL_IRAT_EVT_DO_ACQUISITION_RSP_MSG */

/* event data structure for VAL_IRAT_EVT_DO_RELEASE_RF_IND_MSG */
typedef struct
{
   uint32 RFReturnTime;
   RfIndReasonT Reason;
}IratEventDoReleaseRfindT;
#ifdef MTK_DEV_C2K_SRLTE
/* Data structure for IRAT_CPSDM_CSS_RESEL_REQ */
typedef struct
{
    eas_cas_activate_ccell_req_struct L2cReselInfo;
}IratCssCpsdmReselReqT;

/* event data structure for VAL_IRAT_EVT_RESEL_CNF_MSG */
typedef struct
{
    IratPSTypeT     PsType;
    eas_cas_activate_ccell_cnf_struct L2cReselResult;
}IratEventReselCnfT;

/* Data structure for IRAT_CPSDM_CSS_REDIRECT_REQ */
typedef struct
{
    eas_cas_activate_ccell_req_struct L2cRedirectInfo;
}IratCssCpsdmRedirectReqT;

/* event data structure for VAL_IRAT_EVT_REDIRECT_CNF_MSG */
typedef struct
{
    IratPSTypeT     PsType;
    eas_cas_activate_ccell_cnf_struct L2cRedirectResult;
}IratEventRedirectCnfT;

/* Data structure for IRAT_CPSDM_CSS_RAT_CHANGE_CMP_REQ*/
typedef struct
{
    Bool is_success;
    irat_type_enum iraType;
}IratCssCpsdmRatChangeCmpReqT;

/* event data structure for VAL_IRAT_EVT_RESEL_TO_LTE_IND_MSG */
typedef struct
{
   cas_eas_activate_ecell_req_struct ReselToLteInfo;
}IratEventReselToLteIndT;

/* Data structure for IRAT_CSS_CPSDM_RESEL_TO_LTE_RSP*/
typedef struct
{
    cas_eas_activate_ecell_cnf_struct C2lReselResult;
    kal_bool is_irat_success;
}IratCpsdmCssReselToLteRspT;

typedef struct
{
    bool PwrUp;  /* TRUE-powerup; FALSE-powerdown */
}IratCssValPowerCtrlReqT;

typedef struct
{
    rat_enum   mode;
    OperationModeT op_mode;
    rat_enum   reported_rat;
}IratEventRatModeNotifyT;

#endif /* MTK_DEV_C2K_SRLTE */

void ValSendCssIRATMsg(ValIratEventIdT cmd, uint8 *paramBlock,  uint16 paramBlkLength);
void ValSendCssAtcIRATMsg(ValIratEventIdT cmd, uint8 *paramBlock,  uint16 paramBlkLength);


/*----------------------------------------------------------------------------
 Global API Function Prototypes
----------------------------------------------------------------------------*/

/****************************************************************************

              API functions from LTE  to CDMA(eHRPD,HROD,1x)

*****************************************************************************/
/*******************************************************************************

  FUNCTION NAME: ValApi_CDMA_PowerUp_Req

  DESCRIPTION:
    MMC uses this API to request CDMA modem to power up(both 1X and DO stack).

  PARAMETERS: none

  RETURNED VALUES:  None

*******************************************************************************/
void ValApi_CDMA_PowerUp_Req(void);


/*******************************************************************************

  FUNCTION NAME: ValApi_CDMA_PowerDown_Req

  DESCRIPTION:
    MMC uses this API to request CDMA modem to power down(both 1X and DO stack).

  PARAMETERS: none

  RETURNED VALUES:  None

*******************************************************************************/
void ValApi_CDMA_PowerDown_Req(void);

/*****************************************************************************

  FUNCTION NAME: ValApi_Registration_Hybrid_Req

  DESCRIPTION:
    LTE sends this req to CDMA for CDMA only mode operation. It wil trigger
    hybrid mode operation of both CS and PS on CDMA network.
    1) Register for 1x if it was not already registered
    2) Establish eHRPD/HRPD Sessions
    3) Notify upper layer (val client) about the
        new Network mode (1x, 1x + HRPD, 1x + eHRPD)

  PARAMETERS: None

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Registration_Hybrid_Req(void);



/*****************************************************************************

  FUNCTION NAME: CssApi_Irat_Sys_Acquisition_Req

  DESCRIPTION:
    This is used to request C2K start scan channels based on PRL. After acquired a
    system, NO registration will be performed.

  PARAMETERS:  RATId
                1XRTT: CDMA IRAT will return decoded MCC and sid/nid
                HRPD: CDMA IRAT will return decoded MCC and subnetId

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_Irat_Sys_Acquisition_Req(gmss_css_sys_acquire_req_struct *MsgBufferP);
/*===========================================================================

FUNCTION ValApi_Irat_Init

DESCRIPTION
  Initialize the VAL IRAT module internal.

DEPENDENCIES
  None

RETURN VALUE
  None

===========================================================================*/
void ValApi_Irat_Init( void );
/***************************************************************************

  FUNCTION:      ValApi_Irat_ProcessMsg()

  DESCRIPTION:   Callback to support CP messages from PSW.

  PARAMETERS:    msg_id - received message identifier
                 msg_buffer - pointer to message contents
                 msg_size - size of data to retrieve from *msg_buffer

  RETURN VALUE:  TRUE if msg_id was found

***************************************************************************/
void ValApi_Irat_ProcessMsg( uint32 msg_id,
                             void*  msg_buffer,
                             uint32 msg_size );

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Mcc_Search_Req

  DESCRIPTION:
    Start MCC Search. For 1X MCC, MMC must ensure 1X is in acquired state.
    For DO MCC, no this requirement.

  PARAMETERS: RATId - RATIdT
                      1XRTT: CDMA IRAT will return decoded MCC and sid/nid
                      HRPD: CDMA IRAT will return decoded MCC and subnetId

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Mcc_Search_Req(RATIdT RATId);

/*****************************************************************************

  FUNCTION NAME: CssApi_Irat_Ps_Dereg_Req

  DESCRIPTION:
    GMSS ask to detach C2K PS


  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_Irat_Ps_Dereg_Req(void);

/*****************************************************************************

  FUNCTION NAME: CssApi_Irat_Active_Sim_Info_Req

  DESCRIPTION:
    gmss tell C2K ps service is on sim1 or sim2

   PARAMETERS: act_sim_enum - active_sim,

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_Irat_Active_Sim_Info_Req(act_sim_enum active_sim);

#ifndef MTK_DEV_C2K_SRLTE
/*****************************************************************************

  FUNCTION NAME: CssApi_Eas_Css_Sys_Sniffer_Req

  DESCRIPTION:
    EAS ask to sniffer the suitable EVDO cell


  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_Eas_Css_Sys_Sniffer_Req(void);
#endif
/*****************************************************************************

  FUNCTION NAME: CssApi_Irat_Mcc_Search_Req

  DESCRIPTION:
    Start MCC Search. For 1X MCC, MMC must ensure 1X is in acquired state.
    For DO MCC, no this requirement.

   PARAMETERS: RATId - RATIdT,
                       1XRTT: CDMA IRAT will return decoded MCC and sid/nid
                       HRPD: CDMA IRAT will return decoded MCC and subnetId

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_Irat_MCC_Search_Req(gmss_css_mcc_search_req_struct *MsgBufferP);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Trigger_PS_Reg_Req

  DESCRIPTION:
    Trigger PS Connection

  PARAMETERS: PriorityClass - priority class value get from MSPL defined by VZW.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Trigger_PS_Reg_Req(IratPriorityClassT PriorityClass);

/*****************************************************************************

  FUNCTION NAME: CssApi_Irat_PS_Reg_Req

  DESCRIPTION:
    Trigger PS Connection

  PARAMETERS:
            sys_type      - the sys_type to register
            PriorityClass - priority class value get from MSPL .

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_Irat_PS_Reg_Req(gmss_css_ps_reg_req_struct *gmss_css_ps_reg_req_p);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Trigger_PS_Reg_Freq_Req

  DESCRIPTION:
    Trigger PS connection on frequency F (received in RRC Connection
    release message on LTE)

  PARAMETERS:  Band -CDMA freq band
               Channel - CDMA freq channel number

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Trigger_PS_Reg_Freq_Req(uint16 Band, uint16 Channel);


/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Trigger_PS_Reg_CellIds_Req

  DESCRIPTION:
    Trigger PS connection on a set of cell ids

  PARAMETERS: CellList - the info of the reselected target

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Trigger_PS_Reg_CellIds_Req(IratReSelectionCellListT *CellList);

#ifdef MTK_DEV_C2K_IRAT
/*****************************************************************************

  FUNCTION NAME: CssApi_Irat_Deactivate_Req

  DESCRIPTION:
    Put CDMA modem into suspend mode/LPM/Deep Sleep

  PARAMETERS: None

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_Irat_Deactivate_Req(gmss_css_deactivate_req_struct *pMsgData);
#endif

#ifdef MTK_DEV_C2K_SRLTE
/*****************************************************************************

 FUNCTION NAME: CssApi_rsvac_css_frequency_scan_preempt_ind

 DESCRIPTION:
   Inform CSS that the frequency scan request is preemptted by other RAT.

 PARAMETERS: sessionID which is preempted by RSVAC.

 RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_rsvac_css_frequency_scan_preempt_ind(uint8 sessionId);

/*****************************************************************************

 FUNCTION NAME: CssApi_rsvac_css_frequency_scan_accept_ind

 DESCRIPTION:
   Inform CSS that the frequency scan request is accept by RSVAC.

 PARAMETERS: sessionID which is acceptted by RSVAC.

 RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_rsvac_css_frequency_scan_accept_ind(uint8 sessionId);
void CssApi_mrs_cas_get_lla_occupy_rat_cnf(mrs_cas_get_lla_occupy_rat_cnf_struct *mrs_cas_get_lla_occupy_rat_cnf_p);
void CssApi_Mrs_Cas_Lower_Layer_Available_Update_Ind(void);

void ValApi_Irat_CSS_PSDM_L2C_Resel_Req(eas_cas_activate_ccell_req_struct *pResel);

void ValApi_Irat_CSS_PSDM_L2C_Redir_Req(eas_cas_activate_ccell_req_struct *pRedir);

void ValApi_Irat_CSS_PSDM_C2L_Resel_Rsp(IratCpsdmCssReselToLteRspT *pRsp);

void ValApi_Irat_CSS_PSDM_L2C_Cmp_Req(uint8 succ, uint8 iraType);


/*****************************************************************************

  FUNCTION NAME: CssApi_css_emc_session_start_Req

  DESCRIPTION:
    GMSS inform MD3 that emergency mode started.

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_css_emc_session_start_Req(void);

/*****************************************************************************
 *
 * Function: CssApi_Css_Cs_Resume_Req
 *
 * Description:
 *   GMSS resume cs registration
 *
 * Parameters:
 *   None
 *
 * Returns:
 *   None
 *
 *****************************************************************************/
void CssApi_Css_Cs_Resume_Req(gmss_css_cs_resume_req_struct *pMsgData);

/*****************************************************************************

  FUNCTION NAME: CssApi_css_emc_session_stop_Req

  DESCRIPTION:
    GMSS inform MD3 that emergency mode stoped.

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_css_emc_session_stop_Req(void);


/*****************************************************************************

  FUNCTION NAME: CssApi_css_emc_local_detach_Req

  DESCRIPTION:
    GMSS inform MD3 to do local detach to release PPP conlection.

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_css_emc_local_detach_Req(void);

/*****************************************************************************

  FUNCTION NAME: CssApi_gmss_css_mspl_search_exhausted

  DESCRIPTION:
    GMSS inform MD3 that all mspl records have been used once.

  RETURNED VALUES:  None.

*****************************************************************************/
void CssApi_gmss_css_mspl_search_exhausted(void);
#endif /*MTK_DEV_C2K_SRLTE*/
/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Data_Connection_Cmd

  DESCRIPTION:
    Suspend/ Resume/ Disable the data traffic.
    If Suspend => go into Suspend Mode
    If Resume => go from Suspend Mode into Dormant Mode
    If Disable => Disconnect PS connection on CDMA (only 1xRTT CS is required).
    If Release => tear down PPP link

  PARAMETERS: Cmd - suspend,resume, disable or release

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Data_Connection_Cmd(ConnCmdT Cmd);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Measure_Cmd

  DESCRIPTION:
    Start measuring network information based on IRAT measurement
    information received in RRC Connection Reconfiguration message
    received on LTE

  PARAMETERS: IratInfo - CDMA cell measure info from MeasObjectCDMA2000

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Measure_Cmd(IratMeasInfoT *IratInfo);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Measure_SIB8_Cmd

  DESCRIPTION:
    Start measuring network information based on SIB8 message received on LTE

  PARAMETERS: Sib8 - cell info from SIB8 system info block. For efficience,
                     the entry shall be arranged in CellReselectionPriority
                     order.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Measure_SIB8_Cmd(IratSIB8MeasInfoT *Sib8);


/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Default_Bearer_Req

  DESCRIPTION:
    This is used to request the default Bearer setup

  PARAMETERS: PdnId - Indetifier of a PDN
              PdnType - 1-IPv4,2-IPv6, 3-IPv6andIPv4(X.S0057)
              AttachType - 1 InitialAttach, 3 HandoverAttach
              PDN_Address - IP Address of PDN, if it's IPv6andIPv4,
                            the last uint32 is IPv4
              APNLen - len of APN
              APN - string

              AuthType: 0-NULL, 1-PAP, 2-CHAP
              UserId:char string with NULL char end, username for PAP or CHAP
              pwdLen: length of pwd in bytes
              pwd: hex string , password for PAP or CHAP
              ModuleName:char string with NULL char end, OEM+ModuleNum  for CHAP challenge NAME

              PDPFlag : 0-NULL, 1-P-CSCF Addr Req
  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Ehrpd_Default_Bearer_Req(uint8 PdnId,uint8 PdnType,uint8 AttachType,
                                     uint32 *PDN_Address,uint8 APNLen,uint8 *APN,
                                     uint32* IP4RouterAddr, uint8 AuthType, uint8* UserId, uint8 pwdLen,uint8* pwd, uint8* ModuleName, uint8 PDPFlag);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Dedicated_Bearer_Req

  DESCRIPTION:
    This is used to setup dedicated bearer setup.
    Currently UE cannot do this according to VZW requirements

  PARAMETERS: PdnId - Indetifier of a PDN
              AddrType - 1:IPv4, 2:IPv6, 3:IPv6andIPv4(X.S0057)
              APNLen - Bytes number of APN string
              APN - string
              Num - number of ProfileID
              ProfileID - C.R1001
              Tft - Traffic Flow Template

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Ehrpd_Dedicated_Bearer_Req(uint8 PdnId,uint8 AddrType,uint8 APNLen,uint8 *APN,
                           uint8 Num,uint16 *ProfileID,Irat_QoMPktFilterContentT *Tft);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Detach_Req

  DESCRIPTION:
    Request complete ehprd detach, should disconnect all the eps bearers

  PARAMETERS: None

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Ehrpd_Detach_Req(void);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Bearer_Disconnect_Req

  DESCRIPTION:
    This is used to disconnect the bearer. This can be used for both dedicated
    or the default bearers.

  PARAMETERS: BearerId - Identifier of a Bearer
              PdnId - Identifier of a PDN
              DefaultBearer -1:default. 0:dedicated

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Ehrpd_Bearer_Disconnect_Req(uint8 BearerId,uint8 PdnId,uint8 DefaultBearer);

/*****************************************************************************

  FUNCTION NAME:     ValApi_Ehrpd_Set_IPv6_Addr


  DESCRIPTION:
    This is used to set a Pdn's IPv6 address.

  PARAMETERS:
              PdnId - Identifier of a PDN
              pIpv6Addr - 128 bits IPv6 address
              Type -1:Global IPv6 address. 0:link local IPv6 address

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Ehrpd_Set_IPv6_Addr(uint8 PdnId,uint8* pIPv6Addr, uint8 Type);

/*****************************************************************************

  FUNCTION NAME:     ValApi_Ehrpd_Clear_IPv6_Addr


  DESCRIPTION:
    This is used to clear a Pdn's IPv6 address.

  PARAMETERS:
              PdnId - Identifier of a PDN
              Type -1:Global IPv6 address. 0:link local IPv6 address

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Ehrpd_Clear_IPv6_Addr(uint8 PdnId, uint8 Type);

/*****************************************************************************

  FUNCTION NAME: ValApi_Disable_1X_Only_Req

  DESCRIPTION:
    Disable 1X only without changing DO status

  PARAMETERS: None

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Disable_1X_Only_Req(void);

/*****************************************************************************

  FUNCTION NAME: ValApi_Stop_Cdma_Meas_Req

  DESCRIPTION:
    This is used to cancel the ongoing CDMA meaurement initiated by earlier measure
    request(IRAT MEAS or SIB8 MEAS). this will cause no measurement result sent back.

  PARAMETERS: None

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Stop_Cdma_Meas_Req(void);


/*****************************************************************************

  FUNCTION NAME: ValApi_Cdma_Serving_Meas_Req

  DESCRIPTION:
    This is used to request DO chip to perform serving CDMA network signal measurement.

  PARAMETERS: SysType - IratCdmaTypeT, 1XRTT or HRPD.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Cdma_Serving_Meas_Req(IratCdmaTypeT SysType);

/*****************************************************************************

  FUNCTION NAME: ValApi_Cdma_Tx_Power_Req

  DESCRIPTION:
    This is used to request DO chip to return current CDMA Tx power.

  PARAMETERS: SysType - IratCdmaTypeT, 1XRTT or HRPD.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Cdma_Tx_Power_Req(IratCdmaTypeT SysType);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_DO_Acquisition_Req

  DESCRIPTION:
    This is used to request DO start scan channels based on PRL as RF master.
    The DO scan process is same for the two parameter values. The difference
    is how to report the scan result.

  PARAMETERS:
              scanType - BESTSYS : do PRL-based DO scan and only report the
                                   best system available. Not send result if no system acquired.
                         PSDECISN: do PRL-based DO scan and report the result regardless
                                   of DO system availability.

              scanOrder - 1: DO shall start scan immediately after received this cmd.
                          0: DO shall only start scan after LTE does scan since DO receives this cmd.
              ToKickOff1XScanning - 1X was off prior to this under certain situation. This parameter
                                              allows host to decide whether a kick to 1X scanning is necessary.

  RETURNED VALUES:  None.

*****************************************************************************/


/****************************************************************************

            API functions from  CDMA(eHRPD,HROD,1x) to LTE

*****************************************************************************/
/*******************************************************************************

  FUNCTION NAME: ValApi_CDMA_PowerUp_Rsp

  DESCRIPTION:
    CDMA Irat sends this response to MMC to confirm powered up.

  PARAMETERS: None

  RETURNED VALUES:  None

*******************************************************************************/
void ValApi_CDMA_PowerUp_Rsp(void);

/*******************************************************************************

  FUNCTION NAME: ValApi_CDMA_PowerDown_Rsp

  DESCRIPTION:
    CDMA Irat sends this response to MMC to confirm powered down.

  PARAMETERS: None

  RETURNED VALUES:  None

*******************************************************************************/
void ValApi_CDMA_PowerDown_Rsp(void);

/*****************************************************************************
  FUNCTION NAME: ValApi_Registration_Hybrid_Cnf

  DESCRIPTION:

    Returns status of hybrid Reg Req.

  PARAMETERS: Status1X - 1xRTT system status.
              StatusDO - EVDO system status
              PsTpye - IratPSTypeT. the PS type CDMA can support.
              SysPri - priority of the current PS serving system. 0xff means not available.

  RETURNED VALUES: None.

*****************************************************************************/

/*****************************************************************************

  FUNCTION NAME: ValApi_Registration_1X_Only_Cnf

  DESCRIPTION:

    Returns status of 1X reg req.

  PARAMETERS: Status1X - 1XRTT system status.
              SysPri - priority of 1X system. 0xff means not available.

  RETURNED VALUES: None.

*****************************************************************************/


/*****************************************************************************

  FUNCTION NAME: ValApi_Disable_1X_Only_Rsp

  DESCRIPTION:

    Returns status of 1X disable req.

  PARAMETERS:  status - 0: succeed, 1 - rejected, others - fail.

  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Disable_1X_Only_Rsp(uint8 status);

/*****************************************************************************

  FUNCTION NAME: ValApi_Data_Network_State_Change_Notify

  DESCRIPTION:

    Network notification for network state changed.
      - 1x RTT CS
      - 1x RTT PS
      - HRPD PS
      - eHRPD PS
      - Failure case

  PARAMETERS: Status1X - 1xRTT system status.
              StatusDO - EVDO system status
              PsTpye - IratPSTypeT
                       Following netwrok/PS reg reqs, the value is the PS type CDMA can
                       support. For others, the value is the result PS type of req handling.
                       if a req execute fail, IRAT_PS_NO will be returned.
              CurPsType - type of currently used PS
              Cause - IratNotifyCauseT
                       if a req comes at improper state, REG_WRONG will be returned.
              SysPri - priority of the current PS serving system. 0xff means not available.

  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Data_Network_State_Change_Notify(IratPSTypeT PsType, IratNotifyCauseT Cause);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Mcc_Search_Rsp

  DESCRIPTION:

    Returns MCC Search Result.

  PARAMETERS: SearchResult - when RAT is 1XRTT:
                               0: search fail. 1X system can not  be acquired or MCC is not available from neither network nor ePRL.
                               1: search succeed. Requested sys info is available.
                             when RAT is HRPD:
                               0: search fail. HRPD system can not  be acquired. Requested sys info is unavailable.
                               1: both MCC and SubnetID are available. MCC is from ePRL(subnetID/MCC mapping).
                               2: both MCC and SubnetID are available. MCC is from network.
                               3. only DO subnet ID is available, MCC is not available from neither network nor ePRL.

              Mcc - MCC code in BCD encoding for unencrypted value. C.S.0016D table4.5.4.1-1
              SysType - MSPLSysTypeT
              Sid - System ID of 1XRTT, valid when SysType=RAT_CDMA2KA1 or RAT_CDMA2K1X
              Nid - Network ID of 1XRTT, valid when SysType=RAT_CDMA2KA1 or RAT_CDMA2K1X
              SubnetId - subnetId(16 bytes), valid when SysType=RAT_CDMA2KHRPD

  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Mcc_Search_Rsp(uint8 SearchResult,uint16 Mcc,MSPLSysTypeT SysType,
                                uint16 Sid,uint16 Nid,uint8 *SubnetId);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Trigger_PS_Reg_Rsp

  DESCRIPTION:

    Returns result of Trigger PS Connection Req.

  PARAMETERS: Status1X - 1xRTT system status.
              StatusDO - EVDO system status
              PsTpye - IratPSTypeT. the PS type CDMA can support.
              SysPri - priority of the current PS serving system. 0xff means not available.

  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Trigger_PS_Reg_Rsp(IratPSTypeT PsType, c2k_irat_result_enum FailCause);

#ifdef MTK_DEV_C2K_SRLTE
/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Resel_Cnf

  DESCRIPTION:

    Returns result of LTE to C2K reselction request.

  PARAMETERS: .


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Resel_Cnf(IratEventReselCnfT * ReselResultInfo);
/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Redirect_Cnf

  DESCRIPTION:

    Returns result of LTE to C2K redirection request.

  PARAMETERS: .


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Redirect_Cnf(IratEventRedirectCnfT * RedirectResultInfo);
void ValApi_Irat_Resel_To_Lte_Ind(IratEventReselToLteIndT * ReselInfo);
#endif /* MTK_DEV_C2K_SRLTE */
/*****************************************************************************

  FUNCTION NAME: ValAi_Irat_Trigger_PS_Reg_Freq_Rsp

  DESCRIPTION:

    Returns result of Trigger PS connection on frequency F (received in RRC Connection
    release message on LTE) Req

  PARAMETERS: Status1X - 1xRTT system status.
              StatusDO - EVDO system status
              PsTpye - IratPSTypeT.  the PS type CDMA can support.
              SysPri - priority of the current PS serving system. 0xff means not available.

  RETURNED VALUES: None.

*****************************************************************************/

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Trigger_PS_Reg_CellIds_Rsp

  DESCRIPTION:

    Returns result of Trigger PS connection on a set of cell ids Req

  PARAMETERS: Status1X - 1xRTT system status.
              StatusDO - EVDO system status
              PsTpye - IratPSTypeT.  the PS type CDMA can support.
              SysPri - priority of the current PS serving system. 0xff means not available.

  RETURNED VALUES: None.

*****************************************************************************/

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Cdma_No_Service_Rsp

  DESCRIPTION:

    Confirm the perform of CDMA_NO_SERVICE command.

  PARAMETERS:  status - 0: succeed, others - fail.

  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Cdma_No_Service_Rsp(uint8 status);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Cdma_DataState_Ind

  DESCRIPTION:

    Notify when the cdma modem switches among the following state:
       - Active (PS is active on data path)
       - Dormant (ppp link still up, no TCH)
       - Idle (ppp inactivity expired and released, but PS still on CDMA side. Can
          reestablish PPP link if needed)
       - Suspend (just like dormant, but cannot reactivate. A transient state only)
       - Disabled (no PS on CDMA side, will wait for AP/LTE to send trigger to
         setup PS on CDMA)
       - Unaviable (caused by 1X is on voice call)

  PARAMETERS: state - current CDMA Data state


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Cdma_DataState_Ind(IratCdmaDataStateT state, IratPSTypeT PsType,
                                    IratPSTypeT CurPsType);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Measure_Result_Ind

  DESCRIPTION:

    Returns measurement result based on iRAT measurement information

  PARAMETERS: MeasResult - pointer to data block of IratMeasResultDataT


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Measure_Result_Ind(IratMeasResultDataT *MeasResult);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Measure_SIB8_Result_Ind

  DESCRIPTION:

    Returns measurement result based on SIB8 information

  PARAMETERS: MeasResult - pointer to data block of IratSIB8MeasResultDataT


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_Measure_SIB8_Result_Ind(IratSIB8MeasResultDataT *MeasResult);


/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_LTE_Measure_Info_Ind

  DESCRIPTION:

    CDMA sends current CDAM system info and LTE measurement info to LTE. CDMA
    only send this message when CDMA serving system strength below ThreshServing
    threshold specified by CDMA network(refer to C.S.0087 7.1).

  PARAMETERS: LteInfo - pointer to data block of IratEUTRANMeasureInfoT.
                        NULL means unavailable from CDMA network.
              CDMAInfo - pointer to data block of IratCDMASysInfoT. a Type value
                        IRAT_NULL means only field ServingPriority and ThreshServing available.


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Irat_LTE_Measure_Info_Ind(IratEUTRANMeasureInfoT *LteInfo,IratCDMASysInfoT *CDMAInfo);


/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Default_Bearer_Cnf

  DESCRIPTION:

    This is used to return the default bearer setup result of main PDN
    connection. Either the IP address on successful bearer setup or the
    error code is returned.

  PARAMETERS: pMsg - pointer to data block of Irat_NetwkConnRspMsgT


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Ehrpd_Default_Bearer_Cnf(Irat_NetwkConnRspMsgT *pMsg);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Dedicated_Bearer_Cnf

  DESCRIPTION:

    This is used to return the dedicated bearer setup result of Aux PDN connection
    Error code is returned on failure.

  PARAMETERS: pMsg - pointer to data block of Irat_EpsBearerSetupRspT


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Ehrpd_Dedicated_Bearer_Cnf(Irat_EpsBearerSetupRspT *pMsg);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Dedicated_Bearer_Ind

  DESCRIPTION:

    This is used to report the dedicated bearer setup , initiated by the Network.

  PARAMETERS: pMsg - pointer to data block of Irat_HlpBearerSetupIndT


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Ehrpd_Dedicated_Bearer_Ind(Irat_BearerSetupIndT *pMsg);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehprd_Detach_Cnf

  DESCRIPTION:

    Confirm the perform of EHRPD_DETACH command.

  PARAMETERS: status - 0: succeed, others: fail


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Ehprd_Detach_Cnf(uint8 status);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Bearer_Disconnect_Cnf

  DESCRIPTION:

    result of Aux PDN disconnection.

  PARAMETERS:  pMsg - pointer to data block of Irat_EpsBearerDisconnRspT


  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Ehrpd_Bearer_Disconnect_Cnf(Irat_EpsBearerDisconnRspT *pMsg);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Default_Bearer_Disconnect_Ind

  DESCRIPTION:

    indication of PDN/default bearer disconnection to inform LTE that the
    PDN/default bearer is disconnected by network

  PARAMETERS: PdnId - Identifier of a PDN
              BearerId - Identifier of a Bearer
              ReconnectionInd- Reconnection Indication: 1-reconnect to this APN isn't allowed

  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Ehrpd_Default_Bearer_Disconnect_Ind(uint8 PdnId, uint8 BearerId, uint8 ReconnectionInd);

/*****************************************************************************

  FUNCTION NAME: ValApi_Ehrpd_Bearer_Disconnect_Ind

  DESCRIPTION:

    indication of the dedicated Bearer disconnection to inform LTE that
    the dedicated bearer is disconnected by network

  PARAMETERS: pMsg - pointer to data block of Irat_EpsBearerDisconnIndT

  RETURNED VALUES: None.

*****************************************************************************/
void ValApi_Ehrpd_Bearer_Disconnect_Ind(Irat_EpsBearerDisconnIndT *pMsg);

/*****************************************************************************

  FUNCTION NAME: ValApi_Cdma_Serving_Meas_Rsp

  DESCRIPTION:
    This is used to send CDMA serving network signal measurement result.

  PARAMETERS: CdmaMeasResult - pionter to data block of IratCDMAMeasResultT
              SysPri - priority of the meas system. 0xff means not available.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Cdma_Serving_Meas_Rsp(IratCDMAMeasResultT *CdmaMeasResult,IratPriorityClassT SysPri);

/*****************************************************************************

  FUNCTION NAME: ValApi_Cdma_Tx_Power_Rsp

  DESCRIPTION:
    This is used to send current CDMA TX power.

  PARAMETERS: CdmaTxPower - CDMA Tx power in Q7 dBm format.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Cdma_Tx_Power_Rsp(int16 CdmaTxPower);

/*****************************************************************************

  FUNCTION NAME: ValApi_Cdma_Scan_Status_Ind

  DESCRIPTION:
    This is used to indicate the event of 1X or DO scan and OOSA Sleep.

  PARAMETERS: Scan1X - 0:scanning 1X, 1: 1X enters OOSA Sleep or Idle.
              ScanDO - 0:scanning DO, 1: DO enters OOSA Sleep or idle.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Cdma_Scan_Status_Ind(uint8 Scan1X, uint8 ScanDO);

/*****************************************************************************

  FUNCTION NAME: ValApi_Irat_Cmd_Reject_Ind

  DESCRIPTION:
    This is used to indicate the IRAT cmd is rejected by IRAT for specified reason.

  PARAMETERS:
              IratCdmId - IRAT cmd ID.
              Reason - rejection code for reason or subcmd.

  RETURNED VALUES:  None.

*****************************************************************************/
void ValApi_Irat_Cmd_Reject_Ind(IRATMsgT IratCdmId, IRATCmdRejCodeT Reason);

/*****************************************************************************
  FUNCTION NAME: ValApi_Irat_DO_Acquisition_Rsp

  DESCRIPTION:
    This is used to confirm RF owner status or report the scan result as requested.

  PARAMETERS:
              Result ?Acquired: a system is acquired. The info will be included in DOSysInfo.
              AcqFail- no system is acquired. DOSysInfo is invalid.
              MasterCnf - confirm DO enters RF master operation. DOSysInfo is invalid.

              DOSysInfo ?Irat_DOSysInfoT

  RETURNED VALUES:  None.

*****************************************************************************/

void ValApi_Irat_C2K_LTE_Rat_Change_Req(c2k_lte_rat_change_req_struct *pMsg);
void ValApi_Irat_C2K_LTE_Rat_Change_Cnf(c2k_lte_rat_change_cnf_struct *pMsg);
void ValApi_Irat_LTE_C2K_Rat_Change_Req(lte_c2k_rat_change_req_struct *pMsg);
void ValApi_Irat_LTE_C2K_Rat_Change_Cnf(lte_c2k_rat_change_cnf_struct *pMsg);
void ValApi_Irat_Rat_Change_Start_Req(val_rac_rat_change_start_req_struct *pMsg);
void ValApi_Irat_Rat_Change_Finish_Req(val_rac_rat_change_finish_req_struct *pMsg);

#ifdef MTK_DEV_C2K_IRAT
void ValApi_Irat_Sdm_Val_C2k_Sms_Send_Req(sdm_val_c2k_sms_send_req_struct *pMsg);
void ValApi_Irat_Sdm_Val_C2k_Sms_Send_Cnf(sdm_val_c2k_sms_send_cnf_struct *pMsg);
void ValApi_Irat_Imcsms_Val_C2k_Sms_Send_Req(imcsms_val_c2k_sms_send_req_struct *pMsg);
void ValApi_Irat_Imcsms_Val_C2k_Sms_Send_Cnf(imcsms_val_c2k_sms_send_cnf_struct *pMsg);
void ValApi_Irat_Sdm_Val_C2k_Sms_Ind(sdm_val_c2k_sms_ind_struct *pMsg);
void ValApi_Irat_Sdm_Val_C2k_Sms_Rsp(sdm_val_c2k_sms_rsp_struct *pMsg);
void ValApi_Irat_L4c_Val_Mdstatus_TK_BSP_Ind(l4c_val_mdstatus_tk_bsp_ind_struct *pMsg);
void ValApi_Irat_LTE_C2K_IA_Pdn_Query_Req(void);
void ValApi_Irat_LTE_C2K_IA_Pdn_Query_Cnf(lte_c2k_ia_pdn_query_cnf_struct *pMsg);
void ValApi_Irat_C2K_LTE_Pdn_Transfer_Req(c2k_lte_pdn_context_transfer_req_struct *pMsg);
void ValApi_Irat_C2K_LTE_Pdn_Transfer_Cnf(void);
void ValApi_Irat_LTE_C2K_Pdn_Transfer_Req(lte_c2k_pdn_context_transfer_req_struct *pMsg);
void ValApi_Irat_LTE_C2K_Pdn_Transfer_Cnf(void);
void ValApi_Irat_Imcsms_Val_C2k_Sms_Recv_Req(imcsms_val_c2k_sms_recv_req_struct *pMsg);
void ValApi_Irat_Imcsms_Val_C2k_Sms_Recv_Cnf(imcsms_val_c2k_sms_recv_cnf_struct *pMsg);

#endif

void ValApi_Irat_Rat_Change_Ind(IratPsRatChangeIndT *);
void ValApi_Irat_Rat_Change_Rsp(IratPsRatChangeRspT*);

/* define the val general trace index for IRAT */
enum
{
  VAL_IRAT_GEN_TRACE_INDEX_START = 10,
  VAL_IRAT_GEN_TRACE_INDEX_1 = VAL_IRAT_GEN_TRACE_INDEX_START,
  VAL_IRAT_GEN_TRACE_INDEX_2,
  VAL_IRAT_GEN_TRACE_INDEX_3,
  VAL_IRAT_GEN_TRACE_INDEX_4,
  VAL_IRAT_GEN_TRACE_INDEX_5,
  VAL_IRAT_GEN_TRACE_INDEX_END
};

typedef enum
{
/*  0 - LTE modem active;
 *   1 - C2K modem active and only MD1's SIM task active;
 *   2 - LTE modem and LTE modem are both active; */
    VAL_IRAT_LTE_MD_ACTIVE,
    VAL_IRAT_LTE_SIM_ONLY_AND_C2K_MD_ACTIVE,
    VAL_IRAT_C2K_LTE_BOTH_ACTIVE,
    VAL_IRAT_MODEM_STATUS_NUM,
    VAL_IRAT_INVALID = 0xff /* for initialisation */
} ValIratModemStatusT;

typedef enum
{
    VAL_IRAT_C2K_ONLY_MODE = 0,
    VAL_IRAT_LTE_C2K_MODE = 1,
    VAL_IRAT_4_MODE_MODE = 2,
    VAL_IRAT_C2K_EHRPD_MODE = 3,
    VAL_IRAT_6_MODE_MODE = 4,
    VAL_IRAT_MODE_NUM
} ValIratModeT;

#ifdef MTK_DEV_ENGINEER_MODE
typedef enum
{
    VAL_ECLSC_ENABLE_MODE = 0,
    VAL_ECLSC_DISABLE_MODE = 1,
    VAL_ECLSC_MODE_NUM
} ValLECLSCModeT;
#endif

typedef enum
{
  VAL_CSS_DEAC_POWER_DOWN,
  VAL_CSS_DEAC_CARD_ERROR,
  VAL_CSS_DEAC_CARD_ERROR_THEN_POWER_DOWN,
  VAL_CSS_DEAC_NO_REASON = 0xff
}ValCssDeactiveReasonT;

typedef enum
{
  C2K_DEFAULT_EMDSTATUS_FLOW = 0,/* MD3 wait AP to config EMDSTATUS */
  C2K_INVALID_EMDSTATUS_FLOW = 0xff
}ValEmdstatusFlowVersionT;

typedef struct
{
  bool                      InterRatOperationEnable;
  ValIratModeT              Mode;
#ifdef MTK_DEV_ENGINEER_MODE
  ValLECLSCModeT            EclscMode;
#endif
  uim_access_option_enum    CardAccessOption;
  ValIratModemStatusT       MdStatus;
  bool                      MD1RfClosed;/*  MD1 is EFUN=0 or not */
  bool                      WaitingMD1RfStatus;/* indicate MD3 starts to do EPOF, wait MD1 to enter EFUN=0 */
  bool                      WaitForCssDeactivate;/* start to deactivate CSS irat action, then wait the result */
  ValCssDeactiveReasonT     CssDeacReason;/* reason why to deactivate CSS IRAT action */
  bool                      CssPowerDownDeactCnfReceived;/* CSS IRAT action is clear for power down reason */
  ValEmdstatusFlowVersionT  EmdstatusFlowVersion;/* version = 0: MD3 wait AP to config EMDSTATUS, otherwise wait MD1 to config EMDSTATUS */
}ValIratInfoT;
/*****************************************************************************

  FUNCTION NAME: ValIratSetModemStatus

  DESCRIPTION:

    internal help function: set status of MD1 and MD3.

  PARAMETERS:
                    ModemStatus - MD1 active or MD3 active or both
                    UimAccessOptiop - access local UIM or access remote SIM1 or access remote SIM2

  RETURNED VALUES: None.

*****************************************************************************/
void ValIratSetModemStatus(ValIratModemStatusT ModemStatus, uim_access_option_enum UimAccessOptiop);

/*****************************************************************************

  FUNCTION NAME: ValIratGetSimAccessIndex

  DESCRIPTION:

    internal help function: get SIM access index.

  PARAMETERS:
                   none
  RETURNED VALUES:
                    UimAccessOptiop - access local UIM or access remote SIM1 or access remote SIM2.

*****************************************************************************/
uim_access_option_enum ValIratGetSimAccessIndex(void);

/*****************************************************************************

  FUNCTION NAME: ValIratGetInterRatOperationStatus

  DESCRIPTION:

    internal help function: get the status of inter-rat operation.

  PARAMETERS:
                    none

  RETURNED VALUES:
                            TRUE - enable inter-rat measurement for handover, and CSS
                                      can interface to NWSEL of MD1, MD3 does
                                      searching and registration according GMSS
                            FALSE - disable inter-rat measurement for handover, and
                                        CSS no need to interface to NWSEL of MD1, MD3 does
                                        searching and registration as CDMA only

*****************************************************************************/
bool ValIratGetInterRatOperationStatus(void);

void ValIratSetInterRatOperationStatus(bool status);

uint32 ValIratSendMd1MsgToCc(uint32 MsgId, void* MsgBufferP, uint32 MsgSize);

uint32 ValCssIratMd1MsgHandler(uint32 MsgId, void* MsgBufferP, uint32 MsgSize);

ValIratModeT ValApi_Irat_GetIratMode(void);

uint8 ValApi_Irat_GetNwErrorCode(void);

void ValApi_Irat_SetNwErrorCode(uint8);

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_DEV_C2K_SRLTE)
uint32 ClcIratMd1MsgHandler(uint32 MsgId, void* MsgBufferP, uint32 MsgSize);
#endif

#if defined (MTK_DEV_C2K_IRAT) && defined (MTK_CBP)
void ValRcvC2kRxTxTimeinfoDataReq(l4c_c2k_txrx_active_time_req_struct* pMsg);
void ValSendL4cC2kRxTxTimeinfoRspToMd1(void);

void ValSendC2kL4cCsStatusReqToMd1(uint8 bCallStatu);
void ValSendC2kL4cCsEccStatusReqToMd1(uint8 bEccCallStatus);


#endif
#if defined (MTK_DEV_C2K_IRAT)
void ValSendIRATMsgToCssMb2(IRATMsgT cmd, uint8 *paramBlock,  uint16 paramBlkLength);
#endif

#endif /* MTK_DEV_C2K_IRAT */

#ifdef __cplusplus
}
#endif
#endif /*_VALIRATAPI_H_*/

/**Log information: \main\CBP80\cbp80_yzhang_scbp10127\1 2012-08-03 06:39:25 GMT yzhang
** Sprint EHRPD requirement:IPCP,AUTH,DNS Server Addr in PCO of VSNCP**/
/**Log information: \main\Trophy\Trophy_xding_href22331\1 2013-12-10 07:54:01 GMT xding
** HREF#22331, 合并MMC相关功能到Trophy baseline上**/
/**Log information: \main\Trophy\1 2013-12-10 08:35:53 GMT jzwang
** href#22331:Merge MMC latest implementation from Qilian branch.**/
