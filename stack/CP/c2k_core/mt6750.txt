/*****************************************************************************
*  Copyright Statement:
*  --------------------
*  This software is protected by Copyright and the information contained
*  herein is confidential. The software may not be copied and the information
*  contained herein may not be used or disclosed except with the written
*  permission of MediaTek Inc. (C) 2015
*
*  BY OPENING THIS FILE, BUYER HEREBY UNEQUIVOCALLY ACKNOWLEDGES AND AGREES
*  THAT THE SOFTWARE/FIRMWARE AND ITS DOCUMENTATIONS ("MEDIATEK SOFTWARE")
*  RECEIVED FROM MEDIATEK AND/OR ITS REPRESENTATIVES ARE PROVIDED TO BUYER ON
*  AN "AS-IS" BASIS ONLY. MEDIATEK EXPRESSLY DISCLAIMS ANY AND ALL WARRANTIES,
*  EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE IMPLIED WARRANTIES OF
*  MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE OR NONINFRINGEMENT.
*  NEITHER DOES MEDIATEK PROVIDE ANY WARRANTY WHATSOEVER WITH RESPECT TO THE
*  SOFTWARE OF ANY THIRD PARTY WHICH MAY BE USED BY, INCORPORATED IN, OR
*  SUPPLIED WITH THE MEDIATEK SOFTWARE, AND BUYER AGREES TO LOOK ONLY TO SUCH
*  THIRD PARTY FOR ANY WARRANTY CLAIM RELATING THERETO. MEDIATEK SHALL ALSO
*  NOT BE RESPONSIBLE FOR ANY MEDIATEK SOFTWARE RELEASES MADE TO BUYER'S
*  SPECIFICATION OR TO CONFORM TO A PARTICULAR STANDARD OR OPEN FORUM.
*
*  BUYER'S SOLE AND EXCLUSIVE REMEDY AND MEDIATEK'S ENTIRE AND CUMULATIVE
*  LIABILITY WITH RESPECT TO THE MEDIATEK SOFTWARE RELEASED HEREUNDER WILL BE,
*  AT MEDIATEK'S OPTION, TO REVISE OR REPLACE THE MEDIATEK SOFTWARE AT ISSUE,
*  OR REFUND ANY SOFTWARE LICENSE FEES OR SERVICE CHARGE PAID BY BUYER TO
*  MEDIATEK FOR SUCH MEDIATEK SOFTWARE AT ISSUE.
*
*  THE TRANSACTION CONTEMPLATED HEREUNDER SHALL BE CONSTRUED IN ACCORDANCE
*  WITH THE LAWS OF THE STATE OF CALIFORNIA, USA, EXCLUDING ITS CONFLICT OF
*  LAWS PRINCIPLES.  ANY DISPUTES, CONTROVERSIES OR CLAIMS ARISING THEREOF AND
*  RELATED THERETO SHALL BE SETTLED BY ARBITRATION IN SAN FRANCISCO, CA, UNDER
*  THE RULES OF THE INTERNATIONAL CHAMBER OF COMMERCE (ICC).
*
*****************************************************************************/

/*****************************************************************************
 *
 * Filename:
 * ---------
 *   mt6750.txt
 *
 * Project:
 * --------
 *   C2K jade project
 *
 * Description:
 * ------------
 *   defines the memory map for the validation board

 *============================================================================

 ****************************************************************************/
#define DEFAULT_ALIGN 4 
#define CHECK_ALIGN(Adr,Align) ((Adr) % (Align) == 0)

#define REGION_ROM_BASE    0x00000000
#define REGION_ROM_SIZE    0x00800000     /* The size of bin file */
#define REGION_LOGRAM_BASE 0x00A00000
#define REGION_LOGRAM_SIZE 0x00200000
#define SRAM_BASE          0x00000000
#define SRAM_SIZE          0x00C00000
#define REGION_IRAM_BASE   0x51000000
#define REGION_IRAM_SIZE   (0x00018000 - 0x10)

OUTPUT_FORMAT("elf32-littlearm", "elf32-littlearm", "elf32-littlearm")
OUTPUT_ARCH(arm)
ENTRY(CPInit)
EXTERN(INT_VECTORS)
EXTERN(FLASH_VALID)
EXTERN(JumpCode)
MEMORY
{   
    ROM          : ORIGIN = REGION_ROM_BASE,    LENGTH = REGION_ROM_SIZE
    SRAM_REGION  : ORIGIN = SRAM_BASE,          LENGTH = SRAM_SIZE
    IRAM_REGION  : ORIGIN = REGION_IRAM_BASE,   LENGTH = REGION_IRAM_SIZE
}

SECTIONS
{
    FLASH  0x0  :  ALIGN(4)
    {
      *monutil.obj (EMI_BOOT)
      . = ALIGN(4);
      Image$$FLASH$$Base = . ;
      Image$$FLASH$$RO$$Base = . ;
      *copy_gcc.obj      (Copy)
      *mmuutil_gcc.obj   (MMUUtils)
      *sysutil_misc_gcc.obj (Vector_Table_Exception)
      *                  (CodeInFlash)
      *                  (CodeInSram)
      *                  (INTSRAM_ROCODE)
      *lmddbuf.obj       (LMDCodeInIRAM)
      * (ARMex)
      * (i.*)
      *hwdrfcustnvcfg.obj   (.data*)
      * (EXCLUDE_FILE(*vectordemux.obj
                      *monutil_iram.obj
                      *lmdbit.obj  
                      *lmdcrc.obj
                      *hwdcal_rt.obj
                      *sysdotime.obj
                      *rcptask.obj
                      *rtm_iram.obj
                      *txdma.obj
                      *rtmqos.obj
                      *syscommon.obj
                      *rcptxh_isr.obj
                      *exeutil_iram.obj
                      *cpbuf_iram.obj
                      *quc.obj
                      *pmc.obj
                      *lib_a-str*.o
                      *lib_a-mem*.o
                      *_divsi3.o
                      *_udivsi3.o
                      ).text*)
                      
      *                  (.rodata.str1.1)
      *                  (.rodata*)
      *platform_porting_gcc.obj (INT_local_data)
      . = ALIGN(4);
      Image$$FLASH$$Limit = . ;
      Image$$FLASH$$RO$$Limit = . ;
    } >SRAM_REGION   AT> ROM
    .preinit_array     :
    {
      __preinit_array_start = .;
      KEEP (*(.preinit_array))
      __preinit_array_end = .;
    } >SRAM_REGION   AT> ROM
    .init_array :
    {
      __init_array_start = .;
      KEEP (*(.init_array))
      __init_array_end = .;
    } >SRAM_REGION  AT> ROM
    .ARM.extab .  :  ALIGN(4)
    {
      __extab_start = .;
      * (.ARM.extab*)
      __extab_end = .;
    }>SRAM_REGION   AT> ROM
    .ARM.exidx .  :  ALIGN(4)
    {
      __exidx_start = .;
      * (.ARM.exidx*)
      __exidx_end = .;
    } >SRAM_REGION  AT> ROM
    /* The second link will modify the current position. */
    . = .;
    Image$$RO_REGION$$Limit = .;
    TLB_ADDRESS ALIGN(0x4000) (NOLOAD) :  ALIGN(4)
    /* 
     * the size of SRAM0 is better to be the multiple of 4K
     * because we use small page (4KB) for the region
     * check sys/mmuutil.s when you want to change size, or start address of this region
     * start address should be 16kB align
     */
    {
      Image$$TLB_ADDRESS$$Base = . ;
      *mmuutil_gcc.obj   (mmu_tt)
      . = ALIGN(4);
      Image$$TLB_ADDRESS$$Limit = . ;
    } >SRAM_REGION AT> ROM
        
    
    SRAM0 . :  ALIGN(4)
    {
      /* Place in this are code that should not be cached. */
      Image$$SRAM0$$Base = . ;
      *                  (DmaRwData)
      Image$$SRAM0$$Limit = . ;
    } >SRAM_REGION  AT> ROM
    
    SRAM0_ZI . (NOLOAD)  :  ALIGN(4)
    {
      Image$$SRAM0$$ZI$$Base = . ;
      *                  (DmaData)
      *montask.obj       (NoCachedData)
      *exetaskstack.obj  (ZiDataNonCache)
      *exeutil.obj       (SharedDbgInfo)
      . = ALIGN(4);
      
      Image$$SRAM0$$ZI$$Limit = . ;
    } >SRAM_REGION AT> ROM
    
    SRAM ALIGN(0x1000) :  ALIGN(4)
    {
      Image$$SRAM$$Base = . ;
      Image$$SRAM$$RO$$Base = . ;
      Image$$SRAM$$RO$$Limit = . ;
      *platform_porting_gcc.obj (INT_Data)
      *platform_porting_gcc.obj (NU_Thread_Id_Data)
      *fcptask.obj       (DataInSram)
      *                  (C$$data)
      * (EXCLUDE_FILE(*hwdgdma.obj
                      *syscommon.obj
                      *rtmqos.obj
                      *PPPHA.obj).data*)
      . = ALIGN(4);
      _nvram_ltable$$Base = .;
       * (_nvram_ltable)
      _nvram_ltable$$Limit = .;
      _nvram_ltable$$Length = ABSOLUTE(_nvram_ltable$$Limit - _nvram_ltable$$Base);      
      Image$$SRAM$$Limit = . ;
    } >SRAM_REGION AT> ROM
    .bss . (NOLOAD) :  ALIGN(4)
    {
      Image$$SRAM$$ZI$$Base = . ;
      * (C$$zidata)
      
      * (EXCLUDE_FILE(*hwdmtksdioslave.obj
                      *cpbuf_iram.obj
                      *rmcisr.obj) ZiDataInIram)
      *valmisccust.obj   (VAL_heap_no_cache)
      *init.obj          (no_cache_bss)
      *valspc_io.obj     (NoCachedData)
      *ioprpcmgr.obj     (ZiDataNonCache)
      *init.obj          (no_cache_bss)
      * (EXCLUDE_FILE(*hwdgdma.obj).bss*)
      * (COMMON)
                      
      *platform_porting_gcc.obj          (abort_stack)
      *platform_porting_gcc.obj          (sys_mode_stack)
      *platform_porting_gcc.obj          (undef_stack)
      *platform_porting_gcc.obj          (Heap)
             
      . = ALIGN(4);
      Image$$SRAM$$ZI$$Limit = . ;
    } >SRAM_REGION AT> ROM

#if (defined MTK_DEV_OPTIMIZE_LOGGING)
    LOGBUF (REGION_LOGRAM_BASE) (NOLOAD) :  ALIGN(4)
    {
      *monlogopt.obj       (LogBufferSec)
    } >SRAM_REGION AT> ROM
#endif

    IRAM (REGION_IRAM_BASE)  :  ALIGN(4)
    {
      Image$$IRAM$$Base = . ;
      Image$$IRAM$$RO$$Base = . ;
      *vector_gcc.obj    (Vector_Table)
      *vector_gcc.obj    (Vector_Table1)
      *sysutil_gcc.obj   (C$$code)
      *rcptxh.obj        (RcpTxhCodeInIram)
      *pswtrace.obj      (PSWCodeInIRAM)
      *rlp_ctl.obj       (RLPCodeInIRAM)
      *rlp_que.obj       (RLPCodeInIRAM)
      *rlp_rtc.obj       (RLPCodeInIRAM)
      
      *                  (CodeInIram)

      
      *vectordemux.obj   (.text*)
      *monutil_iram.obj  (.text*)
      *lmdbit.obj        (.text*)
      *lmdcrc.obj        (.text*)
      *hwdcal_rt.obj     (.text*)
      *sysdotime.obj     (.text*)
      *rcptask.obj       (.text*)
      *rtm_iram.obj      (.text*)
      *txdma.obj         (.text*)
      *rtmqos.obj        (.text*)
      *syscommon.obj     (.text*)
      *rcptxh_isr.obj    (.text*)
      *exeutil_iram.obj  (.text*)
      *cpbuf_iram.obj    (.text*)
      *quc.obj           (.text*)
      *pmc.obj           (.text*)
      
      *lib_a-str*.o      (.text*)
      *lib_a-mem*.o      (.text*)
      *_divsi3.o         (.text*)
      *_udivsi3.o        (.text*)
      
      Image$$IRAM$$RO$$Limit = . ;
      
      /*****************
       *  RW Data Segment
       *****************/
      *rmcisr.obj        (DataInIram)
      *hwdgdma.obj       (.data*)
      *syscommon.obj     (.data*)
      *rtmqos.obj        (.data*)
      *PPPHA.obj         (.data*)

      . = ALIGN(4);
      Image$$IRAM$$Limit = . ;
    } >IRAM_REGION AT> ROM
    IRAM_ZI . (NOLOAD) :  ALIGN(4)
    {
      Image$$IRAM$$ZI$$Base = .;
      *hwdgdma.obj       (.bss*)
      *hwdmtksdioslave.obj (ZiDataInIram)
      *cpbuf_iram.obj    (ZiDataInIram)
      *rmcisr.obj        (ZiDataInIram)
      
      /****************
       *  Stack
       ****************/
      *platform_porting_gcc.obj          (irq_stack)
      *platform_porting_gcc.obj          (fiq_stack)
      *platform_porting_gcc.obj          (system_stack)
      *platform_porting_gcc.obj          (hisr_stack)
       . = ALIGN(4);
      Image$$IRAM$$ZI$$Limit = . ;
    } >IRAM_REGION AT> ROM
    
    /***************************************
     * Flash pattern to indicate valid load
     ***************************************/
    FLASHV .  :  ALIGN(4)
    {
      *flashvalid_gcc.obj  (flashvalid)
    }  AT> ROM
    
    Image$$FLASH$$Length = Image$$FLASH$$Limit - Image$$FLASH$$Base;
    Image$$SRAM0$$Length = Image$$SRAM0$$Limit - Image$$SRAM0$$Base;
    Image$$SRAM$$Length = Image$$SRAM$$Limit - Image$$SRAM$$Base;
    Image$$IRAM$$Length = Image$$IRAM$$Limit - Image$$IRAM$$Base;
    Image$$SRAM$$ZI$$Length = Image$$SRAM$$ZI$$Limit - Image$$SRAM$$ZI$$Base;
    Image$$IRAM$$ZI$$Length = Image$$IRAM$$ZI$$Limit - Image$$IRAM$$ZI$$Base;
    Image$$SRAM0$$ZI$$Length = Image$$SRAM0$$ZI$$Limit - Image$$SRAM0$$ZI$$Base;
    Image$$IRAM$$RO$$Length = Image$$IRAM$$RO$$Limit - Image$$IRAM$$RO$$Base;
    Image$$FLASH$$RO$$Length = Image$$FLASH$$RO$$Limit - Image$$FLASH$$RO$$Base;
    
    Image$$SRAM$$End = SRAM_BASE + SRAM_SIZE - REGION_LOGRAM_SIZE;
    
    Load$$SRAM$$Base  = LOADADDR(SRAM);
    Load$$SRAM0$$Base  = LOADADDR(SRAM0);
    Load$$IRAM$$Base = LOADADDR(IRAM);
    Load$$FLASHV$$Base = LOADADDR(FLASHV);
    

    ASSERT(Image$$SRAM$$ZI$$Limit <= Image$$SRAM$$End, "ASSERT:SRAM SIZE exceed limit!")
    /*copy.s  must 4 bytes align!!!  As we use LDR/STR instruction . check at here for security*/
    ASSERT(CHECK_ALIGN(Image$$IRAM$$Length, DEFAULT_ALIGN) && CHECK_ALIGN(Load$$IRAM$$Base, DEFAULT_ALIGN) && CHECK_ALIGN(Image$$IRAM$$Base, DEFAULT_ALIGN),"ASSERT:alignment err")
    ASSERT(CHECK_ALIGN(Image$$SRAM$$Length, DEFAULT_ALIGN) && CHECK_ALIGN(Load$$SRAM$$Base, DEFAULT_ALIGN) && CHECK_ALIGN(Image$$SRAM$$Base, DEFAULT_ALIGN),"ASSERT:alignment err")
    ASSERT(CHECK_ALIGN(Image$$SRAM0$$Length, DEFAULT_ALIGN) && CHECK_ALIGN(Load$$SRAM0$$Base, DEFAULT_ALIGN) && CHECK_ALIGN(Image$$SRAM0$$Base, DEFAULT_ALIGN),"ASSERT:alignment err")
   
    PROVIDE (end = .);
}
